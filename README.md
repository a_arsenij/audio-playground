# Audio-playground

Live version is [available here](https://audio-playground.yoloswag.xyz)

# Instructions for developers:

## Install dependencies

```bash
npm ci
```

## Build

```bash
npm run build
# Result will be located at frontend/dist
```
or
```bash
npm run watch
# Result will be located at frontend/dist
```

```bash
npm run serve
```

## Run tests

```bash
npm run test
```
## TODO

### General features:

- [x] Change length of midi-track-usages in playlist window
- [x] Delete plugins in channels window
- [x] Delete channels
- [x] Move channels left-right (up-down)
- [x] Delete midi-tracks
- [x] Copy midi-tracks
- [x] Custom signal routing in mixer
- [x] Playing midi track
- [x] Playing notes via keyboard
- [x] Soloing channels
- [x] Dropping wavs into playlist
- [x] Automations
- [x] Save/load project
- [x] Render project
- [x] Stereo channels
- [x] Show signal level in channels/playlist
- [x] Wrap/unwrap plugins UI in mixer
- [ ] Notes should have volumes
- [ ] Midi-morphing plugins
- [ ] Cut clips in timelines into two pieces
- [ ] Store big chunks of data (e.g. wavs) somewhere outside of project state object
- [ ] Scrollbar that does not take space
- [ ] Make sample rate configurable
- [ ] Render options dialog (sample rates, bits per sample)
- [x] New icons for save/open/export actions
- [ ] Auto delete automations for deleted plugins
- [ ] Record waves

- [ ] "Are you sure you want to delete X?" popup
- [ ] Plugins picker
- [ ] Disable snapping on alt+move/alt+resize
- [ ] Yellow and red colors of meters should stand for -6db and 0db

- [ ] WavsAsyncTree show loading state for play button
- [ ] Remember previous selected clip timings while adding new in playlist

### New plugins:

- [x] Reverb
- [x] EQ/filter
- [x] WaveShaper
- [ ] Delay
- [ ] Soundfont player
- [ ] Volume changer
- [ ] Stereo shaper

### Features for existing plugins:

- [x] Pitch (integer) and fine-tune (-0.5...0.5) knobs for SimpleSynth
- [ ] Envelope for filter for SimpleSynth
- [ ] More filter settings for SimpleSynth (high-pass, resonance, ...)

### New UI components:

- [ ] Loader/Spinner
  - [ ] Use it in AsyncTree
- [ ] Draggable separator
  - [ ] Use it inbetween ClipsNavigator and AsyncTree
