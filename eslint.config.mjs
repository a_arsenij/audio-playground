import {
    codeConfig,
    initEslint,
    testConfig
} from "./eslint-commons.config.mjs";

export default [
    initEslint,
    codeConfig,
    testConfig,
];
