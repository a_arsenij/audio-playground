export function amplitudeToDb(
    amplitude: number,
): number {
    return 20 * Math.log10(amplitude);
}

export function dbToAmplitude(
    db: number,
): number {
    return 10 ** (db / 20);
}
