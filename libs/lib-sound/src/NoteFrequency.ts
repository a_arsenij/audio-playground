export const NOTES_COUNT = 9 * 12;

export function noteToFrequency(note: number) {
    return 27.5 * 2 ** (note / 12);
}

export function frequencyToNote(frequency: number) {
    return Math.log2(frequency / 27.5) * 12;
}

const NOTE_NAMES = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];

const NOTE_TYPES = [
    "WHITE", "BLACK",
    "WHITE", "BLACK",
    "WHITE",
    "WHITE", "BLACK",
    "WHITE", "BLACK",
    "WHITE", "BLACK",
    "WHITE",
] as const;

export function noteToType(note: number) {
    return NOTE_TYPES[note % 12]!;
}

export function noteToTitle(note: number) {
    const octave = Math.floor(note / 12) + 1;
    const tone = NOTE_NAMES[note % 12]!;
    return tone + octave;
}
