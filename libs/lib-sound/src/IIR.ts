import { arrayOfNItems } from "@audio-playground/lib-common/std/Array";

import { FilterType } from "./FilterType";

export type BiquadFilter = {
    a1: number,
    a2: number,
    b0: number,
    b1: number,
    b2: number,
}

export const ZERO_BIQUAD_FILTER = { a1: 0, a2: 0, b0: 1, b1: 0, b2: 0 };

export function butterworthLowPass(
    cutFrequency: number,
    sampleRate: number,
): BiquadFilter {

    const wp = Math.tan(
        Math.PI * cutFrequency / sampleRate,
    );

    const bottom = 1 + wp ** 2 + Math.sqrt(2) * wp;

    const a = [
        1,
        (-2 + 2 * wp ** 2) / bottom,
        (1 + wp ** 2 - Math.sqrt(2) * wp) / bottom,
    ];

    const b = [
        (wp ** 2) / bottom,
        2 * (wp ** 2) / bottom,
        (wp ** 2) / bottom,
    ];

    return {
        a1: a[1]!,
        a2: a[2]!,
        b0: b[0]!,
        b1: b[1]!,
        b2: b[2]!,
    };
}

export function butterworthHighPass(
    cutFrequency: number,
    sampleRate: number,
): BiquadFilter {

    const wp = Math.tan(
        Math.PI * cutFrequency / sampleRate,
    );

    const bottom = 1 + Math.sqrt(2) * wp + wp ** 2;

    const a = [
        1,
        (-2 + 2 * wp ** 2) / bottom,
        (1 - Math.sqrt(2) * wp + wp ** 2) / bottom,
    ];

    const b = [
        1 / bottom,
        -2 / bottom,
        1 / bottom,
    ];

    return {
        a1: a[1]!,
        a2: a[2]!,
        b0: b[0]!,
        b1: b[1]!,
        b2: b[2]!,
    };
}

const BANDPASS_LOW_FREQ_COEF = 0.8;
const BANDPASS_HIGH_FREQ_COEF = 1 / 0.8;

export function butterworthBandPass(
    cutFrequency: number,
    sampleRate: number,
): BiquadFilter {

    const wp1 = Math.tan(
        Math.PI * cutFrequency * BANDPASS_LOW_FREQ_COEF / sampleRate,
    );
    const wp2 = Math.tan(
        Math.PI * cutFrequency * BANDPASS_HIGH_FREQ_COEF / sampleRate,
    );

    const wo2 = wp1 * wp2;
    const W = wp2 - wp1;

    const bottom = -1 - W - wo2;

    const a = [
        1,
        (2 - 2 * wo2) / bottom,
        (-1 + W - wo2) / bottom,
    ];

    const b = [
        -W / bottom,
        0,
        W / bottom,
    ];

    return {
        a1: a[1]!,
        a2: a[2]!,
        b0: b[0]!,
        b1: b[1]!,
        b2: b[2]!,
    };
}

export function lowShelf(
    dBgain: number,
    slope: number,
    cutFrequency: number,
    sampleRate: number,
): BiquadFilter {

    const A = Math.sqrt(10 ** (dBgain / 20));
    const w0 = 2 * Math.PI * cutFrequency / sampleRate;
    const alpha = Math.sin(w0) / 2 * Math.sqrt((A + 1 / A) * (1 / slope - 1) + 2);

    const b0 = A * ((A + 1) + (A - 1) * Math.cos(w0) + 2 * Math.sqrt(A) * alpha);
    const b1 = -2 * A * ((A - 1) + (A + 1) * Math.cos(w0));
    const b2 = A * ((A + 1) + (A - 1) * Math.cos(w0) - 2 * Math.sqrt(A) * alpha);
    const a0 = (A + 1) - (A - 1) * Math.cos(w0) + 2 * Math.sqrt(A) * alpha;
    const a1 = 2 * ((A - 1) - (A + 1) * Math.cos(w0));
    const a2 = (A + 1) - (A - 1) * Math.cos(w0) - 2 * Math.sqrt(A) * alpha;

    return {
        a1: a1 / a0,
        a2: a2 / a0,
        b0: b0 / a0,
        b1: b1 / a0,
        b2: b2 / a0,
    };
}

export function highShelf(
    dBgain: number,
    slope: number,
    cutFrequency: number,
    sampleRate: number,
): BiquadFilter {

    const A = Math.sqrt(10 ** (dBgain / 20));
    const w0 = 2 * Math.PI * cutFrequency / sampleRate;
    const alpha = Math.sin(w0) / 2 * Math.sqrt((A + 1 / A) * (1 / slope - 1) + 2);

    const b0 = A * ((A + 1) - (A - 1) * Math.cos(w0) + 2 * Math.sqrt(A) * alpha);
    const b1 = 2 * A * ((A - 1) - (A + 1) * Math.cos(w0));
    const b2 = A * ((A + 1) - (A - 1) * Math.cos(w0) - 2 * Math.sqrt(A) * alpha);
    const a0 = (A + 1) + (A - 1) * Math.cos(w0) + 2 * Math.sqrt(A) * alpha;
    const a1 = -2 * ((A - 1) + (A + 1) * Math.cos(w0));
    const a2 = (A + 1) + (A - 1) * Math.cos(w0) - 2 * Math.sqrt(A) * alpha;

    return {
        a1: a1 / a0,
        a2: a2 / a0,
        b0: b0 / a0,
        b1: b1 / a0,
        b2: b2 / a0,
    };
}

export function allPass(
    dBgain: number,
    slope: number,
    cutFrequency: number,
    sampleRate: number,
): [BiquadFilter, BiquadFilter] {

    const a = slope;
    const b = 1 / a;

    return [
        lowShelf(dBgain, 1.2, cutFrequency * a, sampleRate),
        highShelf(dBgain, 1.2, cutFrequency * b, sampleRate),
    ];

}

export function getFilterCoefs(
    filterType: FilterType,
    filterFrequency: number,
    filterQuality: number,
    filterParam: number,
    sampleRate: number,
): BiquadFilter[] {
    switch (filterType) {
        case "lowpass":
            return arrayOfNItems(
                filterQuality,
                () => butterworthLowPass(
                    filterFrequency,
                    sampleRate,
                ));
        case "highpass":
            return arrayOfNItems(
                filterQuality,
                () => butterworthHighPass(
                    filterFrequency,
                    sampleRate,
                ));
        case "bandpass":
            return arrayOfNItems(
                filterQuality,
                () => butterworthBandPass(
                    filterFrequency,
                    sampleRate,
                ));
        case "lowshelf":
            return [lowShelf(
                filterQuality,
                filterParam,
                filterFrequency,
                sampleRate,
            )];
        case "highshelf":
            return [highShelf(
                filterQuality,
                filterParam,
                filterFrequency,
                sampleRate,
            )];
        case "bell":
            return allPass(
                filterQuality,
                filterParam,
                filterFrequency,
                sampleRate,
            );
    }
}

export function getFilterQualitySettings(
    type: FilterType,
): {
    min: number,
    max: number,
    mode: "discrete" | "continuous",
} {
    switch (type) {
        case "lowpass":
        case "highpass":
        case "bandpass":
            return {
                min: 1,
                max: 6,
                mode: "discrete",
            };
        case "lowshelf":
        case "highshelf":
            return {
                min: 0,
                max: 30,
                mode: "continuous",
            };
        case "bell":
            return {
                min: -30,
                max: 30,
                mode: "continuous",
            };
    }
}

export function getFilterParamSettings(
    type: FilterType,
): {
    name: string,
    min: number,
    max: number,
    mode: "discrete" | "continuous",
} | undefined {
    switch (type) {
        case "lowpass":
        case "highpass":
        case "bandpass":
            return undefined;
        case "lowshelf":
        case "highshelf":
            return {
                name: "Slope",
                min: 0.001,
                max: 1.5,
                mode: "continuous",
            };
        case "bell":
            return {
                name: "Width",
                min: 0.1,
                max: 0.4,
                mode: "continuous",
            };
    }
}

export function getFrequencyResponse(
    filter: BiquadFilter,
    freq: number,
    sampleRate: number,
): number {
    const w = freq * 2 / sampleRate * Math.PI;

    const { a1, a2, b0, b1, b2 } = filter;

    return Math.sqrt(
        (
            b0 * b0 + b1 * b1 + b2 * b2 +
            2 * (b0 * b1 + b1 * b2) * Math.cos(w) +
            2 * (b0 * b2) * Math.cos(2 * w)
        ) /
        (
            1 + a1 * a1 + a2 * a2 +
            2 * (a1 + a1 * a2) * Math.cos(w) +
            2 * a2 * Math.cos(2 * w)
        ),
    );
}
