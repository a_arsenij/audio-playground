
export const FILTER_TYPES = [
    "lowpass",
    "highpass",
    "lowshelf",
    "highshelf",
    "bandpass",
    "bell",
] as const;

export type FilterType = (typeof FILTER_TYPES)[number];
