import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";

import { amplitudeToDb, dbToAmplitude } from "../src/AmplitudeToDecibells";

describe("Amplitude <---> decibels conversion", () => {
    beforeEach(monkeyPatch);

    it("Amps to decibels", () => {
        assertNumbersClose(
            amplitudeToDb(1),
            0,
            0,
            "a2d(1) = 0",
        );
        assertNumbersClose(
            amplitudeToDb(0.1),
            -20,
            0,
            "a2d(0.1) = -20",
        );
        assertNumbersClose(
            amplitudeToDb(0.01),
            -40,
            0,
            "a2d(0.01) = -40",
        );
        assertNumbersClose(
            amplitudeToDb(0.001),
            -60,
            0,
            "a2d(0.001) = -60",
        );
    });

    it("Decibels to amps", () => {
        assertNumbersClose(
            dbToAmplitude(0),
            1,
            0,
            "a2d(0) = 0",
        );
        assertNumbersClose(
            dbToAmplitude(-20),
            0.1,
            0,
            "a2d(-20) = 0.1",
        );
        assertNumbersClose(
            dbToAmplitude(-40),
            0.01,
            0,
            "a2d(-40) = 0.01",
        );
        assertNumbersClose(
            dbToAmplitude(-60),
            0.001,
            0,
            "a2d(-60) = 0.001",
        );
    });

});
