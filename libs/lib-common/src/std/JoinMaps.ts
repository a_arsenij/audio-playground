
export function joinMaps<K, V>(...maps: Map<K, V>[]): Map<K, V> {
    const map = new Map<K, V>();
    maps.forEach((m) => {
        m.forEach((v: V, k: K) => {
            map.set(k, v);
        });
    });
    return map;
}
