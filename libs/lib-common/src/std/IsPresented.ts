


export function isPresented<T>(elem: T | null | undefined): elem is T {
    return elem !== null && elem !== undefined;
}

export function isTruethy<T>(elem: T | null | undefined | false): elem is T {
    return elem !== null && elem !== undefined && elem !== false;
}
