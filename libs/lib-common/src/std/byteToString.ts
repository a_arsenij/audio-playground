

export function byteToString(
    b: number,
    prefix: boolean = true,
): string {
    const p = prefix ? "0x" : "";
    const res = b.toString(16);
    if (res.length < 2) {
        return `${p}0${res}`;
    }
    return `${p}${res}`;
}
