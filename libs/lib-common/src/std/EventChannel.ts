import { generateId } from "./Id";

export type EventSubscription = {

    unsubscribe: () => void,

}

export type EventChannel<EVENT_TYPE> = {

    subscribe: (
        listener: (event: EVENT_TYPE) => void
    ) => EventSubscription,

}

export type ToggleableEventChannel<EVENT_TYPE> = EventChannel<EVENT_TYPE> & {
    toggle: (value: EVENT_TYPE) => void,
};

export function createToggleableEventChannel<T>(): ToggleableEventChannel<T> {
    const listeners = new Map<string, (a: any) => void>();
    return {
        subscribe(listener: (event: T) => void): EventSubscription {
            const id = generateId();
            listeners.set(id, listener);
            return {
                unsubscribe: () => {
                    listeners.delete(id);
                },
            };
        },
        toggle<T>(value: T): void {
            listeners.forEach((l) => l(value));
        },
    };
}
