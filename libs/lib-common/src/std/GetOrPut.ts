type MapLike<K, V> = {
    has: (key: K) => boolean,
    get: (key: K) => V | undefined,
    set: (key: K, value: V) => void,
}

export function getOrPut<K, V>(
    mapLike: MapLike<K, V>,
    key: K,
    getByKey: () => V,
) {
    const oldValue = mapLike.get(key);
    if (oldValue !== undefined) {
        return oldValue;
    }
    const newValue = getByKey();
    mapLike.set(key, newValue);
    return newValue;
}
