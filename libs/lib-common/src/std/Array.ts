import { TupleOf } from "../types/Array";
import { getOrPut } from "./GetOrPut";

export function groupBy<
    T,
    PROPERTY extends keyof T,
>(
    arr: Array<T>,
    property: PROPERTY,
): Map<T[PROPERTY], Array<T>> {
    const res: Map<T[PROPERTY], Array<T>> = new Map();
    arr.forEach((item) => {
        const val = item[property];
        const arr = res.get(val);
        if (arr === undefined) {
            res.set(val, [item]);
            return;
        }

        arr.push(item);
    });
    return res;
}

export function toMap<K, T>(
    arr: Array<[K, T]>,
): Map<K, T> {
    return new Map(arr);
}

export function collectToSet<K, T>(
    arr: Array<[K, T]>,
): Map<K, Set<T>> {
    const map = new Map<K, Set<T>>();
    arr.forEach((el) => getOrPut(map, el[0], () => new Set()).add(el[1]));
    return map;
}

export function collectToArray<K, T>(
    arr: Array<[K, T]>,
): Map<K, T[]> {
    const map = new Map<K, T[]>();
    arr.forEach((el) => getOrPut(map, el[0], () => []).push(el[1]));
    return map;
}

export function toMapById<T extends {id: string}>(
    arr: Array<T>,
): Map<string, T> {
    return toMapBy(arr, "id");
}

export function toMapBy<
    T,
    PROPERTY extends keyof T,
>(
    arr: Array<T>,
    property: PROPERTY,
): Map<T[PROPERTY], T> {
    const res: Map<T[PROPERTY], T> = new Map();
    arr.forEach((item) => {
        res.set(item[property], item);
    });
    return res;
}

export function ids<T extends {id: string}>(
    arr: Array<T>,
): string[] {
    return arr.map((v) => v.id);
}

export function toSet<T>(
    arr: Array<T>,
): Set<T> {
    return new Set(arr);
}

export function arrayOfNItems<T>(
    length: number,
    createItem: (idx: number) => T,
) {
    const res: T[] = new Array(length);
    for (let i = 0; i < length; i++) {
        res[i] = createItem(i);
    }
    return res;
}

export function f64ArrayCopyMultipliedTo(
    from: Float64Array,
    to: Float64Array,
    coef: number,
) {
    if (from.length !== to.length) {
        throw new Error("Target array should have same length as source array");
    }
    for (let i = 0; i < from.length; i++) {
        to[i] = from[i]! * coef;
    }
}

export function f64ArrayCopyTo(
    from: Float64Array,
    to: Float64Array,
) {
    if (from.length !== to.length) {
        throw new Error("Target array should have same length as source array");
    }
    for (let i = 0; i < from.length; i++) {
        to[i] = from[i]!;
    }
}

export function arrayCopyTo<T>(
    from: T[],
    to: T[],
) {
    from.forEach((value, i) => {
        to[i] = value;
    });
}

export function arraysAreEqual<
    T extends ArrayLike<K>,
    K = T extends ArrayLike<infer U> ? U : never
>(
    a: T,
    b: T,
    elementsAreEqual: (a: K, b: K) => boolean = (a, b) => a === b,
): boolean {
    if (a.length !== b.length) {
        return false;
    }

    for (let i = 0; i < a.length; i++) {
        if (!elementsAreEqual(a[i]!, b[i]!)) {
            return false;
        }
    }

    return true;
}

export function uniqBy<T>(arr: T[], compare: (a: T, b: T) => number): T[] {
    if (arr.length === 0) {
        return arr;
    }
    const sorted = arr.toSorted(compare);
    const u = [sorted[0]!];
    for (let i = 1; i < sorted.length; i++) {
        const prev = sorted[i - 1]!;
        const curr = sorted[i]!;

        if (compare(curr, prev) !== 0) {
            u.push(curr);
        }
    }
    return u;
}

export function distinct<T>(arr: T[]): T[] {
    const set = new Set(arr);
    return [...set];
}

export function toArray<T>(
    iterable: Iterable<T>,
) {
    return [...iterable];
}

export function hasLength<
    ITEM,
    LENGTH extends number
>(
    array: ITEM[],
    length: LENGTH,
): array is TupleOf<LENGTH, ITEM> {
    return array.length === length;
}

export function toFixedLength<
    ITEM,
    LENGTH extends number
>(
    array: ITEM[],
    length: LENGTH,
): number extends LENGTH ? never : TupleOf<LENGTH, ITEM> {
    if (array.length !== length) {
        throw new Error(`Array has length=${array.length}, but length=${length} was expected`);
    }
    return array as (number extends LENGTH ? never : TupleOf<LENGTH, ITEM>);
}

export function shuffle<T>(arr: T[]) {
    for (let i = 0; i < arr.length; i++) {
        const other = Math.floor(Math.random() * arr.length);
        if (other !== i) {
            const tmp = arr[i]!;
            arr[i] = arr[other]!;
            arr[other] = tmp!;
        }
    }
}


export function toShuffled<T>(arr: T[]): T[] {
    const copy = [...arr];
    shuffle(copy);
    return copy;
}

export function remove<T>(arr: T[], el: T): boolean {
    const idx = arr.indexOf(el);
    if (idx === -1) {
        return false;
    }
    arr.splice(idx, 1);
    return true;
}
