export function areSetsEqual<T>(
    a: Set<T>,
    b: Set<T>,
): boolean {
    if (a.size !== b.size) {
        return false;
    }
    return [...a].every((aa) => b.has(aa));
}

function setToSetOfJsons<T>(
    a: Set<T>,
) {
    return a.values().toArray().map((v) => JSON.stringify(v)).toSet();
}

export function areSetsEqualByJsonValues<T>(
    a: Set<T>,
    b: Set<T>,
): boolean {
    if (a.size !== b.size) {
        return false;
    }
    return areSetsEqual(
        setToSetOfJsons(a),
        setToSetOfJsons(b),
    );
}

export function areArraysUnorderlyEqualByJsonValues<T>(
    a: T[],
    b: T[],
) {
    return areSetsEqualByJsonValues(
        a.toSet(),
        b.toSet(),
    );
}
