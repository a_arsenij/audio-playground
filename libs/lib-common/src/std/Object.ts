export function deepCloneJson<T>(obj: T): T {
    return JSON.parse(JSON.stringify(obj)) as T;
}

export function mapObject<
    T extends object,
    R
>(
    obj: T,
    map: (v: T[keyof T], k: keyof T) => R,
): Record<keyof T, R> {
    return Object.fromEntries(
        Object
            .entries(obj)
            .map((e) => [
                e[0],
                map(e[1], e[0] as keyof T),
            ]),
    ) as Record<keyof T, R>;
}
