


const SMA_PRIME = 141079;
const BIG_PRIME = 1000000007;
const GIA_PRIME = 2932031007403;
const SIGNIFICANT_BITS = 16;

/*
 * Use for generating reproducible tests data only.
 * Do not use for anything where you really need good random distribution.
 */
export class PRNG {

    private seed: number;
    private counter: number = 0;

    constructor(seed: number = 99990001) {
        this.seed = Math.round(seed * BIG_PRIME + seed * seed * SMA_PRIME);
    }

    private spin() {
        this.seed += BIG_PRIME;
        this.seed *= SMA_PRIME;
        this.seed %= GIA_PRIME;
        this.seed = Math.round(this.seed); // just in case of overflow
    }

    private readNextBit(): boolean {
        this.counter++;
        this.counter %= SIGNIFICANT_BITS;
        return (this.seed >> SIGNIFICANT_BITS) % 2 === 0;
    }

    nextBit(): boolean {
        this.spin();
        return this.readNextBit();
    }

    nextNumber(precision: number = 8): number {
        this.spin();
        let sum = 0;
        let digitAmp = 0.5;
        for (let i = 0; i < precision; i++) {
            sum += this.nextBit() ? digitAmp : 0;
            digitAmp /= 2;
        }
        if (sum + digitAmp < 1) {
            sum += this.nextBit() ? digitAmp : 0;
        }
        return sum;
    }

}
