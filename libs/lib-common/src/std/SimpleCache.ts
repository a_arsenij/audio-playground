import { getOrPut } from "./GetOrPut";

export type SimpleCache<K, V> = {

    get: (k: K) => V,

}

export function buildSimpleCache<K extends Object, V>(
    build: (k: K) => V,
) {
    const map = new WeakMap<K, V>();
    return {
        get: (k: K) => getOrPut(
            map,
            k,
            () => build(k),
        ),
    };
}
