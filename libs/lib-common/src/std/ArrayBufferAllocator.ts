import { createAllocator, createAllocatorFromStateString } from "../std/Allocator";
import { copyBuffer } from "../std/Buffers";
import { BlobRef, blobRefToF64, blobRefToString, createBlobRef } from "./BlobRef";

export type ArrayBufferAllocator = {

    readonly put: (
        data: ArrayBufferLike,
        meta: string,
    ) => BlobRef,
    readonly clean: (
        refs: BlobRef[]
    ) => void,
    readonly readF64: (
        ref: BlobRef
    ) => Float64Array,
    readonly onReallocateBuffer: (on: (buffer: SharedArrayBuffer) => void) => void,

    readonly getBuffer: () => ArrayBufferLike,
    readonly setBuffer: (buffer: ArrayBuffer) => void,

    readonly getState: () => string,
    readonly setState: (text: string) => void,


}

export function createArrayBufferAllocator(): ArrayBufferAllocator {

    let buffer = new SharedArrayBuffer(1024);

    function setBufferInstanceImpl(newBuffer: SharedArrayBuffer) {
        buffer = newBuffer;
        onReallocate.forEach((c) => c(newBuffer));
    }

    function recreateBuffer(size: number) {
        const newBuffer = new SharedArrayBuffer(size);
        copyBuffer(buffer, 0, buffer.byteLength, newBuffer, 0);
        setBufferInstanceImpl(newBuffer);
    }

    function setBuffer(data: ArrayBuffer) {
        const newBuffer = new SharedArrayBuffer(data.byteLength);
        copyBuffer(data, 0, data.byteLength, newBuffer, 0);
        setBufferInstanceImpl(newBuffer);
    }

    function onGrow(size: number) {
        // @ts-ignore
        if (buffer.growable) {
            // @ts-ignore
            buffer.grow(size);
        } else {
            recreateBuffer(size);
        }
    }

    let allocator = createAllocator(
        buffer.byteLength,
        onGrow,
    );

    function getState(): string {
        return allocator.saveState();
    }

    function setState(text: string) {
        allocator = createAllocatorFromStateString(text, onGrow);
    }

    const onReallocate: ((buffer: SharedArrayBuffer) => void)[] = [];

    function onReallocateBuffer(on: (buffer: SharedArrayBuffer) => void) {
        onReallocate.push(on);
        on(buffer);
    }

    function put(
        data: ArrayBufferLike,
        meta: string,
    ): BlobRef {
        // TODO: locate same data at same location
        const region = allocator.allocate(data.byteLength, meta);
        copyBuffer(
            data,
            0,
            data.byteLength,
            buffer,
            region.offset,
        );
        console.log(`Reserving region ${region.offset};${region.size} for ${meta}`);
        return createBlobRef(region.offset, region.size);
    }
    function clean(
        refs: BlobRef[],
    ) {
        const refsSet = new Set(refs.map(blobRefToString));
        allocator.dump().forEach((region) => {
            const ref = blobRefToString(createBlobRef(region.offset, region.size));
            if (!refsSet.has(ref)) {
                console.log(`Freeing region ${region.offset};${region.size} for ${region.meta}`);
                allocator.free(region);
            }
        });
    }
    function readF64(
        ref: BlobRef,
    ): Float64Array {
        return blobRefToF64(ref, buffer);
    }
    function getBuffer() {
        return buffer;
    }

    return {
        getBuffer,
        put,
        clean,
        readF64,
        onReallocateBuffer,
        setBuffer,
        getState,
        setState,
    };

}
