import { TupleOf } from "../types/Array";

export function createTupleOf<
    N extends number,
    ITEM,
>(
    size: N,
    item: () => ITEM,
): TupleOf<N, ITEM> {
    return [...Array(size)].map(() => item()) as TupleOf<N, ITEM>;
}
