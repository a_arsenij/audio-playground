

type AppendSubpath<
    PATH extends readonly (string | number)[],
    ACCUMULATED_PATH_STRING extends string = "",
    COUNTER extends void[] = [],
> = `${ACCUMULATED_PATH_STRING}.${PATH[COUNTER["length"]]}`

type Error<MSG extends string> = {
    __PATH_ERROR: MSG,
}

export type TypeAt<
    OBJ,
    PATH extends readonly (string | number)[],
    ACCUMULATED_PATH_STRING extends string = "",
    COUNTER extends void[] = [],
> = PATH["length"] extends COUNTER["length"]
    ? OBJ
    : OBJ extends Array<unknown>
        ? PATH[COUNTER["length"]] extends number
            ? TypeAt<
                OBJ[PATH[COUNTER["length"]]],
                PATH,
                AppendSubpath<PATH, ACCUMULATED_PATH_STRING, COUNTER>,
                [void, ...COUNTER]
            >
            : Error<`Value at path ${ACCUMULATED_PATH_STRING} is array, but next subpath step is ${PATH[COUNTER["length"]]}`>
        : OBJ extends {}
            ? PATH[COUNTER["length"]] extends string
                ? OBJ extends { [k in PATH[COUNTER["length"]]]: unknown }
                    ? TypeAt<
                        OBJ[PATH[COUNTER["length"]]],
                        PATH,
                        AppendSubpath<PATH, ACCUMULATED_PATH_STRING, COUNTER>,
                        [void, ...COUNTER]
                    >
                    : Error<`Value at path ${ACCUMULATED_PATH_STRING} is object, but has no field ${PATH[COUNTER["length"]]}`>
                : Error<`Value at path ${ACCUMULATED_PATH_STRING} is object, but next subpath step is ${PATH[COUNTER["length"]]}`>
            : Error<`Value at path ${ACCUMULATED_PATH_STRING} is neither object nor string`>
    ;

export function withChanged<OBJ, const PATH extends readonly(string | number)[]>(
    obj: OBJ,
    replaceAt: PATH,
    replaceWith: TypeAt<OBJ, PATH>,
): OBJ {
    return withChangedImpl(obj, replaceAt, replaceWith) as OBJ;
}

function withChangedImpl(
    obj: unknown,
    replaceAt: readonly (string | number)[],
    replaceWith: unknown,
): unknown {
    if (isReplaceNeeded(obj, replaceAt, replaceWith)) {
        return withChangedActualReplace(obj, replaceAt, replaceWith);
    } else {
        return obj;
    }
}

function isReplaceNeeded(
    obj: unknown,
    replaceAt: readonly (string | number)[],
    replaceWith: unknown,
    index: number = 0,
): boolean {
    if (index === replaceAt.length) {
        return obj !== replaceWith;
    }
    // @ts-ignore
    return isReplaceNeeded(obj[replaceAt[index]], replaceAt, replaceWith, index + 1);
}

function withChangedActualReplace(
    obj: unknown,
    replaceAt: readonly (string | number)[],
    replaceWith: unknown,
    index: number = 0,
): unknown {
    if (index === replaceAt.length) {
        return replaceWith;
    }
    const sp = replaceAt[index];
    let clone;
    if (Array.isArray(obj)) {
        clone = [...obj];
    } else {
        // @ts-ignore
        clone = { ...obj };
    }
    // @ts-ignore
    clone[sp] = withChangedActualReplace(obj[sp], replaceAt, replaceWith, index + 1);
    return clone;
}
