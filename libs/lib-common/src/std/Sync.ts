import { queue } from "../algo/Queue";
import { CompletablePromise, createCompletablePromise } from "../std/CompletablePromise";


export function sleep(ms: number): Promise<void> {
    return new Promise<void>((resolve) => {
        setTimeout(() => {
            resolve();
        }, ms);
    });
}

export type Lock = {
    lock(): Promise<void>,
    unlock(): void,
}

export function createSemaphore(
    limit: number,
): Lock {

    let locked = 0;
    const q = queue<CompletablePromise<void>>();

    return {
        lock(): Promise<void> {
            if (locked < limit) {
                locked++;
                return Promise.resolve();
            } else {
                const p = createCompletablePromise<void>();
                q.push(p);
                return p;
            }
        },
        unlock() {
            if (q.size() > 0) {
                q.pop()!.resolve();
            } else {
                locked--;
            }
        },
    };

}


export function createLock(): Lock {
    return createSemaphore(1);
}
