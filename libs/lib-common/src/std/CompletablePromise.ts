type Resolve<T> = (value: T) => void;
type Reject = (error: Error) => void;

export const createCompletablePromise = <T>() => {
    let resolveImpl!: Resolve<T>;
    let rejectImpl!: Reject;

    const nativePromise = new Promise((resolve: Resolve<T>, reject: Reject) => {
        resolveImpl = resolve;
        rejectImpl = reject;
    });

    return Object.assign(
        nativePromise,
        {
            resolve: resolveImpl,
            reject: rejectImpl,
        },
    ) as CompletablePromise<T>;
};

export interface CompletablePromise<T> extends Promise<T> {
    resolve: Resolve<T>,
    reject: Reject,
}
