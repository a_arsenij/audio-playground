import { createToggleableEventChannel, EventChannel, EventSubscription } from "./EventChannel";

export type Observable<T> = EventChannel<T> & {
    get: () => T,
    map: <R>(
        mapper: ((t: T) => R),
        mode: MapObservableMode
    ) => Observable<R>,
}

export type StoredObservable<T> = Observable<T> & {
    set: (
        toSet: T | ((prev: T) => T)
    ) => void,
}

type MapObservableMode = "smart" | "hard"

export function createStoredObservable<
    T extends {} | string | number | boolean | undefined | null
>(initial: T): StoredObservable<T> {
    let state = initial;
    const toggleable = createToggleableEventChannel<T>();
    const res: StoredObservable<T> = {
        get(): T {
            return state;
        },
        set(toSet: T | ((prev: T) => T)): void {
            const old = state;
            if (typeof toSet === "function") {
                state = toSet(state);
            } else {
                state = toSet;
            }
            if (old !== state) {
                toggleable.toggle(state);
            }
        },
        subscribe: (listener: (event: T) => void) => toggleable.subscribe(listener),
        map: <R>(
            mapper: (t: T) => R,
            mode: MapObservableMode = "smart",
        ) => createMappedObservable<[Observable<T>], R>(
            [res],
            ([r]) => mapper(r),
            mode,
        ),
    };
    return res;
}

export type OBSERVABLES_TO_PACK<
    OBSERVABLES extends readonly Observable<unknown>[],
    ACC extends unknown[] = [],
> = ACC["length"] extends OBSERVABLES["length"]
    ? ACC
    : OBSERVABLES_TO_PACK<
        OBSERVABLES,
        [
            ...ACC,
            OBSERVABLES[ACC["length"]] extends Observable<infer ITEM>
                ? ITEM
                : never
        ]
    >

export function createMappedObservable<
    const OBSERVABLES extends readonly Observable<unknown>[],
    const RES,
>(
    source: OBSERVABLES,
    mapper: (pack: OBSERVABLES_TO_PACK<OBSERVABLES>) => RES,
    mode: MapObservableMode = "smart",
): Observable<RES> {
    const calc = () => mapper(
        source.map((s) => s.get()) as OBSERVABLES_TO_PACK<OBSERVABLES>,
    );

    if (mode === "hard") {
        const res = createStoredObservable<RES>(calc());
        source.map((obs) => {
            obs.subscribe(() => {
                res.set(calc());
            });
        });
        return res;
    }

    let listeners = 0;
    const unsubs: EventSubscription[] = [];
    const stored = createStoredObservable<RES>(calc());

    const res: Observable<RES> = {
        get: () => {
            if (listeners === 0) {
                return stored.get();
            } else {
                return calc();
            }
        },
        subscribe: (listener) => {
            stored.set(calc());
            const unsub = stored.subscribe(listener);
            if (listeners === 0) {
                source.map((s) => {
                    unsubs.push(s.subscribe(() => {
                        stored.set(calc());
                    }));
                });
            }
            listeners++;
            return {
                unsubscribe: () => {
                    unsub.unsubscribe();
                    listeners--;
                    if (listeners === 0) {
                        unsubs.forEach((s) => s.unsubscribe());
                        unsubs.splice(0, unsubs.length);
                    }
                },
            };
        },
        map: <R>(mapper: (t: RES) => R, mode: MapObservableMode = "smart") =>
            createMappedObservable<[Observable<RES>], R>(
                [res],
                ([r]) => mapper(r),
                mode,
            ),
    };
    return res;
}
