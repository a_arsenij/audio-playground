const ID_LENGTH = 32;

const ALPHABET = "0123456789abcdef";

export function generateId() {
    return [...Array(ID_LENGTH)]
        .map(() => ALPHABET[Math.floor(Math.random() * ALPHABET.length)])
        .join("");
}

export function getIncrementalCounter() {
    let count = 1;
    return () => count++;
}
