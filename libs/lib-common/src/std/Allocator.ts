import { avlAdd, avlDelete, avlNode, avlSearch, AVLTree } from "../algo/AVLTree";


export type MemoryRegion = {
    readonly offset: number,
    readonly size: number,
    readonly meta?: string,
}

export type Allocator = {
    readonly allocate: (
        size: number,
        meta?: string,
    ) => MemoryRegion,
    readonly free: (region: MemoryRegion) => void,
    readonly dump: () => MemoryRegion[],
    readonly saveState: () => string,
}

type UsedMap = {[k in number]: MemoryRegion};

type AllocatorState = {
    currentSize: number,
    freeSortedBySize: AVLTree<MemoryRegion>,
    freeSortedByOffset: AVLTree<MemoryRegion>,
    usedSortedByOffset: UsedMap,
};

function createInitAllocatorState(
    size: number,
): AllocatorState {
    const wholeMemory: MemoryRegion = {
        offset: 0,
        size,
        meta: "Whole memory",
    };
    const freeSortedBySize: AVLTree<MemoryRegion> = avlNode(
        wholeMemory,
        wholeMemory.size,
    );

    const freeSortedByOffset: AVLTree<MemoryRegion> = avlNode(
        wholeMemory,
        wholeMemory.offset,
    );
    const usedSortedByOffset: UsedMap = {};
    return {
        currentSize: size,
        freeSortedBySize,
        freeSortedByOffset,
        usedSortedByOffset,
    };
}


function isNeighbors(a: MemoryRegion, b: MemoryRegion): boolean {
    if (a.offset > b.offset) {
        return isNeighbors(b, a);
    }
    return a.offset + a.size === b.offset;
}

function deleteRegionFromFrees(
    state: AllocatorState,
    region: MemoryRegion,
) {
    state.freeSortedBySize = avlDelete(state.freeSortedBySize, region, region.size)!;
    state.freeSortedByOffset = avlDelete(state.freeSortedByOffset, region, region.offset)!;
}

function addRegionToFrees(
    state: AllocatorState,
    region: MemoryRegion,
) {
    state.freeSortedBySize = avlAdd(state.freeSortedBySize, region, region.size)!;
    state.freeSortedByOffset = avlAdd(state.freeSortedByOffset, region, region.offset)!;
}

function deleteRegionFromUsed(
    state: AllocatorState,
    region: MemoryRegion,
) {
    delete state.usedSortedByOffset[region.offset];
}

function addRegionToUsed(
    state: AllocatorState,
    region: MemoryRegion,
) {
    state.usedSortedByOffset[region.offset] = region;
}

function grow(
    state: AllocatorState,
    required: number,
) {
    const oldSize = state.currentSize;
    state.currentSize = required;
    let offset = oldSize;
    let size = state.currentSize - oldSize;
    const rightNeighbor = avlSearch(
        state.freeSortedByOffset,
        {
            lookingFor: "biggest",
        },
    );
    if (rightNeighbor && isNeighbors(rightNeighbor.value, { offset, size })) {
        deleteRegionFromFrees(state, rightNeighbor.value);
        offset -= rightNeighbor.value.size;
        size += rightNeighbor.value.size;
    }
    addRegionToFrees(state, { offset, size });
}

function allocate(
    state: AllocatorState,
    size: number,
    onGrow: (newSize: number) => void,
    meta?: string,
): MemoryRegion {
    const minSizedMatchingRegion = avlSearch(
        state.freeSortedBySize,
        {
            lookingFor: "smallest",
            filter: {
                operator: "moreOrEqual",
                operand: size,
            },
        },
    );
    if (!minSizedMatchingRegion) {
        const newSize = 2 ** (Math.ceil(Math.log2(state.currentSize + size)));
        grow(
            state,
            newSize,
        );
        onGrow(newSize);
        return allocate(state, size, onGrow, meta);
    }
    deleteRegionFromFrees(state, minSizedMatchingRegion.value);
    if (minSizedMatchingRegion.value.size === size) {
        addRegionToUsed(state, minSizedMatchingRegion.value);
        return minSizedMatchingRegion.value;
    }
    const a: MemoryRegion = {
        offset: minSizedMatchingRegion.value.offset,
        size,
        meta,
    };
    const b: MemoryRegion = {
        offset: minSizedMatchingRegion.value.offset + size,
        size: minSizedMatchingRegion.value.size - size,
    };
    addRegionToFrees(state, b);
    addRegionToUsed(state, a);
    return a;
}

function free(
    state: AllocatorState,
    region: MemoryRegion,
) {
    if (!state.usedSortedByOffset[region.offset]) {
        throw new Error("Attempt to free memory region that is not marked as used");
    }
    deleteRegionFromUsed(state, region);
    let offset = region.offset;
    let size = region.size;
    const leftNeighbor = avlSearch(
        state.freeSortedByOffset,
        {
            lookingFor: "biggest",
            filter: {
                operator: "less",
                operand: region.offset,
            },
        },
    );
    if (leftNeighbor && isNeighbors(region, leftNeighbor.value)) {
        offset -= leftNeighbor.value.size;
        size += leftNeighbor.value.size;
        deleteRegionFromFrees(state, leftNeighbor.value);
    }
    const rightNeighbor = avlSearch(
        state.freeSortedByOffset,
        {
            lookingFor: "smallest",
            filter: {
                operator: "more",
                operand: region.offset,
            },
        },
    );
    if (rightNeighbor && isNeighbors(region, rightNeighbor.value)) {
        size += rightNeighbor.value.size;
        deleteRegionFromFrees(state, rightNeighbor.value);
    }
    const joinedRegion: MemoryRegion = {
        offset,
        size,
    };
    addRegionToFrees(state, joinedRegion);
}

function dump(state: AllocatorState): MemoryRegion[] {
    return Object.values(state.usedSortedByOffset);
}

function createAllocatorFromState(
    state: AllocatorState,
    onGrow: (newSize: number) => void,
): Allocator {
    return {
        allocate: (size, meta) => allocate(
            state,
            size,
            onGrow,
            meta,
        ),
        free: (region) => free(state, region),
        dump: () => dump(state),
        saveState: () => JSON.stringify(state),
    };
}

export function createAllocatorFromStateString(
    stateString: string,
    onGrow: (size: number) => void,
): Allocator {
    const state = JSON.parse(stateString) as AllocatorState;
    return createAllocatorFromState(state, onGrow);
}


export function createAllocator(
    size: number,
    onGrow: (size: number) => void,
): Allocator {
    const state = createInitAllocatorState(size);
    return createAllocatorFromState(state, onGrow);
}
