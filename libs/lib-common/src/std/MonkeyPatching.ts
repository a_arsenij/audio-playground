import { TupleOf } from "../types/Array";
import {
    collectToArray,
    collectToSet,
    distinct,
    groupBy,
    ids, remove, shuffle, toArray,
    toFixedLength,
    toMap,
    toMapBy,
    toMapById,
    toSet, toShuffled,
} from "./Array";
import { getOrPut } from "./GetOrPut";


declare global {
    interface Array<T> {
        groupBy<PROPERTY extends keyof T>(
            property: PROPERTY,
        ): Map<T[PROPERTY], Array<T>>,
        toMapBy<PROPERTY extends keyof T>(
            property: PROPERTY,
        ): Map<T[PROPERTY], T>,
        toMap(): T extends [infer KEY, infer EL]
            ? Map<KEY, EL>
            : T extends readonly [infer KEY, infer EL]
                ? Map<KEY, EL>
                : never,
        collectToSet(): T extends [infer KEY, infer EL]
            ? Map<KEY, Set<EL>>
            : T extends readonly [infer KEY, infer EL]
                ? Map<KEY, Set<EL>>
                : never,
        collectToArray(): T extends [infer KEY, infer EL]
            ? Map<KEY, EL[]>
            : T extends readonly [infer KEY, infer EL]
                ? Map<KEY, EL[]>
                : never,
        toMapById(): T extends {id: string} ? Map<string, T> : never,
        ids(): string[],
        toSet(): Set<T>,
        distinct(): T[],
        toFixedLength<N extends number>(n: N): number extends N ? never : TupleOf<N, T>,
        shuffle(): void,
        toShuffled(): T[],
        remove(el: T): boolean,
    }
    interface Set<T> {
        toArray(): T[],
    }
    interface Map<K, V> {
        getOrPut(key: K, v: () => V): V,
    }
    interface IterableIterator<T> {
        toArray(): T[],
    }
}

function extendArray(
    name: string,
    func: unknown,
) {
    // @ts-ignore
    Array.prototype[name] = function(...args) {
        // @ts-ignore
        return func(this, ...args);
    };
}

function extendMap(
    name: string,
    func: unknown,
) {
    // @ts-ignore
    Map.prototype[name] = function(...args) {
        // @ts-ignore
        return func(this, ...args);
    };
}

function extendSet(
    name: string,
    func: unknown,
) {
    // @ts-ignore
    Set.prototype[name] = function(...args) {
        // @ts-ignore
        return func(this, ...args);
    };
}

function extendIterator(
    name: string,
    func: unknown,
) {
    // @ts-ignore
    Object.getPrototypeOf(Object.getPrototypeOf([][Symbol.iterator]()))[name] = function(...args) {
        // @ts-ignore
        return func(this, ...args);
    };
}

export function monkeyPatch() {
    extendArray("groupBy", groupBy);
    extendArray("toMapBy", toMapBy);
    extendArray("toMap", toMap);
    extendArray("toMapById", toMapById);
    extendArray("collectToSet", collectToSet);
    extendArray("collectToArray", collectToArray);
    extendArray("ids", ids);
    extendArray("toSet", toSet);
    extendArray("distinct", distinct);
    extendArray("toFixedLength", toFixedLength);
    extendArray("shuffle", shuffle);
    extendArray("toShuffled", toShuffled);
    extendArray("remove", remove);

    extendMap("getOrPut", getOrPut);

    extendSet("toArray", toArray);

    extendIterator("toArray", toArray);
}
monkeyPatch();
