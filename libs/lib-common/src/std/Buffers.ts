import { TupleOf } from "../types/Array";

export function loadMultipleBlocks<T extends number | undefined>(
    buffer: ArrayBuffer,
    knownBlocksNumber?: T,
): T extends number
    ? TupleOf<T, ArrayBuffer>
    : ArrayBuffer[] {
    const d = new DataView(buffer);

    let offset = 0;
    let cnt: number;
    if (knownBlocksNumber !== undefined) {
        cnt = knownBlocksNumber;
    } else {
        cnt = Number(d.getUint32(
            0,
            true,
        ));
        offset += 4;
    }
    const sizes = [...Array(cnt)]
        .map((_, idx) => Number(d.getUint32(idx * 4 + offset, true)));
    offset += sizes.length * 4;
    return sizes
        .map((v) => {
            const oldOffset = offset;
            offset += v;
            return copyBuffer(
                buffer,
                oldOffset,
                v,
            );
        }) as (T extends number
        ? TupleOf<T, ArrayBuffer>
        : ArrayBuffer[]);
}

export function saveMultipleBlocks(
    blocks: (ArrayBuffer | (() => ArrayBuffer))[],
    knownBlocksNumber?: number,
): ArrayBuffer {
    const blocksLengths: number[] = [];
    if (knownBlocksNumber === undefined) {
        blocksLengths.push(blocks.length);
    } else {
        if (blocks.length !== knownBlocksNumber) {
            throw new Error("??");
        }
    }
    const blocksBuffers: ArrayBuffer[] = [];
    for (let i = 0; i < blocks.length; i++) {
        const bi = blocks[i]!;
        const buffer = (typeof bi === "function") ? bi() : bi;
        blocksBuffers.push(buffer);
        blocksLengths.push(buffer.byteLength);
    }
    return concatBuffers([
        ui32toBuffer(blocksLengths),
        ...blocksBuffers,
    ]);
}

export function concatBuffers(
    blocks: ArrayBuffer[],
): ArrayBuffer {
    const lengths = blocks
        .map((b) => b.byteLength)
        .reduce((a, b) => a + b);
    const res = new ArrayBuffer(lengths);
    let offset = 0;
    for (const b of blocks) {
        copyBuffer(b, 0, b.byteLength, res, offset);
        offset += b.byteLength;
    }
    return res;
}

export function copyBuffer(
    from: ArrayBuffer,
    fromOffset: number,
    fromLength: number,
): ArrayBuffer;
export function copyBuffer(
    from: ArrayBuffer,
    fromOffset: number,
    fromLength: number,
    to: ArrayBuffer,
    toOffset: number,
): ArrayBuffer;

export function copyBuffer(
    from: ArrayBuffer,
    fromOffset: number,
    fromLength: number,
    to?: ArrayBuffer,
    toOffset?: number,
): ArrayBuffer {
    return copyBufferImpl(from, fromOffset, fromLength, to ?? new ArrayBuffer(fromLength), toOffset ?? 0);
}
function copyBufferImpl(
    from: ArrayBuffer,
    fromOffset: number,
    fromLength: number,
    to: ArrayBuffer,
    toOffset: number,
): ArrayBuffer {
    const fromByteLength = from.byteLength;
    const toByteLength = to.byteLength;
    if (
        fromOffset < 0 ||
        fromOffset >= fromByteLength ||
        fromLength > fromByteLength ||
        toOffset < 0 ||
        toOffset + fromLength > toByteLength
    ) {
        throw new Error([
            "Out of bounds, ",
            "fromByteLength: ", fromByteLength,
            "fromOffset: ", fromOffset,
            "fromOffset: ", fromOffset,
            "toByteLength: ", toByteLength,
            "toOffset: ", toOffset,
        ].join(" "));
    }
    const srcView = new DataView(from);
    const dstView = new DataView(to);
    for (let i = 0; i < fromLength; i++) {
        dstView.setUint8(
            i + toOffset,
            srcView.getUint8(i + fromOffset)!,
        );
    }
    return to;
}

export function xxToBuffer(
    set: (v: DataView) => (offset: number, item: number, little: boolean) => void,
    size: number,
) {
    return (
        numbers: number[],
    ) => {
        const res = new ArrayBuffer(size * numbers.length);
        const v = new DataView(res);
        const setter = set(v).bind(v);
        for (let i = 0; i < numbers.length; i++) {
            setter(i * size, numbers[i]!, true);
        }
        return res;
    };
}

export const f64toBuffer = xxToBuffer((v) => v.setFloat64, 8);
export const f32toBuffer = xxToBuffer((v) => v.setFloat32, 4);
export const i32toBuffer = xxToBuffer((v) => v.setInt32, 4);
export const ui32toBuffer = xxToBuffer((v) => v.setUint32, 4);
export const i16toBuffer = xxToBuffer((v) => v.setInt16, 2);
export const ui16toBuffer = xxToBuffer((v) => v.setUint16, 2);
export const i8toBuffer = xxToBuffer((v) => v.setInt8, 1);
export const ui8toBuffer = xxToBuffer((v) => v.setUint8, 1);

export function bufferToXX(
    get: (v: DataView) => (offset: number, little: boolean) => number,
    size: number,
) {
    return (
        buffer: ArrayBuffer,
    ) => {
        const res: number[] = [];
        const v = new DataView(buffer);
        const gg = get(v).bind(v);
        const length = buffer.byteLength / size;
        for (let i = 0; i < length; i++) {
            res.push(gg(i * size, true));
        }
        return res;
    };
}

export const bufferToUI8 = bufferToXX((v) => v.getUint8, 1);
export const bufferToI8 = bufferToXX((v) => v.getInt8, 1);
export const bufferToUI16 = bufferToXX((v) => v.getUint16, 2);
export const bufferToI16 = bufferToXX((v) => v.getInt16, 2);
export const bufferToUI32 = bufferToXX((v) => v.getUint32, 4);
export const bufferToI32 = bufferToXX((v) => v.getInt32, 4);
export const bufferToF32 = bufferToXX((v) => v.getFloat32, 4);
export const bufferToF64 = bufferToXX((v) => v.getFloat64, 8);
