


export function padEnd(
    v: unknown,
    l: number,
    c: string = " ",
): string {
    let res = `${v}`;
    while (res.length < l) {
        res += c;
    }
    return res;
}

export function padStart(
    v: unknown,
    l: number,
    c: string = " ",
): string {
    let res = `${v}`;
    while (res.length < l) {
        res = c + res;
    }
    return res;
}

export function capitalize(
    str: string,
): string {
    if (str === "") {
        return str;
    }
    return str[0]!.toUpperCase() + str.substring(1);
}
