export function createDebouncer(
    interval: number,
): (
    action: () => void
) => void {

    let lastActionId = 0;

    return (action) => {
        lastActionId++;
        const scopedId = lastActionId;
        setTimeout(() => {
            if (scopedId === lastActionId) {
                requestIdleCallback(() => {
                    action();
                });
            }
        }, interval);
    };

}
