export type BlobRef = {
    __BLOB_REF: 1,
    offset: number,
    size: number,
}

export function blobRefToF64(
    ref: BlobRef,
    buffer: ArrayBufferLike,
): Float64Array {
    return new Float64Array(buffer, ref.offset, ref.size / 8);
}

export function createBlobRef(
    start: number,
    length: number,
): BlobRef {
    return {
        offset: start,
        size: length,
        __BLOB_REF: 1,
    };
}

export function blobRefToString(v: BlobRef): string {
    return `${v.offset};${v.size}`;
}

export function stringToBlobRef(v: string): BlobRef {
    return createBlobRef(
        ...(
            v
                .split(";")
                .map((v) => parseInt(v),
                ) as [number, number]
        ),
    );
}

const KEYS = [
    "__BLOB_REF",
    "offset",
    "size",
];

export function isBlobRef(v: unknown): v is BlobRef {
    if (typeof v !== "object") {
        return false;
    }
    if (!v) {
        return false;
    }
    const keys = Object.keys(v);
    if (keys.length !== KEYS.length) {
        return false;
    }
    keys.sort();
    for (let i = 0; i < KEYS.length; i++) {
        if (keys[i] !== KEYS[i]) {
            return false;
        }
    }

    return true;
}

export function getAllBlobRefs(
    v: unknown,
): BlobRef[] {

    const refs = new Set<string>();
    function go(v: unknown) {
        if (Array.isArray(v)) {
            return v.forEach(go);
        }
        if (typeof v !== "object") {
            return;
        }
        if (!v) {
            return;
        }
        if (isBlobRef(v)) {
            refs.add(blobRefToString(v));
        }
        Object.values(v).forEach(go);
    }
    go(v);
    return refs.values()
        .toArray()
        .map(stringToBlobRef)
    ;

}
