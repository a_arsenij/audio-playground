export type PooledArray = Float64Array & {
    iDontNeedItAnymore: () => void,
}

const pool: {[k in number]: PooledArray[]} = {

};

const registry = new FinalizationRegistry((heldValue) => {
    console.warn(`GCing PooledArray / stack: ${heldValue}`);
});

export function acquireArrayFromPool(
    length: number,
): PooledArray {
    let subpool = pool[length];
    if (!subpool) {
        subpool = [];
        pool[length] = subpool;
    }

    if (subpool.length === 0) {
        console.log(`Creating new pooled array for length=${length}`);
        const res = new Float64Array(length) as PooledArray;
        res.fill(0);
        Object.defineProperty(
            res,
            "iDontNeedItAnymore",
            {
                value: () => {
                    subpool!.push(res);
                    res.fill(0);
                },
                enumerable: false,
            },
        );
        registry.register(res, new Error().stack);
        return res;
    }
    const popped = subpool.pop()!;
    for (let i = 0; i < popped.length; i++) {
        const v = popped[i]!;
        if (v !== 0) {
            throw new Error("??");
        }
    }
    return popped;
}
