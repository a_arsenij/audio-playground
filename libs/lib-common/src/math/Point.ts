export type Point = {
    x: number,
    y: number,
};

export function distance(p1: Point, p2: Point) {
    const diffX = p1.x - p2.x;
    const diffY = p1.y - p2.y;

    return Math.sqrt(diffX * diffX + diffY * diffY);
}

export function addPoint(p1: Point, p2: Point) {
    return {
        x: p1.x + p2.x,
        y: p1.y + p2.y,
    };
}

export function subtractPoint(p1: Point, p2: Point) {
    return {
        x: p1.x - p2.x,
        y: p1.y - p2.y,
    };
}

export function linearCombinePoints(p1: Point, p2: Point, alpha: number, beta: number) {
    return {
        x: alpha * p1.x + beta * p2.x,
        y: alpha * p1.y + beta * p2.y,
    };
}
