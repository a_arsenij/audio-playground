export function logOverCustomBase(
    base: number,
    value: number,
) {
    if (base === 2) {
        return Math.log2(value);
    }
    if (base === 10) {
        return Math.log10(value);
    }
    if (base === Math.E) {
        return Math.log(value);
    }
    return Math.log(value) / Math.log(base);
}
