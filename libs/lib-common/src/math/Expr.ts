import { collectToSet, toMap } from "../std/Array";

export const SINGLE_OPERAND_MATH_OPS = [
    "abs",
    "sin",
    "cos",
    "tan",
    "sqrt",
    "loge",
    "log2",
    "log10",
    "exp",
    "pow2",
    "heaviside",
] as const;


export const TWO_COMMUTATIVE_OPERANDS_MATH_OPS = [
    "plus",
    "multiply",
    "min",
    "max",
    "mod",
    "pow",
] as const;


export const TWO_NOT_COMMUTATIVE_OPERANDS_MATH_OPS = [
    "minus",
    "divide",
    "replaceNaN",
] as const;

export type SingleOperandMathOp = (typeof SINGLE_OPERAND_MATH_OPS)[number];
export type TwoCommutativeOperandsMathOp = (typeof TWO_COMMUTATIVE_OPERANDS_MATH_OPS)[number];
export type TwoNonCommutativeOperandsMathOp = (typeof TWO_NOT_COMMUTATIVE_OPERANDS_MATH_OPS)[number];
export type TwoOperandsMathOp =
    | TwoCommutativeOperandsMathOp
    | TwoNonCommutativeOperandsMathOp
    ;
export const MATH_OPS = [
    ...SINGLE_OPERAND_MATH_OPS,
    ...TWO_COMMUTATIVE_OPERANDS_MATH_OPS,
    ...TWO_NOT_COMMUTATIVE_OPERANDS_MATH_OPS,
];

export type MathOp = (typeof MATH_OPS)[number];


export function isSingleOperandOp(
    op: MathOp,
): op is SingleOperandMathOp {
    // @ts-ignore
    return SINGLE_OPERAND_MATH_OPS.includes(op);
}


export function getMathOpOperandsNumber(
    op: MathOp,
): number {
    // @ts-ignore
    if (SINGLE_OPERAND_MATH_OPS.includes(op)) {
        return 1;
    } else {
        return 2;
    }
}

export type Op1Expr = { type: "op"; op: SingleOperandMathOp; args: [Expr] };
export type Op2Expr = { type: "op"; op: TwoOperandsMathOp; args: [Expr, Expr] };
export type ConstExpr = { type: "const"; value: number };
export type VarExpr = { type: "variable"; name: string };

export type Expr =
    | Op1Expr
    | Op2Expr
    | ConstExpr
    | VarExpr
;

const TOKEN_OPS = [
    ["+", "plus", 3],
    ["-", "minus", 3],
    ["*", "multiply", 2],
    ["/", "divide", 2],
    ["%", "mod", 2],
    ["^", "pow", 1],
] as const;

const PRIORITY_TO_TOKENS = collectToSet(
    TOKEN_OPS
        .map((v) => [v[2], v[0]]),
);

const OP_TO_FUNC = toMap(
    TOKEN_OPS
        .map((e) => [e[0], e[1]]),
);



const TOKEN_OPS_SYMBOLS = TOKEN_OPS.map((t) => t[0]);
const TOKEN_OPS_SYMBOLS_SET = new Set(TOKEN_OPS_SYMBOLS);

const SPACES = [
    " ",
    "\n",
    "\r",
    "\t",
    "\u00A0" /* no-break */,
    "\u200B" /* zero-width */,
];
const SPACES_SET = new Set(SPACES);

const SEPARATORS = [
    ...SPACES,
    ...TOKEN_OPS_SYMBOLS,
    "(",
    ")",
    ",",
];
const SEPARATORS_SET = new Set(SEPARATORS);

const FUNCS_SET = new Set(MATH_OPS);

type BaseToken = {
    first: number,
    last: number,
}

type RawToken = BaseToken & { type: "raw"; value: string }
type BracketsToken = BaseToken & { type: "brackets"; sub: Token[] };
type TupleToken = BaseToken & { type: "tuple"; items: Token[][] };
type ConstToken = BaseToken & { type: "const"; value: number };
type CallToken = BaseToken & { type: "call"; func: MathOp; args: Token[] };
type VariableToken = BaseToken & { type: "variable"; name: string };

type Token =
    | RawToken
    | BracketsToken
    | TupleToken
    | ConstToken
    | CallToken
    | VariableToken
;

class ParseError extends Error {
    readonly type = "error";
    constructor(
        readonly message: string,
        readonly first: number,
        readonly last: number,
    ) {
        super(message);
    }
}

function processSplitToTokens(
    expr: string,
): RawToken[] {
    const lastString: string[] = [];
    const tokens: RawToken[] = [];
    let sumLength = 0;
    function pushToken(s: string) {
        tokens.push({
            type: "raw",
            value: s,
            first: sumLength,
            last: sumLength + s.length - 1,
        });
        sumLength += s.length;
    }
    function pushLastString() {
        if (lastString.length > 0) {
            pushToken(lastString.join(""));
            lastString.splice(0, lastString.length);
        }
    }
    for (const c of expr.split("")) {
        const isSeparator = SEPARATORS_SET.has(c);
        if (isSeparator) {
            pushLastString();
            pushToken(c);
        } else {
            lastString.push(c);
        }
    }
    pushLastString();
    return tokens.filter((v) => !SPACES_SET.has(v.value));
}

type ParseOk = {type: "ok"; expr: Expr };

type ParseResult =
    | ParseOk
    | ParseError
;

const BRACKETS_UNBALANCED_MESSAGE = "Brackets are unbalanced";

function verifyBracketsBalance(
    tokens: RawToken[],
) {
    const stack: RawToken[] = [];
    for (const t of tokens) {
        if (t.value === "(") {
            stack.push(t);
        } else if (t.value === ")") {
            if (stack.length === 0) {
                throw new ParseError(BRACKETS_UNBALANCED_MESSAGE, t.first, t.last);
            }
            stack.pop();
        }
    }
    if (stack.length !== 0) {
        throw new ParseError(BRACKETS_UNBALANCED_MESSAGE, stack[0]!.first, stack[0]!.last);
    }
    return tokens;
}

function processGroupBrackets(
    tokens: Token[],
): Token[] {
    const stack: Token[][] = [[]];
    for (const token of tokens) {
        if (token.type === "raw" && token.value === "(") {
            stack.push([token]);
        } else if (token.type === "raw" && token.value === ")") {
            const last = stack.pop()!;
            stack[stack.length - 1]!.push({
                type: "brackets",
                sub: last.slice(1),
                first: last[0]!.first,
                last: token.last,
            });
        } else {
            stack[stack.length - 1]!.push(token);
        }
    }
    return stack[0]!;
}

function preApplyForChildren(
    tokens: Token[],
    func: (tokens: Token[], parent: Token) => Token[],
): Token[] {
    const newTokens: Token[] = [];
    for (const t of tokens) {
        if (t.type === "brackets") {
            newTokens.push({
                ...t,
                sub: func(t.sub, t),
            });
        } else if (t.type === "call") {
            newTokens.push({
                ...t,
                args: func(t.args, t),
            });
        } else if (t.type === "tuple") {
            newTokens.push({
                ...t,
                items: t.items.map((tt) => func(tt, t)),
            });
        } else {
            newTokens.push(t);
        }
    }
    return newTokens;
}



const numberRegex = /^\d+(\.\d+)?$/;

function processConsts(
    tokens: Token[],
): Token[] {
    const pre = preApplyForChildren(tokens, processConsts);
    const newTokens: Token[] = [];
    for (const t of pre) {
        if (t.type === "raw" && numberRegex.test(t.value)) {
            newTokens.push({
                type: "const",
                value: parseFloat(t.value),
                first: t.first,
                last: t.last,
            });
        } else {
            newTokens.push(t);
        }
    }
    return newTokens;
}

function processConstsUnaryMinuses(
    tokens: Token[],
): Token[] {
    const pre = preApplyForChildren(tokens, processConstsUnaryMinuses);
    const newTokens: Token[] = [];

    let _2: Token | undefined;
    let _1: Token | undefined;

    for (const t of pre) {
        if (t.type === "const" &&
            _1?.type === "raw" &&
            _1.value === "-" &&
            (
                _2?.type === "raw" &&
                // @ts-ignore
                TOKEN_OPS_SYMBOLS_SET.has(_2.value) ||
                _2 === undefined
            )
        ) {
            newTokens.pop();
            newTokens.push({
                type: "const",
                value: -1 * t.value,
                first: _1.first,
                last: t.last,
            });
        } else {
            newTokens.push(t);
        }
        _2 = _1;
        _1 = t;
    }
    return newTokens;
}

function processTuples(
    tokens: Token[],
): Token[] {
    const pre = preApplyForChildren(tokens, processTuples);
    const commaIndices: number[] = [];
    let idx = 0;
    for (const t of pre) {
        if (t.type === "raw" && t.value === ",") {
            commaIndices.push(idx);
        }
        idx++;
    }
    if (commaIndices.length > 0) {

        let prev = 0;
        const subs: Token[][] = [];
        commaIndices.forEach((index) => {
            subs.push(pre.slice(prev, index));
            prev = index + 1;
        });
        subs.push(pre.slice(prev));

        return [{
            type: "tuple",
            first: tokens.at(0)!.first,
            last: tokens.at(-1)!.last,
            items: subs,
        }];
    }
    return pre;
}

function processJsPowAlias(
    tokens: Token[],
): Token[] {
    if (tokens.length <= 1) {
        return tokens;
    }
    const pre = preApplyForChildren(tokens, processJsPowAlias);
    let prev = tokens[0]!;
    const result: Token[] = [prev];
    for (let idx = 1; idx < pre.length; idx++) {
        const t = pre[idx]!;
        // @ts-ignore
        if (
            t.type === "raw" &&
            prev?.type === "raw" &&
            t.value === "*" &&
            prev.value === "*" &&
            prev.first + 1 === t.last
        ) {
            result.pop();
            result.push({
                type: "raw",
                value: "^",
                first: prev.first,
                last: t.last,
            });
        } else {
            result.push(t);
        }
        prev = t;
    }
    return result;
}

function processOps(
    tokens: Token[],
): Token[] {
    const pre = preApplyForChildren(tokens, processOps);
    for (const [, ops] of PRIORITY_TO_TOKENS) {
        for (let idx = pre.length - 1; idx >= 0; idx--) {
            const t = pre[idx]!;
            // @ts-ignore
            if (t.type === "raw" && ops.has(t.value)) {
                // @ts-ignore
                const func = OP_TO_FUNC.get(t.value);
                return [{
                    type: "call",
                    // @ts-ignore
                    func,
                    args: [{
                        type: "tuple",
                        items: [
                            processOps(pre.slice(0, idx)),
                            processOps(pre.slice(idx + 1)),
                        ],
                        first: pre.at(0)!.first,
                        last: pre.at(-1)!.last,
                    }],
                    first: pre.at(0)!.first,
                    last: pre.at(-1)!.last,
                }];
            }
        }
    }
    return pre;
}

function isTokenRawAndFunctionValue(t: Token): t is RawToken & {value: typeof MATH_OPS[number]} {
    if (t.type !== "raw") {
        return false;
    }

    // @ts-ignore
    return FUNCS_SET.has(t.value);
}
function processCalls(
    tokens: Token[],
): Token[] {
    const pre = preApplyForChildren(tokens, processCalls);
    const newTokens: Token[] = [];
    let prev: Token = tokens[0]!;
    for (const t of pre) {
        // @ts-ignore
        if (
            t.type === "brackets" &&
            isTokenRawAndFunctionValue(prev)
        ) {
            newTokens.pop();
            newTokens.push({
                type: "call",
                func: prev.value,
                args: t.sub,
                first: prev.first,
                last: t.last,
            });
        } else {
            newTokens.push(t);
        }
        prev = t;
    }
    return newTokens;
}

const variableNameRegex = /^[A-Za-z_][A-Za-z0-9_]*$/;

function processVariables(
    tokens: Token[],
): Token[] {
    const pre = preApplyForChildren(tokens, processVariables);
    const newTokens: Token[] = [];
    for (const t of pre) {
        if (
            t.type === "raw" && variableNameRegex.test(t.value)
        ) {
            newTokens.push({
                type: "variable",
                name: t.value,
                first: t.first,
                last: t.last,
            });
        } else {
            newTokens.push(t);
        }
    }
    return newTokens;
}

function verifyNoRaws(
    tokens: Token[],
): Token[] {
    const pre = preApplyForChildren(tokens, verifyNoRaws);
    for (const t of pre) {
        if (t.type === "raw") {
            throw new ParseError(`Cannot determine meaning of token ${t.value}`, t.first, t.last);
        }
    }
    return pre;
}


function verifySingleToken(
    tokens: Token[],
    parent: Token,
): Token[] {
    const pre = preApplyForChildren(tokens, verifySingleToken);
    if (pre.length === 0) {
        throw new ParseError("Expected expression, but got nothing", parent.first, parent.last);
    }
    if (pre.length > 1) {
        throw new ParseError("Failed to interprete tokens as single expression", tokens.at(0)!.first, tokens.at(-1)!.last);
    }
    return pre;
}

function processInlineBrackets(
    tokens: Token[],
): Token[] {
    const pre = preApplyForChildren(tokens, processInlineBrackets);
    if (
        pre.length === 1 &&
        pre[0]!.type === "brackets"
    ) {
        return pre[0]!.sub;
    }
    return pre;
}


function prepareTokens(
    expr: string,
): Token {
    const rootToken: RawToken = { type: "raw", first: 0, last: expr.length - 1, value: expr };

    const tokens = processSplitToTokens(expr);
    const bracketsVerified = verifyBracketsBalance(tokens);
    const grouped = processGroupBrackets(bracketsVerified);
    const consts = processConsts(grouped);
    const unaried = processConstsUnaryMinuses(consts);
    const tupled = processTuples(unaried);
    const powed = processJsPowAlias(tupled);
    const oped = processOps(powed);
    const called = processCalls(oped);
    const variabled = processVariables(called);
    const noRaws = verifyNoRaws(variabled);
    const singleToken = verifySingleToken(noRaws, rootToken);
    const inlined = processInlineBrackets(singleToken);

    return inlined[0]!;
}

function buildCallExpr(
    token: CallToken,
): Expr {
    const args =
        token.args[0]!.type === "tuple"
            ? token.args[0]!.items.map((t) => buildExpr(t[0]!))
            : [buildExpr(token.args[0]!)]
    ;
    const requiredArgsNum = getMathOpOperandsNumber(token.func);
    if (args.length !== requiredArgsNum) {
        throw new ParseError(
            `Wrong arguments number for function ${token.func}, expected ${requiredArgsNum}, got ${args.length}`,
            token.first,
            token.last,
        );
    }
    return {
        type: "op",
        op: token.func,
        args,
    } as Expr;
}

function buildExpr(
    token: Token,
): Expr {
    switch (token.type) {
        case "raw": throw new Error("??");
        case "brackets": throw new Error("??");
        case "tuple": throw new ParseError("Unexpected tuple", token.first, token.last);
        case "call": return buildCallExpr(token);
        case "const": return {
            type: "const",
            value: token.value,
        };
        case "variable": return {
            type: "variable",
            name: token.name,
        };
    }
}

export function parseExpr(
    expr: string,
): ParseResult {
    try {
        return {
            type: "ok",
            expr: buildExpr(prepareTokens(expr)),
        };
    } catch (e) {
        if (e instanceof ParseError) {
            return e;
        } else {
            throw e;
        }
    }
}
