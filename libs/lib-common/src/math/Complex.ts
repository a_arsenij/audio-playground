export type Complex = [number, number];

export function createComplex(): Complex {
    return [0, 0];
}

export function multiplyComplex(
    a: Complex,
    b: Complex,
    target: Complex,
) {
    target[0] = (a[0] * b[0] - a[1] * b[1]);
    target[1] = (a[0] * b[1] + a[1] * b[0]);
    return target;
}

export function addComplex(
    a: Complex,
    b: Complex,
    target: Complex,
) {
    target[0] = a[0] + b[0];
    target[1] = a[1] + b[1];
    return target;
}

export function minusComplex(
    a: Complex,
    b: Complex,
    target: Complex,
) {
    target[0] = a[0] - b[0];
    target[1] = a[1] - b[1];
    return target;
}
