import { Interval } from "./Interval";

export type Mixed = Interval | number;

export type MultiInterval = Array<Mixed>;


function equals(a: Mixed, b: Mixed) {
    if (isInterval(a) && isInterval(b)) {
        return Object.is(a[0], b[0]) && Object.is(a[1], b[1]);
    }
    return Object.is(a, b);
}

export function equalsSet(a: MultiInterval, b: MultiInterval) {
    if (a.length !== b.length) {
        return false;
    }
    return a.every((aa, i) => equals(aa, b[i]!));
}

function isNaNLike(a: Mixed) {
    if (isInterval(a)) {
        return isNaN(a[0]) || isNaN(a[1]);
    }
    return isNaN(a);
}

function squash(a: Mixed): Mixed {
    if (isInterval(a) && Object.is(a[0], a[1])) {
        return a[0];
    }
    return a;
}
export function isInterval(a: Mixed): a is Interval {
    return typeof a === "object";
}

function addSingle(a: Mixed, b: Mixed): MultiInterval {
    if (isNaNLike(a) || isNaNLike(b)) {
        return [NaN];
    }

    const a2 = squash(a);
    const b2 = squash(b);
    const aa = isInterval(a2) ? a2 : b2;
    const bb = isInterval(a2) ? b2 : a2;

    if (!isInterval(aa)) {
        return [aa + (bb as number)];
    }
    if (!isInterval(bb)) {
        if (isNaN(aa[0] + bb) || isNaN(aa[1] + bb)) {
            return [NaN, bb];
        }
        return [squash([aa[0] + bb, aa[1] + bb])];
    }


    if (isNaN(aa[0] + bb[1]) || isNaN(aa[1] + bb[0])) {
        return [NaN, [aa[0] + bb[0], aa[1] + bb[1]]];
    }

    return [[aa[0] + bb[0], aa[1] + bb[1]]];
}


export const MIMath = {
    addSingle,
};

