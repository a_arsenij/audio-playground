import { mod } from "./Basic";

export type Interval = readonly [number, number];

export const UNIVERSUM_INTERVAL: Interval = [Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY];
export function inInterval(
    a: number,
    b: Interval,
) {
    return a >= b[0] && a <= b[1];
}

export function intersect(
    a: Interval,
    b: Interval,
): boolean {
    return inInterval(a[0], b) ||
        inInterval(a[1], b) ||
        inInterval(b[0], a);
}

export function buildInterval(
    a: number,
    b: number,
): Interval {
    return [
        Math.min(a, b),
        Math.max(a, b),
    ];
}

export function buildDegenerateInterval(a: number): Interval {
    return [a, a];
}

export function clamp(value: number, min: number, max: number) {
    return Math.max(min, Math.min(max, value));
}

export function isDegenerate(interval: Interval) {
    return Object.is(interval[0], interval[1]);
}

function splitInPoints(int: Interval, points: number[]) {
    const result: Interval[] = [];
    let prevPoint = int[0];

    points.forEach((p) => {
        if (inInterval(p, int)) {
            result.push([prevPoint, p]);
            prevPoint = p;
        }
    });
    result.push([prevPoint, int[1]]);

    return result;

}
export function union(...intervals: Interval[]): Interval {

    return [
        Math.min(...intervals.map((i) => i[0])),
        Math.max(...intervals.map((i) => i[1])),
    ];
}
export const IMath = {
    add,
    sub,
    mult,
    div,
    abs,
    min,
    max,
    sin,
    cos,
    tan,
};


function add(...inter: Interval[]): Interval {
    return [
        inter.reduce((prev, curr) => prev + curr[0], 0),
        inter.reduce((prev, curr) => prev + curr[1], 0),
    ];
}

function sub(a: Interval, b: Interval): Interval {
    return [a[0] - b[1], a[1] - b[0]];
}

function mult(a: Interval, b: Interval): Interval {
    const c = a[0] * b[0];
    const d = a[0] * b[1];
    const e = a[1] * b[0];
    const f = a[1] * b[1];
    return [
        Math.min(c, d, e, f),
        Math.max(c, d, e, f),
    ];
}

function div(a: Interval, b: Interval): Interval {
    if (inInterval(0, b)) {
        return UNIVERSUM_INTERVAL;
    }
    const c = a[0] / b[0];
    const d = a[0] / b[1];
    const e = a[1] / b[0];
    const f = a[1] / b[1];
    return [
        Math.min(c, d, e, f),
        Math.max(c, d, e, f),
    ];
}

function abs(a: Interval): Interval {
    if (a[0] >= 0 && a[1] >= 0) {
        return a;
    }
    if (a[0] <= 0 && a[1] <= 0) {
        return [
            Math.abs(a[1]),
            Math.abs(a[0]),
        ];
    }
    return [
        0,
        Math.max(Math.abs(a[0]), Math.abs(a[1])),
    ];
}

function min([...inter]: Interval[]): Interval {
    return [
        Math.min(...inter.map((i) => i[0])),
        Math.min(...inter.map((i) => i[1])),
    ];
}

function max([...inter]: Interval[]): Interval {
    return [
        Math.max(...inter.map((i) => i[0])),
        Math.max(...inter.map((i) => i[1])),
    ];
}

function calcContinuousF(int: Interval, f: (x: number) => number, extremums: number[]): Interval {
    const monotone = splitInPoints(int, extremums);
    const resultIntervals = monotone.map((int) => calcContinuousMonotoneF(int, f));
    return union(...resultIntervals);
}

export function calcContinuousMonotoneF(int: Interval, f: (x: number) => number): Interval {
    return buildInterval(f(int[0]), f(int[1]));
}
function modInterval(int: Interval, period: number, startFrom = 0): Interval {
    const [a, b] = int;

    const modA = mod(a - startFrom, period);
    const normA = modA + startFrom;
    const normB = b - (a - normA);

    return [normA, normB];
}

const TWO_PI = 2 * Math.PI;
const HALF_PI = 0.5 * Math.PI;

function sin(int: Interval): Interval {
    return cos([int[0] - HALF_PI, int[1] - HALF_PI]);
}
function cos(int: Interval): Interval {
    if (isDegenerate(int)) {
        return buildDegenerateInterval(Math.cos(int[0]));
    }

    if (int[1] - int[0] >= TWO_PI) {
        return [-1, 1];
    }

    const normalisedInterval = modInterval(int, TWO_PI);
    // 0 < A, B < 4pi

    return calcContinuousF(
        normalisedInterval,
        Math.cos,
        [Math.PI, Math.PI * 2, Math.PI * 3],
    );
}

function tan(int: Interval): Interval {
    if (isDegenerate(int)) {
        return buildDegenerateInterval(Math.tan(int[0]));
    }

    if (int[1] - int[0] >= Math.PI) {
        return UNIVERSUM_INTERVAL;
    }

    const normalisedInterval = modInterval(int, Math.PI, -1 * HALF_PI);
    // -0.5pi < A, B < 1.5pi

    if (inInterval(HALF_PI, normalisedInterval)) {
        return UNIVERSUM_INTERVAL;
    }

    return calcContinuousMonotoneF(normalisedInterval, Math.tan);

}

