

export function factorial(
    a: number,
): number {
    let res = 1;
    for (let i = 2; i <= a; i++) {
        res *= i;
    }
    return res;
}
