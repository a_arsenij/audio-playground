import { clamp } from "./Interval";
import { logOverCustomBase } from "./Log";

export function identitySegmentToLinear(
    value: number,
    minValue: number,
    maxValue: number,
    step?: number | undefined,
): number {
    return clamp(
        minValue,
        stepify(
            value * (maxValue - minValue) + minValue,
            step,
        ),
        maxValue,
    );
}

export function identitySegmentToExponential(
    value: number,
    minValue: number,
    maxValue: number,
    base: number,
): number {
    return (base ** value - 1) / (base - 1) * (maxValue - minValue) + minValue;
}

export function stepify(
    value: number,
    step: number | undefined,
) {
    if (step === 0 || step === undefined) {
        return value;
    }
    return Math.round(value / step) * step;
}

export function linearToIdentitySegment(
    value: number,
    minValue: number,
    maxValue: number,
): number {
    return (value - minValue) / (maxValue - minValue);
}

export function exponentialToIdentitySegment(
    value: number,
    minValue: number,
    maxValue: number,
    base: number,
): number {
    return logOverCustomBase(
        base,
        (value - minValue) / (maxValue - minValue) * (base - 1) + 1,
    );
}
