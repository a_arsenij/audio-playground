

export type UnionOmit<T, K extends string | number | symbol> = T extends unknown
    ? Omit<T, K>
    : never;

export type NotAFunction =
    | number
    | string
    | boolean
    | object
    | undefined
    | null
;

export type Equals<X, Y> =
    (<T>() => T extends X ? 1 : 2) extends
        (<T>() => T extends Y ? 1 : 2) ? true : false;

export type Expect<T extends true> = T;
