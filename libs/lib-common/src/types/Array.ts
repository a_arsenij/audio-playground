export type ReadonlyTupleOf<
    N extends number,
    ELEM,
    ACC extends readonly ELEM[] = readonly []
> = [ACC["length"]] extends [N]
    ? ACC
    : ReadonlyTupleOf<N, ELEM, readonly [ELEM, ...ACC]>;

export type TupleOf<
    N extends number,
    ELEM,
    ACC extends ELEM[] = []
> = [ACC["length"]] extends [N]
    ? ACC
    : TupleOf<N, ELEM, [ELEM, ...ACC]>;
