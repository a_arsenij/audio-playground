import { isPresented } from "../std/IsPresented";

type Side = "left" | "right";

export type AVLTreeNode<T> =
    & {
        height: number,
        value: T,
        ownKey: number,
        minKey: number,
        maxKey: number,
    }
    & {
        [k in Side]: (AVLTreeNode<T> | undefined)
    }
;

export type AVLTree<T> = AVLTreeNode<T> | undefined;

function avlRecalcHeight<T>(
    a: AVLTreeNode<T>,
) {
    a.minKey = a.left?.minKey ?? a.ownKey;
    a.maxKey = a.right?.maxKey ?? a.ownKey;
    a.height = Math.max(
        a.left ? (a.left.height + 1) : 0,
        a.right ? (a.right.height + 1) : 0,
    );
}

function avlSmallRebalance<T>(
    a: AVLTreeNode<T>,
    side: boolean,
): AVLTreeNode<T> {
    const left = side ? "left" : "right";
    const right = side ? "right" : "left";
    const l = a[left];
    const b = a[right];
    const c = b?.[left];
    if (
        b &&
        (l?.height ?? 0) < (b.height) &&
        (c?.height ?? 0) <= (b?.[right]?.height ?? 0)
    ) {
        a[right] = c;
        b[left] = a;
        avlRecalcHeight(a);
        avlRecalcHeight(b);
        return b;
    }
    return a;
}
function avlBigRebalance<T>(
    a: AVLTreeNode<T>,
    side: boolean,
): AVLTreeNode<T> {
    const left = side ? "left" : "right";
    const right = side ? "right" : "left";
    const l = a[left];
    const b = a[right];
    const c = b?.[left];
    const m = c?.[left];
    const n = c?.[right];
    if (
        b &&
        c &&
        (l?.height ?? 0) < (b.height) &&
        c.height > (b?.[right]?.height ?? 0)
    ) {
        c[left] = a;
        c[right] = b;
        a[right] = m;
        b[left] = n;
        avlRecalcHeight(a);
        avlRecalcHeight(b);
        avlRecalcHeight(c);
        return c;
    }
    return a;
}

function avlRebalance<T>(
    a: AVLTreeNode<T>,
): AVLTreeNode<T> {

    const diff = Math.abs(
        (a.left?.height ?? 0) -
        (a.right?.height ?? 0),
    );
    if (diff < 2) {
        avlRecalcHeight(a);
        return a;
    }

    let aa = a;
    if ((aa = avlSmallRebalance(aa, true)) !== a) {
        return aa;
    }
    if ((aa = avlSmallRebalance(aa, false)) !== a) {
        return aa;
    }
    if ((aa = avlBigRebalance(aa, true)) !== a) {
        return aa;
    }
    if ((aa = avlBigRebalance(aa, false)) !== a) {
        return aa;
    }
    avlRecalcHeight(a);
    return a;
}

export function avlNode<T>(
    value: T,
    key: number,
): AVLTreeNode<T> {
    return {
        height: 1,
        ownKey: key,
        minKey: key,
        maxKey: key,
        value,
        left: undefined,
        right: undefined,
    };
}

export function _avlAdd<T>(
    root: AVLTreeNode<T>,
    valueToAdd: T,
    key: number,
): AVLTreeNode<T> {
    const side = root.ownKey > key ? "left" : "right";
    const sided = root[side];
    if (!sided) {
        root[side] = avlNode(valueToAdd, key);
        root.height = Math.max(2, root.height);
        return root;
    }
    root[side] = _avlAdd(sided, valueToAdd, key);
    return avlRebalance(root);
}

export function avlAdd<T>(
    root: AVLTree<T>,
    valueToAdd: T,
    key: number,
): AVLTreeNode<T> {
    if (root === undefined) {
        return avlNode(valueToAdd, key);
    }
    return _avlAdd(root, valueToAdd, key);
}

export function _avlDelete<T>(
    root: AVLTreeNode<T>,
    valueToDelete: T,
    key: number,
): AVLTree<T> {
    if (root.value === valueToDelete) {
        if (!root.left && !root.right) {
            return undefined;
        }
        if (root.left && !root.right) {
            return root.left;
        }
        if (root.right && !root.left) {
            return root.right;
        }
        if (root.left && root.right) {
            const closest = [
                avlSearch(root.left, { lookingFor: "biggest" }),
                avlSearch(root.right, { lookingFor: "smallest" }),
            ].filter(isPresented).sort((v) => Math.abs(v.ownKey - key))[0]!;
            if (closest.ownKey < key) {
                root.left = _avlDelete(root.left, closest.value, closest.ownKey);
            } else {
                root.right = _avlDelete(root.right, closest.value, closest.ownKey);
            }
            root.value = closest.value;
            root.ownKey = closest.ownKey;
            return avlRebalance(root);
        }
    }
    const side = root.ownKey > key ? "left" : "right";
    const sided = root[side];
    if (!sided) {
        return root;
    }
    root[side] = _avlDelete(sided, valueToDelete, key);
    return avlRebalance(root);
}

export function avlDelete<T>(
    root: AVLTree<T>,
    valueToDelete: T,
    key: number,
): AVLTree<T> {
    if (root === undefined) {
        return;
    }
    return _avlDelete(root, valueToDelete, key);
}

export type AvlSearchQuery =
    & {
        lookingFor: "smallest" | "biggest",
    }
    & (
        | {filter: { operand: number; operator: "less" | "lessOrEqual" | "more" | "moreOrEqual" } }
        | {filter?: never}
    )

function avlMatches(
    key: number,
    query: AvlSearchQuery,
): boolean {
    if (
        query.filter?.operator === "less" &&
        query.filter?.operand <= key
    ) {
        return false;
    }
    if (
        query.filter?.operator === "lessOrEqual" &&
        query.filter?.operand < key
    ) {
        return false;
    }
    if (
        query.filter?.operator === "more" &&
        query.filter?.operand >= key
    ) {
        return false;
    }
    if (
        query.filter?.operator === "moreOrEqual" &&
        query.filter?.operand > key
    ) {
        return false;
    }
    return true;
}

export function _avlSearch<T>(
    root: AVLTreeNode<T>,
    query: AvlSearchQuery,
): AVLTreeNode<T> | undefined {
    const children: (AVLTreeNode<T> | undefined)[] = [];
    if (query.lookingFor === "smallest") {
        children.push(root.left, root.right);
    } else {
        children.push(root.right, root.left);
    }
    const foundChildren = children.filter(isPresented);
    for (const c of foundChildren) {
        if (avlMatches(c.minKey, query) || avlMatches(c.maxKey, query)) {
            const res = avlSearch(c, query);
            if (res !== undefined) {
                return res;
            }
        }
    }
    if (avlMatches(root.ownKey, query)) {
        return root;
    }
}


export function avlSearch<T>(
    root: AVLTree<T>,
    query: AvlSearchQuery,
): AVLTreeNode<T> | undefined {
    if (!root) {
        return;
    }
    return _avlSearch(root, query);
}
