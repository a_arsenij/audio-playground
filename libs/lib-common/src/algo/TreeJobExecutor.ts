import { createSemaphore } from "../std/Sync";

export type Node<ID, T> = {
    readonly id: ID,
    readonly value: T,
}

export type Connection<ID> = {
    readonly parentId: ID,
    readonly childId: ID,
}

type PreparedGraph<ID, T> = {
    readonly nodeIdToNode: Map<ID, Node<ID, T>>,
    readonly nodeIdToParentId: Map<ID, ID>,
    readonly nodeIdToChildrenId: Map<ID, ID[]>,
    readonly roots: Node<ID, T>[],
    readonly nodes: Node<ID, T>[],
}

export function prepareGraphForExecution<ID, T>(
    nodes: Node<ID, T>[],
    connections: Connection<ID>[],
): PreparedGraph<ID, T> {
    const nodeIdToNode = nodes
        .map((node) => [node.id, node] as const)
        .toMap();
    const nodeIdToParentId = connections
        .map((connection) => [connection.childId, connection.parentId] as const)
        .toMap();
    const nodeIdToChildrenId = connections
        .map((connection) => [connection.parentId, connection.childId] as const)
        .collectToArray();
    const roots = nodes
        .filter((node) => !nodeIdToParentId.get(node.id));
    return {
        nodeIdToNode,
        nodeIdToParentId,
        nodeIdToChildrenId,
        roots,
        nodes,
    };
}

export interface Worker<ID, T> {
    readonly execute: (node: Node<ID, T>) => Promise<void>,
}

export async function executeGraph<ID, T>(
    graph: PreparedGraph<ID, T>,
    workers: Worker<ID, T>[],
) {
    const freeWorkers = [...workers];
    const workersSemaphore = createSemaphore(freeWorkers.length);
    const jobs = new Map<ID, Promise<void>>();
    async function processNode(
        nodeId: ID,
    ) {
        return jobs.getOrPut(
            nodeId,
            async() => {
                const children = graph.nodeIdToChildrenId.get(nodeId) ?? [];
                await Promise
                    .all(children.map((c) => processNode(c)));
                await workersSemaphore.lock();
                const worker = freeWorkers.pop()!;
                await worker.execute(graph.nodeIdToNode.get(nodeId)!);
                freeWorkers.push(worker);
                workersSemaphore.unlock();
            },
        );
    }
    await Promise.all(
        graph.roots.map(async(root) => {
            await processNode(root.id);
        }),
    );

}
