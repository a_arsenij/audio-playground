import { arrayOfNItems } from "../std/Array";

export type Queue<T> = {
    size(): number,
    push(t: T): void,
    pop(): T | undefined,
}


export function queue<T>(
    initialSize = 32,
): Queue<T> {
    let head = 0;
    let tail = -1;
    let size = 0;
    let arr: (T | undefined)[] = arrayOfNItems(initialSize, () => undefined);
    return {
        size(): number {
            return size;
        },
        push(t: T) {
            if (size === arr.length) {
                arr = arrayOfNItems(arr.length * 2, (idx: number) => {
                    if (idx >= size) {
                        return undefined;
                    }
                    return arr[
                        (idx + head) % arr.length
                    ];
                });
                head = 0;
                tail = size - 1;
            }
            size++;
            tail++;
            if (tail >= arr.length) {
                tail = 0;
            }
            arr[tail] = t;
        },
        pop(): T | undefined {
            if (size === 0) {
                return undefined;
            }
            const elem = arr[head];
            arr[head] = undefined;
            head++;
            if (head >= arr.length) {
                head = 0;
            }
            size--;
            return elem;
        },
    };
}
