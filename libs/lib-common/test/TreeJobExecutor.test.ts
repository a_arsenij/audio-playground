

import { beforeEach, describe, it } from "node:test";

import {
    Connection,
    executeGraph,
    Node,
    prepareGraphForExecution,
} from "@audio-playground/lib-common/algo/TreeJobExecutor";
import { arrayOfNItems } from "@audio-playground/lib-common/std/Array";
import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { PRNG } from "@audio-playground/lib-common/std/PRNG";
import { padStart } from "@audio-playground/lib-common/std/String";
import { sleep } from "@audio-playground/lib-common/std/Sync";
import assert from "assert";

const nodesValues = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
];
const nodes = nodesValues.map((v) => ({
    id: v,
    value: v,
}));
const connections = [
    {
        parentId: 1,
        childId: 2,
    },
    {
        parentId: 1,
        childId: 3,
    },
    {
        parentId: 1,
        childId: 4,
    },
    {
        parentId: 1,
        childId: 5,
    },
    {
        parentId: 2,
        childId: 7,
    },
    {
        parentId: 4,
        childId: 8,
    },
    {
        parentId: 7,
        childId: 9,
    },
    {
        parentId: 9,
        childId: 10,
    },
    {
        parentId: 3,
        childId: 10,
    },
    {
        parentId: 10,
        childId: 11,
    },
    {
        parentId: 8,
        childId: 11,
    },
    {
        parentId: 5,
        childId: 11,
    },
    {
        parentId: 6,
        childId: 11,
    },
];

async function checkNThreaded(
    nodes: Node<number, number>[],
    connections: Connection<number>[],
    threads: number,
): Promise<Map<number, number>> {
    const prepared = prepareGraphForExecution(
        nodes,
        connections,
    );
    const executionOrder: number[] = [];
    const tasksPerThread = new Map<number, number>();

    await executeGraph(
        prepared,
        arrayOfNItems(
            threads,
            (idx) => ({
                execute: async(node) => {
                    tasksPerThread.set(
                        idx,
                        (tasksPerThread.get(idx) ?? 0) + 1,
                    );
                    await sleep(Math.random() * 10);
                    executionOrder.push(node.value);
                    return;
                },
            }),
        ),
    );
    for (const connection of connections) {
        const indexOfParent = executionOrder.indexOf(connection.parentId);
        const indexOfChild = executionOrder.indexOf(connection.childId);
        assert(
            indexOfParent > indexOfChild,
            `Parent should be executed later than child, but indexOfParent=${indexOfParent}<${indexOfChild}`,
        );
    }
    return tasksPerThread;
}

describe("TreeJobExecutor", () => {
    beforeEach(monkeyPatch);

    it("1 threaded execution is sequential-ish", async() => {
        await checkNThreaded(nodes, connections, 1);
    });

    it("2 threaded execution is ok", async() => {
        await checkNThreaded(nodes, connections, 2);
    });

    it("16 threaded execution is ok", async() => {
        await checkNThreaded(nodes, connections, 16);
    });

    it("Complex graph, lots of threads", async() => {
        const rng = new PRNG();
        const THREADS = 16;
        const NODES_NUMBER = 128;
        const NODES = arrayOfNItems(
            NODES_NUMBER,
            (i) => ({
                id: i,
                value: i,
            }),
        );
        const CONNECTIONS = [...Array(NODES_NUMBER)].map((_, idx) => {
            const maxParentId = idx - 1;
            if (maxParentId <= 0) {
                return [];
            }
            const connections = Math.round(rng.nextNumber() * rng.nextNumber() * 8);
            return arrayOfNItems(
                Math.round(connections),
                () => Math.round(rng.nextNumber() * maxParentId),
            ).map((p) => ({
                parentId: p,
                childId: idx,
            }));
        }).flat();
        // console.log( CONNECTIONS.map((c) => `n${c.parentId}>n${c.childId}`).join("\n"));
        const distribution = await checkNThreaded(NODES, CONNECTIONS, THREADS);
        console.log("DISTRIBUTION:");
        for (let i = 0; i < THREADS; i++) {
            console.log(`Thread #${padStart(i, 2)}: ${padStart(distribution.get(i) ?? 0, 3)} jobs`);
        }
    });

});


