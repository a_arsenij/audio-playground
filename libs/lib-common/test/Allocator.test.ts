
import { beforeEach, describe, it } from "node:test";

import { createAllocator } from "@audio-playground/lib-common/std/Allocator";
import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import assert from "assert";


describe("Allocator", () => {
    beforeEach(monkeyPatch);

    it("Can allocate", () => {
        const allocator = createAllocator(1024, (req) => req);
        const region = allocator.allocate(8);
        assert.strictEqual(0, region.offset, "Region expected to be allocated at offset=0");
        assert.strictEqual(8, region.size, "Region expected to be allocated with size=8");
    });

    it("Next allocation does not overlap", () => {
        const allocator = createAllocator(1024, (req) => req);
        allocator.allocate(8);
        const region = allocator.allocate(8);
        assert.strictEqual(8, region.offset, "Region expected to be allocated at offset=8");
        assert.strictEqual(8, region.size, "Region expected to be allocated with size=8");
    });

    it("Can free", () => {
        const allocator = createAllocator(1024, (req) => req);
        allocator.free(allocator.allocate(8));
        const region = allocator.allocate(8);
        assert.strictEqual(0, region.offset, "Region expected to be allocated at offset=0");
        assert.strictEqual(8, region.size, "Region expected to be allocated with size=8");
    });

    it("Can grow", () => {
        let growRequested = 0;
        const allocator = createAllocator(8, (req) => {
            growRequested++;
        });
        allocator.allocate(8);
        const a = allocator.allocate(8);
        assert.strictEqual(growRequested, 1, "Heap grow should be requested");
        const b = allocator.allocate(8);
        assert.strictEqual(growRequested, 2, "Heap grow should be requested");
        allocator.free(a);
        allocator.free(b);
        allocator.allocate(23);
        assert.strictEqual(growRequested, 2, "Heap grow should not be requested");
        allocator.allocate(2);
        assert.strictEqual(growRequested, 3, "Heap grow should be requested");
    });

    it("Filling old gap with many small regions", () => {
        const allocator = createAllocator(1024, (req) => req);
        allocator.allocate(32);
        allocator.allocate(32);
        const r = allocator.allocate(32);
        allocator.allocate(32);
        allocator.free(r);
        const a = allocator.allocate(8);
        const b = allocator.allocate(8);
        const c = allocator.allocate(8);
        const d = allocator.allocate(8);

        assert.strictEqual(a.offset, 64, "Expected small region to have offset=64");
        assert.strictEqual(b.offset, 72, "Expected small region to have offset=72");
        assert.strictEqual(c.offset, 80, "Expected small region to have offset=80");
        assert.strictEqual(d.offset, 88, "Expected small region to have offset=88");
    });

});
