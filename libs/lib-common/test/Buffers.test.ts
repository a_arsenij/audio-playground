import { beforeEach, describe, it } from "node:test";

import { assertArraysAreEqual } from "@audio-playground/lib-test-utils/assertArraysAreEqual";

import {
    bufferToF64,
    bufferToI16,
    bufferToI32,
    f64toBuffer,
    i16toBuffer,
    i32toBuffer,
    loadMultipleBlocks,
    saveMultipleBlocks,
} from "../src/std/Buffers";
import { monkeyPatch } from "../src/std/MonkeyPatching";


describe("Buffers", () => {
    beforeEach(monkeyPatch);



    it("saveMultipleBlocks / loadMultipleBlocks", () => {

        const i16s = [2, 5, 7, 13];
        const i32s = [512, 717, 1_000_000];
        const f64s = [Math.PI, Math.E, Math.SQRT2, Math.SQRT1_2, Math.LN2, Math.LN10];

        const buffer = saveMultipleBlocks([
            () => i16toBuffer(i16s),
            i32toBuffer(i32s),
            f64toBuffer(f64s),
        ]);

        const buffers = loadMultipleBlocks(buffer);

        const i16r = bufferToI16(buffers[0]!);
        const i32r = bufferToI32(buffers[1]!);
        const f64r = bufferToF64(buffers[2]!);

        assertArraysAreEqual(i16r, i16s);
        assertArraysAreEqual(i32r, i32s);
        assertArraysAreEqual(f64r, f64s);

    });


});
