import { beforeEach, describe, it } from "node:test";

import { queue } from "@audio-playground/lib-common/algo/Queue";
import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import assert from "assert";


describe("Queue", () => {
    beforeEach(monkeyPatch);

    it("FIFO", () => {
        const q = queue<number>();
        q.push(1);
        q.push(2);
        assert.strictEqual(1, q.pop(), "Order preserved");
        assert.strictEqual(2, q.pop(), "Order preserved");
    });

    it("Can grow", () => {
        const q = queue<number>(1);
        q.push(1);
        q.push(2);
        q.push(3);
        q.push(4);
        q.push(5);
        assert.strictEqual(1, q.pop(), "Order preserved");
        assert.strictEqual(2, q.pop(), "Order preserved");
        assert.strictEqual(3, q.pop(), "Order preserved");
        assert.strictEqual(4, q.pop(), "Order preserved");
        assert.strictEqual(5, q.pop(), "Order preserved");
    });

    it("Can grow in the middle", () => {
        const q = queue<number>(1);
        q.push(1);
        q.push(2);
        q.push(3);
        q.push(4);
        q.pop();
        q.pop();
        q.push(5);
        q.push(6);
        q.push(7);
        q.push(8);
        assert.strictEqual(3, q.pop(), "Order preserved");
        assert.strictEqual(4, q.pop(), "Order preserved");
        assert.strictEqual(5, q.pop(), "Order preserved");
        assert.strictEqual(6, q.pop(), "Order preserved");
        assert.strictEqual(7, q.pop(), "Order preserved");
        assert.strictEqual(8, q.pop(), "Order preserved");
    });

});
