import { beforeEach, describe, it } from "node:test";

import {
    identitySegmentToExponential,
    identitySegmentToLinear,
    linearToIdentitySegment,
} from "@audio-playground/lib-common/math/Ranges";
import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";

describe("Identity range <---> specific range", () => {
    beforeEach(monkeyPatch);

    it("Identity to linear", () => {

        assertNumbersClose(
            0,
            identitySegmentToLinear(0, 0, 1),
            0,
            "Identity transform, segment start",
        );
        assertNumbersClose(
            1,
            identitySegmentToLinear(1, 0, 1),
            0,
            "Identity transform, segment end",
        );

        assertNumbersClose(
            5,
            identitySegmentToLinear(0.5, 0, 10),
            0,
            "0...10 segment, middle",
        );
        assertNumbersClose(
            0,
            identitySegmentToLinear(0.5, -10, 10),
            0,
            "-10...10 segment, middle",
        );

        assertNumbersClose(
            30,
            identitySegmentToLinear(0.3, 0, 100),
            0,
            "1...100 segment, 0.3",
        );

    });

    it("Linear to identity", () => {

        assertNumbersClose(
            0,
            linearToIdentitySegment(0, 0, 1),
            0,
            "Identity transform, segment start",
        );
        assertNumbersClose(
            1,
            linearToIdentitySegment(1, 0, 1),
            0,
            "Identity transform, segment end",
        );

        assertNumbersClose(
            0.5,
            linearToIdentitySegment(5, 0, 10),
            0,
            "0...10 segment, middle",
        );
        assertNumbersClose(
            0.5,
            linearToIdentitySegment(0, -10, 10),
            0,
            "-10...10 segment, middle",
        );

        assertNumbersClose(
            0.3,
            linearToIdentitySegment(30, 0, 100),
            0,
            "1...100 segment, 0.3",
        );

    });

    it("Identity to exp", () => {

        assertNumbersClose(
            0,
            identitySegmentToExponential(0, 0, 1, 10),
            0,
            "Identity transform, segment start",
        );
        assertNumbersClose(
            1,
            identitySegmentToExponential(1, 0, 1, 10),
            0,
            "Identity transform, segment end",
        );

        assertNumbersClose(
            2.4,
            identitySegmentToExponential(0.5, 0, 10, 10),
            0.01,
            "0...10 segment, base=10, 0.5",
        );

        assertNumbersClose(
            0.9,
            identitySegmentToExponential(0.5, 0, 10, 100),
            0.01,
            "0...10 segment, base=100, 0.5",
        );

    });

});
