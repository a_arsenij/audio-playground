import assert from "node:assert";
import { beforeEach, describe, it } from "node:test";

import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";

import { avlAdd, avlDelete, avlNode, avlSearch, AVLTreeNode } from "../src/algo/AVLTree";
import { monkeyPatch } from "../src/std/MonkeyPatching";


describe("AVL Tree", () => {
    beforeEach(monkeyPatch);

    function assertTreeIsBalanced(nodeToCheck: AVLTreeNode<number>) {
        const toCheck: AVLTreeNode<number>[] = [nodeToCheck];
        while (true) {
            const node = toCheck.pop();
            if (!node) {
                return;
            }
            if (!node.left && node.right) {
                assert(
                    node.right.height <= 1,
                    "Height should be <= 1 if there is no other same level subtree",
                );
                toCheck.push(node.right);
            } else if (node.left && !node.right) {
                assert(
                    node.left.height <= 2,
                    "Height should be <= 1 if there is no other same level subtree",
                );
                toCheck.push(node.left);
            } else if (node.left && node.right) {
                assert(
                    Math.abs(node.left.height - node.right.height) <= 1,
                    "Same level subtrees heights should differ by no more than 1",
                );
                toCheck.push(node.right);
                toCheck.push(node.left);
            }
        }
    }

    function assertNotNull(nodeToCheck: AVLTreeNode<number> | undefined) {
        assert(
            nodeToCheck !== null,
            "There should remain some existing nodes",
        );
    }

    it("Tree is balanced after additions - 1,2,...,999,1000", () => {
        let root: AVLTreeNode<number> = avlNode(1, 1);
        for (let i = 2; i < 1000; i++) {
            root = avlAdd(root, i, i);
        }
        assertTreeIsBalanced(root);
    });
    it("Tree is balanced after additions - 1000,999,...,2,1", () => {
        let root: AVLTreeNode<number> = avlNode(1000, 1000);
        for (let i = 999; i >= 0; i--) {
            root = avlAdd(root, i, i);
        }
        assertTreeIsBalanced(root);
    });
    it("Tree is balanced after additions - random", () => {
        let root: AVLTreeNode<number> = avlNode(1, 1);
        for (let i = 0; i <= 999; i++) {
            const n = Math.round(1000 * Math.random());
            root = avlAdd(root, n, n);
        }
        assertTreeIsBalanced(root);
    });
    it("Tree is balanced after deletions - 1,2,...,999", () => {
        let root: AVLTreeNode<number> = avlNode(1, 1);
        for (let i = 2; i < 1000; i++) {
            root = avlAdd(root, i, i);
        }
        for (let i = 1; i < 999; i++) {
            root = avlDelete(root, i, i)!;
            assertNotNull(root);
            assertTreeIsBalanced(root);
        }
        assertTreeIsBalanced(root);
    });
    it("Tree is balanced after deletions - 999,...,2,1", () => {
        let root: AVLTreeNode<number> = avlNode(1, 1);
        for (let i = 2; i < 1000; i++) {
            root = avlAdd(root, i, i);
        }
        for (let i = 999; i >= 1; i--) {
            root = avlDelete(root, i, i)!;
            assertNotNull(root);
            assertTreeIsBalanced(root);
        }
        assertTreeIsBalanced(root);
    });
    it("Tree is balanced after deletions - random", () => {
        let root: AVLTreeNode<number> = avlNode(1, 1);
        for (let i = 2; i < 1000; i++) {
            root = avlAdd(root, i, i);
        }
        for (let i = 0; i <= 999; i++) {
            const n = Math.round(1000 * Math.random());
            root = avlDelete(root, n, n)!;
            assertNotNull(root);
            assertTreeIsBalanced(root);
        }
    });

    it("Tree is able to search: smallest >= 100", () => {
        let root: AVLTreeNode<number> = avlNode(1, 1);
        for (let i = 2; i < 1000; i++) {
            const n = i * 13;
            root = avlAdd(root, n, n);
        }
        const num = avlSearch(
            root,
            {
                lookingFor: "smallest",
                filter: {
                    operator: "moreOrEqual",
                    operand: 100,
                },
            },
        );
        assertNumbersClose(
            num!.value,
            104,
            0,
            "Search results",
        );
    });

    it("Tree is able to search: biggest <= 100", () => {
        let root: AVLTreeNode<number> = avlNode(1, 1);
        for (let i = 2; i < 1000; i++) {
            const n = i * 13;
            root = avlAdd(root, n, n);
        }
        const num = avlSearch(
            root,
            {
                lookingFor: "biggest",
                filter: {
                    operator: "lessOrEqual",
                    operand: 100,
                },
            },
        );
        assertNumbersClose(
            num!.value,
            91,
            0,
            "Search results",
        );
    });

});
