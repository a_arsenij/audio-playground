import { beforeEach, describe, it } from "node:test";

import { Expr, parseExpr } from "@audio-playground/lib-common/math/Expr";
import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import assert from "assert";

function a2s(expr: Expr): string {
    switch (expr.type) {
        case "const": return `${expr.value}`;
        case "variable": return expr.name;
        case "op": return `${expr.op}(${expr.args.map(a2s).join(", ")})`;
    }
}

describe("Expression parser", () => {
    beforeEach(monkeyPatch);

    it("Result Ok", () => {

        ([
            [
                "1+2",
                "plus(1, 2)",
            ],
            [
                " sin(x) ",
                "sin(x)",
            ],
            [
                " 2 + x - x ^ 5 * varName - 7 / 3",
                "minus(minus(plus(2, x), multiply(pow(x, 5), varName)), divide(7, 3))",
            ],
            [
                "e",
                "e",
            ],
            [
                "175*(t +2)",
                "multiply(175, plus(t, 2))",
            ],
            [
                "-2 + -3",
                "plus(-2, -3)",
            ],
            [
                "1+       2",
                "plus(1, 2)",
            ],
            [
                "1 - 2 - 3",
                "minus(minus(1, 2), 3)",
            ],
            [
                "1 - 2 + 3",
                "plus(minus(1, 2), 3)",
            ],
            [
                "1 / 2 / 3",
                "divide(divide(1, 2), 3)",
            ],
            [
                "2 ** 10",
                "pow(2, 10)",
            ],
        ] as const).forEach(([expr, expected], i) => {

            const result = parseExpr(expr);

            assert.strictEqual(result.type, "ok", `Parsed string: ${expr}; ${result.type === "error" ? result.message : ""}`);

            const fmt = a2s(result.expr);

            assert.deepEqual(expected, fmt, `Parsed string: ${expr}`);
        });
    });


    it("Result Error", () => {

        const input = [
            " +",
            "  sin() ",
            " 2 * / 4",
            "^ x",
            "  pow(3) ",
            "1 s + 2",
            "2 * * 10",
        ];

        input.forEach((val) => {
            const result = parseExpr(val);
            assert.strictEqual(result.type, "error");
        });
    });

});
