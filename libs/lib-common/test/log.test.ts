import { beforeEach, describe, it } from "node:test";

import { logOverCustomBase } from "@audio-playground/lib-common/math/Log";
import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";


describe("Log with custom base", () => {
    beforeEach(monkeyPatch);

    it("Log with custom base", () => {

        assertNumbersClose(
            1,
            logOverCustomBase(2, 2),
            0.000001,
            "log_2(2) = 1",
        );
        assertNumbersClose(
            2,
            logOverCustomBase(2, 4),
            0.000001,
            "log_2(4) = 2",
        );

        assertNumbersClose(
            1,
            logOverCustomBase(3, 3),
            0.000001,
            "log_3(3) = 1",
        );
        assertNumbersClose(
            2,
            logOverCustomBase(3, 9),
            0.000001,
            "log_3(9) = 2",
        );

    });
});
