import { beforeEach, describe, it } from "node:test";

import { equalsSet, MIMath, Mixed, MultiInterval } from "@audio-playground/lib-common/math/MultiInterval";
import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertArraysAreEqual } from "@audio-playground/lib-test-utils/assertArraysAreEqual";



describe("Interval calculus", () => {
    beforeEach(monkeyPatch);
    describe("Number set calculus", () => {
        it("Addition", () => {
            const add: [Mixed, Mixed][] = [
                [[1, 2], [3, 4]],
                [[-1, 5], [-0, +0]],
                [0, [-0, +0]],
                [-0, [-0, +0]],

                [Infinity, -Infinity],
                [Infinity, [-Infinity, Infinity]],
                [[-Infinity, Infinity], [-Infinity, Infinity]],
                [[-Infinity, -0], [0, Infinity]],

                [NaN, [1, 5]],
                [[-Infinity, 1], [NaN, NaN]],
                [Infinity, [-1, 1]],

            ];

            const expected: MultiInterval[] = [
                [[4, 6]],
                [[-1, 5]],
                [0],
                [[-0, +0]],

                [NaN],
                [NaN, Infinity],
                [NaN, [-Infinity, Infinity]],
                [NaN, [-Infinity, Infinity]],

                [NaN],
                [NaN],
                [Infinity],

            ];
            const actual = add.map((pair) => MIMath.addSingle(pair[0], pair[1]));
            assertArraysAreEqual(
                actual,
                expected,
                (a) => a.toString(),
                equalsSet,
            );

        });
    });

});
