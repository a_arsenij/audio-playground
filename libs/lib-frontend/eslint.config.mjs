import { generateCommonLibConfig } from "../eslint-libs-common.config.mjs";

export default generateCommonLibConfig("lib-frontend");
