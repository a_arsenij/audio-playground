import { RefObject, useEffect, useRef } from "react";

type AddEventListener = (
    wheel: "wheel",
    action: (e: WheelEvent) => void,
    p: {passive: false}
) => void;

type RemoveEventListener = (
    wheel: "wheel",
    action: (e: WheelEvent) => void
) => void;

export function useActiveWheelEvent<T extends {
    addEventListener: AddEventListener,
    removeEventListener: RemoveEventListener,
}>(
    onWheel: (e: WheelEvent) => void,
    customRef: RefObject<T> | undefined = undefined,
) {

    const ref = useRef<T>(null);
    const refToUse = customRef || ref;

    useEffect(() => {
        const r = refToUse.current;
        if (!r) {
            return;
        }
        r.addEventListener("wheel", onWheel, { passive: false });
        return () => {
            r.removeEventListener("wheel", onWheel);
        };
    }, [onWheel, refToUse]);

    return refToUse;

}
