import { useCallback, useEffect, useMemo, useState } from "react";

import { applySetState } from "../std/ApplySetState";
import { SetState, SetStateValue } from "../std/SetState";

function readObject() {
    let decoded = decodeURIComponent(window.location.hash);
    if (decoded.startsWith("#")) {
        decoded = decoded.slice(1);
    }
    if (!decoded) {
        decoded = "{}";
    }
    return JSON.parse(decoded);
}

export function useStringStateInUrl<T extends string | undefined>(
    key: string,
    initialValueRaw: T,
): [T, SetState<T>] {

    const initialValue = useMemo(() => (readObject()[key] as T | undefined) ?? initialValueRaw, []);
    const [state, setState] = useState<T>(initialValue);
    const onChangeUrl = useCallback(
        () => {
            // TODO: implement
        },
        [],
    );
    useEffect(() => {
        window.addEventListener("hashchange", onChangeUrl);
        return () => {
            window.removeEventListener("hashchange", onChangeUrl);
        };
    }, []);
    const setStateAndUpdateUrl = useCallback(
        (value: SetStateValue<T>) => {
            setState((prev) => {
                const newState = applySetState(value, prev);
                const obj = readObject();
                if (newState === undefined) {
                    delete obj[key];
                } else {
                    obj[key] = newState;
                }
                window.location.hash = encodeURIComponent(JSON.stringify(obj));
                return newState;
            });
        },
        [],
    );
    return [state, setStateAndUpdateUrl];


}
