import { Observable } from "@audio-playground/lib-common/std/Observable";
import { useEffect, useState } from "react";


export function useObserved<T>(observed: Observable<T>): T {
    const [state, setState] = useState<T>(() => observed.get());
    useEffect(() => {
        return observed.subscribe((event) => {
            setState(event);
        }).unsubscribe;
    }, [observed]);
    return state;
}
