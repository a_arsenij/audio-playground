import { generateId } from "@audio-playground/lib-common/std/Id";
import { useCallback, useEffect, useState } from "react";

function read<T extends string>(
    key: string,
    defaultValue: T,
): T {
    return (localStorage.getItem(key) as T | null) ?? defaultValue;
}

const listeners = new Map<string, () => void>();

export function useLocalStorageState<T extends string>(
    key: string,
    defaultValue: T,
) {
    const [value, setValue] = useState<T>(
        read(key, defaultValue),
    );

    const actualSetValue = useCallback((value: T) => {
        localStorage.setItem(key, value);
        setValue(value);
        Array.from(listeners.values()).forEach((a) => a());
    }, [key]);

    useEffect(() => {
        const id = generateId();
        const handle = () => {
            setValue(read(key, defaultValue));
        };
        listeners.set(id, handle);
        return () => {
            listeners.delete(id);
        };
    }, [key, defaultValue]);

    return [
        value,
        actualSetValue,
    ] as const;
}
