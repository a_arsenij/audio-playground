import React, { useCallback, useRef, useState } from "react";

export function useReffedState<T>(
    initialValue: () => T,
): [T, { current: T }, React.Dispatch<React.SetStateAction<T>>] {
    const [state, setState] = useState<T>(initialValue);
    const ref = useRef<T>(state);

    const actualSetState = useCallback((
        arg: T | ((old: T) => T),
    ) => {
        if (typeof arg === "function") {
            const newState = (arg as any)(ref.current);
            ref.current = newState;
            setState(newState);
        } else {
            ref.current = arg;
            setState(arg);
        }
    }, []);

    return [
        state,
        ref,
        actualSetState,
    ];
}
