export function someInputIsFocused() {
    return isElementInput(document.activeElement);
}

export function someButtonIsFocused() {
    return document.activeElement?.tagName === "BUTTON";
}

export function isElementInput(e: Element | undefined | null): boolean {
    return (e?.tagName === "INPUT") || !!(e?.getAttribute("contenteditable"));
}
