import { NotAFunction } from "@audio-playground/lib-common/types/UtilityTypes";
import { Dispatch, SetStateAction, useCallback } from "react";

import { applySetState } from "./ApplySetState";

export type SetState<
    S extends NotAFunction
> = Dispatch<SetStateAction<S>>

export type SetStateValue<
    S extends NotAFunction
> = S | ((old: S) => S)

export function add<EL extends NotAFunction>(
    setter: SetState<EL[]>,
    newlyCreated: EL,
) {
    setter((prev) => [...prev, newlyCreated]);
}

export function useAdd<EL extends NotAFunction>(
    setter: SetState<EL[]>,
) {
    return useCallback(
        (
            newItem: EL,
        ) => add(setter, newItem),
        [setter],
    );
}

export function swapByIdx<EL>(
    setter: SetState<EL[]>,
    a: number,
    b: number,
) {
    setter((prev) => {
        const res = [...prev];
        const aa = res[a]!;
        res[a] = res[b]!;
        res[b] = aa;
        return res;
    });
}

export function useSwapByIdx<EL>(
    setter: SetState<EL[]>,
) {
    return useCallback(
        (
            a: number,
            b: number,
        ) => swapByIdx(setter, a, b),
        [setter],
    );
}

export function deleteByIndex<EL>(
    setter: SetState<EL[]>,
    idx: number,
) {
    setter((prev) => prev.filter((v, i) => i !== idx));
}

export function useDeleteByIndex<EL>(
    setter: SetState<EL[]>,
) {
    return useCallback(
        (
            idx: number,
        ) => deleteByIndex(setter, idx),
        [setter],
    );
}

export function deleteById<EL extends {id: string}>(
    setter: SetState<EL[]>,
    id: string,
) {
    setter((prev) => prev.filter((v) => v.id !== id));
}

export function useDeleteById<EL extends {id: string}>(
    setter: SetState<EL[]>,
) {
    return useCallback(
        (
            id: string,
        ) => deleteById(setter, id),
        [setter],
    );
}

export function setByIndex<
    EL extends NotAFunction,
>(
    setter: SetState<EL[]>,
    index: number,
    value: SetStateValue<EL>,
) {
    setter((prev) =>
        prev.map((p, idx) =>
            idx === index ? applySetState(value, p) : p,
        ),
    );
}

export function useSetByIndex<
    EL extends NotAFunction,
>(
    setter: SetState<EL[]>,
) {
    return useCallback(
        (
            index: number,
            value: SetStateValue<EL>,
        ) => setByIndex(setter, index, value),
        [setter],
    );
}

export function useSetForIndex<
    EL extends NotAFunction & {id: string},
>(
    setter: SetState<EL[]>,
    index: number,
): SetState<EL> {
    return useCallback(
        (
            value: SetStateValue<EL>,
        ) => setByIndex(setter, index, value),
        [setter],
    );
}

export function setById<
    EL extends NotAFunction & {id: string}
>(
    setter: SetState<EL[]>,
    id: string,
    value: SetStateValue<EL>,
) {
    setter((prev) =>
        prev.map((p) =>
            p.id === id ? applySetState(value, p) : p,
        ),
    );
}

export function useSetById<
    EL extends NotAFunction & {id: string},
>(
    setter: SetState<EL[]>,
) {
    return useCallback(
        (
            id: string,
            value: SetStateValue<EL>,
        ) => setById(setter, id, value),
        [setter],
    );
}

export function useSetForId<
    EL extends NotAFunction & {id: string},
>(
    setter: SetState<EL[]>,
    id: string,
): SetState<EL> {
    return useCallback(
        (
            value: SetStateValue<EL>,
        ) => setById(setter, id, value),
        [id, setter],
    );
}

export function setterByKey<
    KEY extends string,
    OBJ extends NotAFunction & object & { [k in KEY]: NotAFunction },
>(
    setter: SetState<OBJ>,
    key: KEY,
) {
    return (value: SetStateValue<OBJ[KEY]>) => {
        setter((prev) => {
            return {
                ...prev,
                [key]: applySetState(value, prev[key]),
            };
        });
    };
}

export function setByKey<
    KEY extends string,
    OBJ extends NotAFunction & object & { [k in KEY]: NotAFunction },
>(
    setter: SetState<OBJ>,
    key: KEY,
    value: SetStateValue<OBJ[KEY]>,
) {
    setterByKey(setter, key)(value);
}

export function useSetByKey<
    KEY extends string,
    OBJ extends NotAFunction & { [k in KEY]: NotAFunction },
>(
    setter: SetState<OBJ>,
    key: KEY,
) {
    return useCallback(
        (value: SetStateValue<OBJ[KEY]>) => setByKey(setter, key, value),
        [setter, key],
    );
}

export function useByKey<
    KEY extends string,
    OBJ extends NotAFunction & { [k in KEY]: NotAFunction },
>(
    value: OBJ,
    setter: SetState<OBJ>,
    key: KEY,
): readonly [OBJ[KEY], SetState<OBJ[KEY]>] {
    return [
        value[key],
        useSetByKey(setter, key),
    ] as const;
}


export function bakeSetBy<KEY, EL extends NotAFunction>(
    setter: (key: KEY, value: SetStateValue<EL>) => void,
    key: KEY,
): SetState<EL> {
    return (value: SetStateValue<EL>) => {
        setter(key, value);
    };
}
