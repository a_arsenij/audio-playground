import React from "react";

export type CssVariable = `${string}`

export function variables(data: { [k in CssVariable]: string | number }): React.CSSProperties {
    return data as React.CSSProperties;
}
