import { createCompletablePromise } from "@audio-playground/lib-common/std/CompletablePromise";

type Extension = `.${string}`;

export function openFile(
    type: string | undefined = undefined,
    accept: Extension | undefined = undefined,
): Promise<ArrayBuffer> {
    const res = createCompletablePromise<ArrayBuffer>();
    const file = document.createElement("input");
    type && file.setAttribute("type", type);
    accept && file.setAttribute("accept", accept);
    file.setAttribute("type", "file");
    file.addEventListener("change", () => {
        const f = file.files![0]!;
        f.arrayBuffer().then((r) => res.resolve(r));
    });
    file.click();
    return res;
}

export function saveFile(
    suggestedName: string,
    types: {
        description: string,
        accept: {[k in string]: Extension[]},
    }[],
    write: (file: FileSystemFileHandle) => Promise<unknown>,
) {
    // @ts-ignore
    window.showSaveFilePicker({
        excludeAcceptAllOption: true,
        suggestedName,
        types,
    }).then(write);
}
