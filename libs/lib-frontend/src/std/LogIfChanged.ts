const obj: any = {};

export function logIfChanged(
    key: string,
    value: any,
) {

    const old = obj[key];
    if (old !== value) {
        console.log(`${key} changed`);
    }
    obj[key] = value;

}
