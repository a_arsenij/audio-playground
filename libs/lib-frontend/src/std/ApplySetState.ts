import { NotAFunction } from "@audio-playground/lib-common/types/UtilityTypes";

import { SetStateValue } from "./SetState";

export function applySetState<T extends NotAFunction>(
    setState: SetStateValue<T>,
    oldState: T,
): T {
    if (typeof setState === "function") {
        return setState(oldState);
    } else {
        return setState as T;
    }
}
