import { factorial } from "@audio-playground/lib-common/math/Factorial";
import { hasLength } from "@audio-playground/lib-common/std/Array";
import { getOrPut } from "@audio-playground/lib-common/std/GetOrPut";

import { FullyDefinedFunction, func, Local } from "../Op";
import { control, f32, f64, i32, i64 } from "../ops";
import { OP } from "../ops/OpsTypes";
import { F32, F64, I32, voidType, WAType } from "../Types";

type T = typeof f32 | typeof f64;

type RESULT<FT extends WAType> = {
    sin: FullyDefinedFunction<string, [FT]>,
    cos: FullyDefinedFunction<string, [FT]>,
    asin: FullyDefinedFunction<string, [FT]>,
    tan: FullyDefinedFunction<string, [FT]>,
    pow: FullyDefinedFunction<string, [FT]>,
    pow201: FullyDefinedFunction<string, [FT]>,
    powI32: FullyDefinedFunction<string, [FT]>,
    pow2: FullyDefinedFunction<string, [FT]>,
    log2: FullyDefinedFunction<string, [FT]>,
    log10: FullyDefinedFunction<string, [FT]>,
    logE: FullyDefinedFunction<string, [FT]>,
    exp01: (members: number) => FullyDefinedFunction<string, [FT]>,
    exp: (members: number) => FullyDefinedFunction<string, [FT]>,
    log: (members: number) => FullyDefinedFunction<string, [FT]>,
    delayToi32: FullyDefinedFunction<string, [I32]>,
    isNaN: FullyDefinedFunction<string, [I32]>,
    sign: FullyDefinedFunction<string, [FT]>,
    polynomial: (arg: Local<string, FT>, coefs: number[]) => OP[],
    mod: FullyDefinedFunction<string, [FT]>,
    heaviside: FullyDefinedFunction<string, [FT]>,
    replaceNaN: FullyDefinedFunction<string, [FT]>,
}

export function createFloatFunctions(type: typeof f64): RESULT<F64>
export function createFloatFunctions(type: typeof f32): RESULT<F32>
export function createFloatFunctions(type: T): RESULT<F32> | RESULT<F64> | RESULT<F32 | F64> {


    const functionPrefix = `${type.type.typeTitle}_`;

    const sin0025 = func()
        .name(`${functionPrefix}sin_0_025`)
        .param("angle", type)
        .result(type)
        .local("fASqr", type)
        .body((locals) => [
            locals.fASqr.set(
                type.mul(
                    locals.angle.get,
                    locals.angle.get,
                ),
            ),
            type.const(-2.39e-08),
            type.mul(locals.fASqr.get),
            type.add(type.const(2.7526e-06)),
            type.mul(locals.fASqr.get),
            type.sub(type.const(1.98409e-04)),
            type.mul(locals.fASqr.get),
            type.add(type.const(8.3333315e-03)),
            type.mul(locals.fASqr.get),
            type.sub(type.const(1.666666664e-01)),
            type.mul(locals.fASqr.get),
            type.add(type.const(1.0)),
            type.mul(locals.angle.get),
        ]);
    const powI32 = func()
        .name(`${functionPrefix}pow_i32`)
        .param("a", type)
        .param("b", i32)

        .local("currentP2", i32)
        .local("currentPower", type)
        .local("result", type)

        .result(type)
        .body((locals) => [

            type.const(1),
            locals.result.set,

            i32.const(1),
            locals.currentP2.set,

            locals.a.get,
            locals.currentPower.set,

            control.loop(voidType, (next) => [


                control.block(voidType, (skip) => [
                    locals.currentP2.get,
                    locals.b.get,
                    i32.and,
                    i32.eqz,
                    control.br_if(skip),

                    locals.result.get,
                    locals.currentPower.get,
                    type.mul,
                    locals.result.set,
                ]),

                locals.currentP2.get,

                locals.b.get,
                i32.lt_u,

                locals.currentP2.get,
                i32.const(1),
                i32.rotl,
                locals.currentP2.set,

                locals.currentPower.get,
                locals.currentPower.get,
                type.mul,
                locals.currentPower.set,

                control.br_if(next),

            ]),

            locals.result.get,


        ]);
    const mod = func()
        .name(`${functionPrefix}mod`)
        .param("a", type)
        .param("b", type)
        .result(type)
        .body((locals) => [
            type.sub(
                locals.a.get,
                type.mul(locals.b.get,
                    type.floor(
                        type.div(
                            locals.a.get,
                            locals.b.get,
                        ),
                    ),
                ),
            ),
        ]);
    const isNaN = func()
        .name(`${functionPrefix}is_nan`)
        .param("a", type)
        .result(i32)
        .body((locals) => [
            locals.a.get,
            locals.a.get,
            type.ne,
        ]);
    const delayToi32 = func()
        .name(`${functionPrefix}delay_to_i32`)
        .param("delay", type)
        .result(i32)
        .body((locals) => [
            locals.delay.get,
            isNaN.call,
            control.if({
                type,
                then: [
                    type.const(1),
                ],
                else: [
                    locals.delay.get,
                ],
            }),
            type.ceil,
            type.const(1),
            type.max,
            i32.trunc_f64_s,
        ]);
    const sign = func()
        .name(`${functionPrefix}sign`)
        .param("a", type)
        .result(type)
        .body((locals) => [
            type.copysign(
                type.const(1),
                locals.a.get,
            ),
        ]);
    const heaviside = func()
        .name(`${functionPrefix}heaviside`)
        .param("x", type)
        .result(type)
        .body((locals) => [
            type.const(1),
            type.const(0),

            locals.x.get,
            type.const(0),
            type.gt,
            control.select,

        ]);
    const mod2pi = func()
        .name(`${functionPrefix}mod_2pi`)
        .param("a", type)
        .result(type)
        .body((locals) => [
            type.sub(
                locals.a.get,
                type.mul(type.const(Math.PI * 2),
                    type.floor(
                        type.mul(
                            locals.a.get,
                            type.const(0.5 / Math.PI),
                        ),
                    ),
                ),
            ),
        ]);
    const normalizeAngle = func()
        .name(`${functionPrefix}normalize_angle`)
        .param("angle", type)
        .result(type)
        .body((locals) => [
            locals.angle.get,
            type.const(Math.PI),
            type.add,
            mod2pi.call,
            type.const(Math.PI),
            type.sub,
        ]);
    const sinPositive = func()
        .name(`${functionPrefix}sin_positive`)
        .param("angle", type)
        .result(type)
        .body((locals) => [

            type.le(type.const(Math.PI / 2), locals.angle.get),
            control.if({
                type,
                then: [
                    type.sub(type.const(Math.PI), locals.angle.get),
                ],
                else: [
                    locals.angle.get,
                ],
            }),
            sin0025.call,
        ],
        );
    const sin = func()
        .name(`${functionPrefix}sin`)
        .param("angle", type)
        .result(type)
        .body((locals) => [
            normalizeAngle.call(locals.angle.get),
            locals.angle.tee,
            type.abs,
            sinPositive.call,
            locals.angle.get,
            type.copysign,
        ]);
    const cos0025 = func()
        .name(`${functionPrefix}cos_0_025`)
        .param("angle", type)
        .result(type)
        .local("square", type)
        .body((locals) => [
            locals.square.tee(
                type.mul(
                    locals.angle.get,
                    locals.angle.get,
                ),
            ),

            type.const(-2.605e-07),
            type.mul,

            type.const(2.47609e-05),
            type.add,

            locals.square.get,
            type.mul,

            type.const(1.3888397e-03),
            type.sub,

            locals.square.get,
            type.mul,

            type.const(4.16666418e-02),
            type.add,

            locals.square.get,
            type.mul,

            type.const(4.999999963e-01),
            type.sub,

            locals.square.get,
            type.mul,

            type.const(1),
            type.add,
        ]);
    const cosPositive = func()
        .name(`${functionPrefix}cos_positive`)
        .param("angle", type)
        .result(type)
        .body((locals) => [
            type.le(type.const(Math.PI / 2), locals.angle.get),
            control.if({
                type,
                then: [
                    type.sub(type.const(Math.PI), locals.angle.get),
                    cos0025.call,
                    type.neg,
                ],
                else: [
                    locals.angle.get,
                    cos0025.call,
                ],
            }),
        ],
        );
    const cos = func()
        .name(`${functionPrefix}cos`)
        .param("angle", type)
        .result(type)
        .body((locals) => [
            normalizeAngle.call(locals.angle.get),
            type.abs,
            cosPositive.call,
        ]);
    const asinCore = func()
        .name(`${functionPrefix}asin_core`)
        .param("value", type)
        .result(type)
        .body((locals) => [

            type.const(Math.PI / 2),


            type.const(-0.0187293),
            type.mul(locals.value.get),
            type.add(type.const(0.0742610)),
            type.mul(locals.value.get),
            type.sub(type.const(0.2121144)),
            type.mul(locals.value.get),
            type.add(type.const(1.5707288)),

            type.sqrt(type.sub(type.const(1), locals.value.get)),

            type.mul,
            type.sub,

        ]);
    const asin = func()
        .name(`${functionPrefix}asin`)
        .param("value", type)
        .result(type)
        .body((locals) => [
            type.copysign(asinCore.call(type.abs(locals.value.get)), locals.value.get),
        ]);
    const tanCore = func()
        .name(`${functionPrefix}tan_core`)
        .param("angle", type)
        .local("square", type)
        .result(type)
        .body((locals) => [
            type.mul(locals.angle.get, locals.angle.get),
            locals.square.tee,

            type.mul(type.const(9.5168091e-03)),
            type.add(type.const(2.900525e-03)),
            type.mul(locals.square.get),
            type.add(type.const(2.45650893e-02)),
            type.mul(locals.square.get),
            type.add(type.const(5.33740603e-02)),
            type.mul(locals.square.get),
            type.add(type.const(1.333923995e-01)),
            type.mul(locals.square.get),
            type.add(type.const(3.333314036e-01)),
            type.mul(locals.square.get),
            type.add(type.const(1)),
            type.mul(locals.angle.get),
        ]);
    const tan = func()
        .name(`${functionPrefix}tan`)
        .param("angle", type)
        .local("angleabs", type)
        .result(type)
        .body((locals) => [
            locals.angle.get,
            type.add(
                type.const(Math.PI / 2),
            ),
            mod.call(
                type.const(Math.PI),
            ),
            type.sub(
                type.const(Math.PI / 2),
            ),
            locals.angle.tee,
            type.abs,
            locals.angleabs.tee,

            type.ge(type.const(Math.PI / 4)),

            control.if({
                type,
                then: [
                    type.div(
                        type.const(1),
                        tanCore.call(type.sub(type.const(Math.PI / 2), locals.angleabs.get)),
                    ),
                ],
                else: [
                    tanCore.call(locals.angleabs.get),
                ],
            }),

            locals.angle.get,
            type.copysign,
        ]);

    function createTailor01Exp(
        members: number,
    ): FullyDefinedFunction<string, [T["type"]]> {
        return func()
            .name(`${functionPrefix}tailor_01_exp_${members}`)
            .param("arg", type)
            .local("xpow", type)
            .result(type)
            .body((locals) => [
                type.const(1),
                locals.xpow.set,
                type.const(1),
                ...[...Array(members)].map((_, idx) => [

                    locals.xpow.get,
                    locals.arg.get,
                    type.mul,
                    locals.xpow.tee,

                    type.const(1 / factorial(idx + 1)),

                    type.mul,

                    type.add,
                ]).flat(),
            ]);
    }

    const tailor01ExpCache = new Map<number, FullyDefinedFunction<string, [T["type"]]>>();

    function exp01(
        members: number,
    ): FullyDefinedFunction<string, [T["type"]]> {
        return getOrPut(tailor01ExpCache, members, () => createTailor01Exp(members));
    }

    function createTailorExp(
        members: number,
    ): FullyDefinedFunction<string, [T["type"]]> {
        return func()
            .name(`${functionPrefix}tailor_exp_${members}`)
            .param("arg", type)
            .local("int", type)
            .result(type)
            .body((locals) => [

                locals.arg.get,

                locals.arg.get,
                type.floor,
                locals.int.tee,

                type.sub,
                exp01(members).call,

                type.const(Math.E),
                locals.int.get,
                i32.trunc_f64_u,
                powI32.call,

                type.mul,


            ]);
    }

    const tailorExpCache = new Map<number, FullyDefinedFunction<string, [T["type"]]>>();

    function exp(
        members: number,
    ): FullyDefinedFunction<string, [T["type"]]> {
        return getOrPut(tailorExpCache, members, () => createTailorExp(members));
    }

    function polynomial0(
        coefs: [number],
    ): OP[] {
        return [
            type.const(coefs[0]!),
        ];
    }

    function polynomial1(
        arg: Local<string, T["type"]>,
        coefs: [number, number],
    ): OP[] {
        return [
            type.const(coefs[1]!),
            type.const(coefs[0]!),
            arg.get,
            type.mul,
            type.add,
        ];
    }

    function polynomial2(
        arg: Local<string, T["type"]>,
        coefs: [number, number, number],
    ): OP[] {
        return [
            type.const(coefs[2]!),
            type.const(coefs[1]!),
            arg.get,
            type.mul,
            type.add,
            type.const(coefs[0]!),
            arg.get,
            arg.get,
            type.mul,
            type.mul,
            type.add,
        ];
    }

    function polynomialN(
        x: Local<string, T["type"]>,
        coefs: number[],
    ): OP[] {
        const res: OP[] = [];

        const first = coefs[0];

        if (first === undefined) {
            return [];
        }

        res.push(type.const(first));

        for (let i = 1; i < coefs.length; i++) {
            const c = coefs[i]!;
            res.push(x.get);
            res.push(type.mul);
            res.push(type.const(c));
            res.push(type.add);
        }

        return res;



    }

    function polynomial(
        arg: Local<string, T["type"]>,
        coefs: number[],
    ): OP[] {
        if (hasLength(coefs, 1)) {
            return polynomial0(coefs);
        }
        if (hasLength(coefs, 2)) {
            return polynomial1(arg, coefs);
        }
        if (hasLength(coefs, 3)) {
            return polynomial2(arg, coefs);
        }
        return polynomialN(arg, coefs);
    }

    const pow201 = func()
        .name(`${functionPrefix}pow2_01`)
        .param("arg", type)
        .result(type)
        .body((locals) => [

            polynomialN(
                locals.arg,
                [
                    /* Err: 6.6817e-11 */
                    0.000021659249227923283,
                    0.00014300329864624928,
                    0.001342909884559873,
                    0.009613634543795158,
                    0.05550523319690637,
                    0.24022637394708068,
                    0.6931471858754241,
                    1.0000000000000007,
                ],
            ),

        ]);


    const pow2 = func()
        .name(`${functionPrefix}pow2`)
        .param("arg", type)
        .local("floor", type)
        .result(type)
        .body((locals) => {
            const typeSpec = type.type.typeTitle === "f64"
                ? [
                    i64.trunc_f64_s,
                    i64.const(1023),
                    i64.add,
                    i64.const(52),
                    i64.rotl,
                    f64.reinterpret_i64,
                ]
                : [
                    i32.trunc_f32_s,
                    i32.const(1023),
                    i32.add,
                    i32.const(52),
                    i32.rotl,
                    f32.reinterpret_i32,
                ];

            return [

                locals.arg.get,
                type.floor,
                locals.floor.tee,

                ...typeSpec,

                locals.arg.get,
                locals.floor.get,
                type.sub,
                pow201.call,

                type.mul,

            ];
        });


    const log2 = func()
        .name(`${functionPrefix}log2`)
        .param("arg", type)
        .local("intI", type)
        .local("x", type)
        .result(type)
        .body((locals) => {
            const intI = type.type.typeTitle === "f64"
                ? [
                    i64.reinterpret_f64,
                    i64.const(0b0111111111110000000000000000000000000000000000000000000000000000n),
                    i64.and,
                    i64.const(52),
                    i64.rotr,
                    i64.const(1023),
                    i64.sub,
                    type.convert_i64_s,
                ]
                : [
                    i32.reinterpret_f32,
                    i32.const(0b01111111100000000000000000000000),
                    i32.and,
                    i32.const(23),
                    i32.rotr,
                    i32.const(127),
                    i32.sub,
                    type.convert_i32_s,
                ];
            const fX = type.type.typeTitle === "f64"
                ? [
                    i64.reinterpret_f64,
                    i64.const(0b0000000000001111111111111111111111111111111111111111111111111111n),
                    i64.and,
                    i64.const(0b0011111111110000000000000000000000000000000000000000000000000000n),
                    i64.or,
                    f64.reinterpret_i64,
                ]
                : [
                    i32.reinterpret_f32,
                    i32.const(0b00000000011111111111111111111111),
                    i32.and,
                    i32.const(0b00111111100000000000000000000000),
                    i32.or,
                    f32.reinterpret_i32,
                ];

            return [
                locals.arg.get,
                ...intI,
                locals.intI.set,

                locals.arg.get,
                ...fX,
                locals.x.set,

                polynomialN(
                    locals.x,
                    [
                        /* Err: 3.2808e-7 */
                        0.015221056507073172,
                        -0.18512172976289776,
                        0.9841668689450671,
                        -3.001913413877105,
                        5.809013198388698,
                        -7.520598352712341,
                        7.147825739646123,
                        -3.24859336702184,
                    ],
                ),

                locals.intI.get,
                type.add,

            ];
        });

    function createLog(
        base: number,
    ): FullyDefinedFunction<string, [T["type"]]> {
        return func()
            .name(`${functionPrefix}log_${base}`)
            .param("arg", type)
            .result(type)
            .body((locals) => [
                type.mul(
                    log2.call(locals.arg.get),
                    type.const(1 / Math.log2(base)),
                ),
            ]);
    }

    const logCache = new Map<number, FullyDefinedFunction<string, [T["type"]]>>();

    function log(
        base: number,
    ): FullyDefinedFunction<string, [T["type"]]> {
        return getOrPut(logCache, base, () => createLog(base));
    }


    const pow = func()
        .name(`${functionPrefix}pow`)
        .param("base", type)
        .param("power", type)
        .result(type)
        .body((locals) => {
            return [
                locals.base.get,
                log2.call,
                locals.power.get,
                type.mul,
                pow2.call,
            ];
        });


    const replaceNaN = func()
        .name(`${functionPrefix}replaceNaN`)
        .param("x", type)
        .param("default", type)
        .result(type)
        .body((locals) => [
            locals.x.get,
            isNaN.call,
            control.if({
                type,
                then: [
                    locals.default.get,
                ],
                else: [
                    locals.x.get,
                ],
            }),
        ]);
    return {
        sin,
        cos,
        asin,
        tan,
        pow,
        pow2,
        powI32,
        pow201,
        log2,
        log10: log(10),
        logE: log(Math.E),
        exp01,
        exp,
        log,
        delayToi32,
        isNaN,
        sign,
        polynomial,
        mod,
        heaviside,
        replaceNaN,
    };
}

