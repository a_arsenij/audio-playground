import { FullyDefinedFunction, func } from "../Op";
import { i32, i64 } from "../ops";
import { I32, I64, WAType } from "../Types";


type T = typeof i32 | typeof i64;

type RESULT<FT extends WAType> = {
    rem: FullyDefinedFunction<string, [FT]>,

}

export function createIntegerFunctions(type: typeof i64): RESULT<I64>
export function createIntegerFunctions(type: typeof i32): RESULT<I32>

export function createIntegerFunctions(type: T): RESULT<I32> | RESULT<I64> | RESULT<I32 | I64> {
    const functionPrefix = `${type.type.typeTitle}_`;

    const rem = func()
        .name(`${functionPrefix}rem`)
        .param("a", type)
        .param("b", type)
        .result(type)
        .body((locals) => [
            locals.a.get,
            locals.b.get,
            type.rem_s,
            locals.b.get,
            type.add,
            locals.b.get,
            type.rem_s,
        ]);

    return {
        rem,
    };
}

