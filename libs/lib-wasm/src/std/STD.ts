
import { f32, f64, i32, i64 } from "../ops";
import {
    createFloatFunctions,
} from "./floatFunction";
import { createIntegerFunctions } from "./integerFunction";

export const std = {
    i64: createIntegerFunctions(i64),
    i32: createIntegerFunctions(i32),
    f64: createFloatFunctions(f64),
    f32: createFloatFunctions(f32),
};
