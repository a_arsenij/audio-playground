import { TupleOf } from "@audio-playground/lib-common/types/Array";

function uintToBits<T extends number | bigint>(
    value: T,
): number[] {
    let tmp = value;
    const result: number[] = [];
    const [_2, _1] = (typeof value === "number") ? [2, 1] : [2n, 1n];
    while (tmp > 0) {
        // @ts-ignore
        result.push(Number(tmp % _2));
        // @ts-ignore
        tmp >>= _1;
    }
    return result;
}

export function uintToLeb128(
    value: number | bigint,
): number[] {

    if (value === 0) {
        return [0];
    }

    const bits = uintToBits(value);
    while (bits.length % 7 !== 0) {
        bits.push(0);
    }
    const bytes: number[] = [];
    const bytesNum = Math.ceil(bits.length / 7);
    for (let i = 0; i < bytesNum; i++) {
        const byteBits = bits.slice(
            i * 7,
            (i + 1) * 7,
        );
        byteBits.reverse();
        let byte = 0;
        for (const bit of byteBits) {
            byte *= 2;
            byte |= bit;
        }
        if (i + 1 !== bytesNum) {
            byte |= 128;
        }
        bytes.push(byte);
    }
    if (bytes.length > 10) {
        throw new Error("Invalid state");
    }
    return bytes;

}

const a: TupleOf<5, number> = [-1, 0, 7, 64, 127];
const b = a.map((n) => BigInt(n)) as TupleOf<5, bigint>;

export function sintToLeb128(value: number | bigint): number[] {

    const [__1, _0, _7, _64, _127] = typeof value === "number" ? a : b;
    // @ts-ignore
    value |= _0;

    const result = [];
    while (true) {
        // @ts-ignore
        const byte_ = value & _127;
        // @ts-ignore
        value = value >> _7;
        // @ts-ignore
        const myByte = byte_ & _64;
        if (
            ((value === _0) && (myByte === _0)) ||
            ((value === __1) && (myByte !== _0))
        ) {
            result.push(Number(byte_));
            return result;
        }
        result.push(Number(byte_) | 128);
    }
}

