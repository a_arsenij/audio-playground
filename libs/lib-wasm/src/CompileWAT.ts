import { collectUsedFunctions } from "./CollectUsedFunctions";
import { Module } from "./Op";
import { OP } from "./ops/OpsTypes";


export function compileModuleToWAT(
    module: Module,
): string {

    const res: string[] = [];
    function write(
        data: string,
    ) {
        res.push(data);
    }

    write("(module\n\n");

    if (module.memory) {
        write(
            `;; Memory: ${module.memory.sizePages} page(s)\n;;         64KB/page\n;; Total : ${64 * module.memory.sizePages}KB\n(import "${module.memory.path[0]}" "${module.memory.path[1]}" (memory ${module.memory.sizePages}))\n\n`);
    }

    if (module.imports.length > 0) {
        write(";; Imports of external functions\n");
        module.imports.forEach((i) => {
            write(`(import "${i.path[0]}" "${i.path[1]}" (func\n    $${i.functionName}\n`);
            if (i.params.length > 0) {
                write(`    (param ${i.params.map((p) => p.type.typeTitle).join(" ")})\n`);
            }
            if (i.result.length > 0) {
                write(`    (result ${i.result.map((r) => r.typeTitle).join(" ")})\n`);
            }
            write("))\n");
        });
        write("\n");
    }

    if (module.globals.length > 0) {
        write(";; Global variables\n");
        module.globals.forEach((g) => {
            write(`(global $${g.globalName} (import "${g.path[0]}" "${g.path[1]}") (${g.mutable ? "mut " : ""}${g.type.typeTitle}))\n`);
        });
        write("\n");
    }

    const functions = collectUsedFunctions(module.functions, 0);
    if (functions.size > 0) {
        write(";; Functions\n");
        functions.forEach((_, f) => {

            write(`(func $${f.functionName} `);
            if (f.isExported) {
                write(`(export "${f.functionName}")`);
            }
            write("\n");
            f.params.forEach((p) => {
                write(`    (param $${p.localName} ${p.type.typeTitle})\n`);
            });
            if (f.result.length > 0) {
                write(`    (result ${f.result.map((v) => v.typeTitle).join(" ")})\n`);
            }
            f.locals.forEach((p) => {
                write(`    (local $${p.localName} ${p.type.typeTitle})\n`);
            });

            let offset = 1;
            function writeOffset() {
                write([...Array(offset * 4)].map(() => " ").join(""));
            }
            function writeOp(op: OP) {
                if (op === undefined) {
                    throw new Error("??");
                }
                if ("args" in op) {
                    op.args.forEach((o) =>
                        writeOp(o),
                    );
                }
                if ("local" in op) {
                    writeOffset();
                    write(`${op.title} $${op.local.localName}\n`);
                } else if ("global" in op) {
                    writeOffset();
                    write(`${op.title} $${op.global.globalName}\n`);
                } else if ("value" in op) {
                    writeOffset();
                    write(`${op.title} ${op.value}\n`);
                } else if ("offset" in op) {
                    writeOffset();
                    const align = op.align ?? 0;
                    write(`${op.title} offset=${op.offset} ${align === 0 ? "" : `align=${2 ** align}`}\n`);
                } else if ("function" in op) {
                    writeOffset();
                    write(`${op.title} $${op.function.functionName}\n`);
                } else if ("label" in op) {
                    writeOffset();
                    write(`(${op.title} $${op.label.name}`);
                    if (op.type.typeTitle !== "void") {
                        write(` (result ${op.type.typeTitle})`);
                    }
                    write("\n");
                    offset++;
                    op.ops.forEach((oo) => writeOp(oo));
                    offset--;
                    writeOffset();
                    write(")\n");
                } else if ("then" in op) {
                    writeOffset();
                    write("(if");
                    if (op.type.typeTitle !== "void") {
                        write(` (result ${op.type.typeTitle})`);
                    }
                    write("\n");
                    offset++;
                    writeOffset();
                    write("(then\n");
                    offset++;
                    op.then.forEach((oo) => writeOp(oo));
                    offset--;
                    writeOffset();
                    write(")\n");

                    if (op.else) {
                        writeOffset();
                        write("(else\n");
                        offset++;
                        op.else.forEach((oo) => writeOp(oo));
                        offset--;
                        writeOffset();
                        write(")\n");
                    }

                    offset--;
                    writeOffset();
                    write(")\n");
                } else if ("goTo" in op) {
                    writeOffset();
                    write(`${op.title} $${op.goTo.name}\n`);
                } else {
                    writeOffset();
                    write(`${op.title}\n`);
                }
            }
            f.ops.forEach((op) => {
                writeOp(op);
            });

            write(")\n");
        });
        write("\n");
    }

    write(")\n");

    return res.join("");

}
