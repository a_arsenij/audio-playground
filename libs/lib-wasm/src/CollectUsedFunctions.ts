import { FullyDefinedFunction } from "./Op";
import { OP } from "./ops/OpsTypes";
import { WAType } from "./Types";


export function collectUsedFunctions(
    functions: FullyDefinedFunction<string, WAType[]>[],
    offset: number,
): Map<FullyDefinedFunction<string, WAType[]>, number> {

    const result = new Map<FullyDefinedFunction<string, WAType[]>, number>();
    function visitOp(op: OP) {
        if ("args" in op) {
            visitOps(op.args);
        }
        if ("function" in op) {
            if ("ops" in op.function) {
                visitFunc(op.function);
            }
        }
        if ("label" in op) {
            visitOps(op.ops);
        }
        if ("then" in op) {
            visitOps(op.then);
            visitOps(op.else || []);
        }
    }
    function visitOps(ops: OP[]) {
        ops.forEach(visitOp);
    }
    function visitFunc(f: FullyDefinedFunction<string, WAType[]>) {
        if (result.has(f)) {
            return;
        }
        result.set(f, result.size);
        visitOps(f.ops);
    }
    functions
        .filter((f) => f.isExported)
        .forEach(visitFunc);
    return new Map(
        [...result.keys()].reverse().map((f, idx) => [f, idx + offset]),
    );
}

