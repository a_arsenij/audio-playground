export type HasType = WAType | { type: WAType }
export type GetType<T extends HasType> = (
    T extends { type: WAType }
        ? T["type"]
        : T
    );
export function getType<T extends HasType>(has: T): GetType<T> {
    if ("type" in has) {
        // @ts-ignore
        return has.type;
    } else {
        // @ts-ignore
        return has;
    }
}
export type GetTypes<
    T extends HasType[],
    ACC extends WAType[] = [],
> = ACC["length"] extends T["length"]
    ? ACC
    : GetTypes<
        T,
        [...ACC, GetType<T[ACC["length"]]>]
    >

export type I32 = {
    typeTitle: "i32",
    typeCode: 0x7f,
}

export const i32Type: I32 = {
    typeTitle: "i32",
    typeCode: 0x7f,
};

export type I64 = {
    typeTitle: "i64",
    typeCode: 0x7e,
}

export const i64Type: I64 = {
    typeTitle: "i64",
    typeCode: 0x7e,
};

export type F32 = {
    typeTitle: "f32",
    typeCode: 0x7d,
}

export const f32Type: F32 = {
    typeTitle: "f32",
    typeCode: 0x7d,
};


export type F64 = {
    typeTitle: "f64",
    typeCode: 0x7c,
}

export const f64Type: F64 = {
    typeTitle: "f64",
    typeCode: 0x7c,
};

export type V128 = {
    typeTitle: "v128",
    typeCode: 0x7B,
}

export const v128Type: V128 = {
    typeTitle: "v128",
    typeCode: 0x7B,
};

export type VoidType = {
    typeTitle: "void",
    typeCode: 0x40,
}

export const voidType: VoidType = {
    typeTitle: "void",
    typeCode: 0x40,
};

export type WAType =
    | I32
    | I64
    | F32
    | F64
    | V128
    | VoidType
    ;
