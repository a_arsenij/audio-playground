import { ArguableOp, BasicOpRoot, OP } from "./OpsTypes";


export function createSetArgs<const T extends BasicOpRoot>(t: T): T & ArguableOp {
    const tt = (...args: OP[]) => {
        // @ts-ignore
        const ttt = { ...t };
        Object.assign(ttt, { args });
        return ttt;
    };
    Object.assign(tt, t);
    // @ts-ignore
    return tt;
}
