

import { createSetArgs } from "../CreateSetArgs";

export const i64x2BasicOps = {
    splat: createSetArgs({
        title: "i64x2.splat",
        opcode: 0xFD_12,
    }),
    extract_lane: (laneidx: number) => createSetArgs({
        title: "i64x2.extract_lane",
        opcode: 0xFD_1D,
        laneidx,
    }),
    replace_lane: (laneidx: number) => createSetArgs({
        title: "i64x2.replace_lane",
        opcode: 0xFD_1E,
        laneidx,
    }),
    abs: createSetArgs({
        title: "i64x2.abs",
        opcode: 0xFD_C0_01,
    }),
    neg: createSetArgs({
        title: "i64x2.neg",
        opcode: 0xFD_C1_01,
    }),
    all_true: createSetArgs({
        title: "i64x2.all_true",
        opcode: 0xFD_C3_01,
    }),
    bitmask: createSetArgs({
        title: "i64x2.bitmask",
        opcode: 0xFD_C4_01,
    }),
    extend_low_i32x4_s: createSetArgs({
        title: "i64x2.extend_low_i32x4_s",
        opcode: 0xFD_C7_01,
    }),
    extend_high_i32x4_s: createSetArgs({
        title: "i64x2.extend_high_i32x4_s",
        opcode: 0xFD_C8_01,
    }),
    extend_low_i32x4_u: createSetArgs({
        title: "i64x2.extend_low_i32x4_u",
        opcode: 0xFD_C9_01,
    }),
    extend_high_i32x4_u: createSetArgs({
        title: "i64x2.extend_high_i32x4_u",
        opcode: 0xFD_CA_01,
    }),
    shl: createSetArgs({
        title: "i64x2.shl",
        opcode: 0xFD_CB_01,
    }),
    shr_s: createSetArgs({
        title: "i64x2.shr_s",
        opcode: 0xFD_CC_01,
    }),
    shr_u: createSetArgs({
        title: "i64x2.shr_u",
        opcode: 0xFD_CD_01,
    }),
    add: createSetArgs({
        title: "i64x2.add",
        opcode: 0xFD_CE_01,
    }),
    sub: createSetArgs({
        title: "i64x2.sub",
        opcode: 0xFD_D1_01,
    }),
    mul: createSetArgs({
        title: "i64x2.mul",
        opcode: 0xFD_D5_01,
    }),
    eq: createSetArgs({
        title: "i64x2.eq",
        opcode: 0xFD_D6_01,
    }),
    ne: createSetArgs({
        title: "i64x2.ne",
        opcode: 0xFD_D7_01,
    }),
    lt_s: createSetArgs({
        title: "i64x2.lt_s",
        opcode: 0xFD_D8_01,
    }),
    gt_s: createSetArgs({
        title: "i64x2.gt_s",
        opcode: 0xFD_D9_01,
    }),
    le_s: createSetArgs({
        title: "i64x2.le_s",
        opcode: 0xFD_DA_01,
    }),
    ge_s: createSetArgs({
        title: "i64x2.ge_s",
        opcode: 0xFD_DB_01,
    }),
    extmul_low_i32x4_s: createSetArgs({
        title: "i64x2.extmul_low_i32x4_s",
        opcode: 0xFD_DC_01,
    }),
    extmul_high_i32x4_s: createSetArgs({
        title: "i64x2.extmul_high_i32x4_s",
        opcode: 0xFD_DD_01,
    }),
    extmul_low_i32x4_u: createSetArgs({
        title: "i64x2.extmul_low_i32x4_u",
        opcode: 0xFD_DE_01,
    }),
    extmul_high_i32x4_u: createSetArgs({
        title: "i64x2.extmul_high_i32x4_u",
        opcode: 0xFD_DF_01,
    }),
};
