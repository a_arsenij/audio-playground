import { FunctionRef, LabelRef } from "../../Op";
import { getType, HasType, WAType } from "../../Types";
import { randomName } from "../../utils/RandomWasmName";
import { createSetArgs } from "../CreateSetArgs";
import { BasicOp, BreakableOp, BreakOp, CallOp, IfOp, OP } from "../OpsTypes";

export const controlOps = {
    /*
     * TODO:
     * memory.init data.drop memory.copy memory.fill
     */
    call(
        func: FunctionRef<string, WAType[]>,
    ): CallOp {
        return createSetArgs({
            title: "call",
            opcode: 0x10,
            function: func,
        });
    },
    if(arg: {
        type: HasType,
        then: OP[],
        else?: OP[],
    }): IfOp {
        return createSetArgs({
            title: "if",
            type: getType(arg.type),
            opcode: 0x04,
            then: arg.then,
            else: arg.else,
        });
    },
    br(label: LabelRef): BreakOp {
        return {
            title: "br",
            opcode: 0x0c,
            goTo: label,
            args: [],
        };
    },
    br_if(label: LabelRef): BreakOp {
        return createSetArgs({
            title: "br_if",
            opcode: 0x0d,
            goTo: label,
        });
    },
    block(type: WAType, createOps: (label: LabelRef) => OP[]): BreakableOp {
        const label: LabelRef = {
            name: `label_${randomName()}`,
        };
        const ops = createOps(label);
        return {
            title: "block",
            type,
            opcode: 0x02,
            ops,
            label,
            args: [],
        };
    },
    loop(type: WAType, createOps: (label: LabelRef) => OP[]): BreakableOp {
        const label: LabelRef = {
            name: `label_${randomName()}`,
        };
        const ops = createOps(label);
        return {
            title: "loop",
            type,
            opcode: 0x03,
            ops,
            label,
            args: [],
        };
    },
    unreachable: {
        title: "unreachable",
        opcode: 0x00,
    },
    return: {
        title: "return",
        opcode: 0x0f,
    },
    drop: {
        title: "drop",
        opcode: 0x1a,
    },
    select: {
        title: "select",
        opcode: 0x1b,
        args: [],
    } as BasicOp,
} as const;
