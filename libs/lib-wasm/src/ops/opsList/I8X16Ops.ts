

import { createSetArgs } from "../CreateSetArgs";

export const i8x16BasicOps = {
    shuffle: (laneidx: [number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number]) => createSetArgs({
        title: "i8x16.shuffle",
        opcode: 0xFD_0D,
        laneidx,
    }),
    swizzle: createSetArgs({
        title: "i8x16.swizzle",
        opcode: 0xFD_0E,
    }),
    splat: createSetArgs({
        title: "i8x16.splat",
        opcode: 0xFD_0F,
    }),
    extract_lane_s: (laneidx: number) => createSetArgs({
        title: "i8x16.extract_lane_s",
        opcode: 0xFD_15,
        laneidx,
    }),
    extract_lane_u: (laneidx: number) => createSetArgs({
        title: "i8x16.extract_lane_u",
        opcode: 0xFD_16,
        laneidx,
    }),
    replace_lane: (laneidx: number) => createSetArgs({
        title: "i8x16.replace_lane",
        opcode: 0xFD_17,
        laneidx,
    }),
    eq: createSetArgs({
        title: "i8x16.eq",
        opcode: 0xFD_23,
    }),
    ne: createSetArgs({
        title: "i8x16.ne",
        opcode: 0xFD_24,
    }),
    lt_s: createSetArgs({
        title: "i8x16.lt_s",
        opcode: 0xFD_25,
    }),
    lt_u: createSetArgs({
        title: "i8x16.lt_u",
        opcode: 0xFD_26,
    }),
    gt_s: createSetArgs({
        title: "i8x16.gt_s",
        opcode: 0xFD_27,
    }),
    gt_u: createSetArgs({
        title: "i8x16.gt_u",
        opcode: 0xFD_28,
    }),
    le_s: createSetArgs({
        title: "i8x16.le_s",
        opcode: 0xFD_29,
    }),
    le_u: createSetArgs({
        title: "i8x16.le_u",
        opcode: 0xFD_2A,
    }),
    ge_s: createSetArgs({
        title: "i8x16.ge_s",
        opcode: 0xFD_2B,
    }),
    ge_u: createSetArgs({
        title: "i8x16.ge_u",
        opcode: 0xFD_2C,
    }),
    abs: createSetArgs({
        title: "i8x16.abs",
        opcode: 0xFD_60,
    }),
    neg: createSetArgs({
        title: "i8x16.neg",
        opcode: 0xFD_61,
    }),
    popcnt: createSetArgs({
        title: "i8x16.popcnt",
        opcode: 0xFD_62,
    }),
    all_true: createSetArgs({
        title: "i8x16.all_true",
        opcode: 0xFD_63,
    }),
    bitmask: createSetArgs({
        title: "i8x16.bitmask",
        opcode: 0xFD_64,
    }),
    narrow_i16x8_s: createSetArgs({
        title: "i8x16.narrow_i16x8_s",
        opcode: 0xFD_65,
    }),
    narrow_i16x8_u: createSetArgs({
        title: "i8x16.narrow_i16x8_u",
        opcode: 0xFD_66,
    }),
    shl: createSetArgs({
        title: "i8x16.shl",
        opcode: 0xFD_6B,
    }),
    shr_s: createSetArgs({
        title: "i8x16.shr_s",
        opcode: 0xFD_6C,
    }),
    shr_u: createSetArgs({
        title: "i8x16.shr_u",
        opcode: 0xFD_6D,
    }),
    add: createSetArgs({
        title: "i8x16.add",
        opcode: 0xFD_6E,
    }),
    add_sat_s: createSetArgs({
        title: "i8x16.add_sat_s",
        opcode: 0xFD_6F,
    }),
    add_sat_u: createSetArgs({
        title: "i8x16.add_sat_u",
        opcode: 0xFD_70,
    }),
    sub: createSetArgs({
        title: "i8x16.sub",
        opcode: 0xFD_71,
    }),
    sub_sat_s: createSetArgs({
        title: "i8x16.sub_sat_s",
        opcode: 0xFD_72,
    }),
    sub_sat_u: createSetArgs({
        title: "i8x16.sub_sat_u",
        opcode: 0xFD_73,
    }),
    min_s: createSetArgs({
        title: "i8x16.min_s",
        opcode: 0xFD_76,
    }),
    min_u: createSetArgs({
        title: "i8x16.min_u",
        opcode: 0xFD_77,
    }),
    max_s: createSetArgs({
        title: "i8x16.max_s",
        opcode: 0xFD_78,
    }),
    max_u: createSetArgs({
        title: "i8x16.max_u",
        opcode: 0xFD_79,
    }),
    avgr_u: createSetArgs({
        title: "i8x16.avgr_u",
        opcode: 0xFD_7B,
    }),
};
