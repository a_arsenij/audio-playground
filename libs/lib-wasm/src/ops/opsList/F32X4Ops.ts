

import { createSetArgs } from "../CreateSetArgs";

export const f32x4BasicOps = {
    splat: createSetArgs({
        title: "f32x4.splat",
        opcode: 0xFD_13,
    }),
    extract_lane: (laneidx: number) => createSetArgs({
        title: "f32x4.extract_lane",
        opcode: 0xFD_1F,
        laneidx,
    }),
    replace_lane: (laneidx: number) => createSetArgs({
        title: "f32x4.replace_lane",
        opcode: 0xFD_20,
        laneidx,
    }),
    eq: createSetArgs({
        title: "f32x4.eq",
        opcode: 0xFD_41,
    }),
    ne: createSetArgs({
        title: "f32x4.ne",
        opcode: 0xFD_42,
    }),
    lt: createSetArgs({
        title: "f32x4.lt",
        opcode: 0xFD_43,
    }),
    gt: createSetArgs({
        title: "f32x4.gt",
        opcode: 0xFD_44,
    }),
    le: createSetArgs({
        title: "f32x4.le",
        opcode: 0xFD_45,
    }),
    ge: createSetArgs({
        title: "f32x4.ge",
        opcode: 0xFD_46,
    }),
    demote_f64x2_zero: createSetArgs({
        title: "f32x4.demote_f64x2_zero",
        opcode: 0xFD_5E,
    }),
    ceil: createSetArgs({
        title: "f32x4.ceil",
        opcode: 0xFD_67,
    }),
    floor: createSetArgs({
        title: "f32x4.floor",
        opcode: 0xFD_68,
    }),
    trunc: createSetArgs({
        title: "f32x4.trunc",
        opcode: 0xFD_69,
    }),
    nearest: createSetArgs({
        title: "f32x4.nearest",
        opcode: 0xFD_6A,
    }),
    abs: createSetArgs({
        title: "f32x4.abs",
        opcode: 0xFD_E0_01,
    }),
    neg: createSetArgs({
        title: "f32x4.neg",
        opcode: 0xFD_E1_01,
    }),
    sqrt: createSetArgs({
        title: "f32x4.sqrt",
        opcode: 0xFD_E3_01,
    }),
    add: createSetArgs({
        title: "f32x4.add",
        opcode: 0xFD_E4_01,
    }),
    sub: createSetArgs({
        title: "f32x4.sub",
        opcode: 0xFD_E5_01,
    }),
    mul: createSetArgs({
        title: "f32x4.mul",
        opcode: 0xFD_E6_01,
    }),
    div: createSetArgs({
        title: "f32x4.div",
        opcode: 0xFD_E7_01,
    }),
    min: createSetArgs({
        title: "f32x4.min",
        opcode: 0xFD_E8_01,
    }),
    max: createSetArgs({
        title: "f32x4.max",
        opcode: 0xFD_E9_01,
    }),
    pmin: createSetArgs({
        title: "f32x4.pmin",
        opcode: 0xFD_EA_01,
    }),
    pmax: createSetArgs({
        title: "f32x4.pmax",
        opcode: 0xFD_EB_01,
    }),
    convert_i32x4_s: createSetArgs({
        title: "f32x4.convert_i32x4_s",
        opcode: 0xFD_FA_01,
    }),
    convert_i32x4_u: createSetArgs({
        title: "f32x4.convert_i32x4_u",
        opcode: 0xFD_FB_01,
    }),
};
