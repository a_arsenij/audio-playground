
import { f32Type } from "../../Types";
import { createSetArgs } from "../CreateSetArgs";

export const f32BasicOps = {
    load: (offset: number, align?: number) => createSetArgs({
        title: "f32.load",
        opcode: 0x2A,
        offset,
        align,
    }),
    store: (offset: number, align?: number) => createSetArgs({
        title: "f32.store",
        opcode: 0x38,
        offset,
        align,
    }),
    const: (v: number | bigint) => ({
        title: "f32.const",
        opcode: 0x43,
        value: v,
        type: f32Type,
        args: [],
    }),
    eq: createSetArgs({
        title: "f32.eq",
        opcode: 0x5B,
    }),
    ne: createSetArgs({
        title: "f32.ne",
        opcode: 0x5C,
    }),
    lt: createSetArgs({
        title: "f32.lt",
        opcode: 0x5D,
    }),
    gt: createSetArgs({
        title: "f32.gt",
        opcode: 0x5E,
    }),
    le: createSetArgs({
        title: "f32.le",
        opcode: 0x5F,
    }),
    ge: createSetArgs({
        title: "f32.ge",
        opcode: 0x60,
    }),
    abs: createSetArgs({
        title: "f32.abs",
        opcode: 0x8B,
    }),
    neg: createSetArgs({
        title: "f32.neg",
        opcode: 0x8C,
    }),
    ceil: createSetArgs({
        title: "f32.ceil",
        opcode: 0x8D,
    }),
    floor: createSetArgs({
        title: "f32.floor",
        opcode: 0x8E,
    }),
    trunc: createSetArgs({
        title: "f32.trunc",
        opcode: 0x8F,
    }),
    nearest: createSetArgs({
        title: "f32.nearest",
        opcode: 0x90,
    }),
    sqrt: createSetArgs({
        title: "f32.sqrt",
        opcode: 0x91,
    }),
    add: createSetArgs({
        title: "f32.add",
        opcode: 0x92,
    }),
    sub: createSetArgs({
        title: "f32.sub",
        opcode: 0x93,
    }),
    mul: createSetArgs({
        title: "f32.mul",
        opcode: 0x94,
    }),
    div: createSetArgs({
        title: "f32.div",
        opcode: 0x95,
    }),
    min: createSetArgs({
        title: "f32.min",
        opcode: 0x96,
    }),
    max: createSetArgs({
        title: "f32.max",
        opcode: 0x97,
    }),
    copysign: createSetArgs({
        title: "f32.copysign",
        opcode: 0x98,
    }),
    convert_i32_s: createSetArgs({
        title: "f32.convert_i32_s",
        opcode: 0xB2,
    }),
    convert_i32_u: createSetArgs({
        title: "f32.convert_i32_u",
        opcode: 0xB3,
    }),
    convert_i64_s: createSetArgs({
        title: "f32.convert_i64_s",
        opcode: 0xB4,
    }),
    convert_i64_u: createSetArgs({
        title: "f32.convert_i64_u",
        opcode: 0xB5,
    }),
    demote_f64: createSetArgs({
        title: "f32.demote_f64",
        opcode: 0xB6,
    }),
    reinterpret_i32: createSetArgs({
        title: "f32.reinterpret_i32",
        opcode: 0xBE,
    }),
};
