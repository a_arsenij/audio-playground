
import { f64Type } from "../../Types";
import { createSetArgs } from "../CreateSetArgs";

export const f64BasicOps = {
    load: (offset: number, align?: number) => createSetArgs({
        title: "f64.load",
        opcode: 0x2B,
        offset,
        align,
    }),
    store: (offset: number, align?: number) => createSetArgs({
        title: "f64.store",
        opcode: 0x39,
        offset,
        align,
    }),
    const: (v: number | bigint) => ({
        title: "f64.const",
        opcode: 0x44,
        value: v,
        type: f64Type,
        args: [],
    }),
    eq: createSetArgs({
        title: "f64.eq",
        opcode: 0x61,
    }),
    ne: createSetArgs({
        title: "f64.ne",
        opcode: 0x62,
    }),
    lt: createSetArgs({
        title: "f64.lt",
        opcode: 0x63,
    }),
    gt: createSetArgs({
        title: "f64.gt",
        opcode: 0x64,
    }),
    le: createSetArgs({
        title: "f64.le",
        opcode: 0x65,
    }),
    ge: createSetArgs({
        title: "f64.ge",
        opcode: 0x66,
    }),
    abs: createSetArgs({
        title: "f64.abs",
        opcode: 0x99,
    }),
    neg: createSetArgs({
        title: "f64.neg",
        opcode: 0x9A,
    }),
    ceil: createSetArgs({
        title: "f64.ceil",
        opcode: 0x9B,
    }),
    floor: createSetArgs({
        title: "f64.floor",
        opcode: 0x9C,
    }),
    trunc: createSetArgs({
        title: "f64.trunc",
        opcode: 0x9D,
    }),
    nearest: createSetArgs({
        title: "f64.nearest",
        opcode: 0x9E,
    }),
    sqrt: createSetArgs({
        title: "f64.sqrt",
        opcode: 0x9F,
    }),
    add: createSetArgs({
        title: "f64.add",
        opcode: 0xA0,
    }),
    sub: createSetArgs({
        title: "f64.sub",
        opcode: 0xA1,
    }),
    mul: createSetArgs({
        title: "f64.mul",
        opcode: 0xA2,
    }),
    div: createSetArgs({
        title: "f64.div",
        opcode: 0xA3,
    }),
    min: createSetArgs({
        title: "f64.min",
        opcode: 0xA4,
    }),
    max: createSetArgs({
        title: "f64.max",
        opcode: 0xA5,
    }),
    copysign: createSetArgs({
        title: "f64.copysign",
        opcode: 0xA6,
    }),
    convert_i32_s: createSetArgs({
        title: "f64.convert_i32_s",
        opcode: 0xB7,
    }),
    convert_i32_u: createSetArgs({
        title: "f64.convert_i32_u",
        opcode: 0xB8,
    }),
    convert_i64_s: createSetArgs({
        title: "f64.convert_i64_s",
        opcode: 0xB9,
    }),
    convert_i64_u: createSetArgs({
        title: "f64.convert_i64_u",
        opcode: 0xBA,
    }),
    promote_f32: createSetArgs({
        title: "f64.promote_f32",
        opcode: 0xBB,
    }),
    reinterpret_i64: createSetArgs({
        title: "f64.reinterpret_i64",
        opcode: 0xBF,
    }),
};
