
import { i32Type } from "../../Types";
import { createSetArgs } from "../CreateSetArgs";

export const i32BasicOps = {
    load: (offset: number, align?: number) => createSetArgs({
        title: "i32.load",
        opcode: 0x28,
        offset,
        align,
    }),
    load8_s: (offset: number, align?: number) => createSetArgs({
        title: "i32.load8_s",
        opcode: 0x2C,
        offset,
        align,
    }),
    load8_u: (offset: number, align?: number) => createSetArgs({
        title: "i32.load8_u",
        opcode: 0x2D,
        offset,
        align,
    }),
    load16_s: (offset: number, align?: number) => createSetArgs({
        title: "i32.load16_s",
        opcode: 0x2E,
        offset,
        align,
    }),
    load16_u: (offset: number, align?: number) => createSetArgs({
        title: "i32.load16_u",
        opcode: 0x2F,
        offset,
        align,
    }),
    store: (offset: number, align?: number) => createSetArgs({
        title: "i32.store",
        opcode: 0x36,
        offset,
        align,
    }),
    store8: (offset: number, align?: number) => createSetArgs({
        title: "i32.store8",
        opcode: 0x3A,
        offset,
        align,
    }),
    store16: (offset: number, align?: number) => createSetArgs({
        title: "i32.store16",
        opcode: 0x3B,
        offset,
        align,
    }),
    const: (v: number | bigint) => ({
        title: "i32.const",
        opcode: 0x41,
        value: v,
        type: i32Type,
        args: [],
    }),
    eqz: createSetArgs({
        title: "i32.eqz",
        opcode: 0x45,
    }),
    eq: createSetArgs({
        title: "i32.eq",
        opcode: 0x46,
    }),
    ne: createSetArgs({
        title: "i32.ne",
        opcode: 0x47,
    }),
    lt_s: createSetArgs({
        title: "i32.lt_s",
        opcode: 0x48,
    }),
    lt_u: createSetArgs({
        title: "i32.lt_u",
        opcode: 0x49,
    }),
    gt_s: createSetArgs({
        title: "i32.gt_s",
        opcode: 0x4A,
    }),
    gt_u: createSetArgs({
        title: "i32.gt_u",
        opcode: 0x4B,
    }),
    le_s: createSetArgs({
        title: "i32.le_s",
        opcode: 0x4C,
    }),
    le_u: createSetArgs({
        title: "i32.le_u",
        opcode: 0x4D,
    }),
    ge_s: createSetArgs({
        title: "i32.ge_s",
        opcode: 0x4E,
    }),
    ge_u: createSetArgs({
        title: "i32.ge_u",
        opcode: 0x4F,
    }),
    clz: createSetArgs({
        title: "i32.clz",
        opcode: 0x67,
    }),
    ctz: createSetArgs({
        title: "i32.ctz",
        opcode: 0x68,
    }),
    popcnt: createSetArgs({
        title: "i32.popcnt",
        opcode: 0x69,
    }),
    add: createSetArgs({
        title: "i32.add",
        opcode: 0x6A,
    }),
    sub: createSetArgs({
        title: "i32.sub",
        opcode: 0x6B,
    }),
    mul: createSetArgs({
        title: "i32.mul",
        opcode: 0x6C,
    }),
    div_s: createSetArgs({
        title: "i32.div_s",
        opcode: 0x6D,
    }),
    div_u: createSetArgs({
        title: "i32.div_u",
        opcode: 0x6E,
    }),
    rem_s: createSetArgs({
        title: "i32.rem_s",
        opcode: 0x6F,
    }),
    rem_u: createSetArgs({
        title: "i32.rem_u",
        opcode: 0x70,
    }),
    and: createSetArgs({
        title: "i32.and",
        opcode: 0x71,
    }),
    or: createSetArgs({
        title: "i32.or",
        opcode: 0x72,
    }),
    xor: createSetArgs({
        title: "i32.xor",
        opcode: 0x73,
    }),
    shl: createSetArgs({
        title: "i32.shl",
        opcode: 0x74,
    }),
    shr_s: createSetArgs({
        title: "i32.shr_s",
        opcode: 0x75,
    }),
    shr_u: createSetArgs({
        title: "i32.shr_u",
        opcode: 0x76,
    }),
    rotl: createSetArgs({
        title: "i32.rotl",
        opcode: 0x77,
    }),
    rotr: createSetArgs({
        title: "i32.rotr",
        opcode: 0x78,
    }),
    wrap_i64: createSetArgs({
        title: "i32.wrap_i64",
        opcode: 0xA7,
    }),
    trunc_f32_s: createSetArgs({
        title: "i32.trunc_f32_s",
        opcode: 0xA8,
    }),
    trunc_f32_u: createSetArgs({
        title: "i32.trunc_f32_u",
        opcode: 0xA9,
    }),
    trunc_f64_s: createSetArgs({
        title: "i32.trunc_f64_s",
        opcode: 0xAA,
    }),
    trunc_f64_u: createSetArgs({
        title: "i32.trunc_f64_u",
        opcode: 0xAB,
    }),
    reinterpret_f32: createSetArgs({
        title: "i32.reinterpret_f32",
        opcode: 0xBC,
    }),
    extend8_s: createSetArgs({
        title: "i32.extend8_s",
        opcode: 0xC0,
    }),
    extend16_s: createSetArgs({
        title: "i32.extend16_s",
        opcode: 0xC1,
    }),
    trunc_sat_f32_s: createSetArgs({
        title: "i32.trunc_sat_f32_s",
        opcode: 0xFC_00,
    }),
    trunc_sat_f32_u: createSetArgs({
        title: "i32.trunc_sat_f32_u",
        opcode: 0xFC_01,
    }),
    trunc_sat_f64_s: createSetArgs({
        title: "i32.trunc_sat_f64_s",
        opcode: 0xFC_02,
    }),
    trunc_sat_f64_u: createSetArgs({
        title: "i32.trunc_sat_f64_u",
        opcode: 0xFC_03,
    }),
};
