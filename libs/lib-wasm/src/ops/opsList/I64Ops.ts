
import { i64Type } from "../../Types";
import { createSetArgs } from "../CreateSetArgs";

export const i64BasicOps = {
    load: (offset: number, align?: number) => createSetArgs({
        title: "i64.load",
        opcode: 0x29,
        offset,
        align,
    }),
    load8_s: (offset: number, align?: number) => createSetArgs({
        title: "i64.load8_s",
        opcode: 0x30,
        offset,
        align,
    }),
    load8_u: (offset: number, align?: number) => createSetArgs({
        title: "i64.load8_u",
        opcode: 0x31,
        offset,
        align,
    }),
    load16_s: (offset: number, align?: number) => createSetArgs({
        title: "i64.load16_s",
        opcode: 0x32,
        offset,
        align,
    }),
    load16_u: (offset: number, align?: number) => createSetArgs({
        title: "i64.load16_u",
        opcode: 0x33,
        offset,
        align,
    }),
    load32_s: (offset: number, align?: number) => createSetArgs({
        title: "i64.load32_s",
        opcode: 0x34,
        offset,
        align,
    }),
    load32_u: (offset: number, align?: number) => createSetArgs({
        title: "i64.load32_u",
        opcode: 0x35,
        offset,
        align,
    }),
    store: (offset: number, align?: number) => createSetArgs({
        title: "i64.store",
        opcode: 0x37,
        offset,
        align,
    }),
    store8: (offset: number, align?: number) => createSetArgs({
        title: "i64.store8",
        opcode: 0x3C,
        offset,
        align,
    }),
    store16: (offset: number, align?: number) => createSetArgs({
        title: "i64.store16",
        opcode: 0x3D,
        offset,
        align,
    }),
    store32: (offset: number, align?: number) => createSetArgs({
        title: "i64.store32",
        opcode: 0x3E,
        offset,
        align,
    }),
    const: (v: number | bigint) => ({
        title: "i64.const",
        opcode: 0x42,
        value: v,
        type: i64Type,
        args: [],
    }),
    eqz: createSetArgs({
        title: "i64.eqz",
        opcode: 0x50,
    }),
    eq: createSetArgs({
        title: "i64.eq",
        opcode: 0x51,
    }),
    ne: createSetArgs({
        title: "i64.ne",
        opcode: 0x52,
    }),
    lt_s: createSetArgs({
        title: "i64.lt_s",
        opcode: 0x53,
    }),
    lt_u: createSetArgs({
        title: "i64.lt_u",
        opcode: 0x54,
    }),
    gt_s: createSetArgs({
        title: "i64.gt_s",
        opcode: 0x55,
    }),
    gt_u: createSetArgs({
        title: "i64.gt_u",
        opcode: 0x56,
    }),
    le_s: createSetArgs({
        title: "i64.le_s",
        opcode: 0x57,
    }),
    le_u: createSetArgs({
        title: "i64.le_u",
        opcode: 0x58,
    }),
    ge_s: createSetArgs({
        title: "i64.ge_s",
        opcode: 0x59,
    }),
    ge_u: createSetArgs({
        title: "i64.ge_u",
        opcode: 0x5A,
    }),
    clz: createSetArgs({
        title: "i64.clz",
        opcode: 0x79,
    }),
    ctz: createSetArgs({
        title: "i64.ctz",
        opcode: 0x7A,
    }),
    popcnt: createSetArgs({
        title: "i64.popcnt",
        opcode: 0x7B,
    }),
    add: createSetArgs({
        title: "i64.add",
        opcode: 0x7C,
    }),
    sub: createSetArgs({
        title: "i64.sub",
        opcode: 0x7D,
    }),
    mul: createSetArgs({
        title: "i64.mul",
        opcode: 0x7E,
    }),
    div_s: createSetArgs({
        title: "i64.div_s",
        opcode: 0x7F,
    }),
    div_u: createSetArgs({
        title: "i64.div_u",
        opcode: 0x80,
    }),
    rem_s: createSetArgs({
        title: "i64.rem_s",
        opcode: 0x81,
    }),
    rem_u: createSetArgs({
        title: "i64.rem_u",
        opcode: 0x82,
    }),
    and: createSetArgs({
        title: "i64.and",
        opcode: 0x83,
    }),
    or: createSetArgs({
        title: "i64.or",
        opcode: 0x84,
    }),
    xor: createSetArgs({
        title: "i64.xor",
        opcode: 0x85,
    }),
    shl: createSetArgs({
        title: "i64.shl",
        opcode: 0x86,
    }),
    shr_s: createSetArgs({
        title: "i64.shr_s",
        opcode: 0x87,
    }),
    shr_u: createSetArgs({
        title: "i64.shr_u",
        opcode: 0x88,
    }),
    rotl: createSetArgs({
        title: "i64.rotl",
        opcode: 0x89,
    }),
    rotr: createSetArgs({
        title: "i64.rotr",
        opcode: 0x8A,
    }),
    extend_i32_s: createSetArgs({
        title: "i64.extend_i32_s",
        opcode: 0xAC,
    }),
    extend_i32_u: createSetArgs({
        title: "i64.extend_i32_u",
        opcode: 0xAD,
    }),
    trunc_f32_s: createSetArgs({
        title: "i64.trunc_f32_s",
        opcode: 0xAE,
    }),
    trunc_f32_u: createSetArgs({
        title: "i64.trunc_f32_u",
        opcode: 0xAF,
    }),
    trunc_f64_s: createSetArgs({
        title: "i64.trunc_f64_s",
        opcode: 0xB0,
    }),
    trunc_f64_u: createSetArgs({
        title: "i64.trunc_f64_u",
        opcode: 0xB1,
    }),
    reinterpret_f64: createSetArgs({
        title: "i64.reinterpret_f64",
        opcode: 0xBD,
    }),
    extend8_s: createSetArgs({
        title: "i64.extend8_s",
        opcode: 0xC2,
    }),
    extend16_s: createSetArgs({
        title: "i64.extend16_s",
        opcode: 0xC3,
    }),
    extend32_s: createSetArgs({
        title: "i64.extend32_s",
        opcode: 0xC4,
    }),
    trunc_sat_f32_s: createSetArgs({
        title: "i64.trunc_sat_f32_s",
        opcode: 0xFC_04,
    }),
    trunc_sat_f32_u: createSetArgs({
        title: "i64.trunc_sat_f32_u",
        opcode: 0xFC_05,
    }),
    trunc_sat_f64_s: createSetArgs({
        title: "i64.trunc_sat_f64_s",
        opcode: 0xFC_06,
    }),
    trunc_sat_f64_u: createSetArgs({
        title: "i64.trunc_sat_f64_u",
        opcode: 0xFC_07,
    }),
};
