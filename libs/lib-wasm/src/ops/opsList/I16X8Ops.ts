

import { createSetArgs } from "../CreateSetArgs";

export const i16x8BasicOps = {
    splat: createSetArgs({
        title: "i16x8.splat",
        opcode: 0xFD_10,
    }),
    extract_lane_s: (laneidx: number) => createSetArgs({
        title: "i16x8.extract_lane_s",
        opcode: 0xFD_18,
        laneidx,
    }),
    extract_lane_u: (laneidx: number) => createSetArgs({
        title: "i16x8.extract_lane_u",
        opcode: 0xFD_19,
        laneidx,
    }),
    replace_lane: (laneidx: number) => createSetArgs({
        title: "i16x8.replace_lane",
        opcode: 0xFD_1A,
        laneidx,
    }),
    eq: createSetArgs({
        title: "i16x8.eq",
        opcode: 0xFD_2D,
    }),
    ne: createSetArgs({
        title: "i16x8.ne",
        opcode: 0xFD_2E,
    }),
    lt_s: createSetArgs({
        title: "i16x8.lt_s",
        opcode: 0xFD_2F,
    }),
    lt_u: createSetArgs({
        title: "i16x8.lt_u",
        opcode: 0xFD_30,
    }),
    gt_s: createSetArgs({
        title: "i16x8.gt_s",
        opcode: 0xFD_31,
    }),
    gt_u: createSetArgs({
        title: "i16x8.gt_u",
        opcode: 0xFD_32,
    }),
    le_s: createSetArgs({
        title: "i16x8.le_s",
        opcode: 0xFD_33,
    }),
    le_u: createSetArgs({
        title: "i16x8.le_u",
        opcode: 0xFD_34,
    }),
    ge_s: createSetArgs({
        title: "i16x8.ge_s",
        opcode: 0xFD_35,
    }),
    ge_u: createSetArgs({
        title: "i16x8.ge_u",
        opcode: 0xFD_36,
    }),
    extadd_pairwise_i8x16_s: createSetArgs({
        title: "i16x8.extadd_pairwise_i8x16_s",
        opcode: 0xFD_7C,
    }),
    extadd_pairwise_i8x16_u: createSetArgs({
        title: "i16x8.extadd_pairwise_i8x16_u",
        opcode: 0xFD_7D,
    }),
    abs: createSetArgs({
        title: "i16x8.abs",
        opcode: 0xFD_80_01,
    }),
    neg: createSetArgs({
        title: "i16x8.neg",
        opcode: 0xFD_81_01,
    }),
    q15mulr_sat_s: createSetArgs({
        title: "i16x8.q15mulr_sat_s",
        opcode: 0xFD_82_01,
    }),
    all_true: createSetArgs({
        title: "i16x8.all_true",
        opcode: 0xFD_83_01,
    }),
    bitmask: createSetArgs({
        title: "i16x8.bitmask",
        opcode: 0xFD_84_01,
    }),
    narrow_i32x4_s: createSetArgs({
        title: "i16x8.narrow_i32x4_s",
        opcode: 0xFD_85_01,
    }),
    narrow_i32x4_u: createSetArgs({
        title: "i16x8.narrow_i32x4_u",
        opcode: 0xFD_86_01,
    }),
    extend_low_i8x16_s: createSetArgs({
        title: "i16x8.extend_low_i8x16_s",
        opcode: 0xFD_87_01,
    }),
    extend_high_i8x16_s: createSetArgs({
        title: "i16x8.extend_high_i8x16_s",
        opcode: 0xFD_88_01,
    }),
    extend_low_i8x16_u: createSetArgs({
        title: "i16x8.extend_low_i8x16_u",
        opcode: 0xFD_89_01,
    }),
    extend_high_i8x16_u: createSetArgs({
        title: "i16x8.extend_high_i8x16_u",
        opcode: 0xFD_8A_01,
    }),
    shl: createSetArgs({
        title: "i16x8.shl",
        opcode: 0xFD_8B_01,
    }),
    shr_s: createSetArgs({
        title: "i16x8.shr_s",
        opcode: 0xFD_8C_01,
    }),
    shr_u: createSetArgs({
        title: "i16x8.shr_u",
        opcode: 0xFD_8D_01,
    }),
    add: createSetArgs({
        title: "i16x8.add",
        opcode: 0xFD_8E_01,
    }),
    add_sat_s: createSetArgs({
        title: "i16x8.add_sat_s",
        opcode: 0xFD_8F_01,
    }),
    add_sat_u: createSetArgs({
        title: "i16x8.add_sat_u",
        opcode: 0xFD_90_01,
    }),
    sub: createSetArgs({
        title: "i16x8.sub",
        opcode: 0xFD_91_01,
    }),
    sub_sat_s: createSetArgs({
        title: "i16x8.sub_sat_s",
        opcode: 0xFD_92_01,
    }),
    sub_sat_u: createSetArgs({
        title: "i16x8.sub_sat_u",
        opcode: 0xFD_93_01,
    }),
    mul: createSetArgs({
        title: "i16x8.mul",
        opcode: 0xFD_95_01,
    }),
    min_s: createSetArgs({
        title: "i16x8.min_s",
        opcode: 0xFD_96_01,
    }),
    min_u: createSetArgs({
        title: "i16x8.min_u",
        opcode: 0xFD_97_01,
    }),
    max_s: createSetArgs({
        title: "i16x8.max_s",
        opcode: 0xFD_98_01,
    }),
    max_u: createSetArgs({
        title: "i16x8.max_u",
        opcode: 0xFD_99_01,
    }),
    avgr_u: createSetArgs({
        title: "i16x8.avgr_u",
        opcode: 0xFD_9B_01,
    }),
    extmul_low_i8x16_s: createSetArgs({
        title: "i16x8.extmul_low_i8x16_s",
        opcode: 0xFD_9C_01,
    }),
    extmul_high_i8x16_s: createSetArgs({
        title: "i16x8.extmul_high_i8x16_s",
        opcode: 0xFD_9D_01,
    }),
    extmul_low_i8x16_u: createSetArgs({
        title: "i16x8.extmul_low_i8x16_u",
        opcode: 0xFD_9E_01,
    }),
    extmul_high_i8x16_u: createSetArgs({
        title: "i16x8.extmul_high_i8x16_u",
        opcode: 0xFD_9F_01,
    }),
};
