

import { createSetArgs } from "../CreateSetArgs";

export const f64x2BasicOps = {
    splat: createSetArgs({
        title: "f64x2.splat",
        opcode: 0xFD_14,
    }),
    extract_lane: (laneidx: number) => createSetArgs({
        title: "f64x2.extract_lane",
        opcode: 0xFD_21,
        laneidx,
    }),
    replace_lane: (laneidx: number) => createSetArgs({
        title: "f64x2.replace_lane",
        opcode: 0xFD_22,
        laneidx,
    }),
    eq: createSetArgs({
        title: "f64x2.eq",
        opcode: 0xFD_47,
    }),
    ne: createSetArgs({
        title: "f64x2.ne",
        opcode: 0xFD_48,
    }),
    lt: createSetArgs({
        title: "f64x2.lt",
        opcode: 0xFD_49,
    }),
    gt: createSetArgs({
        title: "f64x2.gt",
        opcode: 0xFD_4A,
    }),
    le: createSetArgs({
        title: "f64x2.le",
        opcode: 0xFD_4B,
    }),
    ge: createSetArgs({
        title: "f64x2.ge",
        opcode: 0xFD_4C,
    }),
    promote_low_f32x4: createSetArgs({
        title: "f64x2.promote_low_f32x4",
        opcode: 0xFD_5F,
    }),
    ceil: createSetArgs({
        title: "f64x2.ceil",
        opcode: 0xFD_74,
    }),
    floor: createSetArgs({
        title: "f64x2.floor",
        opcode: 0xFD_75,
    }),
    trunc: createSetArgs({
        title: "f64x2.trunc",
        opcode: 0xFD_7A,
    }),
    nearest: createSetArgs({
        title: "f64x2.nearest",
        opcode: 0xFD_94_01,
    }),
    abs: createSetArgs({
        title: "f64x2.abs",
        opcode: 0xFD_EC_01,
    }),
    neg: createSetArgs({
        title: "f64x2.neg",
        opcode: 0xFD_ED_01,
    }),
    sqrt: createSetArgs({
        title: "f64x2.sqrt",
        opcode: 0xFD_EF_01,
    }),
    add: createSetArgs({
        title: "f64x2.add",
        opcode: 0xFD_F0_01,
    }),
    sub: createSetArgs({
        title: "f64x2.sub",
        opcode: 0xFD_F1_01,
    }),
    mul: createSetArgs({
        title: "f64x2.mul",
        opcode: 0xFD_F2_01,
    }),
    div: createSetArgs({
        title: "f64x2.div",
        opcode: 0xFD_F3_01,
    }),
    min: createSetArgs({
        title: "f64x2.min",
        opcode: 0xFD_F4_01,
    }),
    max: createSetArgs({
        title: "f64x2.max",
        opcode: 0xFD_F5_01,
    }),
    pmin: createSetArgs({
        title: "f64x2.pmin",
        opcode: 0xFD_F6_01,
    }),
    pmax: createSetArgs({
        title: "f64x2.pmax",
        opcode: 0xFD_F7_01,
    }),
    convert_low_i32x4_s: createSetArgs({
        title: "f64x2.convert_low_i32x4_s",
        opcode: 0xFD_FE_01,
    }),
    convert_low_i32x4_u: createSetArgs({
        title: "f64x2.convert_low_i32x4_u",
        opcode: 0xFD_FF_01,
    }),
};
