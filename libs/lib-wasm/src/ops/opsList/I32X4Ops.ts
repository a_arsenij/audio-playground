

import { createSetArgs } from "../CreateSetArgs";

export const i32x4BasicOps = {
    splat: createSetArgs({
        title: "i32x4.splat",
        opcode: 0xFD_11,
    }),
    extract_lane: (laneidx: number) => createSetArgs({
        title: "i32x4.extract_lane",
        opcode: 0xFD_1B,
        laneidx,
    }),
    replace_lane: (laneidx: number) => createSetArgs({
        title: "i32x4.replace_lane",
        opcode: 0xFD_1C,
        laneidx,
    }),
    eq: createSetArgs({
        title: "i32x4.eq",
        opcode: 0xFD_37,
    }),
    ne: createSetArgs({
        title: "i32x4.ne",
        opcode: 0xFD_38,
    }),
    lt_s: createSetArgs({
        title: "i32x4.lt_s",
        opcode: 0xFD_39,
    }),
    lt_u: createSetArgs({
        title: "i32x4.lt_u",
        opcode: 0xFD_3A,
    }),
    gt_s: createSetArgs({
        title: "i32x4.gt_s",
        opcode: 0xFD_3B,
    }),
    gt_u: createSetArgs({
        title: "i32x4.gt_u",
        opcode: 0xFD_3C,
    }),
    le_s: createSetArgs({
        title: "i32x4.le_s",
        opcode: 0xFD_3D,
    }),
    le_u: createSetArgs({
        title: "i32x4.le_u",
        opcode: 0xFD_3E,
    }),
    ge_s: createSetArgs({
        title: "i32x4.ge_s",
        opcode: 0xFD_3F,
    }),
    ge_u: createSetArgs({
        title: "i32x4.ge_u",
        opcode: 0xFD_40,
    }),
    extadd_pairwise_i16x8_s: createSetArgs({
        title: "i32x4.extadd_pairwise_i16x8_s",
        opcode: 0xFD_7E,
    }),
    extadd_pairwise_i16x8_u: createSetArgs({
        title: "i32x4.extadd_pairwise_i16x8_u",
        opcode: 0xFD_7F,
    }),
    abs: createSetArgs({
        title: "i32x4.abs",
        opcode: 0xFD_A0_01,
    }),
    neg: createSetArgs({
        title: "i32x4.neg",
        opcode: 0xFD_A1_01,
    }),
    all_true: createSetArgs({
        title: "i32x4.all_true",
        opcode: 0xFD_A3_01,
    }),
    bitmask: createSetArgs({
        title: "i32x4.bitmask",
        opcode: 0xFD_A4_01,
    }),
    extend_low_i16x8_s: createSetArgs({
        title: "i32x4.extend_low_i16x8_s",
        opcode: 0xFD_A7_01,
    }),
    extend_high_i16x8_s: createSetArgs({
        title: "i32x4.extend_high_i16x8_s",
        opcode: 0xFD_A8_01,
    }),
    extend_low_i16x8_u: createSetArgs({
        title: "i32x4.extend_low_i16x8_u",
        opcode: 0xFD_A9_01,
    }),
    extend_high_i16x8_u: createSetArgs({
        title: "i32x4.extend_high_i16x8_u",
        opcode: 0xFD_AA_01,
    }),
    shl: createSetArgs({
        title: "i32x4.shl",
        opcode: 0xFD_AB_01,
    }),
    shr_s: createSetArgs({
        title: "i32x4.shr_s",
        opcode: 0xFD_AC_01,
    }),
    shr_u: createSetArgs({
        title: "i32x4.shr_u",
        opcode: 0xFD_AD_01,
    }),
    add: createSetArgs({
        title: "i32x4.add",
        opcode: 0xFD_AE_01,
    }),
    sub: createSetArgs({
        title: "i32x4.sub",
        opcode: 0xFD_B1_01,
    }),
    mul: createSetArgs({
        title: "i32x4.mul",
        opcode: 0xFD_B5_01,
    }),
    min_s: createSetArgs({
        title: "i32x4.min_s",
        opcode: 0xFD_B6_01,
    }),
    min_u: createSetArgs({
        title: "i32x4.min_u",
        opcode: 0xFD_B7_01,
    }),
    max_s: createSetArgs({
        title: "i32x4.max_s",
        opcode: 0xFD_B8_01,
    }),
    max_u: createSetArgs({
        title: "i32x4.max_u",
        opcode: 0xFD_B9_01,
    }),
    dot_i16x8_s: createSetArgs({
        title: "i32x4.dot_i16x8_s",
        opcode: 0xFD_BA_01,
    }),
    extmul_low_i16x8_s: createSetArgs({
        title: "i32x4.extmul_low_i16x8_s",
        opcode: 0xFD_BC_01,
    }),
    extmul_high_i16x8_s: createSetArgs({
        title: "i32x4.extmul_high_i16x8_s",
        opcode: 0xFD_BD_01,
    }),
    extmul_low_i16x8_u: createSetArgs({
        title: "i32x4.extmul_low_i16x8_u",
        opcode: 0xFD_BE_01,
    }),
    extmul_high_i16x8_u: createSetArgs({
        title: "i32x4.extmul_high_i16x8_u",
        opcode: 0xFD_BF_01,
    }),
    trunc_sat_f32x4_s: createSetArgs({
        title: "i32x4.trunc_sat_f32x4_s",
        opcode: 0xFD_F8_01,
    }),
    trunc_sat_f32x4_u: createSetArgs({
        title: "i32x4.trunc_sat_f32x4_u",
        opcode: 0xFD_F9_01,
    }),
    trunc_sat_f64x2_s_zero: createSetArgs({
        title: "i32x4.trunc_sat_f64x2_s_zero",
        opcode: 0xFD_FC_01,
    }),
    trunc_sat_f64x2_u_zero: createSetArgs({
        title: "i32x4.trunc_sat_f64x2_u_zero",
        opcode: 0xFD_FD_01,
    }),
};
