
import { v128Type } from "../../Types";
import { createSetArgs } from "../CreateSetArgs";

export const v128BasicOps = {
    load: (offset: number, align?: number) => createSetArgs({
        title: "v128.load",
        opcode: 0xFD_00,
        offset,
        align,
    }),
    load8x8_s: (offset: number, align?: number) => createSetArgs({
        title: "v128.load8x8_s",
        opcode: 0xFD_01,
        offset,
        align,
    }),
    load8x8_u: (offset: number, align?: number) => createSetArgs({
        title: "v128.load8x8_u",
        opcode: 0xFD_02,
        offset,
        align,
    }),
    load16x4_s: (offset: number, align?: number) => createSetArgs({
        title: "v128.load16x4_s",
        opcode: 0xFD_03,
        offset,
        align,
    }),
    load16x4_u: (offset: number, align?: number) => createSetArgs({
        title: "v128.load16x4_u",
        opcode: 0xFD_04,
        offset,
        align,
    }),
    load32x2_s: (offset: number, align?: number) => createSetArgs({
        title: "v128.load32x2_s",
        opcode: 0xFD_05,
        offset,
        align,
    }),
    load32x2_u: (offset: number, align?: number) => createSetArgs({
        title: "v128.load32x2_u",
        opcode: 0xFD_06,
        offset,
        align,
    }),
    load8_splat: (offset: number, align?: number) => createSetArgs({
        title: "v128.load8_splat",
        opcode: 0xFD_07,
        offset,
        align,
    }),
    load16_splat: (offset: number, align?: number) => createSetArgs({
        title: "v128.load16_splat",
        opcode: 0xFD_08,
        offset,
        align,
    }),
    load32_splat: (offset: number, align?: number) => createSetArgs({
        title: "v128.load32_splat",
        opcode: 0xFD_09,
        offset,
        align,
    }),
    load64_splat: (offset: number, align?: number) => createSetArgs({
        title: "v128.load64_splat",
        opcode: 0xFD_0A,
        offset,
        align,
    }),
    store: (offset: number, align?: number) => createSetArgs({
        title: "v128.store",
        opcode: 0xFD_0B,
        offset,
        align,
    }),
    const: (v: number | bigint) => ({
        title: "v128.const",
        opcode: 0xFD_0C,
        value: v,
        type: v128Type,
        args: [],
    }),
    not: createSetArgs({
        title: "v128.not",
        opcode: 0xFD_4D,
    }),
    and: createSetArgs({
        title: "v128.and",
        opcode: 0xFD_4E,
    }),
    andnot: createSetArgs({
        title: "v128.andnot",
        opcode: 0xFD_4F,
    }),
    or: createSetArgs({
        title: "v128.or",
        opcode: 0xFD_50,
    }),
    xor: createSetArgs({
        title: "v128.xor",
        opcode: 0xFD_51,
    }),
    bitselect: createSetArgs({
        title: "v128.bitselect",
        opcode: 0xFD_52,
    }),
    any_true: createSetArgs({
        title: "v128.any_true",
        opcode: 0xFD_53,
    }),
    load8_lane: (laneidx: number, offset: number, align?: number) => createSetArgs({
        title: "v128.load8_lane",
        opcode: 0xFD_54,
        offset,
        align,
        laneidx,
    }),
    load16_lane: (laneidx: number, offset: number, align?: number) => createSetArgs({
        title: "v128.load16_lane",
        opcode: 0xFD_55,
        offset,
        align,
        laneidx,
    }),
    load32_lane: (laneidx: number, offset: number, align?: number) => createSetArgs({
        title: "v128.load32_lane",
        opcode: 0xFD_56,
        offset,
        align,
        laneidx,
    }),
    load64_lane: (laneidx: number, offset: number, align?: number) => createSetArgs({
        title: "v128.load64_lane",
        opcode: 0xFD_57,
        offset,
        align,
        laneidx,
    }),
    store8_lane: (laneidx: number, offset: number, align?: number) => createSetArgs({
        title: "v128.store8_lane",
        opcode: 0xFD_58,
        offset,
        align,
        laneidx,
    }),
    store16_lane: (laneidx: number, offset: number, align?: number) => createSetArgs({
        title: "v128.store16_lane",
        opcode: 0xFD_59,
        offset,
        align,
        laneidx,
    }),
    store32_lane: (laneidx: number, offset: number, align?: number) => createSetArgs({
        title: "v128.store32_lane",
        opcode: 0xFD_5A,
        offset,
        align,
        laneidx,
    }),
    store64_lane: (laneidx: number, offset: number, align?: number) => createSetArgs({
        title: "v128.store64_lane",
        opcode: 0xFD_5B,
        offset,
        align,
        laneidx,
    }),
    load32_zero: (offset: number, align?: number) => createSetArgs({
        title: "v128.load32_zero",
        opcode: 0xFD_5C,
        offset,
        align,
    }),
    load64_zero: (offset: number, align?: number) => createSetArgs({
        title: "v128.load64_zero",
        opcode: 0xFD_5D,
        offset,
        align,
    }),
};
