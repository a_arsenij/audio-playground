import { FunctionRef, Global, LabelRef, Local } from "../Op";
import { WAType } from "../Types";

export type BasicOpRoot = {
    title: string,
    opcode: number,
}

export type ArgedOp = BasicOpRoot & {
    args: OP[],
}
export type ArguableOp = BasicOpRoot & ((...args: OP[]) => ArgedOp);

export type BasicOp =
    | ArgedOp
    | ArguableOp
    ;

export type ConstOp = BasicOp & {
    type: WAType,
    value: number,
}

export type LoadStoreOp = BasicOp & {
    offset: number,
    align?: number,
}

export type ExtractReplaceLaneOp = BasicOp & {
    laneidx: number,
}

export type ShuffleOp = BasicOp & {
    laneidx: number[],
}

export type LoadStoreLaneOp = LoadStoreOp & ExtractReplaceLaneOp;

export type LocalOp<NAME extends string, TYPE extends WAType> = BasicOp & {
    local: Local<NAME, TYPE>,
}

export type GlobalOp<NAME extends string, TYPE extends WAType> = BasicOp & {
    global: Global<NAME, TYPE>,
}

export type CallOp = BasicOp & {
    function: FunctionRef<string, WAType[]>,
}

export type BreakableOp = BasicOp & {
    type: WAType,
    ops: OP[],
    label: LabelRef,
}

export type IfOp = BasicOp & {
    type: WAType,
    then: OP[],
    else?: OP[],
}

export type BreakOp = BasicOp & {
    goTo: LabelRef,
}

export type OP =
    | BasicOp
    | ConstOp
    | LoadStoreOp
    | ExtractReplaceLaneOp
    | LoadStoreLaneOp
    | ShuffleOp
    | LocalOp<string, WAType>
    | GlobalOp<string, WAType>
    | CallOp
    | BreakableOp
    | IfOp
    | BreakOp
    ;
