import { f32Type, f64Type, i32Type, i64Type, v128Type } from "../Types";
import { controlOps } from "./opsList/ControlOps";
import { f32BasicOps } from "./opsList/F32Ops";
import { f32x4BasicOps } from "./opsList/F32X4Ops";
import { f64BasicOps } from "./opsList/F64Ops";
import { i8x16BasicOps } from "./opsList/I8X16Ops";
import { i16x8BasicOps } from "./opsList/I16X8Ops";
import { i32BasicOps } from "./opsList/I32Ops";
import { i32x4BasicOps } from "./opsList/I32X4Ops";
import { i64BasicOps } from "./opsList/I64Ops";
import { i64x2BasicOps } from "./opsList/I64X2Ops";
import { v128BasicOps } from "./opsList/V128Ops";

export const i32 = {
    type: i32Type,
    ...i32BasicOps,
};

export const i64 = {
    type: i64Type,
    ...i64BasicOps,
};

export const f32 = {
    type: f32Type,
    ...f32BasicOps,
};

export const f64 = {
    type: f64Type,
    ...f64BasicOps,
};

export const v128 = {
    type: v128Type,
    ...v128BasicOps,
};

export const i8x16 = i8x16BasicOps;
export const i16x8 = i16x8BasicOps;
export const i32x4 = i32x4BasicOps;
export const i64x2 = i64x2BasicOps;
export const f32x4 = f32x4BasicOps;
export const f64x2 = f64BasicOps;

export const control = controlOps;
