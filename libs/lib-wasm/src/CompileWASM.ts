

import { byteToString } from "@audio-playground/lib-common/std/byteToString";
import { getOrPut } from "@audio-playground/lib-common/std/GetOrPut";
import { padStart } from "@audio-playground/lib-common/std/String";

import { collectUsedFunctions } from "./CollectUsedFunctions";
import { sintToLeb128, uintToLeb128 } from "./LEB128";
import { FunctionRef, LabelRef, Local, Module } from "./Op";
import { f32, f64, i32, i64 } from "./ops";
import { OP } from "./ops/OpsTypes";
import { WAType } from "./Types";

const TYPE_SECTION = { sectionCode: 1, sectionTitle: "type" };
const IMPORT_SECTION = { sectionCode: 2, sectionTitle: "import" };
const FUNCTION_SECTION = { sectionCode: 3, sectionTitle: "function" };
const EXPORT_SECTION = { sectionCode: 7, sectionTitle: "export" };
const CODE_SECTION = { sectionCode: 10, sectionTitle: "code" };

type SectionType = {
    sectionCode: number,
    sectionTitle: string,
}

type WriteFunc = (
    data: number,
    comment?: string,
) => void;

type Comment = string | undefined;

function createWriter() {
    const bytecode: number[] = [];
    const comments: Comment[] = [];
    function writeByte(
        data: number,
        comment?: string,
    ) {
        bytecode.push(data);
        comments.push(comment);
    }
    return { bytecode, comments, writeByte };
}

function writeBytes(
    write: WriteFunc,
    bytes: number[] | Uint8Array,
    comment?: string,
) {
    bytes.forEach((b, idx) => {
        if (idx === 0) {
            write(b, comment);
        } else {
            write(b, undefined);
        }
    });
}

const getWriteIntFunction = (toLeb: (value: number | bigint) => number[]) => (
    write: WriteFunc,
    number: number | bigint,
    customComment?: string,
) => {
    writeBytes(write, toLeb(number), customComment || `number ${number}`);
};

const writeUInt = getWriteIntFunction(uintToLeb128);
const writeSInt = getWriteIntFunction(sintToLeb128);


function writeString(
    write: WriteFunc,
    str: string,
) {
    if (!globalThis.TextEncoder) {
        const codepoints = [...Array(str.length)].map((_, idx) =>
            str.charCodeAt(idx),
        );
        if (codepoints.some((v) => v >= 128)) {
            throw new Error("Only ASCII supported now");
        }
        writeUInt(write, codepoints.length, `string "${str}"`);
        writeBytes(write, codepoints);
        return;
    }

    const encoded = (new TextEncoder()).encode(str);
    writeUInt(write, encoded.length, `string "${str}"`);
    writeBytes(write, encoded);
}

function writeBuffer(
    defaultWriteFunc: WriteFunc,
    bytes: number[],
    comments: Comment[],
) {
    bytes.forEach((b, idx) => {
        defaultWriteFunc(b, comments[idx]);
    });
}

function writeSized(
    defaultWriteFunc: WriteFunc,
    action: (write: WriteFunc) => void,
    customTitle?: string,
) {
    const tmp = createWriter();
    action(tmp.writeByte);
    writeUInt(
        defaultWriteFunc,
        tmp.bytecode.length,
        `${customTitle ? `${customTitle} ` : ""}${tmp.bytecode.length} bytes`,
    );
    writeBuffer(defaultWriteFunc, tmp.bytecode, tmp.comments);
}

function writeOpCode(
    write: WriteFunc,
    op: OP,
) {
    let code = op.opcode;
    const bytes: number[] = [];
    while (code > 0) {
        bytes.push(code % 256);
        code = Math.floor(code / 256);
    }
    bytes.reverse();
    bytes.forEach((b) => write(b, op.title));
}

const MAGIC: number[] = [0x00, 0x61, 0x73, 0x6d];
const VERSION: number[] = [0x01, 0x00, 0x00, 0x00];
const FUNC = 0x60;
const END = 0x0b;
const IF = 0x04;
const ELSE = 0x05;

export type WASM = {
    bytecode: number[],
    comments: (string | undefined)[],
}

export function prettyWasm(
    wasm: WASM,
): string {
    return [...wasm.bytecode].map((byte, idx) => `${padStart((idx).toString(16), 7, "0")} : ${byteToString(byte)} ${wasm.comments[idx] ? `;; ${wasm.comments[idx]}` : ""}`).join("\n");
}

export function compileModuleToWASM(
    module: Module,
): WASM {

    const { bytecode, comments, writeByte } = createWriter();

    writeBytes(writeByte, MAGIC, "magic");
    writeBytes(writeByte, VERSION, "version");

    function writeSection(
        type: SectionType,
        action: (write: WriteFunc) => void,
    ) {
        writeByte(type.sectionCode, `${type.sectionTitle} section`);
        writeSized(writeByte, action);
    }

    if (module.functions.length > 0) {

        function getSigKey(
            params: { type: WAType }[],
            result: WAType[],
        ): string {
            if (!Array.isArray(result)) {
                console.log("??/");
            }
            return `${params.map((p) => p.type.typeTitle).join(",")}:${
                result.map((p) => p.typeTitle).join(",")}`;
        }

        const sigkeyToIndex = new Map<string, number>();
        const functions = collectUsedFunctions(
            module.functions,
            module.imports.length,
        );
        const functionsIndices = new Map<FunctionRef<string, WAType[]>, number>(
            functions,
        );
        module.imports.forEach((imp, idx) => {
            functionsIndices.set(imp, idx);
        });

        writeSection(
            TYPE_SECTION,
            (writeByte) => {
                const signatures = createWriter();
                function addSignature(
                    params: { type: WAType }[],
                    result: WAType[],
                ) {
                    const sigKey = getSigKey(params, result);
                    if (!sigkeyToIndex.has(sigKey)) {
                        sigkeyToIndex.set(sigKey, sigkeyToIndex.size);
                        signatures.writeByte(FUNC, "func marker");
                        writeUInt(signatures.writeByte, params.length, `${params.length} params`);
                        params.forEach((p) => {
                            signatures.writeByte(p.type.typeCode, p.type.typeTitle);
                        });
                        writeUInt(signatures.writeByte, result.length, `${result.length} params`);
                        result.forEach((p) => {
                            signatures.writeByte(p.typeCode, p.typeTitle);
                        });
                    }
                }
                functions.forEach((_, f) => addSignature(f.params, f.result));
                module.imports.forEach((i) => addSignature(i.params, i.result));
                writeUInt(writeByte, sigkeyToIndex.size, `${sigkeyToIndex.size} signatures`);
                writeBuffer(writeByte, signatures.bytecode, signatures.comments);
            },
        );
        const numImports = (module.memory ? 1 : 0) +
            (module.imports.length) +
            (module.globals.length);
        if (numImports > 0) {
            writeSection(
                IMPORT_SECTION,
                (writeByte) => {
                    writeUInt(writeByte, numImports, `${numImports} imports`);
                    if (module.memory) {
                        writeString(writeByte, module.memory.path[0]);
                        writeString(writeByte, module.memory.path[1]);
                        writeByte(0x02, "memory"); // memory
                        writeByte(0x00, "min specified next"); // only min
                        writeUInt(writeByte, module.memory.sizePages, `${module.memory.sizePages} pages`);
                    }
                    module.imports.forEach((i) => {
                        writeString(writeByte, i.path[0]);
                        writeString(writeByte, i.path[1]);
                        writeByte(0x00, "function"); // function
                        const signature = sigkeyToIndex.get(getSigKey(i.params, i.result))!;
                        writeUInt(writeByte, signature, `signature #${signature}`);
                    });
                    module.globals.forEach((g) => {
                        writeString(writeByte, g.path[0]);
                        writeString(writeByte, g.path[1]);
                        writeByte(0x03, "global"); // global
                        writeByte(g.type.typeCode, g.type.typeTitle);
                        writeByte(g.mutable ? 0x01 : 0x00, g.mutable ? "mutable" : "const");
                    });
                },
            );
        }
        writeSection(
            FUNCTION_SECTION,
            (writeByte) => {
                writeUInt(writeByte, functions.size, `${functions.size} functions`);
                functions.forEach((_, f) => {
                    const signature = sigkeyToIndex.get(getSigKey(f.params, f.result))!;
                    writeUInt(writeByte, signature, `signature #${signature}`);
                });
            },
        );
        const exportedFunctions = [...functions.entries()]
            .filter((f) => "isExported" in f[0] && f[0].isExported);
        writeSection(
            EXPORT_SECTION,
            (writeByte) => {
                writeUInt(writeByte, exportedFunctions.length, `${exportedFunctions.length} exported functions`);
                exportedFunctions.forEach(([func, funcIdx]) => {
                    writeString(writeByte, func.functionName);
                    writeByte(0);
                    writeUInt(writeByte, funcIdx, `function #${funcIdx} ${func.functionName}`);
                });
            },
        );
        writeSection(
            CODE_SECTION,
            (writeByte) => {
                writeUInt(writeByte, functions.size, `${functions.size} functions`);
                functions.forEach((fIdx, f) => {
                    writeSized(
                        writeByte,
                        (writeByte) => {

                            const typeToLocals = new Map<WAType, Local<string, WAType>[]>();
                            f.locals.forEach((l) => {
                                getOrPut(
                                    typeToLocals,
                                    l.type,
                                    () => [],
                                ).push(l);
                            });
                            const localToIndex = new Map<Local<string, WAType>, number>();
                            f.params.forEach((p) => {
                                localToIndex.set(p, localToIndex.size);
                            });
                            typeToLocals.forEach((v) => v.forEach((vv) => {
                                localToIndex.set(vv, localToIndex.size);
                            }));

                            writeUInt(writeByte, typeToLocals.size, `${typeToLocals.size} locals definitions`);
                            typeToLocals.forEach((arr) => {
                                writeUInt(writeByte, arr.length, `${arr.length} locals with this type (${arr.map((e) => e.localName).join(", ")})`);
                                writeByte(arr[0]!.type.typeCode, arr[0]!.type.typeTitle);
                            });
                            const depths = new Map<LabelRef, number>();
                            function writeOp(
                                op: OP,
                            ) {
                                if ("args" in op) {
                                    op.args.forEach((o) =>
                                        writeOp(o),
                                    );
                                }
                                if ("local" in op) {
                                    writeOpCode(writeByte, op);
                                    const localIndex = localToIndex.get(op.local)!;
                                    writeUInt(writeByte, localIndex, `local #${localIndex} ${op.local.localName}`);
                                } else if ("global" in op) {
                                    writeOpCode(writeByte, op);
                                    writeUInt(writeByte, op.global.index, `global #${op.global.index} ${op.global.globalName}`);
                                } else if ("value" in op) {
                                    writeOpCode(writeByte, op);
                                    if (
                                        op.type.typeTitle === i32.type.typeTitle ||
                                        op.type.typeTitle === i64.type.typeTitle
                                    ) {

                                        writeSInt(writeByte, op.value, `${op.type.typeTitle} ${op.value}`);
                                    } else if (op.type.typeTitle === f32.type.typeTitle) {
                                        const arr = new Float32Array(1);
                                        arr.set([op.value], 0);
                                        const u8 = new Uint8Array(arr.buffer);
                                        writeBytes(writeByte, u8, `${f32.type.typeTitle} ${op.value}`);
                                    } else if (op.type.typeTitle === f64.type.typeTitle) {
                                        const arr = new Float64Array(1);
                                        arr.set([op.value], 0);
                                        const u8 = new Uint8Array(arr.buffer);
                                        writeBytes(writeByte, u8, `${f64.type.typeTitle} ${op.value}`);
                                    }
                                } else if ("offset" in op) {
                                    writeOpCode(writeByte, op);
                                    const align = op.align ?? 1;
                                    writeUInt(
                                        writeByte, align,
                                        `align=${align}`,
                                    );
                                    const offset = op.offset;
                                    writeUInt(
                                        writeByte, offset,
                                        `offset=${offset}`,
                                    );
                                } else if ("function" in op) {
                                    writeOpCode(writeByte, op);
                                    const functionIndex = functionsIndices.get(op.function)!;
                                    writeUInt(writeByte, functionIndex, `func #${functionIndex} ${op.function.functionName}`);
                                } else if ("label" in op) {
                                    const depth = depths.size;
                                    depths.set(op.label, depth);
                                    writeOpCode(writeByte, op);
                                    writeByte(op.type.typeCode, op.type.typeTitle);
                                    writeOps(op.ops);
                                    writeByte(END, `end (${op.title})`);
                                    depths.delete(op.label);
                                } else if ("then" in op) {
                                    writeByte(IF, "if");
                                    writeByte(op.type.typeCode, op.type.typeTitle);
                                    writeOps(op.then);
                                    if (op.else) {
                                        writeByte(ELSE, "else");
                                        writeOps(op.else);
                                    }
                                    writeByte(END, "end (if)");
                                } else if ("goTo" in op) {
                                    writeOpCode(writeByte, op);
                                    const currentDepth = depths.size;
                                    const labelDepth = depths.get(op.goTo)!;
                                    const depth = (currentDepth - labelDepth - 1);
                                    writeUInt(writeByte, depth, `${depth} levels`);
                                } else {
                                    writeOpCode(writeByte, op);
                                }
                            }
                            function writeOps(
                                ops: OP[],
                            ) {
                                ops.forEach(writeOp);
                            }
                            writeOps(f.ops);
                            writeByte(END, "end (func)");
                        }, `func ${f.functionName}`);
                });
            },
        );

    }

    return {
        bytecode,
        comments,
    };

}
