import { control } from "./ops";
import { createSetArgs } from "./ops/CreateSetArgs";
import { ArguableOp, CallOp, GlobalOp, LocalOp, OP } from "./ops/OpsTypes";
import { GetType, getType, GetTypes, HasType, WAType } from "./Types";
import { randomName } from "./utils/RandomWasmName";


export type Local<NAME extends string, TYPE extends WAType> = {
    localName: NAME,
    type: TYPE,
    get: LocalOp<NAME, TYPE>,
    set: LocalOp<NAME, TYPE> & ArguableOp,
    tee: LocalOp<NAME, TYPE> & ArguableOp,
}

export type LabelRef = {
    name: string,
}

type FunctionBase<
    NAME extends string,
    RESULT extends WAType[],
    PARAM_TYPE extends {type: WAType}
> = {
    call: CallOp & ArguableOp,
    functionName: NAME,
    result: RESULT,
    params: PARAM_TYPE[],
}

export type FullyDefinedFunction<NAME extends string, RESULT extends WAType[]> =
    FunctionBase<NAME, RESULT, Local<string, WAType>> & {
    isExported: boolean,
    locals: Local<string, WAType>[],
    ops: OP[],
};

export type Import<NAME extends string, RESULT extends WAType[]> =
    FunctionBase<NAME, RESULT, {type: WAType}> & {
    path: Path,
}

export type FunctionRef<NAME extends string, RESULT extends WAType[]> =
    | FullyDefinedFunction<NAME, RESULT>
    | Import<NAME, RESULT>
;


type LocalsToMap<
    LOCALS extends readonly Local<string, WAType>[],
    DEST extends Record<string, WAType> = {},
    COUNTER extends void[] = []
> = COUNTER["length"] extends LOCALS["length"]
    ? DEST
    : LocalsToMap<
        LOCALS,
        DEST & Record<
            LOCALS[COUNTER["length"]]["localName"],
            LOCALS[COUNTER["length"]]
        >,
        [...COUNTER, void]
    >

type FuncBuilder<
    NAME extends string,
    LOCALS extends readonly Local<string, WAType>[],
    RESULT extends WAType[]
> = {
    name<NEW_NAME extends string>(
        newName: NEW_NAME
    ): FuncBuilder<NEW_NAME, LOCALS, RESULT>,
    exported(isExported: boolean): FuncBuilder<NAME, LOCALS, RESULT>,
    param<
        const K extends string,
        const T extends HasType
    >(name: K, type: T): FuncBuilder<
        NAME,
        [...LOCALS, Local<K, GetType<T>>],
        RESULT
    >,
    local<
        const K extends string,
        const T extends HasType
    >(name: K, type: T): FuncBuilder<
        NAME,
        [...LOCALS, Local<K, GetType<T>>],
        RESULT
    >,
    result<
        NEW_RESULT extends HasType[]
    >(
        ...type: NEW_RESULT
    ): FuncBuilder<
        NAME,
        LOCALS,
        GetTypes<NEW_RESULT>
    >,
    body<const OPS extends readonly (OP | OP[])[]>(
        body: (locals: LocalsToMap<LOCALS>) => OPS
    ): FullyDefinedFunction<NAME, RESULT>,
}

export function func<
    NAME extends string,
>(): FuncBuilder<
    NAME,
    [],
    []
    > {

    const defaultName = randomName();
    let localsIdx = 0;
    // @ts-ignore
    const resultFunc: FullyDefinedFunction<NAME> = {
        isExported: false,
        // @ts-ignore
        functionName: defaultName,
        params: [],
        result: [],
        locals: [],
        ops: [],
    };
    resultFunc.call = createSetArgs(control.call(resultFunc));
    function createLocalRef<NAME extends string, TYPE extends WAType>(
        localName: NAME,
        type: TYPE,
    ): Local<NAME, TYPE> {
        // @ts-ignore
        const ref: Local = {
            localName,
            type,
            index: localsIdx++,
        };
        ref.get = {
            title: "local.get",
            opcode: 0x20,
            local: ref,
            args: [],
        };
        ref.set = createSetArgs({
            title: "local.set",
            opcode: 0x21,
            local: ref,
        });
        ref.tee = createSetArgs({
            title: "local.tee",
            opcode: 0x22,
            local: ref,
        });
        return ref;
    }
    const res: FuncBuilder<
        NAME,
        [],
        []
    > = {
        // @ts-ignore
        name<NEW_NAME extends string>(newName: NEW_NAME) {
            // @ts-ignore
            resultFunc.functionName = newName;
            return res;
        },
        exported(isExported: boolean) {
            resultFunc.isExported = isExported;
            return res;
        },
        // @ts-ignore
        param<K extends string>(name: K, type: HasType) {
            resultFunc.params.push(createLocalRef(name, getType(type)));
            return res;
        },
        // @ts-ignore
        local<K extends string>(name: K, type: HasType) {
            resultFunc.locals.push(createLocalRef(name, getType(type)));
            return res;
        },
        // @ts-ignore
        result<NEW_RESULT extends HasType[]>(...type: NEW_RESULT) {
            resultFunc.result = type.map(getType);
            return res;
        },
        // @ts-ignore
        body(body) {
            const locals = Object.fromEntries(
                [...resultFunc.locals, ...resultFunc.params].map((l) => [l.localName, l]),
            );
            resultFunc.ops = body(locals).flat();
            return resultFunc;
        },
    };
    return res;
}

type Path = [string, string];

type Memory = {path: Path; sizePages: number} | undefined

export type Global<NAME extends string, TYPE extends WAType> = {
    index: number,
    path: Path,
    globalName: NAME,
    type: TYPE,
    mutable: boolean,
    get: GlobalOp<NAME, TYPE>,
    set: GlobalOp<NAME, TYPE> & ArguableOp,
}

export type Module = {
    memory: Memory,
    imports: Import<string, WAType[]>[],
    globals: Global<string, WAType>[],
    functions: FullyDefinedFunction<string, WAType[]>[],
}

type ModuleBuilder<
    GLOBALS extends readonly Global<string, WAType>[],
    FUNCTIONS extends readonly FunctionRef<string, WAType[]>[],
> = {
    memory(
        path: Path,
        sizePages: number
    ): ModuleBuilder<GLOBALS, FUNCTIONS>,
    import<NAME extends string, TYPE extends HasType[]>(
        path: Path,
        name: NAME,
        params: HasType[],
        result?: TYPE
    ): ModuleBuilder<GLOBALS, [...FUNCTIONS, FunctionRef<NAME, GetTypes<TYPE>>]>,
    global<NAME extends string, TYPE extends HasType>(
        path: Path,
        name: NAME,
        type: TYPE,
        mutable: boolean,
    ): ModuleBuilder<[...GLOBALS, Global<NAME, GetType<TYPE>>], FUNCTIONS>,
    func<NAME extends string, TYPE extends WAType[]>(
        build: (
            globals: Record<GLOBALS[number]["globalName"], Global<string, WAType>>,
            functions: Record<FUNCTIONS[number]["functionName"], FunctionRef<string, WAType[]>>,
        ) => FullyDefinedFunction<NAME, TYPE>,
    ): ModuleBuilder<GLOBALS, [...FUNCTIONS, FunctionRef<NAME, TYPE>]>,
    build(): Module,
}

export function createModule(): ModuleBuilder<[], []> {

    const resultModule: Module = {
        memory: undefined,
        imports: [],
        globals: [],
        functions: [],
    };
    const funcsObj = {};
    const funcNames = new Set<string>();
    const globalsObj = {};
    // @ts-ignore
    const res: ModuleBuilder<[], []> = {
        memory(
            path: Path,
            sizePages: number,
        ) {
            resultModule.memory = { path, sizePages };
            return res;
        },
        // @ts-ignore
        import<NAME extends string, RESULT extends HasType[]>(
            path: Path,
            functionName: NAME,
            params: HasType[],
            result?: RESULT,
        ) {
            const imp: Import<NAME, GetTypes<RESULT>> = {
                functionName,
                // @ts-ignore
                result: result?.map(getType) || [],
                params: params.map((p) => ({ type: getType(p) })),
                path,
            };
            // @ts-ignore
            imp.call = control.call(imp);
            // @ts-ignore
            funcsObj[functionName] = imp;
            if (funcNames.has(functionName)) {
                throw new Error(`Redeclaration of ${functionName}`);
            } else {
                funcNames.add(functionName);
            }
            resultModule.imports.push(imp);
            return res;
        },
        // @ts-ignore
        global<NAME extends string, TYPE extends HasType>(
            path: Path,
            globalName: NAME,
            type: TYPE,
            mutable: boolean,
        ) {
            // @ts-ignore
            const glob: Global<NAME, GetType<TYPE>> = {
                index: resultModule.globals.length,
                path,
                globalName,
                type: getType(type),
                mutable,
            };
            glob.get = {
                title: "global.get",
                opcode: 0x23,
                global: glob,
                args: [],
            };
            glob.set = createSetArgs({
                title: "global.set",
                opcode: 0x24,
                global: glob,
            });
            resultModule.globals.push(glob);
            // @ts-ignore
            globalsObj[globalName] = glob;
            return res;
        },
        // @ts-ignore
        func<NAME extends string, RESULT extends WAType[]>(
            build: (
                globals: any,
                functions: any,
            ) => FullyDefinedFunction<NAME, RESULT>,
        ) {
            const f = build(
                globalsObj,
                funcsObj,
            );
            if (funcNames.has(f.functionName)) {
                throw new Error(`Redeclaration of ${f.functionName}`);
            } else {
                funcNames.add(f.functionName);
            }
            // @ts-ignore
            funcsObj[f.functionName] = f;
            resultModule.functions.push(f);
            return res;
        },
        build(): Module {
            return resultModule;
        },
    };
    return res;

}
