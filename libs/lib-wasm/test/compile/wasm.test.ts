import { beforeEach, describe, it } from "node:test";

import { byteToString } from "@audio-playground/lib-common/std/byteToString";
import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertArraysAreEqual } from "@audio-playground/lib-test-utils/assertArraysAreEqual";

import { compileModuleToWASM } from "../../src/CompileWASM";
import { funcsModule } from "./code";

describe("compile module -> wasm", () => {
    beforeEach(monkeyPatch);

    it("module -> wasm", async() => {
        const code: number[] = [
            0x00, 0x61, 0x73, 0x6d, // magic
            0x01, 0x00, 0x00, 0x00, // version

            /* 9 */ 0x01, // type section
            0x0e, // 14 bytes
            0x02, // 2 functions

            /* 12 */ 0x60,
            0x02, // 2 inputs
            0x7c, 0x7c,
            0x01, // 1 output
            0x7c,

            /* 18 */ 0x60,
            0x03, // 3 inputs
            0x7c, 0x7c, 0x7f,
            0x01, // 1 output
            0x7c,

            /* 25 */ 0x03, // function section
            0x04, // 4 bytes
            0x03, // 3 functions
            0x00, 0x00, 0x01, // indices of signatures

            /* 31 */ 0x07, // export section
            0x08, // 8 bytes
            0x01, // 1 export
            0x04, // 4 letters
            0x64, 0x6f, 0x4f, 0x70, // ascii name of exported function
            0x00, // export by func index
            0x02, // index of function

            /* 41 */ 0x0a, // code section
            0x26, // 26 bytes
            0x03, // 3 functions

            /* 52 */ 0x07, // function size
            0x00, // 0 locales
            0x20, 0x00, // local.get 0
            0x20, 0x01, // local.get 1
            0xa2, // f64.mul
            0x0b, // end

            /* 44 */ 0x07, // function size
            0x00, // 0 locals
            0x20, 0x00, // local.get 0
            0x20, 0x01, // local.get 1
            0xa0, // f64.add
            0x0b, // end

            /* 60 */ 0x14, // function size
            0x00, // 0 locales
            0x20, 0x02, // local.get 2

            /* 64 */ 0x04, 0x7c, // if of type f64
            0x20, 0x00, // local.get 0
            0x20, 0x01, // local.get 1
            0x10, 0x01, // call 1
            0x05, // else
            0x20, 0x00, // local.get 0
            0x20, 0x01, // local.get 1
            0x10, 0x00, // call 0
            0x0b, // end

            0x0b, // end

        ];

        const compiled = compileModuleToWASM(
            funcsModule,
        );
        assertArraysAreEqual(
            compiled.bytecode,
            code,
            byteToString,
        );

    });

});
