import { createModule, func } from "../../src/Op";
import { control, f64, i32 } from "../../src/ops";


const plus = func()
    .name("plus")
    .param("a", f64)
    .param("b", f64)
    .result(f64)
    .body((locals) => [
        locals.a.get,
        locals.b.get,
        f64.add,
    ]);

const times = func()
    .name("times")
    .param("a", f64)
    .param("b", f64)
    .result(f64)
    .body((locals) => [
        locals.a.get,
        locals.b.get,
        f64.mul,
    ]);

const doOp = func()
    .name("doOp")
    .exported(true)
    .param("a", f64)
    .param("b", f64)
    .param("op", i32)
    .result(f64)
    .body((locals) => [
        locals.op.get,
        control.if({
            type: f64,
            then: [
                locals.a.get,
                locals.b.get,
                control.call(plus),
            ],
            else: [
                locals.a.get,
                locals.b.get,
                control.call(times),
            ],
        }),
    ]);

export const funcsModule = createModule()
    .func(() => doOp)
    .build();
