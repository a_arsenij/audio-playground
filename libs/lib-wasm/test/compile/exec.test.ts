import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { fail } from "assert";
import * as assert from "assert";

import { compileModuleToWASM } from "../../src/CompileWASM";
import { createModule, func } from "../../src/Op";
import { f32x4, f64, i32, v128 } from "../../src/ops";
import { funcsModule } from "./code";

describe("exec", () => {
    beforeEach(monkeyPatch);

    it("call func", async() => {

        const compiled = compileModuleToWASM(
            funcsModule,
        );

        const obj = await WebAssembly.instantiate(
            new Uint8Array(compiled.bytecode),
        );
        const func = obj.instance.exports.doOp;
        if (!func) {
            fail("No func found by name doOp");
        }
        if (typeof func !== "function") {
            fail("doOp is not a func");
        }

        assert.strictEqual(
            func(2, 3, 0), 6,
            "2*3=6: doOp wasm function operates as expected",
        );
        assert.strictEqual(
            func(2, 3, 1), 5,
            "2+3=5: doOp wasm function operates as expected",
        );
        assert.strictEqual(
            func(4, 5, 0), 20,
            "4*5=20: doOp wasm function operates as expected",
        );
        assert.strictEqual(
            func(4, 5, 1), 9,
            "4+5=9: doOp wasm function operates as expected",
        );

    });


    it("globals", async() => {

        const global = new WebAssembly.Global({ value: "f64", mutable: true }, 0);

        const module = createModule()
            .global(["js", "global"], "global", f64, true)
            .func((globals) => func()
                .exported(true)
                .name("f2call")
                .param("value", f64)
                .body((locals) => {
                    return [
                        globals.global.set(
                            f64.mul(
                                locals.value.get,
                                locals.value.get,
                            ),
                        ),
                    ];
                }),

            )
            .build();

        const compiled = compileModuleToWASM(
            module,
        );

        const importObject = {
            js: { global },
        };

        const obj = await WebAssembly.instantiate(
            new Uint8Array(compiled.bytecode),
            importObject,
        );
        const f2call = obj.instance.exports.f2call as (v: number) => void;

        f2call(4);
        assert.strictEqual(
            global.value, 16,
            "4*4=16 is put in global",
        );
        f2call(3);
        assert.strictEqual(
            global.value, 9,
            "3*3=9 is put in global",
        );

    });


    it("imports", async() => {

        const module = createModule()
            .import(["js", "set"], "set", [f64])
            .func((_, functions) => func()
                .exported(true)
                .name("f2call")
                .param("value", f64)
                .body((locals) => {
                    return [
                        functions.set.call(
                            f64.mul(
                                locals.value.get,
                                locals.value.get,
                            ),
                        ),
                    ];
                }),

            )
            .build();

        const compiled = compileModuleToWASM(
            module,
        );

        let stored: number = 0;
        const importObject = {
            js: {
                set: (v: number) => {
                    stored = v;
                },
            },
        };

        const obj = await WebAssembly.instantiate(
            new Uint8Array(compiled.bytecode),
            importObject,
        );
        const f2call = obj.instance.exports.f2call as (v: number) => void;

        f2call(4);
        assert.strictEqual(
            stored, 16,
            "4*4=16 is passed via imported func",
        );
        f2call(3);
        assert.strictEqual(
            stored, 9,
            "3*3=9 is passed via imported func",
        );

    });


    it("memory", async() => {


        const module = createModule()
            .memory(["js", "mem"], 1)
            .func(() => func()
                .exported(true)
                .name("f2call")
                .param("index", i32)
                .param("value", f64)
                .body((locals) => [
                    f64.store(0)(
                        locals.index.get,
                        f64.mul(
                            locals.value.get,
                            locals.value.get,
                        ),
                    ),
                ]),

            )
            .build();

        const compiled = compileModuleToWASM(
            module,
        );

        const memory = new WebAssembly.Memory({ initial: 1 });

        const importObject = {
            js: { mem: memory },
        };

        const obj = await WebAssembly.instantiate(
            new Uint8Array(compiled.bytecode),
            importObject,
        );
        const f2call = obj.instance.exports.f2call as (idx: number, v: number) => void;
        const view = new DataView(memory.buffer);

        f2call(0, 4);
        f2call(13, 3);

        assert.strictEqual(
            view.getFloat64(0, true), 16,
            "4*4=16 is stored at offset=0",
        );
        assert.strictEqual(
            view.getFloat64(13, true), 9,
            "3*3=9 is stored at offset=13",
        );

    });

    it("vectors", async() => {


        const module = createModule()
            .memory(["js", "mem"], 1)
            .func(() => func()
                .exported(true)
                .name("f2call")
                .body((_locals) => [
                    i32.const(0),
                    i32.const(0),
                    v128.load(0),
                    i32.const(0),
                    v128.load(16),
                    f32x4.add,
                    v128.store(32),
                ]),

            )
            .build();

        const compiled = compileModuleToWASM(
            module,
        );

        const memory = new WebAssembly.Memory({ initial: 1 });

        const importObject = {
            js: { mem: memory },
        };

        const obj = await WebAssembly.instantiate(
            new Uint8Array(compiled.bytecode),
            importObject,
        );
        const f2call = obj.instance.exports.f2call as () => void;
        const view = new DataView(memory.buffer);

        const a = [1, 2, 3, 4];
        const b = [5, 6, 7, 8];
        const c = a.map((aa, idx) => aa * b[idx]!);
        a.forEach((a, idx) => {
            view.setFloat32(idx * 4, a, true);
        });
        b.forEach((b, idx) => {
            view.setFloat32(16 + idx * 4, b, true);
        });
        f2call();

        c.forEach((cc, idx) => {
            const val = view.getFloat32(32 + idx * 4, true);
            assert.strictEqual(
                val,
                a[idx]! + b[idx]!,
                `${a[idx]}+${b[idx]}=${cc}`,
            );
        });

    });

});
