import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertStringsAreEqual } from "@audio-playground/lib-test-utils/assertStringsAreEqual";

import { compileModuleToWAT } from "../../src/CompileWAT";
import { funcsModule } from "./code";

describe("compile module -> wat", () => {
    beforeEach(monkeyPatch);

    it("module -> wat", () => {
        const text = `
(module

;; Functions
(func $times
    (param $a f64)
    (param $b f64)
    (result f64)
    local.get $a
    local.get $b
    f64.mul
)
(func $plus
    (param $a f64)
    (param $b f64)
    (result f64)
    local.get $a
    local.get $b
    f64.add
)
(func $doOp (export "doOp")
    (param $a f64)
    (param $b f64)
    (param $op i32)
    (result f64)
    local.get $op
    (if (result f64)
        (then
            local.get $a
            local.get $b
            call $plus
        )
        (else
            local.get $a
            local.get $b
            call $times
        )
    )
)

)
`;

        assertStringsAreEqual(
            compileModuleToWAT(funcsModule),
            text,
            {
                ignoreEmptyLines: true,
                ignoreTrailingLeadingSpaces: true,
            },
        );
    });

});
