import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertArraysAreEqual } from "@audio-playground/lib-test-utils/assertArraysAreEqual";

import { sintToLeb128, uintToLeb128 } from "../../src/LEB128";


describe("leb128", () => {
    beforeEach(monkeyPatch);

    it("unsigned int -> leb128", () => {

        const data = [
            { num: 64, expected: [64] },
            { num: 624485, expected: [0xE5, 0x8E, 0x26] },
            { num: 9019283812387n, expected: [0xa3, 0xe0, 0xd4, 0xb9, 0xbf, 0x86, 0x02] },
            { num: 0x2bc1, expected: [0xc1, 0x57] },
            { num: 0x9e00000, expected: [0x80, 0x80, 0x80, 0x4f] },
            { num: 0x7e00000, expected: [0x80, 0x80, 0x80, 0x3f] },
        ];


        data.forEach((d) => {
            assertArraysAreEqual(
                uintToLeb128(d.num),
                d.expected,
            );
        });

    });

    it("signed int -> leb128", () => {

        const data = [
            { num: 64, expected: [192, 0] },
            { num: -123456, expected: [0xc0, 0xbb, 0x78] },
            { num: -9019283812387n, expected: [0xdd, 0x9f, 0xab, 0xc6, 0xc0, 0xf9, 0x7d] },
            { num: -0x143f, expected: [0xc1, 0x57] },
            { num: -0x6200000, expected: [0x80, 0x80, 0x80, 0x4f] },
            { num: 0x7e00000, expected: [0x80, 0x80, 0x80, 0x3f] },
        ];


        data.forEach((d) => {
            assertArraysAreEqual(
                sintToLeb128(d.num),
                d.expected,
            );
        });



    });

});
