import { compileModuleToWASM } from "../../src/CompileWASM";
import { createModule, FullyDefinedFunction } from "../../src/Op";
import { WAType } from "../../src/Types";


export function getWasmUnaryFunction(
    func: FullyDefinedFunction<string, WAType[]>,
    memory?: Uint8Array,
): Promise<(...args: number[]) => number> {

    const oldIsExported = func.isExported;
    func.isExported = true;
    const myModule = createModule()
        .func(() => func)
        .build();

    return new Promise((resolve, reject) => {
        WebAssembly.instantiate(
            new Uint8Array(compileModuleToWASM(myModule).bytecode),
            {},
        ).then(async(obj) => {
            func.isExported = oldIsExported;
            const wasmFunc = obj.instance.exports[func.functionName];
            if (!wasmFunc) {
                console.error(`No ${func.functionName} found`);
                process.exit(1);
            }
            if (typeof wasmFunc !== "function") {
                console.error(`${func.functionName} is not a func`);
                process.exit(1);
            }
            // @ts-ignore
            resolve(wasmFunc);
        })
            .catch((e) => reject(e));
    });


}
