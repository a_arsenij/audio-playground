
import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import * as assert from "assert";

import { std } from "../../src/std/STD";
import { getWasmUnaryFunction } from "./_utils";


describe("std signum", () => {
    beforeEach(monkeyPatch);

    it("sign(x)", async() => {

        const sign = await getWasmUnaryFunction(std.f64.sign);

        const data = [
            10,
            -1,
            0,
            -0,
        ];
        const expected = [
            1,
            -1,
            1,
            -1,
        ];
        const actual = data.map((v) => {
            return sign(v);
        });
        assert.deepEqual(actual, expected);
    });
});
