
import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";
import { assertNumbersCloseRatio } from "@audio-playground/lib-test-utils/assertNumbersCloseRatio";

import { std } from "../../src/std/STD";
import { getWasmUnaryFunction } from "./_utils";


describe("std pow2", () => {
    beforeEach(monkeyPatch);

    it("pow2_01", async() => {

        const error = 6.6817e-11;

        const pow201 = await getWasmUnaryFunction(std.f64.pow201);
        for (let x = 0; x < 1; x += 0.001) {
            const native = 2 ** x;
            const wasm = pow201(x);
            assertNumbersClose(
                wasm,
                native,
                error,
                `pow2_01(${x})`,
            );
        }
    });

    it("pow2", async() => {

        const error = 5e-6;

        const pow2 = await getWasmUnaryFunction(std.f64.pow2);
        for (let x = -10; x < 10; x += 0.001) {
            const native = 2 ** x;
            const wasm = pow2(x);
            assertNumbersCloseRatio(
                wasm,
                native,
                error,
                `pow2(${x})`,
            );
        }
    });

});
