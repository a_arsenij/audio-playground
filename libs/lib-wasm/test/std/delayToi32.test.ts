
import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";

import { std } from "../../src/std/STD";
import { getWasmUnaryFunction } from "./_utils";



describe("delayToi32", () => {
    beforeEach(monkeyPatch);

    it("delayToi32", async() => {
        const error = 4.1e-7;

        const toI32 = await getWasmUnaryFunction(std.f64.delayToi32);

        function toI32Js(arg: number) {
            const a = isNaN(arg) ? 1 : arg;
            const b = Math.ceil(a);
            const c = Math.max(b, 1);
            return c;
        }

        const data = [
            0, 1, NaN, 10000, -10000,
        ];

        const res = [
            1, 1, 1, 10000, 1,
        ];
        data.forEach((x, i) => {
            const native = toI32Js(x);
            const wasm = toI32(x);

            assertNumbersClose(
                wasm,
                native,
                error,
                `toI32(${x})`,
            );
            assertNumbersClose(
                wasm,
                res[i]!,
                error,
                `toI32(${x})`,
            );
        });
    });

});
