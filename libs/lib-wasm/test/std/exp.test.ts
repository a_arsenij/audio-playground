
import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";
import { assertNumbersCloseRatio } from "@audio-playground/lib-test-utils/assertNumbersCloseRatio";

import { std } from "../../src/std/STD";
import { getWasmUnaryFunction } from "./_utils";


describe("std exp", () => {
    beforeEach(monkeyPatch);

    it("exp01", async() => {

        const precisions = [
            [4, 1e-2],
            [6, 1e-3],
            [8, 1e-5],
            [10, 1e-7],
            [12, 1e-9],
        ] as const;

        for (const p of precisions) {
            const exp = await getWasmUnaryFunction(std.f64.exp01(p[0]));
            for (let x = 0; x < 1; x += 0.001) {
                const native = Math.exp(x);
                const wasm = exp(x);
                assertNumbersClose(
                    wasm,
                    native,
                    p[1],
                    `exp01(${x}), ${p[0]} members`,
                );
            }
        }
    });

    it("exp", async() => {

        const precisions = [
            [4, 1e-2],
            [6, 1e-3],
            [8, 1e-4],
            [10, 1e-7],
            [12, 1e-9],
        ] as const;

        for (const p of precisions) {
            const exp = await getWasmUnaryFunction(std.f64.exp(p[0]));
            for (let x = 0; x < 100; x += 0.01) {
                const native = Math.exp(x);
                const wasm = exp(x);
                assertNumbersCloseRatio(
                    wasm,
                    native,
                    p[1],
                    `exp(${x}), ${p[0]} members`,
                );
            }
        }

    });


});
