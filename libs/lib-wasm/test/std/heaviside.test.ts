
import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import * as assert from "assert";

import { std } from "../../src/std/STD";
import { getWasmUnaryFunction } from "./_utils";


describe("std heaviside", () => {
    beforeEach(monkeyPatch);

    it("heaviside(x)", async() => {

        const func = await getWasmUnaryFunction(std.f64.heaviside);

        const data = [
            10,
            -1,
            0,
            -0,
        ];
        const expected = [
            1,
            0,
            0,
            0,
        ];
        const actual = data.map((v) => {
            return func(v);
        });
        assert.deepEqual(actual, expected);
    });
});
