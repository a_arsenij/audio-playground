import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";
import { assertNumbersCloseRatio } from "@audio-playground/lib-test-utils/assertNumbersCloseRatio";

import { std } from "../../src/std/STD";
import { getWasmUnaryFunction } from "./_utils";


describe("std trig", () => {
    beforeEach(monkeyPatch);

    it("sin", async() => {

        const sin = await getWasmUnaryFunction(std.f64.sin);

        for (let x = -10; x < 10; x += 0.01) {
            const native = Math.sin(x);
            const wasm = sin(x);
            assertNumbersClose(
                wasm,
                native,
                1e-8,
                `sin(${x})`,
            );
        }

    });

    it("cos", async() => {

        const cos = await getWasmUnaryFunction(std.f64.cos);

        for (let x = -10; x < 10; x += 0.01) {
            const native = Math.cos(x);
            const wasm = cos(x);
            assertNumbersClose(
                wasm,
                native,
                1e-8,
                `cos(${x})`,
            );
        }

    });

    it("tan", async() => {

        const tan = await getWasmUnaryFunction(std.f64.tan);

        for (let x = -10; x < 10; x += 0.01) {
            const native = Math.tan(x);
            const wasm = tan(x);
            assertNumbersCloseRatio(
                wasm,
                native,
                1e-3,
                `tan(${x})`,
            );
        }

    });

});
