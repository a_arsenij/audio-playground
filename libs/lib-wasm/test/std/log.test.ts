
import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";

import { std } from "../../src/std/STD";
import { getWasmUnaryFunction } from "./_utils";



describe("std log", () => {
    beforeEach(monkeyPatch);

    it("log2", async() => {
        const error = 4.1e-7;

        const log = await getWasmUnaryFunction(std.f64.log2);


        for (let x = 1; x < 100; x += 0.01) {
            const native = Math.log2(x);
            const wasm = log(x);

            assertNumbersClose(
                wasm,
                native,
                error,
                `log2(${x})`,
            );
        }
    });

    it("log10", async() => {
        const error = 1e-7;

        const ln = await getWasmUnaryFunction(std.f64.log10);


        for (let x = 1; x < 100; x += 0.01) {
            const native = Math.log10(x);
            const wasm = ln(x);

            assertNumbersClose(
                wasm,
                native,
                error,
                `log10(${x})`,
            );
        }
    });

    it("logE", async() => {
        const error = 3e-7;

        const ln = await getWasmUnaryFunction(std.f64.logE);


        for (let x = 1; x < 100; x += 0.01) {
            const native = Math.log(x);
            const wasm = ln(x);

            assertNumbersClose(
                wasm,
                native,
                error,
                `logE(${x})`,
            );
        }
    });


});
