import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";

import { func } from "../../src/Op";
import { f64 } from "../../src/ops";
import { std } from "../../src/std/STD";
import { getWasmUnaryFunction } from "./_utils";

describe("std polynomial", () => {
    beforeEach(monkeyPatch);

    async function check(coefs: number[]) {
        const f = await getWasmUnaryFunction(
            func()
                .param("x", f64)
                .result(f64)
                .body((locals) => [
                    std.f64.polynomial(
                        locals.x,
                        coefs,
                    ),
                ]),
        );

        for (let x = -10; x < 10; x += 0.01) {
            const nat = coefs.map((coef, idx) =>
                coef * (x ** (coefs.length - idx - 1)),
            ).reduce((a, b) => a + b, 0);
            const wasm = f(x);
            assertNumbersClose(
                wasm,
                nat,
                1e-10,
                `poly(${x})`,
            );
        }
    }

    it("1", async() => {
        await check([1]);
    });

    it("2x+3", async() => {
        await check([2, 3]);
    });

    it("8*x^2+2*x+0.5", async() => {
        await check([8, 2, 0.5]);
    });

    it("5*x^4+4*x^3+3*x^2+2*x+1", async() => {
        await check([5, 4, 3, 2, 1]);
    });


});
