
import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersClose } from "@audio-playground/lib-test-utils/assertNumbersClose";
import { assertNumbersCloseRatio } from "@audio-playground/lib-test-utils/assertNumbersCloseRatio";

import { std } from "../../src/std/STD";
import { getWasmUnaryFunction } from "./_utils";


describe("std pow(a, x)", () => {
    beforeEach(monkeyPatch);

    it("pow (a , x) ; 0 < a < 1, 0 < x < 1", async() => {

        const error = 1e-6;

        const pow = await getWasmUnaryFunction(std.f64.pow);

        for (let a = 0; a <= 1; a += 0.125) {

            for (let x = 0; x <= 1; x += 0.125) {
                const native = a ** x;
                const wasm = pow(a, x);

                assertNumbersClose(
                    wasm,
                    native,
                    error,
                    `pow( ${a} , ${x} )`,
                );
            }
        }
    });

    it("pow (a , x) ; 1 < a < 10, 1 < x < 10", async() => {

        const error = 1e-5;

        const pow = await getWasmUnaryFunction(std.f64.pow);

        for (let a = 1; a <= 10; a += 0.1) {

            for (let x = 1; x <= 10; x += 0.1) {
                const native = a ** x;
                const wasm = pow(a, x);

                assertNumbersCloseRatio(
                    wasm,
                    native,
                    error,
                    `pow( ${a} , ${x} )`,
                );
            }
        }
    });

});
