

import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertNumbersCloseRatio } from "@audio-playground/lib-test-utils/assertNumbersCloseRatio";

import { std } from "../../src/std/STD";
import { getWasmUnaryFunction } from "./_utils";


describe("std primitive", () => {
    beforeEach(monkeyPatch);

    it("f64powI32", async() => {

        const f64powI32 = await getWasmUnaryFunction(std.f64.powI32);

        for (let x = -10; x < 10; x += 0.01) {
            for (let pow = 1; pow < 100; pow++) {
                const native = Math.pow(x, pow);
                const wasm = f64powI32(x, pow);
                assertNumbersCloseRatio(
                    wasm,
                    native,
                    1e-14,
                    `${x}^${pow}`,
                );
            }
        }

    });


});
