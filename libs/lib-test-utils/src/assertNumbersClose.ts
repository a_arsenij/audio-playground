import assert from "assert";

export function assertNumbersClose(
    actual: number,
    expected: number,
    epsilon: number = Number.EPSILON,
    message: string = "",
) {
    const diff = Math.abs(expected - actual);
    if (diff > epsilon) {
        assert(
            false,
            `${message}\nExpected: ${expected}, actual: ${actual}, epsilon: ${epsilon}, diff: ${diff}`,
        );
    }
}
