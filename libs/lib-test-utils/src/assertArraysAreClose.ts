import { areArraysClose } from "@audio-playground/lib-test-utils/areArraysClose";
import assert from "assert";

export function assertArraysAreClose<T extends number>(
    actual: ArrayLike<T>,
    expected: ArrayLike<T>,
    epsilon = 0.00001,
) {
    assert(areArraysClose(actual, expected, epsilon), `Actual: [ ${actual} ] Expected: [ ${expected} ]`);
}
