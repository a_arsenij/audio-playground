import { Complex } from "@audio-playground/lib-common/math/Complex";

export function areComplexArraysClose(a: Complex[], b: Complex[], epsilon = 0.00001) {
    if (a.length !== b.length) {
        return false;
    }
    for (let i = 0; i < a.length; i++) {
        const ai = a[i];
        const bi = b[i];
        if (!ai || !bi) {
            return false;
        }
        const aiReal = ai[0];
        const biReal = bi[0];
        const aiImag = ai[1];
        const biImag = bi[1];
        if (
            typeof aiReal !== "number" ||
            typeof biReal !== "number" ||
            typeof aiImag !== "number" ||
            typeof biImag !== "number"
        ) {
            return false;
        }
        if (isNaN(aiReal) || isNaN(biReal) || isNaN(aiImag) || isNaN(biImag)) {
            return false;
        }
        if (Math.abs(aiReal - biReal) > epsilon) {
            return false;
        }
        if (Math.abs(aiImag - biImag) > epsilon) {
            return false;
        }
    }
    return true;
}
