import { arrayOfNItems } from "@audio-playground/lib-common/std/Array";
import { plot } from "@audio-playground/lib-test-utils/plot";

export function testWithDifferentDataSizesAndPlot<I, O>(
    fun: (arg: I) => O,
    Ns: number[],
    createFunInput: (n: number) => I,
    uuseOutput: (funOut: O[]) => string,
) {

    const gc = global.gc
        ? global.gc
        : () => {
            console.warn("global.gc() not found. Cannot force garbage collection. Please run node with --expose_gc flag.");
        };

    const data: { x: number; y: number; type: string }[] = [];

    Ns.forEach((N) => {

        gc();

        const inputArray = arrayOfNItems(N, createFunInput);
        const outputArray = new Array(N);

        const t0 = performance.now();
        for (let i = 0; i < N; i++) {
            outputArray[i] = fun(inputArray[i]!);
        }
        ;
        const t1 = performance.now();


        data.push({
            x: N,
            y: (t1 - t0),
            type: "a",
        });

        console.log(`
        ${N} items
        `);
        console.log(`time: ${t1 - t0} ms`);
        console.info(`chore:${uuseOutput(outputArray)}`);

    });
    console.log(`
    `);
    console.log(plot(data));
    console.log(`
    `);
}
