


type Point = {
    x: number,
    y: number,
    type: string,
}

type Axis = {
    stepWX: number,
    stepWY: number,
    stepCX: number,
    stepCY: number,
    width: number,
    height: number,
}


function calcAxis(
    points: Point[],
): Axis {

    const WIDTH = 80;
    const HEIGHT = 20;

    const xs = points.map((p) => p.x);
    const ys = points.map((p) => p.y);
    const maxX = Math.max(0, ...xs);
    const maxY = Math.max(0, ...ys);

    const STEPS_X = 6;
    const STEPS_Y = 10;

    const stepWX = Math.round(maxX / STEPS_X);
    const stepWY = Math.round(maxY / STEPS_Y);
    const stepCX = Math.round(WIDTH * stepWX / maxX);
    const stepCY = Math.round(HEIGHT * stepWY / maxY);


    return {
        stepWX,
        stepWY,
        stepCX,
        stepCY,
        width: stepWX === 0 ? WIDTH : (Math.ceil(maxX / stepWX * stepCX) + 1),
        height: stepWY === 0 ? HEIGHT : (Math.ceil(maxY / stepWY * stepCY) + 1),
    };
}

const SHAPES = [
    "▲",
    "●",
    "■",
    "⬟",
    "◆",
];

function steps(swx: number, scx: number, size: number): number[] {
    if (scx === 0) {
        return [0];
    }
    const res: number[] = [];
    let wx = 0;
    let cx = 0;
    while (cx < size) {
        res.push(wx);
        wx += swx;
        cx += scx;
    }
    return res;
}

/**
 * @example
 * const data: Point[] = [
 *     { x: 1, y: 5, type: "a" },
 *     { x: 1, y: 10, type: "b" },
 *     { x: 2, y: 7, type: "a" },
 *     { x: 2, y: 15, type: "b" },
 * ];
 *
 * console.log( plot( data ) );
 */
export function plot(
    points: Point[],
): string {



    const axis = calcAxis(points);
    const xLabels = steps(axis.stepWX, axis.stepCX, axis.width);
    const yLabels = steps(axis.stepWY, axis.stepCY, axis.height);

    const xOffset = Math.max(...yLabels.map((v) => v.toString().length));
    const yOffset = 1;


    function w2cX(x: number): number {
        if (axis.stepWX === 0) {
            return xOffset;
        }
        return Math.round(x / axis.stepWX * axis.stepCX) + xOffset;
    }
    function w2cY(y: number): number {
        if (axis.stepWY === 0) {
            return yOffset;
        }
        return Math.round(y / axis.stepWY * axis.stepCY) + yOffset;
    }

    const canvas: string[][] = [...Array(axis.width + xOffset)].map(() =>
        [...Array(axis.height + yOffset)].map(() => " "),
    );

    const x0 = w2cX(0);
    const y0 = w2cY(0);

    for (let x = 0; x < axis.width + xOffset - 1; x++) {
        canvas[x]![y0] = "─";
    }
    for (let y = 0; y < axis.height + yOffset - 1; y++) {
        canvas[x0]![y] = "│";
    }
    canvas[x0]![y0] = "┼";

    function drawString(
        x: number,
        y: number,
        s: string,
        align: "left" | "right",
    ) {
        if (y < 0 || y >= (axis.height + yOffset)) {
            return;
        }
        let xx = align === "left" ? x : (x - s.length + 1);
        let idx = 0;
        while (idx < s.length) {
            if (
                xx >= 0 &&
                xx < (axis.width + xOffset)
            ) {
                canvas[xx]![y] = s[idx]!;
            }
            xx++;
            idx++;
        }
    }

    function xLabel(xx: number) {
        canvas[w2cX(xx)]![y0] = "┬";
        drawString(
            w2cX(xx),
            y0 - 1,
            xx.toString(),
            "left",
        );
    }
    xLabels.forEach(xLabel);

    function yLabel(yy: number) {
        canvas[x0]![w2cY(yy)] = "┤";
        drawString(
            x0 - 1,
            w2cY(yy),
            yy.toString(),
            "right",
        );
    }
    yLabels.forEach(yLabel);

    const kindToShape = new Map<string, number>();
    points.forEach((p) => {
        if (!kindToShape.has(p.type)) {
            kindToShape.set(p.type, kindToShape.size);
        }
        const shape = kindToShape.get(p.type)!;
        const cx = w2cX(p.x);
        const cy = w2cY(p.y);
        canvas[cx]![cy] = SHAPES[shape]!;
    });

    const plotLines = [...Array(axis.height + yOffset)].map((_, y) =>
        [...Array(axis.width + xOffset)].map((_, x) =>
            canvas[x]![axis.height + yOffset - y - 1],
        ).join(""),
    );

    const legendLine = `Legend:\n${[...kindToShape.entries()].map(([type, idx]) => `${type}: ${SHAPES[idx]!}`).join(" │ ")}`;

    return `${plotLines.join("\n")}\n\n${legendLine}`
    ;


}


