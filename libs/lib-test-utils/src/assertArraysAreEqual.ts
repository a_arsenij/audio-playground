import { padStart } from "@audio-playground/lib-common/std/String";
import { fail } from "assert";
import os from "os";

function formatDiff<T>(
    actual: T[],
    expected: T[],
    tToStr: (t: T) => string,
): string {
    const res: string[] = [];
    const size = Math.max(actual.length, expected.length);
    function toStr(
        el: T | undefined,
    ): string {
        return el === undefined ? "undefined" : tToStr(el);
    }
    for (let i = 0; i < size; i++) {
        const a = actual[i];
        const e = expected[i];
        res.push(`${padStart(i + 1, 8, " ")}: ${toStr(a)} ${toStr(e)} ${a !== e ? " <-- diff" : ""}`);
    }
    return res.join(os.EOL);
}

export function assertArraysAreEqual<T>(
    actual: T[],
    expected: T[],
    tToStr?: (t: T) => string,
    equals = (a: T, b: T) => a === b,
) {
    const elToStr = tToStr || ((s: T) => typeof s === "string" ? `"${s}"` : `${s}`);
    const size = Math.max(actual.length, expected.length);
    for (let i = 0; i < size; i++) {
        const a = actual[i]!;
        const b = expected[i]!;
        if (!equals(a, b)) {
            fail(
                `${os.EOL}LINENUM : ACTUAL EXPECTED${os.EOL}${formatDiff(actual, expected, elToStr)}`,
            );
            return;
        }
    }
}
