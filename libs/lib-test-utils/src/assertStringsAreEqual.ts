import { assertArraysAreEqual } from "@audio-playground/lib-test-utils/assertArraysAreEqual";

const emptyLineRegex = /^[ \t]*$/;
function skipEmptyLines(l: string[]): string[] {
    return l.filter((line) => {
        return !emptyLineRegex.test(line);
    });
}

export function assertStringsAreEqual(
    actual: string,
    expected: string,
    config?: {
        ignoreEmptyLines?: boolean,
        ignoreTrailingLeadingSpaces?: boolean,
    },
) {
    const ignoreEmptyLines = config?.ignoreEmptyLines || false;
    const ignoreTrailingLeadingSpaces = config?.ignoreTrailingLeadingSpaces || false;

    function prepare(l: string): string[] {
        let res = l.split("\n");
        if (ignoreEmptyLines) {
            res = skipEmptyLines(res);
        }
        if (ignoreTrailingLeadingSpaces) {
            res = res.map((l) => l.trim());
        }
        return res;
    }

    const a = prepare(actual);
    const e = prepare(expected);
    assertArraysAreEqual(a, e);
}
