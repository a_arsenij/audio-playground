export function generateRandomArray(length: number) {
    const res = new Float64Array(length);

    for (let i = 0; i < length; i++) {
        res[i] = Math.random() * 100;
    }

    return res;
}
