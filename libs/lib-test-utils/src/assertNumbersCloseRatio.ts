import assert from "assert";

export function assertNumbersCloseRatio(
    actual: number,
    expected: number,
    epsilon: number,
    message: string,
) {
    const diff = Math.abs((expected - actual) / expected);
    if (diff > epsilon) {
        assert(
            false,
            `${message}\nExpected: ${expected}, actual: ${actual}, epsilon: ${epsilon}, diff: ${diff}`,
        );
    }
}
