export function areArraysClose<T extends number>(
    a: ArrayLike<T>,
    b: ArrayLike<T>,
    epsilon = 0.00001,
) {
    if (a.length !== b.length) {
        return false;
    }
    for (let i = 0; i < a.length; i++) {
        const ai = a[i]!;
        const bi = b[i]!;
        if (isNaN(ai) || isNaN(bi)) {
            return false;
        }
        if (Math.abs(ai - bi) > epsilon) {
            return false;
        }
    }
    return true;
}
