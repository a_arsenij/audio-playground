import { plot } from "@audio-playground/lib-test-utils/plot";

export function testNTimesAndPlot<I, O>(
    fun: (arg: I) => O,
    N: number,
    createFunInput: (n: number) => I,
    uuseOutput: (funOut: O) => string,
) {

    const gc = global.gc
        ? global.gc
        : () => {
            console.warn("global.gc() not found. Cannot force garbage collection. Please run node with --expose_gc flag.");
        };

    const data = [];

    for (let j = 0; j < N; j++) {
        gc();

        const funInput = createFunInput(j);

        const t0 = performance.now();
        const output = fun(funInput);
        const t1 = performance.now();


        console.info(`chore:${uuseOutput(output)}`);

        console.log(`time: ${t1 - t0} ms`);
        data.push({
            x: j + 1,
            y: (t1 - t0),
            type: "a",
        });
    }
    console.log(plot(data));
}
