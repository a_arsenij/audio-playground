import {
    codeConfigMatch,
    codeSpecificNoRestrictedSyntax,
    commonNoRestrictedSyntax, commonRules,
    initEslint,
    testConfig
} from "../eslint-commons.config.mjs";

export function generateCommonLibConfig(name) {
    return [
        initEslint,
        {
            ...codeConfigMatch,
            rules: {
                ...commonRules,
                "no-restricted-syntax": [
                    "error",
                    ...codeSpecificNoRestrictedSyntax,
                    ...commonNoRestrictedSyntax,
                    {
                        message: "Do not use own stuff as if it is in some lib",
                        selector: `ImportDeclaration[source.value=/\u002F${name}\u002F/]`,
                    },
                ],
            },
        },
        testConfig
    ];
}
