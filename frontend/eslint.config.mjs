import { fixupPluginRules } from "@eslint/compat";
import react from "eslint-plugin-react";
import reactHooks from "eslint-plugin-react-hooks";
import {
    codeConfigMatch,
    codeSpecificNoRestrictedSyntax,
    commonNoRestrictedSyntax,
    commonRules,
    initEslint,
    testConfig
} from "../eslint-commons.config.mjs";

export default [
    initEslint,
    {
        ...codeConfigMatch,
        ignores: [...codeConfigMatch.ignores, "**/components/Common/GeneratedIcons/**/*"],
        plugins: {
            react,
            "react-hooks": fixupPluginRules(reactHooks),
        },
        rules: {
            ...commonRules,
            "no-restricted-syntax": [
                "error",
                ...codeSpecificNoRestrictedSyntax,
                ...commonNoRestrictedSyntax,
            ],
            "react/jsx-equals-spacing": ["error", "never"],
            "react-hooks/rules-of-hooks": "error",
            "react-hooks/exhaustive-deps": "error",
        },
    },
    testConfig
];
