import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";
import resolve, { nodeResolve } from "@rollup/plugin-node-resolve";
import replace from "@rollup/plugin-replace";
import typescript from "@rollup/plugin-typescript";
import crypto from "crypto";
import fs from "fs";
import path from "path";
import { Plugin, RollupOptions } from "rollup";
import copy from "rollup-plugin-copy";
import postcss from "rollup-plugin-postcss";

function importOwdPresetAsUrl(): Plugin {
    const presetsDir = path.resolve("build/presets");
    if (!fs.existsSync(presetsDir)) {
        fs.mkdirSync(presetsDir);
    }
    return {
        name: "importOwdPresetAsUrl",
        resolveId(
            source: string,
            from: string | undefined,
        ) {
            if (source.endsWith(".owdpreset")) {
                const fromPath = path.parse(from ?? "");
                const full = path.resolve(fromPath.dir, source);
                const file = fs.readFileSync(full, { encoding: "utf8" });
                const hash = crypto.createHash("sha1");
                hash.update(file);
                const digest = hash.digest("hex").substring(0, 8);
                const split = source.split("/").at(-1)!.split(".");
                const newId = `${split.at(-2)}-${digest}.${split.at(-1)}`;
                fs.copyFileSync(
                    full,
                    path.resolve(`build/presets/${newId}`),
                );
                return newId;
            }
            return null;
        },
        load(id) {
            if (id.endsWith(".owdpreset")) {
                return `export default "${id}";`;
            }
            return null;
        },
    };
}

const uiChunk: RollupOptions = {
    input: "src/main.ts",
    output: {
        file: "build/dist.js",
        format: "iife",
        sourcemap: true,
    },
    plugins: [
        copy({
            targets: [{
                src: "index.html",
                dest: "build/",
            }],
        }),
        resolve(),
        commonjs(),
        importOwdPresetAsUrl(),
        typescript({
            filterRoot: false,
        }),
        json(),
        replace({
            preventAssignment: true,
            "process.env.NODE_ENV": JSON.stringify("development"),
        }),
        nodeResolve({
            preferBuiltins: false,
        }),
        postcss({
            inject: false,
            modules: true,
            extract: "bundle.css",
        }),
    ].filter((a) => a),
};

export default () => {
    return [
        uiChunk,
    ];
};
