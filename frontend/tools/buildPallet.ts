import fs from "fs";

// paths
const TARGET_FILE_PATH = "src/pallet.css";
const FIXED_COLORS_PATH = "assets/figmaTokens/fixed-colors.json";
const SEMANTIC_COLORS_PATH = "assets/figmaTokens/semantic-colors.json";

// templates stuff
const NEW_LINE = "\n\t";
const CSS_FIXED_COLOR_PREFIX = "--color-";
const CSS_SEMANTIC_COLOR_PREFIX = "--semantic-";
const PREFIX = `/*                 
 *       
 *     ┌──────────────────────────────────────────────────┐
 *     │   THIS FILE IS AUTO-GENERATED.                   │ 
 *     │   PLEASE DO NOT MODIFY IT DIRECTLY.              │ 
 *     │   INSTEAD CONSIDER ONE OF THESE OPTIONS:         │
 *     │      1. Import new tokens from figma to          │
 *     │         located at ~/frontend/assets/figmaTokens │
 *     │      2. Modify the file template                 │
 *     │         at ~/frontend/tools/buildPallet.ts       │
 *     └──────────────────────────────────────────────────┘ 
 *
 */`;

fs.rmSync(
    TARGET_FILE_PATH,
    { force: true },
);

function main(targetPath: string) {
    let fixedColorsJson: { fixedcolors: Record<string, string>} | undefined;
    let semanticColorsJson: { semanticcolors: { light: Record<string, string>; dark: Record<string, string> } } | undefined;
    try {
        fixedColorsJson = JSON.parse(fs.readFileSync(FIXED_COLORS_PATH, "utf8"), removeRedundantNodes);
        semanticColorsJson = JSON.parse(fs.readFileSync(SEMANTIC_COLORS_PATH, "utf8"), removeRedundantNodes);
        if (!fixedColorsJson?.fixedcolors) {
            throw Error(`File ${FIXED_COLORS_PATH} was not parsed`);
        }
        if (!semanticColorsJson?.semanticcolors) {
            throw Error(`File ${SEMANTIC_COLORS_PATH} was not parsed`);
        }
    } catch (error) {
        console.error("Error during building a css pallet: ", error);
        return;
    }

    const replaceColorsByAliases = getAliasesFrom(fixedColorsJson.fixedcolors);
    fs.writeFileSync(
        `${targetPath}`,
        `${PREFIX}
        ${(JsonToCssBlock("body", fixedColorsJson.fixedcolors))}
        ${(JsonToCssBlock("light", replaceColorsByAliases(semanticColorsJson.semanticcolors.light)))}
        ${(JsonToCssBlock("dark", replaceColorsByAliases(semanticColorsJson.semanticcolors.dark)))}
        `,
    );
}
main(TARGET_FILE_PATH);

type InnerFn = (semanticColors: Record<string, string>) => Record<string, string>
function getAliasesFrom(fixedColors: Record<string, string>): InnerFn {
    const _fn: InnerFn = (semanticColors) => {
        // reverse colors
        const colorToAlias = new Map<string, string>();
        for (const colorAlias in fixedColors) {
            const color = fixedColors[colorAlias];
            if (color) {
                colorToAlias.set(color, colorAlias);
            }
        }

        const result: Record<string, string> = {};
        for (const semanticKey in semanticColors) {
            const semanticColor = semanticColors[semanticKey]!;
            if (colorToAlias.has(semanticColor)) {
                result[semanticKey] = `var(${CSS_FIXED_COLOR_PREFIX}${colorToAlias.get(semanticColor)!})`;
            } else {
                result[semanticKey] = semanticColor;
            }
        }

        return result;
    };
    return _fn;
}

function transformHsla(input: string): string {
    const hslaRegex = /^hsla\((\d+)\s(\d+%)\s(\d+%)\s\/\s([\d.]+)\)$/;

    const match = input.match(hslaRegex);

    if (!match) {
        throw new Error("Invalid HSLA format");
    }

    const [, hue, saturation, lightness, alpha] = match;

    return `hsla(${hue},${saturation},${lightness},${alpha})`;
}

type ColorNode = {
  "$type": "color",
  "$value": string,
}
function isColorNode(value: unknown): value is ColorNode {
    if (!value) {
        return false;
    }
    if (!(typeof value === "object")) {
        return false;
    }
    return "$type" in value && value.$type === "color" && "$value" in value;
}

function removeRedundantNodes(this: unknown, key: unknown, value: unknown): unknown {
    if (isColorNode(value)) {
        return `${transformHsla(value.$value)}`;
    }

    return value;
}

type CssSelectors = "body" | "light" | "dark";
function JsonToCssBlock(jsonBlock: CssSelectors, json: Record<string, string>): string {
    const BLOCK_TO_CSS_SELECTOR = {
        body: "body",
        light: "[data-theme='day']",
        dark: "[data-theme='night']",
    };
    const VAR_PREFIX = {
        body: CSS_FIXED_COLOR_PREFIX,
        light: CSS_SEMANTIC_COLOR_PREFIX,
        dark: CSS_SEMANTIC_COLOR_PREFIX,
    };
    return `
${BLOCK_TO_CSS_SELECTOR[jsonBlock]} {${NEW_LINE}${Object.entries(json).map(([property, value]) => `${VAR_PREFIX[jsonBlock]}${property}: ${value};`).join(NEW_LINE)}
}
`;
}
