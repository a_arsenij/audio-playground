import fs from "fs";
import { JSDOM } from "jsdom";
import { optimize } from "svgo";

const TARGET_FOLDER = "src/components/Common/GeneratedIcons";

fs.rmSync(
    TARGET_FOLDER,
    { recursive: true, force: true },
);

fs.mkdirSync(TARGET_FOLDER);

function toCamelCase(
    s: string,
    firstCapital: boolean,
): string {
    if (s.match(/([a-z]+[-_])+[a-z]+/)) {
        let shouldBeCapital = firstCapital;
        return s.split("").map((c) => {
            if (c === "-" || c === "_") {
                shouldBeCapital = true;
                return "";
            }
            let cc = c;
            if (shouldBeCapital) {
                cc = cc.toUpperCase();
            }
            shouldBeCapital = false;
            return cc;
        }).join("");
    }
    if (s.match(/[a-z]+/)) {
        let f = s[0]!;
        if (firstCapital) {
            f = f.toUpperCase();
        }
        return f + s.substring(1);
    }
    return s;
}

function restyle(style: string): string {
    let v = style.split("=")[1]!.substring(1);
    v = v.substring(0, v.length - 1);
    const vals = v.split(";");
    return `style={{\n${
        vals
            .map((v) => v.split(":"))
            .map(([k, v]) => `        ${(toCamelCase(k!, false))}: "${v}"`)
            .join(",\n")
    }\n    }}`;
}

const PREFIX = `/*                 
 *       
 *     ┌──────────────────────────────────────────────────┐
 *     │   THIS FILE IS AUTO-GENERATED.                   │ 
 *     │   PLEASE DO NOT MODIFY IT DIRECTLY.              │ 
 *     │   INSTEAD CONSIDER ONE OF THIS OPTIONS:          │
 *     │      1. Modify source svg file                   │
 *     │         located at ~/frontend/assets/            │
 *     │      2. Modify file template                     │
 *     │         at ~/frontend/tools/buildSvgComponent.ts │
 *     └──────────────────────────────────────────────────┘ 
 *
 */`;

const COMMON_PREFIX = "assets/icons/";
function handleSvg(filename: string) {
    let component = optimize(
        fs.readFileSync(COMMON_PREFIX + filename, { encoding: "utf-8" }),
    ).data;


    const xmlDoc = new JSDOM(component);
    const svg = xmlDoc.window.document.getElementsByTagName("svg")[0]!;
    const w = svg.getAttribute("width");
    const h = svg.getAttribute("height");
    if (w && h) {
        //svg.removeAttribute("width");
        //svg.removeAttribute("height");
        svg.setAttribute("viewBox", `0 0 ${parseInt(w)} ${parseInt(h)}`);
    }
    if (svg.children.length !== 1 || svg.children[0]!.tagName.toLowerCase() !== "g") {
        const g = xmlDoc.window.document.createElement("g");
        [...svg.children].forEach((c) => {
            svg.removeChild(c);
            g.appendChild(c);
        });
        svg.appendChild(g);
    }
    component = svg.outerHTML;

    component = component.replaceAll(
        "#fff",
        "currentColor",
    );
    component = component.replaceAll(
        "white",
        "currentColor",
    );
    component = component.replaceAll(
        "#000",
        "currentColor",
    );
    component = component.replaceAll(
        "black",
        "currentColor",
    );
    component = component.replaceAll(
        /style="[^"]*"/g,
        restyle,
    );
    component = component
        .replaceAll(/xml:space="[^"]*"/g, "");

    component = component.replaceAll(
        "fill-rule",
        "fillRule",
    );
    component = component.replaceAll(
        "clip-rule",
        "clipRule",
    );
    component = component.replaceAll(
        "stroke-width",
        "strokeWidth",
    );

    component = component
        .replaceAll(
            "<svg",
            "<svg style={fillContainer ? {width: \"100%\", height: \"100%\"} : {}}",
        );

    const splitted = filename.split("/").filter((a) => a);
    let componentName = splitted.join("");
    componentName = componentName.substring(0, componentName.length - 4);
    componentName = `Svg${toCamelCase(componentName, true)}`;
    const jsx = `
    
${PREFIX}    
    
import React from "react";

type Props = {
    fillContainer?: boolean,
}    
    
export function ${componentName}({
    fillContainer,
}: Props) {
    return ${component}
}    
    
    `;
    const folderNameArray = (TARGET_FOLDER + filename).split("/");
    folderNameArray.pop();
    const folderName = folderNameArray.join("/");
    if (!fs.existsSync(folderName)) {
        fs.mkdirSync(folderName, { recursive: true });
    }
    fs.writeFileSync(
        `${folderName}/${splitted.at(-1)!.split(".")[0]}.tsx`,
        jsx,
    );
}

function recursive(root: string) {
    const files = fs.readdirSync(COMMON_PREFIX + root);
    files.forEach((f) => {
        const filename = `${root}/${f}`;
        const stat = fs.statSync(COMMON_PREFIX + filename);
        if (stat.isDirectory()) {
            recursive(filename);
        } else {
            if (filename.endsWith(".svg")) {
                handleSvg(filename);
            }
        }
    });
}
recursive("");
