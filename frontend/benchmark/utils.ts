import { it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/src/std/MonkeyPatching";


monkeyPatch();

export type Experiment<FIELD> = {
    title: string,
    prepare: () => FIELD,
    consume: (output: FIELD) => number,
    action: (input: FIELD) => void,
    times: number,
}

export async function measure<FIELD>(
    args: Experiment<FIELD>,
): Promise<number> {

    const field = args.prepare();

    await new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(undefined);
        }, 1);
    });

    const a = performance.now();
    args.action(field);
    const b = performance.now();

    const counter = args.consume(field);

    await new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(undefined);
        }, counter % 2);
    });

    return b - a;
}

export function experiment<FIELD>(
    args: Experiment<FIELD>,
): Experiment<FIELD> {
    return args;
}

export async function runExperiments(
    ...experiments: Experiment<any>[]
) {
    try {
        const measures = new Map<string, number[]>();
        const tasks: (() => Promise<void>)[] = [];
        experiments.forEach((e) => {
            measures.getOrPut(e.title, () => []);
        });
        experiments.forEach((e) => {
            for (let i = 0; i < e.times; i++) {
                tasks.push(async() => {
                    const t = await measure(e);
                    measures.get(e.title)!.push(t);
                });
            }
        });
        tasks.shuffle();
        let done = 0;
        for (const t of tasks) {
            await t();
            done++;
            console.log(`\n\nDone: ${done}/${tasks.length}`);
            measures.entries().toArray().forEach(([title, items]) => {
                printMeasure(title, items);
            });
        }
    } catch (e) {
        console.error(e);
        throw e;
    }
}



export function printMeasure(
    title: string,
    times: number[],
) {
    console.log(`${title}:`);
    const total = times.reduce((a, b) => a + b, 0);
    console.log(`    Total:    ${total.toFixed(3)} ms`);
    console.log(`    Measures: ${times.length}`);
    if (times.length !== 0) {
        console.log(`    Avg:      ${(total / times.length).toFixed(3)} ms`);

        const _2 = Math.round(2 / 10 * times.length);
        let counter = 0;
        const fine = times
            .toSorted()
            .map((v, idx) => {
                if (idx > _2 && idx + 1 + _2 < times.length) {
                    counter++;
                    return v;
                }
                return 0;
            })
            .reduce((a, b) => a + b);
        if (counter > 0) {
            console.log(`    80% avg:  ${(fine / counter).toFixed(3)} ms`);
        }
    }
}
