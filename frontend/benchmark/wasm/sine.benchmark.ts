
import { compileModuleToWASM } from "@audio-playground/lib-wasm/src/CompileWASM";
import { createModule, func } from "@audio-playground/lib-wasm/src/Op";
import { control, f32, f64, i32 } from "@audio-playground/lib-wasm/src/ops";
import { std } from "@audio-playground/lib-wasm/src/std/STD";
import { voidType } from "@audio-playground/lib-wasm/src/Types";
import { getWasmUnaryFunction } from "@audio-playground/lib-wasm/test/std/_utils";

import { experiment, runExperiments } from "../utils";

const WASM_PAGE_SIZE_BYTES = 64 * 1024;

function createJsJsExperiment(random: () => ArrayLike<number>, SIZE: number, TIMES: number) {


    return experiment({
        title: "Sin Js-Js",
        prepare: () => ({
            args: random(),
            res: new Float64Array(SIZE),
        }),
        consume: (f) => f.res.reduce((a, b) => a + b, 0),
        action: (f) => {
            for (let i = 0; i < SIZE; i++) {
                f.res[i] = Math.sin(f.args[i]!);
            }
        },
        times: TIMES,
    });
}

async function createJsWASMExperiment(random: () => ArrayLike<number>, SIZE: number, TIMES: number) {

    const wasmSine = await getWasmUnaryFunction(std.f64.sin);
    return experiment({
        title: "Sin Js-Wasm",
        prepare: () => ({
            args: random(),
            res: new Float64Array(SIZE),
        }),
        consume: (f) => {
            for (let i = 0; i < SIZE; i++) {
                const a = Math.sin(f.args[i]!);
                const b = f.res[i]!;
                if (Math.abs(a - b) > 1e-7) {
                    throw new Error("Non-referent result");
                }
            }
            return f.res.reduce((a, b) => a + b, 0);
        },
        action: (f) => {
            for (let i = 0; i < SIZE; i++) {
                f.res[i] = wasmSine(f.args[i]!);
            }
        },
        times: TIMES,
    });
}

async function createWASMWASMExperiment(randomAngle: () => number, SIZE: number, TIMES: number) {
    const memPages = Math.ceil(SIZE * 8 * 2 / WASM_PAGE_SIZE_BYTES);

    const module = createModule()
        .memory(["js", "mem"], memPages)
        .func(() => func()
            .exported(true)
            .name("arr")
            .local("index", i32)
            .body((locals) => [
                i32.const(0),
                locals.index.set,
                control.loop(voidType, (repeat) => [

                    locals.index.get,
                    locals.index.get,

                    f64.load(0, 3),
                    std.f64.sin.call,

                    f64.store(SIZE * 8, 3),


                    locals.index.get,
                    i32.const(8),
                    i32.add,
                    locals.index.tee,

                    i32.const(SIZE * 8),
                    i32.lt_u,
                    control.br_if(repeat),
                ]),
            ]),

        )
        .build();

    const compiled = compileModuleToWASM(
        module,
    );

    const memory = new WebAssembly.Memory({ initial: memPages });
    const view = new DataView(memory.buffer);

    const importObject = {
        js: { mem: memory },
    };

    const obj = await WebAssembly.instantiate(
        new Uint8Array(compiled.bytecode),
        importObject,
    );
    const f2call = obj.instance.exports.arr as () => void;

    return experiment({
        title: "Sin Wasm-Wasm no unroll [...input, ...output] memory",
        prepare: () => {
            for (let i = 0; i < SIZE; i++) {
                view.setFloat64(
                    i * 8,
                    randomAngle(),
                    true,
                );
            }
            return {};
        },
        consume: (f) => {
            for (let i = 0; i < SIZE; i++) {
                const angle = view.getFloat64(i * 8, true);
                const a = Math.sin(angle);
                const b = view.getFloat64((i + SIZE) * 8, true);
                if (Math.abs(a - b) > 1e-7) {
                    throw new Error(`Non-referent result i=${i} angle=${angle} expected=${a} got=${b}`);
                }
            }
            return 0;
        },
        action: (f) => {
            f2call();
        },
        times: TIMES,
    });
}

async function createWASM2Experiment(randomAngle: () => number, SIZE: number, TIMES: number) {
    const memPages = Math.ceil(SIZE * 8 * 2 / WASM_PAGE_SIZE_BYTES);


    const module = createModule()
        .memory(["js", "mem"], memPages)
        .func(() => func()
            .exported(true)
            .name("arr")
            .local("index", i32)
            .body((locals) => [
                i32.const(0),
                locals.index.set,
                control.loop(voidType, (repeat) => [

                    locals.index.get,
                    locals.index.get,

                    f64.load(0, 3),
                    std.f64.sin.call,

                    f64.store(8, 3),

                    locals.index.get,
                    i32.const(16),
                    i32.add,
                    locals.index.tee,

                    i32.const(SIZE * 16),
                    i32.lt_u,
                    control.br_if(repeat),
                ]),
            ]),

        )
        .build();

    const compiled = compileModuleToWASM(
        module,
    );

    const memory = new WebAssembly.Memory({ initial: memPages });
    const view = new Float64Array(memory.buffer);

    const importObject = {
        js: { mem: memory },
    };

    const obj = await WebAssembly.instantiate(
        new Uint8Array(compiled.bytecode),
        importObject,
    );
    const f2call = obj.instance.exports.arr as () => void;

    return experiment({
        title: "Sin Wasm-Wasm no unroll [input 0, output 0, ... , input n, output n] memory",
        prepare: () => {
            for (let i = 0; i < SIZE; i++) {
                view[i * 2] = randomAngle();
            }
            return {};
        },
        consume: (f) => {
            for (let i = 0; i < SIZE; i++) {
                const angle = view[i * 2]!;
                const a = Math.sin(angle);
                const b = view[i * 2 + 1]!;
                if (Math.abs(a - b) > 1e-7) {
                    throw new Error(`Non-referent result i=${i} angle=${angle} expected=${a} got=${b}`);
                }
            }
            return 0;
        },
        action: (f) => {
            f2call();
        },
        times: TIMES,
    });
}

async function createWASM3Experiment(randomAngle: () => number, SIZE: number, TIMES: number) {
    const F32_SIZE = 4;
    const memPages = Math.ceil(SIZE * F32_SIZE * 2 / WASM_PAGE_SIZE_BYTES);

    const module = createModule()
        .memory(["js", "mem"], memPages)
        .func(() => func()
            .exported(true)
            .name("arr")
            .local("index", i32)
            .body((locals) => [
                i32.const(0),
                locals.index.set,
                control.loop(voidType, (repeat) => [

                    locals.index.get,
                    locals.index.get,

                    f32.load(0, 2),
                    std.f32.sin.call,

                    f32.store(SIZE * F32_SIZE, 2),


                    locals.index.get,
                    i32.const(F32_SIZE),
                    i32.add,
                    locals.index.tee,

                    i32.const(SIZE * F32_SIZE),
                    i32.lt_u,
                    control.br_if(repeat),
                ]),
            ]),

        )
        .build();

    const compiled = compileModuleToWASM(
        module,
    );

    const memory = new WebAssembly.Memory({ initial: memPages });
    const view = new DataView(memory.buffer);

    const importObject = {
        js: { mem: memory },
    };

    const obj = await WebAssembly.instantiate(
        new Uint8Array(compiled.bytecode),
        importObject,
    );
    const f2call = obj.instance.exports.arr as () => void;

    return experiment({
        title: "Sin Wasm-Wasm no unroll [...input, ...output] memory f32",
        prepare: () => {
            for (let i = 0; i < SIZE; i++) {
                view.setFloat32(
                    i * F32_SIZE,
                    randomAngle(),
                    true,
                );
            }
            return {};
        },
        consume: (f) => {
            for (let i = 0; i < SIZE; i++) {
                const angle = view.getFloat32(i * F32_SIZE, true);
                const a = Math.sin(angle);
                const b = view.getFloat32((i + SIZE) * F32_SIZE, true);
                if (Math.abs(a - b) > 1e-6) {
                    throw new Error(`Non-referent result i=${i} angle=${angle} expected=${a} got=${b}`);
                }
            }
            return 0;
        },
        action: (f) => {
            f2call();
        },
        times: TIMES,
    });
}

(async() => {

    const SIZE = 4096 * 1000;
    const TIMES = 20;

    function randomAngle() {
        return (Math.random() - 0.5) * 2 * 10;
    }
    function random() {
        const arr = new Float64Array(SIZE);
        for (let i = 0; i < SIZE; i++) {
            arr[i] = randomAngle();
        }
        return arr;
    }

    const sinesJsJs = createJsJsExperiment(random, SIZE, TIMES);
    const sinesJsWasm = await createJsWASMExperiment(random, SIZE, TIMES);
    const sinesWasmWasm = await createWASMWASMExperiment(randomAngle, SIZE, TIMES);
    const sinesWasm2 = await createWASM2Experiment(randomAngle, SIZE, TIMES);
    const sinesWasm3 = await createWASM3Experiment(randomAngle, SIZE, TIMES);
    await runExperiments(
        sinesJsJs,
        sinesJsWasm,
        sinesWasmWasm,
        sinesWasm2,
        sinesWasm3,
    );
})();
