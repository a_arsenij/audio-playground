import { monkeyPatch } from "@audio-playground/lib-common/src/std/MonkeyPatching";
import { plot } from "libs/lib-test-utils/src/plot";

import { compileReverbGraphToJs, compileReverbGraphToWASM } from "../../../src/plugins/list/Reverb/compile/ReverbCompile";
import { populateGraphWithStdMacros } from "../../../src/plugins/list/Reverb/macroStd/macroStd";
import { ReverbGraph, WholeReverbGraph } from "../../../src/plugins/list/Reverb/ReverbNodes";
import { DEFAULT_CHECKBOXES } from "../../../test/compileGraph/_utils";
import complexGraphMain from "./complex_graph.json";

monkeyPatch();

const SAMPLE_RATE = 48000;
const BUFFER_LENGTH = 1024;
const OUTPUT_OFFSET = BUFFER_LENGTH * 2;

const times = 10;
async function testWASM() {
    const graph: WholeReverbGraph = {
        main: complexGraphMain as ReverbGraph,
        macros: [],
    };
    populateGraphWithStdMacros(graph);


    const compiled = await compileReverbGraphToWASM(graph, DEFAULT_CHECKBOXES, SAMPLE_RATE, BUFFER_LENGTH);

    const view = new Float64Array(compiled.memory);



    const measure = [];

    for (let i = 0; i < times; i++) {

        for (let j = 0; j < 2 * BUFFER_LENGTH; j++) {
            view[j] = Math.random() * 2 - 1;
        }

        const t0 = performance.now();
        compiled.run(i * BUFFER_LENGTH);
        const t1 = performance.now();

        console.log(t1 - t0);
        measure.push(t1 - t0);

        uuseOutput(view);
    }

    console.log(plot(measure.map((time, i) => ({ x: i, y: time * 1000, type: "wasm" }))));
    console.log("avg", measure.reduce((a, b) => a + b, 0) / measure.length);

}

function uuseOutput(out: ArrayLike<number>) {
    let sum = 0;
    for (let i = 0; i < 2 * BUFFER_LENGTH; i++) {

        sum += out[i + OUTPUT_OFFSET]!;
    }
    console.log(sum);
}

function testJS() {
    const graph: WholeReverbGraph = {
        main: complexGraphMain as ReverbGraph,
        macros: [],
    };
    populateGraphWithStdMacros(graph);


    const compiled = compileReverbGraphToJs(graph, DEFAULT_CHECKBOXES, SAMPLE_RATE, BUFFER_LENGTH);


    const measure = [];

    for (let i = 0; i < times; i++) {
        const inputs = [];

        for (let j = 0; j < 2 * BUFFER_LENGTH; j++) {
            inputs[j] = Math.random() * 2 - 1;
        }


        const t0 = performance.now();
        const res = compiled.run(inputs, i * BUFFER_LENGTH);
        const t1 = performance.now();

        console.log(t1 - t0);
        measure.push(t1 - t0);


        let sum = 0;
        for (let i = 0; i < res.length; i++) {

            sum += res[i]!;
        }
        console.log(sum);
    }
    console.log(plot(measure.map((time, i) => ({ x: i, y: time * 1000, type: "js" }))));
    console.log("avg", measure.reduce((a, b) => a + b, 0) / measure.length);

}

testWASM();
testJS();

