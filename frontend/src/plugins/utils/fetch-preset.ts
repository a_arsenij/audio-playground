import { PluginCommonSettings } from "../../types/Plugin";

function fetchPreset<T extends PluginCommonSettings>(url: string): () => Promise<T> {
    return () => fetch(`presets/${url}`).then((r) => r.json());
}

export function createPreset<T extends PluginCommonSettings>(
    name: string,
    url: { default: string },
): {
    name: string,
    get: () => Promise<T>,
} {
    return {
        name,
        get: fetchPreset(url.default),
    };
}
