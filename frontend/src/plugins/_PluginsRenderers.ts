import { error } from "@audio-playground/lib-common/std/Error";

import { ParametricEQRenderer } from "./list/ParametricEQ/ParametricEQRenderer";
import { ReverbRenderer } from "./list/Reverb/ReverbRenderer";
import { SimpleSamplerRenderer } from "./list/SimpleSampler/SimpleSamplerRenderer";
import { SimpleSynthRenderer } from "./list/SimpleSynth/SimpleSynthRenderer";
import { WaveShaperRenderer } from "./list/WaveShaper/WaveShaperRenderer";

export const PLUGINS_RENDERERS_LIST = [
    SimpleSynthRenderer,
    SimpleSamplerRenderer,
    ReverbRenderer,
    ParametricEQRenderer,
    WaveShaperRenderer,
];

const nameToPluginRendererMap = new Map(PLUGINS_RENDERERS_LIST.map((p) => [p.name, p]));

export function nameToPluginRenderer(name: string) {
    return nameToPluginRendererMap.get(name) || error(`No plugin found for name ${name}`);
}
