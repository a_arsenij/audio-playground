import { error } from "@audio-playground/lib-common/std/Error";

import { ParametricEQUIDecl } from "./list/ParametricEQ/ParametricEQUIDecl";
import { ReverbUIDecl } from "./list/Reverb/ReverbUIDecl";
import { SimpleSamplerUIDecl } from "./list/SimpleSampler/SimpleSamplerUIDecl";
import { SimpleSynthUIDecl } from "./list/SimpleSynth/SimpleSynthUIDecl";
import { WaveShaperUIDecl } from "./list/WaveShaper/WaveShaperUIDecl";

export const PLUGINS_UIS_LIST = [
    SimpleSynthUIDecl,
    SimpleSamplerUIDecl,
    ReverbUIDecl,
    ParametricEQUIDecl,
    WaveShaperUIDecl,
];

const nameToPluginUiMap = new Map(PLUGINS_UIS_LIST.map((p) => [p.name, p]));

export function nameToPluginUi(name: string) {
    return nameToPluginUiMap.get(name) || error(`No plugin UI found for name ${name}`);
}
