import { Svg32PluginsParametricEQ } from "../../../components/Common/GeneratedIcons/32/Plugins/ParametricEQ";
import { PluginUIDecl } from "../../../types/PluginUIDecl";
import { ParametricEQSettings } from "./ParametricEQSettings";
import { ParametricEQUI } from "./ui/ParametricEQUI";

export const ParametricEQUIDecl: PluginUIDecl<ParametricEQSettings> = {

    name: "ParametricEQ",
    UI: ParametricEQUI,
    Icon: Svg32PluginsParametricEQ,

};
