import { acquireArrayFromPool, PooledArray } from "@audio-playground/lib-common/std/ArraysPool";
import { createTupleOf } from "@audio-playground/lib-common/std/Tuple";
import { dbToAmplitude } from "@audio-playground/lib-sound/AmplitudeToDecibells";
import { FilterType } from "@audio-playground/lib-sound/FilterType";
import { getFilterCoefs, getFilterParamSettings, getFilterQualitySettings } from "@audio-playground/lib-sound/IIR";
import { NOTES_COUNT, noteToFrequency } from "@audio-playground/lib-sound/NoteFrequency";

import { MaybeAutomated, RenderBufferArg } from "../../../../types/Plugin";
import { MAX_VOLUME_DB, MIN_VOLUME_DB } from "../../../../utils/MinVolume";
import {
    ParametricEQFilterSettings,
    ParametricEQSettings,
} from "../ParametricEQSettings";
import {
    CHANNELS_NUMBER, ChannelsTupleOf, createChannelsTupleOf,
    createEmptyParametricEQFilterState,
    ParametricEQFilterStage,
    ParametricEQFilterState,
    ParametricEQState,
} from "../ParametricEQState";

function synchronizeStateWithSettings<CHANNELS_NUM extends CHANNELS_NUMBER>(
    channelsNum: CHANNELS_NUM,
    filterType: FilterType,
    filterCutOffNote: number,
    filterQuality: number,
    filterParam: number,
    filterState: ParametricEQFilterState<CHANNELS_NUM>,
    sampleRate: number,
    bufferLength: number,
) {

    if (
        filterCutOffNote !== filterState.cutOffNote ||
        sampleRate !== filterState.sampleRate ||
        filterType !== filterState.type ||
        filterQuality !== filterState.quality ||
        filterParam !== filterState.param ||
        filterState.cutOffNote === -1 ||
        filterState.quality === -1 ||
        filterState.param === -1
    ) {

        filterState.cutOffNote = filterCutOffNote;
        filterState.sampleRate = sampleRate;
        filterState.param = filterParam;
        if (
            filterType !== filterState.type ||
            filterQuality !== filterState.quality
        ) {
            filterState.type = filterType;
            filterState.quality = filterQuality;
            filterState.stages.forEach((stage) => {
                stage.rawBuffer.forEach((v) => v.iDontNeedItAnymore());
                stage.filteredBuffer.forEach((v) => v.iDontNeedItAnymore());
            });
            filterState.stages = getFilterCoefs(
                filterState.type,
                noteToFrequency(filterState.cutOffNote),
                filterState.quality,
                filterState.param,
                sampleRate,
            ).map((coefs) => {
                function b(): ChannelsTupleOf<CHANNELS_NUM, PooledArray> {
                    return createChannelsTupleOf<
                        CHANNELS_NUM,
                        PooledArray
                    >(channelsNum, () => acquireArrayFromPool(bufferLength * 2));
                }
                return {
                    coefs,
                    rawBuffer: b(),
                    filteredBuffer: b(),
                };
            });
        } else {
            const newCoefs = getFilterCoefs(
                filterState.type,
                noteToFrequency(filterState.cutOffNote),
                filterState.quality,
                filterState.param,
                sampleRate,
            );
            newCoefs.forEach((coefs, stageIdx) => {
                filterState.stages[stageIdx]!.coefs = coefs;
            });
        }
    }
}

export function applyFilterStagesChainAt<CHANNELS_NUM extends CHANNELS_NUMBER>(
    channelsNum: CHANNELS_NUM,
    timeSample: number,
    idx: number,
    getChains: (offset: number) =>
        {filters: ParametricEQFilterStage<CHANNELS_NUM>[]; gainDb: number},
    input: ChannelsTupleOf<CHANNELS_NUM, number>,
    bufferLength: number,
) {
    const idxx = (timeSample + idx) % (bufferLength * 2);
    const off = timeSample + idx + bufferLength * 2;
    function index(offset: number) {
        return (
            off + offset
        ) % (bufferLength * 2);
    }
    const idx0 = index(0);
    const idx1 = index(-1);
    const idx2 = index(-2);

    const chains = getChains(idx);
    const gainAmp = dbToAmplitude(chains.gainDb);

    let inputSignal = input.map((v) => v * gainAmp) as ChannelsTupleOf<CHANNELS_NUM, number>;
    let outputSignal = createTupleOf(channelsNum, () => 0) as ChannelsTupleOf<CHANNELS_NUM, number>;

    const filters = chains.filters;
    for (let f = 0; f < filters.length; f++) {
        const filterStage = filters[f]!;

        const previousRaw = filterStage.rawBuffer;

        for (let i = 0; i < channelsNum; i++) {
            previousRaw[i]![idxx] = inputSignal[i]!;
        }

        const rawBuffer = filterStage.rawBuffer;
        const filteredBuffer = filterStage.filteredBuffer;

        const { a1, a2, b0, b1, b2 } = filterStage.coefs;

        for (let i = 0; i < channelsNum; i++) {

            const v = (
                b0 * rawBuffer[i]![idx0]! +
                b1 * rawBuffer[i]![idx1]! +
                b2 * rawBuffer[i]![idx2]! -
                a1 * filteredBuffer[i]![idx1]! -
                a2 * filteredBuffer[i]![idx2]!
            );
            filteredBuffer[i]![idx0] = v;
            outputSignal[i] = v;

        }

        const tmp = inputSignal;
        inputSignal = outputSignal;
        outputSignal = tmp;
    }
    return inputSignal;
}

export function applyFilterStagesChain<CHANNELS_NUM extends CHANNELS_NUMBER>(
    channelsNum: CHANNELS_NUM,
    timeSample: number,
    input: ChannelsTupleOf<CHANNELS_NUM, Float64Array>,
    output: ChannelsTupleOf<CHANNELS_NUM, Float64Array>,
    getChains: (offset: number) =>
        {filters: ParametricEQFilterStage<CHANNELS_NUM>[]; gainDb: number},
    bufferLength: number,
) {
    for (let idx = 0; idx < bufferLength; idx++) {
        applyFilterStagesChainAt(
            channelsNum,
            idx,
            timeSample,
            getChains,
            [...Array(channelsNum)]
                .map((_, i) => input[i]![idx]!) as ChannelsTupleOf<CHANNELS_NUM, number>,
            bufferLength,
        ).forEach((v, i) => {
            output[i]![idx] = v;
        });
    }
}

export function syncFiltersAmount<CHANNELS_NUM extends CHANNELS_NUMBER>(
    pluginSettings: ParametricEQSettings,
    pluginState: ParametricEQState<CHANNELS_NUM>,
) {
    if (pluginSettings.filters.length !== pluginState.filters.length) {
        pluginState.filters.forEach((f) => {
            f.stages.forEach((s) => {
                s.filteredBuffer.forEach((v) => v.iDontNeedItAnymore());
                s.rawBuffer.forEach((v) => v.iDontNeedItAnymore());
            });
        });
        pluginState.filters = pluginSettings.filters.map(() =>
            createEmptyParametricEQFilterState(),
        );
    }
}

export function createGetChains<CHANNELS_NUM extends CHANNELS_NUMBER>(
    channelsNum: CHANNELS_NUM,
    filtersSettings: ParametricEQFilterSettings[],
    gainDb: number,
    filtersState: ParametricEQFilterState<CHANNELS_NUM>[],
    getMaybeAutomated: (
        knobId: string,
        minValue: number,
        maxValue: number,
        defaultValue: number,
    ) => MaybeAutomated,
    sampleRate: number,
    getCurrentTimeBars: (offsetSamples: number) => number,
    bufferLength: number,
) {
    const getGainDb = getMaybeAutomated(
        "gainDb",
        MIN_VOLUME_DB,
        MAX_VOLUME_DB,
        gainDb,
    );
    const automations = filtersSettings.map((f) => {
        const qualitySettings = getFilterQualitySettings(f.type);
        const paramSettings = getFilterParamSettings(f.type);
        return [
            getMaybeAutomated(
                `cutOffNote:${f.id}`,
                0,
                NOTES_COUNT,
                f.cutOffNote,
            ),
            getMaybeAutomated(
                `quality:${f.id}`,
                qualitySettings.min,
                qualitySettings.max,
                f.quality,
            ),
            paramSettings
                ? getMaybeAutomated(
                    `param:${f.id}`,
                    paramSettings.min,
                    paramSettings.max,
                    f.param,
                )
                : () => f.param,
        ] as const;
    });
    return (offset: number) => {
        return {
            filters: filtersSettings.map((filter, idx) => {
                const t = getCurrentTimeBars(offset);
                const cutOffNote = automations[idx]![0](t);
                const quality = automations[idx]![1](t);
                const param = automations[idx]![2](t);
                synchronizeStateWithSettings(
                    channelsNum,
                    filter.type,
                    cutOffNote,
                    quality,
                    param,
                    filtersState[idx]!,
                    sampleRate,
                    bufferLength,
                );
                return filtersState[idx]!.stages;
            }).flat(),
            gainDb: getGainDb(getCurrentTimeBars(offset)),
        };
    };
}

export function parametricEQRender({
    timeSample,
    sampleRate,
    input,
    output,
    pluginSettings,
    pluginState,
    getMaybeAutomated,
    getCurrentTimeBars,
    bufferLength,
}: RenderBufferArg<ParametricEQSettings, ParametricEQState<2>>) {
    syncFiltersAmount(pluginSettings, pluginState);
    const getChains = createGetChains(
        2,
        pluginSettings.filters,
        pluginSettings.gainDb,
        pluginState.filters,
        getMaybeAutomated,
        sampleRate,
        getCurrentTimeBars,
        bufferLength,
    );
    applyFilterStagesChain(
        2,
        timeSample,
        input as [PooledArray, PooledArray],
        output as [PooledArray, PooledArray],
        getChains,
        bufferLength,
    );
}
