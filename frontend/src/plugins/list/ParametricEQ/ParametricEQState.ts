import { PooledArray } from "@audio-playground/lib-common/std/ArraysPool";
import { FilterType } from "@audio-playground/lib-sound/FilterType";
import { BiquadFilter } from "@audio-playground/lib-sound/IIR";

export type CHANNELS_NUMBER = 1 | 2;

export type ChannelsTupleOf<
    SIZE extends CHANNELS_NUMBER,
    ELEM
> =
    SIZE extends 1 ? [ELEM] : [ELEM, ELEM]
;

export function createChannelsTupleOf<
    N extends CHANNELS_NUMBER,
    ITEM,
>(
    size: N,
    item: () => ITEM,
): ChannelsTupleOf<N, ITEM> {
    if (size === 1) {
        return [item()] as ChannelsTupleOf<N, ITEM>;
    } else {
        return [item(), item()] as ChannelsTupleOf<N, ITEM>;
    }
}

export type ParametricEQFilterStage<CHANNELS_NUM extends CHANNELS_NUMBER> = {
    rawBuffer: ChannelsTupleOf<CHANNELS_NUM, PooledArray>,
    filteredBuffer: ChannelsTupleOf<CHANNELS_NUM, PooledArray>,
    coefs: BiquadFilter,
}

export type ParametricEQFilterState<CHANNELS_NUM extends CHANNELS_NUMBER> = {

    stages: ParametricEQFilterStage<CHANNELS_NUM>[],

    sampleRate: number,
    type: FilterType,
    quality: number,
    param: number,
    cutOffNote: number,

}

export type ParametricEQState<CHANNELS_NUM extends CHANNELS_NUMBER> = {
    filters: ParametricEQFilterState<CHANNELS_NUM>[],
}

export function createEmptyParametricEQFilterState<
    CHANNELS_NUM extends CHANNELS_NUMBER
>(): ParametricEQFilterState<CHANNELS_NUM> {
    return {
        stages: [],
        sampleRate: -1,
        quality: -1,
        param: -1,
        type: "lowpass",
        cutOffNote: -1,
    };
}

export function createParametricEQState<
    CHANNELS_NUM extends CHANNELS_NUMBER
>(): ParametricEQState<CHANNELS_NUM> {
    return {
        filters: [],
    };
}
