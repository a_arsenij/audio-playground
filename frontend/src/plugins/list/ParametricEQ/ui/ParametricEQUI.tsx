import { SetState, useByKey, useDeleteById, useSetById } from "@audio-playground/lib-frontend/std/SetState";
import { amplitudeToDb, dbToAmplitude } from "@audio-playground/lib-sound/AmplitudeToDecibells";
import { FilterType } from "@audio-playground/lib-sound/FilterType";
import {
    getFilterCoefs,
    getFilterParamSettings,
    getFilterQualitySettings,
    getFrequencyResponse,
} from "@audio-playground/lib-sound/IIR";
import { NOTES_COUNT, noteToFrequency } from "@audio-playground/lib-sound/NoteFrequency";
import React, { useMemo } from "react";

import { AutomatedKnobCheckbox } from "../../../../components/Common/AutomatedCheckbox/AutomatedCheckbox";
import { Button } from "../../../../components/Common/Button/Button";
import { FilterButton } from "../../../../components/Common/FilterButton/FilterButton";
import { Svg18CommonDelete } from "../../../../components/Common/GeneratedIcons/18/Common/Delete";
import { defaultRenderValue, Knob } from "../../../../components/Common/Knob/Knob";
import { PiecewiseLinearGraph } from "../../../../components/Common/PiecewiseLinearGraph/PiecewiseLinearGraph";
import { PluginUIProps } from "../../../../types/Plugin";
import { MAX_VOLUME_DB, MIN_VOLUME_DB } from "../../../../utils/MinVolume";
import {
    createDefaultParametricEQFilter, ParametricEQFilterSettings,
    ParametricEQSettings,
} from "../ParametricEQSettings";
import styles from "./ParametricEQUI.module.css";

export const ParametricEQUI = function({
    pluginSettings,
    setPluginSettings,
    isAutomated,
    setIsAutomated,
    sampleRate,
}: PluginUIProps<ParametricEQSettings>) {
    const [filters, setFilters] = useByKey(pluginSettings, setPluginSettings, "filters");
    const [gainDb, setGainDb] = useByKey(pluginSettings, setPluginSettings, "gainDb");
    return <FiltersUI
        gainDb={gainDb}
        setGainDb={setGainDb}
        filters={filters}
        setFilters={setFilters}
        isAutomated={isAutomated}
        setIsAutomated={setIsAutomated}
        sampleRate={sampleRate}
    />;
};

type ParametricEQUIImplProps = {
    gainDb: number,
    setGainDb: SetState<number>,
    filters: ParametricEQFilterSettings[],
    setFilters: SetState<ParametricEQFilterSettings[]>,
    isAutomated: (name: string) => boolean,
    setIsAutomated: (name: string, isAutomated: boolean) => void,
    sampleRate: number,
}

export const FiltersUI = function({
    gainDb,
    setGainDb,
    filters,
    setFilters,
    isAutomated,
    setIsAutomated,
    sampleRate,
}: ParametricEQUIImplProps) {

    const setFilterById = useSetById(setFilters);
    const deleteFilterByIdImpl = useDeleteById(setFilters);

    const deleteFilterById = (
        id: string,
    ) => {
        deleteFilterByIdImpl(id);
        setIsAutomated(`cutOffNote:${id}`, false);
        setIsAutomated(`quality:${id}`, false);
        setIsAutomated(`param:${id}`, false);
    };

    const setTypeById = (
        id: string,
        type: FilterType,
    ) => setFilterById(id, (prev) => ({
        ...prev,
        type,
    }));

    const setCutOffById = (
        id: string,
        cutOffNote: number,
    ) => setFilterById(id, (prev) => ({
        ...prev,
        cutOffNote,
    }));

    const setQualityById = (
        id: string,
        quality: number,
    ) => setFilterById(id, (prev) => ({
        ...prev,
        quality,
    }));

    const setParamById = (
        id: string,
        param: number,
    ) => setFilterById(id, (prev) => ({
        ...prev,
        param,
    }));

    const addFilter = () => {
        setFilters((prev) => [
            ...prev,
            createDefaultParametricEQFilter(),
        ]);
    };

    const afc = useMemo(() => {

        const coefs = filters
            .map((f) => getFilterCoefs(
                f.type,
                noteToFrequency(f.cutOffNote),
                f.quality,
                f.param,
                sampleRate,
            ))
            .flat();

        const amp = dbToAmplitude(gainDb);

        const values = [...Array(NOTES_COUNT)].map((_, idx) => {
            let res = 1;
            coefs.forEach((biquad) => {
                res *= getFrequencyResponse(
                    biquad,
                    noteToFrequency(idx),
                    sampleRate,
                );
            });
            return amplitudeToDb(res * amp);
        });

        return <PiecewiseLinearGraph
            points={values.map((amp, note) => ({
                x: note,
                y: amp,
            }))}
            fromX={0}
            toX={NOTES_COUNT}
            fromY={-60}
            toY={10}
        />;
    }, [sampleRate, filters, gainDb]);

    return <div className={styles.parametricEQUI}>
        <div className={styles.filter}>
            <Button
                onClick={addFilter}
                title={"Add one more filter"}
            />

            <Knob
                label={"Gain"}
                value={gainDb}
                setValue={setGainDb}
                minValue={MIN_VOLUME_DB}
                maxValue={MAX_VOLUME_DB}
                neutralValue={MIN_VOLUME_DB}
                defaultValue={0}
                showNumberValue={true}
                renderValue={
                    (v) => <>{defaultRenderValue(v)}db</>
                }
                func={"linear"}
                extraContextMenuChildren={<>
                    <AutomatedKnobCheckbox
                        isAutomated={isAutomated}
                        setIsAutomated={setIsAutomated}
                        knobName={"gainDb"}
                    />
                </>}
            />
        </div>

        {filters.map((filter, idx) => {
            const qualitySettings = getFilterQualitySettings(filter.type);
            const param = getFilterParamSettings(filter.type);
            return <div
                key={idx}
                className={styles.filter}
            >
                <Button
                    style={"ghost"}
                    onClick={() => deleteFilterById(filter.id)}
                    iconLeft={Svg18CommonDelete}
                    title={"Delete filter"}
                />
                <FilterButton
                    type={filters[idx]!.type}
                    setType={(type) => setTypeById(filter.id, type)}
                />
                <Knob
                    label={"Note"}
                    func={"linear"}
                    maxValue={NOTES_COUNT}
                    showNumberValue={true}
                    value={filters[idx]!.cutOffNote}
                    setValue={(v) => setCutOffById(filter.id, v)}
                    extraContextMenuChildren={<>
                        <AutomatedKnobCheckbox
                            isAutomated={isAutomated}
                            setIsAutomated={setIsAutomated}
                            knobName={`cutOffNote:${filter.id}`}
                        />
                    </>}
                />
                <Knob
                    label={"Q"}
                    func={"linear"}
                    minValue={qualitySettings.min}
                    maxValue={qualitySettings.max}
                    step={qualitySettings.mode === "discrete" ? 1 : 0}
                    showNumberValue={true}
                    value={filters[idx]!.quality}
                    setValue={(v) => setQualityById(filter.id, v)}
                    extraContextMenuChildren={<>
                        <AutomatedKnobCheckbox
                            isAutomated={isAutomated}
                            setIsAutomated={setIsAutomated}
                            knobName={`quality:${filter.id}`}
                        />
                    </>}
                />
                {
                    param &&
                    <Knob
                        label={param.name}
                        func={"linear"}
                        minValue={param.min}
                        maxValue={param.max}
                        step={param.mode === "discrete" ? 1 : 0}
                        showNumberValue={true}
                        value={filters[idx]!.param}
                        setValue={(v) => setParamById(filter.id, v)}
                        extraContextMenuChildren={<>
                            <AutomatedKnobCheckbox
                                isAutomated={isAutomated}
                                setIsAutomated={setIsAutomated}
                                knobName={`param:${filter.id}`}
                            />
                        </>}
                    />
                }
            </div>;
        })}
        <div className={styles.plot}>
            {afc}
        </div>
    </div>;
};
