import { generateId } from "@audio-playground/lib-common/std/Id";
import { FilterType } from "@audio-playground/lib-sound/FilterType";

import { createDefaultEffectPluginPorts, PluginCommonSettings } from "../../../types/Plugin";

export type ParametricEQFilterSettings = {
    id: string,
    type: FilterType,
    cutOffNote: number,
    quality: number,
    param: number,
    isEnabled: boolean,
}

export function createDefaultParametricEQFilter(): ParametricEQFilterSettings {
    return {
        id: generateId(),
        type: "lowpass",
        quality: 1,
        param: 1,
        cutOffNote: 40,
        isEnabled: true,
    };
}

export type ParametricEQSettings =
    & PluginCommonSettings
    & {
        gainDb: number,
        filters: ParametricEQFilterSettings[],
    }
;

export function createParametricEQSettings(): ParametricEQSettings {
    return {
        gainDb: 0,
        filters: [
            {
                id: generateId(),
                type: "lowpass",
                quality: 1,
                param: 1,
                cutOffNote: 40,
                isEnabled: false,
            },
        ],
        ...createDefaultEffectPluginPorts(),
    };
}
