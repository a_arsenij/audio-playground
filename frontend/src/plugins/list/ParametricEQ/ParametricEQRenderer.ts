import { PluginRenderer } from "../../../types/PluginRenderer";
import { destroyParametricEQ } from "./ParametricEQDestroy";
import { createParametricEQSettings, ParametricEQSettings } from "./ParametricEQSettings";
import { createParametricEQState } from "./ParametricEQState";
import { parametricEQRender } from "./render/ParametricEQRender";

export const ParametricEQRenderer: PluginRenderer<ParametricEQSettings, any> = {

    name: "ParametricEQ",
    renderBuffer: parametricEQRender,
    createSettings: createParametricEQSettings,
    createState: createParametricEQState,
    destroy: destroyParametricEQ,

};
