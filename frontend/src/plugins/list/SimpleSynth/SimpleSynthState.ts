import { FilterType } from "@audio-playground/lib-sound/FilterType";

export type SimpleSynthState = {

    filterSampleRate: number,
    filterType: FilterType,
    filterCutOffNote: number,
    filterQuality: number,
    filterParam: number,

    // releasedNotes: Map<string, EndedRenderableNote>,
}

export function createSimpleSynthState(): SimpleSynthState {
    return {
        filterSampleRate: 0,
        filterType: "lowpass",
        filterCutOffNote: -1,
        filterQuality: -1,
        filterParam: -1,
        // releasedNotes: new Map<string, EndedRenderableNote>(),
    };
}
