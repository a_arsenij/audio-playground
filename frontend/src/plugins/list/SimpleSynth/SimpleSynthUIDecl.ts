import { Svg32PluginsSimpleSynth } from "../../../components/Common/GeneratedIcons/32/Plugins/SimpleSynth";
import { PluginUIDecl } from "../../../types/PluginUIDecl";
import { SimpleSynthSettings } from "./SimpleSynthSettings";
import { SimpleSynthUI } from "./ui/SimpleSynthUI";

export const SimpleSynthUIDecl: PluginUIDecl<SimpleSynthSettings> = {

    name: "SimpleSynth",
    UI: SimpleSynthUI,
    Icon: Svg32PluginsSimpleSynth,

};
