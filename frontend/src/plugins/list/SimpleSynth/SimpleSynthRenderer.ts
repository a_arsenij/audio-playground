import { PluginRenderer } from "../../../types/PluginRenderer";
import { simpleSynthRender } from "./render/SimpleSynthRender";
import { destroySimpleSynth } from "./SimpleSynthDestroy";
import { createSimpleSynthSettings, SimpleSynthSettings } from "./SimpleSynthSettings";
import { createSimpleSynthState } from "./SimpleSynthState";

export const SimpleSynthRenderer: PluginRenderer<SimpleSynthSettings, any> = {

    name: "SimpleSynth",
    renderBuffer: simpleSynthRender,
    createSettings: createSimpleSynthSettings,
    createState: createSimpleSynthState,
    destroy: destroySimpleSynth,

};
