import { RenderBufferArg } from "../../../../types/Plugin";
import { addToBuffers } from "../../../../utils/AddToBuffer";
import { SimpleSynthSettings } from "../SimpleSynthSettings";
import { SimpleSynthState } from "../SimpleSynthState";

/* function waveformToGenerator(
    waveform: WaveformShape,
): ToneGenerator {
    switch (waveform) {
        case "sine": return generateSineTone;
        case "triangle": return generateTriangleTone;
        case "saw": return generateSawTone;
        case "square": return generateSquareTone;
    }
}

const RELEASE_TIME_MULTIPLIER = 3; */

/* function getADSRAt(
    adsr: ADSR,
    timeAfterPressSec: number,
    timeAfterReleaseSec: number | undefined,
): number {
    const releaseTime = adsr[3] * RELEASE_TIME_MULTIPLIER;
    if (timeAfterReleaseSec !== undefined) {
        if (timeAfterReleaseSec >= releaseTime) {
            return 0;
        }
        return (releaseTime - timeAfterReleaseSec) / releaseTime * adsr[2];
    }

    if (timeAfterPressSec < adsr[0]) {
        return timeAfterPressSec / adsr[0];
    }
    if (timeAfterPressSec < adsr[0] + adsr[1]) {
        return 1 - ((timeAfterPressSec - adsr[0]) / adsr[1]) * (1 - adsr[2]);
    }
    return adsr[2];
} */

export function simpleSynthRender({
    input,
    output,
    /* sampleRate,
    pluginSettings,
    getMaybeAutomated, */
}: RenderBufferArg<SimpleSynthSettings, SimpleSynthState>) {

    /* const panAutomation = getMaybeAutomated(
        "pan",
        -1,
        1,
        pluginSettings.pan,
    ); */

    // const waveformShape = pluginSettings.waveform;
    // const adsrEnvelope = pluginSettings.adsrEnvelope;
    // const isAdsrEnabled = pluginSettings.isAdsrEnabled;
    // const releaseTimeSec = isAdsrEnabled ? adsrEnvelope[3] * RELEASE_TIME_MULTIPLIER : 0;
    /* const releaseTimeSamples = releaseTimeSec * sampleRate;
    const generator: ToneGenerator = waveformToGenerator(
        waveformShape,
    ); */

    /* const notesToPlay = handleReleasedNotes(
        timeSample,
        notes,
        pluginState.releasedNotes,
        () => releaseTimeSamples,
    );

    notesToPlay.forEach((note) => {
        const noteFrequency = noteToFrequency(
            note.key + pluginSettings.tuneFine + pluginSettings.tuneBig,
        );
        const minIdx = Math.max(0, note.startedAtSample - timeSample);
        const maxIdx = Math.min(
            bufferLength - 1,
            (
                note.endedAtSample
                    ? note.endedAtSample + releaseTimeSamples
                    : Number.MAX_SAFE_INTEGER
            ) - timeSample,
        );

        for (let idx = minIdx; idx <= maxIdx; idx++) {
            const noteTimeAfterPressSample = (timeSample + idx) - note.startedAtSample;
            const noteTimeAfterPressSec = noteTimeAfterPressSample / sampleRate;
            const noteTimeAfterReleaseSample = note.endedAtSample
                ? (timeSample + idx) - note.endedAtSample
                : undefined;
            const noteTimeAfterReleaseSec =
                noteTimeAfterReleaseSample === undefined ||
                noteTimeAfterReleaseSample < 0
                    ? undefined
                    : noteTimeAfterReleaseSample / sampleRate;
            const tone = generator(
                noteFrequency,
                noteTimeAfterPressSec,
            );
            const adsrValue = (isAdsrEnabled
                ? getADSRAt(
                    adsrEnvelope,
                    noteTimeAfterPressSec,
                    noteTimeAfterReleaseSec,
                )
                : 1);
            const value = tone * adsrValue;

            const timeBars = getCurrentTimeBars(idx);
            const panValue = panAutomation(timeBars);
            const leftVolume = (1 - panValue);
            const rightVolume = (1 + panValue);

            output[0]![idx] += value * leftVolume;
            output[1]![idx] += value * rightVolume;
        }
    }); */

    // TODO: Filter

    addToBuffers(input, output);
}
