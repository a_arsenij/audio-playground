
import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { useByKey } from "@audio-playground/lib-frontend/std/SetState";
import { NOTES_COUNT } from "@audio-playground/lib-sound/NoteFrequency";
import React from "react";

import { AutomatedKnobCheckbox } from "../../../../components/Common/AutomatedCheckbox/AutomatedCheckbox";
import { Checkbox } from "../../../../components/Common/Checkbox/Checkbox";
import { FilterSwitch } from "../../../../components/Common/FilterSwitch/FilterSwitch";
import { Svg18WaveshapeSaw } from "../../../../components/Common/GeneratedIcons/18/Waveshape/Saw";
import { Svg18WaveshapeSine } from "../../../../components/Common/GeneratedIcons/18/Waveshape/Sine";
import { Svg18WaveshapeSquare } from "../../../../components/Common/GeneratedIcons/18/Waveshape/Square";
import { Svg18WaveshapeTriangle } from "../../../../components/Common/GeneratedIcons/18/Waveshape/Triangle";
import { Knob } from "../../../../components/Common/Knob/Knob";
import { LabeledContainer } from "../../../../components/Common/LabeledContainer/LabeledContainer";
import {
    PiecewiseLinearGraph,
} from "../../../../components/Common/PiecewiseLinearGraph/PiecewiseLinearGraph";
import { DeprecatedSwitch } from "../../../../components/Common/Switch/Switch";
import { PluginUIProps } from "../../../../types/Plugin";
import { ADSR, SimpleSynthSettings, WAVEFORM_SHAPES, WaveformShape } from "../SimpleSynthSettings";
import styles from "./SimpleSynthUI.module.css";

const ADSR_LABELS = [
    "At",
    "Dt",
    "Sv",
    "Rt",
];

const ICONS = {
    sine: Svg18WaveshapeSine,
    triangle: Svg18WaveshapeTriangle,
    saw: Svg18WaveshapeSaw,
    square: Svg18WaveshapeSquare,
};

export const SimpleSynthUI = function({
    pluginSettings,
    setPluginSettings,
    isAutomated,
    setIsAutomated,
}: PluginUIProps<SimpleSynthSettings>) {
    const waveformShape = pluginSettings.waveform;
    const setWaveformShape = (shape: WaveformShape) => {
        setPluginSettings({ ...pluginSettings, waveform: shape });
    };

    const isAdsrEnabled = pluginSettings.isAdsrEnabled;
    const setAdsrEnabled = (value: boolean) => {
        setPluginSettings({ ...pluginSettings, isAdsrEnabled: value });
    };

    const adsrEnvelope = pluginSettings.adsrEnvelope;
    const setAdsr = (idx: number, value: number) => {
        const oldEnv: ADSR = [...pluginSettings.adsrEnvelope];
        oldEnv[idx] = value;
        setPluginSettings({
            ...pluginSettings,
            adsrEnvelope: oldEnv,
        });
    };

    const [filterType, setFilterType] = useByKey(
        pluginSettings,
        setPluginSettings,
        "filterType",
    );

    const [filterCutOffNote, setFilterCutOffNote] =
        useByKey(pluginSettings, setPluginSettings, "filterCutOffNote");
    const [filterQuality, setFilterQuality] =
        useByKey(pluginSettings, setPluginSettings, "filterQuality");

    const isFilterEnabled = pluginSettings.isFilterEnabled;
    const setFilterEnabled = (value: boolean) => {
        setPluginSettings({ ...pluginSettings, isFilterEnabled: value });
    };

    const [tuneFine, setTuneFine] = useByKey(pluginSettings, setPluginSettings, "tuneFine");

    const TUNE_BIG_RANGE = 48;
    const [tuneBig, setTuneBig] = useByKey(pluginSettings, setPluginSettings, "tuneBig");

    const [pan, setPan] = useByKey(pluginSettings, setPluginSettings, "pan");

    return <div className={styles.simpleSynthUI}>
        <LabeledContainer label={"Tune"}>
            <div className={styles.knobs}>
                <Knob
                    key={"Fine"}
                    label={"Fine"}
                    value={tuneFine}
                    setValue={setTuneFine}
                    minValue={-0.5}
                    maxValue={0.5}
                    defaultValue={0}
                    neutralValue={0}
                    func={"linear"}
                    showNumberValue={true}
                />
                <Knob
                    key={"Big"}
                    label={"Big"}
                    value={tuneBig}
                    setValue={setTuneBig}
                    minValue={-TUNE_BIG_RANGE}
                    maxValue={TUNE_BIG_RANGE}
                    step={0.5}
                    defaultValue={0}
                    neutralValue={0}
                    func={"linear"}
                    showNumberValue={true}
                />
                <Knob
                    key={"Pan"}
                    label={"Pan"}
                    value={pan}
                    setValue={setPan}
                    defaultValue={0}
                    neutralValue={0}
                    minValue={-1}
                    maxValue={1}
                    func={"linear"}
                    showNumberValue={true}
                    extraContextMenuChildren={<>
                        <AutomatedKnobCheckbox
                            isAutomated={isAutomated}
                            setIsAutomated={setIsAutomated}
                            knobName={"pan"}
                        />
                    </>}
                />
            </div>
        </LabeledContainer>
        <LabeledContainer label={"Waveform"}>
            <DeprecatedSwitch
                options={WAVEFORM_SHAPES}
                value={waveformShape}
                setValue={setWaveformShape}
                renderOption={(shape) => {
                    const IconComponent = ICONS[shape];
                    return <IconComponent/>;
                }}
            />
        </LabeledContainer>
        <LabeledContainer label={"Envelope"}>
            <div className={styles.envelope}>
                <div className={styles.knobs}>
                    <Checkbox value={isAdsrEnabled} setValue={setAdsrEnabled}/>
                    <div className={styles.space}/>
                    {[...Array(4)].map((_, idx) =>
                        <Knob
                            key={idx}
                            label={ADSR_LABELS[idx]}
                            value={adsrEnvelope[idx] || 0}
                            setValue={(v) => setAdsr(idx, v)}
                            func={"linear"}
                            extraContextMenuChildren={<>
                                <AutomatedKnobCheckbox
                                    isAutomated={isAutomated}
                                    setIsAutomated={setIsAutomated}
                                    knobName={`ADSR ${ADSR_LABELS[idx]}`}
                                />
                            </>}
                        />,
                    )}
                </div>
                <div className={clsx(
                    styles.envelopeGraph,
                    isAdsrEnabled && styles.graphEnabled,
                    !isAdsrEnabled && styles.graphDisabled,
                )}>
                    <PiecewiseLinearGraph
                        points={[
                            { x: 0, y: 0 },
                            { x: adsrEnvelope[0], y: 1 },
                            { x: adsrEnvelope[0] + adsrEnvelope[1], y: adsrEnvelope[2] },
                            { x: adsrEnvelope[0] + adsrEnvelope[1] + 0.5, y: adsrEnvelope[2] },
                            { x: adsrEnvelope[0] + adsrEnvelope[1] + 0.5 + adsrEnvelope[3], y: 0 },
                        ]}
                        fromX={0}
                        toX={adsrEnvelope[0] + adsrEnvelope[1] + 0.5 + adsrEnvelope[3]}
                        fromY={0}
                        toY={1}
                    />
                </div>
            </div>
        </LabeledContainer>
        <LabeledContainer label={"Filter"}>
            <div className={styles.knobs}>
                <Checkbox value={isFilterEnabled} setValue={setFilterEnabled}/>
                <div className={styles.space}/>
                <FilterSwitch type={filterType} setType={setFilterType}/>
            </div>
            <div className={styles.knobs}>
                <Knob
                    label={"Freq"}
                    value={filterCutOffNote}
                    setValue={setFilterCutOffNote}
                    minValue={0}
                    maxValue={NOTES_COUNT}
                    func={"linear"}
                    extraContextMenuChildren={<>
                        <AutomatedKnobCheckbox
                            isAutomated={isAutomated}
                            setIsAutomated={setIsAutomated}
                            knobName={"filterCutOffNote"}
                        />
                    </>}
                />
                <Knob
                    label={"Order"}
                    func={"linear"}
                    minValue={1}
                    maxValue={6}
                    step={1}
                    defaultValue={1}
                    value={filterQuality}
                    setValue={setFilterQuality}
                />
            </div>
        </LabeledContainer>

    </div>;
};
