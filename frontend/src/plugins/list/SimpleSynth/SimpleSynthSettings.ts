import { FilterType } from "@audio-playground/lib-sound/FilterType";

import { createDefaultInstrumentPluginPorts, PluginCommonSettings } from "../../../types/Plugin";

export type ADSR = [number, number, number, number];

export const WAVEFORM_SHAPES = ["sine", "triangle", "saw", "square"] as const;

export type WaveformShape = (typeof WAVEFORM_SHAPES)[number];

const SIMPLE_SYNTH_DEFAULT_WAVEFORM: WaveformShape = "sine";
const SIMPLE_SYNTH_DEFAULT_ADSR_ENVELOPE: ADSR = [0.1, 0.1, 0.4, 0.5];
const SIMPLE_SYNTH_DEFAULT_ADSR_ENABLED = false;

export type SimpleSynthSettings =
    & PluginCommonSettings
    & {
        waveform: WaveformShape,
        adsrEnvelope: ADSR,
        isAdsrEnabled: boolean,

        isFilterEnabled: boolean,
        filterType: FilterType,
        filterCutOffNote: number,
        filterQuality: number,
        filterParam: number,
        filterMult: number,

        tuneFine: number,
        tuneBig: number,
        pan: number,
    }
;

export function createSimpleSynthSettings(): SimpleSynthSettings {
    return {
        waveform: SIMPLE_SYNTH_DEFAULT_WAVEFORM,
        adsrEnvelope: SIMPLE_SYNTH_DEFAULT_ADSR_ENVELOPE,
        isAdsrEnabled: SIMPLE_SYNTH_DEFAULT_ADSR_ENABLED,
        isFilterEnabled: false,

        filterType: "lowpass",
        filterCutOffNote: 40,
        filterQuality: 2,
        filterParam: 1,
        filterMult: 1,

        tuneFine: 0,
        tuneBig: 0,
        pan: 0,

        ...createDefaultInstrumentPluginPorts(),
    };
}
