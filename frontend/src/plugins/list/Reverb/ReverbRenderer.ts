import { PluginRenderer } from "../../../types/PluginRenderer";
import { reverbRender } from "./render/ReverbRender";
import { destroyReverb } from "./ReverbDestroy";
import { createSimpleReverbSettings, ReverbSettings } from "./ReverbSettings";
import { createSimpleReverbState, ReverbState } from "./ReverbState";

export const ReverbRenderer: PluginRenderer<ReverbSettings, ReverbState> = {

    name: "Reverb",
    renderBuffer: reverbRender,
    createSettings: createSimpleReverbSettings,
    createState: createSimpleReverbState,
    destroy: destroyReverb,

};
