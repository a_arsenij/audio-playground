import { ReverbMacro, WholeReverbGraph } from "../ReverbNodes";
import linearInterpolate from "./conditions/linearInterpolate.json";
import trigger from "./conditions/trigger.json";
import biquadFilter1MacroGraph from "./filters/biquadFilter1.json";
import biquadFilterNMacroGraph from "./filters/biquadFilterN.json";
import butterworthHighPassCoefficients from "./filters/butterworthHighPassCoefficients.json";
import butterworthLowPassCoefficients from "./filters/butterworthLowPassCoefficients.json";
import dotProduct4 from "./math/dotProduct4.json";
import dotProduct8 from "./math/dotProduct8.json";
import householderMatrix88FromVec8 from "./math/householderMatrix88FromVec8.json";
import mulMat88ToVec8 from "./math/mulMat88ToVec8.json";
import mulVec4ToNum from "./math/mulVec4ToNum.json";
import mulVec8ToNum from "./math/mulVec8ToNum.json";
import mulVec8ToVec8 from "./math/mulVec8ToVec8.json";
import normalizeVec8 from "./math/normalizeVec8.json";
import signum from "./math/signum.json";
import frequencyToNoteMacroGraph from "./musical/frequencyToNote.json";
import noteToFrequencyMacroGraph from "./musical/noteToFrequency.json";
import oscillator from "./musical/oscillator.json";
import toCyclicNormalizedFrequency from "./musical/toCyclicNormalizedFrequency.json";
import delayMs from "./utils/delayMs.json";
import delayMultichannelMs from "./utils/delayMultichannelMs.json";
import millisecondsToSamples from "./utils/millisecondsToSamples.json";
import timeSamples from "./utils/timeSamples.json";
export const MACROS_STD: Map<string, ReverbMacro> = new Map(([
    {
        id: "std::conditions::linearInterpolate",
        title: "Linear interpolate",
        graph: linearInterpolate,
    },
    {
        id: "std::conditions::trigger",
        title: "Trigger",
        graph: trigger,
    },
    {
        id: "std::filters::BiquadFilter1",
        title: "BiquadFilter 1",
        graph: biquadFilter1MacroGraph,
    },
    {
        id: "std::filters::BiquadFilterN",
        title: "BiquadFilter N",
        graph: biquadFilterNMacroGraph,
    },
    {
        id: "std::musical::NoteToFrequency",
        title: "Note to frequency",
        graph: noteToFrequencyMacroGraph,
    },
    {
        id: "std::musical::FrequencyToNote",
        title: "Frequency to note",
        graph: frequencyToNoteMacroGraph,
    },
    {
        id: "std::musical::CyclicNormalizedFrequency",
        title: "To cyclic normalized frequency",
        graph: toCyclicNormalizedFrequency,
    },
    {
        id: "std::musical::ButterworthLowPassCoefficients",
        title: "Butterworth low pass filter coefficients",
        graph: butterworthLowPassCoefficients,
    },
    {
        id: "std::musical::ButterworthHighPassCoefficients",
        title: "Butterworth high pass filter coefficients",
        graph: butterworthHighPassCoefficients,
    },
    {
        id: "std::musical::Oscillator",
        title: "Oscillator",
        graph: oscillator,
    },
    {
        id: "std::utils::TimeSamples",
        title: "Time counter (samples)",
        graph: timeSamples,
    },
    {
        id: "std::utils::MillisecondsToSamples",
        title: "Milliseconds to samples",
        graph: millisecondsToSamples,
    },
    {
        id: "std::utils::DelayMS",
        title: "Delay (ms)",
        graph: delayMs,
    },
    {
        id: "std::utils::DelayMultichannelMS",
        title: "Delay multichannel (ms)",
        graph: delayMultichannelMs,
    },
    {
        id: "std::math::DotProduct4",
        title: "Dot product of vec4",
        graph: dotProduct4,
    },
    {
        id: "std::math::DotProduct8",
        title: "Dot product of vec8",
        graph: dotProduct8,
    },
    {
        id: "std::math::MulVec4ToNum",
        title: "Mul vec4 to number",
        graph: mulVec4ToNum,
    },
    {
        id: "std::math::MulVec8ToNum",
        title: "Mul vec8 to number",
        graph: mulVec8ToNum,
    },
    {
        id: "std::math::MulMat88ToVec8",
        title: "Mat_8_8 * Vec_8 => Vec_8",
        graph: mulMat88ToVec8,
    },
    {
        id: "std::math::NormalizeVec8",
        title: "Normalize Vec_8",
        graph: normalizeVec8,
    },
    {
        id: "std::math::MulVec8ToVec8",
        title: "Vec_8 * Vec_8 => Mat_8_8",
        graph: mulVec8ToVec8,
    },
    {
        id: "std::math::HouseholderMatrix88FromVec8",
        title: "Generate Householder matrix of size 8: H = I - 2*v*v⃰ ; where v - normalized input vector of size 8",
        graph: householderMatrix88FromVec8,
    },
    {
        id: "std::math::Signum",
        title: "sgn(x)",
        graph: signum,
    },
] as ReverbMacro[]).map((m) => [m.id, m]));


export function populateGraphWithStdMacros(graph: WholeReverbGraph) {
    graph.macros.push(...MACROS_STD.values());
}
