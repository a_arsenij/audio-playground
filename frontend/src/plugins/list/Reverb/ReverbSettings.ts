import { createConnection } from "../../../components/Common/Gridy/NodeEditor/Utils";
import { CreateSettingsArg, PluginCommonSettings } from "../../../types/Plugin";
import { createNewChannelPort } from "../../../utils/projectChanges/addChannel";
import {
    createInputNode,
    createOutputNode,
    ReverbMainInputNode,
    ReverbMainOutputNode,
    WholeReverbGraph,
} from "./ReverbNodes";
import { ReverbUIElement } from "./ui/ReverbUIEditor/ReverbUIEditor";

export type ReverbSettings =
    & PluginCommonSettings
    & {
    graph: WholeReverbGraph,
    elements: ReverbUIElement[],
    knobsValues: Record<string, number>,
    checkboxesValues: Record<string, boolean>,
}
    ;

export function reverbIONodesToPorts(
    pluginId: string,
    inputNode: ReverbMainInputNode,
    outputNode: ReverbMainOutputNode,
) {
    return {
        inputMidiChannels: [
            createNewChannelPort("Key", `${pluginId}:key`),
        ],
        outputMidiChannels: [],
        inputSignalChannels: inputNode.outputs
            .filter((v) => v.type === "mainInput.signal")
            .map((v) => createNewChannelPort(v.title, v.id)),
        outputSignalChannels: outputNode.inputs
            .filter((v) => v.type === "mainOutput.signal")
            .map((v) => createNewChannelPort(v.title, v.id)),
    };
}

export function createSimpleReverbSettings({
    pluginInstanceId,
}: CreateSettingsArg): ReverbSettings {
    const inputNode = createInputNode(0, 0);
    const outputNode = createOutputNode(10, 0);
    return {
        knobsValues: {},
        checkboxesValues: {},
        graph: {
            main: {
                nodes: [
                    inputNode,
                    outputNode,
                ],
                connections: [
                    createConnection(
                        inputNode.outputs[0]!.id,
                        outputNode.inputs[0]!.id,
                    ),
                    createConnection(
                        inputNode.outputs[1]!.id,
                        outputNode.inputs[1]!.id,
                    ),
                ],
            },
            macros: [],
        },
        elements: [],
        ...reverbIONodesToPorts(
            pluginInstanceId,
            inputNode,
            outputNode,
        ),
    };
}
