import {
    getMathOpOperandsNumber,
    MathOp,
    SingleOperandMathOp,
    TwoCommutativeOperandsMathOp,
    TwoNonCommutativeOperandsMathOp,
} from "@audio-playground/lib-common/math/Expr";
import { generateId } from "@audio-playground/lib-common/std/Id";
import { isTruethy } from "@audio-playground/lib-common/std/IsPresented";
import { areSetsEqual } from "@audio-playground/lib-common/std/Set";
import { withChanged } from "@audio-playground/lib-common/std/WithChanged";
import { applySetState } from "@audio-playground/lib-frontend/std/ApplySetState";
import { SetStateValue } from "@audio-playground/lib-frontend/std/SetState";

import {
    GetNodeInputsType,
    GetNodeOutputsType,
    NodeEditorGraph,
    NodeEditorNode,
    NodeEditorPort,
} from "../../../components/Common/Gridy/NodeEditor/NodeEditor";
import { MACROS_STD } from "./macroStd/macroStd";

export const REVERB_SINGLE_OPERAND_GATE_OPS = [
    "not",
] as const;

export const REVERB_TWO_COMMUTATIVE_OPERANDS_GATE_OPS = [
    "and",
    "or",
] as const;

export const REVERB_TWO_NOT_COMMUTATIVE_OPERANDS_GATE_OPS = [
    "xor",
    "xnor",
] as const;

export type ReverbSingleOperandGateOp = (typeof REVERB_SINGLE_OPERAND_GATE_OPS)[number];
export type ReverbTwoCommutativeOperandsGateOp = (typeof REVERB_TWO_COMMUTATIVE_OPERANDS_GATE_OPS)[number];
export type ReverbTwoNonCommutativeOperandsGateOp = (typeof REVERB_TWO_NOT_COMMUTATIVE_OPERANDS_GATE_OPS)[number];

export const REVERB_GATE_OPS = [
    ...REVERB_SINGLE_OPERAND_GATE_OPS,
    ...REVERB_TWO_COMMUTATIVE_OPERANDS_GATE_OPS,
    ...REVERB_TWO_NOT_COMMUTATIVE_OPERANDS_GATE_OPS,
] as const;

export type ReverbGateOp = (typeof REVERB_GATE_OPS)[number];

export type NumberPort = NodeEditorPort & {
    valueType: "number",
}

export type BooleanPort = NodeEditorPort & {
    valueType: "boolean",
}

export function isNumberPort<
    T extends NodeEditorPort & {valueType: "number" | "boolean"}
>(port: T): port is (T & NumberPort) {
    return port.valueType === "number";
}

export function isBooleanPort<
    T extends NodeEditorPort & {valueType: "number" | "boolean"}
>(port: T): port is (T & BooleanPort) {
    return port.valueType === "boolean";
}

export type ReverbMainInputSignalNodePort = NumberPort & {
    type: "mainInput.signal",
    title: string,
};

export type ReverbMainInputKnobNodePort = NumberPort & {
    type: "mainInput.knob",
    min: number,
    max: number,
    title: string,
};


export function isInputSignalPort(port: ReverbPort): port is ReverbMainInputSignalNodePort {
    return port.type === "mainInput.signal";
}

export function isKnobPort(port: ReverbPort): port is ReverbMainInputKnobNodePort {
    return port.type === "mainInput.knob";
}

export type ReverbMainInputKeyNodePort = NumberPort & {
    type: "mainInput.key",
    title: string,
};

export type ReverbMainInputCheckboxNodePort = BooleanPort & {
    type: "mainInput.checkbox",
    title: string,
};

export type ReverbMainInputNodePort =
    | ReverbMainInputSignalNodePort
    | ReverbMainInputKnobNodePort
    | ReverbMainInputKeyNodePort
    | ReverbMainInputCheckboxNodePort
;


export function createReverbMainInputSignalNodePort(
    title: string,
): ReverbMainInputNodePort {
    return {
        type: "mainInput.signal",
        valueType: "number",
        id: generateId(),
        title,
    };
}

export function createReverbMainInputKnobNodePort(
    title: string,
    min: number,
    max: number,
): ReverbMainInputNodePort {
    return {
        type: "mainInput.knob",
        valueType: "number",
        id: generateId(),
        title,
        min,
        max,
    };
}

export function createReverbMainInputKeyNodePort(): ReverbMainInputNodePort {
    return {
        type: "mainInput.key",
        valueType: "number",
        id: generateId(),
        title: "Key",
    };
}

export function createReverbMainInputCheckboxNodePort(
    title: string,
): ReverbMainInputNodePort {
    return {
        type: "mainInput.checkbox",
        valueType: "boolean",
        id: generateId(),
        title,
    };
}

export type ReverbMainInputNode = NodeEditorNode<
    [],
    (ReverbMainInputNodePort)[]
> & { type: "mainInput" };

export function isReverbMainInputNode(
    value: ReverbNode,
): value is ReverbMainInputNode {
    return value.type === "mainInput";
}

export type ReverbMainOutputUINodePort = NumberPort & {
    type: "mainOutput.ui",
    title: string,
};
export type ReverbMainOutputSignalNodePort = NumberPort & {
    type: "mainOutput.signal",
    title: string,
};


export function isOutputSignalPort(port: ReverbPort): port is ReverbMainOutputSignalNodePort {
    return port.type === "mainOutput.signal";
}

export type ReverbMainOutputNodePort =
    | ReverbMainOutputUINodePort
    | ReverbMainOutputSignalNodePort
    ;


export function createReverbMainOutputUINodePort(
    title: string,
): ReverbMainOutputUINodePort {
    return {
        type: "mainOutput.ui",
        valueType: "number",
        id: generateId(),
        title,
    };
}

export function createReverbMainOutputSignalNodePort(
    title: string,
): ReverbMainOutputSignalNodePort {
    return {
        type: "mainOutput.signal",
        valueType: "number",
        id: generateId(),
        title,
    };
}



export type ReverbMainOutputNode = NodeEditorNode<
    (ReverbMainOutputNodePort)[],
    []
> & { type: "mainOutput" };

export function isReverbMainOutputNode(
    value: ReverbNode,
): value is ReverbMainOutputNode {
    return value.type === "mainOutput";
}

type ConstDisablePort = BooleanPort & {
    type: "const.disable",
}

type ConstValuePort = NumberPort & {
    type: "const.value",
}

export type ReverbConstNode = NodeEditorNode<
    [ConstDisablePort],
    [ConstValuePort]
> & {
    type: "const",
    value: number,
};

type ExprVariablePort= NumberPort & {
    type: "expr.variable",
    variable: string,
}

type ExprResultPort = NumberPort & {
    type: "expr.result",
}

export type ReverbExprNode = NodeEditorNode<
    ExprVariablePort[],
    [ExprResultPort]
> & {
    type: "expr",
    expr: string,
};


type TimeValuePort = NumberPort & {
    type: "time.value",
}

export const REVERB_TIME_OPS = [
    "sampleRate",
] as const;

export type ReverbTimeOp = (typeof REVERB_TIME_OPS)[number];

export type ReverbTimeNode = NodeEditorNode<
    [],
    [TimeValuePort]
> & {
    type: "time",
    op: ReverbTimeOp,
};

export type MathInputPort = NumberPort & {
    type: "math.input",
    inputIndex: number,
}

type MathDisablePort = BooleanPort & {
    type: "math.disable",
}

type MathOutputPort = NumberPort & {
    type: "math.output",
}

export type ReverbSingleOpMathNode = NodeEditorNode<
    [MathInputPort, MathDisablePort],
    [MathOutputPort]
> & {
    type: "math",
    op: SingleOperandMathOp,
};

export type ReverbCommutativeOpMathNode = NodeEditorNode<
    [MathInputPort, MathInputPort, ...MathInputPort[], MathDisablePort],
    [MathOutputPort]
> & {
    type: "math",
    op: TwoCommutativeOperandsMathOp,
};

export type ReverbNonCommutativeOpMathNode = NodeEditorNode<
    [MathInputPort, MathInputPort, MathDisablePort],
    [MathOutputPort]
> & {
    type: "math",
    op: TwoNonCommutativeOperandsMathOp,
};

export type ReverbMathNode =
    | ReverbSingleOpMathNode
    | ReverbCommutativeOpMathNode
    | ReverbNonCommutativeOpMathNode
;

export type ReverbMathOpToNode<T extends MathOp> =
    T extends SingleOperandMathOp
        ? ReverbSingleOpMathNode
        : T extends TwoCommutativeOperandsMathOp
            ? ReverbCommutativeOpMathNode
            : T extends TwoNonCommutativeOperandsMathOp
                ? ReverbNonCommutativeOpMathNode
                : never;

type GateInputPort = BooleanPort & {
    type: "gate.input",
    inputIndex: number,
}

type GateOutputPort = BooleanPort & {
    type: "gate.output",
}


export type ReverbSingleOpGateNode = NodeEditorNode<
    [GateInputPort],
    [GateOutputPort]
> & {
    type: "gate",
    op: ReverbSingleOperandGateOp,
};

export type ReverbCommutativeOpGateNode = NodeEditorNode<
    [GateInputPort, GateInputPort, ...GateInputPort[]],
    [GateOutputPort]
> & {
    type: "gate",
    op: ReverbTwoCommutativeOperandsGateOp,
};

export type ReverbNonCommutativeOpGateNode = NodeEditorNode<
    [GateInputPort, GateInputPort],
    [GateOutputPort]
> & {
    type: "gate",
    op: ReverbTwoNonCommutativeOperandsGateOp,
};

export type ReverbGateNode =
    | ReverbSingleOpGateNode
    | ReverbCommutativeOpGateNode
    | ReverbNonCommutativeOpGateNode
    ;

export type ReverbGateOpToNode<T extends ReverbGateOp> =
    T extends ReverbSingleOperandGateOp
        ? ReverbSingleOpGateNode
        : T extends ReverbTwoCommutativeOperandsGateOp
            ? ReverbCommutativeOpGateNode
            : T extends ReverbTwoNonCommutativeOperandsGateOp
                ? ReverbNonCommutativeOpGateNode
                : never;

type DelayInputPort = NumberPort & {
    type: "delay.input",
}
type DelayTimePort = NumberPort & {
    type: "delay.time",
}
type DelayDisablePort = BooleanPort & {
    type: "delay.disable",
}
type DelayOutputPort = NumberPort & {
    type: "delay.output",
}

export type ReverbDelayTimeType = "milliseconds" | "samples";
export type ReverbDelayNode = NodeEditorNode<
    [DelayInputPort, DelayTimePort, DelayDisablePort],
    [DelayOutputPort]
> & {
    type: "delay",
    timeType: ReverbDelayTimeType,
    maxLength: number,
};

export type MacroUsageInputPort = (NumberPort | BooleanPort) & {
    type: "macroUsage.input",
    macroPortId: string,
}

export type MacroUsageOutputPort = (NumberPort | BooleanPort) & {
    type: "macroUsage.output",
    macroPortId: string,
}

export type ReverbMacroUsageNode = NodeEditorNode<
    MacroUsageInputPort[],
    MacroUsageOutputPort[]
> & {
    type: "macroUsage",
    macroId: string,
};

export function isReverbMacroUsageNode(
    value: ReverbNode,
): value is ReverbMacroUsageNode {
    return value.type === "macroUsage";
}


export function createMacrosDefinitionInputPort(
    valueType: "number" | "boolean",
): MacrosDefinitionInputPort {
    return {
        type: "macroDefinitionInput.output",
        valueType,
        id: generateId(),
        title: "New port",
    };
}

export function createMacrosDefinitionOutputPort(
    valueType: "number" | "boolean",
): MacrosDefinitionOutputPort {
    return {
        type: "macroDefinitionOutput.input",
        valueType,
        id: generateId(),
        title: "New port",
    };
}

export type MacrosDefinitionInputPort = (NumberPort | BooleanPort) & {
    type: "macroDefinitionInput.output",
    title: string,
}

export type ReverbMacrosDefinitionInputNode = NodeEditorNode<
    [],
    MacrosDefinitionInputPort[]
> & {
    type: "macroDefinitionInput",
};

export function isReverbMacroDefinitionInputNode(
    value: ReverbNode,
): value is ReverbMacrosDefinitionInputNode {
    return value.type === "macroDefinitionInput";
}

export type MacrosDefinitionOutputPort = (NumberPort | BooleanPort) & {
    type: "macroDefinitionOutput.input",
    title: string,
}

export type ReverbMacrosDefinitionOutputNode = NodeEditorNode<
    MacrosDefinitionOutputPort[],
    []
> & {
    type: "macroDefinitionOutput",
};

export function isReverbMacroDefinitionOutputNode(
    value: ReverbNode,
): value is ReverbMacrosDefinitionOutputNode {
    return value.type === "macroDefinitionOutput";
}

export function isReverbMacrosDefinitionNode(
    node: ReverbNode,
): node is (ReverbMacrosDefinitionInputNode | ReverbMacrosDefinitionOutputNode) {
    return isReverbMacroDefinitionInputNode(node) || isReverbMacroDefinitionOutputNode(node);
}


export type ReverbNode =
    | ReverbMainInputNode
    | ReverbMainOutputNode
    | ReverbConstNode
    | ReverbGateNode
    | ReverbMathNode
    | ReverbDelayNode
    | ReverbTimeNode
    | ReverbExprNode
    | ReverbMacroUsageNode
    | ReverbMacrosDefinitionInputNode
    | ReverbMacrosDefinitionOutputNode
;

export type ReverbInputPorts = ReverbNode extends NodeEditorNode<infer T, NodeEditorPort[]>
    ? T
    : never;
export type ReverbOutputPorts = ReverbNode extends NodeEditorNode<NodeEditorPort[], infer T>
    ? T
    : never;

export type ReverbPort = (ReverbInputPorts | ReverbOutputPorts)[number];

export type ReverbCheckbox = {
    id: string,
    title: string,
}

export function createReverbCheckbox(): ReverbCheckbox {
    return {
        id: generateId(),
        title: "New checkbox",
    };
}


export type ReverbGraph<T extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>= ReverbNode> = NodeEditorGraph<T>;

export type ReverbMacro<T extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>= ReverbNode> = {
    id: string,
    title: string,
    graph: ReverbGraph<T>,
};

export type WholeReverbGraph<T extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>= ReverbNode> = {
    main: ReverbGraph<T>,
    macros: ReverbMacro<T>[],
}

const ID_SAFE_REPLACEMENT = "_";
const ID_TYPE_SEPARATOR = "_";

function safeId(text: string): string {
    return `_${text
        .split("")
        .map((c) => {
            const l = c.toLowerCase();
            if (l >= "a" && l <= "z" || c >= "0" && c <= "9") {
                return c;
            }
            return ID_SAFE_REPLACEMENT;
        })
        .join("")}`;
}

type ReverbNumberPort = ReverbPort & NumberPort;
type ReverbBooleanPort = ReverbPort & BooleanPort;

export function numberPort<
    T extends ReverbNumberPort
>(
    type: T["type"],
    rest: Omit<T, "id" | "type" | "valueType">,
): T {
    return {
        id: `${safeId(type)}${ID_TYPE_SEPARATOR}${generateId()}`,
        type,
        valueType: "number",
        ...rest,
    } as T;
}

export function booleanPort<
    T extends ReverbBooleanPort
>(
    type: T["type"],
    rest: Omit<T, "id" | "type" | "valueType">,
): T {
    return {
        id: `${safeId(type)}${ID_TYPE_SEPARATOR}${generateId()}`,
        type,
        valueType: "boolean",
        ...rest,
    } as T;
}

function createNode<T extends ReverbNode>(
    value: Omit<
        T,
        "inputs" | "outputs" | "x" | "y" | "id" | "direction"
    >,
    inputs: GetNodeInputsType<T>,
    outputs: GetNodeOutputsType<T>,
    x: number = 0,
    y: number = 0,
): T {
    return {
        id: `${safeId(value.type)}${ID_TYPE_SEPARATOR}${generateId()}`,
        direction: "ltr" as const,
        x,
        y,
        inputs,
        outputs,
        ...value,
    } as T;
}

export function createInputNode(
    x: number,
    y: number,
    config?: {
        signalPorts?: boolean,
        keyPort?: boolean,
    },
): ReverbMainInputNode {
    return createNode<ReverbMainInputNode>(
        {
            type: "mainInput",
        },
        [],
        [
            config?.signalPorts !== false && createReverbMainInputSignalNodePort("Left"),
            config?.signalPorts !== false && createReverbMainInputSignalNodePort("Right"),
            config?.keyPort !== false && createReverbMainInputKeyNodePort(),
        ].filter(isTruethy),
        x,
        y,
    );
}

export function createOutputNode(
    x: number,
    y: number,
): ReverbMainOutputNode {
    return createNode<ReverbMainOutputNode>(
        {
            type: "mainOutput",
        },
        [
            createReverbMainOutputSignalNodePort("Left"),
            createReverbMainOutputSignalNodePort("Right"),
        ],
        [],
        x,
        y,
    );
}

export function createConstNode(
    value: number = 0,
): ReverbConstNode {
    return createNode(
        {
            type: "const",
            value,
        },
        [booleanPort("const.disable", {})],
        [numberPort("const.value", {})],
    );
}

export function createMathOpNodeInputs<NODE extends ReverbMathNode>(
    op: MathOp,
): GetNodeInputsType<NODE> {
    return [
        ...[...Array(getMathOpOperandsNumber(op))].map((_, idx) => numberPort(
            "math.input", {
                inputIndex: idx,
            })),
        booleanPort("math.disable", {}),
    ] as GetNodeInputsType<NODE>;
}

export function createMathOpNode<T extends MathOp>(
    op: T,
): ReverbMathOpToNode<T> {
    return createNode(
        // @ts-ignore
        {
            type: "math",
            op,
        },
        createMathOpNodeInputs(op),
        [numberPort("math.output", {})],
    );
}

export function getGateOpOperandsNumber(
    op: ReverbGateOp,
): number {
    return op === "not" ? 1 : 2;
}

export function createGateOpNodeInputs<T extends ReverbGateNode>(
    op: T["op"],
): GetNodeInputsType<T> {
    return [...Array(getGateOpOperandsNumber(op))].map((_, idx) => (booleanPort(
        "gate.input", {
            inputIndex: idx,
        }))) as GetNodeInputsType<T>;
}

export function createGateOpNode<T extends ReverbGateOp>(
    op: T,
): ReverbGateOpToNode<T> {
    return createNode(
        // @ts-ignore
        {
            type: "gate",
            op,
        },
        createGateOpNodeInputs(op),
        [booleanPort("gate.output", {})],
    );
}

export function createDelayNodeInputPort(): DelayInputPort {
    return numberPort("delay.input", {});
}

export function createDelayNodeOutputPort(): DelayOutputPort {
    return numberPort("delay.output", {});
}

export function createDelayNode(
    maxLength: number = 100_000,
    timeType: "milliseconds" | "samples" = "samples",
): ReverbDelayNode {
    return createNode(
        {
            type: "delay",
            timeType,
            maxLength,
        },
        [
            createDelayNodeInputPort(),
            numberPort("delay.time", {}),
            booleanPort("delay.disable", {}),
        ],
        [createDelayNodeOutputPort()],
    );
}

export function createTimeNode(
    func: ReverbTimeOp = "sampleRate",
): ReverbTimeNode {
    return createNode(
        {
            type: "time",
            op: func,
        },
        [],
        [numberPort("time.value", {})],
    );
}

export function createExprNode(): ReverbExprNode {
    return createNode(
        {
            type: "expr",
            expr: "1",
        },
        [],
        [numberPort("expr.result", {})],
    );
}

export function createMacroUsageNode(
    macroId: string = "",
): ReverbMacroUsageNode {
    return createNode(
        {
            type: "macroUsage",
            macroId,
        },
        [],
        [],
    );
}

export function createMacroDefinitionInputNode(): ReverbMacrosDefinitionInputNode {
    return createNode(
        {
            type: "macroDefinitionInput",
        },
        [],
        [],
        0,
    );
}

export function createMacroDefinitionOutputNode(): ReverbMacrosDefinitionOutputNode {
    return createNode(
        {
            type: "macroDefinitionOutput",
        },
        [],
        [],
        10,
    );
}

export function createMacros(
    title: string = "New Macro",
) {
    return {
        title,
        id: generateId(),
        graph: {
            nodes: [
                createMacroDefinitionInputNode(),
                createMacroDefinitionOutputNode(),
            ],
            connections: [],
        },
    };
}
export function reverbCreateMacroUsagePorts<SIDE extends "input" | "output">(
    side: SIDE,
    ports: (SIDE extends "input"
        ? MacrosDefinitionInputPort
        : MacrosDefinitionOutputPort
    )[],
): (SIDE extends "input" ? MacroUsageInputPort : MacroUsageOutputPort)[] {
    // @ts-ignore
    return ports.map((p) => ({
        id: generateId(),
        type: `macroUsage.${side}`,
        valueType: p.valueType,
        macroPortId: p.id,
    }));
}

export function reverbCreateMacroUsageInputs(
    input: ReverbMacrosDefinitionInputNode,
): MacroUsageInputPort[] {
    return reverbCreateMacroUsagePorts("input", input.outputs);
}

export function reverbCreateMacroUsageOutputs(
    output: ReverbMacrosDefinitionOutputNode,
): MacroUsageOutputPort[] {
    return reverbCreateMacroUsagePorts("output", output.inputs);
}

export function updateMacroUsagePortsAccordingToMacro(
    usage: ReverbMacroUsageNode,
    macro: ReverbMacro,
) {
    usage.inputs = reverbCreateMacroUsageInputs(
        macro.graph.nodes.find(isReverbMacroDefinitionInputNode)!,
    );
    usage.outputs = reverbCreateMacroUsageOutputs(
        macro.graph.nodes.find(isReverbMacroDefinitionOutputNode)!,
    );
}

export function getMacro(
    id: string,
    macros: Map<string, ReverbMacro>,
): ReverbMacro | undefined {
    return macros.get(id) ?? MACROS_STD.get(id);
}


export function reverbReplaceMainPorts<T extends "input" | "output">(
    graph: WholeReverbGraph,
    state: SetStateValue<(T extends "input"
        ? ReverbMainInputNodePort
        : ReverbMainOutputNodePort
        )[]>,
    side: T,
): WholeReverbGraph {
    const nodes = graph.main.nodes;
    const nodeIdx = nodes.findIndex(
        side === "input"
            ? isReverbMainInputNode
            : isReverbMainOutputNode,
    );
    const node = nodes[nodeIdx] as (
        T extends "input"
            ? ReverbMainInputNode
            : ReverbMainOutputNode
        );
    const newPorts = applySetState(
        state,
        (
            side === "input"
                ? node.outputs
                : node.inputs
        ) as (T extends "input" ? ReverbMainInputNodePort : ReverbMainOutputNodePort)[],
    );
    return withChanged(
        graph,
        [
            "main",
            "nodes", nodeIdx, side === "input" ? "outputs" : "inputs",
        ],
        // @ts-ignore
        newPorts,
    );
}


export function reverbReplaceMacrosPorts<T extends "input" | "output">(
    graph: WholeReverbGraph,
    subgraphIdx: number,
    state: SetStateValue<(T extends "input"
        ? MacrosDefinitionInputPort
        : MacrosDefinitionOutputPort
    )[]>,
    side: T,
): WholeReverbGraph {
    const macros = graph.macros[subgraphIdx]!;
    const nodes = macros.graph.nodes;
    const nodeIdx = nodes.findIndex(
        side === "input"
            ? isReverbMacroDefinitionInputNode
            : isReverbMacroDefinitionOutputNode,
    );
    const node = nodes[nodeIdx] as (
        T extends "input"
            ? ReverbMacrosDefinitionInputNode
            : ReverbMacrosDefinitionOutputNode
        );
    const newPorts = applySetState(
        state,
        (
            side === "input"
                ? node.outputs
                : node.inputs
        ) as (T extends "input" ? MacrosDefinitionInputPort : MacrosDefinitionOutputPort)[],
    );
    let changed = withChanged(
        graph,
        [
            "macros", subgraphIdx, "graph",
            "nodes", nodeIdx, side === "input" ? "outputs" : "inputs",
        ],
        // @ts-ignore
        newPorts,
    );
    const oldIds = node[side === "input" ? "outputs" : "inputs"].ids().toSet();
    const newIds = newPorts.ids().toSet();
    if (!areSetsEqual(oldIds, newIds)) {
        [
            changed.main,
            ...changed.macros.map((m) => m.graph),
        ].map((plane, idx) => {
            let newPlane = plane;
            newPlane.nodes.forEach((n, nodeIdx) => {
                if (isReverbMacroUsageNode(n) && n.macroId === macros.id) {
                    newPlane = withChanged(
                        newPlane,
                        ["nodes", nodeIdx],
                        {
                            ...n,
                            [side === "input"
                                ? "inputs"
                                : "outputs"
                            ]: reverbCreateMacroUsagePorts(side, newPorts),
                        },
                    );
                }
            });
            if (newPlane !== plane) {
                if (idx === 0) {
                    changed = withChanged(
                        changed,
                        ["main"],
                        newPlane,
                    );
                } else {
                    changed = withChanged(
                        changed,
                        ["macros", idx - 1, "graph"],
                        newPlane,
                    );
                }
            }
        });
    }
    return changed;
}
