import { Svg32PluginsNodes } from "../../../components/Common/GeneratedIcons/32/Plugins/Nodes";
import { PluginUIDecl } from "../../../types/PluginUIDecl";
import { ReverbSettings } from "./ReverbSettings";
import { ReverbUI } from "./ui/ReverbUI";

export const ReverbUIDecl: PluginUIDecl<ReverbSettings> = {
    name: "Reverb",
    UI: ReverbUI,
    Icon: Svg32PluginsNodes,
};
