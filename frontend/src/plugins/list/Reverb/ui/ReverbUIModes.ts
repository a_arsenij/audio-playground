
export const REVERB_UI_MODES = [
    "ui",
    "nodes",
    "uieditor",
] as const;

export type ReverbUIMode = (typeof REVERB_UI_MODES)[number];
