import { generateId } from "@audio-playground/lib-common/std/Id";
import { SetState, useAdd, useByKey } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import { Button } from "../../../../../components/Common/Button/Button";
import { Svg18UiCheckbox } from "../../../../../components/Common/GeneratedIcons/18/Ui/Checkbox";
import { Svg18UiKnob } from "../../../../../components/Common/GeneratedIcons/18/Ui/Knob";
import { Svg18UiLabel } from "../../../../../components/Common/GeneratedIcons/18/Ui/Label";
import { Svg18UiRadiobutton } from "../../../../../components/Common/GeneratedIcons/18/Ui/Radiobutton";
import { UIEditor, UIEditorElement } from "../../../../../components/Common/Gridy/UIEditor/UIEditor";
import {
    booleanPropertyF,
    booleanPropertyGS,
    enumPropertyF,
    numberPropertyF,
    Property,
    stringPropertyF,
} from "../../../../../components/Common/Properties/Properties";
import { isReverbMainInputNode } from "../../ReverbNodes";
import { ReverbSettings } from "../../ReverbSettings";
import styles from "./ReverbUIEditor.module.css";

export type ReverbUIKnobLinearElementValue = {
    func: "linear",
    step: number | undefined,
}

export type ReverbUIKnobExponentialElementValue = {
    func: "exponent",
    base: number,
}

export type ReverbUIKnobElement = {
    type: "knob",
    defaultValue: number,
    visual: "knob" | "slider",
    portId: string,
} & UIEditorElement & (
    | ReverbUIKnobLinearElementValue
    | ReverbUIKnobExponentialElementValue
);

export function createKnobElement(
    portId: string = "",
): ReverbUIKnobElement {
    return {
        type: "knob",
        func: "linear",
        visual: "knob",
        portId,
        step: undefined,
        defaultValue: 0,
        x: 0,
        y: 0,
        id: generateId(),
    };
}

export type ReverbUICheckboxElement = UIEditorElement & {
    type: "checkbox",
    portId: string,
    defaultState: boolean,
};

export function createCheckboxElement(portId: string = ""): ReverbUICheckboxElement {
    return {
        type: "checkbox",
        portId,
        defaultState: false,
        x: 0,
        y: 0,
        id: generateId(),
    };
}

export type ReverbUIRadiobuttonElement = UIEditorElement & {
    type: "radiobutton",
    groupId: string,
    portId: string,
    defaultState: boolean,
};

export function isRadiobuttonElement(e: ReverbUIElement): e is ReverbUIRadiobuttonElement {
    return e.type === "radiobutton";
}

function createRadiobuttonElement(): ReverbUIRadiobuttonElement {
    return {
        type: "radiobutton",
        groupId: generateId(),
        portId: "",
        defaultState: false,
        x: 0,
        y: 0,
        id: generateId(),
    };
}

export type ReverbUILabelElement = UIEditorElement &{
    type: "label",
    text: string,
};

function createLabelElement(): ReverbUILabelElement {
    return {
        type: "label",
        text: "",
        x: 0,
        y: 0,
        id: generateId(),
    };
}

function getTitle(element: ReverbUIElement) {
    return element.type;
}

function getContentIcon(element: ReverbUIElement) {
    switch (element.type) {
        case "checkbox":
            return <Svg18UiCheckbox fillContainer={true}/>;
        case "knob":
            return <Svg18UiKnob fillContainer={true}/>;
        case "radiobutton":
            return <Svg18UiRadiobutton fillContainer={true}/>;
        case "label":
            return <Svg18UiLabel fillContainer={true}/>;
    }
}

function getContent(element: ReverbUIElement) {
    return <div className={styles.iconWrapper}>
        <div className={styles.icon}>
            {getContentIcon(element)}
        </div>
    </div>;
}


function getCheckboxProperties(
    checkboxesIds: Map<string, { title: string }>,
): Property[] {
    return [
        booleanPropertyF("defaultState", {
            name: "Default value",
        }),
        enumPropertyF("portId", {
            name: "Binded checkbox",
            options: checkboxesIds.keys().toArray(),
            getVisibleTitle: (id) => checkboxesIds.get(id)?.title || "Unknown",
        }),
    ];
}


function getRadiobuttonProperties(
    checkboxesIds: Map<string, { title: string }>,
): Property[] {
    return [
        booleanPropertyF("defaultState", {
            name: "Default value",
        }),
        enumPropertyF("portId", {
            name: "Binded checkbox",
            options: checkboxesIds.keys().toArray(),
            getVisibleTitle: (id) => checkboxesIds.get(id)?.title || "Unknown",
        }),
        stringPropertyF("groupId", {
            name: "Radio group id",
        }),
    ];
}



function getKnobProperties(
    value: ReverbUIKnobElement,
    knobsIds: Map<string, { title: string }>,
): Property[] {
    return [
        enumPropertyF("portId", {
            name: "Binded knob",
            options: knobsIds.keys().toArray(),
            getVisibleTitle: (id) => knobsIds.get(id)?.title || "Unknown",
        }),
        enumPropertyF("func", {
            name: "Function",
            options: ["exponent", "linear"],
        }),
        numberPropertyF("defaultValue", {
            name: "Default value",
        }),
        value.func === "exponent" && numberPropertyF("base", {
            name: "Exponent base",
            defaultValue: 2,
        }),
        value.func === "linear" && booleanPropertyGS(
            (v) => v.step !== undefined,
            (v, f) => ({ ...v, step: f ? 0 : undefined }),
            {
                name: "Is steppy",
            }),
        value.func === "linear" && value.step !== undefined && numberPropertyF("step", {
            name: "Step",
        }),
    ].filter((v) => v) as Property[];
}

function getLabelProperties(
): Property[] {
    return [
        stringPropertyF("text", {
            name: "Text",
        }),
    ];
}

function getVProperties(
    value: ReverbUIElement,
    portIdToPort: Map<string, { title: string }>,
): Property[] {
    switch (value.type) {
        case "knob":
            return getKnobProperties(value, portIdToPort);
        case "checkbox":
            return getCheckboxProperties(portIdToPort);
        case "radiobutton":
            return getRadiobuttonProperties(portIdToPort);
        case "label":
            return getLabelProperties();
    }
}

function getEProperties(
    element: ReverbUIElement,
    portIdToPort: Map<string, { title: string }>,
): Property[] {
    return getVProperties(
        element,
        portIdToPort,
    ).map((p) => ({
        ...p,
        get: (v) => p.get(v),
        set: (e, val) => p.set(e, val),
    }));
}

export type ReverbUIElement =
    | ReverbUIKnobElement
    | ReverbUICheckboxElement
    | ReverbUIRadiobuttonElement
    | ReverbUILabelElement
;

type ReverbUIEditorProps = {
    pluginSettings: ReverbSettings,
    setPluginSettings: SetState<ReverbSettings>,
}

export function ReverbUIEditor({
    pluginSettings,
    setPluginSettings,
}: ReverbUIEditorProps) {

    const [elements, setElements] = useByKey(
        pluginSettings,
        setPluginSettings,
        "elements",
    );

    const addElement = useAdd(setElements);

    const input = pluginSettings.graph.main.nodes.find(isReverbMainInputNode)!;
    const portIdToPort = input.outputs.toMapById();

    return <UIEditor
        getTitle={getTitle}
        getContent={getContent}
        getProperties={(v: ReverbUIElement) => getEProperties(
            v,
            portIdToPort,
        )}
        elements={elements}
        setElements={setElements}
        rightOverlayContent={<>
            <Button
                title={"Add knob"}
                onClick={() => addElement(createKnobElement())}
                iconLeft={Svg18UiKnob}
            />
            <Button
                title={"Add checkbox"}
                onClick={() => addElement(createCheckboxElement())}
                iconLeft={Svg18UiCheckbox}
            />
            <Button
                title={"Add radiobutton"}
                onClick={() => addElement(createRadiobuttonElement())}
                iconLeft={Svg18UiRadiobutton}
            />
            <Button
                title={"Add label"}
                onClick={() => addElement(createLabelElement())}
                iconLeft={Svg18UiLabel}
            />
        </>}
    />;
}
