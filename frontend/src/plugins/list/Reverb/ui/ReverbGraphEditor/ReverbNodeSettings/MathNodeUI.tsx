import { getMathOpOperandsNumber, MATH_OPS, MathOp } from "@audio-playground/lib-common/math/Expr";
import React, { useCallback } from "react";

import { LabeledContainer } from "../../../../../../components/Common/LabeledContainer/LabeledContainer";
import { Select } from "../../../../../../components/Common/Select/Select";
import {
    createMathOpNodeInputs,
    ReverbMathNode,
} from "../../../ReverbNodes";
import { NodeUIProps } from "../ReverbNodesEditor/ReverbNodesEditor";


type MathNodeUIProps = NodeUIProps<ReverbMathNode>

export function MathNodeUI({
    value,
    setValue,
}: MathNodeUIProps) {
    const changeOp = useCallback((op: MathOp) => {
        setValue((prev) => {
            const oldKind = getMathOpOperandsNumber(prev.op);
            const newKind = getMathOpOperandsNumber(op);
            const inputs = (oldKind !== newKind) ? createMathOpNodeInputs(op) : prev.inputs;
            return {
                ...prev,
                inputs,
                op,
            } as ReverbMathNode;
        });
    }, [setValue]);
    return <>
        <LabeledContainer label={"Operation"} size={"small"}>
            <Select
                items={MATH_OPS}
                render={(e) => e}
                value={value.op}
                onChange={changeOp}
            />
        </LabeledContainer>
    </>;
}

