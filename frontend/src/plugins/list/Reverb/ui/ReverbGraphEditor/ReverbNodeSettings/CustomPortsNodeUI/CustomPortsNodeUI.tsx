import { applySetState } from "@audio-playground/lib-frontend/std/ApplySetState";
import {
    SetState,
    SetStateValue,
    useAdd,
    useDeleteById,
    useSetById, useSetByKey,
} from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback } from "react";

import { Button } from "../../../../../../../components/Common/Button/Button";
import { HoriLine } from "../../../../../../../components/Common/Containers/HoriLine/HoriLine";
import { VerColumn } from "../../../../../../../components/Common/Containers/VerColumn/VerColumn";
import { Svg18ClipsAddClipNotes } from "../../../../../../../components/Common/GeneratedIcons/18/Clips/AddClip/Notes";
import { Svg18ClipsAddClipWave } from "../../../../../../../components/Common/GeneratedIcons/18/Clips/AddClip/Wave";
import { Svg18CommonDelete } from "../../../../../../../components/Common/GeneratedIcons/18/Common/Delete";
import { Svg18UiCheckbox } from "../../../../../../../components/Common/GeneratedIcons/18/Ui/Checkbox";
import { Svg18UiKnob } from "../../../../../../../components/Common/GeneratedIcons/18/Ui/Knob";
import { NumberInput, TextInput } from "../../../../../../../components/Common/Input/Input";
import {
    createMacrosDefinitionInputPort,
    createMacrosDefinitionOutputPort,
    createReverbMainInputCheckboxNodePort,
    createReverbMainInputKeyNodePort,
    createReverbMainInputKnobNodePort,
    createReverbMainInputSignalNodePort,
    createReverbMainOutputSignalNodePort,
    createReverbMainOutputUINodePort,
    ReverbMacrosDefinitionInputNode,
    ReverbMacrosDefinitionOutputNode,
    ReverbMainInputNode,
    ReverbMainOutputNode,
    ReverbPort,
} from "../../../../ReverbNodes";
import { NodeUIProps } from "../../ReverbNodesEditor/ReverbNodesEditor";
import styles from "./CustomPortsNodeUI.module.css";

type MacroDefinitionInputNodeUIProps = NodeUIProps<ReverbMacrosDefinitionInputNode>;

export function MacroDefinitionInputNodeUI({
    value,
    setValue,
}: MacroDefinitionInputNodeUIProps) {
    const ports = value.outputs;
    const setPorts = useSetByKey(setValue, "outputs");
    return <Ports
        ports={ports}
        setPorts={setPorts}
        newPorts={[
            {
                title: "Number",
                create: () => createMacrosDefinitionInputPort("number"),
            },
            {
                title: "Boolean",
                create: () => createMacrosDefinitionInputPort("boolean"),
            },
        ]}
        renderBeforePort={(p) => {
            return <div className={styles.portIcon}>
                {(() => {
                    switch (p.valueType) {
                        case "number": return <Svg18UiKnob/>;
                        case "boolean": return <Svg18UiCheckbox/>;
                    }
                })()}
            </div>;
        }}
    />;
}

type MacroDefinitionOutputNodeUIProps = NodeUIProps<ReverbMacrosDefinitionOutputNode>;

export function MacroDefinitionOutputNodeUI({
    value,
    setValue,
}: MacroDefinitionOutputNodeUIProps) {
    const ports = value.inputs;
    const setPorts = useSetByKey(setValue, "inputs");
    return <Ports
        ports={ports}
        setPorts={setPorts}
        newPorts={[
            {
                title: "Number",
                create: () => createMacrosDefinitionOutputPort("number"),
            },
            {
                title: "Boolean",
                create: () => createMacrosDefinitionOutputPort("boolean"),
            },
        ]}
        renderBeforePort={(p) => {
            return <div className={styles.portIcon}>
                {(() => {
                    switch (p.valueType) {
                        case "number": return <Svg18UiKnob/>;
                        case "boolean": return <Svg18UiCheckbox/>;
                    }
                })()}
            </div>;
        }}
    />;
}

type MainInputNodeUIProps = NodeUIProps<ReverbMainInputNode>;

export function MainInputNodeUI({
    value,
    setValue,
}: MainInputNodeUIProps) {
    const ports = value.outputs;
    const setPorts = useSetByKey(setValue, "outputs");
    return <Ports
        ports={ports}
        setPorts={setPorts}
        newPorts={[
            {
                title: "Signal",
                create: () => createReverbMainInputSignalNodePort("Signal"),
            },
            {
                title: "Knob",
                create: () => createReverbMainInputKnobNodePort("Knob", 1, 10),
            },
            {
                title: "Key",
                create: () => createReverbMainInputKeyNodePort(),
            },
            {
                title: "Checkbox",
                create: () => createReverbMainInputCheckboxNodePort("Checkbox"),
            },
        ]}
        renderAfterPort={(p, set) => {
            if (p.type !== "mainInput.knob") {
                return;
            }
            return <>
                <NumberInput
                    key={"min"}
                    size={"tiny"}
                    value={p.min}
                    onChange={(v) => set((prev) => ({ ...prev, min: v }))}
                />
                <NumberInput
                    key={"max"}
                    size={"tiny"}
                    value={p.max}
                    onChange={(v) => set((prev) => ({ ...prev, max: v }))}
                />
            </>;
        }}
        renderBeforePort={(p) => {
            return <div className={styles.portIcon}>
                {(() => {
                    switch (p.type) {
                        case "mainInput.signal": return <Svg18ClipsAddClipWave/>;
                        case "mainInput.key": return <Svg18ClipsAddClipNotes/>;
                        case "mainInput.knob": return <Svg18UiKnob/>;
                        case "mainInput.checkbox": return <Svg18UiCheckbox/>;
                    }
                })()}
            </div>;
        }}
    />;
}

type MainOutputNodeUIProps = NodeUIProps<ReverbMainOutputNode>;

export function MainOutputNodeUI({
    value,
    setValue,
}: MainOutputNodeUIProps) {
    const ports = value.inputs;
    const setPorts = useSetByKey(setValue, "inputs");
    return <Ports
        ports={ports}
        setPorts={setPorts}
        newPorts={[
            {
                title: "Signal",
                create: () => createReverbMainOutputSignalNodePort("Signal"),
            },
            {
                title: "UI",
                create: () => createReverbMainOutputUINodePort("UI"),
            },
        ]}
        renderBeforePort={(p) => {
            return <div className={styles.portIcon}>
                {(() => {
                    switch (p.type) {
                        case "mainOutput.signal": return <Svg18ClipsAddClipWave/>;
                        case "mainOutput.ui": return <Svg18UiKnob/>;
                    }
                })()}
            </div>;
        }}
    />;
}

type PortsProps<
    T extends ReverbPort & {title: string},
> = {
    ports: T[],
    setPorts: SetState<T[]>,
    newPorts: {
        title: string,
        create: () => T,
    }[],
    renderBeforePort?: (t: T, set: SetState<T>) => React.ReactNode,
    renderAfterPort?: (t: T, set: SetState<T>) => React.ReactNode,
}

function Ports<
    T extends ReverbPort & {title: string},
>({
    ports,
    setPorts,
    newPorts,
    renderBeforePort,
    renderAfterPort,
}: PortsProps<T>) {
    const deletePortById = useDeleteById(setPorts);
    const setPortById = useSetById(setPorts);
    const setNameById = useCallback(
        (
            id: string,
            title: SetStateValue<string>,
        ) => {
            setPortById(
                id,
                (p) => ({
                    ...p,
                    title: applySetState(title, p.title),
                }),
            );
        },
        [setPortById],
    );
    const addPort = useAdd(setPorts);
    const addNewPort = useCallback((port: T) => {
        addPort(port);
    }, [addPort]);
    return <>
        <VerColumn>
            <HoriLine>
                {
                    newPorts.map((c) => <Button
                        onClick={() => addNewPort(c.create())}
                        title={c.title}
                        key={c.title}
                    />)
                }
            </HoriLine>
            {ports.map((p) =>
                <Port
                    key={p.id}
                    valueType={p.valueType}
                    title={p.title}
                    setTitle={(v) => setNameById(p.id, v)}
                    onDelete={() => deletePortById(p.id)}
                    before={renderBeforePort && renderBeforePort(
                        p,
                        (set) => setPortById(p.id, set),
                    )}
                    after={renderAfterPort && renderAfterPort(
                        p,
                        (set) => setPortById(p.id, set),
                    )}
                />,
            )}
        </VerColumn>
    </>;
}


type PortProps = {
    valueType: "boolean" | "number",
    title: string,
    setTitle: SetState<string>,
    onDelete: () => void,
    before?: React.ReactNode,
    after?: React.ReactNode,
}

function Port({
    title,
    setTitle,
    onDelete,
    before,
    after,
}: PortProps) {
    return <HoriLine>
        {before}
        <TextInput
            size={"tiny"}
            value={title}
            onChange={setTitle}
        />
        {after}
        <Button
            onClick={onDelete}
            iconLeft={Svg18CommonDelete}
            title={"Delete port"}
        />
    </HoriLine>;
}
