import { useByKey } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import { NumberInput } from "../../../../../../components/Common/Input/Input";
import { LabeledContainer } from "../../../../../../components/Common/LabeledContainer/LabeledContainer";
import { ReverbDelayNode } from "../../../ReverbNodes";
import { NodeUIProps } from "../ReverbNodesEditor/ReverbNodesEditor";

export const DelayNodeUI = (props: NodeUIProps<ReverbDelayNode>) => {
    const [length, setLength] = useByKey(
        props.value,
        props.setValue,
        "maxLength",
    );
    return <>
        <LabeledContainer label={"Max length"} size={"small"}>
            <NumberInput
                value={length}
                onChange={setLength}
                size={"small"}
            />
        </LabeledContainer>
    </>;
};
