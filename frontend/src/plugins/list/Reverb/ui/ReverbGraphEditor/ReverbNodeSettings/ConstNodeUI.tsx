import { useByKey } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import { Button } from "../../../../../../components/Common/Button/Button";
import { HoriLine } from "../../../../../../components/Common/Containers/HoriLine/HoriLine";
import { VerColumn } from "../../../../../../components/Common/Containers/VerColumn/VerColumn";
import { NumberInput } from "../../../../../../components/Common/Input/Input";
import { LabeledContainer } from "../../../../../../components/Common/LabeledContainer/LabeledContainer";
import { ReverbConstNode } from "../../../ReverbNodes";
import { NodeUIProps } from "../ReverbNodesEditor/ReverbNodesEditor";

const CONST_NODE_PRESETS = [
    {
        title: "π",
        value: Math.PI,
    },
    {
        title: "2×π",
        value: 2 * Math.PI,
    },
    {
        title: "e",
        value: Math.E,
    },
];

const PRIMES = [
    2, 3, 5, 7, 11, 13, 17, 19,
    23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
    73, 79, 83, 89, 97,
];

const RANDOMS = [
    {
        min: -1,
        max: 1,
    },
    {
        min: 0,
        max: 1,
    },
    {
        min: 0,
        max: 10,
    },
    {
        min: 0,
        max: 100,
    },
];

export const ConstNodeUI = (props: NodeUIProps<ReverbConstNode>) => {
    const [value, setValue] = useByKey(
        props.value,
        props.setValue,
        "value",
    );
    return <VerColumn>
        <LabeledContainer label={"Value"} size={"small"}>
            <HoriLine>
                <NumberInput
                    value={value}
                    onChange={setValue}
                    size={"small"}
                />
            </HoriLine>
        </LabeledContainer>
        <LabeledContainer label={"Math constants"} size={"small"}>
            <HoriLine>
                {CONST_NODE_PRESETS.map((p) =>
                    <Button
                        key={p.title}
                        title={p.title}
                        onClick={() => setValue(p.value)}
                    />,
                )}
            </HoriLine>
        </LabeledContainer>
        <LabeledContainer label={"Primes"} size={"small"}>
            <HoriLine>
                {PRIMES.map((p) =>
                    <Button
                        key={p}
                        onClick={() => setValue(p)}
                        title={`${p}`}
                    />,
                )}
            </HoriLine>
        </LabeledContainer>
        <LabeledContainer label={"Randoms"} size={"small"}>
            <HoriLine>
                {RANDOMS.map((p) =>
                    <Button
                        key={`${p.min}-${p.max}`}
                        onClick={() => setValue(
                            p.min + Math.random() * (p.max - p.min),
                        )}
                        title={`${p.min}...${p.max}`}
                    />,
                )}
            </HoriLine>
        </LabeledContainer>
    </VerColumn>;
};
