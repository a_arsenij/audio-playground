import { applySetState } from "@audio-playground/lib-frontend/std/ApplySetState";
import { SetState, SetStateValue } from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback } from "react";

import {
    ReverbConstNode,
    ReverbDelayNode,
    ReverbExprNode,
    ReverbGateNode,
    ReverbMacro,
    ReverbMacrosDefinitionInputNode,
    ReverbMacrosDefinitionOutputNode,
    ReverbMacroUsageNode,
    ReverbMainInputNode,
    ReverbMainOutputNode,
    ReverbMathNode,
    ReverbNode, ReverbTimeNode,
} from "../../../ReverbNodes";
import { ConstNodeUI } from "./ConstNodeUI";
import {
    MacroDefinitionInputNodeUI,
    MacroDefinitionOutputNodeUI,
    MainInputNodeUI,
    MainOutputNodeUI,
} from "./CustomPortsNodeUI/CustomPortsNodeUI";
import { DelayNodeUI } from "./DelayNodeUI";
import { ExprNodeUI } from "./ExprNodeUI";
import { GateNodeUI } from "./GateNodeUI";
import { MacroUsageNodeUI } from "./MacroUsageNodeUI";
import { MathNodeUI } from "./MathNodeUI";
import styles from "./ReverbNodeSettings.module.css";
import { TimeNodeUI } from "./TimeNodeUI";

type ContentProps = {
    macros: Map<string, ReverbMacro>,
    node: ReverbNode,
    setNodeById: (
        id: string,
        set: SetStateValue<ReverbNode>,
        updatePluginIO: boolean
    ) => void,
    fastAddMacro: () => string,
}

export function isUneditableNode(node: ReverbNode): node is (
    | ReverbMainInputNode
    | ReverbMainOutputNode
    | ReverbMacrosDefinitionInputNode
    | ReverbMacrosDefinitionOutputNode
    ) {
    return node.type === "mainInput" ||
        node.type === "mainOutput" ||
        node.type === "macroDefinitionInput" ||
        node.type === "macroDefinitionOutput";
}

export function isNodeDeleteable(node: ReverbNode) {
    return !isUneditableNode(node);
}

export function isNodeCopiable(node: ReverbNode) {
    return !isUneditableNode(node);
}

export function ReverbNodeSettings({
    macros,
    node,
    setNodeById,
    fastAddMacro,
}: ContentProps) {
    const setNode = useCallback(
        (
            change: SetStateValue<ReverbNode>,
        ) => setNodeById(
            node.id,
            (node) => applySetState(change, node),
            false,
        ),
        [node.id, setNodeById],
    );
    const setMainNode = useCallback(
        (
            change: SetStateValue<ReverbNode>,
        ) => setNodeById(
            node.id,
            (node) => applySetState(change, node),
            true,
        ),
        [node.id, setNodeById],
    );
    return <div
        className={styles.reverbNodeSettings}
        key={node.id}
    >
        {(() => {
            switch (node.type) {
                case "macroDefinitionInput":
                    return <MacroDefinitionInputNodeUI
                        value={node}
                        setValue={setNode as SetState<ReverbMacrosDefinitionInputNode>}
                    />;
                case "macroDefinitionOutput":
                    return <MacroDefinitionOutputNodeUI
                        value={node}
                        setValue={setNode as SetState<ReverbMacrosDefinitionOutputNode>}
                    />;
                case "mainInput":
                    return <MainInputNodeUI
                        value={node}
                        setValue={setMainNode as SetState<ReverbMainInputNode>}
                    />;
                case "mainOutput":
                    return <MainOutputNodeUI
                        value={node}
                        setValue={setMainNode as SetState<ReverbMainOutputNode>}
                    />;
                case "macroUsage":
                    return <MacroUsageNodeUI
                        fastAddMacro={fastAddMacro}
                        macros={macros}
                        value={node}
                        setValue={setNode as SetState<ReverbMacroUsageNode>}
                    />;
                case "math":
                    return <MathNodeUI
                        value={node}
                        setValue={setNode as SetState<ReverbMathNode>}
                    />;
                case "gate":
                    return <GateNodeUI
                        value={node}
                        setValue={setNode as SetState<ReverbGateNode>}
                    />;
                case "const":
                    return <ConstNodeUI
                        value={node}
                        setValue={setNode as SetState<ReverbConstNode>}
                    />;
                case "delay":
                    return <DelayNodeUI
                        value={node}
                        setValue={setNode as SetState<ReverbDelayNode>}
                    />;
                case "time":
                    return <TimeNodeUI
                        value={node}
                        setValue={setNode as SetState<ReverbTimeNode>}
                    />;
                case "expr":
                    return <ExprNodeUI
                        value={node}
                        setValue={setNode as SetState<ReverbExprNode>}
                    />;
            }
        })()}
    </div>;
}
