
import { parseExpr } from "@audio-playground/lib-common/math/Expr";
import { generateId } from "@audio-playground/lib-common/std/Id";
import { useAdd, useByKey, useDeleteByIndex, useSetByIndex } from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback } from "react";

import { Button } from "../../../../../../components/Common/Button/Button";
import { HoriLine } from "../../../../../../components/Common/Containers/HoriLine/HoriLine";
import { Svg18CommonDelete } from "../../../../../../components/Common/GeneratedIcons/18/Common/Delete";
import { Svg18SignPlus } from "../../../../../../components/Common/GeneratedIcons/18/Sign/Plus";
import { TextInput } from "../../../../../../components/Common/Input/Input";
import {
    InputWithAddressedError,
} from "../../../../../../components/Common/InputWithAddressedError/InputWithAddressedError";
import { LabeledContainer } from "../../../../../../components/Common/LabeledContainer/LabeledContainer";
import { ReverbExprNode } from "../../../ReverbNodes";
import { NodeUIProps } from "../ReverbNodesEditor/ReverbNodesEditor";


type ExprNodeUIProps = NodeUIProps<ReverbExprNode>

export function ExprNodeUI({
    value,
    setValue,
}: ExprNodeUIProps) {
    const [expr, setExpr] = useByKey(
        value,
        setValue,
        "expr",
    );

    const [ports, setPorts] = useByKey(
        value,
        setValue,
        "inputs",
    );

    const addPort = useAdd(setPorts);

    const addNewPort = useCallback(() => {
        addPort({
            id: generateId(),
            variable: `_${Date.now()}`,
            type: "expr.variable",
            valueType: "number",
        });
    }, [addPort]);

    const deletePortByIndex = useDeleteByIndex(setPorts);
    const changeByIndex = useSetByIndex(setPorts);

    const setTitle = useCallback((title: string, index: number) => {
        changeByIndex(index, (prev) => ({
            ...prev,
            variable: title,
        }));
    }, [changeByIndex]);

    const validate = useCallback(
        (e: string) => {
            const p = parseExpr(e);
            if (p.type === "ok") {
                return undefined;
            }
            return p;
        },
        [],
    );

    return <>
        <LabeledContainer label={"Function"} size={"small"}>
            <InputWithAddressedError
                value={expr}
                onChange={setExpr}
                size={"small"}
                validate={validate}
            />
        </LabeledContainer>
        <LabeledContainer label={"Inputs"}>
            {ports.map((p, idx) =>
                <ExprNodePort
                    title={p.variable}
                    setTitle={(t) => setTitle(t, idx)}
                    onDelete={() => deletePortByIndex(idx)}
                />,
            )}
            <HoriLine>
                <Button
                    onClick={addNewPort}
                    iconLeft={Svg18SignPlus}
                    title={"Add new port"}
                />
            </HoriLine>
        </LabeledContainer>
    </>;
}

type ExprNodePortProps = {
    title: string,
    setTitle: (value: string) => void,
    onDelete: () => void,
}

function ExprNodePort({
    title, setTitle, onDelete,
}: ExprNodePortProps) {
    return <HoriLine>
        <TextInput
            value={title}
            onChange={setTitle}
        />
        <Button
            onClick={onDelete}
            iconLeft={Svg18CommonDelete}
            title={"Delete port"}
        />
    </HoriLine>;
}
