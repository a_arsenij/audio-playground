import React, { useCallback } from "react";

import { Svg18GatesAnd } from "../../../../../../components/Common/GeneratedIcons/18/Gates/And";
import { Svg18GatesNot } from "../../../../../../components/Common/GeneratedIcons/18/Gates/Not";
import { Svg18GatesOr } from "../../../../../../components/Common/GeneratedIcons/18/Gates/Or";
import { Svg18GatesXnor } from "../../../../../../components/Common/GeneratedIcons/18/Gates/Xnor";
import { Svg18GatesXor } from "../../../../../../components/Common/GeneratedIcons/18/Gates/Xor";
import { LabeledContainer } from "../../../../../../components/Common/LabeledContainer/LabeledContainer";
import { Select } from "../../../../../../components/Common/Select/Select";
import {
    createGateOpNodeInputs,
    getGateOpOperandsNumber, REVERB_GATE_OPS,
    ReverbGateNode,
    ReverbGateOp,
} from "../../../ReverbNodes";
import { NodeUIProps } from "../ReverbNodesEditor/ReverbNodesEditor";

type GateNodeUIProps = NodeUIProps<ReverbGateNode>

function gateTo18Icon(op: ReverbGateNode["op"]) {
    switch (op) {
        case "and": return Svg18GatesAnd;
        // case "nand": return Svg18GatesNand;
        case "or": return Svg18GatesOr;
        // case "nor": return Svg18GatesNor;
        case "xor": return Svg18GatesXor;
        case "xnor": return Svg18GatesXnor;
        case "not": return Svg18GatesNot;
    }
}

export function GateNodeUI({
    value,
    setValue,
}: GateNodeUIProps) {
    const changeOp = useCallback((op: ReverbGateOp) => {
        setValue((prev) => {
            const oldKind = getGateOpOperandsNumber(prev.op);
            const newKind = getGateOpOperandsNumber(op);
            const inputs = (oldKind !== newKind) ? createGateOpNodeInputs(op) : prev.inputs;
            return {
                ...prev,
                inputs,
                op,
            } as ReverbGateNode;
        });
    }, [setValue]);
    return <>
        <LabeledContainer label={"Operation"} size={"small"}>
            <Select
                items={REVERB_GATE_OPS}
                render={(e) => ({
                    icon: gateTo18Icon(e),
                    title: e,
                })}
                value={value.op}
                onChange={changeOp}
            />
        </LabeledContainer>
    </>;
}
