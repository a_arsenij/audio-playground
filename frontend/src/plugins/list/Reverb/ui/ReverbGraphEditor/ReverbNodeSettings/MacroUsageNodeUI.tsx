import { useByKey } from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback } from "react";

import { Button } from "../../../../../../components/Common/Button/Button";
import { HoriLine } from "../../../../../../components/Common/Containers/HoriLine/HoriLine";
import { Svg18SignPlus } from "../../../../../../components/Common/GeneratedIcons/18/Sign/Plus";
import { LabeledContainer } from "../../../../../../components/Common/LabeledContainer/LabeledContainer";
import { Select } from "../../../../../../components/Common/Select/Select";
import { MACROS_STD } from "../../../macroStd/macroStd";
import {
    getMacro,
    ReverbMacro,
    ReverbMacroUsageNode, updateMacroUsagePortsAccordingToMacro,
} from "../../../ReverbNodes";
import { NodeUIProps } from "../ReverbNodesEditor/ReverbNodesEditor";


type MacroUsageNodeUIProps = NodeUIProps<ReverbMacroUsageNode> & {
    fastAddMacro: () => string,
    macros: Map<string, ReverbMacro>,
}

export function getMacroTitle(
    id: string,
    macros: Map<string, ReverbMacro>,
): string {
    return getMacro(id, macros)?.title ?? "";
}

export const MacroUsageNodeUI = ({
    value,
    setValue,
    macros,
    fastAddMacro,
}: MacroUsageNodeUIProps) => {
    const [macroId, setMacroId] = useByKey(
        value,
        setValue,
        "macroId",
    );
    const changeMacroId = useCallback(
        (newId: string) => {
            setValue((prev) => {
                const usage = {
                    ...prev,
                    macroId: newId,
                    inputs: [],
                    outputs: [],
                };
                const macro = getMacro(newId, macros);
                if (macro) {
                    updateMacroUsagePortsAccordingToMacro(usage, macro);
                }
                return usage;
            });
        },
        [macros, setValue],
    );
    const selectNewMacro = useCallback(() => {
        const id = fastAddMacro();
        setMacroId(id);
    }, [setMacroId, fastAddMacro]);
    return <>
        <LabeledContainer label={"Title"} size={"small"}>
            <HoriLine>
                <Select
                    items={[...macros.keys().toArray(), ...MACROS_STD.keys()]}
                    value={macroId}
                    onChange={changeMacroId}
                    render={(v) => getMacroTitle(v, macros)}
                />

                <Button
                    onClick={selectNewMacro}
                    title={"Fast create new macro"}
                    iconLeft={Svg18SignPlus}
                />
            </HoriLine>
        </LabeledContainer>
    </>;
};
