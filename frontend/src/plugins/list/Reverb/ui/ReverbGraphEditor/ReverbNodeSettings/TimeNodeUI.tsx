import React, { useCallback } from "react";

import { LabeledContainer } from "../../../../../../components/Common/LabeledContainer/LabeledContainer";
import { Select } from "../../../../../../components/Common/Select/Select";
import {
    REVERB_TIME_OPS,
    ReverbTimeNode,
    ReverbTimeOp,
} from "../../../ReverbNodes";
import { NodeUIProps } from "../ReverbNodesEditor/ReverbNodesEditor";


type TimeNodeUIProps = NodeUIProps<ReverbTimeNode>

export function TimeNodeUI({
    value,
    setValue,
}: TimeNodeUIProps) {
    const changeOp = useCallback((op: ReverbTimeOp) => {
        setValue((prev) => {
            return {
                ...prev,
                op,
            } as ReverbTimeNode;
        });
    }, [setValue]);
    return <>
        <LabeledContainer label={"Function"} size={"small"}>
            <Select
                items={REVERB_TIME_OPS}
                render={(e) => e}
                value={value.op}
                onChange={changeOp}
            />
        </LabeledContainer>
    </>;
}
