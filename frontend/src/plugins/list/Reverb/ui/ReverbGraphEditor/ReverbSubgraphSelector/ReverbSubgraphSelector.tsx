import { SetState, useAdd, useByKey } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import { Button } from "../../../../../../components/Common/Button/Button";
import { Svg18CommonDelete } from "../../../../../../components/Common/GeneratedIcons/18/Common/Delete";
import { OptionallySelectedBox } from "../../../../../../components/Common/OptionallySelectedBox/OptionallySelectedBox";
import { createMacros, WholeReverbGraph } from "../../../ReverbNodes";
import styles from "./ReverbSubgraphSelector.module.css";

type ReverbSubgraphSelectorProps = {
    subgraphIdx: number,
    setSubgraphIdx: SetState<number>,
    graph: WholeReverbGraph,
    setGraph: SetState<WholeReverbGraph>,
    onDeleteMacros: (idx: number) => void,
}

export function ReverbSubgraphSelector(props: ReverbSubgraphSelectorProps) {

    const { subgraphIdx, setSubgraphIdx, graph, setGraph } = props;

    const [macros, setMacros] = useByKey(
        graph,
        setGraph,
        "macros",
    );
    const addMacros = useAdd(setMacros);


    return <div className={styles.reverbSubgraphSelector}>
        <Button
            title={"Add new macro"}
            onClick={() => {
                const newMacros = createMacros("New Macro");
                addMacros(newMacros);
            }}
        />

        <ul>
            <Subgraph
                key={"main"}
                title={"Main"}
                subgraphIdx={-1}
                selectedSubgraphIdx={subgraphIdx}
                setSelectedSubgraphIdx={setSubgraphIdx}
                onDeleteMacro={undefined}
            />
            {macros.map((macros, i) => {
                return <Subgraph
                    key={macros.id}
                    title={macros.title}
                    subgraphIdx={i}
                    selectedSubgraphIdx={subgraphIdx}
                    setSelectedSubgraphIdx={setSubgraphIdx}
                    onDeleteMacro={() => props.onDeleteMacros(i)}
                />;
            })}
        </ul>
    </div>;
}




type SubgraphProps = {
    title: string,
    subgraphIdx: number,
    selectedSubgraphIdx: number,
    setSelectedSubgraphIdx: SetState<number>,
    onDeleteMacro: (() => void) | undefined,
}

function Subgraph({
    title,
    subgraphIdx,
    selectedSubgraphIdx,
    setSelectedSubgraphIdx,
    onDeleteMacro,
}: SubgraphProps) {
    return <li>
        <OptionallySelectedBox
            isSelected={selectedSubgraphIdx === subgraphIdx}
            onClick={() => {
                setSelectedSubgraphIdx(subgraphIdx);
            }}
        >
            <div className={styles.subgraph}>
                <div className={styles.title}>
                    {title}
                </div>
                {onDeleteMacro && <Button
                    title={"Delete macro"}
                    onClick={onDeleteMacro}
                    style={"ghost"}
                    iconLeft={Svg18CommonDelete}
                />}
            </div>
        </OptionallySelectedBox>
    </li>;
}
