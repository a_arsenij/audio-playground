import { applySetState } from "@audio-playground/lib-frontend/std/ApplySetState";
import {
    setById,
    setByIndex,
    SetState,
    SetStateValue,
    useByKey,
    useDeleteByIndex,
    useSetByIndex,
} from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback, useMemo, useState } from "react";

import { Button } from "../../../../../components/Common/Button/Button";
import { HoriLine } from "../../../../../components/Common/Containers/HoriLine/HoriLine";
import { Padded } from "../../../../../components/Common/Containers/Padded/Padded";
import { Svg18CommonDelete } from "../../../../../components/Common/GeneratedIcons/18/Common/Delete";
import { TextInput } from "../../../../../components/Common/Input/Input";
import { SidePanel } from "../../../../../components/Common/SidePanel/SidePanel";
import { ReverbGraph, ReverbMainInputNode, ReverbMainOutputNode, ReverbNode } from "../../ReverbNodes";
import { reverbIONodesToPorts, ReverbSettings } from "../../ReverbSettings";
import { ReverbNodesEditor } from "./ReverbNodesEditor/ReverbNodesEditor";
import { ReverbNodeSettings } from "./ReverbNodeSettings/ReverbNodeSettings";
import { ReverbSubgraphSelector } from "./ReverbSubgraphSelector/ReverbSubgraphSelector";

type ReverbGraphEditorProps = {
    pluginId: string,
    pluginSettings: ReverbSettings,
    setPluginSettings: SetState<ReverbSettings>,
    fastAddMacro: () => string,
}

export function ReverbGraphEditor({
    pluginId,
    pluginSettings,
    setPluginSettings,
    fastAddMacro,
}: ReverbGraphEditorProps) {
    const [graph, setGraph] = useByKey(
        pluginSettings,
        setPluginSettings,
        "graph",
    );
    const [main, setMain] = useByKey(
        graph,
        setGraph,
        "main",
    );
    const [macros, setMacros] = useByKey(graph, setGraph, "macros");
    const setMacrosByIdx = useSetByIndex(setMacros);
    const [unsafeSubgraphIdx, setSubgraphIdx] = useState(-1);
    const subgraphIdx = unsafeSubgraphIdx >= macros.length ? -1 : unsafeSubgraphIdx;
    const currentGraph = subgraphIdx === -1 ? main : macros[subgraphIdx]!.graph;
    const setCurrentGraph = useCallback((v: SetStateValue<ReverbGraph>) => {
        if (subgraphIdx === -1) {
            setMain(v);
        } else {
            setMacrosByIdx(subgraphIdx, (prev) => ({
                ...prev,
                graph: applySetState(v, prev.graph),
            }));
        }
    }, [setMacrosByIdx, setMain, subgraphIdx]);

    const deleteMacrosByIndex = useDeleteByIndex(setMacros);
    const onDeleteMacros = useCallback(() => {
        deleteMacrosByIndex(subgraphIdx);
    }, [deleteMacrosByIndex, subgraphIdx]);

    const [selectedNodeId, setSelectedNodeId] = useState<string | undefined>(
        undefined,
    );
    const selectedNode = useMemo(
        () => currentGraph.nodes.find((n) => n.id === selectedNodeId),
        [currentGraph, selectedNodeId],
    );
    const [, setNodes] = useByKey(
        currentGraph,
        setCurrentGraph,
        "nodes",
    );
    const setNodeById = useCallback(
        (
            id: string,
            set: SetStateValue<ReverbNode>,
            updatePluginIO: boolean,
        ) => {
            setById(setNodes, id, set);
            if (!updatePluginIO) {
                return;
            }
            setPluginSettings((prev) => {
                const mainInput = prev.graph.main.nodes.find((n) => n.type === "mainInput")! as ReverbMainInputNode;
                const mainOutput = prev.graph.main.nodes.find((n) => n.type === "mainOutput")! as ReverbMainOutputNode;
                return {
                    ...prev,
                    ...reverbIONodesToPorts(
                        pluginId,
                        mainInput,
                        mainOutput,
                    ),
                };
            });
        },
        [pluginId, setPluginSettings, setNodes],
    );

    const macrosMap = useMemo(
        () => pluginSettings.graph.macros.toMapById(),
        [pluginSettings.graph.macros],
    );

    const editMacroNameUI = subgraphIdx !== -1
        ? <Padded>
            <HoriLine>
                <TextInput
                    size={"small"}
                    value={macros[subgraphIdx]!.title}
                    onChange={(v) => {
                        setByIndex(
                            setMacros,
                            subgraphIdx,
                            (m) => ({
                                ...m,
                                title: v,
                            }),
                        );
                    }}
                />
                <Button
                    onClick={onDeleteMacros}
                    iconLeft={Svg18CommonDelete}
                    title={"Delete macro"}
                />
            </HoriLine>
        </Padded>
        : undefined;

    const nodeSettingsUI =
        selectedNode === undefined
            ? undefined
            : <ReverbNodeSettings
                node={selectedNode}
                setNodeById={setNodeById}
                macros={macrosMap}
                fastAddMacro={fastAddMacro}
            />;

    const rightPanelContent = (editMacroNameUI || nodeSettingsUI)
        ? <>
            {editMacroNameUI}
            {nodeSettingsUI}
        </>
        : undefined;


    return <SidePanel
        mainContent={<ReverbNodesEditor
            pluginSettings={pluginSettings}
            graph={currentGraph}
            setGraph={setCurrentGraph}
            selectedNodeId={selectedNodeId}
            setSelectedNodeId={setSelectedNodeId}
            macrosMap={macrosMap}
        /> }
        leftPanelContent={<ReverbSubgraphSelector
            subgraphIdx={subgraphIdx}
            setSubgraphIdx={setSubgraphIdx}
            graph={graph}
            setGraph={setGraph}
            onDeleteMacros={deleteMacrosByIndex}
        />}
        rightPanelContent={rightPanelContent}
    />;
}

