import { capitalize } from "@audio-playground/lib-common/std/String";
import { SetState, useSetByKey } from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback, useMemo } from "react";

import { Button } from "../../../../../../components/Common/Button/Button";
import { Svg18CommonCopy } from "../../../../../../components/Common/GeneratedIcons/18/Common/Copy";
import { Svg18NodeEditorBoolOpNode } from "../../../../../../components/Common/GeneratedIcons/18/NodeEditor/BoolOpNode";
import { Svg18NodeEditorConstNode } from "../../../../../../components/Common/GeneratedIcons/18/NodeEditor/ConstNode";
import { Svg18NodeEditorDelayNode } from "../../../../../../components/Common/GeneratedIcons/18/NodeEditor/DelayNode";
import { Svg18NodeEditorExprNode } from "../../../../../../components/Common/GeneratedIcons/18/NodeEditor/ExprNode";
import {
    Svg18NodeEditorMacroUsageNode,
} from "../../../../../../components/Common/GeneratedIcons/18/NodeEditor/MacroUsageNode";
import { Svg18NodeEditorMathOpNode } from "../../../../../../components/Common/GeneratedIcons/18/NodeEditor/MathOpNode";
import { Svg18NodeEditorTimeNode } from "../../../../../../components/Common/GeneratedIcons/18/NodeEditor/TimeNode";
import { Svg32GatesAnd } from "../../../../../../components/Common/GeneratedIcons/32/Gates/And";
import { Svg32GatesNot } from "../../../../../../components/Common/GeneratedIcons/32/Gates/Not";
import { Svg32GatesOr } from "../../../../../../components/Common/GeneratedIcons/32/Gates/Or";
import { Svg32GatesXnor } from "../../../../../../components/Common/GeneratedIcons/32/Gates/Xnor";
import { Svg32GatesXor } from "../../../../../../components/Common/GeneratedIcons/32/Gates/Xor";
import { GridOverlay } from "../../../../../../components/Common/Gridy/GridOverlay/GridOverlay";
import { NodeDim, NodeEditor } from "../../../../../../components/Common/Gridy/NodeEditor/NodeEditor";
import { basicCloneNode } from "../../../../../../components/Common/Gridy/NodeEditor/Utils";
import { MACROS_STD } from "../../../macroStd/macroStd";
import {
    createConstNode,
    createDelayNode,
    createExprNode,
    createGateOpNode,
    createMacroUsageNode,
    createMathOpNode,
    createTimeNode,
    isReverbMacrosDefinitionNode, ReverbGateNode,
    ReverbGraph,
    ReverbMacro,
    ReverbNode,
    ReverbPort,
} from "../../../ReverbNodes";
import { ReverbSettings } from "../../../ReverbSettings";
import { getMacroTitle } from "../ReverbNodeSettings/MacroUsageNodeUI";
import { isNodeCopiable, isNodeDeleteable, isUneditableNode } from "../ReverbNodeSettings/ReverbNodeSettings";
import styles from "./ReverbNodesEditor.module.css";

export type NodeUIProps<VALUE extends ReverbNode> = {
    value: VALUE,
    setValue: SetState<VALUE>,
}

function dim(
    wl: number,
    wr: number,
    w: number = 4,
    h: number = 2,
): NodeDim {
    return {
        contentWidth: w,
        contentHeight: h,
        inputsWidth: wl,
        outputsWidth: wr,
    };
}

const DS = 3;

const NODES_DIMS: Record<ReverbNode["type"], NodeDim> = {
    mainInput: dim(0, 6, 0, 0),
    mainOutput: dim(6, 0, 0, 0),
    macroDefinitionInput: dim(0, 6, 0, 0),
    macroDefinitionOutput: dim(6, 0, 0, 0),
    macroUsage: dim(6, 6),
    const: dim(DS, DS),
    math: dim(DS, DS),
    gate: dim(DS, DS),
    delay: dim(DS, DS),
    time: dim(0, DS),
    expr: dim(DS, DS),
};

function getNodeDim(node: ReverbNode): NodeDim {
    return NODES_DIMS[node.type];
}

const getPortShape = (p: ReverbPort) => {
    switch (p.valueType) {
        case "boolean":
            return "square";
        case "number":
            return "circle";
    }
};

const getPortTitle = (
    p: ReverbPort,
    portIdToTitle: Map<string, string>,
) => {
    switch (p.type) {
        case "macroUsage.input":
            return portIdToTitle.get(p.macroPortId) ?? "Unknown";
        case "macroUsage.output":
            return portIdToTitle.get(p.macroPortId) ?? "Unknown";
        case "macroDefinitionOutput.input":
            return p.title;
        case "macroDefinitionInput.output":
            return p.title;
        case "mainInput.signal":
        case "mainInput.knob":
        case "mainInput.key":
        case "mainInput.checkbox":
        case "mainOutput.signal":
        case "mainOutput.ui":
            return p.title ?? "Unknown";
        case "const.disable":
            return "Disable";
        case "gate.input":
            return `Input ${p.inputIndex + 1}`;
        case "math.input":
            return `Input ${p.inputIndex + 1}`;
        case "math.disable":
            return "Disable";
        case "delay.input":
            return "Input";
        case "delay.time":
            return "Time, samples";
        case "delay.disable":
            return "Disable";
        case "const.value":
            return "Value";
        case "gate.output":
            return "Output";
        case "math.output":
            return "Output";
        case "delay.output":
            return "Output";
        case "time.value":
            return "Value";
        case "expr.result":
            return "Result";
        case "expr.variable":
            return p.variable;
    }
};

const getTitle = (node: ReverbNode) => {
    return capitalize(node.type);
};

type ReverbNodesEditorProps = {
    pluginSettings: ReverbSettings,
    graph: ReverbGraph,
    setGraph: SetState<ReverbGraph>,
    selectedNodeId: string | undefined,
    setSelectedNodeId: SetState<string | undefined>,
    macrosMap: Map<string, ReverbMacro>,
}

export function ReverbNodesEditor({
    pluginSettings,
    graph,
    setGraph,
    selectedNodeId,
    setSelectedNodeId,
    macrosMap,
}: ReverbNodesEditorProps) {

    const isPortLinkableTo = useCallback((
        f: ReverbPort[],
        t: ReverbPort[],
    ) => {

        for (let i = 0; i < f.length; i++) {
            const ff = f[i]!;
            const tt = t[i]!;
            if (ff.valueType !== tt.valueType) {
                return "Cannot join numeric and boolean ports";
            }
        }

        return undefined;
    }, []);

    const macros = pluginSettings.graph.macros;
    const portIdToTitle = useMemo(() => {
        return [...macros, ...MACROS_STD.values()]
            .map((m) => m.graph.nodes)
            .flat()
            .filter(isReverbMacrosDefinitionNode)
            .map((m) => [...m.outputs, ...m.inputs])
            .flat()
            .map((p) => [p.id, p.title])
            .toMap();
    }, [macros]);
    const portTitle = useCallback((port: ReverbPort) => {
        return getPortTitle(port, portIdToTitle);
    }, [portIdToTitle]);

    const copySubgraph = useCallback(() => {
        navigator.clipboard.writeText(JSON.stringify(graph));
    }, [graph]);

    const pasteSubgraph = useCallback(() => {
        navigator.clipboard.readText().then(
            (t) => setGraph(JSON.parse(t)),
        );
    }, [setGraph]);

    return <>
        <NodeEditor
            selectedNodeId={selectedNodeId}
            setSelectedNodeId={setSelectedNodeId}
            getTitle={getTitle}
            isNodeDeleteable={isNodeDeleteable}
            isNodeCopiable={isNodeCopiable}
            isPortLinkableTo={isPortLinkableTo}
            getNodeDim={getNodeDim}
            getContent={(node: ReverbNode) => getNodePreview(node, macrosMap)}
            getPortShape={getPortShape}
            getPortTitle={portTitle}
            graph={graph}
            setGraph={setGraph}
            createClone={basicCloneNode}
        />
        <GridOverlay side={"right"} >
            <AddNodeButtons setGraph={setGraph}/>
            <Button
                title={"Copy subgraph"}
                onClick={() => copySubgraph()}
                iconLeft={Svg18CommonCopy}
            />
            <Button
                title={"Paste subgraph"}
                onClick={() => pasteSubgraph()}
                iconLeft={Svg18CommonCopy}
            />
        </GridOverlay>
    </>;
}

function gateTo32Icon(op: ReverbGateNode["op"]) {
    switch (op) {
        case "and": return Svg32GatesAnd;
        // case "nand": return Svg32GatesNand;
        case "or": return Svg32GatesOr;
        // case "nor": return Svg32GatesNor;
        case "xor": return Svg32GatesXor;
        case "xnor": return Svg32GatesXnor;
        case "not": return Svg32GatesNot;
    }
}

function getNodePreview(
    node: ReverbNode,
    macros: Map<string, ReverbMacro>,
): React.ReactNode | undefined {
    if (isUneditableNode(node)) {
        return undefined;
    }
    return <div className={styles.nodePreview}>
        {(() => {
            switch (node.type) {
                case "const": return `${node.value}`;
                case "gate": return <>
                    {gateTo32Icon(node.op)({})}
                    {node.op}
                </>;
                case "math": return node.op;
                case "delay": return `Max: ${node.maxLength}`;
                case "time": return node.op;
                case "macroUsage": return getMacroTitle(node.macroId, macros);
                case "expr": return node.expr;
            }
        })()}
    </div>;

}

type AddNodeButtonsProps = {
    setGraph: SetState<ReverbGraph>,
}

function AddNodeButtons({ setGraph }: AddNodeButtonsProps) {
    const setNodes = useSetByKey(
        setGraph,
        "nodes",
    );
    const addNode = (node: ReverbNode) => {
        // @ts-ignore
        setNodes((prev) => [
            ...prev,
            node,
        ]);
    };
    return <>
        <Button
            title={"Add const"}
            onClick={() => addNode(createConstNode())}
            iconLeft={Svg18NodeEditorConstNode}
        />
        <Button
            title={"Add math op"}
            onClick={() => addNode(createMathOpNode("plus"))}
            iconLeft={Svg18NodeEditorMathOpNode}
        />
        <Button
            title={"Add gate op"}
            onClick={() => addNode(createGateOpNode("and"))}
            iconLeft={Svg18NodeEditorBoolOpNode}
        />
        <Button
            title={"Add delay"}
            onClick={() => addNode(createDelayNode())}
            iconLeft={Svg18NodeEditorDelayNode}
        />
        <Button
            title={"Add time"}
            onClick={() => addNode(createTimeNode())}
            iconLeft={Svg18NodeEditorTimeNode}
        />
        <Button
            title={"Add expr"}
            onClick={() => addNode(createExprNode())}
            iconLeft={Svg18NodeEditorExprNode}
        />
        <Button
            title={"Add macros usage"}
            onClick={() => addNode(createMacroUsageNode())}
            iconLeft={Svg18NodeEditorMacroUsageNode}
        />
    </>;
}
