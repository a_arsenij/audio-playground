import { useStringStateInUrl } from "@audio-playground/lib-frontend/hooks/useRouting";
import React, { useCallback } from "react";

import { HeaderedPlugin } from "../../../../components/App/HeaderedPlugin/HeaderedPlugin";
import { DeprecatedSwitch } from "../../../../components/Common/Switch/Switch";
import { PluginUIProps } from "../../../../types/Plugin";
import { REVERB_PRESETS } from "../presets/ReverbPresets";
import { createMacros } from "../ReverbNodes";
import { ReverbSettings } from "../ReverbSettings";
import { ReverbForm } from "./ReverbForm/ReverbForm";
import { ReverbGraphEditor } from "./ReverbGraphEditor/ReverbGraphEditor";
import { ReverbUIEditor } from "./ReverbUIEditor/ReverbUIEditor";
import { REVERB_UI_MODES, ReverbUIMode } from "./ReverbUIModes";


export const ReverbUI = function({
    pluginId,
    pluginSettings,
    setPluginSettings,
    isAutomated,
    setIsAutomated,
}: PluginUIProps<ReverbSettings>) {

    const [
        reverbUIMode,
        setReverbUIMode,
    ] = useStringStateInUrl<ReverbUIMode>(
        "reverb-plugin-ui-mode",
        "ui",
    );

    const fastAddMacro = useCallback(() => {
        const macro = createMacros("New Macro");
        setPluginSettings((prev) => ({
            ...prev,
            graph: {
                main: prev.graph.main,
                macros: [
                    ...prev.graph.macros,
                    macro,
                ],
            },
        }));
        return macro.id;
    }, [setPluginSettings]);

    return <HeaderedPlugin
        title={"Reverb"}
        pluginSettings={pluginSettings}
        setPluginSettings={setPluginSettings}
        buildInPresets={REVERB_PRESETS}
        customUI={<>
            <DeprecatedSwitch
                options={REVERB_UI_MODES}
                value={reverbUIMode}
                setValue={setReverbUIMode}
                renderOption={(v) => {
                    switch (v) {
                        case "ui": return "UI";
                        case "nodes": return "Nodes";
                        case "uieditor": return "UI Editor";
                    }
                }}
            />
        </>}
    >
        { reverbUIMode === "ui" && <ReverbForm
            pluginSettings={pluginSettings}
            setPluginSettings={setPluginSettings}
            isAutomated={isAutomated}
            setIsAutomated={setIsAutomated}
        /> }
        { reverbUIMode === "nodes" && <ReverbGraphEditor
            pluginId={pluginId}
            pluginSettings={pluginSettings}
            setPluginSettings={setPluginSettings}
            fastAddMacro={fastAddMacro}
        /> }
        { reverbUIMode === "uieditor" && <ReverbUIEditor
            pluginSettings={pluginSettings}
            setPluginSettings={setPluginSettings}
        /> }
    </HeaderedPlugin>;
};
