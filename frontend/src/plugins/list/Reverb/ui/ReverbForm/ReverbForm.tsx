import { SetState, useByKey } from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback } from "react";

import { AutomatedKnobCheckbox } from "../../../../../components/Common/AutomatedCheckbox/AutomatedCheckbox";
import { Checkbox } from "../../../../../components/Common/Checkbox/Checkbox";
import { Knob } from "../../../../../components/Common/Knob/Knob";
import { isBooleanPort, isKnobPort, isReverbMainInputNode } from "../../ReverbNodes";
import { ReverbSettings } from "../../ReverbSettings";
import {
    isRadiobuttonElement,
    ReverbUIKnobElement,
    ReverbUILabelElement,
    ReverbUIRadiobuttonElement,
} from "../ReverbUIEditor/ReverbUIEditor";
import styles from "./ReverbForm.module.css";

type ReverbFormProps = {
    pluginSettings: ReverbSettings,
    setPluginSettings: SetState<ReverbSettings>,
    isAutomated: (name: string) => boolean,
    setIsAutomated: (name: string, isAutomated: boolean) => void,
}

export function ReverbForm({
    pluginSettings,
    setPluginSettings,
    isAutomated,
    setIsAutomated,
}: ReverbFormProps) {

    const [knobsValues, setKnobsValues] = useByKey(
        pluginSettings,
        setPluginSettings,
        "knobsValues",
    );

    const [checkboxesValues, setCheckboxesValues] = useByKey(
        pluginSettings,
        setPluginSettings,
        "checkboxesValues",
    );

    const elements = pluginSettings.elements;
    let minX = Number.POSITIVE_INFINITY;
    let maxX = Number.NEGATIVE_INFINITY;
    let minY = Number.POSITIVE_INFINITY;
    let maxY = Number.NEGATIVE_INFINITY;
    elements.forEach((e) => {
        minX = Math.min(minX, e.x);
        maxX = Math.max(maxX, e.x);
        minY = Math.min(minY, e.y);
        maxY = Math.max(maxY, e.y);
    });


    const input = pluginSettings.graph.main.nodes.find(isReverbMainInputNode)!;

    const checkboxes = input.outputs
        .filter(isBooleanPort)
        .map((p) => ({
            ...p,
            value: pluginSettings.checkboxesValues[p.id] ?? false,
        }))
        .toMapById();

    const setCheckboxValue = useCallback((
        checkboxId: string,
        value: boolean,
    ) => {
        setCheckboxesValues((prev) => ({
            ...prev,
            [checkboxId]: value,
        }));
    }, [setCheckboxesValues]);

    const toggleRadioValue = useCallback((radioId: string) => {
        const elem = elements.find((e) => e.type === "radiobutton" && e.portId === radioId) as ReverbUIRadiobuttonElement | undefined;
        if (!elem) {
            return;
        }
        const groupId = elem.groupId;
        const radioElements = elements.filter(isRadiobuttonElement);
        const group = radioElements
            .filter((e) => e.groupId === groupId);
        setCheckboxesValues((prev) => {
            const res = Object.fromEntries([
                ...Object.entries(prev),
                ...group.map((r) =>
                    [r.portId, r.portId === elem.portId],
                ),
            ]);
            console.log("res", res);
            return res;
        });
    }, [elements, setCheckboxesValues]);

    const knobs = input.outputs
        .filter(isKnobPort)
        .map((p) => ({
            ...p,
            value: pluginSettings.knobsValues[p.id] ?? false,
        }))
        .toMapById();

    const setKnobValue = useCallback((
        knobId: string,
        value: number,
    ) => {
        setKnobsValues((prev) => ({
            ...prev,
            [knobId]: value,
        }));
    }, [setKnobsValues]);

    return <div className={styles.ui}>
        <div
            className={styles.grid}
            style={{
                gridTemplateColumns: `repeat(${maxX - minX + 4}, var(--cell-size))`,
                gridTemplateRows: `repeat(${maxY - minY + 4}, var(--cell-size))`,
            }}
        >
            {elements.map((e) => {
                const x = e.x - minX;
                const y = e.y - minY;
                return <div
                    key={e.id}
                    className={styles.element}
                    style={{
                        gridColumnStart: x + 1,
                        gridColumnEnd: x + 5,
                        gridRowStart: y + 1,
                        gridRowEnd: y + 5,
                    }}
                >
                    {e.type === "knob" && (() => {
                        const knob = knobs.get(e.portId);
                        const knobId = knob?.id;
                        const knobValue = knobsValues[knobId ?? ""];
                        return <KnobElement
                            element={e}
                            value={knobValue ?? knob?.min ?? 0}
                            min={knob?.min ?? 0}
                            max={knob?.max ?? 0}
                            defaultValue={e.defaultValue}
                            setValue={(v) => knobId && setKnobValue(knobId, v)}
                            isAutomated={isAutomated}
                            setIsAutomated={setIsAutomated}
                            knobId={knobId ?? ""}
                        />;
                    })()}
                    {(e.type === "checkbox" || e.type === "radiobutton") && (() => {
                        const checkbox = checkboxes.get(e.portId);
                        const checkboxId = checkbox?.id;
                        const checkboxValue = checkboxesValues[checkboxId ?? ""] ?? e.defaultState;

                        return <>
                            {e.type === "checkbox" && <CheckboxElement
                                value={checkboxValue}
                                setValue={(v) => checkboxId && setCheckboxValue(checkboxId, v)}
                            />}
                            {e.type === "radiobutton" && <RadioButtonElement
                                value={checkboxValue}
                                onToggle={() => checkboxId && toggleRadioValue(checkboxId)}
                            />}
                        </>;
                    })()}
                    {e.type === "label" && <LabelElement element={e}/>}
                </div>;
            })}
        </div>
    </div>;
}

type KnobProps = {
    element: ReverbUIKnobElement,
    value: number,
    min: number,
    max: number,
    defaultValue: number,
    setValue: (v: number) => void,
    isAutomated: (name: string) => boolean,
    setIsAutomated: (name: string, isAutomated: boolean) => void,
    knobId: string,
}

function KnobElement({
    element,
    value,
    min,
    max,
    defaultValue,
    setValue,
    isAutomated,
    setIsAutomated,
    knobId,
}: KnobProps) {
    const set = useCallback((v: number) => {
        setValue(isNaN(v) ? min : v);
    }, [min, setValue]);
    return <div className={styles.wrapped}>
        <Knob
            minValue={min}
            maxValue={max}
            func={element.func}
            step={"step" in element ? element.step : undefined}
            base={"base" in element ? element.base : 0}
            value={value}
            setValue={set}
            showNumberValue={true}
            defaultValue={defaultValue}
            extraContextMenuChildren={<>
                <AutomatedKnobCheckbox
                    isAutomated={isAutomated}
                    setIsAutomated={setIsAutomated}
                    knobName={knobId}
                />
            </>}
        />
    </div>;
}

type CheckboxProps = {
    value: boolean,
    setValue: (v: boolean) => void,
}

function CheckboxElement({
    value, setValue,
}: CheckboxProps) {
    return <div className={styles.wrapped}>
        <Checkbox
            value={value}
            setValue={setValue}
        />
    </div>;
}

type RadioButtonProps = {
    value: boolean,
    onToggle: () => void,
}

function RadioButtonElement({
    value, onToggle,
}: RadioButtonProps) {
    return <div className={styles.wrapped}>
        <Checkbox
            value={value}
            setValue={onToggle}
        />
    </div>;
}


type LabelProps = {
    element: ReverbUILabelElement,
}

function LabelElement({
    element,
}: LabelProps) {
    return element.text;
}

