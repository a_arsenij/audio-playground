import { ReverbGraph, WholeReverbGraph } from "./ReverbNodes";

function deepEqual(
    a: any,
    b: any,
): boolean {
    if (a === b) {
        return true;
    }

    if (a === null || typeof a !== "object" || b === null || typeof b !== "object") {
        return a === b;
    }

    const keysA = Object.keys(a);
    const keysB = Object.keys(b);

    if (keysA.length !== keysB.length) {
        return false;
    }

    for (const key of keysA) {
        if (!keysB.includes(key) || !deepEqual(a[key], b[key])) {
            return false;
        }
    }

    return true;
}

function reverbSubGraphsAreEqual(
    graph1: ReverbGraph,
    graph2: ReverbGraph,
): boolean {
    if (graph1 === graph2) {
        return true;
    }
    if (graph1.nodes.length !== graph2.nodes.length) {
        return false;
    }

    for (let i = 0; i < graph1.nodes.length; i += 1) {
        const node1: Record<string, unknown> = { ...graph1.nodes[i]! };
        const node2: Record<string, unknown> = { ...graph2.nodes[i]! };

        delete node1.x;
        delete node1.y;
        delete node2.x;
        delete node2.y;

        if (!deepEqual(
            node1,
            node2,
        )) {
            return false;
        }
    }

    return deepEqual(graph1.connections, graph2.connections);
}

export function reverbGraphsAreEqual(
    graph1: WholeReverbGraph,
    graph2: WholeReverbGraph,
): boolean {
    if (graph1 === graph2) {
        return true;
    }
    if (!reverbSubGraphsAreEqual(graph1.main, graph2.main)) {
        return false;
    }
    if (graph1.macros.length !== graph2.macros.length) {
        return false;
    }
    for (let i = 0; i < graph1.macros.length; i++) {
        const a = graph1.macros[i]!;
        const b = graph2.macros[i]!;
        if (a.id !== b.id) {
            return false;
        }
        if (!reverbSubGraphsAreEqual(a.graph, b.graph)) {
            return false;
        }
        // we don't care much if macros name changed
    }
    return true;
}

export function reverbCheckboxesAreEqual(
    a: Record<string, boolean>,
    b: Record<string, boolean>,
): boolean {
    if (a === b) {
        return true;
    }
    const k1 = Object.entries(a);
    const k2 = Object.entries(b);
    if (k1.length !== k2.length) {
        return false;
    }
    return k1.every((k) => k[1] === b[k[0]]);
}

