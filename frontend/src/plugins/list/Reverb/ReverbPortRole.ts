


export const REVERB_INPUT_PORT_ROLES = [
    "signal",
    "knob",
    "key",
] as const;

export type ReverbInputPortRole = (typeof REVERB_INPUT_PORT_ROLES)[number];

export const REVERB_OUTPUT_PORT_ROLES = [
    "signal",
    "visual",
    "key",
] as const;

export type ReverbOutputPortRole = (typeof REVERB_OUTPUT_PORT_ROLES)[number];

export type ReverbPortNumberInputSignalRole = {
    role: "signal",
}

export type ReverbPortNumberInputKnobRole = {
    role: "knob",
    minimum: number,
    maximum: number,
    default: number,
}

export type ReverbPortNumberInputKeyRole = {
    role: "key",
}

export type ReverbPortNumberInputRole =
    | ReverbPortNumberInputSignalRole
    | ReverbPortNumberInputKnobRole
    | ReverbPortNumberInputKeyRole
;

export type ReverbPortBooleanInputRole = {
    role: ReverbInputPortRole,
    default: boolean,
};

export type ReverbPortNumberOutputRole = {
    role: ReverbOutputPortRole,
    default: number,
};

export type ReverbPortRoles = {
    numberInputs: {[k in string]: ReverbPortNumberInputRole},
    booleanInputs: {[k in string]: ReverbPortBooleanInputRole},
    numberOutputs: {[k in string]: ReverbPortNumberOutputRole},
}
