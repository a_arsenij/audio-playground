import { deepCloneJson } from "@audio-playground/lib-common/std/Object";
import { noteToFrequency } from "@audio-playground/lib-sound/NoteFrequency";

import { MaybeAutomated, RenderBufferArg } from "../../../../types/Plugin";
import { getKeyEventKey, getKeyEventTimeSample, getKeyEventType } from "../../../../utils/KeyEvent";
import { populateGraphWithStdMacros } from "../macroStd/macroStd";
import {
    isKnobPort,
    isReverbMainInputNode,
    isReverbMainOutputNode,
} from "../ReverbNodes";
import { ReverbSettings } from "../ReverbSettings";
import { createReverbStateCache, indexateInputs, indexateOutputs, ReverbState } from "../ReverbState";
import { reverbCheckboxesAreEqual, reverbGraphsAreEqual } from "../utils";

export function reverbRender(
    arg: RenderBufferArg<ReverbSettings, ReverbState>,
) {

    if (
        !reverbGraphsAreEqual(
            arg.pluginSettings.graph,
            arg.pluginState.compiledFor.graph,
        ) ||
        !reverbCheckboxesAreEqual(
            arg.pluginSettings.checkboxesValues,
            arg.pluginState.compiledFor.checkboxValues,
        )
    ) {
        const inputs = indexateInputs(
            arg.pluginSettings.graph.main.nodes
                .find(isReverbMainInputNode)
                ?.outputs ?? [],
        );
        arg.pluginState.compiledFor = {
            graph: arg.pluginSettings.graph,
            checkboxValues: arg.pluginSettings.checkboxesValues,
            inputs,
            outputs: indexateOutputs(
                arg.pluginSettings.graph.main.nodes
                    .find(isReverbMainOutputNode)
                    ?.inputs ?? [],
            ),
        };

        const graphCopy = deepCloneJson(arg.pluginSettings.graph);
        populateGraphWithStdMacros(graphCopy);
        console.log("Recompile graph");
        arg.pluginState.compiler.compile(graphCopy, arg.pluginSettings.checkboxesValues, arg.sampleRate, arg.bufferLength);

        arg.pluginState.cache = createReverbStateCache(
            inputs.filter((p) => p.type === "mainInput.key").ids(),
        );
    }

    const knobs = arg.pluginState.compiledFor.inputs
        .filter(isKnobPort)
        .map((n) =>
            [n.id, arg.getMaybeAutomated(
                n.id,
                n.min,
                n.max,
                arg.pluginSettings.knobsValues[n.id] ?? n.min,
            )] as const,
        )
        .toMap();

    runReverb(
        arg,
        knobs,
    );

}


function runReverb(
    arg: RenderBufferArg<ReverbSettings, ReverbState>,
    knobs: Map<string, MaybeAutomated>,
) {

    const input = arg.input;
    const output = arg.output;
    const inputs = arg.pluginState.compiledFor.inputs;
    const outputs = arg.pluginState.compiledFor.outputs;
    const compilationResult = arg.pluginState.compiler.compilationResult;
    const timeSample = arg.timeSample;
    const bufferLength = arg.bufferLength;

    const { run, memory } = compilationResult;
    const view = new Float64Array(memory);

    let offset = 0;

    const eventsCopy = arg.keyEvents.toReversed();

    const cache = arg.pluginState.cache;
    for (let sampleIdx = 0; sampleIdx < bufferLength; sampleIdx++) {
        const timeBars = arg.getCurrentTimeBars(sampleIdx);
        const timeSamples = timeSample + sampleIdx;

        while (eventsCopy.length > 0) {
            const next = eventsCopy.at(-1)!;
            const nextTime = getKeyEventTimeSample(next);
            if (nextTime !== timeSamples) {
                break;
            }
            eventsCopy.pop();
            const type = getKeyEventType(next);
            const key = getKeyEventKey(next);
            if (type === "noteOn") {
                const freePortIndex = cache.portsStates.findIndex((p) => p.key === undefined);
                if (freePortIndex < 0) {
                    continue;
                }
                cache.portsStates[freePortIndex]!.key = key;
            } else {
                const portIndex = cache.portsStates.findIndex((p) => p.key === key);
                if (portIndex < 0) {
                    continue;
                }
                cache.portsStates[portIndex]!.key = undefined;
            }
        }

        inputs.map((port) => {
            switch (port.type) {
                case "mainInput.signal": {
                    return input[port.channelIndex]![sampleIdx]!;
                }
                case "mainInput.knob": {
                    return knobs.get(port.id)?.(timeBars) ?? 0;
                }
                case "mainInput.key": {
                    const portId = port.id;
                    const portIndex = cache.portsStates.findIndex((p) => p.portId === portId);
                    const note = cache.portsStates[portIndex]!.key;
                    if (!note) {
                        return -1;
                    }
                    const freq = noteToFrequency(note);
                    return freq;
                }
            }
        }).forEach((pv) => {
            view[offset++] = pv;
        });

    }

    run(timeSample);

    offset = bufferLength * inputs.length;
    for (let sampleIdx = 0; sampleIdx < bufferLength; sampleIdx++) {

        for (const port of outputs) {
            switch (port.type) {
                case "mainOutput.ui": {
                    break;
                }
                case "mainOutput.signal": {
                    output[port.channelIndex]![sampleIdx] = view[offset] ?? 0;
                    break;
                }
            }
            offset++;
        }
    }
}
