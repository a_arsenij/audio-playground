
import { CreateStateArg } from "../../../types/Plugin";
import { createReverbCompilerWASM } from "./compile/ReverbCompile";
import {
    createInputNode,
    createOutputNode,
    isInputSignalPort,
    isNumberPort,
    isOutputSignalPort,
    ReverbMainInputKeyNodePort,
    ReverbMainInputKnobNodePort,
    ReverbMainInputNodePort,
    ReverbMainInputSignalNodePort,
    ReverbMainOutputNodePort,
    ReverbMainOutputSignalNodePort,
    ReverbMainOutputUINodePort,
    WholeReverbGraph,
} from "./ReverbNodes";

export type ReverbCompilationResultRun = (timeSamples: number) => void

export type ReverbCompilationResult = {
    run: ReverbCompilationResultRun,
    memory: ArrayBuffer,
    inputsCount: number,
    outputsCount: number,
}
export type ReverbCompilationResultJS = {
    run: (arr: number[], timeSamples: number) => number[],
    inputsCount: number,
    outputsCount: number,
}
export type ReverbCompiler = {
    compile: (
        graph: WholeReverbGraph,
        checkboxesValues: Record<string, boolean>,
        sampleRate: number,
        bufferLength: number,
    ) => void,
    compilationResult: ReverbCompilationResult,
}

export type HasChannelIndex = {
    channelIndex: number,
}

export type ReverbState = {
    prevKnobsValues: Record<string, number>,
    compiledFor: {
        graph: WholeReverbGraph,
        checkboxValues: Record<string, boolean>,
        inputs: (
            | ReverbMainInputSignalNodePort & HasChannelIndex
            | ReverbMainInputKnobNodePort
            | ReverbMainInputKeyNodePort
        )[],
        outputs: (
            | ReverbMainOutputSignalNodePort & HasChannelIndex
            | ReverbMainOutputUINodePort
        )[],
    },
    compiler: ReverbCompiler,
    cache: {
        portsStates: {
            portId: string,
            key: number | undefined,
        }[],
    },
}

type WithChannelIndices<
    ALL,
    SPECIFIC extends ALL,
> = (Exclude<ALL, SPECIFIC> | (SPECIFIC & HasChannelIndex))[];

export function addChannelsIndex<
    ALL,
    SPECIFIC extends ALL,
>(
    items: ALL[],
    isSpecific: (item: ALL) => item is SPECIFIC,
): WithChannelIndices<ALL, SPECIFIC> {
    let idx = 0;
    const res: WithChannelIndices<ALL, SPECIFIC> = [];
    for (const item of items) {
        if (isSpecific(item)) {
            res.push({
                ...item,
                channelIndex: idx++,
            });
        } else {
            res.push(item as Exclude<ALL, SPECIFIC>);
        }
    }
    return res;
}

export function indexateInputs(
    items: ReverbMainInputNodePort[],
): ReverbState["compiledFor"]["inputs"] {
    return addChannelsIndex(
        items.filter(isNumberPort),
        isInputSignalPort,
    );
}

export function indexateOutputs(
    items: ReverbMainOutputNodePort[],
): ReverbState["compiledFor"]["outputs"] {
    return addChannelsIndex(
        items.filter(isNumberPort),
        isOutputSignalPort,
    );
}

export function createReverbStateCache(
    freePortsIds: string[],
) {
    return {
        portsStates: freePortsIds.map((id) => ({
            portId: id,
            key: undefined,
        })),
    };
}
export function createSimpleReverbState({ sampleRate, bufferLength }: CreateStateArg): ReverbState {
    const input = createInputNode(0, 0);
    const output = createOutputNode(5, 0);
    const graph: WholeReverbGraph = {
        main: {
            nodes: [
                input,
                output,
            ],
            connections: [],
        },
        macros: [],
    };
    const compiler = createReverbCompilerWASM();
    compiler.compile(graph, {}, sampleRate, bufferLength);
    const inputs = indexateInputs(input.outputs);
    return {
        prevKnobsValues: {},
        compiledFor: {
            graph,
            checkboxValues: {},
            inputs,
            outputs: indexateOutputs(output.inputs),
        },
        compiler,
        cache: createReverbStateCache(
            inputs.filter((p) => p.type === "mainInput.key").ids(),
        ),
    };
}
