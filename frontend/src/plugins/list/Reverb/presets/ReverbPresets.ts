import { createPreset } from "../../../utils/fetch-preset";
import { ReverbSettings } from "../ReverbSettings";
import * as _3synth from "./list/3-synth.owdpreset";
import * as stereoMixer from "./list/stereo-mixer.owdpreset";

export const REVERB_PRESETS: {
    name: string,
    get: () => Promise<ReverbSettings>,
}[] = [
    createPreset("3-synth", _3synth),
    createPreset("Stereo mixer", stereoMixer),
];
