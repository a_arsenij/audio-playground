import { deepCloneJson } from "@audio-playground/lib-common/std/Object";

import { WholeReverbGraph } from "../ReverbNodes";
import { ReverbCompilationResult, ReverbCompilationResultJS, ReverbCompiler } from "../ReverbState";
import { compileToJsFunction } from "./backend/ReverbCompileToJs";
import { compileToWASM } from "./backend/ReverbcompileToWASM";
import { compileFrontend } from "./frontend";
import { compileMiddleend } from "./middleend";

const DEFAULT_COMPILATION_RESULT: ReverbCompilationResult = {
    run: () => {},
    memory: new ArrayBuffer(0),
    inputsCount: 0,
    outputsCount: 0,
};

export function createReverbCompilerWASM(): ReverbCompiler {
    const newCompiler = {
        compilationResult: DEFAULT_COMPILATION_RESULT,
        compile: (
            graph: WholeReverbGraph,
            checkboxesValues: Record<string, boolean>,
            sampleRate: number,
            bufferLength: number,
        ) => {
            try {
                compileReverbGraphToWASM(graph, checkboxesValues, sampleRate, bufferLength)
                    .then((res) => {
                        newCompiler.compilationResult = res;
                    }).catch((error) => {
                        console.error(error);
                    });
            } catch (e) {
                console.error(e);
            }
        },
    };
    return newCompiler;
}


export function compileReverbGraphToWASM(
    graph: WholeReverbGraph,
    checkboxesValues: Record<string, boolean>,
    sampleRate: number,
    bufferLength: number,
): Promise<ReverbCompilationResult> {
    const graphCopy = deepCloneJson(graph);
    const fe = compileFrontend(graphCopy, checkboxesValues, sampleRate);
    const me = compileMiddleend(fe);
    return compileToWASM(me, bufferLength);
}

export function compileReverbGraphToJs(
    graph: WholeReverbGraph,
    checkboxesValues: Record<string, boolean>,
    sampleRate: number,
    bufferLength: number,
): ReverbCompilationResultJS {
    const graphCopy = deepCloneJson(graph);
    const fe = compileFrontend(graphCopy, checkboxesValues, sampleRate);
    const me = compileMiddleend(fe);
    return compileToJsFunction(me, bufferLength);
}

