import { generateId } from "@audio-playground/lib-common/std/Id";

import { NodeEditorNode } from "../../../../../components/Common/Gridy/NodeEditor/NodeEditor";
import {
    BooleanPort,
    ReverbMainInputKeyNodePort,
    ReverbMainInputKnobNodePort,
    ReverbMainInputNode,
    ReverbMainInputSignalNodePort,
    ReverbNode,
    WholeReverbGraph,
} from "../../ReverbNodes";


export type SaturatedCheckboxNode = NodeEditorNode<
    BooleanPort[],
    BooleanPort[]
> & {
    type: "checkbox",
    value: boolean,
}

type SaturatedMainInputNodePort = (
    ReverbMainInputSignalNodePort
    | ReverbMainInputKnobNodePort
    | ReverbMainInputKeyNodePort) & {
    min: number,
    max: number,
}

export type SaturatedMainInputNode = NodeEditorNode<
    [],
    SaturatedMainInputNodePort[]
> & { type: "mainInput" }


type SaturatedInputGraphNode =
    | Exclude<ReverbNode, ReverbMainInputNode>
    | SaturatedCheckboxNode
    | SaturatedMainInputNode
;


function isMainInputNode(node: ReverbNode): node is ReverbMainInputNode {
    return "type" in node && node.type === "mainInput";
}

const MIN_MAX_FOR_PORTS = {
    "mainInput.signal": {
        min: -1,
        max: 1,
    },
    "mainInput.key": {
        min: -1,
        max: 128,
    },
} as const;

export function fillInputNodeMinMaxAddCheckboxNodes(
    checkboxesValues: Record<string, boolean | undefined>,
    graph: WholeReverbGraph,
): WholeReverbGraph<SaturatedInputGraphNode> {
    const mainInputNode = graph.main.nodes.find(isMainInputNode);
    if (!mainInputNode) {
        return graph as WholeReverbGraph<SaturatedInputGraphNode>;
    }

    const newPorts = mainInputNode.outputs.map((p) => {
        const t = p.type;
        switch (t) {
            case "mainInput.signal":
                return {
                    ...p,
                    ...MIN_MAX_FOR_PORTS[t],
                };
            case "mainInput.key":
                return {
                    ...p,
                    ...MIN_MAX_FOR_PORTS[t],
                };
            case "mainInput.knob":
                return p;
            case "mainInput.checkbox":
                return undefined;
        }
    }).filter((a) => !!a) as SaturatedMainInputNodePort[];


    const newCheckboxNodes = Object.entries(checkboxesValues).map(([key, value]) => {
        return createCheckboxNode(!!value, key);
    });

    const nodes = graph.main.nodes.map((n) => {
        if (isMainInputNode(n)) {
            return {
                ...n,
                outputs: newPorts,
            };
        }
        return n;
    });

    return {
        ...graph,
        main: {
            ...graph.main,
            nodes: [...nodes, ...newCheckboxNodes],
        },
    } as WholeReverbGraph<SaturatedInputGraphNode>;
}


function createCheckboxNode(val: boolean, outPortId: string): SaturatedCheckboxNode {
    return {
        type: "checkbox",
        id: `boolConst${generateId()}`,
        value: val,
        outputs: [
            {
                id: outPortId,
                valueType: "boolean",
            },
        ],
        inputs: [],

        direction: "ltr",
        x: 0,
        y: 0,
    };
}
