import {
    createConstNode,
    ReverbConstNode,
    ReverbTimeNode,
} from "../../ReverbNodes";
import { NEN, SimpleReplacement } from "./SimpleReplacements";

function match(obj: NEN): obj is ReverbTimeNode {
    return "type" in obj &&
        obj.type === "time"
    ;
}

export function replaceSampleRateNodes(sampleRate: number): SimpleReplacement<
    ReverbTimeNode,
    ReverbConstNode
> {
    return {
        match,
        replace: (arg) => {
            const newNode = createConstNode(sampleRate);
            arg.addNode(newNode);
            arg.outputConnections.forEach((c) => {
                c.portIdFrom = newNode.outputs[0].id;
            });
        },
    };
}
