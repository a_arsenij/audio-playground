import { WholeReverbGraph } from "../../ReverbNodes";


export function fillMissingCheckboxValues(
    checkboxesValues: Record<string, boolean>,
    graph: WholeReverbGraph,
) {
    const boxes = graph.main.nodes.find((n) => n.type === "mainInput")!.outputs.filter((p) => p.type === "mainInput.checkbox").map((p) => p.id);
    const copy = { ...checkboxesValues };
    const set = Object.keys(checkboxesValues).toSet();
    boxes.forEach((b) => {
        if (!set.has(b)) {
            copy[b] = false;
        }
    });
    return copy;
}
