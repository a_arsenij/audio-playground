

import {
    ConstExpr, Expr,
    getMathOpOperandsNumber,
    Op1Expr,
    Op2Expr,
    parseExpr, VarExpr,
} from "@audio-playground/lib-common/math/Expr";

import { createConnection } from "../../../../../components/Common/Gridy/NodeEditor/Utils";
import {
    createConstNode, createMathOpNode, MathInputPort, numberPort, ReverbConstNode,
    ReverbExprNode, ReverbMathNode,
} from "../../ReverbNodes";
import { NEN, SimpleReplacement } from "./SimpleReplacements";

function match(obj: NEN): obj is ReverbExprNode {
    return "type" in obj &&
        obj.type === "expr"
    ;
}

export function replaceExprNodes(): SimpleReplacement<
    ReverbExprNode,
    ReverbMathNode | ReverbConstNode
    > {
    return {
        match,
        replace: (arg) => {

            const expr = parseExpr(arg.node.expr);
            if (expr.type === "error") {
                arg.removeConnections(...arg.inputConnections);
                arg.removeConnections(...arg.outputConnections);
                const node = createConstNode(0);
                arg.addNode(node);
                arg.outputConnections.forEach((c) => arg.addConnections(
                    createConnection(
                        node.outputs[0].id,
                        c.portIdTo,
                    ),
                ));
                return;
            }
            function goConstValue(value: number): string {
                const newNode = createConstNode(value);
                arg.addNode(newNode);
                return newNode.outputs[0].id;
            }
            function goConst(node: ConstExpr): string {
                return goConstValue(node.value);
            }
            function goOp(node: Op1Expr | Op2Expr): string {
                const newNode = createMathOpNode(node.op);
                for (let i = 0; i < getMathOpOperandsNumber(node.op); i++) {
                    arg.addConnections(createConnection(
                        goDeep(node.args[i]!),
                        newNode.inputs[i]!.id,
                    ));
                }
                arg.addNode(newNode);
                return newNode.outputs[0].id;
            }
            function goVar(node: VarExpr): string {
                const port = arg.node.inputs
                    .find((p) => p.variable === node.name);
                if (port === undefined) {
                    return goConstValue(0);
                }
                const connections = arg.inputConnections
                    .filter((c) => c.portIdTo === port.id);
                if (connections.length === 0) {
                    return goConstValue(0);
                }
                if (connections.length === 1) {
                    const conn = connections[0]!;
                    arg.removeConnections(conn);
                    return conn.portIdFrom;
                }
                const plusNode = createMathOpNode(
                    "plus",
                );
                plusNode.inputs.splice(0, plusNode.inputs.length);
                connections.forEach((c, idx) => {
                    const port: MathInputPort = numberPort(
                        "math.input",
                        {
                            inputIndex: idx,
                        },
                    );
                    plusNode.inputs.push(port);
                    c.portIdTo = port.id;
                });
                arg.addNode(plusNode);
                return plusNode.outputs[0].id;
            }

            function goDeep(node: Expr) {
                switch (node.type) {
                    case "const": return goConst(node);
                    case "op": return goOp(node);
                    case "variable": return goVar(node);
                }
            }
            const outputPort = goDeep(expr.expr);
            arg.outputConnections.forEach((outputConnection) => {
                outputConnection.portIdFrom = outputPort;
            });


        },
    };
}
