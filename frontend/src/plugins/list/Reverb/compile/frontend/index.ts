import {
    ReverbConstNode,
    ReverbDelayNode,
    ReverbGateNode,
    ReverbMacrosDefinitionInputNode,
    ReverbMacrosDefinitionOutputNode,
    ReverbMacroUsageNode,
    ReverbMainOutputNode,
    ReverbMathNode,
    WholeReverbGraph,
} from "../../ReverbNodes";
import {
    fillInputNodeMinMaxAddCheckboxNodes,
    SaturatedCheckboxNode,
    SaturatedMainInputNode,
} from "./fillInputNodeMinMaxAddCheckboxNodes";
import { fillMissingCheckboxValues } from "./filMissingCheckboxValues";
import { replaceExprNodes } from "./replaceExprNodes";
import { replaceSampleRateNodes } from "./replaceSampleRateNodes";
import { applyIndependentReplaces } from "./SimpleReplacements";


export type FEPreparedGraphNode =
    | SaturatedMainInputNode
    | ReverbMainOutputNode
    | ReverbConstNode
    | ReverbGateNode
    | SaturatedCheckboxNode
    | ReverbMathNode
    | ReverbDelayNode
    | ReverbMacroUsageNode
    | ReverbMacrosDefinitionInputNode
    | ReverbMacrosDefinitionOutputNode
    ;
export type FEPreparedGraph = WholeReverbGraph<FEPreparedGraphNode>;
export function compileFrontend(
    graph: WholeReverbGraph,
    checkboxesValues: Record<string, boolean>,
    sampleRate: number,
): FEPreparedGraph {

    const filledCheckboxes = fillMissingCheckboxValues(checkboxesValues, graph);

    const minMaxFilled = fillInputNodeMinMaxAddCheckboxNodes(filledCheckboxes, graph);

    const prepared = applyIndependentReplaces(
        minMaxFilled,
        [
            replaceSampleRateNodes(sampleRate),
            replaceExprNodes(),
        ],
    );

    return prepared;

}
