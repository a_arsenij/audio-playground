import {
    NodeEditorConnection,
    NodeEditorNode,
    NodeEditorPort,
} from "../../../../../components/Common/Gridy/NodeEditor/NodeEditor";
import { ReverbGraph, WholeReverbGraph } from "../../ReverbNodes";


export type NEN = NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>;

export type SimpleReplacement<
    ELIMINATED_NODE_TYPE extends NEN,
    APPEARED_NODE_TYPE extends NEN,
> = {
    match: (obj: NEN) => obj is ELIMINATED_NODE_TYPE,
    replace: (arg: {
        inputConnections: NodeEditorConnection[],
        outputConnections: NodeEditorConnection[],
        node: ELIMINATED_NODE_TYPE,
        addNode: (node: APPEARED_NODE_TYPE) => void,
        addConnections: (...connections: NodeEditorConnection[]) => void,
        removeConnections: (...connections: NodeEditorConnection[]) => void,
    }) => void,
}

function applySimpleReplaceSubgraph<
    ELIMINATED_NODE_TYPE extends NEN,
    EXISTING_NODE_TYPE extends NEN,
    APPEARED_NODE_TYPE extends NEN,
>(
    graph: ReverbGraph<EXISTING_NODE_TYPE>,
    replacement: SimpleReplacement<ELIMINATED_NODE_TYPE, APPEARED_NODE_TYPE>,
): ReverbGraph<Exclude<EXISTING_NODE_TYPE, ELIMINATED_NODE_TYPE> | APPEARED_NODE_TYPE> {

    let changed = true;
    while (changed) {
        changed = false;
        for (const node of graph.nodes) {
            if (replacement.match(node)) {
                graph.nodes.remove(node);
                const inputs = node.inputs.map((p) => p.id).toSet();
                const outputs = node.outputs.map((p) => p.id).toSet();
                replacement.replace({
                    node: node as unknown as ELIMINATED_NODE_TYPE,
                    inputConnections: graph.connections.filter((c) => inputs.has(c.portIdTo)),
                    outputConnections: graph.connections.filter((c) => outputs.has(c.portIdFrom)),
                    // @ts-ignore
                    addNode: (n: APPEARED_NODE_TYPE) => graph.nodes.push(n),
                    addConnections: (c: NodeEditorConnection) => graph.connections.push(c),
                    removeConnections: (c: NodeEditorConnection) => graph.connections.remove(c),
                });
                changed = true;
                break;
            }
        }
    }

    return graph as ReverbGraph<Exclude<EXISTING_NODE_TYPE, ELIMINATED_NODE_TYPE> | APPEARED_NODE_TYPE>;

}

function applySimpleReplaceFullGraph<
    ELIMINATED_NODE_TYPE extends NEN,
    EXISTING_NODE_TYPE extends NEN,
    APPEARED_NODE_TYPE extends NEN,
>(
    graph: WholeReverbGraph<EXISTING_NODE_TYPE>,
    replacement: SimpleReplacement<APPEARED_NODE_TYPE, ELIMINATED_NODE_TYPE>,
): WholeReverbGraph<Exclude<EXISTING_NODE_TYPE, ELIMINATED_NODE_TYPE> | APPEARED_NODE_TYPE> {
    applySimpleReplaceSubgraph(graph.main, replacement);
    graph.macros.forEach((sub) => applySimpleReplaceSubgraph(sub.graph, replacement));
    return graph as WholeReverbGraph<Exclude<EXISTING_NODE_TYPE, ELIMINATED_NODE_TYPE> | APPEARED_NODE_TYPE>;
}

type IndiReps<
    EXISTING_NODE_TYPE extends NEN,
    REPS extends readonly SimpleReplacement<any, any>[],
    INDEX extends void[] = [],
    RES extends [NEN, NEN] = [never, never],
> = INDEX["length"] extends REPS["length"]
    ? WholeReverbGraph<Exclude<EXISTING_NODE_TYPE, RES[0]> | RES[1]>
    : REPS[INDEX["length"]] extends SimpleReplacement<infer A, infer B>
        ? IndiReps<EXISTING_NODE_TYPE, REPS, [void, ...INDEX], [RES[0] | A, RES[1] | B]>
        : never
    ;

export function applyIndependentReplaces<
    EXISTING_NODE_TYPE extends NEN,
    const REPS extends readonly SimpleReplacement<any, any>[],
>(
    graph: WholeReverbGraph<EXISTING_NODE_TYPE>,
    replacements: REPS,
): IndiReps<EXISTING_NODE_TYPE, REPS> {
    let res: any = graph;
    replacements.forEach((rep) => {
        res = applySimpleReplaceFullGraph(res, rep);
    });
    return res;
}
