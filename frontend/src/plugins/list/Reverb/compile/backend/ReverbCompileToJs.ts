import { ReverbCompilationResultJS } from "../../ReverbState";
import { ReverbGraphCompiled, ReverbNodeCompiled, ReverbPortCompiled } from "../middleend";
import { NoConstMathNode } from "../middleend/inlineConstants";
import { getSignalInputNode, getSignalOutputNode } from "./utils";

export function compileToJsFunction(nodes: ReverbGraphCompiled, bufferLength: number): ReverbCompilationResultJS {

    const signalOutputVars = getInputPortTokens(getSignalOutputNode(nodes));
    const end = signalOutputVars.map((v, i) => {
        return `memView[${i}] = ${v};`;
    }).join(`
    `);

    const signalInputNode = getSignalInputNode(nodes);
    const inputNodeParams = signalInputNode ? signalInputNode.outputs.map(portToToken) : [];
    const params = ["count", "memView", ...inputNodeParams].join(", ");

    const instructions = [...nodes.map(nodeToJsInstruction), end];

    const body = instructions.join(`
    `);
    // console.log(body);

    const delays: number[][] = [];
    nodes.forEach((dn) => {
        if (dn.type === "delayInput") {
            delays[dn.delayId] = (new Array(dn.maxLength)).fill(0);
        }
    });

    const thisObj = {
        delays,
    };

    const run = new Function(params, body);

    const mem = new ArrayBuffer(8 * signalOutputVars.length);
    const memView = new Float64Array(mem);
    let count = 0;
    function runOnArray(arr: number[]): number[] {
        const res: number[] = [];
        for (let i = 0; i < bufferLength; i++) {
            const rest = inputNodeParams.map((_, j) => arr[i * inputNodeParams.length + j]);
            run.call(thisObj, count, memView, ...rest);


            signalOutputVars.forEach((_, j) => {
                res.push(memView[j]!);
            });
            count++;
        }
        return res;
    };
    return {
        run: runOnArray,
        inputsCount: inputNodeParams.length,
        outputsCount: signalOutputVars.length,
    };
}

function nodeToJsInstruction(n: ReverbNodeCompiled): string {
    const prefix = getJsPrefix(n);


    switch (n.type) {
        case "mainInput":
        case "mainOutput":
            return "";
        case "math":
            return `${prefix} ${mathNodeToInstruction(n)}`;
        case "delayOutput":{
            const i00 = n.inputs[0];
            if (!i00) {
                throw new Error(`cannot find input port 0 in delayOutput node: ${n.id}`);
            }
            const i0 = portToToken(i00);
            return `this.delays[${n.delayId}][ count % this.delays[${n.delayId}].length ] = ${i0} ;`;
        }
        case "delayInput":{
            const i00 = n.inputs[0];
            if (!i00) {
                throw new Error(`cannot find input port 0 in delayInput node: ${n.id}`);
            }
            const i0 = portToToken(i00);
            return `${prefix} this.delays[${n.delayId}][ ( count - Math.max( Math.ceil( isNaN(${i0}) ? 1 : ${i0} ), 1) + this.delays[${n.delayId}].length ) % this.delays[${n.delayId}].length ];`;
        }
    }
}

function mathNodeToInstruction(n: NoConstMathNode): string {
    const i00 = n.inputs[0];
    if (!i00) {
        throw new Error(`cannot find input port 0 in math node: ${n.id}`);
    }
    const i0 = portToToken(i00);
    switch (n.op) {
        case "plus":
            return `${getInputPortTokens(n).join(" + ")} ;`;
        case "minus":{
            const ins = n.inputs.toFixedLength(2).map(portToToken);
            return `${ins[0]} - ${ins[1]} ;`;
        }
        case "multiply":
            return `${getInputPortTokens(n).join(" * ")} ;`;
        case "divide":{
            const ins = n.inputs.toFixedLength(2).map(portToToken);
            return `${ins[0]} / ${ins[1]} ;`;
        }
        case "replaceNaN": {
            const ins = n.inputs.toFixedLength(2).map(portToToken);
            return `isNaN(${ins[0]}) ? ${ins[1]} : ${ins[0]}`;
        }
        case "min":
            return `Math.min(${getInputPortTokens(n).join(" , ")}) ;`;
        case "max":
            return `Math.max(${getInputPortTokens(n).join(" , ")}) ;`;
        case "abs":
            return `Math.abs(${i0}) ;`;
        case "sin":
            return `Math.sin(${i0}) ;`;
        case "cos":
            return `Math.cos(${i0}) ;`;
        case "tan":
            return `Math.tan(${i0}) ;`;
        case "sqrt":
            return `Math.sqrt(${i0}) ;`;
        case "exp":
            return `Math.exp(${i0}) ;`;
        case "pow2":
            return `2**${i0}`;
        case "loge":
            return `Math.log(${i0}) ;`;
        case "log2":
            return `Math.log2(${i0}) ;`;
        case "log10":
            return `Math.log10(${i0}) ;`;
        case "mod": {
            const ins = n.inputs.toFixedLength(2).map(portToToken);
            return `((${ins[0]} % ${ins[1]}) + ${ins[1]}) % ${ins[1]} ;`;
        }
        case "pow": {
            const ins = n.inputs.toFixedLength(2).map(portToToken);
            return `${ins[0]} ** ${ins[1]}`;
        }
        case "heaviside": {
            const ins = n.inputs.toFixedLength(1).map(portToToken);
            return `${ins[0]} > 0 ? 1 : 0`;
        }
    }
}

function portToToken(p: ReverbPortCompiled) {
    if ("constValue" in p && p.constValue !== undefined) {
        return p.constValue.toString();
    }
    return fId(p.id);
}
function getInputPortTokens(n: ReverbNodeCompiled) {
    return n.inputs.map(portToToken);
}

function fId(s: string = "") {
    return `_${s}`;
}

function getJsPrefix(n: ReverbNodeCompiled) {
    if (n.outputs.length === 0) {
        return "";
    }
    if (n.outputs.length === 1) {
        const outs = n.outputs.toFixedLength(1);
        return `const ${(fId(outs[0].id))} = `;
    }
    return `const [${n.outputs.map(portToToken)}] = `;
}

