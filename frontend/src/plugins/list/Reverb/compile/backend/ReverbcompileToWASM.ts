
import { compileModuleToWASM } from "@audio-playground/lib-wasm/CompileWASM";
import { createModule, func, Global, Local } from "@audio-playground/lib-wasm/Op";
import { control, f64, i32 } from "@audio-playground/lib-wasm/ops";
import { OP } from "@audio-playground/lib-wasm/ops/OpsTypes";
import { std } from "@audio-playground/lib-wasm/std/STD";
import { voidType, WAType } from "@audio-playground/lib-wasm/Types";

import { ReverbCompilationResult, ReverbCompilationResultRun } from "../../ReverbState";
import { ReverbGraphCompiled, ReverbNodeCompiled, ReverbPortCompiled } from "../middleend";
import {
    NoConstDelayInputNode,
    NoConstSignalInputNode,
    NoConstSignalOutputNode,
} from "../middleend/inlineConstants";
import { getSignalInputNode, getSignalOutputNode } from "./utils";

const F64_PER_PAGE = 8192;

const EXPONENT_PRECISION = 4;

type OP2 = OP | OP[]
type LocalsT = Record<string, Local<string, WAType>> & {
    count: Local<string, WAType>,
}


export function compileToWASM(
    nodes: ReverbGraphCompiled,
    bufferLength: number,
): Promise<ReverbCompilationResult> {
    const signalInputNode = getSignalInputNode(nodes);
    const paramNames = signalInputNode ? signalInputNode.outputs.map((p) => p.id) : [];

    const resultPorts = getSignalOutputNode(nodes).inputs;

    const resultNames = resultPorts.map((_, i) => {
        return `result_${i}`;
    });


    const MEMORY_RESERVED_FOR_INPUT = paramNames.length * bufferLength;
    const MEMORY_RESERVED_FOR_OUTPUT = resultNames.length * bufferLength;

    const INPUT_OFFSET = 0;
    const OUTPUT_OFFSET = MEMORY_RESERVED_FOR_INPUT;



    const opNodes = nodes.filter(notInOrOut);


    const localNames = opNodes.flatMap(nodeToLocalName);

    const halfDoneModule = createModule();
    const globals: Record<string, WebAssembly.Global<"f64">> = {};


    const delayOffsetMap: Record<string, number> = {};
    const delayLengthMap: Record<string, number> = {};
    const delayInputs = opNodes.filter(isDelayInput);
    let offset = MEMORY_RESERVED_FOR_OUTPUT + MEMORY_RESERVED_FOR_INPUT;
    const delaysMaxLength = delayInputs.map((n) => {
        const id = n.delayId;
        delayOffsetMap[id] = offset;
        const mL = n.maxLength ?? 1;
        const noNaN = isNaN(mL) ? 1 : mL;
        const l = Math.ceil(noNaN);
        delayLengthMap[id] = l;
        offset += l;
        return l;
    });


    const delayF64NumbersTotal = delaysMaxLength.reduce((prev, curr) => prev + curr, 0) + MEMORY_RESERVED_FOR_OUTPUT + MEMORY_RESERVED_FOR_INPUT;
    const PAGES = Math.ceil(delayF64NumbersTotal / F64_PER_PAGE);
    const funcName = "runWithMemory";
    const myModule = halfDoneModule
        .memory(["js", "mem"], PAGES)
        .func(
            (globals, _functions) => {
                const f = func()
                    .name("graphRunner")
                    .result(
                        ...resultPorts.map(() => f64),
                    )
                    .param("count", i32);
                let f2 = f;

                paramNames.forEach((pN) => {
                    // @ts-ignore
                    f2 = f2.param(pN, f64);
                });

                localNames.forEach((lN) => {
                    // @ts-ignore
                    f2 = f2.local(lN, f64);
                });

                const f3 = f2
                    .body((locals) => [
                        ...opNodes.flatMap((n) => {
                            return nodeToOp(n, locals, globals, delayOffsetMap, delayLengthMap);
                        }),
                        ...resultPorts.map((p) => inputPortToOp(p, locals)),
                    ]);
                return f3;
            },
        )
        .func((_globals, functions) => {
            const fPart1 = func()
                .name(funcName)
                .param("time", i32)
                .exported(true)
                .result();
            let fPart2 = fPart1;

            for (const resultName of resultNames) {
                // @ts-ignore
                fPart2 = fPart2.local(resultName, f64);
            }
            type LocalsB = Record<string, Local<string, WAType>> & {
                time: Local<string, WAType>,
                index: Local<string, WAType>,
                indexInput: Local<string, WAType>,
                indexOutput: Local<string, WAType>,
            }
            return fPart2
                .local("index", i32)
                .local("indexInput", i32)
                .local("indexOutput", i32)
                .body((locals: LocalsB) => [
                    i32.const(0),
                    locals.index.set,
                    i32.const(0),
                    locals.indexInput.set,
                    i32.const(0),
                    locals.indexOutput.set,

                    control.loop(voidType, (label) => {

                        const time = [
                            locals.index.get,
                            locals.time.get,
                            i32.add,
                        ];
                        const paramsLoad = paramNames.flatMap((_, i) => {
                            return [
                                locals.indexInput.get,
                                f64.load((INPUT_OFFSET + i) * 8, 3),
                            ];
                        });

                        const main = [
                            functions.graphRunner.call,
                        ];

                        const resultLocals = resultNames.map((name) => {
                            const l = locals[name];
                            if (!l) {
                                throw new Error(`cannot find local variable for result: ${name}`);
                            }
                            return l;
                        });
                        const revertOutputs = resultLocals.map((l) => {
                            return l.set;
                        }).toReversed();
                        const storeOutputs = resultLocals.flatMap((l, i) => {
                            return [
                                locals.indexOutput.get,
                                l.get,
                                f64.store((OUTPUT_OFFSET + i) * 8, 3),
                            ];
                        });

                        const cycle = [
                            locals.indexInput.get,
                            i32.const(paramNames.length * 8),
                            i32.add,
                            locals.indexInput.set,

                            locals.indexOutput.get,
                            i32.const(resultNames.length * 8),
                            i32.add,
                            locals.indexOutput.set,

                            locals.index.get,
                            i32.const(1),
                            i32.add,
                            locals.index.tee,

                            i32.const(bufferLength),
                            i32.lt_u,
                            control.br_if(label),
                        ];
                        return [
                            ...time,
                            ...paramsLoad,
                            ...main,
                            ...revertOutputs,
                            ...storeOutputs,
                            ...cycle,
                        ];
                    }),
                ]);
        },
        )
        .build();

    // const wat = compileModuleToWAT(myModule);
    // console.log(wat);


    const wasm = compileModuleToWASM(myModule);
    // console.log(prettyWasm(wasm));

    // console.log(wasm.bytecode.map((v) => byteToString(v, false)).join(""));

    const wasmBytes = new Uint8Array(wasm.bytecode);


    const memory = new WebAssembly.Memory({ initial: PAGES });

    const importObject = {
        js: { mem: memory },
        globals,
    };

    const setKnobValue = (knobId: string, value: number) => {
        const glob = globals[knobId];
        if (!glob) {
            console.warn(
                "knobId not found in globals, check if knob is connected to main part of the graph",
            );
            return;
        }
        glob.value = value;
    };

    return WebAssembly.instantiate(
        wasmBytes,
        importObject,
    ).then((obj) => {

        const func = obj.instance.exports.runWithMemory;
        if (!func) {
            throw new Error(`No ${funcName} found`);
        }
        if (typeof func !== "function") {
            throw new Error(`${funcName} is not a function`);
        }

        return {
            setKnobValue,
            run: func as ReverbCompilationResultRun,
            memory: memory.buffer,
            inputsCount: paramNames.length,
            outputsCount: resultNames.length,
        };
    });
}


function nodeToLocalName(node: ReverbNodeCompiled): string[] {
    return node.outputs.map((p) => p.id);
}


function nodeToOp(
    node: Exclude<ReverbNodeCompiled, NoConstSignalInputNode | NoConstSignalOutputNode>,
    locals: LocalsT,
    _: Record<string, Global<string, WAType>>,
    delayOffsetMap: Record<string, number>,
    delayLengthMap: Record<string, number>,
): OP2[] {

    switch (node.type) {
        case "math":
            switch (node.op) {
                case "plus":
                    return mathOp(f64.add, node, locals, "multi");
                case "minus":
                    return mathOp(f64.sub, node, locals, 2);
                case "multiply":
                    return mathOp(f64.mul, node, locals, "multi");
                case "divide":
                    return mathOp(f64.div, node, locals, 2);
                case "replaceNaN":
                    return mathOp(std.f64.replaceNaN.call, node, locals, 2);
                case "min":
                    return mathOp(f64.min, node, locals, "multi");
                case "max":
                    return mathOp(f64.max, node, locals, "multi");
                case "sqrt":
                    return mathOp(f64.sqrt, node, locals, 1);
                case "abs":
                    return mathOp(f64.abs, node, locals, 1);
                case "sin":
                    return mathOp(std.f64.sin.call, node, locals, 1);
                case "cos":
                    return mathOp(std.f64.cos.call, node, locals, 1);
                case "tan":
                    return mathOp(std.f64.tan.call, node, locals, 1);
                case "loge":
                    return mathOp(std.f64.logE.call, node, locals, 1);
                case "log2":
                    return mathOp(std.f64.log2.call, node, locals, 1);
                case "log10":
                    return mathOp(std.f64.log10.call, node, locals, 1);
                case "exp":
                    return mathOp(std.f64.exp(EXPONENT_PRECISION).call, node, locals, 1);
                case "pow2":
                    return mathOp(std.f64.pow2.call, node, locals, 1);
                case "mod":
                    return mathOp(std.f64.mod.call, node, locals, 2);
                case "pow":
                    return mathOp(std.f64.pow.call, node, locals, 2);
                case "heaviside":
                    return mathOp(std.f64.heaviside.call, node, locals, 1);
            }
            break;

        case "delayInput":{
            const id = node.delayId;
            const delayArrLength = delayLengthMap[id];
            if (delayArrLength === undefined) {
                throw new Error(`cannot find array length value for delay: ${id}`);
            }
            const offset = delayOffsetMap[id];
            if (offset === undefined) {
                throw new Error(`cannot find offset value for delay: ${id}`);
            }
            const in0 = node.inputs[0];
            if (!in0) {
                throw new Error(`cannot find input port 0 in delayInput node: ${node.id}`);
            }
            const out0 = node.outputs[0];
            if (!out0) {
                throw new Error(`cannot find output port 0 in delayInput node: ${node.id}`);
            }
            const out = locals[out0.id];
            if (!out) {
                throw new Error(`cannot find local variable for port: ${out0.id}`);
            }

            const delayOffset = offset * 8;
            const getIn0 = inputPortToOp(in0, locals);

            return [
                locals.count.get,

                getIn0,
                std.f64.delayToi32.call,

                i32.sub,

                i32.const(delayArrLength),
                std.i32.rem.call,

                i32.const(8),
                i32.mul,

                f64.load(delayOffset, 3),
                out.set,
            ];
        }
        case "delayOutput": {
            const id = node.delayId;
            const delayArrLength = delayLengthMap[id];
            if (delayArrLength === undefined) {
                throw new Error(`cannot find array length value for delay: ${id}`);
            }
            const offset = delayOffsetMap[id];
            if (offset === undefined) {
                throw new Error(`cannot find offset value for delay: ${id}`);
            }
            const in0 = node.inputs[0];
            if (!in0) {
                throw new Error(`cannot find input port 0 in delayOutput node: ${node.id}`);
            }
            const delayOffset = offset * 8;
            const getIn0 = inputPortToOp(in0, locals);

            return [
                locals.count.get,
                i32.const(delayArrLength),
                std.i32.rem.call,
                i32.const(8),
                i32.mul,
                getIn0,
                f64.store(delayOffset, 3),
            ];
        }
    }
}

function inputPortToOp(port: ReverbPortCompiled, locals: LocalsT): OP {
    if ("constValue" in port && port.constValue !== undefined) {
        return f64.const(port.constValue);
    }
    const local = locals[port.id];
    if (!local) {
        throw new Error(`cannot find local variable for port: ${port.id}`);
    }
    return local.get;
}

function mathOp(
    wasmOp: OP,
    node: ReverbNodeCompiled,
    locals: LocalsT,
    argsCount: 1 | 2 | "multi",
): OP2[] {
    const out0 = node.outputs[0];
    if (!out0) {
        throw new Error(`cannot find output port 0 in math node: ${node.id}`);
    }
    const out = locals[out0.id];
    if (!out) {
        throw new Error(`cannot find local variable for port: ${out0.id}`);
    }

    const ins = argsCount === "multi"
        ? node.inputs
        : node.inputs.toFixedLength(argsCount);
    const ops: OP[] = argsCount === "multi"
        ? new Array(node.inputs.length - 1).fill(wasmOp)
        : [wasmOp];

    return [
        ...ins.map((p) => inputPortToOp(p, locals)),
        ...ops,
        out.set,
    ];
}

function isDelayInput(n: ReverbNodeCompiled): n is NoConstDelayInputNode {
    return n.type === "delayInput";
}

function notInOrOut(
    n: ReverbNodeCompiled,
): n is Exclude<ReverbNodeCompiled, NoConstSignalInputNode | NoConstSignalOutputNode> {
    return n.type !== "mainInput" && n.type !== "mainOutput";
}
