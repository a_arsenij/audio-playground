import { ReverbGraphCompiled } from "../middleend";

export function getSignalInputNode(nodes: ReverbGraphCompiled) {
    return nodes.find((n) => n.type === "mainInput");
}

export function getSignalOutputNode(nodes: ReverbGraphCompiled) {
    const outNode = nodes.find((n) => n.type === "mainOutput");
    if (!outNode) {
        throw new Error("signal output node not found");
    }
    return outNode;
}
