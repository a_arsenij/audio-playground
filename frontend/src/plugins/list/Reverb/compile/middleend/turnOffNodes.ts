import { NoMacroNode } from "./inlineMacro";
import {
    ConstNode,
    DEFAULT_CONST_0_PORT, DelayNode, MathNode,
    ReducedCheckboxNode,
    ReducedGateNode, ReducedInputPort,
    ReducedOutputPort, ReducedSignalInputNode, SignalInputNode, SignalOutputNode,
} from "./transformGraph";

export function turnOffNodes(graph: NoMacroNode[]): NoBoolNode[] {
    const boolValue: Map<ReducedInputPort, boolean> = compileBooleanConstants(graph);
    const outPortToInputPorts: Record<string, ReducedInputPort[]> = {};

    graph.forEach((n) => {
        n.inputs.number.forEach((p) => {
            const arr = outPortToInputPorts[p.id] || [];
            arr.push(p);
            outPortToInputPorts[p.id] = arr;
        });
    });
    function disconnectNode(n: NoMacroNode) {
        n.outputs.number.forEach((p) => {
            const inputs = outPortToInputPorts[p.id] || [];
            inputs.forEach((ip) => {
                ip.id = DEFAULT_CONST_0_PORT.id;
            });
        });
    }

    const newNodes: NoBoolNode[] = [];
    graph.forEach((n) => {
        if (isGateNode(n) || isCheckboxNode(n)) {
            return;
        }

        const disabledPort = n.inputs.boolean[0];

        const disabled = disabledPort ? boolValue.get(disabledPort) : false;
        if (disabled) {
            disconnectNode(n);
            return;
        }
        newNodes.push({
            ...n,
            inputs: n.inputs.number,
            outputs: n.outputs.number,
        });
    });
    return newNodes;
}
export function isGateNode(n: NoMacroNode): n is ReducedGateNode {
    return n.type === "gate";
}
export function isCheckboxNode(n: NoMacroNode): n is ReducedCheckboxNode {
    return n.type === "checkbox";
}
export function isInputNode(n: NoMacroNode): n is ReducedSignalInputNode {
    return n.type === "mainInput";
}
function compileBooleanConstants(graph: NoMacroNode[]): Map<ReducedInputPort, boolean> {

    const boolValue = new Map<ReducedInputPort, boolean>();
    const inputPortsToNodeMap:
        Record<string, Exclude<NoMacroNode, ReducedGateNode | ReducedCheckboxNode>> = {};
    const inputDisablePorts: ReducedInputPort[] = [];
    const outPortIdToPortMap: Record<string, ReducedOutputPort> = {};
    const outPortIdToNodeMap: Record<string, NoMacroNode> = {};

    const outPortToInputPorts: Record<string, ReducedInputPort[]> = {};


    graph.forEach((n) => {
        if (isGateNode(n) || isCheckboxNode(n)) {
            return;
        }
        const disablePort = n.inputs.boolean[0];
        if (disablePort) {
            inputPortsToNodeMap[disablePort.id] = n;
            inputDisablePorts.push(disablePort);
        }
    });

    graph.forEach((n) => {
        n.outputs.boolean.forEach((p) => {
            outPortIdToPortMap[p.id] = p;
            outPortIdToNodeMap[p.id] = n;
        });
    });

    graph.forEach((n) => {
        n.inputs.boolean.forEach((p) => {
            const arr = outPortToInputPorts[p.id] || [];
            arr.push(p);
            outPortToInputPorts[p.id] = arr;
        });
    });

    inputDisablePorts.forEach((ip) => {
        const constValue = boolValue.get(ip);
        if (constValue !== undefined) {
            return;
        }
        const n = outPortIdToNodeMap[ip.id];
        if (!n) {
            throw new Error(`cannot find node by port id: ${ip.id}`);
        }
        computeBooleanNode(n);
    });

    function computeBooleanNode(n: NoMacroNode) {

        if (n.type === "checkbox") {
            n.outputs.boolean.forEach((p) => {
                updateInputBooleanPorts(p, n.value);
            });
            return;
        }

        if (n.type === "gate") {
            const inputValues = n.inputs.boolean.map((p) => {
                const constValue = boolValue.get(p);
                if (constValue !== undefined) {
                    return constValue;
                }
                const n = outPortIdToNodeMap[p.id];
                if (!n) {
                    throw new Error(`cannot find node by port id: ${p.id}`);
                }
                computeBooleanNode(n);

                const val = boolValue.get(p);
                if (val === undefined) {
                    throw new Error(`boolean value for port ${p.id} was not computed`);
                }

                return val;
            });

            const resPort0 = n.outputs.boolean[0];

            if (!resPort0) {
                throw new Error(`cannot find boolean output port in gate node, node id: ${n.id}`);
            }

            const in0 = !!inputValues[0];
            const in1 = !!inputValues[1];

            let resValue: boolean;

            switch (n.op) {
                case "not": {
                    resValue = !in0;
                    break;
                }
                case "and": {
                    resValue = in0 && in1;
                    break;
                }
                case "or": {
                    resValue = in0 || in1;
                    break;
                }
                case "xor": {
                    resValue = in0 !== in1;
                    break;
                }
                case "xnor": {
                    resValue = in0 === in1;
                    break;
                }
            }
            updateInputBooleanPorts(resPort0, resValue);
            return;

        }
    }

    function updateInputBooleanPorts(outPort: ReducedOutputPort, val: boolean) {
        const inputs = outPortToInputPorts[outPort.id] || [];
        inputs.forEach((ip) => {
            boolValue.set(ip, val);
        });
    }
    return boolValue;
}





export type NoBoolBase = {
    id: string,
    inputs: ReducedInputPort[],
    outputs: ReducedOutputPort[],
}

type NoBoolMathNode = NoBoolBase & MathNode
type NoBoolConstNode = NoBoolBase & ConstNode
export type NoBoolDelayNode = NoBoolBase & DelayNode
export type NoBoolSignalInputNode = NoBoolBase & SignalInputNode
export type NoBoolSignalOutputNode = NoBoolBase & SignalOutputNode

export type NoBoolNode =
    | NoBoolMathNode
    | NoBoolConstNode
    | NoBoolDelayNode
    | NoBoolSignalInputNode
    | NoBoolSignalOutputNode
