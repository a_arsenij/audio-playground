
import { getIncrementalCounter } from "@audio-playground/lib-common/std/Id";

import { estimateMaxDelay } from "./estimateMaxDelay";
import { inlineConstants, NoConstNode } from "./inlineConstants";
import { inlineMacro } from "./inlineMacro";
import { orderGraph } from "./orderGraph";
import { removeUnreachable } from "./removeUnreachable";
import { transformDelayNodes } from "./transformDelayNodes";
import { FEPreparedGraph, transformWholeGraph } from "./transformGraph";
import { turnOffNodes } from "./turnOffNodes";



export type ReverbGraphCompiled = NoConstNode[];
export type ReverbNodeCompiled = NoConstNode;
export type ReverbPortCompiled = NoConstNode["inputs"][number] | NoConstNode["outputs"][number];
export function compileMiddleend(
    prepared: FEPreparedGraph,
): ReverbGraphCompiled {


    const transformedGraph = transformWholeGraph(prepared);

    const linkedNodes = inlineMacro(transformedGraph);

    const turnedOnNodes = turnOffNodes(linkedNodes);

    const reachableNodes = removeUnreachable(turnedOnNodes);

    const delayedNodes = transformDelayNodes(reachableNodes, getIncrementalCounter());

    const orderedGraph = orderGraph(delayedNodes);

    const inlinedConstantsGraph = inlineConstants(orderedGraph);

    const withMaxDelayGraph = estimateMaxDelay(inlinedConstantsGraph);

    return withMaxDelayGraph;
}
