import { generateId } from "@audio-playground/lib-common/std/Id";

import { NoBoolBase, NoBoolDelayNode, NoBoolNode } from "./turnOffNodes";

export function transformDelayNodes(
    nodes: NoBoolNode[],
    getDelayId: () => number,
): NewDelayNode[] {
    const nn = nodes.map((n: NoBoolNode) => {
        if (n.type === "delay") {
            return tearDelayNode(n, getDelayId);
        } else {
            return n;
        }
    }).flat(1);

    return nn;
}

function tearDelayNode(
    n: NoBoolDelayNode,
    getDelayId: () => number,
) {

    const delayId = getDelayId();
    const in0 = n.inputs[0];
    const in1 = n.inputs[1];
    const out0 = n.outputs[0];
    if (!in0 || !in1 || !out0) {
        throw new Error(`incorrect number of ports in delay node: ${n.id}`);
    }
    const inNode: DelayInputNodeD = {
        id: generateId(),
        type: "delayInput",
        maxLength: n.maxLength,
        delayId,
        inputs: [in1],
        outputs: [out0],
    };

    const outNode: DelayOutputNodeD = {
        id: generateId(),
        type: "delayOutput",
        delayId,
        inputs: [in0],
        outputs: [],
    };

    return [
        outNode,
        inNode,
    ];
}
export type DelayInputNode = {
    type: "delayInput",
    maxLength: number,
    delayId: number,
}
type DelayInputNodeD = NoBoolBase & DelayInputNode
export type DelayOutputNode = {
    type: "delayOutput",
    delayId: number,
}
type DelayOutputNodeD = NoBoolBase & DelayOutputNode

export type NewDelayNode = Exclude<NoBoolNode, NoBoolDelayNode> | DelayInputNodeD | DelayOutputNodeD;
