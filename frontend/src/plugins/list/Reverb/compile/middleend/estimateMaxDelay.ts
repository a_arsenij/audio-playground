
import { IMath, Interval, union, UNIVERSUM_INTERVAL } from "@audio-playground/lib-common/math/Interval";

import { ConstInputPort, NoConstMathNode, NoConstNode } from "./inlineConstants";
import { findNodeByPortId, getPortNodeMap } from "./utils";

export function estimateMaxDelay(nodes: NoConstNode[]): NoConstNode[] {
    const portNodeMap = getPortNodeMap(nodes);
    const nodeMap = Object.fromEntries(nodes.map((n) => [n.id, n]));

    const mapPortToInterval = new Map<string, Interval>();

    const weakSetCalcInterval = new WeakSet();

    nodes.forEach((n) => {
        if (n.type === "delayInput") {
            markParentNodes(n);
        }
    });

    function markParentNodes(
        node: NoConstNode,
    ) {
        if (weakSetCalcInterval.has(node)) {
            return;
        }
        node.inputs
            .filter(notConstPort)
            .map((p) => findNodeByPortId(p.id, portNodeMap, nodeMap))
            .forEach((n) => {
                weakSetCalcInterval.add(node);
                markParentNodes(n);
            });
    }

    nodes.forEach((n) => {
        if (!weakSetCalcInterval.has(n)) {
            return;
        }
        let intervals: Interval[] = [];
        switch (n.type) {
            case "math":
                intervals = [calcMathNodeInterval(n, mapPortToInterval)];
                break;
            case "mainInput":
                intervals = n.outputs.map((p) => {
                    return [p.min ?? Number.NEGATIVE_INFINITY, p.max ?? Number.POSITIVE_INFINITY];
                });
                break;
            case "mainOutput":
            case "delayInput":
            case "delayOutput":
                break;
        }
        n.outputs.forEach((p, i) => {
            const int = intervals[i] || UNIVERSUM_INTERVAL;
            mapPortToInterval.set(p.id, int);
        });
    });
    nodes.forEach((n) => {
        if (n.type === "delayInput") {
            const int = mapPortToInterval.get(n.inputs[0]!.id);
            const interval = int || UNIVERSUM_INTERVAL;
            const calculatedMax = isNaN(interval[1]) ? Number.POSITIVE_INFINITY : interval[1];
            n.maxLength = Math.max(1, Math.min(calculatedMax, n.maxLength));
        }
    });

    return nodes;
}

function notConstPort<T extends object>(p: T): p is Exclude<T, ConstInputPort> {
    return !("constValue" in p) || p.constValue === undefined;
}

function calcMathNodeInterval(n: NoConstMathNode, intervalMap: Map<string, Interval>): Interval {
    const intervals: Interval[] = n.inputs.map((p) => {
        if ("constValue" in p && p.constValue !== undefined) {
            if (isNaN(p.constValue)) {
                return UNIVERSUM_INTERVAL;
            }
            return [p.constValue, p.constValue];
        }
        const int = intervalMap.get(p.id);
        return int || UNIVERSUM_INTERVAL;
    });

    switch (n.op) {
        case "plus":
            return IMath.add(...intervals);
        case "minus":
            return IMath.sub(intervals[0]!, intervals[1]!);
        case "multiply":
            return intervals.reduce((prev, curr) => IMath.mult(prev, curr), [1, 1]);
        case "divide":
            return IMath.div(intervals[0]!, intervals[1]!);
        case "abs":
            return IMath.abs(intervals[0]!);
        case "min":
            return IMath.min(intervals);
        case "max":
            return IMath.max(intervals);
        case "sin":
            return IMath.sin(intervals[0]!);
        case "cos":
            return IMath.cos(intervals[0]!);
        case "tan":
            return IMath.tan(intervals[0]!);
        case "sqrt": return [0, Number.POSITIVE_INFINITY];
        case "loge": return UNIVERSUM_INTERVAL;
        case "log2": return UNIVERSUM_INTERVAL;
        case "log10": return UNIVERSUM_INTERVAL;
        case "exp": return UNIVERSUM_INTERVAL;
        case "pow2": return [0, Number.POSITIVE_INFINITY];
        case "mod": return [0, Number.POSITIVE_INFINITY];
        case "pow": return UNIVERSUM_INTERVAL;
        case "heaviside":
            return [0, 1];
        case "replaceNaN":
            return union(...intervals);

    }
    throw new Error("??");
}
