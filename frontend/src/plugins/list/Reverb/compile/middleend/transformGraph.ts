

import { MathOp } from "@audio-playground/lib-common/math/Expr";
import { generateId } from "@audio-playground/lib-common/std/Id";
import { Expect } from "@audio-playground/lib-common/types/UtilityTypes";

import {
    MacroUsageInputPort,
    ReverbGateOp, ReverbGraph,
    WholeReverbGraph,
} from "../../ReverbNodes";
import { FEPreparedGraphNode } from "../frontend";

export type ReducedOutputPort = {
    id: string,
    min?: number,
    max?: number,
}
export type ReducedInputPort = {
    id: string,
    oldId: string,
}
export type ReducedMacroUsageOutputPort = ReducedOutputPort & {
    macroPortId: string,
}
type ReducedMacroUsageInputPort = ReducedInputPort & {
    macroPortId: string,
}
type Ports =
    | ReducedInputPort
    | ReducedOutputPort
    | ReducedMacroUsageOutputPort
    | ReducedMacroUsageInputPort
;

export type ReducedPorts<T extends Ports> = {
    number: T[],
    boolean: T[],
}

export type ReducedNodeBase = {
    id: string,
    inputs: ReducedPorts<ReducedInputPort>,
    outputs: ReducedPorts<ReducedOutputPort>,
}

export type CheckboxNode = {
    type: "checkbox",
    value: boolean,
}

export type ReducedCheckboxNode = ReducedNodeBase & CheckboxNode

export type ConstNode = {
    type: "const",
    value: number,
}
export type ReducedConstNode = ReducedNodeBase & ConstNode
export type GateNode = {
    type: "gate",
    op: ReverbGateOp,
}
export type ReducedGateNode = ReducedNodeBase & GateNode

export type MathNode = {
    type: "math",
    op: MathOp,
}
export type ReducedMathNode = ReducedNodeBase & MathNode
export type DelayNode = {
    type: "delay",
    maxLength: number,
}
export type ReducedDelayNode = ReducedNodeBase & DelayNode

export type ReducedMacroUsageNode = {
    id: string,
    type: "macroUsage",
    macroId: string,
    inputs: ReducedPorts<ReducedMacroUsageInputPort>,
    outputs: ReducedPorts<ReducedMacroUsageOutputPort>,
}
export type MacrosDefinitionInputNode = {
    type: "macroDefinitionInput",
}
export type ReducedMacrosDefinitionInputNode = ReducedNodeBase & MacrosDefinitionInputNode
export type MacrosDefinitionOutputNode = {
    type: "macroDefinitionOutput",
}
export type ReducedMacrosDefinitionOutputNode = ReducedNodeBase & MacrosDefinitionOutputNode

export type SignalInputNode = {
    type: "mainInput",
}
export type ReducedSignalInputNode = ReducedNodeBase & SignalInputNode
export type SignalOutputNode = {
    type: "mainOutput",
}
export type ReducedSignalOutputNode = ReducedNodeBase & SignalOutputNode


export type ReducedNode =
    | ReducedMathNode
    | ReducedConstNode
    | ReducedCheckboxNode
    | ReducedGateNode
    | ReducedMacroUsageNode
    | ReducedDelayNode
    | ReducedMacrosDefinitionInputNode
    | ReducedMacrosDefinitionOutputNode
    | ReducedSignalInputNode
    | ReducedSignalOutputNode

export type ReducedGraph = {
    macros: Record<string, ReducedNode[]>,
    main: ReducedNode[],
};


export type FEPreparedGraph = WholeReverbGraph<FEPreparedGraphNode>;
type FEPreparedSubgraph = ReverbGraph<FEPreparedGraphNode>;

export type ValueType = Exclude<FEPreparedGraphNode["inputs"][number], never>["valueType"];

type ParamNodeTypes = FEPreparedGraphNode["type"];
type ResultNodeTypes = ReducedNode["type"];

type Check = ParamNodeTypes extends ResultNodeTypes ? (
    ResultNodeTypes extends ParamNodeTypes ? true : false
    ) : false;

type A = Exclude<ParamNodeTypes, ResultNodeTypes>;
type B = Exclude<ResultNodeTypes, ParamNodeTypes>;
type AssertAllNodeTypesAreHandled = Expect<Check>;


const DEFAULT_CONST_FALSE_PORT: ReducedOutputPort = {
    id: "003",
};
const DEFAULT_CONST_FALSE_NODE: ReducedCheckboxNode = {
    id: "004",
    type: "checkbox",
    value: false,
    inputs: {
        boolean: [],
        number: [],
    },
    outputs: {
        boolean: [DEFAULT_CONST_FALSE_PORT],
        number: [],
    },
};

export const DEFAULT_CONST_0_PORT: ReducedOutputPort = {
    id: "001",
};
const DEFAULT_CONST_0_NODE: ReducedConstNode = {
    id: "002",
    type: "const",
    value: 0,
    inputs: {
        boolean: [{ id: DEFAULT_CONST_FALSE_PORT.id, oldId: "005" }],
        number: [],
    },
    outputs: {
        boolean: [],
        number: [DEFAULT_CONST_0_PORT],
    },
};

export function transformWholeGraph(graph: FEPreparedGraph): ReducedGraph {
    return {
        main: transformGraph(graph.main),
        macros: Object.fromEntries(graph.macros.map((m) => {
            return [m.id, transformGraph(m.graph)];
        })),
    };
}


type WithValType = {
    valueType: ValueType,
}
function transformNodePorts(ports: (ReducedMacroUsageInputPort & WithValType)[]): ReducedPorts<ReducedMacroUsageInputPort>
function transformNodePorts(ports: (ReducedMacroUsageOutputPort & WithValType)[]): ReducedPorts<ReducedMacroUsageOutputPort>
function transformNodePorts(ports: (ReducedInputPort & WithValType)[]): ReducedPorts<ReducedInputPort>
function transformNodePorts(ports: (ReducedOutputPort & WithValType)[]): ReducedPorts<ReducedOutputPort>

function transformNodePorts<T extends(ReducedMacroUsageOutputPort | ReducedMacroUsageInputPort | ReducedOutputPort | ReducedInputPort) & WithValType>(
    ports: T[],
): ReducedPorts<Omit<T, "valueType">> {
    const res: ReducedPorts<Omit<T, "valueType">> = {
        number: [],
        boolean: [],
    };

    ports.forEach((p) => {
        const { valueType, ...rest } = p;
        res[valueType].push(rest);
    });

    return res;
}


type Inputs = Exclude<FEPreparedGraphNode["inputs"], never[]>
type InputPort = Inputs[number];

function mapInput<T extends InputPort>(p: T, extraNodes: ReducedMathNode[], graph: FEPreparedSubgraph): T extends MacroUsageInputPort ? ReducedMacroUsageInputPort & WithValType : ReducedInputPort & WithValType {

    const fromIds = findConnectionsTo(graph, p.id);
    let id: string;

    switch (fromIds.length) {
        case 0:
            id = p.valueType === "number" ? DEFAULT_CONST_0_PORT.id : DEFAULT_CONST_FALSE_PORT.id;
            break;
        case 1:
            id = fromIds[0]!;
            break;
        default: {
            const node = createPlusNode(fromIds);
            extraNodes.push(node);
            id = node.outputs.number[0]!.id;
        }
    }

    return {
        id,
        oldId: p.id,
        macroPortId: "macroPortId" in p ? p.macroPortId : undefined,
        valueType: p.valueType,
    } as T extends MacroUsageInputPort ? ReducedMacroUsageInputPort & WithValType: ReducedInputPort & WithValType;
}

function transformGraph(graph: FEPreparedSubgraph): ReducedNode[] {
    const extraNodes: ReducedMathNode[] = [];
    const mappedNodes: ReducedNode[] = graph.nodes
        .map((n: FEPreparedGraphNode) => {
            const { x, y, direction, inputs, outputs, ...rest } = n;
            const ins = inputs.map((p) => {
                return mapInput(p, extraNodes, graph);
            });
            return {
                ...rest,
                outputs: transformNodePorts(outputs),
                inputs: transformNodePorts(ins),
            } as ReducedNode;
        });

    const nodes = [...mappedNodes, ...extraNodes, DEFAULT_CONST_0_NODE, DEFAULT_CONST_FALSE_NODE];
    return nodes;
}

function createPlusNode(from: string[]): ReducedMathNode {
    return {
        id: generateId(),
        type: "math",
        op: "plus",
        inputs: {
            number: from.map((f) => {
                return {
                    id: f,
                    oldId: f,
                };
            }),
            boolean: [],
        },
        outputs: {
            boolean: [],
            number: [{
                id: generateId(),
            }],
        },
    };
}

function findConnectionsTo(graph: FEPreparedSubgraph, portId: string): string[] {
    return graph.connections.filter((c) => {
        return c.portIdTo === portId;
    }).map((c) => c.portIdFrom);
}
