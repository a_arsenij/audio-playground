export function findOutput<T extends {type: string}>(nodes: T[]): T {
    const out = nodes.find((n) => n.type === "mainOutput");
    if (!out) {
        throw new Error("Compilation Error: Signal output node not found");
    }
    return out;
}

export function findNodeByPortId<T>(
    pId: string,
    portNodeMap: Record<string, string>,
    nodeMap: Record<string, T>,
): T {

    const nodeId = portNodeMap[pId];
    if (!nodeId) {
        throw new Error(`node id not found, port Id: ${pId}`);
    }
    const node = nodeMap[nodeId];
    if (!node) {
        throw new Error(`cannot find node with id: ${nodeId}`);
    }
    return node;
}


export function getPortNodeMap<
    T extends {id: string; outputs: {id: string}[]}>(nodes: T[]): Record<string, string
> {
    const portNodeMap: Record<string, string> = {};
    nodes.forEach((n) => {
        n.outputs.forEach((p) => {
            portNodeMap[p.id] = n.id;
        });
    });
    return portNodeMap;
}
