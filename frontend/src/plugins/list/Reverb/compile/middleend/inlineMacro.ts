import { Identifiable } from "@audio-playground/lib-common/types/Idetifiable";

import {
    ReducedGraph,
    ReducedMacrosDefinitionInputNode,
    ReducedMacrosDefinitionOutputNode,
    ReducedMacroUsageNode, ReducedMacroUsageOutputPort,
    ReducedNode,
    ReducedOutputPort,
    ReducedPorts,
} from "./transformGraph";

export function inlineMacro(graph: ReducedGraph): NoMacroNode[] {

    return computeMain({
        ...graph,
        macrosCache: {},
    });
}

function pr(prefix: string, s: string): string {
    return `${prefix}${s}`;
}

function addPrefix(macro: NoMacroUseNode[], prefix = ""): NoMacroUseNode[] {
    function updateId<T extends Identifiable>(item: T) {
        return { ...item, id: pr(prefix, item.id) };
    }

    return macro.map((n) => ({
        ...n,
        id: pr(prefix, n.id),
        inputs: {
            number: n.inputs.number.map(updateId),
            boolean: n.inputs.boolean.map(updateId),
        },
        outputs: {
            number: n.outputs.number.map(updateId),
            boolean: n.outputs.boolean.map(updateId),
        },
    }));
}

function connectMacro(
    parentMacro: ReducedNode[],
    macroUseNode: ReducedMacroUsageNode,
    macro: NoMacroUseNode[],
): NoMacroNode[] {
    const output = macro.find((n) => n.type === "macroDefinitionOutput");

    if (!output) {
        throw new Error(`cannot find macro output node in macro ${macroUseNode.macroId}`);
    }
    const prefix = macroUseNode.id;

    const map: Record<string, string> = {};

    const fun = (useInputPort: ReducedMacroUsageOutputPort) => {
        map[pr(prefix, useInputPort.macroPortId)] = useInputPort.id;
    };

    const fun2 = (type: "number" | "boolean") => (useOutPort: ReducedMacroUsageOutputPort) => {
        const oldId = useOutPort.macroPortId;
        const macroOutDefInputPort = output.inputs[type].find((p) => p.oldId === oldId);
        if (!macroOutDefInputPort) {
            throw new Error(`cannot find corresponding macro definitionOutput input-port for old port id: ${oldId}`);
        }
        map[useOutPort.id] = macroOutDefInputPort.id;
    };
    macroUseNode.inputs.number.forEach(fun);
    macroUseNode.inputs.boolean.forEach(fun);
    macroUseNode.outputs.number.forEach(fun2("number"));
    macroUseNode.outputs.boolean.forEach(fun2("boolean"));

    function replaceDeep(id: string) {
        let r = id;
        let newR = map[r];
        while (newR !== undefined) {
            r = newR;
            newR = map[r];
        }
        return r;
    }

    macro.forEach((n) => {
        forEachPort(n.inputs, (p) => {
            p.id = replaceDeep(p.id);
        });
    });

    parentMacro.forEach((n) => {
        forEachPort(n.inputs, (p) => {
            p.id = replaceDeep(p.id);
        });
    });


    return macro.filter(isNotMacroDef);
}

function isNotMacroDef<T extends ReducedNode>(
    n: T,
): n is Exclude<T, ReducedMacrosDefinitionInputNode | ReducedMacrosDefinitionOutputNode> {
    return n.type !== "macroDefinitionInput" && n.type !== "macroDefinitionOutput";
}
function forEachPort(ports: ReducedPorts<ReducedOutputPort>, f: (p: ReducedOutputPort) => void) {
    ports.number.forEach(f);
    ports.boolean.forEach(f);
}

function isMacroUsageNode(n: ReducedNode): n is ReducedMacroUsageNode {
    return n.type === "macroUsage";
}


function computeMain(graph: InlineMacroGraph): NoMacroNode[] {
    const macro = graph.main;
    const noMacroDefs = macro.every(isNotMacroDef);
    if (!noMacroDefs) {
        throw new Error("macro definition node was found in main graph, macro definition nodes are only allowed in macros");
    }
    return computeNodes(macro, graph);

}

function computeNodes(macro: NoMacroDefNode[], graph: InlineMacroGraph): NoMacroNode[];
function computeNodes(macro: ReducedNode[], graph: InlineMacroGraph): NoMacroUseNode[];
function computeNodes(macroOld: ReducedNode[], graph: InlineMacroGraph) {
    const macro = macroOld.slice();
    for (let i = 0; i < macro.length; i++) {
        const n = macro[i]!;
        if (!isMacroUsageNode(n)) {
            continue;
        }
        const id = n.macroId;
        const computedMacro = computeMacroAndCache(graph, id);
        const prefixedMacro = addPrefix(computedMacro, n.id);
        const connected = connectMacro(macro, n, prefixedMacro);
        const deleteCount = 1;
        macro.splice(i, deleteCount, ...connected);
        i = i + connected.length - deleteCount;
    }
    return macro;
}
function computeMacroAndCache(graph: InlineMacroGraph, id: string): NoMacroUseNode[] {
    const cache = graph.macrosCache;
    const cashed = cache[id];
    if (cashed) {
        return cashed;
    }

    const macro = graph.macros[id];
    if (!macro) {
        throw new Error(`macro not found by id: ${id}`);
    }
    const newMacro = computeNodes(macro, graph);

    cache[id] = newMacro;

    return newMacro;
}

type InlineMacroGraph = ReducedGraph & {
    macrosCache: Record<string, NoMacroUseNode[]>,
}
type NoMacroDefNode = Exclude<
    ReducedNode,
    ReducedMacrosDefinitionOutputNode | ReducedMacrosDefinitionInputNode
>
type NoMacroUseNode = Exclude<
    ReducedNode,
    ReducedMacroUsageNode
>;
export type NoMacroNode = Exclude<
    ReducedNode,
    ReducedMacroUsageNode | ReducedMacrosDefinitionInputNode | ReducedMacrosDefinitionOutputNode
>;
