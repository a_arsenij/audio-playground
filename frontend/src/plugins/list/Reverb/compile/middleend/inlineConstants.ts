import { MathOp } from "@audio-playground/lib-common/math/Expr";

import { DelayInputNode, DelayOutputNode, NewDelayNode } from "./transformDelayNodes";
import {
    MathNode,
    ReducedInputPort,
    ReducedOutputPort,
    SignalInputNode,
    SignalOutputNode,
} from "./transformGraph";

export function inlineConstants(nodes: (NewDelayNode | NoConstNode)[]): NoConstNode[] {
    const newNodes: NoConstNode[] = [];

    for (let i = 0; i < nodes.length; i++) {
        const n = nodes[i]!;
        if (n.type === "const") {
            const constPort = n.outputs[0]!.id;
            replacePortWithValue(constPort, n.value, i, nodes);

        } else if (n.type === "math" && isReadyToCalc(n)) {
            const value = calcMathNode(n);
            replacePortWithValue(n.outputs[0]!.id, value, i, nodes);
        } else {
            newNodes.push(n);
        }
    }
    return newNodes;
}
function replacePortWithValue(
    portId: string,
    constValue: number,
    sinceIdx: number,
    nodes: NewDelayNode[],
) {
    for (let j = sinceIdx + 1; j < nodes.length; j++) {
        const n2 = nodes[j]!;
        if (n2.type === "const") {
            continue;
        }
        const newNode: NoConstNode = {
            ...n2,
            inputs: n2.inputs.map((p) => {
                if (p.id === portId) {
                    return {
                        ...p,
                        constValue,
                    };
                }
                return p;
            }),
        };
        nodes[j] = newNode;
    }
}

function op1(args: number[], op: (n: number) => number) {
    const a1 = args.toFixedLength(1);
    return op(a1[0]);
}
function calcMathNode(n: ReadyToCalcMathNode): number {
    const args = n.inputs.map((p) => p.constValue);


    switch (n.op) {
        case "plus":
            return args.reduce((prev, curr) => prev + curr, 0);
        case "minus": {
            const [a0, a1] = args.toFixedLength(2);
            return a0 - a1;
        }
        case "multiply":
            return args.reduce((prev, curr) => prev * curr, 1);
        case "divide":{
            const [a0, a1] = args.toFixedLength(2);
            return a0 / a1;
        }
        case "replaceNaN": {
            const [a0, a1] = args.toFixedLength(2);
            return isNaN(a0) ? a1 : a0;
        }
        case "abs":
            return op1(args, Math.abs);
        case "min":
            return Math.min(...args);
        case "max":
            return Math.max(...args);
        case "sin":
            return op1(args, Math.sin);
        case "cos":
            return op1(args, Math.cos);
        case "tan":
            return op1(args, Math.tan);
        case "exp":
            return op1(args, Math.exp);
        case "pow2":
            return op1(args, (a) => 2 ** a);
        case "sqrt":
            return op1(args, Math.sqrt);
        case "loge":
            return op1(args, Math.log);
        case "log2":
            return op1(args, Math.log2);
        case "log10":
            return op1(args, Math.log10);
        case "mod":{
            const [a0, a1] = args.toFixedLength(2);
            return ((a0 % a1) + a0) % a1;
        }
        case "pow":{
            const [a0, a1] = args.toFixedLength(2);
            return a0 ** a1;
        }
        case "heaviside":
        {
            const [a0] = args.toFixedLength(1);
            return a0 > 0 ? 1 : 0;
        }
    }
}

function isReadyToCalc(n: NoConstMathNode): n is ReadyToCalcMathNode {
    return n.inputs.every((p) => {
        return "constValue" in p && p.constValue !== undefined;
    });
}

type ReadyToCalcMathNode = {
    id: string,
    inputs: ConstInputPort[],
    outputs: ReducedOutputPort[],
    type: "math",
    op: MathOp,
}


export type ConstInputPort = ReducedInputPort & {
    constValue: number,
}

type NoConstBase = {
    id: string,
    inputs: (ReducedInputPort | ConstInputPort)[],
    outputs: ReducedOutputPort[],
}

export type NoConstDelayInputNode = NoConstBase & DelayInputNode
export type NoConstDelayOutputNode = NoConstBase & DelayOutputNode

export type NoConstMathNode = NoConstBase & MathNode

export type NoConstSignalInputNode = NoConstBase & SignalInputNode
export type NoConstSignalOutputNode = NoConstBase & SignalOutputNode


export type NoConstNode =
    | NoConstMathNode
    | NoConstSignalInputNode
    | NoConstSignalOutputNode
    | NoConstDelayInputNode
    | NoConstDelayOutputNode;


