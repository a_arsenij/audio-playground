import { NoBoolNode } from "./turnOffNodes";
import { findOutput, getPortNodeMap } from "./utils";

export function removeUnreachable(graph: NoBoolNode[]): NoBoolNode[] {
    const res: NoBoolNode[] = [];
    const outNode = findOutput(graph);
    const portToNodeMap = getPortNodeMap(graph);
    const nodeMap = Object.fromEntries(graph.map((n) => [n.id, n]));

    const visited = new Set<NoBoolNode>();

    const queue = [outNode];
    visited.add(outNode);

    while (queue.length > 0) {
        const n = queue.pop()!;
        res.push(n);

        n.inputs.forEach((p) => {
            const nId = portToNodeMap[p.id];
            if (nId === undefined) {
                console.log(n);
                throw new Error(`cannot find node for port id: ${p.id}`);
            }
            const node = nodeMap[nId];
            if (!node) {
                throw new Error(`cannot find node by id: ${nId}`);
            }
            if (visited.has(node)) {
                return;
            }
            queue.push(node);
            visited.add(node);
        });
    }
    return res;
}
