import { NewDelayNode } from "./transformDelayNodes";
import { findNodeByPortId, findOutput, getPortNodeMap } from "./utils";

export function orderGraph(nodes: NewDelayNode[]): NewDelayNode[] {
    const result: NewDelayNode[] = [];
    const portNodeMap = getPortNodeMap(nodes);
    const nodeMap = Object.fromEntries(nodes.map((n) => [n.id, n]));

    const signalOutput = findOutput(nodes);
    if (!signalOutput) {
        throw new Error(" no signal output found");
    }
    const delayOutputs = nodes.filter((n) => n.type === "delayOutput");
    const rootList = [signalOutput, ...delayOutputs];
    const colorMap: WeakMap<NewDelayNode, 1 | 2> = new WeakMap();

    function deepSearch(curr: NewDelayNode) {
        const col = colorMap.get(curr);
        if (col === 2) {
            return;
        }
        if (col === 1) {
            throw new Error("Compilation Error: cyclic graph");
        }
        colorMap.set(curr, 1);
        curr.inputs
            .map((p) => findNodeByPortId(p.id, portNodeMap, nodeMap))
            .forEach((c) => {
                deepSearch(c);
            });
        colorMap.set(curr, 2);
        result.push(curr);
    }

    rootList.forEach((r) => deepSearch(r));

    return result;
}
