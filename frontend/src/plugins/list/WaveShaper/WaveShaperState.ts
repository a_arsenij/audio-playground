import {
    createPiecewiseFunction,
    createPiecewiseFunctionData,
    PiecewiseFunctionData,
    RealFunction,
} from "../../../utils/SvgGraphFunctions";

export type WaveShaperState = {
    data: PiecewiseFunctionData,
    function: RealFunction,
}

export function createWaveShaperState(): WaveShaperState {
    const data = createPiecewiseFunctionData();
    return {
        data,
        function: createPiecewiseFunction(data),
    };
}
