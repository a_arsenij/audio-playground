import { RenderBufferArg } from "../../../../types/Plugin";
import { arePiecewiseFunctionDataEquals, createPiecewiseFunction } from "../../../../utils/SvgGraphFunctions";
import {
    WaveShaperSettings,
} from "../WaveShaperSettings";
import {
    WaveShaperState,
} from "../WaveShaperState";

export function waveShaperRender({
    input,
    output,
    pluginSettings,
    pluginState,
    bufferLength,
}: RenderBufferArg<WaveShaperSettings, WaveShaperState>) {

    if (!arePiecewiseFunctionDataEquals(pluginState.data, pluginSettings.function)) {
        pluginState.data = pluginSettings.function;
        pluginState.function = createPiecewiseFunction(pluginSettings.function);
    }

    function apply(inValue: number): number {
        return Math.sign(inValue) * pluginState.function(Math.abs(inValue));
    }

    for (let i = 0; i < bufferLength; i++) {
        output[0]![i] = apply(input[0]![i]!);
        output[1]![i] = apply(input[1]![i]!);
    }

}
