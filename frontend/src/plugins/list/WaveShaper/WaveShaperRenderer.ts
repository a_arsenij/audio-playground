import { PluginRenderer } from "../../../types/PluginRenderer";
import { waveShaperRender } from "./render/WaveShaperRender";
import { destroyWaveShaper } from "./WaveShaperDestroy";
import { createWaveShaperSettings, WaveShaperSettings } from "./WaveShaperSettings";
import { createWaveShaperState } from "./WaveShaperState";

export const WaveShaperRenderer: PluginRenderer<WaveShaperSettings, any> = {

    name: "WaveShaper",
    renderBuffer: waveShaperRender,
    createSettings: createWaveShaperSettings,
    createState: createWaveShaperState,
    destroy: destroyWaveShaper,

};
