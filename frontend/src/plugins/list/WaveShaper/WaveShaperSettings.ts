import { createDefaultEffectPluginPorts, PluginCommonSettings } from "../../../types/Plugin";
import { createPiecewiseFunctionData, PiecewiseFunctionData } from "../../../utils/SvgGraphFunctions";

export type WaveShaperSettings =
    & PluginCommonSettings
    & {
        function: PiecewiseFunctionData,
    }
;

export function createWaveShaperSettings(): WaveShaperSettings {
    return {
        function: createPiecewiseFunctionData(),
        ...createDefaultEffectPluginPorts(),
    };
}
