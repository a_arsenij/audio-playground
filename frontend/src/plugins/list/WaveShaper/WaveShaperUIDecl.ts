import { Svg32PluginsWaveShaper } from "../../../components/Common/GeneratedIcons/32/Plugins/WaveShaper";
import { PluginUIDecl } from "../../../types/PluginUIDecl";
import { WaveShaperUI } from "./ui/WaveShaperUI";
import { WaveShaperSettings } from "./WaveShaperSettings";

export const WaveShaperUIDecl: PluginUIDecl<WaveShaperSettings> = {

    name: "WaveShaper",
    UI: WaveShaperUI,
    Icon: Svg32PluginsWaveShaper,

};
