import { useByKey } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import {
    PiecewiseGraphInteractive,
} from "../../../../components/Common/PiecewiseGraphInteractive/PiecewiseGraphInteractive";
import { PluginUIProps } from "../../../../types/Plugin";
import {
    WaveShaperSettings,
} from "../WaveShaperSettings";
import styles from "./WaveShaperUI.module.css";

export const WaveShaperUI = function({
    pluginSettings,
    setPluginSettings,
}: PluginUIProps<WaveShaperSettings>) {

    const [func, setFunc] = useByKey(
        pluginSettings,
        setPluginSettings,
        "function",
    );

    const [pieceData, setPieceData] = useByKey(
        func,
        setFunc,
        "pieceData",
    );
    const [points, setPoints] = useByKey(
        func,
        setFunc,
        "points",
    );

    return <div className={styles.waveShaperUI}>

        <PiecewiseGraphInteractive
            points={points}
            pieceData={pieceData}
            setPieceData={setPieceData}
            setPoints={setPoints}
        />

    </div>;
};
