import { applySetState } from "@audio-playground/lib-frontend/std/ApplySetState";
import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { SetStateValue } from "@audio-playground/lib-frontend/std/SetState";
import { noteToTitle, noteToType } from "@audio-playground/lib-sound/NoteFrequency";
import React, { useCallback } from "react";

import { Checkbox } from "../../../../components/Common/Checkbox/Checkbox";
import { Svg18CommonCheck } from "../../../../components/Common/GeneratedIcons/18/Common/Check";
import { Svg18CommonCross } from "../../../../components/Common/GeneratedIcons/18/Common/Cross";
import { Svg18SignPlus } from "../../../../components/Common/GeneratedIcons/18/Sign/Plus";
import { Knob } from "../../../../components/Common/Knob/Knob";
import { LabeledContainer } from "../../../../components/Common/LabeledContainer/LabeledContainer";
import { PluginUIProps } from "../../../../types/Plugin";
import { ROOT_KEY } from "../../../../utils/KeyboardToNotes";
import { openAudioFile } from "../../../../utils/OpenFile";
import {
    audioBufferToSimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSamples,
    SimpleSamplerSettings,
} from "../SimpleSamplerSettings";
import styles from "./SimpleSamplerUI.module.css";

export const SimpleSamplerUI = function({
    pluginSettings,
    setPluginSettings,
    allocator,
}: PluginUIProps<SimpleSamplerSettings>) {

    const setSample = useCallback((idx: number, change: SetStateValue<SimpleSamplerSample>) => {

        setPluginSettings((prev) => ({
            ...prev,
            samples: prev.samples.map((s, i) =>
                idx === i
                    ? applySetState(change, s)
                    : s,
            ) as SimpleSamplerSamples,
        }));

    }, [setPluginSettings]);

    const loadSample = (idx: number) => {
        openAudioFile().then((buffer) => {
            setSample(idx, (prev) => audioBufferToSimpleSamplerSample(
                prev.volume,
                buffer,
                allocator,
            ));
        });
    };

    const samples = pluginSettings.samples;
    const playTilEnd = pluginSettings.playTilEnd;
    const setPlayTilEnd = (value: boolean) => {
        setPluginSettings({ ...pluginSettings, playTilEnd: value });
    };

    const setSampleVolume = (idx: number, value: number) => {
        setSample(idx, (s) => ({
            ...s,
            volume: value,
        }));
    };

    return <div className={styles.simpleSamplerUI}>
        <LabeledContainer label={"Samples"}>
            <div className={styles.samples}>
                {[...Array(12)].map((_, idx) => {
                    const sample = samples[idx]!;
                    const note = ROOT_KEY + idx;
                    const type = noteToType(note);
                    return <div
                        key={idx}
                        className={clsx(
                            styles.sample,
                            type === "WHITE" && styles.white,
                            type === "BLACK" && styles.black,
                        )}
                    >
                        <div className={styles.sampleTitle}>
                            {noteToTitle(note)}
                        </div>
                        <div
                            className={styles.loadedState}
                            title={sample.buffer ? "Sample selected" : "No sample selected"}
                        >
                            {
                                sample.buffer
                                    ? <Svg18CommonCheck/>
                                    : <Svg18CommonCross/>
                            }
                        </div>
                        <button
                            className={styles.pickSampleButton}
                            title={"Open file"}
                            onClick={() => loadSample(idx)}
                        >
                            <Svg18SignPlus/>
                        </button>
                        <div className={styles.volume}>
                            Volume:
                            <Knob
                                func={"linear"}
                                neutralValue={0}
                                defaultValue={1}
                                minValue={0}
                                maxValue={2}
                                value={sample.volume}
                                setValue={(v) => setSampleVolume(idx, v)}
                            />
                        </div>

                    </div>;
                })}
            </div>
        </LabeledContainer>
        <LabeledContainer label={"Settings"}>
            <Checkbox
                label={"Play til end"}
                value={playTilEnd}
                setValue={(v) => setPlayTilEnd(v)}
            />
        </LabeledContainer>
    </div>;
};
