import { RenderBufferArg } from "../../../../types/Plugin";
import { addToBuffers } from "../../../../utils/AddToBuffer";
import {
    SimpleSamplerSettings,
} from "../SimpleSamplerSettings";
import { SimpleSamplerState } from "../SimpleSamplerState";

export function simpleSamplerRender(
    {
        input,
        output,
    }: RenderBufferArg<SimpleSamplerSettings, SimpleSamplerState>,
) {

    /*
    const samples = pluginSettings.samples;
    const playTilEnd = pluginSettings.playTilEnd;
    const notesToPlay = handleReleasedNotes(
        timeSample,
        notes,
        pluginState.releasedNotes,
        (note) => {
            if (!pluginSettings.playTilEnd) {
                return 0;
            }
            const sample = samples[note.key - ROOT_KEY];
            if (!sample) {
                return 0;
            }
            const buffer = sample.buffer;
            if (!buffer) {
                return 0;
            }
            return buffer[0]!.size * sampleRate / sample.sampleRate;
        },
    );

    notesToPlay.map((note) => {
        const idx = note.key - ROOT_KEY;
        const sample = samples[idx];
        if (!sample) {
            return;
        }
        const blobRefs = sample.buffer;
        if (!blobRefs) {
            return;
        }
        const buffers = blobRefs.map((r) => blobRefToF64(r, sharedBuffer));
        const sampleLength = blobRefs[0].size * sampleRate / sample.sampleRate;
        const minIdx = Math.max(0, note.startedAtSample - timeSample);
        const maxIdx = Math.min(
            bufferLength - 1,
            (
                playTilEnd
                    ? note.startedAtSample + sampleLength
                    : (note.endedAtSample ?? Number.MAX_SAFE_INTEGER)
            ) - timeSample,
        );

        for (let idx = minIdx; idx <= maxIdx; idx++) {
            const timeSampleSamples = Math.round(
                (timeSample - note.startedAtSample + idx) *
                sample.sampleRate /
                sampleRate,
            );
            if (timeSampleSamples < blobRefs[0].size) {
                output[0]![idx] += sample.volume * buffers[0 % buffers.length]![timeSampleSamples]!;
                output[1]![idx] += sample.volume * buffers[1 % buffers.length]![timeSampleSamples]!;
            }
        }
        return playTilEnd
            ? note.startedAtSample + sampleLength <= timeSample + output.length
            : note.endedAtSample !== undefined;
    }); */

    addToBuffers(input, output);
}
