import { Svg32PluginsSimpleSampler } from "../../../components/Common/GeneratedIcons/32/Plugins/SimpleSampler";
import { PluginUIDecl } from "../../../types/PluginUIDecl";
import { SimpleSamplerSettings } from "./SimpleSamplerSettings";
import { SimpleSamplerUI } from "./ui/SimpleSamplerUI";

export const SimpleSamplerUIDecl: PluginUIDecl<SimpleSamplerSettings> = {

    name: "SimpleSampler",
    UI: SimpleSamplerUI,
    Icon: Svg32PluginsSimpleSampler,

};
