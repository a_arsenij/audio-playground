import { ArrayBufferAllocator } from "@audio-playground/lib-common/std/ArrayBufferAllocator";
import { BlobRef } from "@audio-playground/lib-common/std/BlobRef";

import { createDefaultInstrumentPluginPorts, PluginCommonSettings } from "../../../types/Plugin";

export type SimpleSamplerSample = {
    buffer: [BlobRef] | [BlobRef, BlobRef] | undefined,
    sampleRate: number,
    volume: number,
}

export type SimpleSamplerSamples = [
    SimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSample,
    SimpleSamplerSample,
]

export type SimpleSamplerSettings =
    & PluginCommonSettings
    & {
        samples: SimpleSamplerSamples,
        playTilEnd: boolean,
    }
;

export function createSimpleSamplerEmptySample(): SimpleSamplerSample {
    return {
        buffer: undefined,
        sampleRate: 44100,
        volume: 1,
    };
}

export function createSimpleSamplerSettings(): SimpleSamplerSettings {
    return {
        samples: [...Array(12)].map(() => ({
            buffer: undefined,
            sampleRate: 44100,
            volume: 1,
        })) as SimpleSamplerSamples,
        playTilEnd: true,
        ...createDefaultInstrumentPluginPorts(),
    };
}

export function audioBufferToSimpleSamplerSample(
    volume: number,
    buffer: AudioBuffer,
    allocator: ArrayBufferAllocator,
): SimpleSamplerSample {
    const left = buffer.getChannelData(0);
    const right = buffer.numberOfChannels >= 2
        ? buffer.getChannelData(1)
        : left;
    const leftRef = allocator.put(left.buffer, "Sample loaded in SimpleSampler");
    const rightRef = allocator.put(right.buffer, "Sample loaded in SimpleSampler");
    return {
        volume,
        buffer: [
            leftRef,
            rightRef,
        ],
        sampleRate: buffer.sampleRate,
    };
}
