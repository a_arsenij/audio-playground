import { PluginRenderer } from "../../../types/PluginRenderer";
import { simpleSamplerRender } from "./render/SimpleSamplerRender";
import { createSimpleSamplerSettings, SimpleSamplerSettings } from "./SimpleSamplerSettings";
import { createSimpleSamplerState, SimpleSamplerState } from "./SimpleSamplerState";

export const SimpleSamplerRenderer: PluginRenderer<SimpleSamplerSettings, SimpleSamplerState> = {

    name: "SimpleSampler",
    renderBuffer: simpleSamplerRender,
    createSettings: createSimpleSamplerSettings,
    createState: createSimpleSamplerState,
    destroy: () => {},

};
