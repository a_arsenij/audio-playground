import {
    ArrayBufferAllocator,
    createArrayBufferAllocator,
} from "@audio-playground/lib-common/std/ArrayBufferAllocator";
import { getAllBlobRefs } from "@audio-playground/lib-common/std/BlobRef";
import { ui8toBuffer } from "@audio-playground/lib-common/std/Buffers";
import { createCompletablePromise } from "@audio-playground/lib-common/std/CompletablePromise";
import { createDebouncer } from "@audio-playground/lib-common/std/Debouncer";
import {
    createStoredObservable,
    Observable,
    StoredObservable,
} from "@audio-playground/lib-common/std/Observable";
import { useObserved } from "@audio-playground/lib-frontend/hooks/useObserved";
import React from "react";
import ReactDOM from "react-dom/client";

import { UI } from "./components/App/UI/UI";
import { DEFAULT_SAMPLE_RATE, EMPTY_PROJECT } from "./engine/DefaultProject";
import { loadProjectState, saveProjectState } from "./engine/Persist";
import { AUDIO_WORKER_NAME } from "./threads/DumbWorklet";
import { Loop } from "./types/Loop";
import {
    BufferRenderedMessage,
    MasterToUiMessage,
    UiToDumbWorkletMessage,
    UiToMasterMessage,
} from "./types/Messages";
import { Project } from "./types/project/Project";
import { fromBase64, toBase64 } from "./utils/base64";
import { BUNDLE_FILENAME, MASTER_WORKER_NAME } from "./utils/Env";
import { handle } from "./utils/OnPort";

export async function initApp() {

    const master = new Worker(
        BUNDLE_FILENAME,
        { name: MASTER_WORKER_NAME },
    );

    const onBufferRendered = (msg: BufferRenderedMessage) => {
        currentTimeBars.set(msg.timeBars);
        channelsLevels.set((prev) => {
            if (prev.length === 0 && msg.channelsLevels.length === 0) {
                return prev;
            }
            return msg.channelsLevels;
        });
    };
    const p = createCompletablePromise();
    handle<MasterToUiMessage>(
        master,
        {
            bufferRendered: onBufferRendered,
            iAmReady: () => p.resolve(undefined),
        },
    );
    await p;
    const masterBuffer = new SharedArrayBuffer(
        2 * 4 +
        EMPTY_PROJECT.bufferLength * 4 * 2,
    );

    function sendToMaster(message: UiToMasterMessage) {
        master.postMessage(
            message,
        );
    }

    sendToMaster({
        type: "setMasterBufferOnly",
        masterBuffer,
    });

    const toListen = [
        "mousedown",
        "keypress",
    ];

    function onHappened() {
        for (const t of toListen) {
            window.removeEventListener(t, onHappened);
        }
        const audioContext = new AudioContext({
            sampleRate: DEFAULT_SAMPLE_RATE,
        });
        audioContext.audioWorklet.addModule(BUNDLE_FILENAME).then(() => {
            const audioProcessor = new AudioWorkletNode(
                audioContext,
                AUDIO_WORKER_NAME,
                {
                    numberOfOutputs: 1,
                    outputChannelCount: [2],
                },
            );

            function sendToDumbWorklet(message: UiToDumbWorkletMessage) {
                audioProcessor.port.postMessage(
                    message,
                );
            }

            sendToDumbWorklet({
                type: "setMasterBuffer",
                masterBuffer,
                bufferLength: project.get().bufferLength,
                sampleRate: project.get().sampleRate,
            });
            audioProcessor.connect(audioContext.destination);
            return audioProcessor.port;
        });
    }

    for (const t of toListen) {
        window.addEventListener(t, onHappened);
    }


    const currentTimeBars = createStoredObservable<number | undefined>(undefined);
    const channelsLevels = createStoredObservable<
        number[][]
    >([]);

    const allocator = createArrayBufferAllocator();
    const project = createStoredObservable<Project>(EMPTY_PROJECT);

    allocator.onReallocateBuffer((buffer) => {
        sendToMaster({
            type: "setSharedMemory",
            memory: buffer,
        });
    });

    (() => {
        const stored = localStorage.getItem("project");
        if (stored !== null) {
            const bytes = fromBase64(stored);
            loadProjectState(
                ui8toBuffer(bytes),
                project.set,
                allocator,
            );
        }
    })();
    const saveDebouncer = createDebouncer(1000);
    project.subscribe((p) => {
        const allBlobRefs = getAllBlobRefs(p);
        allocator.clean(allBlobRefs);
        saveDebouncer(() => {
            const buffer = saveProjectState(
                p,
                allocator,
            );
            localStorage.setItem(
                "project",
                toBase64(buffer),
            );
        });
    });
    const loop = createStoredObservable<Loop>({
        type: "playlist",
        borders: {
            timeStartBar: 0,
            timeEndBar: 8,
        },
    });

    sendToMaster({
        type: "setProject",
        project: project.get(),
    });
    sendToMaster({
        type: "setLoop",
        loop: loop.get(),
    });

    const isPlaying = currentTimeBars.map(
        (v) => v !== undefined,
        "hard",
    );


    function onKeyPress(note: number) {
        sendToMaster({
            type: "pressKey",
            key: note,
        });
    }

    function onKeyRelease(note: number) {
        sendToMaster({
            type: "releaseKey",
            key: note,
        });
    }

    function play() {
        sendToMaster({ type: "play" });
    }

    function stop() {
        sendToMaster({ type: "stop" });
    }

    loop.subscribe((v) => sendToMaster({
        type: "setLoop",
        loop: v,
    }));

    project.subscribe((v) => {
        sendToMaster({
            type: "setProject",
            project: v,
        });
    });

    ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
        <App
            play={play}
            stop={stop}
            isPlaying={isPlaying}
            allocator={allocator}
            project={project}
            currentTimeBars={currentTimeBars}
            onKeyPress={onKeyPress}
            onKeyRelease={onKeyRelease}
            loop={loop}
            channelsLevels={channelsLevels}
        />,
    );
}

type AppProps = {
    play: () => void,
    stop: () => void,
    isPlaying: Observable<boolean>,
    project: StoredObservable<Project>,
    allocator: ArrayBufferAllocator,
    currentTimeBars: Observable<number | undefined>,
    onKeyPress: (note: number) => void,
    onKeyRelease: (note: number) => void,
    loop: StoredObservable<Loop>,
    channelsLevels: StoredObservable<number[][]>,
}

const App = (props: AppProps) => {
    const isPlaying = useObserved(props.isPlaying);
    const project = useObserved(props.project);
    const currentTimeBars = useObserved(props.currentTimeBars);
    const loop = useObserved(props.loop);
    const channelsLevels = useObserved(props.channelsLevels);
    return <UI
        play={props.play}
        stop={props.stop}
        isPlaying={isPlaying}
        project={project}
        setProject={props.project.set}
        allocator={props.allocator}
        currentTimeBars={currentTimeBars}
        onKeyPress={props.onKeyPress}
        onKeyRelease={props.onKeyRelease}
        loop={loop}
        setLoop={props.loop.set}
        channelsLevels={channelsLevels}
    />;
};
