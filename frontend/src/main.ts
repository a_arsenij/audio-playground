import "./pallet.css";
import "./theme.css";
import "./reset.css";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { initApp } from "./App";
import { initDumbWorklet } from "./threads/DumbWorklet";
import { initMaster } from "./threads/MasterWorker";
import { isAudioWorklet, isMasterWorker, isUIThread } from "./utils/Env";

monkeyPatch();
switch (true) {
    case isUIThread(): {
        console.log("Init app");
        initApp();
        break;
    }
    case isAudioWorklet(): {
        console.log("Init dumb worklet");
        initDumbWorklet();
        break;
    }
    case isMasterWorker(): {
        console.log("Init master worker");
        initMaster();
        break;
    }
    default:
        throw new Error("???");
}
