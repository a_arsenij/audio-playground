import { isElementInput } from "@audio-playground/lib-frontend/std/SomeInputIsFocused";
import React from "react";

const KEYBOARD = [
    "a", "w", "s", "e", "d", "f", "t", "g", "y", "h", "u", "j", "k", "o", "l", "p", ";", "'", "]",
];

export const ROOT_KEY = 48;
export const eventToNote = (e: React.KeyboardEvent) => {
    // @ts-ignore
    if (isElementInput(e.target)) {
        return -1;
    }
    if (e.repeat) {
        return -1;
    }
    const k = e.key;
    const res = KEYBOARD.indexOf(k);
    if (res === -1) {
        return -1;
    }
    return res + ROOT_KEY;
};
