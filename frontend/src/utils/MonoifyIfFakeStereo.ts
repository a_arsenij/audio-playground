import { arraysAreEqual } from "@audio-playground/lib-common/std/Array";

export function monoifyIfFakeStereo(
    signal: [number[]] | [number[], number[]],
): [number[]] | [number[], number[]] {
    if (signal.length === 1) {
        return signal;
    }
    if (arraysAreEqual(
        signal[0], signal[1],
    )) {
        return [signal[0]];
    }
    return signal;
}
