import { useLocalStorageState } from "@audio-playground/lib-frontend/hooks/useLocalStorageState";

const DEBUG_DEFAULT_VALUE = "off";

const DEBUG_KEY = "debug";

export function useDebug() {
    return useLocalStorageState<"off" | "on">(
        DEBUG_KEY,
        DEBUG_DEFAULT_VALUE,
    );
}
