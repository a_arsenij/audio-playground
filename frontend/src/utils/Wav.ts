export async function renderWav(
    fileHandle: FileSystemFileHandle,
    getNextSample: () => Promise<"cancelled" | "finished" | [Float64Array, Float64Array]>,
    bufferLength: number,
    sampleRate: number,
) {

    // http://soundfile.sapp.org/doc/WaveFormat/

    const writer = await fileHandle.createWritable();

    const HEADER_SIZE = 44;

    let size = HEADER_SIZE;
    writer.truncate(size);

    const RANGE_MIN = -32768;
    const RANGE_MAX = 32767;
    const RANGE = (RANGE_MAX - RANGE_MIN);
    function toRange(v: number): number {
        const normalized = Math.max(-1, Math.min(1, v)) / 2 + 0.5;
        const scaled = normalized * RANGE;
        return Math.round(scaled + RANGE_MIN);
    }

    const BYTES_PER_SAMPLE = 2;
    const CHANNELS = 2;

    const BUFFER_SIZE_BYTES = bufferLength * CHANNELS * BYTES_PER_SAMPLE;
    const buffer = new ArrayBuffer(BUFFER_SIZE_BYTES);
    const view = new DataView(buffer);

    while (true) {
        const data = await getNextSample();
        if (data === "cancelled") {
            await writer.close();
            return;
        }
        if (data === "finished") {

            const headersBuffer = new ArrayBuffer(HEADER_SIZE);
            const headersView = new DataView(headersBuffer);

            headersView.setUint32(0, 0x52494646, false);
            headersView.setUint32(4, size - 8, true);
            headersView.setUint32(8, 0x57415645, false);

            headersView.setUint32(12, 0x666d7420, false);
            headersView.setUint32(16, 0x10000000, false);
            headersView.setUint16(20, 0x0100, false);
            headersView.setUint16(22, 0x0200, false);

            headersView.setUint32(24, sampleRate, true);
            headersView.setUint32(28, sampleRate * BYTES_PER_SAMPLE * CHANNELS, true);
            headersView.setUint16(32, BYTES_PER_SAMPLE * CHANNELS, true);
            headersView.setUint16(34, BYTES_PER_SAMPLE * 8, true);

            headersView.setUint32(36, 0x64617461, false);
            headersView.setUint32(40, (size - HEADER_SIZE), true);

            await writer.write({
                type: "write",
                data: headersBuffer,
                position: 0,
            });

            await writer.close();
            return;
        }

        for (let i = 0; i < bufferLength; i++) {
            view.setInt16(
                i * BYTES_PER_SAMPLE * CHANNELS,
                toRange(data[0][i]!),
                true,
            );
            view.setInt16(
                i * BYTES_PER_SAMPLE * CHANNELS + BYTES_PER_SAMPLE,
                toRange(data[1][i]!),
                true,
            );
        }

        const prevOffset = size;
        size += BUFFER_SIZE_BYTES;
        await writer.truncate(size);
        await writer.write({
            type: "write",
            data: buffer,
            position: prevOffset,
        });

    }
}
