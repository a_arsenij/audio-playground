/*
import {KeyEvent} from "./KeyEvent";

export function handleReleasedNotes(
    timeSample: number,
    notes: KeyEvent[],
    releasedNotes: Map<string, EndedRenderableNote>,
    releaseTimeSamples: (note: EndedRenderableNote) => number,
) {
    [...releasedNotes.entries()].forEach(([id, note]) => {
        if (note.endedAtSample + releaseTimeSamples(note) < timeSample) {
            releasedNotes.delete(id);
        }
    });

    const notesIds = new Set(notes.map((n) => n.id));

    const notesToPlay = [...notes];
    releasedNotes.forEach((n) => {
        if (!notesIds.has(n.id)) {
            notesToPlay.push(n);
        }
    });

    notes.forEach((note) => {
        if (note.endedAtSample !== undefined && !releasedNotes.has(note.id)) {
            releasedNotes.set(note.id, note as EndedRenderableNote);
        }
    });

    return notesToPlay;
}
*/
