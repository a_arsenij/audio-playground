

export function toBase64(
    data: ArrayBuffer,
): string {
    return btoa(
        [...new Uint8Array(data)]
            .map((c) => String.fromCharCode(c))
            .join(""),
    );
}

export function fromBase64(
    text: string,
): number[] {
    return atob(text)
        .split("")
        .map((c) => c.charCodeAt(0));
}
