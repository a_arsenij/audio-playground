

export function handle<T extends {type: string}>(
    onMessageable: {
        onmessage: MessagePort["onmessage"] | Worker["onmessage"] | DedicatedWorkerGlobalScope["onmessage"],
    },
    handle: {
        [k in T["type"]]: (event: T & {type: k}) => void
    },
) {
    onMessageable.onmessage = (e) => {
        const msg = e.data as T;
        // @ts-ignore
        handle[msg.type](msg);
    };
}
