import { PooledArray } from "@audio-playground/lib-common/std/ArraysPool";


export function addToBuffer(
    src: PooledArray,
    dst: PooledArray,
) {
    for (let i = 0; i < src.length; i++) {
        dst[i]! += src[i]!;
    }
}

export function addToBuffers(
    src: PooledArray[],
    dst: PooledArray[],
) {
    for (let i = 0; i < src.length; i++) {
        addToBuffer(src[i]!, dst[i]!);
    }
}
