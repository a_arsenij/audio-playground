


export type ArrayWriter = {
    write: (data: ArrayBuffer) => void,
    get: () => ArrayBuffer,
}


export function createArrayWriter(): ArrayWriter {
    const bytes: number[] = [];
    return {
        write: (data: ArrayBuffer) => {
            new Uint8Array(data).forEach((v) => {
                bytes.push(v);
            });
        },
        get: () => {
            return new Uint8Array(bytes);
        },
    };
}
