type Coords = { clientX: number; clientY: number };
export function getSvgCoordinates(e: Coords, svg: SVGSVGElement | null) {

    if (!svg) {
        return new DOMPoint(0, 0);
    }
    const pt = new DOMPoint(e.clientX, e.clientY);

    const ctm = svg.getScreenCTM();
    if (!ctm) {
        return new DOMPoint(0, 0);
    }
    return pt.matrixTransform(ctm.inverse());
}
