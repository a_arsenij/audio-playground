
import { applySetState } from "@audio-playground/lib-frontend/std/ApplySetState";
import { SetStateValue } from "@audio-playground/lib-frontend/std/SetState";

import { ChannelPort, PortsRoute } from "../../types/project/Channel";
import { Project } from "../../types/project/Project";


function check(
    channelInputPorts: ChannelPort[],
    channelOutputPorts: ChannelPort[],
    pluginsInputPorts: ChannelPort[],
    pluginsOutputPorts: ChannelPort[],
    routes: PortsRoute[],
    desc?: string,
) {
    const channelSignalInPorts = channelInputPorts.ids().toSet();
    const channelSignalOutPorts = channelOutputPorts.ids().toSet();
    const pluginsSignalInPorts = pluginsInputPorts.ids().toSet();
    const pluginsSignalOutPorts = pluginsOutputPorts.ids().toSet();
    for (const r of routes) {
        if (
            !channelSignalInPorts.has(r.from) &&
            !pluginsSignalOutPorts.has(r.from)
        ) {
            console.warn(`[${desc}] Orphan plugin route detected. No channel in/plugin out port with id=${r.from}`);
        }
        if (
            !channelSignalOutPorts.has(r.to) &&
            !pluginsSignalInPorts.has(r.to)
        ) {
            console.warn(`[${desc}] Orphan plugin route detected. No channel out/plugin in port with id=${r.to}`);
        }
    }
}

export function checkNoOrphanRoutesInProject(
    project: Project,
    desc?: string,
): Project {
    const channelsSignalInPorts = project.channels.flatMap((c) => c.inputSignalPorts.ids()).toSet();
    const channelsSignalOutPorts = project.channels.flatMap((c) => c.outputSignalPorts.ids()).toSet();
    for (const r of project.channelsRouting) {
        if (!channelsSignalOutPorts.has(r.from)) {
            console.warn(`[${desc}] Orphan channel route detected. No channel out port with id=${r.from}`);
        }
        if (!channelsSignalInPorts.has(r.to)) {
            console.warn(`[${desc}] Orphan channel route detected. No channel in port with id=${r.from}`);
        }
    }
    for (const channel of project.channels) {
        check(
            channel.inputSignalPorts,
            channel.outputSignalPorts,
            channel.plugins.flatMap((p) => p.settings.inputSignalChannels),
            channel.plugins.flatMap((p) => p.settings.outputSignalChannels),
            channel.signalPluginsRouting,
            desc,
        );
        check(
            channel.inputMidiPorts,
            channel.outputMidiPorts,
            channel.plugins.flatMap((p) => p.settings.inputMidiChannels),
            channel.plugins.flatMap((p) => p.settings.outputMidiChannels),
            channel.midiPluginsRouting,
            desc,
        );
    }
    return project;
}

export function checkNoOrphanRoutes(
    project: SetStateValue<Project>,
): SetStateValue<Project> {

    return (prev: Project) => {
        checkNoOrphanRoutesInProject(prev, "Before mutation");
        const newProj = applySetState(project, prev);
        checkNoOrphanRoutesInProject(newProj, "After mutation");
        return newProj;
    };

}
