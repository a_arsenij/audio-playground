import { arrayOfNItems } from "@audio-playground/lib-common/std/Array";

import { ChannelPort, PortsRoute } from "../../types/project/Channel";

function commutateOneToMany(
    from: ChannelPort[],
    to: ChannelPort[],
) {
    return arrayOfNItems(
        to.length,
        (i) => ({
            from: from[0]!.id,
            to: to[i]!.id,
        }),
    );
}

function commutateManyToOne(
    from: ChannelPort[],
    to: ChannelPort[],
) {
    return arrayOfNItems(
        from.length,
        (i) => ({
            from: from[i]!.id,
            to: to[0]!.id,
        }),
    );
}

function commutateParallel(
    from: ChannelPort[],
    to: ChannelPort[],
): PortsRoute[] {
    return arrayOfNItems(
        Math.min(from.length, to.length),
        (i) => ({
            from: from[i]!.id,
            to: to[i]!.id,
        }),
    );
}

export function autoCommutatePorts(
    from: ChannelPort[],
    to: ChannelPort[],
): PortsRoute[] {
    if (from.length === 0 || to.length === 0) {
        return [];
    }
    if (from.length === 1) {
        return commutateOneToMany(from, to);
    }
    if (to.length === 1) {
        return commutateManyToOne(from, to);
    }
    return commutateParallel(from, to);
}

export function arePortsAutoCommutated(
    from: ChannelPort[],
    to: ChannelPort[],
    routes: PortsRoute[],
): boolean {
    const hypoteticalRTL = autoCommutatePorts(
        from,
        to,
    );
    const fromIds = from.ids().toSet();
    const toIds = to.ids().toSet();
    return routes
        .every((r) => {
            if (!fromIds.has(r.from) && !toIds.has(r.to)) {
                return true;
            }
            if (
                !fromIds.has(r.from) && toIds.has(r.to) ||
                fromIds.has(r.from) && !toIds.has(r.to)
            ) {
                return false;
            }
            return hypoteticalRTL.some((e) => e.from === r.from && e.to === r.to);
        });
}
