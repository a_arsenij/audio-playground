import { SetState } from "@audio-playground/lib-frontend/std/SetState";

import { Project } from "../../types/project/Project";
import { checkNoOrphanRoutes } from "./checkNoOrphanRoutes";

export function removeChannel(
    setProject: SetState<Project>,
    index: number,
) {
    if (index === 0) {
        throw new Error("??");
    }
    setProject(checkNoOrphanRoutes((proj) => {

        const channel = proj.channels[index]!;

        const inSet = channel.inputSignalPorts.ids().toSet();
        const outSet = channel.outputSignalPorts.ids().toSet();

        return {
            ...proj,
            channels: proj.channels.filter((_, i) => i !== index),
            channelsRouting: proj.channelsRouting.filter((r) =>
                !outSet.has(r.from) &&
                !inSet.has(r.to),
            ),
        };


    }));
}

export function isChannelAllowedToBeDeleted(
    idx: number,
) {
    return idx !== 0;
}
