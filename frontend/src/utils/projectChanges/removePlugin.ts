import { SetState } from "@audio-playground/lib-frontend/std/SetState";

import { Project } from "../../types/project/Project";
import { arePortsAutoCommutated, autoCommutatePorts } from "./autoCommutatePorts";
import { checkNoOrphanRoutes } from "./checkNoOrphanRoutes";

export function removeNthPlugin(
    setProject: SetState<Project>,
    channelId: string,
    index: number,
) {
    setProject(checkNoOrphanRoutes((p) => {
        const channel = p.channels.find((c) => c.id === channelId);
        if (!channel) {
            throw new Error("??");
        }
        if (index < 0 || index >= channel.plugins.length) {
            throw new Error("??");
        }

        const leftPorts = index === 0
            ? channel.outputSignalPorts
            : channel.plugins[index - 1]!.settings.inputSignalChannels;

        const selfLeftPorts = channel.plugins[index]!.settings.outputSignalChannels;
        const selfLeftPortsIds = selfLeftPorts.ids().toSet();

        const selfRightPorts = channel.plugins[index]!.settings.inputSignalChannels;
        const selfRightPortsIds = selfRightPorts.ids().toSet();

        const rightPorts = index + 1 === channel.plugins.length
            ? channel.inputSignalPorts
            : channel.plugins[index + 1]!.settings.outputSignalChannels;

        const pluginsRouting = channel.signalPluginsRouting
            .filter((c) => !selfLeftPortsIds.has(c.from) && !selfRightPortsIds.has(c.to));

        const autoRouted =
            arePortsAutoCommutated(selfLeftPorts, leftPorts, channel.signalPluginsRouting) &&
            arePortsAutoCommutated(rightPorts, selfRightPorts, channel.signalPluginsRouting)
        ;

        if (autoRouted) {
            pluginsRouting.push(
                ...autoCommutatePorts(
                    rightPorts,
                    leftPorts,
                ),
            );
        }

        const newPlugins = channel.plugins.filter((_, idx) => idx !== index);
        return {
            ...p,
            channels: p.channels.map((c) =>
                c.id === channelId
                    ? {
                        ...c,
                        plugins: newPlugins,
                        pluginsRouting,
                    }
                    : c,
            ),
        };
    }));
}
