import { generateId } from "@audio-playground/lib-common/std/Id";
import { SetState } from "@audio-playground/lib-frontend/std/SetState";

import { PluginCommonSettings } from "../../types/Plugin";
import { PluginRenderer } from "../../types/PluginRenderer";
import { ChannelPort, PortsRoute } from "../../types/project/Channel";
import { Project } from "../../types/project/Project";
import { arePortsAutoCommutated, autoCommutatePorts } from "./autoCommutatePorts";
import { checkNoOrphanRoutes } from "./checkNoOrphanRoutes";


function createNewPluginInstance(
    p: PluginRenderer<PluginCommonSettings, object>,
    id: string = generateId(),
) {
    return {
        id,
        name: p.name,
        settings: p.createSettings({
            pluginInstanceId: id,
        }),
        minimized: false,
    };
}

export function addNthPlugin(
    setProject: SetState<Project>,
    channelId: string,
    plugin: PluginRenderer<PluginCommonSettings, object>,
    index: number,
) {
    const id = generateId();
    setProject(checkNoOrphanRoutes((p) => {
        const channel = p.channels.find((c) => c.id === channelId);
        if (!channel) {
            throw new Error("??");
        }
        if (index < 0 || index > channel.plugins.length) {
            throw new Error("??");
        }
        const newPluginInstance = createNewPluginInstance(plugin, id);

        const leftSignalPorts = index === 0
            ? channel.outputSignalPorts
            : channel.plugins[index - 1]!.settings.inputSignalChannels;
        const leftMidiPorts = index === 0
            ? channel.outputMidiPorts
            : channel.plugins[index - 1]!.settings.inputMidiChannels;

        const rightSignalPorts = index === channel.plugins.length
            ? channel.inputSignalPorts
            : channel.plugins[index]!.settings.outputSignalChannels;
        const rightMidiPorts = index === channel.plugins.length
            ? channel.inputMidiPorts
            : channel.plugins[index]!.settings.outputMidiChannels;

        const signalPluginsRouting = autoCommutateOnInsert(
            leftSignalPorts,
            newPluginInstance.settings.outputSignalChannels,
            newPluginInstance.settings.inputSignalChannels,
            rightSignalPorts,
            channel.signalPluginsRouting,
        );
        const midiPluginsRouting = autoCommutateOnInsert(
            leftMidiPorts,
            newPluginInstance.settings.outputMidiChannels,
            newPluginInstance.settings.inputMidiChannels,
            rightMidiPorts,
            channel.midiPluginsRouting,
        );



        const newPlugins = [...channel.plugins];
        newPlugins.splice(index, 0, newPluginInstance);
        return {
            ...p,
            channels: p.channels.map((c) =>
                c.id === channelId
                    ? {
                        ...c,
                        plugins: newPlugins,
                        signalPluginsRouting,
                        midiPluginsRouting,
                    }
                    : c,
            ),
        };
    }));
    return id;
}

function autoCommutateOnInsert(
    leftPorts: ChannelPort[],
    selfLeftPorts: ChannelPort[],
    selfRightPorts: ChannelPort[],
    rightPorts: ChannelPort[],
    oldRouting: PortsRoute[],
) {

    const rightSignalPortsIds = rightPorts.map((p) => p.id).toSet();

    let signalPluginsRouting = [...oldRouting];
    signalPluginsRouting.push(
        ...autoCommutatePorts(
            selfLeftPorts,
            leftPorts,
        ),
    );

    const rightSignalPortsCommutatedOnlyToLeftPorts = arePortsAutoCommutated(
        rightPorts,
        leftPorts,
        oldRouting,
    );

    if (rightSignalPortsCommutatedOnlyToLeftPorts) {
        signalPluginsRouting = signalPluginsRouting.filter(
            (r) => !rightSignalPortsIds.has(r.from),
        );
        signalPluginsRouting.push(
            ...autoCommutatePorts(
                rightPorts,
                selfRightPorts,
            ),
        );
    }
    return signalPluginsRouting;
}
