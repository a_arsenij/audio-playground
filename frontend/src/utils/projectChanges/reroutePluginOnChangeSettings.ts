import { arraysAreEqual } from "@audio-playground/lib-common/std/Array";
import { generateId } from "@audio-playground/lib-common/std/Id";
import { areSetsEqual } from "@audio-playground/lib-common/std/Set";
import { applySetState } from "@audio-playground/lib-frontend/std/ApplySetState";
import { SetState, SetStateValue } from "@audio-playground/lib-frontend/std/SetState";

import { PluginCommonSettings } from "../../types/Plugin";
import { PortsRoute } from "../../types/project/Channel";
import { Project } from "../../types/project/Project";
import { checkNoOrphanRoutes } from "./checkNoOrphanRoutes";

function isJustChangeSettingsCase(
    oldSettings: PluginCommonSettings,
    newSettings: PluginCommonSettings,
): boolean {
    const oldInSignalIds = oldSettings.inputSignalChannels.ids().toSet();
    const oldOutSignalIds = oldSettings.outputSignalChannels.ids().toSet();
    const oldInMidiIds = oldSettings.inputMidiChannels.ids().toSet();
    const oldOutMidiIds = oldSettings.outputMidiChannels.ids().toSet();
    return true &&
        areSetsEqual(
            oldInSignalIds,
            newSettings.inputSignalChannels.ids().toSet(),
        ) &&
        areSetsEqual(
            oldOutSignalIds,
            newSettings.outputSignalChannels.ids().toSet(),
        ) &&
        areSetsEqual(
            oldInMidiIds,
            newSettings.inputMidiChannels.ids().toSet(),
        ) &&
        areSetsEqual(
            oldOutMidiIds,
            newSettings.outputMidiChannels.ids().toSet(),
        )
    ;
}

function handleJustChangeSettingsCase(
    _: PluginCommonSettings,
    newSettings: PluginCommonSettings,
    signalPluginsRouting: PortsRoute[],
    midiPluginsRouting: PortsRoute[],
): {
    signalPluginsRouting: PortsRoute[],
    midiPluginsRouting: PortsRoute[],
    settings: PluginCommonSettings,
} {
    return {
        signalPluginsRouting,
        midiPluginsRouting,
        settings: newSettings,
    };
}


function isLoadPresetCase(
    oldSettings: PluginCommonSettings,
    newSettings: PluginCommonSettings,
): boolean {
    return true &&
        arraysAreEqual(
            oldSettings.inputSignalChannels.map((c) => c.title),
            newSettings.inputSignalChannels.map((c) => c.title),
        ) &&
        arraysAreEqual(
            oldSettings.outputSignalChannels.map((c) => c.title),
            newSettings.outputSignalChannels.map((c) => c.title),
        ) &&
        arraysAreEqual(
            oldSettings.inputMidiChannels.map((c) => c.title),
            newSettings.inputMidiChannels.map((c) => c.title),
        ) &&
        arraysAreEqual(
            oldSettings.outputMidiChannels.map((c) => c.title),
            newSettings.outputMidiChannels.map((c) => c.title),
        )
    ;
}

function handleLoadPresetCase(
    oldSettings: PluginCommonSettings,
    newSettings: PluginCommonSettings,
    signalPluginsRouting: PortsRoute[],
    midiPluginsRouting: PortsRoute[],
): {
    signalPluginsRouting: PortsRoute[],
    midiPluginsRouting: PortsRoute[],
    settings: PluginCommonSettings,
} {
    const newIdsToNewNewIds = [
        newSettings.inputSignalChannels,
        newSettings.outputSignalChannels,
        newSettings.inputMidiChannels,
        newSettings.outputMidiChannels,
    ].flat().map((c) => [c.id, generateId()] as const).toMap();

    const oldSignalInIdToNewId = oldSettings.inputSignalChannels.map((c, i) => [c.id, newSettings.inputSignalChannels[i]!.id] as const).toMap();
    const oldSignalOutIdToNewId = oldSettings.outputSignalChannels.map((c, i) => [c.id, newSettings.outputSignalChannels[i]!.id] as const).toMap();

    const oldMidiInIdToNewId = oldSettings.inputMidiChannels.map((c, i) => [c.id, newSettings.inputMidiChannels[i]!.id] as const).toMap();
    const oldMidiOutIdToNewId = oldSettings.outputMidiChannels.map((c, i) => [c.id, newSettings.outputMidiChannels[i]!.id] as const).toMap();

    const newSignalRoutes = signalPluginsRouting.map((r) => ({
        from: newIdsToNewNewIds.get(oldSignalOutIdToNewId.get(r.from) ?? r.from) ?? r.from,
        to: newIdsToNewNewIds.get(oldSignalInIdToNewId.get(r.to) ?? r.to) ?? r.to,
    }));

    const newMidiRoutes = midiPluginsRouting.map((r) => ({
        from: newIdsToNewNewIds.get(oldMidiOutIdToNewId.get(r.from) ?? r.from) ?? r.from,
        to: newIdsToNewNewIds.get(oldMidiInIdToNewId.get(r.to) ?? r.to) ?? r.to,
    }));

    return {
        signalPluginsRouting: newSignalRoutes,
        midiPluginsRouting: newMidiRoutes,
        settings: {
            ...newSettings,
            inputSignalChannels: newSettings.inputSignalChannels.map((c) => ({ ...c, id: newIdsToNewNewIds.get(c.id)! })),
            outputSignalChannels: newSettings.outputSignalChannels.map((c) => ({ ...c, id: newIdsToNewNewIds.get(c.id)! })),
            inputMidiChannels: newSettings.inputMidiChannels.map((c) => ({ ...c, id: newIdsToNewNewIds.get(c.id)! })),
            outputMidiChannels: newSettings.outputMidiChannels.map((c) => ({ ...c, id: newIdsToNewNewIds.get(c.id)! })),
        },
    };
}

function handleUnknownCase(
    oldSettings: PluginCommonSettings,
    newSettings: PluginCommonSettings,
    signalPluginsRouting: PortsRoute[],
    midiPluginsRouting: PortsRoute[],
): {
    signalPluginsRouting: PortsRoute[],
    midiPluginsRouting: PortsRoute[],
    settings: PluginCommonSettings,
} {
    const oldInSignalIds = oldSettings.inputSignalChannels.ids().toSet();
    const oldOutSignalIds = oldSettings.outputSignalChannels.ids().toSet();
    const oldInMidiIds = oldSettings.inputMidiChannels.ids().toSet();
    const oldOutMidiIds = oldSettings.outputMidiChannels.ids().toSet();
    return {
        signalPluginsRouting: signalPluginsRouting.filter((r) => !oldOutSignalIds.has(r.from) && !oldInSignalIds.has(r.to)),
        midiPluginsRouting: midiPluginsRouting.filter((r) => !oldOutMidiIds.has(r.from) && !oldInMidiIds.has(r.to)),
        settings: newSettings,
    };
}

export function reroutePluginOnChangeSettings(
    setProject: SetState<Project>,
    channelId: string,
    pluginId: string,
    setter: SetStateValue<PluginCommonSettings>,
) {

    setProject(checkNoOrphanRoutes((proj) => {

        const channel = proj.channels.find((c) => c.id === channelId)!;
        const plugin = channel.plugins.find((p) => p.id === pluginId)!;
        const oldSettings = plugin.settings;
        const newSettings = applySetState(setter, oldSettings);
        const {
            settings,
            signalPluginsRouting,
            midiPluginsRouting,
        } = (() => {
            if (isJustChangeSettingsCase(oldSettings, newSettings)) {
                return handleJustChangeSettingsCase;
            }
            if (isLoadPresetCase(oldSettings, newSettings)) {
                return handleLoadPresetCase;
            }
            return handleUnknownCase;
        })()(
            oldSettings,
            newSettings,
            channel.signalPluginsRouting,
            channel.midiPluginsRouting,
        );

        return {
            ...proj,
            channels: proj.channels.map((c) =>
                c.id !== channelId
                    ? c
                    : (() => {
                        return {
                            ...c,
                            signalPluginsRouting,
                            midiPluginsRouting,
                            plugins: c.plugins.map((p) =>
                                p.id !== pluginId
                                    ? p
                                    : {
                                        ...p,
                                        settings,
                                    },
                            ),
                        };
                    })(),
            ),
        };

    }));

}
