import { generateId } from "@audio-playground/lib-common/std/Id";
import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import { useCallback } from "react";

import { Channel, ChannelPort } from "../../types/project/Channel";
import { Project } from "../../types/project/Project";
import { autoCommutatePorts } from "./autoCommutatePorts";
import { checkNoOrphanRoutes } from "./checkNoOrphanRoutes";

export function createNewChannelPort(
    title: string,
    id: string = generateId(),
): ChannelPort {
    return {
        title,
        id,
    };
}

export function useAddNewChannel(setProject: SetState<Project>) {
    return useCallback(() => {
        createAndAddNewChannel(setProject);
    }, [setProject]);
}

export function createAndAddNewChannel(setProject: SetState<Project>) {
    const newChannel = createNewChannelInstance();
    addNewChannel(setProject, newChannel);
}

function addNewChannel(
    setProject: SetState<Project>,
    newChannel: Channel,
) {
    setProject(checkNoOrphanRoutes((proj) => {
        const masterIn = proj.channels[0]!.inputSignalPorts;
        const channelOut = newChannel.outputSignalPorts;
        return {
            ...proj,
            channels: [
                ...proj.channels,
                newChannel,
            ],
            channelsRouting: [
                ...proj.channelsRouting,
                ...autoCommutatePorts(
                    channelOut,
                    masterIn,
                ),
            ],
        };
    }));
}

function createNewChannelInstance(): Channel {
    const inputSignalPorts: [ChannelPort, ChannelPort] = [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ];
    const outputSignalPorts: [ChannelPort, ChannelPort] = [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ];
    const inputMidiPorts: [ChannelPort] = [
        createNewChannelPort("Midi"),
    ];
    return {
        id: generateId(),
        title: `New channel ${(new Date()).getTime()}`,
        volume: 1,
        pan: 0,
        muted: false,
        plugins: [],
        automations: [],
        inputSignalPorts,
        outputSignalPorts,
        inputMidiPorts,
        outputMidiPorts: [],
        signalPluginsRouting: autoCommutatePorts(
            inputSignalPorts,
            outputSignalPorts,
        ),
        midiPluginsRouting: [],
    };
}
