export type ToneGenerator = (
    frequency: number,
    noteTimeSec: number
) => number

const TWO_PI = Math.PI * 2;

export function generateSineTone(
    frequency: number,
    noteTimeSec: number,
): number {
    const notePhase = noteTimeSec * TWO_PI * frequency;
    const tone = Math.sin(notePhase);
    return tone;
}

export function generateTriangleTone(
    frequency: number,
    noteTimeSec: number,
): number {
    const notePhase = noteTimeSec * frequency * 2 + 0.5;
    const tone = Math.abs(notePhase % 2 - 1) * 2 - 1;
    return tone;
}

export function generateSawTone(
    frequency: number,
    noteTimeSec: number,
): number {
    const notePhase = noteTimeSec * frequency;
    const tone = (notePhase % 1) * 2 - 1;
    return tone;
}

export function generateSquareTone(
    frequency: number,
    noteTimeSec: number,
): number {
    const notePhase = noteTimeSec * frequency;
    const tone = (notePhase % 1) > 0.5 ? 1 : -1;
    return tone;
}
