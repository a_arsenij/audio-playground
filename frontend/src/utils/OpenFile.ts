import { openFile } from "@audio-playground/lib-frontend/std/OpenFile";

import { getContext } from "./Context";

export function openAudioFile(): Promise<AudioBuffer> {
    return openFile()
        .then((file) =>
            getContext().decodeAudioData(file),
        );
}
