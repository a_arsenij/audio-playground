declare const __nominal__type: unique symbol;

export type KeyEvent = number & {
    readonly [__nominal__type]: true,
};

function createNoteEvent(
    firstBit: number,
    key: number,
    velocity: number,
    timeSample: number,
): KeyEvent {
    return (
        firstBit +
        2 * key +
        256 * velocity +
        32768 * timeSample
    ) as KeyEvent;
}

export function createNoteOnEvent(
    key: number,
    velocity: number,
    timeSample: number,
): KeyEvent {
    return createNoteEvent(1, key, velocity, timeSample);
}
export function createNoteOffEvent(
    key: number,
    velocity: number,
    timeSample: number,
): KeyEvent {
    return createNoteEvent(0, key, velocity, timeSample);
}

export function getKeyEventType(
    event: KeyEvent,
): "noteOn" | "noteOff" {
    return (0b1 & event as number) ? "noteOn" : "noteOff";
}
export function getKeyEventKey(
    event: KeyEvent,
): number {
    return (0b11111110 & event) / 2;
}
export function getKeyEventVelocity(
    event: KeyEvent,
): number {
    return (0b111111100000000 & event) / 256;
}
export function getKeyEventTimeSample(
    event: KeyEvent,
): number {
    return Math.floor(event / 32768);
}

export function keyEventToString(
    event: KeyEvent,
): string {
    const type = getKeyEventType(event);
    const key = getKeyEventKey(event);
    const time = getKeyEventTimeSample(event);
    return `type: ${type}, key: ${key}, time: ${time}`;
}
