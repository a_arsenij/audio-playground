import { addPoint, distance, Point, subtractPoint } from "@audio-playground/lib-common/math/Point";
import { arrayOfNItems } from "@audio-playground/lib-common/std/Array";

export type PointAndDerivative = Point & {
    yDer: number,
}

export function interpolatedCubicBezierPath(points: PointAndDerivative[]) {

    if (points.length < 2) {
        return " M 0 0";
    }

    const path0 = getFirstCubicBezier(points);

    const paths: string[] = [];

    for (let i = 2; i < points.length; i++) {
        const P3 = points[i]!;
        const P0 = points[i - 1]!;
        const dt = distance(P0, P3) / 4;

        const dP2 = getNormalizedDxDy(P3.yDer, dt);
        const P2 = subtractPoint(P3, dP2);

        paths.push(`S ${P2.x} ${P2.y}, ${P3.x} ${P3.y}`);
    }

    return ` ${path0} ${paths.join(" ")} `;

}

function getFirstCubicBezier(points: PointAndDerivative[]) {

    const P0 = points[0]!;

    const P3 = points[1]!;
    const d = distance(P0, P3);

    const dt = d / 4;

    const dP0 = getNormalizedDxDy(P0.yDer, dt);
    const P1 = addPoint(P0, dP0);

    const dt3 = Math.min(P3.x, dt);

    const dP3 = getNormalizedDxDy(P3.yDer, dt3);
    const P2 = subtractPoint(P3, dP3);

    return `M ${P0.x} ${P0.y} C ${P1.x} ${P1.y}, ${P2.x} ${P2.y}, ${P3.x} ${P3.y} `;
}

function getNormalizedDxDy(derivative: number, dt: number) {

    const dx = dt;

    if (Number.isFinite(derivative)) {

        const alpha = Math.sqrt(1 + derivative * derivative);
        const dy = dx * derivative;

        return {
            x: dx / alpha,
            y: dy / alpha,
        };
    }
    return {
        x: 0,
        y: derivative > 0 ? dt : -dt,
    };

}

const DEFAULT_FINENESS = 6;

export function getPartition(fineness = DEFAULT_FINENESS) {

    const left = arrayOfNItems(fineness, (i) => {
        return 1 / 2 ** (i + 2);
    });

    const right = left.map((n) => 1 - n);

    left.reverse();

    return [0, ...left, 0.5, ...right, 1];
}
