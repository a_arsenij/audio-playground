import { SvgFunctionTypeRoot } from "./index";
import { getPartition, interpolatedCubicBezierPath } from "./interpolation";

export const powerFunctionType: SvgFunctionTypeRoot<1> = {
    id: "power",
    paramsSettings: [{
        label: "x^a", defaultValue: 0.3,
    }],
    getFunctionFromParams: getPowerFunctionFromParam,
    path: (a: number) => {

        const f = getPowerFunctionFromParam(a);
        const fDer = getPowerFunctionDerivativeFromParam(a);

        const pointsWithDerivative = getPartition().map((x) => {
            return {
                x,
                y: f(x),
                yDer: fDer(x),
            };
        });
        return interpolatedCubicBezierPath(pointsWithDerivative);
    },
};

const stepBottom = (x: number) => x === 1 ? 1 : 0;
const stepBottomDer = (x: number) => x === 1 ? Number.POSITIVE_INFINITY : 0;
const stepTop = (x: number) => x === 0 ? 0 : 1;
const stepTopDer = (x: number) => x === 0 ? Number.POSITIVE_INFINITY : 0;
function getPowerFunctionFromParam(a: number) {
    if (a === 0) {
        return stepTop;
    }
    if (a === 1) {
        return stepBottom;
    }

    const aa = 1 / (1 - a) - 1;

    return (x: number) => {
        const y = Math.pow(x, aa);
        return y;
    };

};
function getPowerFunctionDerivativeFromParam(a: number) {
    if (a === 0) {
        return stepTopDer;
    }
    if (a === 1) {
        return stepBottomDer;
    }

    const aa = 1 / (1 - a) - 1;

    return (x: number) => {
        const y = aa * Math.pow(x, aa - 1);
        return y;
    };

};
