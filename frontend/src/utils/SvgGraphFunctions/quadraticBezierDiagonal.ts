import { identity, SvgFunctionTypeRoot } from "./index";

export const quadraticBezierDiagonal: SvgFunctionTypeRoot<1> = {
    id: "quadraticBezier",
    paramsSettings: [{
        label: "P1 = [a; 1 - a]",
        defaultValue: 0.3,
    }],
    getFunctionFromParams: (a: number) => {
        // P0 = (0;0) P1 = (a; 1 - a) P2 = (1;1)
        if (a === 0.5) {
            return identity;
        }

        const aa = 1 - 2 * a;
        const aaa = 2 / aa;
        const aSquared = a * a;

        return (x) => {

            const y = aaa * (Math.sqrt(x * aa + aSquared) - a) - x;
            return y;
        };

    },
    path: (a: number) => {
        const P1x = a;
        const P1y = 1 - a;
        return `M 0 0 Q ${P1x} ${P1y} 1 1`;
    },
};
