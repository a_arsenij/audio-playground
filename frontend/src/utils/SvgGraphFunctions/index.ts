import { Point } from "@audio-playground/lib-common/math/Point";
import { ReadonlyTupleOf } from "@audio-playground/lib-common/types/Array";

import { powerFunctionType } from "./power";
import { quadraticBezierDiagonal } from "./quadraticBezierDiagonal";

export type RealFunction = (x: number) => number;
export type SvgFunctionTypeId = keyof typeof FUNCTION_TYPES_MAP;
export type SvgFunctionType = typeof FUNCTION_TYPES_MAP[SvgFunctionTypeId];

type ParamSetting = {
    label: string,
    defaultValue: number,
}
export type SvgFunctionTypeRoot<N extends number> = {
    id: SvgFunctionTypeId,
    getFunctionFromParams: (...args: ReadonlyTupleOf<N, number>) => RealFunction,
    paramsSettings: ReadonlyTupleOf<N, ParamSetting>,
    path: (...args: ReadonlyTupleOf<N, number>) => string,
}
export const identity = (x: number) => x;

const linear: SvgFunctionTypeRoot<0> = {
    id: "linear",
    paramsSettings: [],
    getFunctionFromParams: () => identity,
    path: () => "M 0 0 L 1 1",
};

export const FUNCTION_TYPES_MAP = {
    linear,
    quadraticBezier: quadraticBezierDiagonal,
    power: powerFunctionType,
} as const;

export type PieceData = {
    fun: SvgFunctionTypeId,
    params: number[],
}

export type PiecewiseFunctionData = {
    points: Point[],
    pieceData: PieceData[],
}

export function createPiecewiseFunctionData(): PiecewiseFunctionData {
    return {
        points: [
            { x: 0, y: 0 },
            { x: 1, y: 1 },
        ],
        pieceData: [
            {
                fun: "linear",
                params: [],
            },
        ],
    };
}

export function arePiecewiseFunctionDataEquals(
    a: PiecewiseFunctionData,
    b: PiecewiseFunctionData,
): boolean {
    if (a.points.length !== b.points.length) {
        return false;
    }
    if (a.points.some((aa, idx) => {
        const bb = b.points[idx]!;
        return aa.x !== bb.x || aa.y !== bb.y;
    })) {
        return false;
    }
    if (a.pieceData.length !== b.pieceData.length) {
        return false;
    }
    if (a.pieceData.some((aa, idx) => {
        const bb = b.pieceData[idx]!;
        if (aa.fun !== bb.fun) {
            return true;
        }
        if (aa.params.length !== bb.params.length) {
            return true;
        }
        return !aa.params.every((aaa, idx) => {
            const bbb = bb.params[idx];
            return aaa === bbb;
        });
    })) {
        return false;
    }
    return true;

}

export function createPiecewiseFunction(data: PiecewiseFunctionData): RealFunction {
    const { points, pieceData } = data;

    if (
        points.length < 2 ||
        pieceData.length < 1 ||
        pieceData.length + 1 !== points.length ||
        points[0]!.x !== 0 ||
        points[points.length - 1]!.x !== 1
    ) {
        console.error(`invalid piecewise function data: ${data}`);
        return () => 1;
    }

    const pointsX = points.map((p) => p.x);

    const functions = pieceData.map((p) => {
        const f = FUNCTION_TYPES_MAP[p.fun];
        // @ts-ignore
        return f.getFunctionFromParams(...p.params);
    });

    return (x) => {
        if (x <= 0) {
            return points[0]!.y;
        }

        if (x >= 1) {
            return points[points.length - 1]!.y;
        }

        const pieceIdx = pointsX.findLastIndex((px) => {
            return x >= px;
        });

        const pieceFunction = functions[pieceIdx]!;

        const a = points[pieceIdx]!;
        const b = points[pieceIdx + 1]!;

        if (a.x === b.x) {
            return a.y;
        }
        const xPiece = (x - a.x) / (b.x - a.x);
        const yPiece = pieceFunction(xPiece);
        const y = a.y + yPiece * (b.y - a.y);

        return y;
    };
}
