import { monoifyIfFakeStereo } from "./MonoifyIfFakeStereo";

export function audioBufferToStereoBuffer(
    buffer: AudioBuffer,
): {
    sampleRate: number,
    wave: [number[]] | [number[], number[]],
} {
    return {
        sampleRate: buffer.sampleRate,
        wave: monoifyIfFakeStereo(
            buffer.numberOfChannels === 1
                ? [
                    [...buffer.getChannelData(0)],
                ]
                : [
                    [...buffer.getChannelData(0)],
                    [...buffer.getChannelData(1)],
                ],
        ),
    };
}
