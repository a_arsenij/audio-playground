import { DEFAULT_SAMPLE_RATE } from "../engine/DefaultProject";

let __context: AudioContext;
export const getContext = () => {
    if (__context === undefined) {
        __context = new AudioContext({
            sampleRate: DEFAULT_SAMPLE_RATE,
        });
    }
    return __context;
};
