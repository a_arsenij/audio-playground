export function isWorker() {
    return typeof WorkerGlobalScope !== "undefined" && self instanceof WorkerGlobalScope;
}

export function isAudioWorklet() {
    return !!globalThis.AudioWorkletProcessor;
}

export function isUIThread() {
    return !isWorker() && !isAudioWorklet();
}

export function isPoolWorker() {
    return isWorker() && self.name.startsWith(POOL_WORKER_NAME);
}

export function isMasterWorker() {
    return isWorker() && self.name.startsWith(MASTER_WORKER_NAME);
}

export const MASTER_WORKER_NAME = "MasterWorkerThread";
export const POOL_WORKER_NAME = "PoolWorkerThread#";

export const BUNDLE_FILENAME = "dist.js";
