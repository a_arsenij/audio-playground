import { acquireArrayFromPool, PooledArray } from "@audio-playground/lib-common/std/ArraysPool";
import { dbToAmplitude } from "@audio-playground/lib-sound/AmplitudeToDecibells";

import { Channel } from "../../types/project/Channel";
import { addToBuffer } from "../../utils/AddToBuffer";
import { KeyEvent } from "../../utils/KeyEvent";
import { MAX_VOLUME_DB, MIN_VOLUME_DB } from "../../utils/MinVolume";
import { EngineState } from "../EngineState";
import { getMaybeAutomated } from "../GetMaybeAutomated";
import { renderPlugin } from "./RenderPlugin";
import { renderWaveClipItself, renderWaveClipsForChannel } from "./RenderWaveClipsForChannel";


export function renderChannel(
    state: EngineState,
    channel: Channel,
    keyEvents: KeyEvent[],
    sharedBuffer: ArrayBufferLike,
    input: PooledArray[],
    output: PooledArray[],
) {

    if (channel.muted) {
        return;
    }

    const bufferLength = state.project.get().bufferLength;

    const loopType = state.loop.get().type;

    if (state.isPlaying.get()) {
        (
            loopType === "playlist"
                ? renderWaveClipsForChannel
                : renderWaveClipItself
        )(
            state,
            channel,
            sharedBuffer,
            input,
            bufferLength,
        );
    }

    const portsIds = channel.plugins
        .map((p) => [p.settings.inputSignalChannels.ids(), p.settings.outputSignalChannels.ids()])
        .flat(2);

    const portsIdsSet = portsIds
        .toSet();

    if (portsIds.length !== portsIdsSet.size) {
        throw new Error(`Why ${portsIds.length}!=${portsIdsSet.size}?`);
    }

    const portIdToBuffer = portsIdsSet.toArray()
        .map((id) => [id, acquireArrayFromPool(bufferLength)] as const)
        .toMap();
    const tmpBuffers = portIdToBuffer.values().toArray();
    channel.inputSignalPorts.forEach((p, idx) => {
        portIdToBuffer.set(p.id, input[idx]!);
    });
    channel.outputSignalPorts.forEach((p, idx) => {
        portIdToBuffer.set(p.id, output[idx]!);
    });
    const routes = [
        ...channel.signalPluginsRouting,
        ...channel.midiPluginsRouting,
    ];
    const portsRoutes = routes.map((c) => [c.from, c.to] as const).collectToArray();
    function exportPortValue(portId: string) {
        const bufferFrom = portIdToBuffer.get(portId)!;
        const connectedTo = portsRoutes.get(portId);
        connectedTo?.forEach((targetPortId) => {
            const bufferTo = portIdToBuffer.get(targetPortId)!;
            addToBuffer(bufferFrom, bufferTo);
        });
    }
    channel.inputSignalPorts.ids().forEach(exportPortValue);

    for (let i = channel.plugins.length - 1; i >= 0; i--) {
        const plugin = channel.plugins[i]!;
        const outputIds = plugin.settings.outputSignalChannels.ids();
        const pluginInputBuffers = plugin.settings.inputSignalChannels.ids().map((p) => portIdToBuffer.get(p)!);
        const pluginOutputBuffers = outputIds.map((p) => portIdToBuffer.get(p)!);
        renderPlugin(
            state,
            channel,
            plugin,
            keyEvents,
            sharedBuffer,
            pluginInputBuffers,
            pluginOutputBuffers,
        );
        outputIds.forEach(exportPortValue);
    }

    const volume = getMaybeAutomated(
        state,
        channel,
        { type: "volume" },
        MIN_VOLUME_DB,
        MAX_VOLUME_DB,
        channel.volume,
    );

    for (let i = 0; i < bufferLength; i++) {
        const currentTimeBars = state.getCurrentTimeBars(i);
        const volumeValue = dbToAmplitude(volume(currentTimeBars));
        for (let j = 0; j < output.length; j++) {
            output[j]![i]! *= volumeValue;
        }
    }

    tmpBuffers.forEach((b) => b.iDontNeedItAnymore());
}
