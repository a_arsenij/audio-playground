import { arrayOfNItems } from "@audio-playground/lib-common/std/Array";
import { acquireArrayFromPool, PooledArray } from "@audio-playground/lib-common/std/ArraysPool";

import { EngineState } from "../EngineState";
import { findKeyEventsForChannel } from "../findEntities/FindNotes";
import { renderChannel } from "./RenderChannel";

const EPSILON = 0.00001;


export function renderMaster(
    state: EngineState,
    sharedBuffer: ArrayBufferLike,
    buffer: PooledArray[],
): [boolean, number[][]] {
    state.incTimeSamples();

    const project = state.project.get();
    const channelsNum = project.channels.length;

    if (channelsNum === 0) {
        return [true, []];
    }

    const bufferLength = project.bufferLength;
    const channelsInputs: PooledArray[][] = project.channels
        .map((channel) => arrayOfNItems(
            channel.inputSignalPorts.length,
            () => acquireArrayFromPool(bufferLength),
        ));

    const channelsOutputs: PooledArray[][] = project.channels
        .map((channel) => arrayOfNItems(
            channel.outputSignalPorts.length,
            () => acquireArrayFromPool(bufferLength),
        ));

    const inputPortToChannelAndIndex = project.channels
        .map((channel, channelIndex) =>
            channel.inputSignalPorts
                .map(
                    (port, portIndex) => [port.id, { channelIndex, portIndex }] as const,
                ),
        )
        .flat()
        .toMap();

    const channelOutputPortToChannelInputPort = project.channelsRouting
        .map((r) => [r.from, r.to] as const)
        .collectToSet();
    for (let i = channelsNum - 1; i >= 0; i--) {
        const channel = state.project.get().channels[i]!;
        const keyEvents = findKeyEventsForChannel(state, channel.id);
        const channelInput = channelsInputs[i]!;
        const channelOutput = channelsOutputs[i]!;
        renderChannel(
            state,
            channel,
            keyEvents,
            sharedBuffer,
            channelInput,
            channelOutput,
        );
        channel.outputSignalPorts.forEach((outputSignalPort, outputIdx) => {
            const output = channelOutput[outputIdx]!;
            const mixTo = channelOutputPortToChannelInputPort?.get(outputSignalPort.id)?.values()?.toArray();
            if (!mixTo) {
                return;
            }
            mixTo.forEach((to) => {
                const { channelIndex, portIndex } = inputPortToChannelAndIndex.get(to)!;
                const toBuffer = channelsInputs[channelIndex]![portIndex]!;
                for (let j = 0; j < bufferLength; j++) {
                    toBuffer[j]! += output[j]!;
                }
            });
        });
    }

    const channelsLevels: [number, number][] = new Array(channelsNum);
    let everyLevelIsZero = true;
    for (let i = 0; i < channelsNum; i++) {
        const levels: [number, number] = [0, 0];
        for (let s = 0; s < bufferLength; s++) {
            levels[0] += channelsOutputs[i]![0]![s]! ** 2;
            levels[1] += channelsOutputs[i]![1]![s]! ** 2;
        }
        levels[0] /= bufferLength;
        levels[1] /= bufferLength;
        if (levels[0]! > EPSILON || levels[1]! > EPSILON) {
            everyLevelIsZero = false;
        }
        channelsLevels[i] = levels;
    }

    for (let i = 0; i < channelsOutputs[0]!.length; i++) {
        for (let j = 0; j < bufferLength; j++) {
            buffer[i]![j] = channelsOutputs[0]![i]![j]!;
        }
    }

    for (let c = 0; c < channelsNum; c++) {
        channelsInputs[c]![0]!.iDontNeedItAnymore();
        channelsInputs[c]![1]!.iDontNeedItAnymore();
        channelsOutputs[c]![0]!.iDontNeedItAnymore();
        channelsOutputs[c]![1]!.iDontNeedItAnymore();
    }

    return [everyLevelIsZero, channelsLevels];
}

