import { PooledArray } from "@audio-playground/lib-common/std/ArraysPool";
import { blobRefToF64 } from "@audio-playground/lib-common/std/BlobRef";

import { WaveLoop } from "../../types/Loop";
import { Channel } from "../../types/project/Channel";
import { EngineState } from "../EngineState";
import { isClipUsageIntersectsWithBuffer } from "../findEntities/IsClipUsageIntersectsWithBuffer";

type WaveBuffer = Float32Array | Float64Array;

function writeWaveBuffersToTargetBuffersSameChannelsNumber(
    waveBuffers: WaveBuffer[],
    waveSampleIndex: number,
    targetBuffers: WaveBuffer[],
    targetSampleIndex: number,
) {
    for (let i = 0; i < waveBuffers.length; i++) {
        targetBuffers[i]![targetSampleIndex]! += waveBuffers[i]![waveSampleIndex]!;
    }
}

function writeWaveBuffersToTargetBuffersMixdownAndSplit(
    waveBuffers: WaveBuffer[],
    waveSampleIndex: number,
    targetBuffers: WaveBuffer[],
    targetSampleIndex: number,
) {
    let sum = 0;
    for (let i = 0; i < waveBuffers.length; i++) {
        sum += waveBuffers[i]![waveSampleIndex]!;
    }
    sum /= waveBuffers.length;
    for (let i = 0; i < waveBuffers.length; i++) {
        targetBuffers[i]![targetSampleIndex]! += sum;
    }
}

function getWriteWaveBuffersToTargetBuffers(
    waveBuffers: WaveBuffer[],
    targetBuffers: WaveBuffer[],
) {
    if (waveBuffers.length === targetBuffers.length) {
        return writeWaveBuffersToTargetBuffersSameChannelsNumber;
    }
    return writeWaveBuffersToTargetBuffersMixdownAndSplit;
}

export function renderWaveClipItself(
    state: EngineState,
    channel: Channel,
    sharedBuffer: ArrayBufferLike,
    output: PooledArray[],
    bufferLength: number,
) {
    const loop = state.loop.get() as WaveLoop;
    const clipId = loop.clipId;
    const clipUsage = state.clipsUsages.get().find(
        (u) => u.clipId === clipId,
    );
    if (!clipId) {
        return;
    }
    if (clipUsage?.rowId !== channel.id) {
        return;
    }
    const loopStartBars = loop.borders.timeStartBar;
    const loopStartSamples = state.timeBarsToTimeSamples(loopStartBars);
    const loopLengthSamples = state.loopLengthSamples.get();
    const initialTimeSinceLoopStartSamples = state.timeSinceLoopStartSamples.get();

    const clip = state.idToClips.get().get(clipUsage.clipId)!;
    if (clip.type !== "wave") {
        return;
    }
    const waveLength = clip.wave[0].size;

    const buffers = clip.wave.map((w) => blobRefToF64(w, sharedBuffer));
    const writeWaveBuffers = getWriteWaveBuffersToTargetBuffers(buffers, output);
    for (let i = 0; i < bufferLength; i++) {
        const timeSamples = (
            initialTimeSinceLoopStartSamples + loopStartSamples + i
        ) % loopLengthSamples + loopStartSamples;
        if (i < 0 || i >= waveLength) {
            continue;
        }
        writeWaveBuffers(
            buffers, timeSamples,
            output, i,
        );
    }
}

export function renderWaveClipsForChannel(
    state: EngineState,
    channel: Channel,
    sharedBuffer: ArrayBufferLike,
    output: PooledArray[],
    bufferLength: number,
) {

    const loop = state.loop.get();

    const loopStartBars = loop.borders.timeStartBar;
    const loopLengthSamples = state.loopLengthSamples.get();
    const initialTimeSinceLoopStartSamples = state.timeSinceLoopStartSamples.get();
    state.project.get().clipsUsages.forEach((usage) => {

        if (usage.rowId !== channel.id) {
            return;
        }

        const usageTime = isClipUsageIntersectsWithBuffer(state, usage);
        if (!usageTime) {
            return;
        }

        const clip = state.idToClips.get().get(usage.clipId)!;
        if (clip.type !== "wave") {
            return;
        }
        const clipWaves = clip.wave.map((v) =>
            blobRefToF64(v, sharedBuffer),
        );
        const writeWaveBuffers = getWriteWaveBuffersToTargetBuffers(clipWaves, output);
        const waveLength = clipWaves[0]!.length;

        for (let i = 0; i < bufferLength; i++) {

            const timeSinceLoopStartSamples =
                (initialTimeSinceLoopStartSamples + i) % loopLengthSamples;

            if (
                timeSinceLoopStartSamples > usageTime.usageEndTimeSinceLoopStartSamples ||
                timeSinceLoopStartSamples < usageTime.usageStartTimeSinceLoopStartSamples
            ) {
                return;
            }

            const relativeTime = (
                state.timeSamplesToTimeBars(timeSinceLoopStartSamples) -
                usage.timelineTimeStartBars + loopStartBars
            ) / usage.timelineTimeLengthBars;
            const samplesTime = Math.round(
                (relativeTime * usage.sourceTimeLength) + usage.sourceTimeStart,
            );
            if (samplesTime < 0 || samplesTime >= waveLength) {
                continue;
            }

            writeWaveBuffers(
                clipWaves,
                samplesTime,
                output,
                i,
            );

        }

    });

}
