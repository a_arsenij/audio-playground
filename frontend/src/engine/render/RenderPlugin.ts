import { PooledArray } from "@audio-playground/lib-common/std/ArraysPool";

import { nameToPluginRenderer } from "../../plugins/_PluginsRenderers";
import { Channel } from "../../types/project/Channel";
import { ChannelPlugin } from "../../types/project/ChannelPlugin";
import { KeyEvent } from "../../utils/KeyEvent";
import { EngineState } from "../EngineState";
import { getMaybeAutomated } from "../GetMaybeAutomated";


export function renderPlugin(
    state: EngineState,
    channel: Channel,
    channelPlugin: ChannelPlugin,
    keyEvents: KeyEvent[],
    sharedBuffer: ArrayBufferLike,
    input: PooledArray[],
    output: PooledArray[],
) {
    const plugin = nameToPluginRenderer(channelPlugin.name);
    plugin.renderBuffer({
        timeSample: state.timeSamples.get(),
        sampleRate: state.project.get().sampleRate,
        bufferLength: state.project.get().bufferLength,
        input,
        output,
        keyEvents,
        pluginSettings: channelPlugin.settings as any,
        pluginState: state.getStateForPlugin(channelPlugin.id, channelPlugin.name) as any,
        sharedBuffer,
        getMaybeAutomated: (
            knobId: string,
            minValue: number,
            maxValue: number,
            defaultValue: number,
        ) => getMaybeAutomated(
            state,
            channel,
            {
                type: "plugin",
                pluginInstanceId: channelPlugin.id,
                knobId,
            },
            minValue,
            maxValue,
            defaultValue,
        ),
        getCurrentTimeBars: state.getCurrentTimeBars,
    });
}
