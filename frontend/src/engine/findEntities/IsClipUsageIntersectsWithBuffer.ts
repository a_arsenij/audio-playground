import { intersect, Interval } from "@audio-playground/lib-common/math/Interval";

import { ClipUsage } from "../../types/project/ClipUsage";
import { EngineState } from "../EngineState";

export function isClipUsageIntersectsWithBuffer(
    state: EngineState,
    usage: ClipUsage,
): undefined | {
    usageStartTimeSinceLoopStartSamples: number,
    usageEndTimeSinceLoopStartSamples: number,
} {
    const loop = state.loop.get();
    if (usage.timelineTimeStartBars >= loop.borders.timeEndBar) {
        return undefined;
    }
    if (
        usage.timelineTimeStartBars + usage.timelineTimeLengthBars <
        loop.borders.timeStartBar
    ) {
        return undefined;
    }

    const usageStartTimeSinceLoopStartSamples = state.timeBarsToTimeSamples(
        usage.timelineTimeStartBars - loop.borders.timeStartBar,
    );
    const usageEndTimeSinceLoopStartSamples = state.timeBarsToTimeSamples(
        usage.timelineTimeStartBars + usage.timelineTimeLengthBars - loop.borders.timeStartBar,
    );
    const usageInterval: Interval =
        [usageStartTimeSinceLoopStartSamples, usageEndTimeSinceLoopStartSamples];
    if (
        !state.currentBufferTimeSinceLoopStartSamples.get()
            .some((i) => intersect(i, usageInterval))
    ) {
        return undefined;
    }
    return {
        usageStartTimeSinceLoopStartSamples,
        usageEndTimeSinceLoopStartSamples,
    };
}
