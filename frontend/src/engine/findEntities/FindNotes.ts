import { intersect, Interval } from "@audio-playground/lib-common/math/Interval";

import { trackIdxToNote } from "../../components/App/PianoRoll/utils";
import { PianoRollLoop } from "../../types/Loop";
import { createNoteOffEvent, createNoteOnEvent, getKeyEventTimeSample, KeyEvent } from "../../utils/KeyEvent";
import { EngineState } from "../EngineState";
import { isClipUsageIntersectsWithBuffer } from "./IsClipUsageIntersectsWithBuffer";

function processMidiNotes(
    state: EngineState,
    clipId: string,
    target: KeyEvent[],
    trackStartOffsetBars: number,
    endTimeSinceLoopStartSamples: number | undefined,
    speed: number,
    sourceTimeStart: number | undefined,
    sourceTimeLength: number | undefined,
    bufferLength: number,
) {
    if (!state.isPlaying.get()) {
        return;
    }

    const clip = state.idToClips.get().get(clipId)!;

    if (clip.type !== "notes") {
        return;
    }

    const startOffsetBars = trackStartOffsetBars - state.loop.get().borders.timeStartBar;

    const now = state.timeSamples.get();
    clip.notes.forEach((note) => {

        if (
            sourceTimeStart !== undefined &&
            note.timelineTimeStartBars + note.timelineTimeLengthBars <= sourceTimeStart
        ) {
            return;
        }

        if (
            sourceTimeStart !== undefined &&
            sourceTimeLength !== undefined &&
            note.timelineTimeStartBars >= sourceTimeStart + sourceTimeLength
        ) {
            return;
        }

        const noteStartSamples = state.timeBarsToTimeSamples(
            startOffsetBars +
            note.timelineTimeStartBars * speed,
        );
        const noteEndSamples = state.timeBarsToTimeSamples(
            startOffsetBars +
            (note.timelineTimeStartBars +
                note.timelineTimeLengthBars) * speed,
        );
        if (
            endTimeSinceLoopStartSamples &&
            noteStartSamples > endTimeSinceLoopStartSamples) {
            return;
        }

        const noteInterval: Interval = [noteStartSamples, noteEndSamples];
        if (
            !state.currentBufferTimeSinceLoopStartSamples.get()
                .some((i) => intersect(i, noteInterval))
        ) {
            return;
        }
        const key = trackIdxToNote(note.rowId);

        const absStartTimeSamples =
            now + noteStartSamples - state.timeSinceLoopStartSamples.get();
        const absEndTimeSamples =
            now + noteEndSamples - state.timeSinceLoopStartSamples.get();

        // TODO: crop note if it lasts over the loop

        function add(
            start: number,
        ) {
            const end = start + absEndTimeSamples - absStartTimeSamples;
            if (start >= now + bufferLength) {
                return;
            }
            if (end < now) {
                return;
            }
            if (start >= now && start <= now + bufferLength) {
                target.push(createNoteOnEvent(
                    key,
                    127,
                    start,
                ));
                target.push(createNoteOffEvent(
                    key,
                    127,
                    end,
                ));
            }
        }

        add(absStartTimeSamples);
        add(absStartTimeSamples + state.loopLengthSamples.get());

    });

}

const channelIdToFutureEnds = new Map<string, KeyEvent[]>();

function withFutureEnds(
    state: EngineState,
    channelId: string,
    func: (target: KeyEvent[]) => KeyEvent[],
): KeyEvent[] {

    const bufferLength = state.project.get().bufferLength;
    const target: KeyEvent[] = channelIdToFutureEnds.get(channelId) ?? [];
    channelIdToFutureEnds.delete(channelId);

    func(target);

    const now = state.timeSamples.get();
    target.sort((a, b) => getKeyEventTimeSample(a) - getKeyEventTimeSample(b));
    const futureEnds: KeyEvent[] = [];
    while (target.length > 0) {
        const last = target.at(-1)!;
        const lastTime = getKeyEventTimeSample(last);
        if (lastTime >= now + bufferLength) {
            futureEnds.push(target.pop()!);
        } else {
            break;
        }
    }
    channelIdToFutureEnds.set(channelId, futureEnds);
    return target;
}

function findNotesForPlaylistChannel(
    state: EngineState,
    channelId: string,
): KeyEvent[] {
    return withFutureEnds(
        state,
        channelId,
        (target) => {
            const clipsUsagesOfChannel = state.clipsUsages.get().filter((u) =>
                u.rowId === channelId,
            );
            clipsUsagesOfChannel.forEach((usage) => {

                const intersects = isClipUsageIntersectsWithBuffer(state, usage);
                if (intersects === undefined) {
                    return;
                }

                /*
                 * TODO: check if usage intersects with current rendered buffer
                 */

                const speed = usage.timelineTimeLengthBars / usage.sourceTimeLength;

                processMidiNotes(
                    state,
                    usage.clipId,
                    target,
                    usage.timelineTimeStartBars -
                    usage.sourceTimeStart * speed,
                    intersects.usageEndTimeSinceLoopStartSamples,
                    speed,
                    usage.sourceTimeStart,
                    usage.sourceTimeLength,
                    state.project.get().bufferLength,
                );

            });
            return target;
        },
    );
}

function findNotesForPianoRollChannel(
    state: EngineState,
    channelId: string,
): KeyEvent[] {
    return withFutureEnds(
        state,
        channelId,
        (target) => {
            if (state.channelIdOfLoopedPianoRoll.get() !== channelId) {
                return [];
            }

            const l = state.loop.get() as PianoRollLoop;
            processMidiNotes(
                state,
                l.clipId,
                target,
                0,
                undefined,
                1,
                undefined,
                undefined,
                state.project.get().bufferLength,
            );
            return target;
        },
    );

}

export function findKeyEventsForChannel(
    state: EngineState,
    channelId: string,
): KeyEvent[] {
    const loopType = state.loop.get().type;
    if (loopType === "playlist") {
        return findNotesForPlaylistChannel(state, channelId);
    } else if (loopType === "piano-roll") {
        return findNotesForPianoRollChannel(state, channelId);
    }
    return [];
}
