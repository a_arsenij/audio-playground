import { Interval } from "@audio-playground/lib-common/math/Interval";
import { getOrPut } from "@audio-playground/lib-common/std/GetOrPut";
import {
    createMappedObservable,
    createStoredObservable,
    Observable,
    StoredObservable,
} from "@audio-playground/lib-common/std/Observable";

import { nameToPluginRenderer } from "../plugins/_PluginsRenderers";
import { Loop } from "../types/Loop";
import { Clip } from "../types/project/Clip";
import { ClipUsage } from "../types/project/ClipUsage";
import { Project } from "../types/project/Project";
import { EMPTY_PROJECT } from "./DefaultProject";

export type EngineState = {

    sharedBuffer: StoredObservable<SharedArrayBuffer>,
    project: Observable<Project>,
    timeSamples: Observable<number>,
    isPlaying: Observable<boolean>,
    playingStartedAtSamples: StoredObservable<number | undefined>,
    currentTimeBars: Observable<number | undefined>,
    idToClips: Observable<Map<string, Clip>>,
    loop: StoredObservable<Loop>,
    currentBufferTimeSinceLoopStartSamples: Observable<Interval[]>,
    timeSinceLoopStartSamples: Observable<number>,
    loopLengthSamples: Observable<number>,
    clipsUsages: Observable<ClipUsage[]>,
    channelIdOfLoopedPianoRoll: Observable<string>,

    // --- --- --- ---

    setProject: (project: Project) => void,
    incTimeSamples: () => void,
    timeBarsToTimeSamples: (timeBars: number) => number,
    timeSamplesToTimeBars: (timeSamples: number) => number,
    getCurrentTimeBars: (offsetSamples: number) => number,
    getStateForPlugin: (id: string, name: string) => unknown,

    // --- --- --- ---

    pluginsStates: Map<string, [string, unknown]>,

}

export function createEngineState(): EngineState {

    const memory = createStoredObservable<SharedArrayBuffer>(new SharedArrayBuffer(0));
    const project = createStoredObservable<Project>(EMPTY_PROJECT);

    function setProject(
        newProject: Project,
    ) {
        const pluginsIds = new Set(
            newProject.channels.map((c) => c.plugins).flat().map((p) => p.id),
        );
        [...pluginsStates.entries()].forEach(([id, [name, pstate]]) => {
            if (!pluginsIds.has(id)) {
                const p = nameToPluginRenderer(name);
                // @ts-ignore
                p.destroy(pstate);
                pluginsStates.delete(id);
            }
        });
        project.set(newProject);
    }

    const bufferLength = createMappedObservable(
        [project],
        ([p]) => p.bufferLength,
        "hard",
    );

    const sampleRate = createMappedObservable(
        [project],
        ([p]) => p.sampleRate,
        "hard",
    );

    const bpm = createMappedObservable(
        [project],
        ([p]) => p.bpm,
        "hard",
    );

    const samplesPerBeat = createMappedObservable(
        [sampleRate, bpm],
        ([sampleRate, bpm]) => sampleRate * 60 / bpm,
        "hard",
    );

    function timeBarsToTimeSamples(
        timeBars: number,
    ) {
        return Math.round(timeBars * samplesPerBeat.get());
    }

    function timeSamplesToTimeBars(
        timeSamples: number,
    ) {
        return timeSamples / samplesPerBeat.get();
    }

    const timeSamples = createStoredObservable<number>(0);

    const playingStartedAtSamples = createStoredObservable<number | undefined>(undefined);
    const isPlaying = playingStartedAtSamples.map(
        (v) => v !== undefined,
        "hard",
    );

    const clips = project.map((p) => p.clips, "hard");

    const idToClips = clips.map(
        (tracks) => new Map(tracks.map((t) => [t.id, t])),
        "hard",
    );

    const clipsUsages = project.map((p) => p.clipsUsages, "hard");

    const loop = createStoredObservable<Loop>({
        type: "playlist",
        borders: {
            timeStartBar: 0,
            timeEndBar: 8,
        },
    });

    const defaultChannelId = createMappedObservable(
        [project],
        ([p]) => p.defaultChannelId,
        "hard",
    );

    const channelIdOfLoopedPianoRoll = createMappedObservable(
        [defaultChannelId, loop, clipsUsages],
        ([defaultChannelId, loop, clipsUsages]) => {
            if (loop.type === "playlist") {
                return defaultChannelId;
            }
            const res = clipsUsages
                .find((u) => u.clipId === loop.clipId)
                ?.rowId;
            return res || defaultChannelId;
        },
        "hard",
    );
    const loopStartSamples = createMappedObservable(
        [loop, project],
        ([loop]) => timeBarsToTimeSamples(
            loop.borders.timeStartBar,
        ),
        "hard",
    );

    const loopEndSamples = createMappedObservable(
        [loop, project],
        ([loop]) => timeBarsToTimeSamples(
            loop.borders.timeEndBar,
        ),
        "hard",
    );

    const loopLengthSamples = createMappedObservable(
        [loopStartSamples, loopEndSamples],
        ([a, b]) => b - a,
        "hard",
    );

    const currentTimeBars = createMappedObservable(
        [
            timeSamples,
            playingStartedAtSamples,
            loopStartSamples,
            loopEndSamples,
        ],
        ([
            timeSamples,
            playingStartedAtSamples,
            loopStartSamples,
            loopEndSamples,
        ]) => {
            if (
                playingStartedAtSamples !== undefined &&
                loopStartSamples !== loopEndSamples
            ) {
                const timeSinceStartSamples = timeSamples - playingStartedAtSamples;
                const loopedTimeSinceStartSamples =
                    timeSinceStartSamples % (loopEndSamples - loopStartSamples);

                const currentTimeSamples = loopStartSamples + loopedTimeSinceStartSamples;
                return timeSamplesToTimeBars(
                    currentTimeSamples,
                );
            } else {
                return undefined;
            }
        },
        "hard",
    );

    function getCurrentTimeBars(
        offsetSamples: number,
    ): number {
        const ctb = currentTimeBars.get();
        if (ctb === undefined) {
            return loop.get().borders.timeStartBar;
        }
        return ctb + timeSamplesToTimeBars(offsetSamples);
    }

    const timeSincePlayStartSamples = createMappedObservable(
        [timeSamples, playingStartedAtSamples],
        ([timeSamples, playingStartedAtSamples]) => playingStartedAtSamples === undefined
            ? 0
            : timeSamples - playingStartedAtSamples,
        "hard",
    );
    const timeSinceLoopStartSamples = createMappedObservable(
        [timeSincePlayStartSamples, loopLengthSamples],
        ([timeSincePlayStartSamples, loopLengthSamples]) =>
            timeSincePlayStartSamples % loopLengthSamples,
        "hard",
    );

    const currentBufferTimeSinceLoopStartSamples: Observable<Interval[]> = createMappedObservable(
        [bufferLength, timeSamples, playingStartedAtSamples, loopLengthSamples],
        ([bufferLength, timeSamples, playingStartedAtSamples, loopLengthSamples]) => {
            if (playingStartedAtSamples === undefined) {
                return [] as Interval[];
            }
            const timeSinceStartSamples = (
                timeSamples - playingStartedAtSamples
            ) % loopLengthSamples;
            if (timeSinceStartSamples + bufferLength < loopLengthSamples) {
                return [
                    [
                        timeSinceStartSamples,
                        timeSinceStartSamples + bufferLength,
                    ],
                ] as Interval[];
            } else {
                return [
                    [
                        timeSinceStartSamples,
                        loopLengthSamples - 1,
                    ],
                    [
                        0,
                        (timeSinceStartSamples + bufferLength) % loopLengthSamples,
                    ],
                ] as Interval[];
            }
        },
        "hard",
    );

    const pluginsStates = new Map<string, [string, unknown]>();

    function getStateForPlugin(
        id: string,
        name: string,
    ): unknown {
        return getOrPut(
            pluginsStates,
            id,
            () => [name, nameToPluginRenderer(name).createState({
                sampleRate: sampleRate.get(),
                bufferLength: bufferLength.get(),
            })] as [string, unknown],
        )[1];
    }

    const incTimeSamples = () => {
        timeSamples.set((prev) => prev + bufferLength.get());
    };

    return {
        sharedBuffer: memory,
        project,
        timeSamples,
        isPlaying,
        playingStartedAtSamples,
        currentTimeBars,
        idToClips,
        loop,
        currentBufferTimeSinceLoopStartSamples,
        timeSinceLoopStartSamples,
        loopLengthSamples,
        clipsUsages,
        channelIdOfLoopedPianoRoll,
        // --- --- --- ---
        setProject,
        incTimeSamples,
        timeBarsToTimeSamples,
        timeSamplesToTimeBars,
        getCurrentTimeBars,
        getStateForPlugin,
        // --- --- --- ---
        pluginsStates,
    };

}
