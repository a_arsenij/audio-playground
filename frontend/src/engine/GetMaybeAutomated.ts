import { getOrPut } from "@audio-playground/lib-common/std/GetOrPut";

import { MaybeAutomated } from "../types/Plugin";
import { AutomationData, Channel } from "../types/project/Channel";
import { AutomationClip, Clip } from "../types/project/Clip";
import { ClipUsage } from "../types/project/ClipUsage";
import { createPiecewiseFunction, PiecewiseFunctionData } from "../utils/SvgGraphFunctions";
import { EngineState } from "./EngineState";


const piecewiseFunctionsCache = new WeakMap<
    PiecewiseFunctionData,
    (x: number) => number
>();

function getAutomationClipValue(
    clip: AutomationClip,
    usage: ClipUsage,
    timeBars: number,
): number {

    const timeRelativeToUsage = (timeBars - usage.timelineTimeStartBars) /
        usage.timelineTimeLengthBars;

    const timeRelativeToClip = timeRelativeToUsage * usage.sourceTimeLength + usage.sourceTimeStart;

    const func = getOrPut(
        piecewiseFunctionsCache,
        clip.piecewiseFunction,
        () => createPiecewiseFunction(clip.piecewiseFunction),
    );

    if (timeRelativeToClip <= usage.sourceTimeStart) {
        return func(usage.sourceTimeStart);
    }
    if (timeRelativeToClip >= usage.sourceTimeStart + usage.sourceTimeLength) {
        return func(usage.sourceTimeStart + usage.sourceTimeLength);
    }
    return func(timeRelativeToClip);

}


function createMaybeAutomatedFromDefaultValue(
    defaultValue: number,
): MaybeAutomated {
    return () => defaultValue;
}

function createMaybeAutomatedFromAutomationClips(
    clipsUsages: ClipUsage[],
    idToClips: Map<string, Clip>,
    minValue: number,
    maxValue: number,
): MaybeAutomated {
    const diff = maxValue - minValue;

    return (timeBars: number) => {

        function getAutomationClipValueForUsage(
            usage: ClipUsage,
        ): number {
            const clip = idToClips.get(usage.clipId) as AutomationClip;
            return getAutomationClipValue(
                clip,
                usage,
                timeBars,
            ) * diff + minValue;
        }

        const first = clipsUsages[0]!;
        if (timeBars <= first.timelineTimeStartBars + first.timelineTimeLengthBars) {
            return getAutomationClipValueForUsage(first);
        }

        for (let i = 1; i < clipsUsages.length; i++) {
            const usage = clipsUsages[i]!;
            if (timeBars < usage.timelineTimeStartBars) {
                return getAutomationClipValueForUsage(
                    clipsUsages[i - 1]!,
                );
            }
        }

        const last = clipsUsages.at(-1)!;
        return getAutomationClipValueForUsage(last);

    };

}

export function getMaybeAutomated(
    state: EngineState,
    channel: Channel,
    automation: AutomationData,
    minValue: number,
    maxValue: number,
    defaultValue: number,
): MaybeAutomated {

    const automationRow = channel.automations.find((a) =>
        automation.type === "pan" && a.type === "pan" ||
        automation.type === "volume" && a.type === "volume" ||
        (
            automation.type === "plugin" && a.type === "plugin" &&
            automation.pluginInstanceId === a.pluginInstanceId && automation.knobId === a.knobId
        ),
    );
    if (!automationRow) {
        return createMaybeAutomatedFromDefaultValue(defaultValue);
    }
    const automationClipsUsages = state.clipsUsages.get().filter(
        (u) => u.rowId === automationRow.id,
    );
    if (automationClipsUsages.length === 0) {
        return createMaybeAutomatedFromDefaultValue(defaultValue);
    }
    return createMaybeAutomatedFromAutomationClips(
        automationClipsUsages,
        state.idToClips.get(),
        minValue,
        maxValue,
    );

}
