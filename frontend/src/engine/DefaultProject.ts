import { generateId } from "@audio-playground/lib-common/std/Id";

import { Project } from "../types/project/Project";
import { checkNoOrphanRoutesInProject } from "../utils/projectChanges/checkNoOrphanRoutes";

export const MASTER_CHANNEL_ID = "master";

const DEFAULT_BPM = 140;
const DEFAULT_BUFFER_SIZE = 128;
export const DEFAULT_SAMPLE_RATE = 44100;

export const MASTER_INPUT_LEFT = "master.signalInLeft";
export const MASTER_INPUT_RIGHT = "master.signalInRight";
export const MASTER_OUTPUT_LEFT = "master.signalOutLeft";
export const MASTER_OUTPUT_RIGHT = "master.signalOutRight";

export const EMPTY_PROJECT: Project = checkNoOrphanRoutesInProject({
    bpm: DEFAULT_BPM,
    bufferLength: DEFAULT_BUFFER_SIZE,
    defaultChannelId: MASTER_CHANNEL_ID,
    sampleRate: DEFAULT_SAMPLE_RATE,
    channels: [
        {
            id: MASTER_CHANNEL_ID,
            title: "Master",
            plugins: [],
            volume: 1,
            pan: 0,
            muted: false,
            automations: [],
            inputMidiPorts: [{
                id: "master.midiIn",
                title: "Midi",
            }],
            outputMidiPorts: [],
            inputSignalPorts: [
                {
                    id: MASTER_INPUT_LEFT,
                    title: "Left",
                },
                {
                    id: MASTER_INPUT_RIGHT,
                    title: "Right",
                },
            ],
            outputSignalPorts: [
                {
                    id: MASTER_OUTPUT_LEFT,
                    title: "Left",
                },
                {
                    id: MASTER_OUTPUT_RIGHT,
                    title: "Right",
                },
            ],
            signalPluginsRouting: [],
            midiPluginsRouting: [],
        },
    ],
    clips: [
        {
            type: "notes",
            id: generateId(),
            title: "Pattern 1",
            notes: [],
        },
    ],
    clipsUsages: [],
    channelsRouting: [],
});
