import { ArrayBufferAllocator } from "@audio-playground/lib-common/std/ArrayBufferAllocator";
import { loadMultipleBlocks, saveMultipleBlocks } from "@audio-playground/lib-common/std/Buffers";

import { Project } from "../types/project/Project";


export function saveProjectState(
    project: Project,
    allocator: ArrayBufferAllocator,
): ArrayBuffer {

    const projectState = new TextEncoder().encode(JSON.stringify(project)).buffer;
    const allocatorState = new TextEncoder().encode(allocator.getState()).buffer;
    const binary = allocator.getBuffer();

    return saveMultipleBlocks([
        projectState,
        allocatorState,
        binary,
    ], 3);

}

export function loadProjectState(
    res: ArrayBuffer,
    setProject: (p: Project) => void,
    allocator: ArrayBufferAllocator,
) {
    const blocks = loadMultipleBlocks(res, 3);
    const projectText = new TextDecoder().decode(blocks[0]);
    const allocatorText = new TextDecoder().decode(blocks[1]);
    const binary = blocks[2];
    allocator.setBuffer(binary);
    allocator.setState(allocatorText);

    setProject(JSON.parse(projectText));
}
