import { nameToPluginRenderer } from "../plugins/_PluginsRenderers";
import { EngineState } from "./EngineState";

export function halt(
    state: EngineState,
) {
    state.project.get().channels.forEach((c) => {
        c.plugins.forEach((p) => {
            const pstate = state.pluginsStates.get(p.id);
            if (!pstate) {
                return;
            }
            const plugin = nameToPluginRenderer(p.name);
            plugin.destroy(
                pstate[1]!,
                // @ts-ignore
                p.settings,
            );
        });
    });
    state.pluginsStates.clear();
}
