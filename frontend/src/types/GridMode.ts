export type GridMode =
    | "drag"
    | "edit"
    | "erase"
;

export const GRID_MODES: GridMode[] = [
    "drag",
    "edit",
    "erase",
];
