export type ResizeMode =
    | "crop"
    | "stretch"
;

export const RESIZE_MODES: ResizeMode[] = [
    "crop",
    "stretch",
];
