export type ZoomMode =
    | "zoomIn"
    | "zoomOut"
    | "zoomReset"
;
