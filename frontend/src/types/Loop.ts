export type LoopBorders = {
    timeStartBar: number,
    timeEndBar: number,
}

export type GenericLoop = {
    borders: LoopBorders,
}

export type WaveLoop = GenericLoop & {
    type: "wave",
    clipId: string,
}

export type PianoRollLoop = GenericLoop & {
    type: "piano-roll",
    clipId: string,
}

export type PlaylistLoop = GenericLoop & {
    type: "playlist",
}

export type Loop = WaveLoop | PianoRollLoop | PlaylistLoop;
