import { Loop } from "./Loop";
import { Project } from "./project/Project";

export type SetSharedMemory = {
    type: "setSharedMemory",
    memory: SharedArrayBuffer,
}
export type SetProject = {
    type: "setProject",
    project: Project,
}
export type PlayMessage = {
    type: "play",
}
export type StopMessage = {
    type: "stop",
}
export type SetLoop = {
    type: "setLoop",
    loop: Loop,
}
export type PressKey = {
    type: "pressKey",
    key: number,
}
export type ReleaseKey = {
    type: "releaseKey",
    key: number,
}
export type SetMasterBufferOnlyMessage = {
    type: "setMasterBufferOnly",
    masterBuffer: SharedArrayBuffer,
}
export type UiToMasterMessage =
    | SetSharedMemory
    | SetProject
    | PlayMessage
    | StopMessage
    | SetLoop
    | PressKey
    | ReleaseKey
    | SetMasterBufferOnlyMessage
;
export type BufferRenderedMessage = {
    type: "bufferRendered",
    timeBars: number | undefined,
    channelsLevels: number[][],
}
export type IAmReadyMessage = {
    type: "iAmReady",
}
export type MasterToUiMessage =
    | BufferRenderedMessage
    | IAmReadyMessage
 ;

export type SetMasterBufferMessage = {
    type: "setMasterBuffer",
    masterBuffer: SharedArrayBuffer,
    bufferLength: number,
    sampleRate: number,
}
export type UiToDumbWorkletMessage =
    | SetMasterBufferMessage
;
