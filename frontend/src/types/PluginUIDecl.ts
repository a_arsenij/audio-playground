import React from "react";

import { PluginCommonSettings, PluginUIProps } from "./Plugin";

export type PluginUIDecl<SETTINGS extends PluginCommonSettings> = {
    name: string,
    UI: React.FC<PluginUIProps<SETTINGS>>,
    Icon: React.FC<{}>,
}
