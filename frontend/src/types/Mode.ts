export const UI_MODES = [
    "piano-roll",
    "playlist",
    "mixer",
    "plugin",
] as const;

export type UiMode = (typeof UI_MODES)[number];
