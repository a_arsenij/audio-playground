import { ArrayBufferAllocator } from "@audio-playground/lib-common/std/ArrayBufferAllocator";
import { PooledArray } from "@audio-playground/lib-common/std/ArraysPool";
import { SetState } from "@audio-playground/lib-frontend/std/SetState";

import { KeyEvent } from "../utils/KeyEvent";
import { createNewChannelPort } from "../utils/projectChanges/addChannel";
import { ChannelPort } from "./project/Channel";

export type PluginCommonSettings = {
    inputSignalChannels: ChannelPort[],
    outputSignalChannels: ChannelPort[],
    inputMidiChannels: ChannelPort[],
    outputMidiChannels: ChannelPort[],
}

export function createDefaultInstrumentPluginPorts() {
    return {
        inputMidiChannels: [
            createNewChannelPort("Midi"),
        ],
        outputMidiChannels: [],
        inputSignalChannels: [
            createNewChannelPort("Left"),
            createNewChannelPort("Right"),
        ],
        outputSignalChannels: [
            createNewChannelPort("Left"),
            createNewChannelPort("Right"),
        ],
    };
}

export function createDefaultEffectPluginPorts() {
    return {
        inputMidiChannels: [],
        outputMidiChannels: [],
        inputSignalChannels: [
            createNewChannelPort("Left"),
            createNewChannelPort("Right"),
        ],
        outputSignalChannels: [
            createNewChannelPort("Left"),
            createNewChannelPort("Right"),
        ],
    };
}

export type PluginUIProps<SETTINGS extends PluginCommonSettings> = {
    pluginId: string,
    pluginSettings: SETTINGS,
    setPluginSettings: SetState<SETTINGS>,
    isAutomated: (name: string) => boolean,
    setIsAutomated: (name: string, isAutomated: boolean) => void,
    sampleRate: number,
    bufferLength: number,
    allocator: ArrayBufferAllocator,
}

export type CreateSettingsArg = {
    pluginInstanceId: string,
}

export type CreateStateArg = {
    sampleRate: number,
    bufferLength: number,
}

export type MaybeAutomated = (timeBars: number) => number

export type RenderBufferArg<
    SETTINGS,
    STATE = unknown,
> = {
    timeSample: number,
    input: PooledArray[],
    output: PooledArray[],
    sampleRate: number,
    bufferLength: number,
    keyEvents: KeyEvent[],
    pluginSettings: SETTINGS,
    pluginState: STATE,
    getMaybeAutomated: (
        knobId: string,
        minValue: number,
        maxValue: number,
        defaultValue: number,
    ) => MaybeAutomated,
    getCurrentTimeBars: (offsetSamples: number) => number,
    sharedBuffer: ArrayBufferLike,
}
