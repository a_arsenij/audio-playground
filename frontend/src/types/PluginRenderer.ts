import { CreateSettingsArg, CreateStateArg, PluginCommonSettings, RenderBufferArg } from "./Plugin";

export type PluginRenderer<SETTINGS extends PluginCommonSettings, STATE extends object> = {
    name: string,
    createSettings: (arg: CreateSettingsArg) => SETTINGS,
    createState: (arg: CreateStateArg) => STATE,
    destroy: (state: STATE) => void,
    renderBuffer: (arg: RenderBufferArg<SETTINGS, STATE>) => void,
}
