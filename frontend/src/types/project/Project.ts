import { Channel, PortsRoute } from "./Channel";
import { Clip } from "./Clip";
import { ClipUsage } from "./ClipUsage";

export type Project = {

    bpm: number,
    bufferLength: number,
    sampleRate: number,
    clips: Clip[],
    channels: Channel[],
    clipsUsages: ClipUsage[],
    defaultChannelId: string,
    channelsRouting: PortsRoute[],


}
