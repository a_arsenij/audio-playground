import { BlobRef } from "@audio-playground/lib-common/std/BlobRef";

import { PianoRollNote } from "../../components/App/TimelineWithClips/PianoRollNote";
import { PiecewiseFunctionData } from "../../utils/SvgGraphFunctions";

type RootClip = {
    id: string,
    title: string,
}

export type NotesClip = RootClip & {
    type: "notes",
    notes: PianoRollNote[],
}

export type AutomationClip = RootClip & {
    type: "automation",
    piecewiseFunction: PiecewiseFunctionData,
}

export type WaveClip = RootClip & {
    type: "wave",
    wave: [BlobRef] | [BlobRef, BlobRef],
    sampleRate: number,
}

export type Clip =
    | NotesClip
    | AutomationClip
    | WaveClip
;
