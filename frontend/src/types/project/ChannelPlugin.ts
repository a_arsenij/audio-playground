import { PluginCommonSettings } from "../Plugin";

export type ChannelPlugin = {
    id: string,
    name: string,
    settings: PluginCommonSettings,
}
