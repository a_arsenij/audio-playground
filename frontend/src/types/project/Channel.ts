import { ChannelPlugin } from "./ChannelPlugin";

export type ChannelPort = {
    id: string,
    title: string,
}

export type PortsRoute = {
    from: string,
    to: string,
}

export function createRoute(
    from: ChannelPort,
    to: ChannelPort,
): PortsRoute {
    return {
        from: from.id,
        to: to.id,
    };
}

export type Channel = {
    id: string,
    title: string,
    volume: number,
    muted: boolean,
    pan: number,
    plugins: ChannelPlugin[],
    automations: Automation[],
    inputSignalPorts: ChannelPort[],
    outputSignalPorts: ChannelPort[],
    inputMidiPorts: ChannelPort[],
    outputMidiPorts: ChannelPort[],
    signalPluginsRouting: PortsRoute[],
    midiPluginsRouting: PortsRoute[],
}

export type AutomationData =
    | { type: "pan" }
    | { type: "volume" }
    | {
    type: "plugin",
    pluginInstanceId: string,
    knobId: string,
}

export type Automation = {id: string} & AutomationData;
