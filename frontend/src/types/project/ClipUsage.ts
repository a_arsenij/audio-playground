export type ClipUsage = {

    id: string,
    clipId: string,
    rowId: string,

    timelineTimeStartBars: number,
    timelineTimeLengthBars: number,

    sourceTimeStart: number,
    sourceTimeLength: number,

}
