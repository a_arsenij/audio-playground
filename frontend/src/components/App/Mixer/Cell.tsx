import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React, { CSSProperties, PropsWithChildren } from "react";

import styles from "./MixerLayout.module.css";

type CellProps = PropsWithChildren & {
    col?: number,
    colStart?: number,
    colEnd?: number,
    row?: number,
    rowStart?: number,
    rowEnd?: number,
    isConnection?: boolean,
}

export function Cell(props: CellProps) {
    const {
        children,
        col,
        colStart = 0,
        colEnd = 1000,
        row,
        rowStart = 0,
        rowEnd = 1000,
        isConnection,
    } = props;

    function getCoord(start: number, end: number, exact?: number) {
        if (exact) {
            return exact;
        }
        if (end === -1) {
            return `${start} / ${end}`;
        }
        return end > start ? `${start} / ${end}` : `${end} / ${start}`;
    }

    return <div
        className={clsx(styles.gridCell)}
        style={{
            "--col": getCoord(colStart, colEnd, col),
            "--row": getCoord(rowStart, rowEnd, row),
            "--height": isConnection ? rowEnd - rowStart : undefined,
        } as CSSProperties}
    >
        {children}
    </div>;
}
