import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React from "react";

import { ChannelPort } from "../../../types/project/Channel";
import styles from "./Port.module.css";

export type ChannelPortFull = ChannelPort & {
    type: "midi" | "signal",
    isInputDir: boolean,
}
type PortProps = ChannelPortFull & {
    onClickPort: (p: ChannelPortFull) => void,
    selectedPort?: string,
};

export function Port(props: PortProps) {
    const {
        onClickPort,
        type,
        selectedPort,
        id,
    } = props;
    const selected = selectedPort === id;
    return <div className={clsx(styles.port, selected && styles.selected)} onClick={() => {
        onClickPort(props);
    }}>
        {type[0]}
    </div>;
}
