import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import React, { useState } from "react";

import { Channel, PortsRoute } from "../../../types/project/Channel";
import { Project } from "../../../types/project/Project";
// import { useAddNewChannel } from "../../../utils/projectChanges/addChannel";
import { MixerLayout } from "./MixerLayout";

type MixerProps = {
    channelsLevels: number[][],
    channels: Channel[],
    setChannels: SetState<Channel[]>,
    setProject: SetState<Project>,
    defaultChannelId: string,
    setDefaultChannelId: (id: string) => void,
    openPlugin: (channelId: string, instanceId: string) => void,
    project: Project,
}

type PortWithCoords = {id: string; type: string; isInputDir: boolean};

export function Mixer(props: MixerProps) {
    const {
        // channelsLevels,
        channels,
        setChannels,
        setProject,
        // defaultChannelId,
        // setDefaultChannelId,
        // openPlugin,
        project,
    } = props;

    const [selectedPort, setSelectedPort] = useState<PortWithCoords | undefined>();

    // const addNewChannel = useAddNewChannel(setProject);

    function getChannelByPort(id: string) {
        return channels.find((ch) => {
            const allPorts = [
                ...ch.outputSignalPorts,
                ...ch.outputMidiPorts,
                ...ch.inputMidiPorts,
                ...ch.inputSignalPorts,
            ];
            return allPorts.some((p) => p.id === id);
        });
    }

    function getPluginChannelByPort(id: string) {
        let plugin;
        const channel = channels.find((ch) => {
            const plugin2 = ch.plugins.find((pl) => {
                const allPorts = [
                    ...pl.settings.inputMidiChannels,
                    ...pl.settings.outputMidiChannels,
                    ...pl.settings.inputSignalChannels,
                    ...pl.settings.outputSignalChannels,
                ];
                return allPorts.some((p) => p.id === id);
            });
            if (plugin2) {
                plugin = plugin2;
                return true;
            }
        });

        return { channel, plugin };
    }

    function createConnection(p1: PortWithCoords, p2: PortWithCoords, reverse?: boolean) {
        const dir = !!reverse;
        return p1.isInputDir === dir
            ? {
                from: p1.id,
                to: p2.id,
            }
            : {
                to: p1.id,
                from: p2.id,
            };
    }
    function removeConnection(connections: PortsRoute[], connection: PortsRoute) {
        return connections
            .filter((c) => !(c.from === connection.from && c.to === connection.to));
    }
    function connectOuterPorts(p1: PortWithCoords, p2: PortWithCoords) {
        if (p1.isInputDir === p2.isInputDir) {
            return;
        }
        const connections = project.channelsRouting;
        const newConnection = createConnection(p1, p2);
        const removedConnection = removeConnection(connections, newConnection);
        setProject((p) => {
            return {
                ...p,
                channelsRouting: removedConnection.length !== connections.length ? removedConnection : [...connections, newConnection],
            };
        });
    }
    function makeConnection(p1: PortWithCoords, p2: PortWithCoords) {
        const isP1Channel = getChannelByPort(p1.id);
        const isP2Channel = getChannelByPort(p2.id);
        if (!isP1Channel && !isP2Channel) {
            if (p1.isInputDir === p2.isInputDir) {
                return;
            }
            return createConnection(p1, p2);
        }

        if (isP1Channel && isP2Channel) {
            if (p1.isInputDir === p2.isInputDir) {
                return;
            }
            return createConnection(p1, p2, true);
        }

        if (p1.isInputDir !== p2.isInputDir) {
            return;
        }
        const chP = isP1Channel ? p1 : p2;
        const nonChP = isP1Channel ? p2 : p1;
        return createConnection(chP, nonChP, true);
    }


    function connectInnerPorts(p1: PortWithCoords, p2: PortWithCoords) {
        const isP1Channel = getChannelByPort(p1.id);
        const isP2Channel = getChannelByPort(p2.id);

        const p1Parents = getPluginChannelByPort(p1.id);
        const p2Parents = getPluginChannelByPort(p2.id);

        const p1Channel = isP1Channel || p1Parents.channel;
        const p2Channel = isP2Channel || p2Parents.channel;

        if (!p1Channel || !p2Channel || p1Channel !== p2Channel) {
            return;
        }
        const ch = p1Channel;

        const newConnection: PortsRoute | undefined = makeConnection(p1, p2);

        if (!newConnection) {
            return;
        }
        const type = p2.type;
        const chField = type === "midi" ? "midiPluginsRouting" : "signalPluginsRouting";

        const connections = ch[chField];
        const removedConnection = removeConnection(connections, newConnection);
        setChannels((chans) => {
            return chans.map((c) => {

                if (c.id !== ch.id) {
                    return c;
                }
                return {
                    ...c,
                    [chField]: removedConnection.length !== connections.length
                        ? removedConnection
                        : [...c[chField], newConnection],
                };
            });
        });
    }

    function onClickPort(port: PortWithCoords) {
        if (selectedPort === undefined) {
            setSelectedPort(port);
            return;
        }
        if (selectedPort.type !== port.type) {
            return;
        }
        const selectedPortChannel = getChannelByPort(selectedPort.id);
        const newPortChannel = getChannelByPort(port.id);

        if (selectedPortChannel && newPortChannel) {
            if (selectedPortChannel.id === newPortChannel.id) {
                connectInnerPorts(selectedPort, port);
            } else {
                connectOuterPorts(selectedPort, port);
            }
        } else {
            connectInnerPorts(selectedPort, port);
        }
        setSelectedPort(undefined);
    }

    const [expandedChannels, setExpandedChannels] = useState<string[]>([]);
    return <MixerLayout
        {...props}
        onClickPort={onClickPort}
        expandedChannels={expandedChannels}
        selectedPort={selectedPort?.id}
        toggleChannelExpanded={(id) => {
            setExpandedChannels((ech) => {
                if (ech.includes(id)) {
                    return ech.filter((idid) => idid !== id);
                } else {
                    return [...ech, id];
                }
            });
        }}
    />;
}

