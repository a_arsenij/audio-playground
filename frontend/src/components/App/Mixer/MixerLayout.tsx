import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import { Channel, ChannelPort } from "../../../types/project/Channel";
import { Project } from "../../../types/project/Project";
import { ChannelComp } from "../Channel/Channel";
import { Cell } from "./Cell";
import { Connection, InnerChannelConnection } from "./Connection";
import styles from "./MixerLayout.module.css";
import { PluginContent } from "./Plugin";
import { ChannelPortFull, Port } from "./Port";

type PortWithCoords = {id: string; type: string; isInputDir: boolean};

type MixerLayoutProps = {
    channelsLevels: number[][],
    channels: Channel[],
    setChannels: SetState<Channel[]>,
    setProject: SetState<Project>,
    defaultChannelId: string,
    setDefaultChannelId: (id: string) => void,
    openPlugin: (channelId: string, instanceId: string) => void,
    project: Project,
    expandedChannels: string[],
    selectedPort?: string,
    toggleChannelExpanded: (id: string) => void,
    onClickPort: (p: PortWithCoords) => void,
}

export function MixerLayout(props: MixerLayoutProps) {
    const {
        channels,
        expandedChannels,
        channelsLevels,
        setProject,
        defaultChannelId,
        setDefaultChannelId,
        openPlugin,
        toggleChannelExpanded,
        project,
        onClickPort,
        selectedPort,
    } = props;


    let x = 1;

    const channelsMaps = channels.map((ch) => {
        const expanded = expandedChannels.includes(ch.id);

        const channelStartX = x;
        const channelPorts: (ChannelPortFull & { x: number })[] = [];
        let channelCenterStartX;
        let channelCenterEndX;
        if (expanded) {
            channelCenterStartX = x;
            x++;
            channelCenterEndX = x;
            ch.outputMidiPorts?.forEach((p) => {
                channelPorts.push({
                    ...p,
                    x,
                    type: "midi",
                    isInputDir: false,
                });
                x++;
            });
            ch.outputSignalPorts?.forEach((p) => {
                channelPorts.push({
                    ...p,
                    x,
                    type: "signal",
                    isInputDir: false,
                });
                x++;
            });
        } else {
            ch.outputMidiPorts?.forEach((p) => {
                channelPorts.push({
                    ...p,
                    x,
                    type: "midi",
                    isInputDir: false,
                });
                x++;
            });
            ch.outputSignalPorts?.forEach((p) => {
                channelPorts.push({
                    ...p,
                    x,
                    type: "signal",
                    isInputDir: false,
                });
                x++;
            });
            channelCenterStartX = x;
            x++;
            channelCenterEndX = x;
        }

        const plugins = expanded
            ? ch.plugins.map((pl) => {
                const inMidi = pl.settings.inputMidiChannels;
                const outMidi = pl.settings.outputMidiChannels;
                const inSignal = pl.settings.inputSignalChannels;
                const outSignal = pl.settings.outputSignalChannels;

                const pluginPorts: (ChannelPortFull & { x: number })[] = [];

                const startPlPos = x;

                outMidi.forEach((p) => {
                    pluginPorts.push({
                        ...p,
                        x,
                        type: "midi",
                        isInputDir: false,
                    });
                    x++;
                });
                outSignal.forEach((p) => {
                    pluginPorts.push({
                        ...p,
                        x,
                        type: "signal",
                        isInputDir: false,
                    });
                    x++;
                });
                const centerStartX = x;
                x++;
                const centerEndX = x;

                inSignal.forEach((p) => {
                    pluginPorts.push({
                        ...p,
                        x,
                        type: "signal",
                        isInputDir: true,
                    });
                    x++;
                });
                inMidi.forEach((p) => {
                    pluginPorts.push({
                        ...p,
                        x,
                        type: "midi",
                        isInputDir: true,
                    });
                    x++;
                });

                return {
                    ...pl,
                    pluginPorts,
                    channelId: ch.id,
                    startX: startPlPos,
                    endX: x,
                    centerStartX,
                    centerEndX,
                };
            })
            : [];

        ch.inputSignalPorts?.forEach((p) => {
            channelPorts.push({
                ...p,
                x,
                type: "signal",
                isInputDir: true,
            });
            x++;
        });
        ch.inputMidiPorts?.forEach((p) => {
            channelPorts.push({
                ...p,
                x,
                type: "midi",
                isInputDir: true,
            });
            x++;
        });

        return {
            ...ch,
            startX: channelStartX,
            endX: x,
            channelPorts,
            centerStartX: channelCenterStartX,
            centerEndX: channelCenterEndX,
            channelPlugins: plugins,
        };
    });

    function getChannelPortCoordsById(id: string) {
        for (const ch of channelsMaps) {
            const port = ch.channelPorts.find((p) => p.id === id);
            if (port) {
                return port;
            }
        }
    }

    const channelConnections = project.channelsRouting
        .map((con) => {

            const fr = getChannelPortCoordsById(con.from);
            const to = getChannelPortCoordsById(con.to);
            return {
                fr, to,
            };
        })
        .filter(
            ({ fr, to }) => fr && to,
        ) as ({ fr: ChannelPort & { x: number }; to: ChannelPort & { x: number } })[];
    const channelPortsY = channelConnections.length + 2;


    const channelConnectionelements = channelConnections
        .map((con, i) => {
            return <>
                <Connection {...con} i={i} baseY={channelPortsY}/>
            </>;
        });
    const channelElements = channelsMaps.flatMap((channel, idx) => {

        const innerConnections = [
            ...channel.midiPluginsRouting,
            ...channel.signalPluginsRouting,
        ].map((con) => {
            function getPortCoordsById(id: string) {
                const chPort = getChannelPortCoordsById(id);
                if (chPort) {
                    return { ...chPort, isChannelPort: true };
                }
                for (const pl of channel.channelPlugins) {
                    const port = pl.pluginPorts.find((p) => p.id === id);
                    if (port) {
                        return { ...port, isChannelPort: false };
                    }
                }
            }

            const fr = getPortCoordsById(con.from);
            const to = getPortCoordsById(con.to);
            return {
                fr, to,
            };
        }).filter(
            ({ fr, to }) => fr && to,
        ) as ({
            fr: ChannelPort & { isChannelPort: boolean; x: number },
            to: ChannelPort & { isChannelPort: boolean; x: number },
        })[];

        const pluginPortsY = channelPortsY + 3 + innerConnections.length;

        const innerConnectionsElements = innerConnections
            .map((con, i) => {
                return <InnerChannelConnection {...con} i={i} upperLayerY={channelPortsY} baseY={pluginPortsY}/>;
            });

        const ports = channel.channelPorts.map((p) => {
            return <Cell col={p.x} row={channelPortsY}>
                <Port {...p} selectedPort={selectedPort} onClickPort={onClickPort}/>
            </Cell>;
        });
        const center = <Cell colStart={channel.centerStartX} colEnd={channel.centerEndX} row={channelPortsY}></Cell>;
        const channelBox = <Cell colStart={channel.startX} colEnd={channel.endX} rowStart={channelPortsY + 1}><div className={styles.channelBox}></div></Cell>;
        const channelText = <Cell colStart={channel.centerStartX} colEnd={channel.centerEndX}
            rowStart={channelPortsY + 1}>
            <ChannelComp
                key={channel.id}
                channel={channel}
                channelIndex={idx}
                channelsNumber={channels.length}
                channelLevel={channelsLevels[idx] || [0, 0]}
                setProject={setProject}
                isDefaultChannel={channel.id === defaultChannelId}
                setDefault={setDefaultChannelId}
                openPlugin={(instanceId) => openPlugin(
                    channel.id,
                    instanceId,
                )}
                expand={() => {
                    toggleChannelExpanded(channel.id);
                }}
            />
        </Cell>;

        const plugins = channel.channelPlugins?.map((pl, i) => {
            const pls = pl.pluginPorts.map((p) => {
                return <Cell col={p.x} row={pluginPortsY}>
                    <Port {...p} selectedPort={selectedPort} onClickPort={onClickPort}/>
                </Cell>;
            });
            const content = <Cell rowStart={pluginPortsY + 1} colStart={pl.centerStartX} colEnd={pl.centerEndX}>
                <PluginContent
                    {...pl}
                    idx={i}
                    setProject={setProject}
                    openPlugin={openPlugin}
                />
            </Cell>;
            const pluginBox = <Cell colStart={pl.startX} colEnd={pl.endX} rowStart={pluginPortsY + 1}><div className={styles.channelBox}></div></Cell>;
            return [pluginBox, pls, content];
        });
        return [channelBox, innerConnectionsElements, ports, center, channelText, plugins];
    });
    return <div className={styles.mixer}>
        <div className={styles.grid}>
            {channelConnectionelements}
            {channelElements}

        </div>
    </div>;
}

