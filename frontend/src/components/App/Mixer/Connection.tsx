import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React from "react";

import { ChannelPort } from "../../../types/project/Channel";
import { Cell } from "./Cell";
import styles from "./MixerLayout.module.css";

type InnerConnectionProps = {
    fr: ChannelPort & { isChannelPort: boolean; x: number },
    to: ChannelPort & { isChannelPort: boolean; x: number },
    upperLayerY: number,
    baseY: number,
    i: number,
}

export function InnerChannelConnection(props: InnerConnectionProps) {
    const {
        fr,
        to,
        upperLayerY,
        baseY,
        i,
    } = props;
    if (fr.isChannelPort || to.isChannelPort) {
        return <>
            <Cell colStart={to.x} colEnd={fr.x} rowStart={upperLayerY} rowEnd={baseY - i - 1}
                isConnection={true}>
                <div
                    className={clsx(styles.connection, styles.pluginToChanelConnectionTop, to.isChannelPort && styles.flipX)}></div>
            </Cell>
            <Cell colStart={to.x} colEnd={fr.x} rowStart={baseY - i - 1} rowEnd={baseY}
                isConnection={true}>
                <div
                    className={clsx(styles.connection, styles.pluginToChanelConnectionBottom, to.isChannelPort && styles.flipX)}></div>
            </Cell>
        </>;
    }
    return <>
        <Connection {...props}/>
    </>;
}

type ConnectionProps = {
    fr: ChannelPort & { x: number },
    to: ChannelPort & { x: number },
    baseY: number,
    i: number,
}
export function Connection(props: ConnectionProps) {
    const {
        fr,
        to,
        baseY,
        i,
    } = props;
    return <Cell colStart={to.x} colEnd={fr.x} rowStart={baseY - i - 1} rowEnd={baseY}
        isConnection={true}>
        <div className={styles.connection}></div>
    </Cell>;
}
