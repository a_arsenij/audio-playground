import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import { Project } from "../../../types/project/Project";
import { addNthPlugin } from "../../../utils/projectChanges/addPlugin";
import { removeNthPlugin } from "../../../utils/projectChanges/removePlugin";
import { Button } from "../../Common/Button/Button";
import { Svg18Cog } from "../../Common/GeneratedIcons/18/Cog";
import { Svg18CommonDelete } from "../../Common/GeneratedIcons/18/Common/Delete";
import { AddPluginButton } from "../AddPluginButton/AddPluginButton";
import styles from "./Plugin.module.css";

type PluginContentProps = {
    name: string,
    setProject: SetState<Project>,
    channelId: string,
    idx: number,
    openPlugin: (channelId: string, instanceId: string) => void,
    id: string,
}

export function PluginContent(props: PluginContentProps) {
    const {
        name,
        setProject,
        channelId,
        idx,
        openPlugin,
        id,
    } = props;

    return <>
        <div className={styles.pluginBox}
        >
            <div className={styles.pluginName}>
                {name}
                <div className={styles.pluginButtons}>
                    <Button
                        onClick={() => removeNthPlugin(
                            setProject,
                            channelId,
                            idx,
                        )}
                        key={"Delete plugin"}
                        title={"Delete plugin"}
                        iconLeft={Svg18CommonDelete}
                        style={"ghost"}
                    />
                    <Button
                        onClick={() => openPlugin(
                            channelId,
                            id,
                        )}
                        key={"Configure plugin"}
                        title={"Configure plugin"}
                        iconLeft={Svg18Cog}
                        style={"ghost"}
                    />
                    <AddPluginButton
                        key={"Add plugin after"}
                        title={"Add plugin after"}
                        onAddPlugin={(p) => addNthPlugin(
                            setProject,
                            channelId,
                            p,
                            idx + 1,
                        )}
                    />
                    <AddPluginButton
                        key={"Add plugin before"}
                        title={"Add plugin before"}
                        onAddPlugin={(p) => addNthPlugin(
                            setProject,
                            channelId,
                            p,
                            idx,
                        )}
                    />
                </div>
            </div>
        </div>
    </>;
}
