import React from "react";

import { PLUGINS_UIS_LIST } from "../../../plugins/_PluginsUIs";
import { Button } from "../../Common/Button/Button";
import { Dialog } from "../../Common/Dialog/Dialog";
import styles from "./AddPluginDialog.module.css";

type AddPluginDialogProps = {
    open: boolean,
    onClose: () => void,
    onAddPlugin: (pluginName: string) => void,
}

export function AddPluginDialog({
    open,
    onClose,
    onAddPlugin,
}: AddPluginDialogProps) {
    return <Dialog
        title={"Add plugin"}
        onClose={onClose}
        open={open}
    >
        <div className={styles.addPluginDialog}>
            {PLUGINS_UIS_LIST.map((p) =>
                <Button
                    key={p.name}
                    // @ts-ignore
                    onClick={() => onAddPlugin(p.name)}
                    iconLeft={p.Icon}
                    title={p.name}
                    showBothIconAndTitle={true}
                    stretch={true}
                    align={"left"}
                />,
            )}
        </div>
    </Dialog>;
}
