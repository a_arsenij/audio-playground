import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import { nameToPluginUi } from "../../../plugins/_PluginsUIs";
import { PluginCommonSettings } from "../../../types/Plugin";
import { ChannelPlugin } from "../../../types/project/ChannelPlugin";
import styles from "./PluginUI.module.css";

type PluginUIProps = {
    channelPlugin: ChannelPlugin,
    isAutomated: (name: string) => boolean,
    setIsAutomated: (
        name: string,
        isAutomated: boolean
    ) => void,
    setPluginSettings: SetState<PluginCommonSettings>,
}

export function PluginUI(props: PluginUIProps) {

    const plugin = nameToPluginUi(props.channelPlugin.name);
    const UI = plugin.UI;

    return <div className={styles.pluginUI}>

        <UI
            pluginId={props.channelPlugin.id}
            // @ts-ignore
            pluginSettings={props.channelPlugin.settings}
            // @ts-ignore
            setPluginSettings={props.setPluginSettings}
            isAutomated={props.isAutomated}
            setIsAutomated={props.setIsAutomated}
        />

    </div>;
}
