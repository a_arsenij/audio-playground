import {
    SetState,
    setterByKey,
    useSetForId,
} from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback } from "react";

import { Channel } from "../../../types/project/Channel";
import { Project } from "../../../types/project/Project";
import { isChannelAllowedToBeDeleted, removeChannel } from "../../../utils/projectChanges/removeChannel";
import { Button } from "../../Common/Button/Button";
import { Svg18ArrowDown } from "../../Common/GeneratedIcons/18/Arrow/Down";
import { Svg18ArrowLeft } from "../../Common/GeneratedIcons/18/Arrow/Left";
import { Svg18ArrowRight } from "../../Common/GeneratedIcons/18/Arrow/Right";
import { Svg18ArrowUp } from "../../Common/GeneratedIcons/18/Arrow/Up";
import { Svg18ChannelsMute } from "../../Common/GeneratedIcons/18/Channels/Mute";
import { Svg18ChannelsUnmute } from "../../Common/GeneratedIcons/18/Channels/Unmute";
import { Svg18CommonDelete } from "../../Common/GeneratedIcons/18/Common/Delete";
import { Svg18Keys } from "../../Common/GeneratedIcons/18/Keys";
import styles from "./ChannelOps.module.css";

type ChannelOpsProps = {
    mode: "vertical" | "horizontal",
    channel: Channel,
    channelIndex: number,
    channelsNumber: number,
    setProject: SetState<Project>,
    isDefaultChannel: boolean,
    setDefault: (id: string) => void,
}

function swapChannels(
    setChannels: SetState<Channel[]>,
    a: number,
    b: number,
) {
    if (a === b) {
        return;
    }
    setChannels((channels) => channels.map((c, idx) => {
        const channel = (idx === a ? channels[b] : idx === b ? channels[a] : c)!;
        return {
            ...channel,
        };
    }));
}

function useSwapChannels(
    setChannels: SetState<Channel[]>,
) {
    return useCallback((a: number, b: number) => {
        swapChannels(setChannels, a, b);
    }, [setChannels]);
}

function isChannelAllowedToSwapLeft(
    idx: number,
) {
    return idx > 1;
}

function isChannelAllowedToSwapRight(
    idx: number,
    length: number,
) {
    return idx !== 0 && idx < length - 1;
}

function useMute(
    setChannel: SetState<Channel>,
) {
    return useCallback(() => {
        setChannel((prev) => ({
            ...prev,
            muted: !prev.muted,
        }));
    }, [setChannel]);
}

function useSolo(
    setProject: SetState<Project>,
    channelId: string,
) {
    return useCallback(() => {
        setProject((proj) => {
            const isMuted = proj.channels.find((c) => c.id === channelId)?.muted;
            if (isMuted) {
                return {
                    ...proj,
                    channels: proj.channels.map((c) => c.id === channelId ? { ...c, muted: false } : c),
                };
            } else {
                const shouldNotBeMuted = new Set<string>();
                function visit(id: string) {
                    if (shouldNotBeMuted.has(id)) {
                        return;
                    }
                    shouldNotBeMuted.add(id);
                    proj.channelsRouting.forEach((p) => {
                        if (p.from === id) {
                            visit(p.to);
                        }
                    });
                }
                visit(channelId);
                const enableSolo = proj.channels.some((c) => !c.muted && !shouldNotBeMuted.has(c.id));
                if (enableSolo) {
                    return {
                        ...proj,
                        channels: proj.channels.map((c) => ({
                            ...c,
                            muted: !shouldNotBeMuted.has(c.id),
                        })),
                    };
                } else {
                    return {
                        ...proj,
                        channels: proj.channels.map((c) => ({
                            ...c,
                            muted: false,
                        })),
                    };
                }
            }
        });
    }, [channelId, setProject]);
}

function ChannelOpsRaw({
    mode,
    channel,
    channelIndex,
    channelsNumber,
    setProject,
    isDefaultChannel,
    setDefault,
}: ChannelOpsProps) {

    const setChannels = setterByKey(setProject, "channels");
    const swapChannels = useSwapChannels(setChannels);
    const setChannel = useSetForId(setChannels, channel.id);
    const mute = useMute(setChannel);
    const solo = useSolo(setProject, channel.id);

    const onMoveLeft = useCallback(
        () => swapChannels(channelIndex, channelIndex - 1),
        [channelIndex, swapChannels],
    );
    const onMoveRight = useCallback(
        () => swapChannels(channelIndex, channelIndex + 1),
        [channelIndex, swapChannels],
    );

    return <div className={styles.channelOps}>
        <Button
            title={"Route keyboard keys here"}
            style={isDefaultChannel ? "primary" : "ghost"}
            onClick={() => setDefault(channel.id)}
            iconLeft={Svg18Keys}
        />
        <Button
            title={"Mute/solo channel"}
            style={"ghost"}
            onClick={mute}
            onRightClick={solo}
            iconLeft={channel.muted
                ? Svg18ChannelsUnmute
                : Svg18ChannelsMute}
        />
        <Button
            title={"Swap channel with left one"}
            style={"ghost"}
            disabled={!isChannelAllowedToSwapLeft(channelIndex)}
            onClick={onMoveLeft}
            iconLeft={mode === "vertical"
                ? Svg18ArrowUp
                : Svg18ArrowLeft}
        />
        <Button
            title={"Swap channel with right one"}
            style={"ghost"}
            disabled={!isChannelAllowedToSwapRight(channelIndex, channelsNumber)}
            onClick={onMoveRight}
            iconLeft={mode === "vertical"
                ? Svg18ArrowDown
                : Svg18ArrowRight}
        />
        <Button
            title={"Delete channel"}
            style={"ghost"}
            disabled={!isChannelAllowedToBeDeleted(channelIndex)}
            onClick={() => removeChannel(setProject, channelIndex)}
            iconLeft={Svg18CommonDelete}
        />
    </div>;
}

export const ChannelOps = React.memo(ChannelOpsRaw);
