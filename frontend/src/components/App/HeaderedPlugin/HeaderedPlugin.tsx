
import { openFile, saveFile } from "@audio-playground/lib-frontend/std/OpenFile";
import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback } from "react";

import { Button } from "../../Common/Button/Button";
import { Menu } from "../../Common/Menu/Menu";
import styles from "./HeaderedPlugin.module.css";

type HeaderedPluginProps<SETTINGS extends object> = {
    title: string,
    children: React.ReactNode,
    customUI: React.ReactNode,
    pluginSettings: SETTINGS,
    setPluginSettings: SetState<SETTINGS>,
    buildInPresets?: {
        name: string,
        get: () => Promise<SETTINGS>,
    }[],
}

export function HeaderedPlugin<SETTINGS extends object>(
    props: HeaderedPluginProps<SETTINGS>,
) {
    const {
        pluginSettings,
        setPluginSettings,
    } = props;
    const savePreset = useCallback(() => {
        saveFile(
            `${props.title}-preset-${Date.now()}.owdpreset`,
            [
                {
                    description: "OpenWebDaw preset file",
                    accept: { "application/json": [".owdpreset"] },
                },
            ],
            async(fileHandle) => {
                const buffer = new TextEncoder().encode(JSON.stringify(pluginSettings));
                const stream = await fileHandle.createWritable();
                await stream.write(buffer);
                await stream.close();
            },
        );
    }, [pluginSettings, props.title]);
    const openPreset = useCallback(() => {
        openFile(
            "OpenWebDaw preset file",
            ".owdpreset",
        ).then((d) => {
            const j = JSON.parse(new TextDecoder().decode(d));
            setPluginSettings(j);
        });
    }, [setPluginSettings]);
    return <div className={styles.headeredPlugin}>
        <div className={styles.header}>
            <div className={styles.title}>
                {props.title}
            </div>
            <div className={styles.customUI}>
                {props.customUI}
            </div>
            <div className={styles.presets}>
                <Button
                    onClick={savePreset}
                    title={"Save preset file"}
                />
                <Button
                    onClick={openPreset}
                    title={"Open preset file"}
                />
                {
                    props.buildInPresets && <Menu
                        title={"Built-in presets"}
                        items={props.buildInPresets.map((i) => ({
                            title: i.name,
                            onClick: () => {
                                i.get().then((p) => {
                                    setPluginSettings(p);
                                });
                            },
                        }))}
                        stretch={false}
                    />
                }
            </div>
        </div>
        <div className={styles.pluginUI}>
            {props.children}
        </div>
    </div>;
}
