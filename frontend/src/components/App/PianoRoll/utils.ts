import { NOTES_COUNT } from "@audio-playground/lib-sound/NoteFrequency";

export function trackIdxToNote(idx: number) {
    return NOTES_COUNT - idx - 1;
}
