import { generateId } from "@audio-playground/lib-common/std/Id";
import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import { NOTES_COUNT, noteToTitle, noteToType } from "@audio-playground/lib-sound/NoteFrequency";
import React, { useCallback, useMemo, useRef } from "react";

import { PianoRollNote } from "../TimelineWithClips/PianoRollNote";
import {
    AddClipEvent,
    CommonTimelineProps,
    defaultMoveHandler,
    DeleteClipEvent,
    MoveClipEvent,
    PasteClipEvent,
    ResizeClipEvent,
    TimelineWithClips,
    TouchClipEvent,
} from "../TimelineWithClips/TimelineWithClips";
import styles from "./PianoRoll.module.css";
import { trackIdxToNote } from "./utils";

type PianoRollRow = {
    id: number,
    noteColor: "BLACK" | "WHITE",
    noteTitle: string,
    note: number,
}

type PianoRollProps = CommonTimelineProps & {
    notes: PianoRollNote[],
    setNotes: SetState<PianoRollNote[]>,
    onKeyPress: (note: number) => void,
    onKeyRelease: (note: number) => void,
}

const GET_TRACK_COLOR = (track: PianoRollRow) => clsx(
    track.noteColor === "WHITE"
        ? "var(--piano-roll-note-white-background)"
        : "var(--piano-roll-note-black-background)",
);

const renderClip = (_: PianoRollNote, isSelected: boolean) => <div
    className={clsx(
        styles.noteClip,
        isSelected && styles.selected,
        !isSelected && styles.unselected,
    )}
/>;

export function PianoRoll({
    loopBorders,
    setLoopBorders,
    notes,
    setNotes,
    onKeyPress,
    onKeyRelease,
    bpm,
    currentTimeBars,
}: PianoRollProps) {
    const tracks: PianoRollRow[] = useMemo(() => {
        return [...Array(NOTES_COUNT)].map((_, idx) => {
            const note = trackIdxToNote(idx);
            return {
                id: idx,
                noteTitle: noteToTitle(note),
                noteColor: noteToType(note),
                note,
            };
        });
    }, []);
    const lastTouchedNoteLength = useRef(1);
    const renderTrackTitle = useMemo(() => {
        return (track: PianoRollRow) => {
            return <div
                onMouseDown={(e) => {
                    e.preventDefault();
                    onKeyPress(track.note);
                    const handleRelease = (e: MouseEvent) => {
                        e.preventDefault();
                        onKeyRelease(track.note);
                        window.removeEventListener("mouseup", handleRelease);
                    };
                    window.addEventListener("mouseup", handleRelease);
                }}
                className={clsx(
                    styles.noteTrackTitle,
                )}
            >
                {track.noteTitle}
            </div>;
        };
    }, [onKeyPress, onKeyRelease]);
    const handlePaste = useCallback((event: PasteClipEvent<number, PianoRollNote>) => {
        const newNotes = event.clips.map((c) => ({
            ...c,
            id: generateId(),
        }));
        setNotes((prev) => [...prev, ...newNotes]);
        return newNotes;
    }, [setNotes]);

    const handleResize = useCallback((event: ResizeClipEvent<number, PianoRollNote>) => {
        const resizedNotes = new Map(event.clips.map((c) => [c.id, c]));
        setNotes((note) => note.map((note) => {
            const resizedNote = resizedNotes.get(note.id);
            if (!resizedNote) {
                return note;
            }
            if (event.side === "end") {
                return {
                    ...note,
                    timelineTimeLengthBars: resizedNote.timelineTimeLengthBars + event.deltaTime,
                };
            } else {
                return {
                    ...note,
                    timelineTimeStartBars: resizedNote.timelineTimeStartBars + event.deltaTime,
                    timelineTimeLengthBars: resizedNote.timelineTimeLengthBars - event.deltaTime,
                };
            }
        }));
    }, [setNotes]);

    const handleMove = useCallback(
        (event: MoveClipEvent<number, PianoRollNote>) => defaultMoveHandler(
            setNotes,
            event,
            tracks,
        ),
        [setNotes, tracks],
    );

    const handleDoubleClick = useCallback(
        () => {},
        [],
    );

    const onAddClip = useCallback(
        (event: AddClipEvent<number, PianoRollRow, PianoRollNote>) => {
            const id = generateId();
            const lengthBars = lastTouchedNoteLength.current;
            const newNote = {
                id,
                timelineTimeStartBars: event.timeBars,
                timelineTimeLengthBars: lengthBars,
                rowId: event.row.id,
            };
            setNotes((prev) => [...prev, newNote]);
            return newNote;
        },
        [setNotes],
    );

    const onTouchClip = useCallback(
        (event: TouchClipEvent) => {
            const note = notes.find((n) => n.id === event.clipId);
            if (note) {
                lastTouchedNoteLength.current = note.timelineTimeLengthBars;
            }
        },
        [notes],
    );

    const onDeleteClip = useCallback(
        (event: DeleteClipEvent) =>
            setNotes(
                (notes) => notes.filter((n) => !event.clipIds.includes(n.id)),
            ),
        [setNotes],
    );

    return <TimelineWithClips
        rows={tracks}
        bpm={bpm}
        currentTimeBars={currentTimeBars}
        rowTitlesWidth={50}
        rowHeight={32}
        loopBorders={loopBorders}
        setLoopBorders={setLoopBorders}
        clips={notes}
        getRowColor={GET_TRACK_COLOR}
        renderRowTitle={renderTrackTitle}
        renderClip={renderClip}
        onPasteClip={handlePaste}
        onDeleteClips={onDeleteClip }
        onMoveClips={handleMove}
        onResizeClips={handleResize}
        onAddClip={onAddClip}
        onTouchClip={onTouchClip}
        onDoubleClickClip={handleDoubleClick}
        autoAlignVerticalScroll={true}
    />;
}
