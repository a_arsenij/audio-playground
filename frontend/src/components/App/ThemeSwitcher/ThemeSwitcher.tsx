import { useLocalStorageState } from "@audio-playground/lib-frontend/hooks/useLocalStorageState";
import React, { useEffect, useMemo, useState } from "react";

import { Svg18MainThemeAuto } from "../../Common/GeneratedIcons/18/Main/Theme/Auto";
import { Svg18MainThemeDark } from "../../Common/GeneratedIcons/18/Main/Theme/Dark";
import { Svg18MainThemeLight } from "../../Common/GeneratedIcons/18/Main/Theme/Light";
import { Switch } from "../../Common/Switch/Switch";

type Theme = "day" | "night";
type ThemeOption = "day" | "auto" | "night";

export function ThemeSwitcher() {

    const [themeOption, setThemeOption] = useLocalStorageState<ThemeOption>("theme", "auto");

    const isDark = useMemo(
        () => window.matchMedia("(prefers-color-scheme: dark)"),
        [],
    );
    const [theme, setTheme] = useState<Theme>(themeOption !== "auto" ? themeOption : isDark.matches ? "night" : "day");

    useEffect(() => {
        if (themeOption !== "auto") {
            setTheme(themeOption);
            return;
        }
        const handle = (e: MediaQueryListEvent) => {
            setTheme(e.matches ? "night" : "day");
        };
        isDark.addEventListener("change", handle);
        return () => isDark.removeEventListener("change", handle);
    }, [themeOption, isDark]);

    useEffect(() => {
        document.body.setAttribute("data-theme", theme);
    }, [theme]);

    return <Switch
        options={[
            { value: "night", title: "Dark", icon: Svg18MainThemeDark },
            { value: "auto", title: "Auto", icon: Svg18MainThemeAuto },
            { value: "day", title: "Light", icon: Svg18MainThemeLight },
        ]}
        value={themeOption}
        setValue={setThemeOption}
    />;
}
