import { SetState, useByKey } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import { PiecewiseFunctionData } from "../../../utils/SvgGraphFunctions";
import { PiecewiseGraphInteractiveWithTimeline } from "../../Common/PiecewiseGraphInteractive/PiecewiseGraphInteractiveWithTimeline";
import styles from "./AutomationEditor.module.css";

type AutomationEditorProps = {
    piecewiseFunction: PiecewiseFunctionData,
    setPiecewiseFunction: SetState<PiecewiseFunctionData>,
}

export function AutomationEditor(props: AutomationEditorProps) {

    const [points, setPoints] = useByKey(
        props.piecewiseFunction,
        props.setPiecewiseFunction,
        "points",
    );

    const [pieceData, setPieceData] = useByKey(
        props.piecewiseFunction,
        props.setPiecewiseFunction,
        "pieceData",
    );

    return <div className={styles.automationEditor}>
        <PiecewiseGraphInteractiveWithTimeline
            points={points}
            setPoints={setPoints}
            pieceData={pieceData}
            setPieceData={setPieceData}
        />
    </div>;
}
