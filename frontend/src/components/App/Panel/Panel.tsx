import { someInputIsFocused } from "@audio-playground/lib-frontend/std/SomeInputIsFocused";
import React, { useEffect, useRef, useState } from "react";

import { GridMode } from "../../../types/GridMode";
import { MixMode } from "../../../types/MixSideMode";
import { UiMode } from "../../../types/Mode";
import { ResizeMode } from "../../../types/ResizeMode";
import { ZoomMode } from "../../../types/ZoomMode";
import { useDebug } from "../../../utils/useDebug";
import { Button } from "../../Common/Button/Button";
import { ContextMenu } from "../../Common/ContextMenu/ContextMenu";
import { DropdownItem } from "../../Common/DropdownItem/DropdownItem";
import { Svg18ArrowLeftDouble } from "../../Common/GeneratedIcons/18/Arrow/LeftDouble";
import { Svg18ArrowRightDouble } from "../../Common/GeneratedIcons/18/Arrow/RightDouble";
import { Svg18ClipsModeCrop } from "../../Common/GeneratedIcons/18/ClipsMode/Crop";
import { Svg18ClipsModeStretch } from "../../Common/GeneratedIcons/18/ClipsMode/Stretch";
import { Svg18CommonReset } from "../../Common/GeneratedIcons/18/Common/Reset";
import { Svg18MainDevOff } from "../../Common/GeneratedIcons/18/Main/Dev/Off";
import { Svg18MainDevOn } from "../../Common/GeneratedIcons/18/Main/Dev/On";
import { Svg18MainFileExportWav } from "../../Common/GeneratedIcons/18/Main/File/ExportWav";
import { Svg18MainFileOpenProject } from "../../Common/GeneratedIcons/18/Main/File/OpenProject";
import { Svg18MainFileSaveProject } from "../../Common/GeneratedIcons/18/Main/File/SaveProject";
import { Svg18MainLeftPanelClips } from "../../Common/GeneratedIcons/18/Main/LeftPanel/Clips";
import { Svg18MainLeftPanelLibrary } from "../../Common/GeneratedIcons/18/Main/LeftPanel/Library";
import { Svg18MainLeftPanelOff } from "../../Common/GeneratedIcons/18/Main/LeftPanel/Off";
import { Svg18MainWindowChannels } from "../../Common/GeneratedIcons/18/Main/Window/Channels";
import { Svg18MainWindowPianoRoll } from "../../Common/GeneratedIcons/18/Main/Window/PianoRoll";
import { Svg18MainWindowPlaylist } from "../../Common/GeneratedIcons/18/Main/Window/Playlist";
import { Svg18MainWindowPlugin } from "../../Common/GeneratedIcons/18/Main/WindowPlugin";
import { Svg18PlayStatePlay } from "../../Common/GeneratedIcons/18/PlayState/Play";
import { Svg18PlayStateStop } from "../../Common/GeneratedIcons/18/PlayState/Stop";
import { Svg18Routing } from "../../Common/GeneratedIcons/18/Routing";
import { Svg18ToolDrag } from "../../Common/GeneratedIcons/18/Tool/Drag";
import { Svg18ToolEdit } from "../../Common/GeneratedIcons/18/Tool/Edit";
import { Svg18ToolEraser } from "../../Common/GeneratedIcons/18/Tool/Eraser";
import { Svg18ZoomMinus } from "../../Common/GeneratedIcons/18/Zoom/Minus";
import { Svg18ZoomPlus } from "../../Common/GeneratedIcons/18/Zoom/Plus";
import { Svg18ZoomReset } from "../../Common/GeneratedIcons/18/Zoom/Reset";
import { NumberInput } from "../../Common/Input/Input";
import { Switch } from "../../Common/Switch/Switch";
import { ThemeSwitcher } from "../ThemeSwitcher/ThemeSwitcher";
import styles from "./Panel.module.css";

type PanelProps = {
  onSave: () => void,
  onOpen: () => void,
  onRender: () => void,
  onPlay: () => void,
  onStop: () => void,
  isPlaying: boolean,
  bpm: number,
  setBpm: (v: number) => void,
  mode: UiMode,
  setMode: (m: UiMode) => void,
  resizeMode: ResizeMode,
  setResizeMode: (m: ResizeMode) => void,
  gridMode: GridMode,
  setGridMode: (m: GridMode) => void,
  zoomMode: ZoomMode,
  setZoomMode: (m: ZoomMode) => void,
  mixSideMode: MixMode,
  setMixSideMode: (m: MixMode) => void,
}

function PanelRaw({
    onSave,
    onOpen,
    onRender,
    onPlay,
    onStop,
    isPlaying,
    bpm,
    setBpm,
    gridMode,
    setGridMode,
    mode,
    setMode,
    resizeMode,
    setResizeMode,
    zoomMode,
    setZoomMode,
    mixSideMode,
    setMixSideMode,
}: PanelProps) {
    // TODO implement
    const [leftPanelMode, setLeftPanelMode] = useState<LeftPanelMode>("off");

    useEffect(() => {
        const handle = (e: KeyboardEvent) => {
            if (someInputIsFocused()) {
                return;
            }
            if (e.key === " ") {
                e.preventDefault();
                e.stopPropagation();
                if (isPlaying) {
                    onStop();
                } else {
                    onPlay();
                }
            }
        };
        window.addEventListener("keydown", handle, {
            capture: true,
        });
        return () => {
            window.removeEventListener("keydown", handle, {
                capture: true,
            });
        };
    }, [isPlaying, onPlay, onStop]);

    const [debug, setDebug] = useDebug();

    const bpmRef = useRef<HTMLInputElement>(null);


    return <header className={styles.panel}>
        <div className={styles.mainContent}>
            <div className={styles.buttonGroup}>
                <Switch
                    options={[
                        { value: "play", title: "Play", icon: Svg18PlayStatePlay },
                        { value: "stop", title: "Stop", icon: Svg18PlayStateStop },
                    ]}
                    value={isPlaying ? "play" : "stop"}
                    setValue={(v) => v === "play"
                        ? onPlay()
                        : onStop()
                    }
                />
            </div>

            <div className={styles.bpm}>
                <div className={styles.bpmTitle}>
                BPM:
                </div>
                <NumberInput
                    inputRef={bpmRef}
                    value={bpm}
                    onChange={setBpm}
                    className={styles.bpmValue}
                    size={"small"}
                />
            </div>
            <ContextMenu
                rightMouseClickTargetRef={bpmRef}
                ChildComponent={BpmContextMenu}
                props={{ setBpm }}
            />
        </div>

        <div className={styles.secondaryContent}>
            <div className={styles.buttonGroup}>
                { mode === "playlist" && <>
                    <Switch
                        options={[
                            { value: "crop", title: "Crop", icon: Svg18ClipsModeCrop },
                            { value: "stretch", title: "Stretch", icon: Svg18ClipsModeStretch },
                        ]}
                        value={resizeMode}
                        setValue={setResizeMode}
                    />
                </> }
            </div>

            <div className={styles.buttonGroup}>
                { mode === "playlist" && <>
                    <Switch
                        options={[
                            { value: "drag", title: "Drag", icon: Svg18ToolDrag },
                            { value: "edit", title: "Edit", icon: Svg18ToolEdit },
                            { value: "erase", title: "Erase", icon: Svg18ToolEraser },
                        ]}
                        value={gridMode}
                        setValue={setGridMode}
                    />
                </> }
            </div>


            <div className={styles.buttonGroup}>
                { mode === "playlist" && <>
                    <Switch
                        options={[
                            { value: "off", title: "off", icon: Svg18MainLeftPanelOff },
                            { value: "clips", title: "clips", icon: Svg18MainLeftPanelClips },
                            { value: "library", title: "library", icon: Svg18MainLeftPanelLibrary },
                        ]}
                        value={leftPanelMode}
                        setValue={setLeftPanelMode}
                    />
                </> }
            </div>

            <div className={styles.buttonGroup}>
                { mode === "playlist" && <>
                    <Switch
                        options={[
                            { value: "zoomIn", title: "Zoom In", icon: Svg18ZoomPlus },
                            { value: "zoomOut", title: "Zoom Out", icon: Svg18ZoomMinus },
                            { value: "zoomReset", title: "Reset Zoom", icon: Svg18ZoomReset },
                        ]}
                        value={zoomMode}
                        setValue={setZoomMode}
                    />
                </> }
            </div>

            <div className={styles.buttonGroup}>
                { mode === "mixer" && <>
                    <Switch
                        options={[
                            { value: "left", title: "Left", icon: Svg18ArrowLeftDouble },
                            { value: "right", title: "Right", icon: Svg18ArrowRightDouble },
                        ]}
                        value={mixSideMode}
                        setValue={setMixSideMode}
                    />
                </> }
            </div>

            { mode === "mixer" && <>
                <Button
                    style="primary"
                    onClick={() => {
                        // TODO routing
                    }}
                    iconLeft={Svg18Routing}
                    title={"Show channels routing"}
                />
            </>}

            <div className={styles.buttonGroup}>
                <Switch
                    options={[
                        { value: "piano-roll", title: "piano-roll", icon: Svg18MainWindowPianoRoll },
                        { value: "playlist", title: "playlist", icon: Svg18MainWindowPlaylist },
                        { value: "mixer", title: "mixer", icon: Svg18MainWindowChannels },
                        { value: "plugin", title: "plugin", icon: Svg18MainWindowPlugin },
                    ]}
                    value={mode}
                    setValue={setMode}
                />
            </div>

            <Button
                style="secondary"
                onClick={() => {
                    localStorage.removeItem("project");
                    window.location.reload();
                }}
                iconLeft={Svg18CommonReset}
                title={"Reset project"}
            />

            <div className={styles.buttonGroup}>
                <ThemeSwitcher/>
            </div>

            <div className={styles.buttonGroup}>
                <Switch
                    options={[
                        {
                            value: "off",
                            title: "Off",
                            icon: Svg18MainDevOff,
                        },
                        {
                            value: "on",
                            title: "On",
                            icon: Svg18MainDevOn,
                        },
                    ]}
                    value={debug}
                    setValue={setDebug}
                />
            </div>


            <div className={styles.buttonGroup}>
                <Button
                    onClick={onSave}
                    style={"secondary"}
                    title={"Save project"}
                    iconLeft={Svg18MainFileSaveProject}
                />
                <Button
                    onClick={onOpen}
                    style={"secondary"}
                    title={"Open project"}
                    iconLeft={Svg18MainFileOpenProject}
                />
                <Button
                    onClick={onRender}
                    style={"secondary"}
                    title={"Export to wav"}
                    iconLeft={Svg18MainFileExportWav}
                />
            </div>


        </div>
    </header>;
};

export type LeftPanelMode =
    | "off"
    | "clips"
    | "library"
;

export const Panel = React.memo(PanelRaw);

type BpmContextMenuProps = {
  closeContextMenu: () => void,
  setBpm: (v: number) => void,
}

function BpmContextMenu({ closeContextMenu, setBpm }: BpmContextMenuProps) {
    return <>
        {[...Array(13)].map((_, idx) => {
            const bpm = idx * 10 + 80;
            return <DropdownItem
                onClick={() => {
                    setBpm(bpm);
                    closeContextMenu();
                }}
                title={`${bpm} bpm`}
                key={idx}
            />;
        })}

    </>;
}
