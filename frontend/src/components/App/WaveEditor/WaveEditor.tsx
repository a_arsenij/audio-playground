import { ArrayBufferAllocator } from "@audio-playground/lib-common/std/ArrayBufferAllocator";
import { BlobRef } from "@audio-playground/lib-common/std/BlobRef";
import { useReffedState } from "@audio-playground/lib-frontend/hooks/useReffedState";
import React, { CSSProperties } from "react";

import { LoopBorders } from "../../../types/Loop";
import { Timeline } from "../../Common/Timeline/Timeline";
import { Wave } from "../../Common/Wave/Wave";
import styles from "./WaveEditor.module.css";

type WaveEditorProps = {
    bpm: number,
    allocator: ArrayBufferAllocator,
    wave: [BlobRef] | [BlobRef, BlobRef],
    sampleRate: number,
    loopBorders: LoopBorders | undefined,
    setLoopBorders: (newLoopBorders: LoopBorders) => void,
}

export function WaveEditor(props: WaveEditorProps) {

    const [pxPerBar, , setPxPerBar] = useReffedState(() => 1024);

    const lengthSeconds = props.wave[0].size / props.sampleRate;
    const lengthMinutes = lengthSeconds / 60;
    const lengthBars = lengthMinutes * props.bpm;
    const lengthPx = pxPerBar * lengthBars;

    return <Timeline

        loopBorders={props.loopBorders}
        setLoopBorders={props.setLoopBorders}
        rowsTitles={<div/>}
        rowsElems={<div
            style={{
                "--width": `${lengthPx}`,
            } as CSSProperties}
            className={styles.waveEditor}
        >
            <Wave
                allocator={props.allocator}
                blobRef={props.wave[0]}
                startSample={0}
                lengthSample={props.wave[0].size}
            />
            {props.wave.length === 2 && <Wave
                allocator={props.allocator}
                blobRef={props.wave[1]}
                startSample={0}
                lengthSample={props.wave[1].size}
            />}
        </div>}
        contentWidthBars={2}
        rowTitlesWidth={0}
        rowHeight={256}
        pxPerBar={pxPerBar}
        setPxPerBar={setPxPerBar}
    />

    ;
}
