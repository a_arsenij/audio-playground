import { ArrayBufferAllocator } from "@audio-playground/lib-common/std/ArrayBufferAllocator";
import { generateId } from "@audio-playground/lib-common/std/Id";
import { buildSimpleCache } from "@audio-playground/lib-common/std/SimpleCache";
import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import { amplitudeToDb } from "@audio-playground/lib-sound/AmplitudeToDecibells";
import React, { memo, useCallback, useMemo } from "react";

import { Automation, Channel } from "../../../types/project/Channel";
import { Clip, WaveClip } from "../../../types/project/Clip";
import { ClipUsage } from "../../../types/project/ClipUsage";
import { Project } from "../../../types/project/Project";
import { ResizeMode } from "../../../types/ResizeMode";
import { useAddNewChannel } from "../../../utils/projectChanges/addChannel";
import { Button } from "../../Common/Button/Button";
import { Svg18SignPlus } from "../../Common/GeneratedIcons/18/Sign/Plus";
import { MultiMeter } from "../../Common/MultiMeter/MultiMeter";
import { PiecewiseGraph } from "../../Common/PiecewiseGraph/PiecewiseGraph";
import { Wave } from "../../Common/Wave/Wave";
import { ChannelOps } from "../ChannelOps/ChannelOps";
import { trackIdxToNote } from "../PianoRoll/utils";
import { PianoRollNote } from "../TimelineWithClips/PianoRollNote";
import {
    AddClipEvent,
    CommonTimelineProps,
    DeleteClipEvent, DoubleClickClipEvent,
    MoveClipEvent, PasteClipEvent, ResizeClipEvent,
    TimelineClip, TimelineRow, TimelineWithClips,
    TouchClipEvent,
} from "../TimelineWithClips/TimelineWithClips";
import styles from "./Playlist.module.css";

const PLAYLIST_ROW_TITLES_WIDTH = 160;
const PLAYLIST_ROW_HEIGHT = 95;

type PlaylistProps = CommonTimelineProps & {
    channels: Channel[],
    setProject: SetState<Project>,
    channelsLevels: number[][],
    clips: Clip[],
    clipsUsages: ClipUsage[],
    setClipsUsages: SetState<ClipUsage[]>,
    selectedClip: Clip | undefined,
    setSelectedClipId: (id: string) => void,
    onDoubleClickClip: (id: string) => void,
    defaultChannelId: string,
    setDefaultChannelId: (id: string) => void,
    resizeMode: ResizeMode,
    allocator: ArrayBufferAllocator,
}

type PlaylistRow = TimelineRow<string> & (
    | {
        type: "notes",
        channel: Channel,
        channelIdx: number,
    }
    | {
        type: "automation",
        channel: Channel,
        automation: Automation,
    }
    | {
        type: "add",
    }
)

type PlaylistClipUsage = TimelineClip<string> & {
    clipId: string,

    sourceTimeStart: number,
    sourceTimeLength: number,
}

const notesToSvg = buildSimpleCache<PianoRollNote[], React.ReactNode>(
    (notes) => {
        if (notes.length === 0) {
            return null;
        }
        const minNote = notes
            .map((a) => trackIdxToNote(a.rowId))
            .reduce((a, b) => Math.min(a, b));
        const maxNote = notes
            .map((a) => trackIdxToNote(a.rowId))
            .reduce((a, b) => Math.max(a, b));
        return <div
            className={styles.notesPreview}
            style={variables({
                "--minNote": minNote,
                "--maxNote": maxNote,
            })}
        >
            {
                notes.map((note) => <div
                    key={note.id}
                    className={styles.notePreview}
                    style={variables({
                        "--note": trackIdxToNote(note.rowId),
                        "--noteTimeStart": note.timelineTimeStartBars,
                        "--noteTimeLength": note.timelineTimeLengthBars,
                    })}
                >
                    <div className={styles.notePreviewFill}/>
                </div>)
            }
        </div>;
    },
);

function resizeNotesClipStart(
    resizeMode: ResizeMode,
    clipUsage: ClipUsage,
    initialClipUsage: ClipUsage,
    initialSpeed: number,
    event: ResizeClipEvent<string, ClipUsage>,
): ClipUsage {
    const diff = event.deltaTime / initialSpeed;
    return {
        ...clipUsage,
        ...(resizeMode === "stretch"
            ? {}
            : {
                sourceTimeStart:
                    initialClipUsage.sourceTimeStart + diff,
                sourceTimeLength:
                    initialClipUsage.sourceTimeLength - diff,
            }),
        timelineTimeStartBars:
            initialClipUsage.timelineTimeStartBars + event.deltaTime,
        timelineTimeLengthBars:
            initialClipUsage.timelineTimeLengthBars - event.deltaTime,
    };
}

function resizeNotesClipEnd(
    resizeMode: ResizeMode,
    clipUsage: ClipUsage,
    initialClipUsage: ClipUsage,
    initialSpeed: number,
    event: ResizeClipEvent<string, ClipUsage>,
): ClipUsage {
    const diff = event.deltaTime / initialSpeed;
    return {
        ...clipUsage,
        ...(resizeMode === "stretch"
            ? {}
            : {
                sourceTimeLength: initialClipUsage.sourceTimeLength + diff,
            }),
        timelineTimeLengthBars:
            initialClipUsage.timelineTimeLengthBars + event.deltaTime,
    };
}

function resizeNotesClip(
    resizeMode: ResizeMode,
    clipUsage: ClipUsage,
    initialClipUsage: ClipUsage,
    event: ResizeClipEvent<string, ClipUsage>,
): ClipUsage {
    const initialSpeed =
        initialClipUsage.timelineTimeLengthBars / initialClipUsage.sourceTimeLength;
    if (event.side === "end") {
        return resizeNotesClipEnd(
            resizeMode,
            clipUsage,
            initialClipUsage,
            initialSpeed,
            event,
        );
    } else {
        return resizeNotesClipStart(
            resizeMode,
            clipUsage,
            initialClipUsage,
            initialSpeed,
            event,
        );
    }
}

function resizeWaveClipStart(
    resizeMode: ResizeMode,
    clip: WaveClip,
    clipUsage: ClipUsage,
    initialClipUsage: ClipUsage,
    initialSpeed: number,
    event: ResizeClipEvent<string, ClipUsage>,
    bpm: number,
): ClipUsage {
    const diff = event.deltaTime / bpm * 60 * clip.sampleRate / initialSpeed;
    return {
        ...clipUsage,
        ...(resizeMode === "stretch"
            ? {}
            : {
                sourceTimeStart: initialClipUsage.sourceTimeStart + diff,
                sourceTimeLength: initialClipUsage.sourceTimeLength - diff,
            }),
        timelineTimeStartBars: initialClipUsage.timelineTimeStartBars + event.deltaTime,
        timelineTimeLengthBars: initialClipUsage.timelineTimeLengthBars - event.deltaTime,
    };
}

function resizeWaveClipEnd(
    resizeMode: ResizeMode,
    clip: WaveClip,
    clipUsage: ClipUsage,
    initialClipUsage: ClipUsage,
    initialSpeed: number,
    event: ResizeClipEvent<string, ClipUsage>,
    bpm: number,
): ClipUsage {
    const diff = event.deltaTime / bpm * 60 * clip.sampleRate / initialSpeed;
    return {
        ...clipUsage,
        ...(resizeMode === "stretch"
            ? {}
            : {
                sourceTimeLength: initialClipUsage.sourceTimeLength + diff,
            }),
        timelineTimeLengthBars: initialClipUsage.timelineTimeLengthBars + event.deltaTime,
    };
}

function resizeWaveClip(
    resizeMode: ResizeMode,
    clip: WaveClip,
    clipUsage: ClipUsage,
    initialClipUsage: ClipUsage,
    event: ResizeClipEvent<string, ClipUsage>,
    bpm: number,
): ClipUsage {
    const initialSpeed =
        initialClipUsage.timelineTimeLengthBars /
        (initialClipUsage.sourceTimeLength / clip.sampleRate / 60 * bpm);
    if (event.side === "end") {
        return resizeWaveClipEnd(
            resizeMode,
            clip,
            clipUsage,
            initialClipUsage,
            initialSpeed,
            event,
            bpm,
        );
    } else {
        return resizeWaveClipStart(
            resizeMode,
            clip,
            clipUsage,
            initialClipUsage,
            initialSpeed,
            event,
            bpm,
        );
    }
}

function resizeAutomationClipStart(
    resizeMode: ResizeMode,
    clipUsage: ClipUsage,
    initialClipUsage: ClipUsage,
    initialSpeed: number,
    event: ResizeClipEvent<string, ClipUsage>,
): ClipUsage {
    const diff = event.deltaTime / initialSpeed;
    return {
        ...clipUsage,
        ...(resizeMode === "stretch"
            ? {}
            : {
                sourceTimeStart: initialClipUsage.sourceTimeStart + diff,
                sourceTimeLength: initialClipUsage.sourceTimeLength - diff,
            }),
        timelineTimeStartBars: initialClipUsage.timelineTimeStartBars + event.deltaTime,
        timelineTimeLengthBars: initialClipUsage.timelineTimeLengthBars - event.deltaTime,
    };
}

function resizeAutomationClipEnd(
    resizeMode: ResizeMode,
    clipUsage: ClipUsage,
    initialClipUsage: ClipUsage,
    initialSpeed: number,
    event: ResizeClipEvent<string, ClipUsage>,
): ClipUsage {
    const diff = event.deltaTime / initialSpeed;
    return {
        ...clipUsage,
        ...(resizeMode === "stretch"
            ? {}
            : {
                sourceTimeLength: initialClipUsage.sourceTimeLength + diff,
            }),
        timelineTimeLengthBars: initialClipUsage.timelineTimeLengthBars + event.deltaTime,
    };
}

function resizeAutomationClip(
    resizeMode: ResizeMode,
    clipUsage: ClipUsage,
    initialClipUsage: ClipUsage,
    event: ResizeClipEvent<string, ClipUsage>,
): ClipUsage {
    const initialSpeed =
        initialClipUsage.timelineTimeLengthBars / initialClipUsage.sourceTimeLength;
    if (event.side === "end") {
        return resizeAutomationClipEnd(
            resizeMode,
            clipUsage,
            initialClipUsage,
            initialSpeed,
            event,
        );
    } else {
        return resizeAutomationClipStart(
            resizeMode,
            clipUsage,
            initialClipUsage,
            initialSpeed,
            event,
        );
    }
}

export function Playlist({
    allocator,
    channels,
    channelsLevels,
    clips,
    clipsUsages,
    selectedClip,
    setSelectedClipId,
    setClipsUsages,
    setProject,
    bpm,
    currentTimeBars,
    loopBorders,
    setLoopBorders,
    onDoubleClickClip,
    defaultChannelId,
    setDefaultChannelId,
    resizeMode,
}: PlaylistProps) {

    const addNewChannel = useAddNewChannel(setProject);

    const rows: PlaylistRow[] = useMemo(() => {
        return [
            ...channels.map((c, idx) => {
                return [
                    {
                        id: c.id,
                        type: "notes",
                        channel: c,
                        channelIdx: idx,
                    },
                    ...c.automations.map((a) => ({
                        id: a.id,
                        type: "automation",
                        automation: a,
                    })),
                ] as PlaylistRow[];
            }).flat(),
            { id: "add", type: "add" },
        ];
    }, [channels]);

    const idToClip = useMemo(() => {
        return new Map(clips.map((t) => [t.id, t]));
    }, [clips]);

    const idToClipUsage = useMemo(() => {
        return new Map(clipsUsages.map((t) => [t.id, t]));
    }, [clipsUsages]);

    const pluginIdToPlugin = useMemo(() => {
        return new Map(
            channels.map((c) => c.plugins).flat().map((p) => [p.id, p]),
        );
    }, [channels]);

    const renderTrackTitle = useCallback((track: PlaylistRow) => {
        return <div className={ styles.trackTitle }>
            {
                track.type === "automation" && <React.Fragment>
                    {
                        track.automation.type === "plugin"
                            ? <>
                                {pluginIdToPlugin.get(track.automation.pluginInstanceId)?.name}:
                                {track.automation.knobId}
                            </>
                            : track.automation.type
                    }
                </React.Fragment>
            }
            {
                track.type === "notes" && <React.Fragment>
                    {track.channel.title}
                    <ChannelOps
                        mode={"vertical"}
                        channel={track.channel}
                        setProject={setProject}
                        channelIndex={track.channelIdx}
                        channelsNumber={channels.length}
                        isDefaultChannel={defaultChannelId === track.channel.id}
                        setDefault={() => setDefaultChannelId(track.channel.id)}
                    />
                    <div className={styles.levels}>
                        <MultiMeter
                            value={(channelsLevels[track.channelIdx] ?? [0, 0]).map((v) => amplitudeToDb(v))}
                        />
                    </div>
                </React.Fragment>
            }
            {
                track.type === "add" && <React.Fragment>
                    <Button
                        onClick={addNewChannel}
                        iconLeft={Svg18SignPlus}
                        title={"Add new channel"}
                    />
                </React.Fragment>
            }
        </div>;
    }, [
        pluginIdToPlugin,
        setProject,
        channels.length,
        defaultChannelId,
        channelsLevels,
        addNewChannel,
        setDefaultChannelId,
    ]);

    const renderClip = useCallback((playlistClip: PlaylistClipUsage, isSelected: boolean) => {
        const clip = idToClip.get(playlistClip.clipId)!;
        return <PlaylistClip
            allocator={allocator}
            isSelected={isSelected}
            playlistClip={playlistClip}
            clip={clip}
        />;
    }, [allocator, idToClip]);

    const onResizeClips = useCallback((event: ResizeClipEvent<string, ClipUsage>) => {
        const resizedClipsUsages = new Map(event.clips.map((c) => [c.id, c]));
        setClipsUsages((note) => note.map((clipUsage) => {
            const initialClipUsage = resizedClipsUsages.get(clipUsage.id);
            if (!initialClipUsage) {
                return clipUsage;
            }
            const clip = idToClip.get(clipUsage.clipId);
            if (!clip) {
                return clipUsage;
            }

            switch (clip.type) {
                case "notes":
                    return resizeNotesClip(
                        resizeMode,
                        clipUsage,
                        initialClipUsage,
                        event,
                    );
                case "wave":
                    return resizeWaveClip(
                        resizeMode,
                        clip,
                        clipUsage,
                        initialClipUsage,
                        event,
                        bpm,
                    );
                case "automation":
                    return resizeAutomationClip(
                        resizeMode,
                        clipUsage,
                        initialClipUsage,
                        event,
                    );
            }

        }));
    }, [setClipsUsages, idToClip, resizeMode, bpm]);

    function isClipAndRowCompatible(
        clip: Clip,
        row: PlaylistRow,
    ) {
        return !(
            row.type === "automation" && clip.type !== "automation" ||
            row.type !== "automation" && clip.type === "automation"
        );
    }

    const onMoveClips = useCallback(
        (event: MoveClipEvent<string, ClipUsage>) => {

            const idToRowIndex = new Map(
                rows.map((t, idx) => [t.id, idx]),
            );
            const indexToRow = new Map(
                rows.map((t, idx) => [idx, t]),
            );

            const movedClips = new Map(event.clips.map((c) => [c.id, c]));
            setClipsUsages((prevClipsUsages) => prevClipsUsages.map((note) => {
                const movedClip = movedClips.get(note.id);
                if (!movedClip) {
                    return note;
                }
                const oldRowIndex = idToRowIndex.get(movedClip.rowId)!;
                const newRowIndex = oldRowIndex + event.deltaRow;
                const newRow = indexToRow.get(newRowIndex);

                if (newRow === undefined) {
                    return note;
                }

                const clip = idToClip.get(movedClip.clipId);
                if (!clip) {
                    return note;
                }

                if (!isClipAndRowCompatible(clip, newRow)) {
                    return note;
                }

                return {
                    ...note,
                    rowId: newRow.id,
                    timelineTimeStartBars: movedClip.timelineTimeStartBars + event.deltaTime,
                };
            }));
        },
        [setClipsUsages, rows, idToClip],
    );

    const onDeleteClips = useCallback((event: DeleteClipEvent) => {
        setClipsUsages((prev) => prev.filter(
            (u) => {
                const found = event.clipIds.find((ee) => ee === u.id);
                return !found;
            },
        ));
    }, [setClipsUsages]);

    const onPasteClip = useCallback((event: PasteClipEvent<string, ClipUsage>) => {
        const newNotes = event.clips.map((c) => ({
            ...c,
            id: generateId(),
        }));
        setClipsUsages((prev) => [...prev, ...newNotes]);
        return newNotes;
    }, [setClipsUsages]);

    const selectedClipLengthBars = useMemo(() => {
        if (selectedClip === undefined) {
            return 1;
        }
        if (selectedClip.type === "automation") {
            return 1;
        }
        if (selectedClip.type === "wave") {
            return selectedClip.wave[0].size / 8 / selectedClip.sampleRate / 60 * bpm;
        }
        if (selectedClip.notes.length === 0) {
            return 1;
        }
        const maxTime = selectedClip.notes
            .map((n) => n.timelineTimeStartBars + n.timelineTimeLengthBars)
            .reduce((a, b) => Math.max(a, b));
        return Math.ceil(maxTime / 4) * 4;
    }, [selectedClip, bpm]);

    const selectedClipSourceLength = useMemo(() => {
        if (selectedClip === undefined) {
            return 1;
        }
        if (selectedClip.type === "automation") {
            return 1;
        }
        if (selectedClip.type === "wave") {
            return selectedClip.wave[0].size / 8;
        }
        return selectedClipLengthBars;
    }, [selectedClip, selectedClipLengthBars]);

    const onAddClip = useCallback((event: AddClipEvent<string, PlaylistRow, PlaylistClipUsage>) => {
        if (selectedClip?.id === undefined) {
            return;
        }
        const selected = event.selectedUsage?.clipId === selectedClip.id
            ? event.selectedUsage
            : undefined;
        const row = event.row;
        if (!isClipAndRowCompatible(selectedClip, row)) {
            // TODO: show hint that clips should be placed onto rows with same type
            return;
        }
        const id = generateId();
        const newClip = {
            id,
            rowId: event.row.id,
            timelineTimeStartBars: event.timeBars,
            timelineTimeLengthBars: selected?.timelineTimeLengthBars ?? selectedClipLengthBars,
            sourceTimeStart: selected?.sourceTimeStart ?? 0,
            sourceTimeLength: selected?.sourceTimeLength ?? selectedClipSourceLength,
            clipId: selectedClip.id,
        };
        setClipsUsages((prev) => [...prev, newClip]);
        return newClip;
    }, [selectedClip, selectedClipLengthBars, selectedClipSourceLength, setClipsUsages]);

    const onTouchClip = useCallback((event: TouchClipEvent) => {
        const clipUsage = idToClipUsage.get(event.clipId)!;
        setSelectedClipId(
            clipUsage?.clipId,
        );
    }, [idToClipUsage, setSelectedClipId]);

    const getTrackColor = useCallback(() => "#FFF0", []);

    const handleDoubleClick = useCallback(
        (event: DoubleClickClipEvent) => {
            onDoubleClickClip(event.clipId);
        },
        [onDoubleClickClip],
    );

    return <TimelineWithClips

        rows={rows}
        clips={clipsUsages}

        bpm={bpm}
        currentTimeBars={currentTimeBars}
        loopBorders={loopBorders}
        setLoopBorders={setLoopBorders}
        rowHeight={PLAYLIST_ROW_HEIGHT}
        rowTitlesWidth={PLAYLIST_ROW_TITLES_WIDTH}
        getRowColor={getTrackColor}
        renderClip={renderClip}
        renderRowTitle={renderTrackTitle}

        onDoubleClickClip={handleDoubleClick}
        onTouchClip={onTouchClip}
        onMoveClips={onMoveClips}
        onResizeClips={onResizeClips}
        onDeleteClips={onDeleteClips}
        onPasteClip={onPasteClip}
        onAddClip={onAddClip}

        autoAlignVerticalScroll={false}

    />;
}

type PlaylistClipImplProps = {
    isSelected: boolean,
    playlistClip: PlaylistClipUsage,
    clip: Clip,
    allocator: ArrayBufferAllocator,
}

function PlaylistClipImpl({
    isSelected,
    playlistClip,
    clip,
    allocator,
}: PlaylistClipImplProps) {
    return <div className={clsx(
        styles.playlistClip,
        isSelected && styles.playlistClipSelected,
        !isSelected && styles.playlistClipUnselected,
    )}>
        <div className={styles.playlistClipTitle}>{clip.title}</div>
        {
            clip.type === "notes" && <div
                className={styles.playlistClipNotes}
                style={variables({
                    "--clipSourceTime": playlistClip.sourceTimeStart,
                    "--clipSourceLength": playlistClip.sourceTimeLength,
                })}
            >
                {notesToSvg.get(clip.notes)}
            </div>
        }
        {
            clip.type === "automation" && <div
                className={styles.playlistClipAutomation}
            >
                <div
                    className={styles.playlistClipAutomationScaleWrapper}
                    style={variables({
                        "--clipSourceTime": playlistClip.sourceTimeStart,
                        "--clipSourceLength": playlistClip.sourceTimeLength,
                    })}
                >
                    <PiecewiseGraph
                        points={clip.piecewiseFunction.points}
                        pieceData={clip.piecewiseFunction.pieceData}
                    />
                </div>
            </div>
        }
        {
            clip.type === "wave" && <div className={styles.playlistClipWave}>
                <Wave
                    allocator={allocator}
                    blobRef={clip.wave[0]}
                    startSample={playlistClip.sourceTimeStart}
                    lengthSample={playlistClip.sourceTimeLength}
                    preview={true}
                />
                {
                    clip.wave.length === 2 &&
                    <Wave
                        allocator={allocator}
                        blobRef={clip.wave[1]}
                        startSample={playlistClip.sourceTimeStart}
                        lengthSample={playlistClip.sourceTimeLength}
                        preview={true}
                    />
                }
            </div>
        }
    </div>;
}

const PlaylistClip = memo(PlaylistClipImpl);
