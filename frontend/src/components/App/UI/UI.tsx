import { ArrayBufferAllocator } from "@audio-playground/lib-common/std/ArrayBufferAllocator";
import { acquireArrayFromPool, PooledArray } from "@audio-playground/lib-common/std/ArraysPool";
import { BlobRef } from "@audio-playground/lib-common/std/BlobRef";
import { generateId } from "@audio-playground/lib-common/std/Id";
import { useStringStateInUrl } from "@audio-playground/lib-frontend/hooks/useRouting";
import { applySetState } from "@audio-playground/lib-frontend/std/ApplySetState";
import { openFile, saveFile } from "@audio-playground/lib-frontend/std/OpenFile";
import {
    SetState, SetStateValue,
    useAdd, useByKey, useSetById, useSetByKey,
} from "@audio-playground/lib-frontend/std/SetState";
import React, { SetStateAction, useCallback, useMemo, useState } from "react";

import { createEngineState } from "../../../engine/EngineState";
import { loadProjectState, saveProjectState } from "../../../engine/Persist";
import { renderMaster } from "../../../engine/render/RenderMaster";
import { Loop, LoopBorders } from "../../../types/Loop";
import { UiMode } from "../../../types/Mode";
import { PluginCommonSettings } from "../../../types/Plugin";
import { Clip } from "../../../types/project/Clip";
import { ClipUsage } from "../../../types/project/ClipUsage";
import { Project } from "../../../types/project/Project";
import { ResizeMode } from "../../../types/ResizeMode";
import { audioBufferToStereoBuffer } from "../../../utils/AudioBufferToStereoBuffer";
import { getContext } from "../../../utils/Context";
import { eventToNote } from "../../../utils/KeyboardToNotes";
import { reroutePluginOnChangeSettings } from "../../../utils/projectChanges/reroutePluginOnChangeSettings";
import {
    createPiecewiseFunctionData,
    PiecewiseFunctionData,
} from "../../../utils/SvgGraphFunctions";
import { renderWav } from "../../../utils/Wav";
import { VerticalSplitter } from "../../Common/VerticalSplitter/VerticalSplitter";
import { AutomationEditor } from "../AutomationEditor/AutomationEditor";
import { ClipsNavigator } from "../ClipsNavigator/ClipsNavigator";
import { Mixer } from "../Mixer/Mixer";
import { Panel } from "../Panel";
import { PianoRoll } from "../PianoRoll/PianoRoll";
import { Playlist } from "../Playlist/Playlist";
import { PluginUI } from "../PluginUI/PluginUI";
import { PianoRollNote } from "../TimelineWithClips/PianoRollNote";
import { WaveEditor } from "../WaveEditor/WaveEditor";
import { WavsAsyncTree } from "../WavsAsyncTree/WavsAsyncTree";
import styles from "./UI.module.css";

type UIProps = {

    project: Project,
    setProject: SetState<Project>,
    allocator: ArrayBufferAllocator,

    play: () => void,
    stop: () => void,
    isPlaying: boolean,
    onKeyPress: (note: number) => void,
    onKeyRelease: (note: number) => void,
    currentTimeBars: number | undefined,

    loop: Loop,
    setLoop: SetState<Loop>,

    channelsLevels: number[][],
}

const NEW_NOTES_TITLE = "New notes clip";
const NEW_WAVE_TITLE = "New wave clip";
const NEW_AUTOMATION_TITLE = "New automation clip";

export function UI({
    project,
    setProject,
    play,
    stop,
    isPlaying,
    onKeyPress,
    onKeyRelease,
    currentTimeBars,
    loop,
    setLoop,
    channelsLevels,
    allocator,
}: UIProps) {

    const [mode, setMode] = useStringStateInUrl<UiMode>(
        "main-ui-window",
        "playlist",
    );

    const save = useCallback(() => {
        saveFile(
            `${new Date().getTime()}.owdproj`,
            [
                {
                    description: "OpenWebDaw project file",
                    accept: { "application/json": [".owdproj"] },
                },
            ],
            async(fileHandle) => {
                const stream = await fileHandle.createWritable();
                await stream.write(
                    saveProjectState(
                        project,
                        allocator,
                    ),
                );
                await stream.close();
            },
        );
    }, [allocator, project]);

    const open = useCallback(() => {
        openFile(
            "OpenWebDaw project file",
            ".owdproj",
        ).then((res) => {
            loadProjectState(
                res,
                setProject,
                allocator,
            );
        });
    }, [allocator, setProject]);

    const render = useCallback(() => {
        saveFile(
            `${new Date().getTime()}.wav`,
            [
                {
                    description: "wav",
                    accept: { "audio/wav": [".wav"] },
                },
            ],
            async(fileHandle) => {

                const engineState = createEngineState();
                engineState.setProject(project);
                engineState.playingStartedAtSamples.set(
                    engineState.timeSamples.get() + project.bufferLength,
                );
                engineState.loop.set(loop);

                const buffer: [PooledArray, PooledArray] = [
                    acquireArrayFromPool(project.bufferLength),
                    acquireArrayFromPool(project.bufferLength),
                ];

                const lengthSamples = engineState.loopLengthSamples.get() + project.bufferLength;

                await renderWav(
                    fileHandle,
                    () => {
                        renderMaster(
                            engineState,
                            allocator.getBuffer(),
                            buffer,
                        );
                        const timeSamples = engineState.timeSamples.get();
                        console.log(`timeSamples: ${timeSamples}`);
                        if (timeSamples > lengthSamples) {
                            return Promise.resolve("finished");
                        }
                        return Promise.resolve([
                            buffer[0],
                            buffer[1],
                        ]);
                    },
                    project.bufferLength,
                    project.sampleRate,
                );

                console.log("Done");

                buffer[0].iDontNeedItAnymore();
                buffer[1].iDontNeedItAnymore();

            },
        );
    }, [allocator, loop, project]);

    const [resizeMode, setResizeMode] = useStringStateInUrl<ResizeMode>(
        "main-ui-resize-mode",
        "crop",
    );

    const [selectedClipId, setSelectedClipId] = useState(
        project.clips[0]!.id,
    );

    const setChannels = useSetByKey(setProject, "channels");

    const [
        selectedChannelId,
        setSelectedChannelId,
    ] = useStringStateInUrl<string | undefined>(
        "selected-channel-id",
        undefined,
    );
    const [
        selectedPluginId,
        setSelectedPluginId,
    ] = useStringStateInUrl<string | undefined>(
        "selected-plugin-id",
        undefined,
    );

    const openPlugin = useCallback(
        (channelId: string, instanceId: string) => {
            setSelectedChannelId(channelId);
            setSelectedPluginId(instanceId);
            setMode("plugin");
        },
        [setMode, setSelectedChannelId, setSelectedPluginId],
    );

    const selectedChannelPlugin =
        selectedChannelId === undefined || selectedPluginId === undefined
            ? undefined
            : project.channels
                .find((c) => c.id === selectedChannelId)
                ?.plugins
                ?.find((p) => p.id === selectedPluginId);

    const setSelectedPluginSettings = useCallback((
        setter: SetStateValue<PluginCommonSettings>,
    ) => {
        if (selectedChannelId === undefined || selectedPluginId === undefined) {
            return;
        }
        reroutePluginOnChangeSettings(
            setProject,
            selectedChannelId,
            selectedPluginId,
            setter,
        );
    }, [selectedChannelId, selectedPluginId, setProject]);

    const selectedClip = useMemo(() => {
        return project.clips.find((t) => t.id === selectedClipId);
    }, [project, selectedClipId]);

    const setTracks = useSetByKey(
        setProject,
        "clips",
    );

    const setTrackById = useSetById(
        setTracks,
    );

    const setSelectedTrack = useCallback(
        (v: SetStateValue<Clip>) => setTrackById(selectedClipId, v),
        [setTrackById, selectedClipId],
    );

    const setNotes = useCallback(
        (change: SetStateAction<PianoRollNote[]>) => {
            setSelectedTrack((prev) => {
                if (prev.type !== "notes") {
                    throw new Error("???");
                }
                return {
                    ...prev,
                    notes: applySetState(change, prev.notes),
                };
            });
        },
        [setSelectedTrack],
    );

    const setPiecewiseFunction = useCallback(
        (change: SetStateAction<PiecewiseFunctionData>) => {
            setSelectedTrack((prev) => {
                if (prev.type !== "automation") {
                    throw new Error("???");
                }
                return {
                    ...prev,
                    piecewiseFunction: applySetState(change, prev.piecewiseFunction),
                };
            });
        },
        [setSelectedTrack],
    );

    const renameSelectedTrack = useSetByKey(
        setSelectedTrack,
        "title",
    );

    const addTrack = useAdd(setTracks);

    const createNewWavTrack = useCallback((
        buffer: ArrayBuffer,
        title: string,
    ) => {
        const newTrackId = generateId();
        getContext()
            .decodeAudioData(buffer)
            .then((res) => {
                const b = audioBufferToStereoBuffer(res);
                Promise.all(
                    b.wave
                        .map((vals) => new Float64Array(vals))
                        .map((f) => f.buffer)
                        .map((b) => allocator.put(b, `${title}::${newTrackId}`)),
                ).then((blobRefs) => {
                    const newClip: Clip = {
                        type: "wave",
                        id: newTrackId,
                        title,
                        sampleRate: b.sampleRate,
                        wave: blobRefs as ([BlobRef] | [BlobRef, BlobRef]),
                    };
                    addTrack(newClip);
                    setSelectedClipId(newTrackId);
                });
            });
    }, [addTrack, allocator]);

    const createNewTrack = useCallback((
        type: "notes" | "automation" | "wave",
    ) => {
        const date = ` ${(new Date()).getTime()}`;
        if (type === "wave") {
            openFile().then((res) => {
                createNewWavTrack(res, NEW_WAVE_TITLE + date);
            });
        } else {
            const newTrackId = generateId();
            const newClip: Clip =
                type === "notes"
                    ? {
                        type: "notes",
                        id: newTrackId,
                        title: NEW_NOTES_TITLE + date,
                        notes: [],
                    }
                    : {
                        type: "automation",
                        id: newTrackId,
                        title: NEW_AUTOMATION_TITLE + date,
                        piecewiseFunction: createPiecewiseFunctionData(),
                    };
            addTrack(newClip);
            setSelectedClipId(newTrackId);
        }
    }, [addTrack, createNewWavTrack]);

    const [bpm, setBpm] = useByKey(project, setProject, "bpm");

    const setClipsUsages = useCallback(
        (set: SetStateValue<ClipUsage[]>) => {
            setProject((prev) => {
                const usages = prev.clipsUsages;
                const newUsages = applySetState(set, usages);
                // TODO: use some sorting that works fast for not-very-unsorted cases
                newUsages.sort(
                    (a, b) => a.timelineTimeStartBars - b.timelineTimeStartBars,
                );
                return {
                    ...prev,
                    clipsUsages: newUsages,
                };
            });
        },
        [setProject],
    );

    const copySelectedTrack = useCallback(() => {
        if (!selectedClip) {
            return;
        }
        const newTrack = {
            ...selectedClip,
            id: generateId(),
            title: `${selectedClip.title} (Copy)`,
        };
        addTrack(newTrack);
        setSelectedClipId(newTrack.id);
    }, [selectedClip, addTrack]);

    const deleteSelectedTrack = useCallback(() => {
        setClipsUsages((u) =>
            u.filter((uu) => uu.clipId !== selectedClipId),
        );
        setTracks((prev) => {
            const newTracks = prev.filter((t) => t.id !== selectedClipId);
            setSelectedClipId(newTracks[0]!.id);
            return newTracks;
        });
    }, [selectedClipId, setClipsUsages, setTracks]);

    const setLoopBorders = useCallback((
        borders: LoopBorders,
    ) => {
        if (mode === "piano-roll") {
            if (!selectedClip) {
                return;
            }
            if (selectedClip.type === "notes") {
                setLoop({
                    type: "piano-roll",
                    clipId: selectedClip.id,
                    borders,
                });
            } else if (selectedClip.type === "wave") {
                setLoop({
                    type: "wave",
                    clipId: selectedClip.id,
                    borders,
                });
            }
        } else {
            setLoop({
                type: "playlist",
                borders,
            });
        }
    }, [mode, selectedClip, setLoop]);

    const onDoubleClickClip = useCallback((id: string) => {
        const clipId = project.clipsUsages.find((u) => u.id === id)?.clipId;
        if (clipId !== undefined) {
            setMode("piano-roll");
            setSelectedClipId(clipId);
        }
    }, [project.clipsUsages, setMode]);

    const onKeyDown = (e: React.KeyboardEvent) => {
        const note = eventToNote(e);
        if (note === -1) {
            return;
        }
        onKeyPress(note);
    };

    const onKeyUp = (e: React.KeyboardEvent) => {
        const note = eventToNote(e);
        if (note === -1) {
            return;
        }
        onKeyRelease(note);
    };

    const [
        defaultChannelId,
        setDefaultChannelId,
    ] = useByKey(project, setProject, "defaultChannelId");

    const showClipsNavigator = mode === "playlist" || mode === "piano-roll";
    const showNotesEditor = mode === "piano-roll" &&
        selectedClip?.type === "notes";
    const showAutomationEditor = mode === "piano-roll" &&
        selectedClip?.type === "automation";
    const showWaveEditor = mode === "piano-roll" &&
        selectedClip?.type === "wave";
    const showPlaylist = mode === "playlist";
    const showMixer = mode === "mixer";
    const showPlugin = mode === "plugin" && selectedChannelPlugin !== undefined;

    const openClip = useCallback(
        (id: string) => {
            setSelectedClipId(id);
            setMode("piano-roll");
        },
        [setMode],
    );

    const automations = project.channels.map((c) => c.automations).flat();
    const isAutomated: (
        name: string
    ) => boolean = useCallback((
        name,
    ) => {
        if (selectedPluginId === undefined) {
            return false;
        }
        return automations.some(
            (a) => a.type === "plugin" && a.pluginInstanceId === selectedPluginId && a.knobId === name,
        );
    }, [selectedPluginId, automations]);
    const setIsAutomated: (
        name: string,
        isAutomated: boolean
    ) => void = useCallback((
        name,
        isAutomated,
    ) => {
        if (selectedChannelId === undefined || selectedPluginId === undefined) {
            return;
        }
        setChannels((prev) => {
            return prev.map((channel) => {
                if (channel.id !== selectedChannelId) {
                    return channel;
                }
                const automations = [...channel.automations];
                if (isAutomated) {
                    automations.push({
                        id: generateId(),
                        type: "plugin",
                        pluginInstanceId: selectedPluginId,
                        knobId: name,
                    });
                } else {
                    const idx = automations.findIndex(
                        (a) => a.type === "plugin" &&
                            a.pluginInstanceId === selectedPluginId &&
                            a.knobId === name,
                    );
                    if (idx >= 0) {
                        const automation = automations[idx]!;
                        setClipsUsages((prev) => prev.filter((usage) =>
                            usage.rowId !== automation.id,
                        ));
                        automations.splice(idx, 1);
                    }
                }
                return {
                    ...channel,
                    automations,
                };
            });
        });
    }, [selectedChannelId, selectedPluginId, setChannels, setClipsUsages]);

    return <>
        <div
            className={styles.ui}
            onKeyDown={onKeyDown}
            onKeyUp={onKeyUp}
        >
            <div className={styles.topPanel}>
                <Panel
                    onSave={save}
                    onOpen={open}
                    onRender={render}
                    onPlay={play}
                    onStop={stop}
                    isPlaying={isPlaying}
                    bpm={bpm}
                    setBpm={setBpm}
                    mode={mode}
                    setMode={setMode}
                    resizeMode={resizeMode}
                    setResizeMode={setResizeMode}
                    gridMode={"drag"}
                    setGridMode={() => {
                        console.log("setGridMode not implemented");
                    }}
                    zoomMode={"zoomIn"}
                    setZoomMode={() => {
                        console.log("setZoomMode not implemented");
                    }}
                    mixSideMode={"left"}
                    setMixSideMode={() => {
                        console.log("setMixSideMode not implemented");
                    }} />
            </div>
            <div className={styles.content}>
                {showClipsNavigator && <div className={styles.sidePanel}>
                    <VerticalSplitter
                        top={
                            <ClipsNavigator
                                selectedClip={selectedClip}
                                project={project}
                                renameSelectedClip={renameSelectedTrack}
                                selectClip={setSelectedClipId}
                                openClip={openClip}
                                createNewClip={createNewTrack}
                                copySelectedClip={copySelectedTrack}
                                deleteSelectedClip={
                                    project.clips.length > 1
                                        ? deleteSelectedTrack
                                        : undefined
                                }
                            />}
                        bottom={mode === "playlist" && <WavsAsyncTree
                            onAdd={(name, buffer) => createNewWavTrack(buffer, name)}
                        />}
                    />
                </div>}
                <div className={styles.mainContent}>

                    { showNotesEditor && <PianoRoll
                        notes={selectedClip.notes}
                        setNotes={setNotes}
                        onKeyPress={onKeyPress}
                        onKeyRelease={onKeyRelease}
                        currentTimeBars={currentTimeBars}
                        bpm={bpm}
                        loopBorders={
                            loop.type === "piano-roll" && loop.clipId === selectedClipId
                                ? loop.borders
                                : undefined
                        }
                        setLoopBorders={setLoopBorders}
                    /> }

                    { showAutomationEditor && <AutomationEditor
                        piecewiseFunction={selectedClip.piecewiseFunction}
                        setPiecewiseFunction={setPiecewiseFunction}
                    /> }

                    { showWaveEditor && <WaveEditor
                        bpm={bpm}
                        allocator={allocator}
                        wave={selectedClip.wave}
                        sampleRate={selectedClip.sampleRate}
                        loopBorders={
                            loop.type === "wave" && loop.clipId === selectedClipId
                                ? loop.borders
                                : undefined
                        }
                        setLoopBorders={setLoopBorders}
                    /> }

                    { showPlaylist && <Playlist
                        allocator={allocator}
                        channels={project.channels}
                        channelsLevels={channelsLevels}
                        setProject={setProject}
                        clips={project.clips}
                        clipsUsages={project.clipsUsages}
                        setClipsUsages={setClipsUsages}
                        selectedClip={selectedClip}
                        setSelectedClipId={setSelectedClipId}
                        onDoubleClickClip={onDoubleClickClip}
                        currentTimeBars={currentTimeBars}
                        bpm={bpm}
                        loopBorders={
                            loop.type === "playlist"
                                ? loop.borders
                                : undefined
                        }
                        setLoopBorders={setLoopBorders}
                        defaultChannelId={defaultChannelId}
                        setDefaultChannelId={setDefaultChannelId}
                        resizeMode={resizeMode}
                    /> }

                    { showMixer && <Mixer
                        channels={project.channels}
                        project={project}
                        setChannels={setChannels}
                        channelsLevels={channelsLevels}
                        setProject={setProject}
                        defaultChannelId={defaultChannelId}
                        setDefaultChannelId={setDefaultChannelId}
                        openPlugin={openPlugin}
                    /> }

                    { showPlugin && <PluginUI
                        channelPlugin={selectedChannelPlugin}
                        isAutomated={isAutomated}
                        setIsAutomated={setIsAutomated}
                        setPluginSettings={setSelectedPluginSettings}
                    /> }

                </div>
            </div>
        </div>
    </>;
}
