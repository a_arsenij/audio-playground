import React from "react";

import { Clip } from "../../../types/project/Clip";
import { Project } from "../../../types/project/Project";
import { Button } from "../../Common/Button/Button";
import { Svg18ClipsAddClipEnvelope } from "../../Common/GeneratedIcons/18/Clips/AddClip/Envelope";
import { Svg18ClipsAddClipNotes } from "../../Common/GeneratedIcons/18/Clips/AddClip/Notes";
import { Svg18ClipsAddClipWave } from "../../Common/GeneratedIcons/18/Clips/AddClip/Wave";
import { Svg18CommonCopy } from "../../Common/GeneratedIcons/18/Common/Copy";
import { Svg18CommonDelete } from "../../Common/GeneratedIcons/18/Common/Delete";
import { TextInput } from "../../Common/Input/Input";
import { LabeledContainer } from "../../Common/LabeledContainer/LabeledContainer";
import { OptionallySelectedBox } from "../../Common/OptionallySelectedBox/OptionallySelectedBox";
import styles from "./ClipsNavigator.module.css";

type ClipsNavigatorProps = {
    project: Project,
    selectedClip: Clip | undefined,
    renameSelectedClip: (newName: string) => void,
    copySelectedClip: () => void,
    deleteSelectedClip: (() => void) | undefined,
    selectClip: (trackId: string) => void,
    openClip: (id: string) => void,
    createNewClip: (type: "notes" | "automation" | "wave") => void,
}

function ClipsNavigatorRaw({
    project,
    selectedClip,
    renameSelectedClip,
    selectClip,
    createNewClip,
    deleteSelectedClip,
    copySelectedClip,
    openClip,
}: ClipsNavigatorProps) {
    return <div className={styles.clipsNavigator}>

        { selectedClip && <LabeledContainer label={"Selected clip:"}>
            <div className={styles.trackInfo}>
                <TextInput value={selectedClip.title} onChange={renameSelectedClip}/>
                <div className={styles.trackActions}>
                    <Button
                        title={"Delete selected clip"}
                        disabled={!deleteSelectedClip}
                        onClick={deleteSelectedClip}
                        iconLeft={Svg18CommonDelete}
                    />
                    <Button
                        title={"Clone selected clip"}
                        onClick={copySelectedClip}
                        iconLeft={Svg18CommonCopy}
                    />
                    <Button
                        title={"Add notes"}
                        onClick={() => createNewClip("notes")}
                        iconLeft={Svg18ClipsAddClipNotes}
                    />
                    <Button
                        title={"Add automation"}
                        onClick={() => createNewClip("automation")}
                        iconLeft={Svg18ClipsAddClipEnvelope}
                    />
                    <Button
                        title={"Add wave"}
                        onClick={() => createNewClip("wave")}
                        iconLeft={Svg18ClipsAddClipWave}
                    />
                </div>
            </div>
        </LabeledContainer> }

        <div className={styles.tracksList}>
            <LabeledContainer label={"Clips"}>
                {project.clips.map((t) =>
                    <OptionallySelectedBox
                        isSelected={t.id === selectedClip?.id}
                        key={t.id}
                        onClick={() => selectClip(t.id)}
                        onDoubleClick={() => openClip(t.id)}
                    >
                        <div className={styles.clipIcon}>
                            {t.type === "notes" && <Svg18ClipsAddClipNotes/>}
                            {t.type === "automation" && <Svg18ClipsAddClipEnvelope/>}
                            {t.type === "wave" && <Svg18ClipsAddClipWave/>}
                        </div>
                        <div className={styles.clipTitle}>
                            {t.title}
                        </div>
                    </OptionallySelectedBox>,
                )}
            </LabeledContainer>
        </div>
        <div className={styles.space}></div>

    </div>;
}

export const ClipsNavigator = React.memo(ClipsNavigatorRaw);
