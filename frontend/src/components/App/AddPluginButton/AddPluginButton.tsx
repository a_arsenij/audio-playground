import React, { useState } from "react";

import { nameToPluginRenderer } from "../../../plugins/_PluginsRenderers";
import { PluginCommonSettings } from "../../../types/Plugin";
import { PluginRenderer } from "../../../types/PluginRenderer";
import { Button } from "../../Common/Button/Button";
import { Svg18SignPlus } from "../../Common/GeneratedIcons/18/Sign/Plus";
import { AddPluginDialog } from "../AddPluginDialog/AddPluginDialog";

type AddPluginButtonProps = {
    title: string,
    onAddPlugin: (p: PluginRenderer<PluginCommonSettings, object>) => void,
};

export function AddPluginButton({
    title,
    onAddPlugin,
}: AddPluginButtonProps) {

    const [open, setOpen] = useState(false);
    return <>
        <Button
            style={"ghost"}
            onClick={() => setOpen(true)}
            title={title}
            iconLeft={Svg18SignPlus}
        />
        <AddPluginDialog
            open={open}
            onClose={() => setOpen(false)}
            onAddPlugin={(p) => {
                onAddPlugin(
                    nameToPluginRenderer(p) as unknown as PluginRenderer<PluginCommonSettings, object>,
                );
                setOpen(false);
            }}
        />
    </>;
}
