import {
    SetState,
} from "@audio-playground/lib-frontend/std/SetState";
import { amplitudeToDb } from "@audio-playground/lib-sound/AmplitudeToDecibells";
import React, { useCallback } from "react";

import { PluginCommonSettings } from "../../../types/Plugin";
import { PluginRenderer } from "../../../types/PluginRenderer";
import { Channel } from "../../../types/project/Channel";
import { Project } from "../../../types/project/Project";
import { addNthPlugin } from "../../../utils/projectChanges/addPlugin";
import { isChannelAllowedToBeDeleted, removeChannel } from "../../../utils/projectChanges/removeChannel";
import { Button } from "../../Common/Button/Button";
import { Svg18CommonDelete } from "../../Common/GeneratedIcons/18/Common/Delete";
import { MultiMeter } from "../../Common/MultiMeter/MultiMeter";
import { AddPluginButton } from "../AddPluginButton/AddPluginButton";
import styles from "./Channel.module.css";

type ChannelProps = {
    channel: Channel,
    channelIndex: number,
    channelsNumber: number,
    setProject: SetState<Project>,
    channelLevel: number[],
    isDefaultChannel: boolean,
    setDefault: (id: string) => void,
    openPlugin: (instanceId: string) => void,
    expand?: () => void,
}
export function ChannelComp(props: ChannelProps) {

    const {
        setProject,
        channel,
        channelLevel,
        expand,
        channelIndex,
    } = props;

    const channelId = channel.id;
    const onAddPlugin = useCallback((p: PluginRenderer<PluginCommonSettings, object>) => {
        addNthPlugin(
            setProject,
            channelId,
            p,
            0,
        );
    }, [setProject, channelId]);

    return <div className={styles.channel}>
        <div className={styles.meter}>
            <MultiMeter
                value={channelLevel.map((v) => amplitudeToDb(v))}
                orientation={"vertical"}
            />
        </div>
        <div className={styles.title}>
            {channel.title}
            <Button title={"expand"} onClick={expand} showBothIconAndTitle={true}/>
            { isChannelAllowedToBeDeleted(channelIndex) && <Button
                onClick={() => removeChannel(setProject, channelIndex)}
                title={"Delete channel"}
                iconLeft={Svg18CommonDelete}
            /> }
        </div>
        {
            channel.plugins.length === 0 && <AddPluginButton
                title={"Add plugin"}
                onAddPlugin={onAddPlugin}
            />
        }
    </div>;
}
