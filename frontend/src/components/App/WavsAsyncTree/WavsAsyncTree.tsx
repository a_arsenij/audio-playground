
import React, { useState } from "react";

import { fromBase64 } from "../../../utils/base64";
import { getContext } from "../../../utils/Context";
import { AsyncTree, AsyncTreeLoader } from "../../Common/AsyncTree/AsyncTree";
import { Button } from "../../Common/Button/Button";
import { Svg18PlayStatePlay } from "../../Common/GeneratedIcons/18/PlayState/Play";
import { Svg18PlayStateStop } from "../../Common/GeneratedIcons/18/PlayState/Stop";
import { Svg18SignPlus } from "../../Common/GeneratedIcons/18/Sign/Plus";
import { Wave } from "../../Common/Wave/Wave";
import styles from "./WavsAsyncTree.module.css";

type WavsAsyncTreeProps = {
    onAdd: (filename: string, wav: ArrayBuffer) => void,
}

export function WavsAsyncTree(
    props: WavsAsyncTreeProps,
) {
    return <div className={styles.wavsAsyncTreeWrapper}>
        <div className={styles.wavsAsyncTree}>
            <AsyncTree
                load={WAVS_REMOTE_FOLDER}
                renderFile={(path, meta) => <WavsFile
                    path={path}
                    meta={meta}
                    onAdd={props.onAdd}
                />}
            />
        </div>
    </div>;
}

type WavFileMeta = {
    sampleRate: number,
    channels: number,
    length: number,
    wave: string,
}

type WavsFileProps = {
    path: string[],
    meta: WavFileMeta,
    onAdd: (filename: string, wav: ArrayBuffer) => void,
}



const waveComponentCache = new Map<string, React.ReactNode>();
const wavsCache = new Map<string, Promise<ArrayBuffer>>();

function getWav(path: string[]): Promise<ArrayBuffer> {
    const joined = path.join("/");
    return wavsCache.getOrPut(
        joined,
        () => fetch(
            [
                "samples-library",
                ...path,
            ].join("/"),
        )
            .then((r) => r.arrayBuffer()),
    ).then((r) => r.slice(0, r.byteLength));
}

function WavsFile({
    path, meta, onAdd,
}: WavsFileProps) {
    const [
        isPlaying,
        setIsPlaying,
    ] = useState<AudioBufferSourceNode | undefined>(undefined);
    return <div className={styles.wavFile}>
        <div className={styles.wavFileName}>
            {path.at(-1)}
        </div>
        <div className={styles.wavFileProperties}>
            {meta.sampleRate} Hz / {Math.round(meta.length * 1000 / meta.sampleRate) / 1000} sec
            / {meta.channels} ch
        </div>
        <div
            className={styles.wavFileWave}
        >
            {waveComponentCache.getOrPut(
                meta.wave,
                () => {
                    const bytes = fromBase64(meta.wave)
                        .map((v) => (v - 128) / 127);
                    return <Wave
                        values={bytes}
                        startSample={0}
                        lengthSample={bytes.length}
                    />;
                },
            )}
            <div className={styles.wavFileButtons}>
                <Button
                    onClick={() => {
                        if (isPlaying) {
                            isPlaying.stop();
                            isPlaying.disconnect();
                            setIsPlaying(undefined);
                            return;
                        }
                        const context = getContext();
                        getWav(path).then((r) => {
                            context.decodeAudioData(r.slice(0, r.byteLength), function(buffer) {
                                const source = context.createBufferSource();
                                setIsPlaying(source);
                                source.buffer = buffer;
                                source.connect(context.destination);
                                source.start(0);
                                source.addEventListener("ended", () => {
                                    source.disconnect();
                                    setIsPlaying(undefined);
                                });
                            });
                        });
                    }}
                    iconLeft={isPlaying ? Svg18PlayStateStop : Svg18PlayStatePlay}
                    title={"Play once"}
                />
                <Button
                    onClick={() => {
                        getWav(path).then((r) => {
                            onAdd(path.at(-1)!, r);
                        });
                    }}
                    iconLeft={Svg18SignPlus}
                    title={"Add to project"}
                />
            </div>
        </div>
    </div>;
}

const WAVS_REMOTE_FOLDER: AsyncTreeLoader<WavFileMeta> = (path: string[]) => {
    console.log("path: ", path);
    return fetch(`${[
        "samples-library-metainfo",
        ...path,
        "index.json",
    ].join("/")}`)
        .then((r) => r.json())
        .then((r) => {
            return {
                folders: r.dirs,
                files: Object.fromEntries(r.files.map((f: any) => [
                    f.name,
                    {
                        sampleRate: f.sampleRate,
                        channels: f.channels,
                        length: f.length,
                        wave: f.preview,
                    },
                ])),
            };
        });
};
