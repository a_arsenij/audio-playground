import { useReffedState } from "@audio-playground/lib-frontend/hooks/useReffedState";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React, { useRef } from "react";

import { LoopBorders } from "../../../../types/Loop";
import { Carret } from "../Carret/Carret";
import styles from "./BarTitles.module.css";

type BarTitlesProps = {
    contentWidthBar: number,
    loopBorders: LoopBorders | undefined,
    setLoopBorders: ((newLoopBorders: LoopBorders) => void) | undefined,
    currentTimeBars: number | undefined,
}

function BarTitlesRaw({
    contentWidthBar,
    loopBorders,
    setLoopBorders,
    currentTimeBars,
}: BarTitlesProps) {
    const ref = useRef<HTMLDivElement>(null);

    const [loopStart, loopStartRef, setLoopStart] =
        useReffedState<number | undefined>(() => undefined);
    const [loopEnd, loopEndRef, setLoopEnd] =
        useReffedState<number | undefined>(() => undefined);

    function handleDown(e: React.MouseEvent, idx: number) {
        if (!setLoopBorders) {
            return;
        }
        e.preventDefault();
        setLoopStart(idx);

        function getRects() {
            const parent = ref.current!;
            const children = parent.getElementsByClassName(styles.barTitle!);
            return Array.from(children).map((e) => e.getBoundingClientRect());
        }

        function handleMove(e: MouseEvent) {
            e.preventDefault();
            e.stopPropagation();
            if (e.buttons & 1) {
                const rects = getRects();
                setLoopEnd(
                    rects.findIndex((r) => r.x <= e.clientX && e.clientX <= r.x + r.width),
                );
            }
        }
        function handleUp(e: MouseEvent) {
            e.preventDefault();
            e.stopPropagation();
            window.removeEventListener("mousemove", handleMove);
            window.removeEventListener("mouseup", handleUp);
            const a = loopStartRef.current;
            const b = loopEndRef.current;
            if (
                a !== undefined &&
                b !== undefined &&
                a !== b
            ) {
                setLoopBorders && setLoopBorders({
                    timeStartBar: Math.min(a),
                    timeEndBar: Math.max(b),
                });
            }
            setLoopStart(undefined);
            setLoopEnd(undefined);
        }

        window.addEventListener("mousemove", handleMove);
        window.addEventListener("mouseup", handleUp);
    }

    return <div
        className={styles.barTitles}
        ref={ref}
    >
        <div
            className={styles.emptySquare}
        />
        {[...Array(Math.ceil(contentWidthBar))].map((_, idx) =>
            <div
                key={idx}
                className={styles.barTitle}
                onMouseDown={(e) => handleDown(e, idx)}
            >
                {idx}
            </div>,
        )}
        {loopStart !== undefined && <div
            className={styles.carret}
            style={variables({ "--bar": loopStart })}
        >
            <Carret char={"s"}/>
        </div>}
        {loopBorders !== undefined && <div
            className={styles.carret}
            style={variables({ "--bar": loopBorders.timeStartBar })}
        >
            <Carret char={"S"}/>
        </div> }
        {loopEnd !== undefined && <div
            className={styles.carret}
            style={variables({ "--bar": loopEnd })}
        >
            <Carret char={"e"}/>
        </div> }
        {loopBorders !== undefined && <div
            className={styles.carret}
            style={variables({ "--bar": loopBorders.timeEndBar })}
        >
            <Carret char={"E"}/>
        </div> }
        {
            loopBorders !== undefined &&
            currentTimeBars !== undefined &&
            <div
                className={styles.carret}
                style={variables({ "--bar": currentTimeBars })}
            >
                <Carret char={"C"}/>
            </div> }
    </div>;
}

export const BarTitles = React.memo(BarTitlesRaw);
