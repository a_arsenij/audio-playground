import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React from "react";

import { ClipComponentProps, TimelineClip } from "../TimelineWithClips";
import styles from "./Clip.module.css";

type ClipProps<
    ROW_ID,
    CLIP extends TimelineClip<ROW_ID>
> = ClipComponentProps<ROW_ID, CLIP> & {
    onDoubleClickClip: () => void,
    renderClip: (clip: CLIP, isSelected: boolean) => React.ReactNode,
};

export function Clip<ROW_ID, CLIP extends TimelineClip<ROW_ID>>(props: ClipProps<ROW_ID, CLIP>) {
    return <div
        style={variables({
            "--startBar": props.timelineTimeStartBars,
            "--lengthBars": props.timelineTimeLengthBars,
        })}
        className={clsx(
            styles.clip,
            props.isSelected && styles.clipSelected,
            !props.isSelected && styles.clipUnselected,
        )}
        onDoubleClick={props.onDoubleClickClip}
        key={props.id}
    >
        {props.renderClip(props, props.isSelected)}
        <div className={styles.resizeBefore}/>
        <div className={styles.resizeAfter}/>
    </div>;
}
