import { useReffedState } from "@audio-playground/lib-frontend/hooks/useReffedState";
import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import React, { useMemo, useRef } from "react";

import { LoopBorders } from "../../../types/Loop";
import { Timeline } from "../../Common/Timeline/Timeline";
import { RowsElemsImpl } from "./RowsElems/RowsElems";
import { RowsTitlesImpl } from "./RowsTitles/RowsTitles";

type ClipDim<ROW_ID> = {
    timelineTimeStartBars: number,
    timelineTimeLengthBars: number,
    rowId: ROW_ID,
}

export type TimelineClip<ROW_ID> = ClipDim<ROW_ID> & {
    id: string,
}

export type TimelineRow<ROW_ID> = {
    id: ROW_ID,
}

export type ClipComponentProps<ROW_ID, CLIP extends TimelineClip<ROW_ID>> = CLIP & {
    isSelected: boolean,
}

export type MoveClipEvent<ROW_ID, CLIP extends TimelineClip<ROW_ID>> = {
    clips: CLIP[],
    deltaTime: number,
    deltaRow: number,
}

export type ResizeClipEvent<ROW_ID, CLIP extends TimelineClip<ROW_ID>> = {
    clips: CLIP[],
    deltaTime: number,
    side: "start" | "end",
}

export type DeleteClipEvent = {
    clipIds: string[],
}

export type AddClipEvent<
    ROW_ID,
    ROW extends TimelineRow<ROW_ID>,
    CLIP extends TimelineClip<ROW_ID>
> = {
    row: ROW,
    rowIdx: number,
    timeBars: number,
    selectedUsage: CLIP | undefined,
}

export type TouchClipEvent = {
    clipId: string,
}

export type DoubleClickClipEvent = {
    clipId: string,
}

export type PasteClipEvent<ROW_ID, CLIP extends TimelineClip<ROW_ID>> = {
    clips: CLIP[],
}

export type CommonTimelineProps = {
    loopBorders: LoopBorders | undefined,
    setLoopBorders: (newLoopBorders: LoopBorders) => void,
    currentTimeBars: number | undefined,
    bpm: number,
}

type TimelineProps<
    ROW_ID,
    ROW extends TimelineRow<ROW_ID>,
    CLIP extends TimelineClip<ROW_ID>
> = CommonTimelineProps & {
    rows: ROW[],
    clips: CLIP[],
    rowHeight: number,
    getRowColor: (row: ROW) => string,
    renderRowTitle: (row: ROW) => React.ReactNode,
    renderClip: (clip: CLIP, isSelected: boolean) => React.ReactNode,
    onDeleteClips: (event: DeleteClipEvent) => void,
    onMoveClips: (event: MoveClipEvent<ROW_ID, CLIP>) => void,
    onResizeClips: (event: ResizeClipEvent<ROW_ID, CLIP>) => void,
    onAddClip: (event: AddClipEvent<ROW_ID, ROW, CLIP>) => CLIP | undefined,
    onPasteClip: (event: PasteClipEvent<ROW_ID, CLIP>) => CLIP[],
    rowTitlesWidth: number,
    onTouchClip: (event: TouchClipEvent) => void,
    onDoubleClickClip: (event: DoubleClickClipEvent) => void,
    autoAlignVerticalScroll: boolean,
}

export function defaultMoveHandler<
    ROW_ID,
    ROW extends TimelineRow<ROW_ID>,
    CLIP extends TimelineClip<ROW_ID>
>(
    setClips: SetState<CLIP[]>,
    event: MoveClipEvent<ROW_ID, CLIP>,
    rows: ROW[],
) {

    const idToRowIndex = new Map(
        rows.map((t, idx) => [t.id, idx]),
    );
    const indexToRowId = new Map(
        rows.map((t, idx) => [idx, t.id]),
    );

    const movedClips = new Map(event.clips.map((c) => [c.id, c]));
    setClips((prevClips) => prevClips.map((note) => {
        const movedClip = movedClips.get(note.id);
        if (!movedClip) {
            return note;
        }
        const oldRowIndex = idToRowIndex.get(movedClip.rowId)!;
        const newRowIndex = oldRowIndex + event.deltaRow;
        const newRowId = indexToRowId.get(newRowIndex);

        if (newRowId === undefined) {
            return note;
        }

        return {
            ...note,
            rowId: newRowId,
            timelineTimeStartBars: movedClip.timelineTimeStartBars + event.deltaTime,
        };
    }));
}

export function TimelineWithClips<
    ROW_ID,
    ROW extends TimelineRow<ROW_ID>,
    CLIP extends TimelineClip<ROW_ID>,
>(
    props: TimelineProps<ROW_ID, ROW, CLIP>,
) {

    const {
        clips,
        loopBorders,
        setLoopBorders,
        currentTimeBars,
        rowTitlesWidth,
        rowHeight,

        rows,
        getRowColor,
        renderRowTitle,

        ...rest
    } = props;
    const rootRef = useRef<HTMLDivElement>(null);

    const contentWidthBars = useMemo(() => {
        return Math.max(
            17,
            clips
                .map((c) => c.timelineTimeStartBars + c.timelineTimeLengthBars)
                .reduce((a, b) => Math.max(a, b), 4) * 2,
        );
    }, [clips]);

    const [pxPerBar, pxPerBarRef, setPxPerBar] = useReffedState(() => 100);

    const timelineRef = useRef<HTMLDivElement>(null);

    return <Timeline
        loopBorders={loopBorders}
        setLoopBorders={setLoopBorders}
        currentTimeBars={currentTimeBars}
        timelineRef={timelineRef}
        gridRef={rootRef}
        rowsTitles={
            <RowsTitlesImpl
                rows={rows}
                getRowColor={getRowColor}
                renderRowTitle={renderRowTitle}
            />
        }
        rowsElems={
            <RowsElemsImpl
                {...rest}
                timelineRef={timelineRef}
                rootRef={rootRef}
                rows={rows}
                clips={clips}
                getRowColor={getRowColor}
                pxPerBarRef={pxPerBarRef}
            />
        }
        contentWidthBars={contentWidthBars}
        rowTitlesWidth={rowTitlesWidth}
        rowHeight={rowHeight}
        pxPerBar={pxPerBar}
        setPxPerBar={setPxPerBar}
    />;
}
