import { getOrPut } from "@audio-playground/lib-common/std/GetOrPut";
import { useReffedState } from "@audio-playground/lib-frontend/hooks/useReffedState";
import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import { LEFT_BUTTON, RIGHT_BUTTON } from "@audio-playground/lib-frontend/std/MouseButtons";
import { someInputIsFocused } from "@audio-playground/lib-frontend/std/SomeInputIsFocused";
import React, { RefObject, useCallback, useEffect, useMemo, useRef } from "react";

import { Clip } from "../Clip/Clip";
import {
    AddClipEvent,
    DeleteClipEvent, DoubleClickClipEvent, MoveClipEvent,
    PasteClipEvent, ResizeClipEvent,
    TimelineClip,
    TimelineRow,
    TouchClipEvent,
} from "../TimelineWithClips";
import styles from "./RowsElems.module.css";

const RESIZE_AREA_SIZE = 1 / 8;
const ROUND_TO = 1 / 8;

type RowsElemsProps<
    ROW_ID,
    ROW extends TimelineRow<ROW_ID>,
    CLIP extends TimelineClip<ROW_ID>,
> = {
    timelineRef: RefObject<HTMLDivElement>,
    rootRef: RefObject<HTMLDivElement>,
    rows: ROW[],
    clips: CLIP[],
    getRowColor: (row: ROW, rowNumber: number) => string,
    autoAlignVerticalScroll: boolean,
    onPasteClip: (event: PasteClipEvent<ROW_ID, CLIP>) => CLIP[],
    onDeleteClips: (event: DeleteClipEvent) => void,
    onTouchClip: (event: TouchClipEvent) => void,
    onAddClip: (event: AddClipEvent<ROW_ID, ROW, CLIP>) => CLIP | undefined,
    onMoveClips: (event: MoveClipEvent<ROW_ID, CLIP>) => void,
    onResizeClips: (event: ResizeClipEvent<ROW_ID, CLIP>) => void,
    onDoubleClickClip: (event: DoubleClickClipEvent) => void,
    renderClip: (clip: CLIP, isSelected: boolean) => React.ReactNode,
    pxPerBarRef: RefObject<number>,
}

export function RowsElemsImpl<
    ROW_ID,
    ROW extends TimelineRow<ROW_ID>,
    CLIP extends TimelineClip<ROW_ID>,
>({
    timelineRef,
    rootRef,
    rows,
    clips,
    getRowColor,
    autoAlignVerticalScroll,
    onPasteClip,
    onDeleteClips,
    onTouchClip,
    onAddClip,
    onMoveClips,
    onResizeClips,
    onDoubleClickClip,
    renderClip,
    pxPerBarRef,
}: RowsElemsProps<ROW_ID, ROW, CLIP>) {

    const buildIdToClip = useCallback(() => {
        return new Map(clips.map((c) => [c.id, c]));
    }, [clips]);

    const rowIdToIdx = useMemo(() => {
        return new Map(rows.map((t, idx) => [t.id, idx]));
    }, [rows]);

    const [, moveStartedAtRef, setMoveStartedAt] =
        useReffedState<[boolean, number, number]>(() => [false, 0, 0]);

    const [, dragMode, setDragMode] = useReffedState<
        | "move"
        | "resize-start"
        | "resize-end"
    >(() => "move");
    const buttonsPressed = useRef([false, false]);
    const [
        frameStartedAt,
        frameStartedAtRef,
        setFrameStartedAt,
    ] = useReffedState<[boolean, number, number]>(() => [false, 0, 0]);
    const [, frameEndRef, setFrameEnd] = useReffedState<[number, number]>(() => [0, 0]);
    const [, copyRef, setCopy] = useReffedState<CLIP[]>(() => []);

    const [
        selectedClips,
        selectedClipsRef,
        setSelectedClips,
    ] = useReffedState(() => new Map<string, CLIP>());

    const rowIdxToClips = useMemo(() => {
        const rowIdToIdx = new Map(
            rows.map((row, idx) => [row.id, idx]),
        );
        const map = new Map<number, CLIP[]>();
        clips.forEach((clip) => {
            getOrPut(
                map,
                rowIdToIdx.get(clip.rowId)!,
                () => [],
            ).push(clip);
        });
        return map;
    }, [rows, clips]);

    const refToMiddleElement = useRef<HTMLDivElement>(null);
    const midElementIdx = useMemo(() => {
        if (!autoAlignVerticalScroll) {
            return 0;
        }
        const rowIdxToAmountOfClips = new Map<number, number>();
        clips.forEach((c) => {
            const rowIdx = rowIdToIdx.get(c.rowId)!;
            const old = rowIdxToAmountOfClips.get(rowIdx) || 0;
            rowIdxToAmountOfClips.set(rowIdx, old + 1);
        });
        let rowIdx = -1;
        let max = 0;
        for (let idx = 0; idx < rows.length; idx++) {
            const value = rowIdxToAmountOfClips.get(idx);
            if (value && value > max) {
                max = value;
                rowIdx = idx;
            }
        }
        return rowIdx;
        // I WANT THIS TO BE COUNTED ONLY ONCE AT MOUNT, NO MATTER WHAT CHANGES
        // THAT'S WHY I DISABLE DEPENDENCY CHECK
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (!autoAlignVerticalScroll) {
            return;
        }
        refToMiddleElement.current?.scrollIntoView({
            behavior: "instant",
            block: "center",
        });
        timelineRef.current!.scrollTo({
            behavior: "instant",
            left: 0,
        });
        // I WANT THIS TO BE RUN ONLY ONCE AT MOUNT, NO MATTER WHAT CHANGES
        // THAT'S WHY I DISABLE DEPENDENCY CHECK
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onCopy = useCallback(() => {
        const clipsMap = buildIdToClip();
        setCopy(
            [...selectedClipsRef.current.keys()]
                .map((id) => clipsMap.get(id))
                .filter((v) => v)
                .map((c) => ({
                    ...c!,
                })),
        );
    }, [buildIdToClip, selectedClipsRef, setCopy]);

    useEffect(() => {
        const handle = (event: KeyboardEvent) => {
            if (someInputIsFocused()) {
                return;
            }

            function handled() {
                event.preventDefault();
                event.stopPropagation();
            }
            if (event.ctrlKey && event.key === "c") {
                onCopy();
                return handled();
            } else if (event.ctrlKey && event.key === "v") {
                const toCopy = copyRef.current;
                if (toCopy.length === 0) {
                    return;
                }
                const minT = toCopy
                    .map((c) => c.timelineTimeStartBars)
                    .reduce((a, b) => Math.min(a, b));
                const maxT = toCopy
                    .map((c) => c.timelineTimeStartBars + c.timelineTimeLengthBars)
                    .reduce((a, b) => Math.max(a, b));
                const length = Math.ceil(maxT - minT);
                const newInstances: CLIP[] = toCopy.map((c) => ({
                    ...c,
                    timelineTimeStartBars: c.timelineTimeStartBars + length,
                }));
                const newSelectedIds = new Map<string, CLIP>();
                onPasteClip({ clips: newInstances }).forEach((c, idx) => {
                    newInstances[idx]!.id = c.id;
                    newSelectedIds.set(c.id, c);
                });
                setSelectedClips(newSelectedIds);
                return handled();
            } else if (event.ctrlKey && event.key === "a") {
                setSelectedClips(new Map(clips.map((c) => [c.id, c])));
                return handled();
            } else if (event.key === "Delete") {
                onDeleteClips({
                    clipIds: [...selectedClipsRef.current.keys()],
                });
                setSelectedClips(new Map());
                return handled();
            }
        };
        window.addEventListener("keydown", handle);
        return () => {
            window.removeEventListener("keydown", handle);
        };
    }, [onCopy, clips, onDeleteClips, onPasteClip, copyRef, setSelectedClips, selectedClipsRef]);

    const toBars = useCallback((x: number) => {
        const compX = rootRef.current!.getBoundingClientRect().x;
        return (x - compX) / pxPerBarRef.current!;
    }, [rootRef, pxPerBarRef]);

    const round = useCallback((x: number) => {
        return Math.floor(toBars(x) / ROUND_TO) * ROUND_TO;
    }, [toBars]);

    const getPointedClips = useCallback((timeBars: number, rowIdx: number) => {
        return rowIdxToClips
            .get(rowIdx)
            ?.filter((c) =>
                c.timelineTimeStartBars <= timeBars &&
                    timeBars < (c.timelineTimeStartBars + c.timelineTimeLengthBars),
            )
            ?.map((c) => c) ||
            [];
    }, [rowIdxToClips]);

    const setOriginalStartsValues = useCallback((
        newSelected: {keys: () => IterableIterator<string>},
        timeBars: number,
        rowIdx: number,
        additionalOriginals: CLIP[] = [],
    ) => {

        const idToClip = buildIdToClip();

        const newSelectedMap = new Map(
            [...newSelected.keys()].map((id) => [id, idToClip.get(id)!]),
        );
        additionalOriginals.forEach((v) => {
            newSelectedMap.set(v.id, v);
        });

        setSelectedClips(newSelectedMap);

        setMoveStartedAt([
            true,
            timeBars,
            rowIdx,
        ]);
    }, [
        buildIdToClip,
        setMoveStartedAt,
        setSelectedClips,
    ]);

    const onMouseDown = useCallback((event: React.MouseEvent, rowIdx: number) => {
        const onMouseUp = () => {
            if (event.button === LEFT_BUTTON) {
                buttonsPressed.current[0] = false;
            } else if (event.button === RIGHT_BUTTON) {
                buttonsPressed.current[1] = false;
            }
            setMoveStartedAt([false, 0, 0]);
            setFrameStartedAt([false, 0, 0]);
            window.removeEventListener("mouseup", onMouseUp);
        };
        window.addEventListener("mouseup", onMouseUp);

        if (event.button === RIGHT_BUTTON) {
            buttonsPressed.current[1] = true;
            setSelectedClips(new Map());
            const timeBars = round(event.clientX);
            const pointedClips = getPointedClips(timeBars, rowIdx);
            onDeleteClips({
                clipIds: pointedClips.map((c) => c.id),
            });
        } else if (event.button === LEFT_BUTTON) {
            buttonsPressed.current[0] = true;
            const timeBars = round(event.clientX);
            const timeBarsRaw = toBars(event.clientX);
            const pointedClips = getPointedClips(timeBars, rowIdx);

            if (pointedClips.length === 1) {
                onTouchClip({ clipId: pointedClips.map((c) => c.id)[0]! });
            }

            if (event.shiftKey) {
                // Add to selection and start drag
                setSelectedClips((prev) => {
                    const newMap = new Map(prev);
                    pointedClips.forEach((c) => {
                        if (newMap.has(c.id)) {
                            newMap.delete(c.id);
                        } else {
                            newMap.set(c.id, c);
                        }
                    });
                    setOriginalStartsValues(newMap, timeBars, rowIdx);
                    setSelectedClips(newMap);
                    return newMap;
                });
            } else if (event.ctrlKey) {
                const timeBars = round(event.clientX);
                setFrameStartedAt([true, timeBars, rowIdx]);
                setFrameEnd([timeBars, rowIdx]);
                event.preventDefault();
                event.stopPropagation();
            } else {
                const relativeCoordsX = pointedClips.map((c) =>
                    (timeBarsRaw - c.timelineTimeStartBars) / c.timelineTimeLengthBars,
                );
                setDragMode(
                    relativeCoordsX.some((v) => v < RESIZE_AREA_SIZE)
                        ? "resize-start"
                        : relativeCoordsX.some((v) => v > 1 - RESIZE_AREA_SIZE)
                            ? "resize-end"
                            : "move",
                );
                if (pointedClips.length === 0) {

                    const selectedUsage = selectedClipsRef.current.size === 1
                        ? selectedClipsRef.current.values().toArray()[0]!
                        : undefined;

                    // Create new and start drag
                    const newClip = onAddClip({
                        row: rows[rowIdx]!,
                        timeBars,
                        rowIdx,
                        selectedUsage,
                    });
                    if (!newClip) {
                        return;
                    }
                    const newSelected = new Map([[newClip.id, newClip]]);
                    setOriginalStartsValues(
                        new Set<string>(),
                        timeBars + ROUND_TO,
                        rowIdx,
                        [newClip],
                    );
                    setSelectedClips(newSelected);
                } else if (!pointedClips.every((c) => selectedClipsRef.current.has(c.id))) {
                    // Reset selection and start drag
                    const newSelected = new Map(pointedClips.map((c) => [c.id, c]));
                    setOriginalStartsValues(newSelected, timeBars, rowIdx);
                    setSelectedClips(newSelected);
                } else {
                    // Just start drag
                    setOriginalStartsValues(
                        selectedClipsRef.current,
                        timeBars,
                        rowIdx,
                    );
                    setSelectedClips(selectedClipsRef.current);
                }
            }
        }
    }, [
        setMoveStartedAt, setFrameStartedAt,
        setSelectedClips, round, getPointedClips,
        onDeleteClips, toBars, onTouchClip,
        setOriginalStartsValues, setFrameEnd,
        setDragMode, onAddClip, selectedClipsRef,
        rows,
    ]);

    const buildFrame = useCallback(() => {
        return [
            Math.min(
                frameStartedAtRef.current[1], frameEndRef.current[0],
            ),
            Math.max(
                frameStartedAtRef.current[1], frameEndRef.current[0],
            ) + ROUND_TO,
            Math.min(
                frameStartedAtRef.current[2], frameEndRef.current[1],
            ),
            Math.max(
                frameStartedAtRef.current[2], frameEndRef.current[1],
            ) + 1,
        ] as const;
    }, [frameEndRef, frameStartedAtRef]);

    const onMouseMove = useCallback((event: React.MouseEvent, rowIdx: number) => {
        if (buttonsPressed.current[1]) {
            const timeBars = round(event.clientX);
            const pointedClips = getPointedClips(timeBars, rowIdx);
            onDeleteClips({
                clipIds: pointedClips.map((c) => c.id),
            });
            event.preventDefault();
        } else if (frameStartedAtRef.current[0]) {
            const timeBars = round(event.clientX);
            setFrameEnd([timeBars, rowIdx]);
            const frame = buildFrame();
            const newSelectedIds = new Map<string, CLIP>();
            clips.forEach((c) => {
                const rowIdx = rowIdToIdx.get(c.rowId)!;
                if (
                    c.timelineTimeStartBars + c.timelineTimeLengthBars >= frame[0] &&
                    c.timelineTimeStartBars < frame[1] &&
                    rowIdx >= frame[2] &&
                    rowIdx < frame[3]
                ) {
                    newSelectedIds.set(c.id, c);
                }
            });
            setSelectedClips(newSelectedIds);
        } else if (moveStartedAtRef.current[0]) {
            const timeBars = round(event.clientX);
            const deltaTime = (timeBars - moveStartedAtRef.current[1]);
            const deltaRow = (rowIdx - moveStartedAtRef.current[2]);

            const original = [...selectedClipsRef.current.values()];

            if (dragMode.current === "move") {
                onMoveClips({
                    clips: original,
                    deltaTime,
                    deltaRow,
                });
            } else if (
                dragMode.current === "resize-end" ||
                dragMode.current === "resize-start"
            ) {
                onResizeClips({
                    clips: original,
                    side: dragMode.current === "resize-end"
                        ? "end"
                        : "start",
                    deltaTime,
                });
            }
            event.preventDefault();
        }
    }, [
        frameStartedAtRef, moveStartedAtRef,
        round, getPointedClips, onDeleteClips,
        setFrameEnd, buildFrame, clips, setSelectedClips,
        dragMode, onMoveClips, selectedClipsRef,
        onResizeClips,
        rowIdToIdx,
    ]);

    const clipsComponents = useMemo(() => <React.Fragment>
        {
            rows.map((row, rowIdx) => <div
                style={variables({
                    "--rowColor": getRowColor(row, rowIdx),
                    "--resizeAreaSize": RESIZE_AREA_SIZE,
                })}
                className={clsx(styles.row)}
                onMouseDown={(e) => onMouseDown(e, rowIdx)}
                onMouseMove={(e) => onMouseMove(e, rowIdx)}
                ref={rowIdx === midElementIdx ? refToMiddleElement : undefined}
                key={rowIdx}
            >
                {rowIdxToClips.get(rowIdx)?.map((clipIdx) =>
                    <Clip
                        key={clipIdx.id}
                        onDoubleClickClip={() => onDoubleClickClip({ clipId: clipIdx.id })}
                        isSelected={selectedClips.has(clipIdx.id)}
                        renderClip={renderClip}
                        {...clipIdx}
                    />)}
            </div>)
        }
    </React.Fragment>, [
        midElementIdx,
        onDoubleClickClip,
        onMouseDown,
        onMouseMove,
        rows,
        getRowColor,
        renderClip,
        selectedClips,
        rowIdxToClips,
    ]);

    const [frameFromBars, frameToBars, frameFromRow, frameToRow] = buildFrame();
    const isFramingNow = frameStartedAt[0];
    const frame = useMemo(() => {
        return isFramingNow
            ? <div
                className={styles.frame}
                style={variables({
                    "--frameFromBars": frameFromBars,
                    "--frameToBars": frameToBars,
                    "--frameFromRow": frameFromRow,
                    "--frameToRow": frameToRow,
                })}
            />
            : null;
    }, [
        frameFromBars,
        frameToBars,
        frameFromRow,
        frameToRow,
        isFramingNow,
    ]);

    return <>
        {clipsComponents}
        {frame}
    </>;
}
