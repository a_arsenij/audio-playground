import React, { HTMLProps, RefObject } from "react";

import styles from "./Grid.module.css";

type GridProps = HTMLProps<HTMLDivElement> & {
    gridRef: RefObject<HTMLDivElement> | undefined,
    children: React.ReactNode,
    isCssHeightParentDefined?: boolean,
}

export function Grid(props: GridProps) {
    const {
        gridRef,
        children,
        isCssHeightParentDefined = false,
        ...other
    } = props;
    return <div
        className={[styles.grid, isCssHeightParentDefined ? styles.parentDefined : ""].join(" ")}
        ref={gridRef}
        {...other}
    >
        {children}
    </div>;
}
