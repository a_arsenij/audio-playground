import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React from "react";

import { TimelineRow } from "../TimelineWithClips";
import styles from "./RowsTitles.module.css";

type RowsTitlesProps<
    ROW_ID,
    ROW extends TimelineRow<ROW_ID>,
> = {
    rows: ROW[],
    getRowColor: (row: ROW, rowNumber: number) => string,
    renderRowTitle: (row: ROW, rowIdx: number) => React.ReactNode,
}

export function RowsTitlesImpl<
    ROW_ID,
    ROW extends TimelineRow<ROW_ID>,
>({
    rows,
    getRowColor,
    renderRowTitle,
}: RowsTitlesProps<ROW_ID, ROW>) {
    return <div className={styles.rowTitles}>
        {
            rows.map((row, idx) =>
                <div
                    style={variables({
                        "--rowColor": getRowColor(row, idx),
                    })}
                    className={clsx(styles.rowTitle)}
                    key={idx}
                >
                    {renderRowTitle(row, idx)}
                </div>,
            )
        }
    </div>;
}
