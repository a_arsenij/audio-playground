import React from "react";

import styles from "./Carret.module.css";

type CarretProps = {
    char: string,
}

export function Carret({
    char,
}: CarretProps) {
    return <svg
        version="1.1"
        viewBox="0 0 1 1.5"
        className={styles.carret}
    >
        <path
            className={styles.path}
            vectorEffect="non-scaling-stroke"
            d="M 0 0 l 1 0 l 0 1 l -0.5 0.5 l -0.5 -0.5 l 0 -1"
        />
        <text x="0.5" y="0.6" className={styles.text}>{char}</text>
    </svg>;
}
