import React, { useEffect, useRef, useState } from "react";

import styles from "./ContextMenu.module.css";

type ContextMenuProps<PROPS> = {
    rightMouseClickTargetRef: React.RefObject<HTMLElement>,
    ChildComponent: React.FC<PROPS & {
        closeContextMenu: () => void,
    }>,
    props: PROPS,
}

export function ContextMenu<PROPS>(props: ContextMenuProps<PROPS>) {

    const ref = useRef<HTMLDivElement>(null);

    const [opened, setOpened] = useState<
        [number, number] | undefined
    >(undefined);

    useEffect(() => {
        const rightMouseClickTarget = props.rightMouseClickTargetRef.current;
        const handle = (e: MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            const rect = rightMouseClickTarget!.getBoundingClientRect();
            setOpened(
                [rect.x + rect.width, rect.y],
            );
        };
        rightMouseClickTarget?.addEventListener("contextmenu", handle);
        return () => {
            rightMouseClickTarget?.removeEventListener("contextmenu", handle);
        };
    }, [props.rightMouseClickTargetRef]);

    useEffect(() => {
        if (!opened) {
            return;
        }
        const me = ref.current;
        const handleMousePress = (e: MouseEvent) => {
            let t = e.target;
            while (t !== null) {
                if (t === me) {
                    return;
                }
                // @ts-ignore
                t = t.parentElement;
            }
            setOpened(undefined);
        };
        const handleKeyPress = (e: KeyboardEvent) => {
            if (e.code === "Escape") {
                e.preventDefault();
                e.stopPropagation();
                setOpened(undefined);
            }
        };
        window.addEventListener("mousedown", handleMousePress);
        window.addEventListener("keydown", handleKeyPress);
        return () => {
            window.removeEventListener("mousedown", handleMousePress);
            window.removeEventListener("keydown", handleKeyPress);
        };
    }, [opened]);

    if (!opened) {
        return null;
    }

    return <div
        ref={ref}
        style={{ left: opened[0], top: opened[1] }}
        className={styles.contextMenu}
    >
        <props.ChildComponent
            closeContextMenu={() => {
                setOpened(undefined);
            }}
            {...props.props}
        />
    </div>;

}
