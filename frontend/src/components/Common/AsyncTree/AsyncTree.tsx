import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React, { useCallback, useEffect, useState } from "react";

import { Svg18FolderClose } from "../GeneratedIcons/18/Folder/Close";
import { Svg18FolderOpen } from "../GeneratedIcons/18/Folder/Open";
import styles from "./AsyncTree.module.css";

type Loadable<T> =
    | { state: "not yet" }
    | { state: "loading" }
    | { state: "ready"; value: T }
    | { state: "failed" }
;

export type AsyncTreeLoader<FILE_META> = (path: string[]) => Promise<{
    folders: string[],
    files: Record<string, FILE_META>,
}>;

type AsyncTreeProps<FILE_META> = {
    load: AsyncTreeLoader<FILE_META>,
    renderFile: (path: string[], meta: FILE_META) => React.ReactNode,
}

type AsyncFolderContent<FILE_META> = Loadable<{
    folders: Record<string, AsyncFolderState<FILE_META>>,
    files: Record<string, FILE_META>,
}>;

type AsyncFolderState<FILE_META> = AsyncFolderContent<FILE_META> & {
    open: boolean,
}

type AsyncTreeState<FILE_META> = AsyncFolderState<FILE_META>;


export function AsyncTree<FILE_META>({
    load,
    renderFile,
}: AsyncTreeProps<FILE_META>) {

    const [
        state,
        setState,
    ] = useState<AsyncTreeState<FILE_META>>({ state: "not yet", open: true });

    useEffect(() => {
        if (state.state === "not yet") {
            const content = load([]);
            setState((prev) => ({
                ...prev,
                state: "loading",
            }));
            content
                .then((loaded) => {
                    setState((prev) => ({
                        ...prev,
                        state: "ready",
                        value: {
                            files: loaded.files,
                            folders: Object.fromEntries(loaded.folders.map((v) => [v, {
                                open: false,
                                state: "not yet",
                            }])),
                        },
                    }));
                })
                .catch(() => {
                    setState((prev) => ({
                        ...prev,
                        state: "failed",
                    }));
                });
        }
    }, [state.state, load]);

    const applyChange = useCallback((
        path: string[],
        apply: (el: AsyncFolderState<FILE_META>) => AsyncFolderState<FILE_META>,
    ) => {
        setState((prev: AsyncFolderState<FILE_META>) => {
            function go(obj: AsyncFolderState<FILE_META>, index: number) {
                if (index === path.length) {
                    return apply(obj);
                }
                if (obj.state !== "ready") {
                    throw new Error("??");
                }
                const sub = go(
                    obj.value.folders[path[index]!]!,
                    index + 1,
                );
                const r = { ...obj };
                obj.value.folders[path[index]!] = sub;
                return r;
            }
            return go(prev, 0);
        });
    }, []);

    const onOpen = useCallback((path: string[]) => {
        return applyChange(path, (e) => {
            const res = {
                ...e,
                open: true,
            };
            if (res.state === "not yet") {
                setTimeout(() => {
                    load(path)
                        .then((res) => {
                            applyChange(path, (e) => ({
                                ...e,
                                state: "ready",
                                value: {
                                    files: res.files,
                                    folders: Object.fromEntries(res.folders.map((name) => ([name, {
                                        open: false,
                                        state: "not yet",
                                    }]))),
                                },
                            }));
                        })
                        .catch(() => {
                            applyChange(path, (e) => ({
                                ...e,
                                state: "failed",
                            }));
                        });
                });
                return {
                    ...res,
                    state: "loading",
                };
            }
            return res;
        });
    }, [applyChange, load]);

    const onClose = useCallback((path: string[]) => {
        return applyChange(path, (e) => {
            if (e.state !== "ready") {
                return e;
            }
            return {
                ...e,
                open: false,
            };
        });
    }, [applyChange]);

    return <div className={styles.root}>
        <AsyncTreeDumb
            path={[]}
            state={state}
            renderFile={renderFile}
            onOpen={onOpen}
            onClose={onClose}
        />
    </div>;
}

type AsyncTreeDumbProps<FILE_META> = {
    path: string[],
    state: AsyncFolderContent<FILE_META>,
    renderFile: (path: string[], meta: FILE_META) => React.ReactNode,
    onOpen: (path: string[]) => void,
    onClose: (path: string[]) => void,
}

function AsyncTreeDumb<FILE_META>({
    path,
    state,
    renderFile,
    onOpen,
    onClose,
}: AsyncTreeDumbProps<FILE_META>) {
    if (state.state === "failed") {
        return "Failed";
    }
    if (state.state !== "ready") {
        return "Loading...";
    }
    return <div className={styles.asyncTree}>
        {Object.entries(state.value.folders).map(([k, v]) =>
            <AsyncTreeFolder
                key={k}
                path={[...path, k]}
                state={v}
                renderFile={renderFile}
                onOpen={onOpen}
                onClose={onClose}
            />,
        )}
        {Object.entries(state.value.files).map(([k, v]) =>
            <AsyncTreeFile key={k}>
                {renderFile([...path, k], v)}
            </AsyncTreeFile>,
        )}
    </div>;
}

type AsyncTreeFolderProps<FILE_META> = {
    path: string[],
    state: AsyncFolderState<FILE_META>,
    renderFile: (path: string[], meta: FILE_META) => React.ReactNode,
    onOpen: (path: string[]) => void,
    onClose: (path: string[]) => void,
}

function AsyncTreeFolder<FILE_META>({
    path,
    state,
    renderFile,
    onOpen,
    onClose,
}: AsyncTreeFolderProps<FILE_META>) {
    return <>
        <button
            className={clsx(styles.item, styles.folder)}
            onClick={() => {
                (state.open ? onClose : onOpen)(path);
            }}
        >
            <div className={styles.folderIcon}>
                {state.open ? <Svg18FolderClose/> : <Svg18FolderOpen/>}
            </div>
            <div className={styles.folderTitle}>
                {path.at(-1)}
            </div>
        </button>
        {state.open && <AsyncTreeDumb
            path={path}
            state={state}
            renderFile={renderFile}
            onOpen={onOpen}
            onClose={onClose}
        />}
    </>;
}

type AsyncTreeFileProps = {
    children: React.ReactNode,
}

function AsyncTreeFile({
    children,
}: AsyncTreeFileProps) {
    return <div className={clsx(styles.item, styles.file)}>
        {children}
    </div>;
}
