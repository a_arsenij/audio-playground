import { useActiveWheelEvent } from "@audio-playground/lib-frontend/hooks/useActiveWheelEvent";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import React, { RefObject, useCallback, useEffect, useRef } from "react";

import { LoopBorders } from "../../../types/Loop";
import { BarTitles } from "../../App/TimelineWithClips/BarTitles/BarTitles";
import { Grid } from "../../App/TimelineWithClips/Grid/Grid";
import styles from "./Timeline.module.css";

type TimelineProps = {
    loopBorders?: LoopBorders | undefined,
    setLoopBorders?: ((newLoopBorders: LoopBorders) => void) | undefined,
    currentTimeBars?: number | undefined,
    timelineRef?: RefObject<HTMLDivElement> | undefined,
    gridRef?: RefObject<HTMLDivElement> | undefined,
    rowsTitles?: React.ReactNode,
    rowsElems: React.ReactNode,
    contentWidthBars: number,

    rowTitlesWidth: number,
    rowHeight: number,

    pxPerBar: number,
    setPxPerBar: SetState<number>,

    isCssHeightParentDefined?: boolean,

}

export function Timeline({
    loopBorders = undefined,
    setLoopBorders = undefined,
    currentTimeBars = undefined,
    timelineRef = undefined,
    gridRef = undefined,
    rowsTitles = null,
    isCssHeightParentDefined,
    rowsElems,
    contentWidthBars,

    rowTitlesWidth,
    rowHeight,

    pxPerBar,
    setPxPerBar,
}: TimelineProps) {

    const pxPerGrid = (() => {
        let res = pxPerBar;
        while (res > 128) {
            res /= 4;
        }
        return res;
    })();

    const onZoom = useCallback((event: WheelEvent) => {
        if (event.ctrlKey) {
            event.preventDefault();
            if (event.deltaY > 0) {
                setPxPerBar((prev) => Math.max(8, Math.round(prev / 1.2)));
            } else if (event.deltaY < 0) {
                setPxPerBar((prev) => prev * 1.2);
            }
            return false;
        }
    }, [setPxPerBar]);

    const newTimelineRef = useRef<HTMLDivElement>(null);
    const usedTimelineRef = timelineRef || newTimelineRef;

    useActiveWheelEvent<HTMLDivElement>(onZoom, usedTimelineRef);

    useEffect(() => {
        function handleContextMenu(e: MouseEvent) {
            e.preventDefault();
        }
        const el = usedTimelineRef.current!;
        el.addEventListener("contextmenu", handleContextMenu);
        return () => {
            el.removeEventListener("contextmenu", handleContextMenu);
        };
    }, [usedTimelineRef]);

    return <div
        ref={usedTimelineRef}
        style={variables({
            "--pxPerGrid": pxPerGrid,
            "--pxPerBar": pxPerBar,
            "--contentWidthBar": contentWidthBars,
            "--rowTitlesWidth": rowTitlesWidth,
            "--rowHeight": rowHeight,
        })}
        className={styles.timeline}
    >

        <div className={styles.barTitles}>
            <BarTitles
                contentWidthBar={contentWidthBars}
                loopBorders={loopBorders}
                setLoopBorders={setLoopBorders}
                currentTimeBars={currentTimeBars}
            />
        </div>

        {rowsTitles}

        <Grid
            isCssHeightParentDefined={isCssHeightParentDefined}
            gridRef={gridRef}
        >
            {rowsElems}
        </Grid>

    </div>;

}
