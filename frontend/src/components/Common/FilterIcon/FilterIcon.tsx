import { FilterType } from "@audio-playground/lib-sound/FilterType";

import { Svg18FilterBandPass } from "../GeneratedIcons/18/Filter/BandPass";
import { Svg18FilterBell } from "../GeneratedIcons/18/Filter/Bell";
import { Svg18FilterHighPass } from "../GeneratedIcons/18/Filter/HighPass";
import { Svg18FilterHighShelf } from "../GeneratedIcons/18/Filter/HighShelf";
import { Svg18FilterLowPass } from "../GeneratedIcons/18/Filter/LowPass";
import { Svg18FilterLowShelf } from "../GeneratedIcons/18/Filter/LowShelf";


export function filterTitle(type: FilterType) {
    switch (type) {
        case "lowpass":
            return "LowPass";
        case "bandpass":
            return "BandPass";
        case "lowshelf":
            return "LowShelf";
        case "highshelf":
            return "HighShelf";
        case "highpass":
            return "HighPass";
        case "bell":
            return "Bell";
    }
}

export function filterIcon(type: FilterType) {
    switch (type) {
        case "lowpass":
            return Svg18FilterLowPass;
        case "bandpass":
            return Svg18FilterBandPass;
        case "lowshelf":
            return Svg18FilterLowShelf;
        case "highshelf":
            return Svg18FilterHighShelf;
        case "highpass":
            return Svg18FilterHighPass;
        case "bell":
            return Svg18FilterBell;
    }
}
