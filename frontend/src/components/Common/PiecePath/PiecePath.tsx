import { Point } from "@audio-playground/lib-common/math/Point";
import { useActiveWheelEvent } from "@audio-playground/lib-frontend/hooks/useActiveWheelEvent";
import React, { useCallback } from "react";

import { FUNCTION_TYPES_MAP, SvgFunctionTypeId } from "../../../utils/SvgGraphFunctions";
import styles from "./PiecePath.module.css";

const WHEEL_SENSITIVITY = 0.0003;

export type Piece = {
    from: Point,
    to: Point,
    fun: SvgFunctionTypeId,
    params: number[],
    onClick?: () => void,
    onWheel?: (diff: number) => void,
    hoverable?: boolean,
}

export type PiecePathProps = Piece;

export function PiecePath(props: PiecePathProps) {

    const {
        from: a,
        to: b,
        fun,
        params,
        onClick,
        onWheel,
        hoverable,
    } = props;

    const f = FUNCTION_TYPES_MAP[fun];

    const isDegenerate = a.x === b.x || a.y === b.y;

    const transformX = isDegenerate ? "" : `translate( ${a.x}, 0 ) scale( ${b.x - a.x}, 1)`;
    const transformY = isDegenerate ? "" : `translate( 0, ${a.y} ) scale(1, ${b.y - a.y})`;

    // @ts-ignore
    const path = isDegenerate ? `M ${a.x} ${a.y} L ${b.x} ${b.y}` : f.path(...params);

    const r = isDegenerate
        ? {
            x: a.x, y: 0, width: b.x - a.x, height: 1,
        }
        : {
            x: 0, y: 0, width: 1, height: 1,
        };

    const onWheelHandler = useCallback((e: WheelEvent) => {
        if (!onWheel) {
            return;
        }
        e.preventDefault();
        const diff = e.deltaY * WHEEL_SENSITIVITY;
        onWheel(diff);
    }, [onWheel]);

    const ref = useActiveWheelEvent<SVGGElement>(onWheelHandler);

    return <g
        ref={ref}
        transform={transformX}
        className={hoverable ? styles.pathG : styles.pathGStatic}
        onClick={onClick}
    >
        <rect {...r} fill={"none"} stroke={"none"}/>
        <path
            transform={transformY}
            strokeLinejoin="round"
            vectorEffect="non-scaling-stroke"
            d={path}
        />
    </g>
    ;
}
