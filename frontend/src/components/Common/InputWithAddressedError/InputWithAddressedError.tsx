import React, { useEffect, useMemo, useState } from "react";

import { HighlightedInput, HighlightedSpan } from "../Input/Input";
import { Size } from "../Size";
import styles from "./InputWithAddressedError.module.css";

type InputWithAddressedErrorProps = {
    value: string,
    onChange: (newValue: string) => void,
    validate: (text: string) => undefined | { first: number; last: number; message: string },
    size?: Size,
}

export function InputWithAddressedError({
    value,
    onChange,
    validate,
    size,
}: InputWithAddressedErrorProps) {

    const [
        text,
        setText,
    ] = useState(value);

    useEffect(() => {
        setText(value);
    }, [value]);

    type T = [HighlightedSpan[], string | undefined];
    const [formatted, message] = useMemo<T>(
        () => {
            const validated = validate(text);
            if (validated === undefined) {
                if (value !== text) {
                    onChange(text);
                }
                return [text.length > 0 ? [{ text }] : [], undefined] as T;
            }
            return [[
                {
                    text: text.substring(0, validated.first),
                },
                {
                    text: text.substring(validated.first, validated.last),
                    color: "red",
                },
                {
                    text: text.substring(validated.last),
                },
            ].filter((t) => t.text.length > 0), validated.message] as T;
        },
        [onChange, value, text, validate],
    );

    return <div className={styles.inputWithAddressedError}>
        <HighlightedInput
            value={formatted}
            onChange={setText}
            size={size}
        />
        {message && <div className={styles.errorMessage}>
            {message}
        </div>}
    </div>;
}
