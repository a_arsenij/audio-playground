import React from "react";

import { PiecewiseFunctionData } from "../../../utils/SvgGraphFunctions";
import { PiecePath } from "../PiecePath/PiecePath";
import styles from "./PiecewiseGraph.module.css";

const BBLeft = 0;
const BBRight = 1;
const BBBottom = 0;
const BBTop = 1;

export function PiecewiseGraph(props: PiecewiseFunctionData) {
    const { points, pieceData } = props;

    const pieceElems = pieceData.map((f, i) => {
        return <PiecePath
            {...f}
            key={i}
            from={points[i]!}
            to={ points[i + 1]!}
        />;
    });

    return <svg
        xmlns={"http://www.w3.org/2000/svg"}
        className={styles.svg}
        viewBox={`${BBLeft} ${BBBottom} ${BBRight - BBLeft} ${BBTop - BBBottom}`}
        preserveAspectRatio={"none"}
    >
        {pieceElems}
    </svg>;

}
