import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React from "react";

import { Button } from "../Button/Button";
import { Size } from "../Size";
import styles from "./Switch.module.css";

type DeprecatedSwitchProps<OPTION> = {
    options: readonly OPTION[],
    value: OPTION,
    setValue: (v: OPTION) => void,
    renderOption: (option: OPTION, isSelected: boolean) => React.ReactNode,
    size?: Size,
}

export function DeprecatedSwitch<OPTION>({
    options,
    value,
    setValue,
    renderOption,
    size = "small",
}: DeprecatedSwitchProps<OPTION>) {
    return <div
        className={styles.switch}
    >
        {
            options.map((option, idx) =>
                <button
                    key={idx}
                    className={clsx(
                        styles.option,
                        option === value && styles.selected,
                        size === "big" && styles.big,
                        size === "medium" && styles.big,
                        size === "small" && styles.small,
                    )}
                    onClick={() => setValue(option)}
                >
                    {renderOption(option, option === value)}
                </button>,
            )
        }
    </div>;
}

type SwitchProps<OPTION> = {
    options: readonly {
        value: OPTION,
        title: string,
        icon?: React.FC<{}>,
    }[],
    value: OPTION,
    setValue: (v: OPTION) => void,
}

export function Switch<OPTION>({
    options,
    value,
    setValue,
}: SwitchProps<OPTION>) {
    return options.map((option, idx) => {
        return <Button
            key={idx}
            style={option.value === value ? "primary" : "secondary"}
            onClick={() => setValue(option.value)}
            title={option.title}
            iconLeft={option.icon}
        />;
    },
    );
}
