import { FILTER_TYPES, FilterType } from "@audio-playground/lib-sound/FilterType";
import React from "react";

import { filterIcon, filterTitle } from "../FilterIcon/FilterIcon";
import { Switch } from "../Switch/Switch";

type FilterSwitchProps = {
    type: FilterType,
    setType: (type: FilterType) => void,
}

export function FilterSwitch({
    type,
    setType,
}: FilterSwitchProps) {
    return <Switch
        options={FILTER_TYPES.map((v) => ({
            value: v,
            icon: filterIcon(v),
            title: filterTitle(v),
        }))}
        value={type}
        setValue={setType}
    />;
}
