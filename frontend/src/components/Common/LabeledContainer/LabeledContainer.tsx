import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React from "react";

import { Size } from "../Size";
import styles from "./LabeledContainer.module.css";

type LabeledContainerProps = {
    label: string,
    children: React.ReactNode,
    size?: Size,
}

export function LabeledContainer({
    label,
    children,
    size = "big",
}: LabeledContainerProps) {
    return <div
        className={styles.labeledContainer}
    >
        <div className={clsx(
            styles.box,
            size === "tiny" && styles.tiny,
            size === "small" && styles.small,
            size === "medium" && styles.medium,
            size === "big" && styles.big,
        )}>
            <div className={styles.label}>{label}</div>
            <div className={styles.content}>{children}</div>
        </div>
    </div>;
}
