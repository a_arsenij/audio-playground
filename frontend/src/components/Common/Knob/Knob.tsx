import { clamp } from "@audio-playground/lib-common/math/Interval";
import {
    exponentialToIdentitySegment,
    identitySegmentToExponential,
    identitySegmentToLinear,
    linearToIdentitySegment,
    stepify,
} from "@audio-playground/lib-common/math/Ranges";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React, { MouseEvent as ReactMouseEvent, useCallback, useMemo, useRef, useState } from "react";

import { ContextMenu } from "../ContextMenu/ContextMenu";
import { DropdownItem } from "../DropdownItem/DropdownItem";
import { NumberInput } from "../Input/Input";
import styles from "./Knob.module.css";

type LinearKnobProps = {
    func: "linear",
    step?: number,
}

type ExpKnobProps = {
    func: "exponent",
    base: number,
}

type CustomKnobProps = {
    func: "custom",
    identitySegmentToValue: (value: number) => number,
    valueToIdentitySegment: (value: number) => number,
}

type KnobProps = (LinearKnobProps | ExpKnobProps | CustomKnobProps) & {

    label?: string,

    value: number,
    setValue: (v: number) => void,

    neutralValue?: number,
    defaultValue?: number,

    minValue?: number,
    maxValue?: number,

    showNumberValue?: boolean,

    extraContextMenuChildren?: React.ReactNode,

    renderValue?: (v: number) => React.ReactNode,

}

export function defaultRenderValue(value: number): React.ReactNode {
    const abs = Math.abs(value);
    if (value === 0) {
        return "0";
    } else if (abs > 1000 || abs < 0.001) {
        const l = (
            abs > 1000
                ? Math.floor
                : Math.ceil
        )(
            Math.log10(abs),
        );
        const mantiss = Math.round(abs * 10 / (10 ** l));
        return <>{value < 0 ? "-" : ""}{mantiss / 10}×10<span className={styles.pow}>{l}</span></>;
    } else {
        const pow = abs > 10 ? 10 : 100;
        return Math.round((value * pow)) / pow;
    }
}

export function Knob(props: KnobProps) {

    const {
        value,
        setValue,
        defaultValue,
        neutralValue,
        extraContextMenuChildren,
        label,
        showNumberValue = false,
        minValue = 0,
        maxValue = 1,
        renderValue,
    } = props;

    const step = "step" in props ? props.step : 0;
    const base = "base" in props ? props.base : 2;

    const propsToIdentity = useCallback((value: number) => {
        return (() => {
            switch (props.func) {
                case "linear":
                    return linearToIdentitySegment(
                        value,
                        minValue,
                        maxValue,
                    );
                case "exponent":
                    return exponentialToIdentitySegment(
                        value,
                        minValue,
                        maxValue,
                        base,
                    );
                case "custom":
                    return props.valueToIdentitySegment(value);
            }
        })();
    }, [props, minValue, maxValue, base]);

    const identityToProps = useCallback((value: number) => {
        return (() => {
            switch (props.func) {
                case "linear":
                    return identitySegmentToLinear(
                        value,
                        minValue,
                        maxValue,
                        step,
                    );
                case "exponent":
                    return identitySegmentToExponential(
                        value,
                        minValue,
                        maxValue,
                        base,
                    );
                case "custom":
                    return props.identitySegmentToValue(value);
            }
        })();
    }, [props, minValue, maxValue, step, base]);

    const _01Value = propsToIdentity(value);
    const _01Neutral = neutralValue === undefined ? undefined : propsToIdentity(neutralValue);

    const set01Value = useCallback(
        (value: number) => {
            const i2p = identityToProps(value);
            const stepified = stepify(i2p, step);
            const clamped = clamp(
                minValue,
                stepified,
                maxValue,
            );
            setValue(clamped);
        },
        [identityToProps, maxValue, minValue, setValue, step],
    );

    const handleMouseDown = (downEvent: ReactMouseEvent<HTMLDivElement, MouseEvent>) => {
        downEvent.preventDefault();
        const handleMouseMove = (moveEvent: MouseEvent) => {
            moveEvent.preventDefault();
            const dy = (moveEvent.clientY - downEvent.clientY);
            const v = Math.max(0, Math.min(1,
                _01Value - dy / 100,
            ));
            set01Value(v);
        };
        window.addEventListener("mousemove", handleMouseMove);
        const handleMouseUp = () => {
            window.removeEventListener("mousemove", handleMouseMove);
            window.removeEventListener("mouseup", handleMouseUp);
        };
        window.addEventListener("mouseup", handleMouseUp);
    };

    const rmbRef = useRef<HTMLDivElement>(null);

    const displayedValue = useMemo(() => {
        if (!showNumberValue) {
            return "";
        }
        if (renderValue) {
            return renderValue(value);
        }
        return defaultRenderValue(value);
    }, [renderValue, showNumberValue, value]);

    return <>
        <ContextMenu
            rightMouseClickTargetRef={rmbRef}
            ChildComponent={KnobContextMenu}
            props={{
                defaultValue,
                value,
                setValue: (v: number) => {
                    setValue(clamp(
                        minValue,
                        v,
                        maxValue,
                    ));
                },
                extraContextMenuChildren,
            }}
        />
        <div
            className={styles.wrapper}
            ref={rmbRef}
            onMouseDown={handleMouseDown}
        >
            {label && <div className={styles.label}>{label}</div> }
            <div
                className={styles.knob}
                style={variables({
                    "--knob-value": _01Value,
                    "--knob-neutral-min-value": Math.min(_01Value, (_01Neutral || 0)),
                    "--knob-neutral-abs-value": Math.abs(_01Value - (_01Neutral || 0)),
                })}
            >
                <svg
                    className={styles.rangeArc}
                    viewBox={"0 0 2 2"}
                >
                    <circle
                        vectorEffect="non-scaling-stroke"
                        className={styles.rangeArcCircle}
                        cx="1"
                        cy="1"
                        r="1"
                    />
                    { _01Neutral !== undefined && <circle
                        vectorEffect="non-scaling-stroke"
                        className={styles.valueArcCircle}
                        cx="1"
                        cy="1"
                        r="1"
                    /> }
                </svg>
                <div className={styles.barWrapper}>
                    <div className={styles.bar}/>
                </div>
            </div>
            {showNumberValue && <div className={styles.value}>
                {displayedValue}
            </div>}
        </div>
    </>;
}

type KnobContextMenuProps = {
    closeContextMenu: () => void,
    value: number,
    defaultValue: number | undefined,
    setValue: (v: number) => void,
    extraContextMenuChildren?: React.ReactNode,
}

function KnobContextMenu(
    props: KnobContextMenuProps,
) {
    const [mode, setMode] = useState<"initial" | "setValue">("initial");
    if (mode === "setValue") {
        return <NumberInput
            value={props.value}
            onChange={props.setValue}
            onEnter={() => props.closeContextMenu()}
        />;
    }
    if (mode === "initial") {
        return <>
            <DropdownItem
                title={"Set value"}
                onClick={() => setMode("setValue")}
            />
            {props.defaultValue !== undefined && <DropdownItem
                title={"Reset"}
                onClick={() => {
                    props.setValue(props.defaultValue!);
                    props.closeContextMenu();
                }}
            />}
            {props.extraContextMenuChildren}
        </>;
    }
}
