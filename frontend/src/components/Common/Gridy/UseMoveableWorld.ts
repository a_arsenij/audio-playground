import { useActiveWheelEvent } from "@audio-playground/lib-frontend/hooks/useActiveWheelEvent";
import { LEFT_BUTTON } from "@audio-playground/lib-frontend/std/MouseButtons";
import { Ref, useCallback, useEffect, useRef, useState } from "react";

import { calcZoom, GRID_CELL_PX } from "./Grid/Grid";

export type MoveableWorld = {
    gridRef: Ref<HTMLDivElement>,
    offsetX: number,
    offsetY: number,
    zoomBase: number,
    zoom: number,
    isDraggingWorldNow: boolean,
    zoomIn: () => void,
    zoomOut: () => void,
    resetPosition: () => void,
}

type Pos = {
    offsetX: number,
    offsetY: number,
    zoomBase: number,
}

export function useMoveableWorld(
    onStartMoveWorld: () => void,
): MoveableWorld {

    const gridRef = useRef<HTMLDivElement>(null);

    const [pos, setPos] = useState<Pos>({ offsetX: 0, offsetY: 0, zoomBase: 0 });
    const zoom = calcZoom(pos.zoomBase);
    const [isDraggingWorldNow, setIsDraggingWorldNow] = useState<boolean>(false);

    const moveWorld = useCallback(
        (e: MouseEvent) => {
            if (e.button !== LEFT_BUTTON) {
                return;
            }
            if (e.target !== gridRef.current) {
                return;
            }
            e.stopPropagation();
            setIsDraggingWorldNow(true);
            onStartMoveWorld();
            const ox = pos.offsetX;
            const oy = pos.offsetY;
            const sx = e.clientX;
            const sy = e.clientY;

            function onMove(e: MouseEvent) {
                e.preventDefault();
                e.stopPropagation();
                const dx = (e.clientX - sx) / zoom / GRID_CELL_PX;
                const dy = (e.clientY - sy) / zoom / GRID_CELL_PX;
                const nx = ox + dx;
                const ny = oy + dy;
                setPos((prev) => ({
                    ...prev,
                    offsetX: nx,
                    offsetY: ny,
                }));
            }

            function onUp() {
                setIsDraggingWorldNow(false);
                window.removeEventListener("mousemove", onMove);
                window.removeEventListener("mouseup", onUp);
            }

            window.addEventListener("mousemove", onMove);
            window.addEventListener("mouseup", onUp);
        },
        [pos, onStartMoveWorld, zoom],
    );

    const onZoom = useCallback((e: WheelEvent) => {
        e.preventDefault();
        e.stopPropagation();

        const x = e.clientX;
        const y = e.clientY;

        const cbr = gridRef.current!.getBoundingClientRect();

        setPos((prev) => {
            const prevZoom = calcZoom(prev.zoomBase);
            const newZoomBase = prev.zoomBase + (e.deltaY > 0 ? -1 : 1);
            const newZoom = calcZoom(newZoomBase);

            const scaleFactor = (1 / prevZoom - 1 / newZoom) / GRID_CELL_PX;

            return {
                zoomBase: newZoomBase,
                offsetX: prev.offsetX - scaleFactor * (x - cbr.x),
                offsetY: prev.offsetY - scaleFactor * (y - cbr.y),
            };
        });
    }, []);

    useActiveWheelEvent<HTMLDivElement>(onZoom, gridRef);
    useEffect(() => {
        const el = gridRef.current!;
        el.addEventListener("mousedown", moveWorld);
        return () => {
            el.removeEventListener("mousedown", moveWorld);
        };
    }, [moveWorld]);

    const zoomIn = useCallback(() => {
        setPos((pos) => ({
            ...pos,
            zoomBase: pos.zoomBase + 1,
        }));
    }, []);

    const zoomOut = useCallback(() => {
        setPos((pos) => ({
            ...pos,
            zoomBase: pos.zoomBase - 1,
        }));
    }, []);

    const resetPosition = useCallback(() => {
        setPos({
            offsetX: 0,
            offsetY: 0,
            zoomBase: 0,
        });
    }, []);

    return {
        gridRef,
        offsetX: pos.offsetX,
        offsetY: pos.offsetY,
        zoomBase: pos.zoomBase,
        zoom,
        isDraggingWorldNow,
        zoomIn,
        zoomOut,
        resetPosition,
    };

}
