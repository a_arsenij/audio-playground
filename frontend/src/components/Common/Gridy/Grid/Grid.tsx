import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React, { Ref } from "react";

import styles from "./Grid.module.css";

type GridProps = {
    offsetX: number,
    offsetY: number,
    zoomBase: number,
    gridRef: Ref<HTMLDivElement>,
    children?: React.ReactNode,
}

export function Grid({
    offsetX,
    offsetY,
    zoomBase,
    gridRef,
    children,
}: GridProps) {
    return <div
        className={styles.grid}
        style={variables({
            "--zoom": calcZoom(zoomBase),
            "--offsetX": offsetX,
            "--offsetY": offsetY,
            "--gridSize": GRID_CELL_PX,
        })}
        ref={gridRef}
    >
        {children}
    </div>;
}


export function calcZoom(base: number) {
    return 2 ** (0.1 * base);
}

export const GRID_CELL_PX = 32;
