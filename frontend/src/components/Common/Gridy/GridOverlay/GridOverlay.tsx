import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React from "react";

import styles from "./GridOverlay.module.css";

type GridOverlayProps = {
    side: "left" | "right",
    children?: React.ReactNode,
}

export function GridOverlay(props: GridOverlayProps) {
    return <div className={clsx(
        styles.gridOverlay,
        props.side === "left" && styles.left,
        props.side === "right" && styles.right,
    )}>
        {props.children}
    </div>;
}
