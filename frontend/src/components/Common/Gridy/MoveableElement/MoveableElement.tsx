import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React from "react";

import { Svg18ToolDrag } from "../../GeneratedIcons/18/Tool/Drag";
import styles from "./MoveableElement.module.css";

type MoveableElementProps = {
    x: number,
    y: number,
    width: number,
    height: number,
    title: string,
    isSelectedNow?: boolean,
    isDraggedNow: boolean,
    onMouseDown: (e: React.MouseEvent) => void,
    children: React.ReactNode,
    buttons: (false | {
        onClick: () => void,
        title: string,
        icon: React.ReactNode,
    })[],
}

export function MoveableElement({
    x,
    y,
    width,
    height,
    title,
    isSelectedNow,
    isDraggedNow,
    onMouseDown,
    children,
    buttons,
}: MoveableElementProps) {
    return <div
        className={clsx(
            styles.node,
            isSelectedNow && styles.selectedNode,
            isDraggedNow && styles.draggedNode,
        )}
        style={variables({
            "--x": x,
            "--y": y,
            "--width": width,
            "--height": height,
        })}
    >
        <div className={styles.titleText}>
            {title}
        </div>
        <div
            className={styles.nodeTitleBar}
        >
            <div
                className={styles.nodeTitle}
                onMouseDown={onMouseDown}
            >
                <div
                    className={styles.dragIcon}
                >
                    <Svg18ToolDrag fillContainer={true}/>
                </div>

            </div>
            {
                buttons.map((b) =>
                    b && <button
                        key={b.title}
                        onClick={b.onClick}
                        className={styles.nodeTitleButton}
                        title={b.title}
                    >
                        {b.icon}
                    </button>,
                )
            }
        </div>
        <div className={styles.nodeContent}>
            {children}
        </div>
    </div>;
}
