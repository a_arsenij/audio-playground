import { SetState, useSetById } from "@audio-playground/lib-frontend/std/SetState";
import React, { SetStateAction, useCallback, useState } from "react";

import { Property } from "../../Properties/Properties";
import { SidePanel } from "../../SidePanel/SidePanel";
import { GridOverlay } from "../GridOverlay/GridOverlay";
import { UIEditorGrid } from "./UIEditorGrid/UIEditorGrid";
import { UIEditorProperties } from "./UIEditorProperties/UIEditorProperties";

export type UIEditorElement = {
    id: string,
    x: number,
    y: number,
}


type UIEditorProps<
    ELEMENT extends UIEditorElement,
> = {
    getTitle: (element: ELEMENT) => string,
    getContent: (element: ELEMENT) => React.ReactNode,
    getProperties: (element: ELEMENT) => Property[],
    elements: ELEMENT[],
    setElements: SetState<ELEMENT[]>,
    rightOverlayContent: React.ReactElement,
}

export function UIEditor<
    ELEMENT extends UIEditorElement,
>({
    getTitle,
    getContent,
    getProperties,
    elements,
    setElements,
    rightOverlayContent,
}: UIEditorProps<ELEMENT>) {
    const [selectedId, setSelectedId] = useState<string | undefined>(
        undefined,
    );
    const setById = useSetById(setElements);
    const selectedElement = elements.find((e) => e.id === selectedId);
    const setSelectedElement = useCallback((setter: SetStateAction<ELEMENT>) => {
        if (selectedId === undefined) {
            return;
        }
        setById(selectedId, setter);
    }, [selectedId, setById]);
    return <SidePanel
        mainContent={<>
            <UIEditorGrid
                getTitle={getTitle}
                getContent={getContent}
                elements={elements}
                setElements={setElements}
                setSelectedElementId={setSelectedId}
                selectedId={selectedId}
            />
            <GridOverlay side={"right"}>
                {rightOverlayContent}
            </GridOverlay>
        </>}
        rightPanelContent={<UIEditorProperties
            selectedElement={selectedElement}
            setSelectedElement={setSelectedElement}
            getTitle={getTitle}
            getProperties={getProperties}
        />}
    />;
}
