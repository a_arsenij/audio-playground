import { SetState, useSetById } from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback } from "react";

import { Svg18CommonDelete } from "../../../GeneratedIcons/18/Common/Delete";
import { Grid } from "../../Grid/Grid";
import { MoveableElement } from "../../MoveableElement/MoveableElement";
import { PositioningGridOverlay } from "../../PositioningGridOverlay/PositioningGridOverlay";
import { useMoveableElements } from "../../UseMoveableElements";
import { useMoveableWorld } from "../../UseMoveableWorld";
import { UIEditorElement } from "../UIEditor";
import styles from "./UIEditorGrid.module.css";

type UIEditorGridProps<
    ELEMENT extends UIEditorElement
> = {
    selectedId: string | undefined,
    setSelectedElementId: (id: string | undefined) => void,
    getTitle: (element: ELEMENT) => string,
    getContent: (element: ELEMENT) => React.ReactNode,
    elements: ELEMENT[],
    setElements: SetState<ELEMENT[]>,
}


export function UIEditorGrid<
    ELEMENT extends UIEditorElement
>({
    selectedId,
    setSelectedElementId,
    getTitle,
    getContent,
    elements,
    setElements,
}: UIEditorGridProps<ELEMENT>) {

    const resetSelection = useCallback(() => {
        setSelectedElementId(undefined);
    }, [setSelectedElementId]);

    const {
        offsetX,
        offsetY,
        zoomBase,
        zoom,
        // isDraggingWorldNow,
        gridRef,
        resetPosition,
        zoomIn,
        zoomOut,
    } = useMoveableWorld(resetSelection);

    const setElementById = useSetById(setElements);

    const {
        draggedElementId,
        moveElement,
    } = useMoveableElements({
        zoom,
        elements,
        setElementById,
        setSelectedElementId,
    });

    const deleteNote = (elementId: string) => {
        setElements((prev) => prev.filter((e) => e.id !== elementId));
    };

    return <div className={styles.uIEditorGrid}>
        <Grid
            offsetX={offsetX}
            offsetY={offsetY}
            zoomBase={zoomBase}
            gridRef={gridRef}
        >
            {elements.map((element) => {
                return <MoveableElement
                    key={element.id}
                    x={element.x}
                    y={element.y}
                    title={getTitle(element)}
                    width={4}
                    height={3}
                    isSelectedNow={selectedId === element.id}
                    isDraggedNow={draggedElementId === element.id}
                    onMouseDown={(e) => moveElement(e, element.id)}
                    buttons={[
                        {
                            onClick: () => deleteNote(element.id),
                            title: "Delete element",
                            icon: <Svg18CommonDelete/>,
                        },
                    ]}
                >
                    {getContent(element)}
                </MoveableElement>;
            })}
        </Grid>
        <PositioningGridOverlay
            zoomIn={zoomIn}
            zoomOut={zoomOut}
            resetPosition={resetPosition}
        />
    </div>;
}
