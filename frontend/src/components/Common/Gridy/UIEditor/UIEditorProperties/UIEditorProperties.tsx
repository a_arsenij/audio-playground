import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import { numberPropertyF, Properties, Property } from "../../../Properties/Properties";
import { UIEditorElement } from "../UIEditor";
import styles from "./UIEditorProperties.module.css";

type UIEditorPropertiesProps<
    ELEMENT extends UIEditorElement
> = {
    getTitle: (element: ELEMENT) => string,
    selectedElement: ELEMENT | undefined,
    setSelectedElement: SetState<ELEMENT>,
    getProperties: (element: ELEMENT) => Property[],
}

export function UIEditorProperties<
    ELEMENT extends UIEditorElement
>({
    getTitle,
    getProperties,
    selectedElement,
    setSelectedElement,
}: UIEditorPropertiesProps<
    ELEMENT
>) {
    return <div className={styles.uIEditorProperties}>
        <div className={styles.header}>
            Selected element: {selectedElement ? getTitle(selectedElement) : "None"}
        </div>
        {selectedElement && <div className={styles.body}>
            <Properties
                element={selectedElement}
                setElement={setSelectedElement}
                properties={ [
                    ...getProperties(selectedElement),
                    numberPropertyF("x",
                        {
                            name: "X",
                        },
                    ),
                    numberPropertyF("y",
                        {
                            name: "Y",
                        },
                    ),
                ] }
            />
        </div>}
    </div>;
}
