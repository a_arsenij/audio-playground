import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { SetState, useByKey, useSetById } from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback, useEffect, useMemo, useState } from "react";

import { Grid } from "../Grid/Grid";
import { PositioningGridOverlay } from "../PositioningGridOverlay/PositioningGridOverlay";
import { useMoveableElements } from "../UseMoveableElements";
import { useMoveableWorld } from "../UseMoveableWorld";
import { CurvePoint, Curves } from "./Curve/Curve";
import { Node } from "./Node/Node";
import styles from "./NodeEditor.module.css";
import { createConnection } from "./Utils";

export type PortShape = "circle" | "square";

export type NodeEditorConnection = {
    id: string,
    portIdFrom: string,
    portIdTo: string,
}

export type NodeEditorPort = {
    id: string,
}

export type NodeEditorNode<
    INPUT_PORTS extends NodeEditorPort[],
    OUTPUT_PORTS extends NodeEditorPort[],
> = {
    id: string,
    x: number,
    y: number,
    inputs: INPUT_PORTS,
    outputs: OUTPUT_PORTS,
    direction: "ltr" | "rtl",
}

export type NodeEditorGraph<
    NODE extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>
> = {
    nodes: NODE[],
    connections: NodeEditorConnection[],
}

export type NodeDim = {
    inputsWidth: number,
    outputsWidth: number,
    contentWidth: number,
    contentHeight: number,
};

export type GetGraphNodeType<GRAPH extends NodeEditorGraph<NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>>> =
    GRAPH["nodes"][number];

export type GetNodeInputsType<NODE extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>> =
    NODE["inputs"];

export type GetNodeOutputsType<NODE extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>> =
    NODE["outputs"];

export type GetNodeInputsOutputsType<NODE extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>> =
    | GetNodeInputsType<NODE>
    | GetNodeOutputsType<NODE>
    ;

export type GetNodeInputType<NODE extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>> =
    GetNodeInputsType<NODE>[number];

export type GetNodeOutputType<NODE extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>> =
    GetNodeOutputsType<NODE>[number];

export type GetNodeInputOutputType<NODE extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>> =
    | GetNodeInputType<NODE>
    | GetNodeOutputType<NODE>
    ;

type NodeEditorProps<
    GRAPH extends NodeEditorGraph<NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>>,
> = {
    selectedNodeId: string | undefined,
    setSelectedNodeId: SetState<string | undefined>,
    isPortLinkableTo: (
        from: GetNodeOutputType<GetGraphNodeType<GRAPH>>[],
        to: GetNodeInputType<GetGraphNodeType<GRAPH>>[]
    ) => string | undefined,
    getTitle: (t: GetGraphNodeType<GRAPH>) => string,
    getNodeDim: (t: GetGraphNodeType<GRAPH>) => NodeDim,
    getContent: (t: GetGraphNodeType<GRAPH>) => React.ReactNode,
    getPortTitle: (port: GetNodeInputOutputType<GetGraphNodeType<GRAPH>>) => string,
    getPortShape: (port: GetNodeInputOutputType<GetGraphNodeType<GRAPH>>) => PortShape,
    isNodeDeleteable: (node: GetGraphNodeType<GRAPH>) => boolean,
    isNodeCopiable: (node: GetGraphNodeType<GRAPH>) => boolean,
    graph: GRAPH,
    setGraph: SetState<GRAPH>,
    createClone: (
        node: GetGraphNodeType<GRAPH>,
        nodes: GetGraphNodeType<GRAPH>[],
    ) => GetGraphNodeType<GRAPH>,
}

export function NodeEditor<
    const GRAPH extends NodeEditorGraph<NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>>,
>({
    selectedNodeId,
    setSelectedNodeId,
    graph,
    setGraph,
    getTitle,
    isNodeDeleteable,
    isNodeCopiable,
    getNodeDim,
    getContent,
    getPortTitle,
    getPortShape,
    createClone,
    isPortLinkableTo,
}: NodeEditorProps<GRAPH>) {

    type NODE = GetGraphNodeType<GRAPH>;
    type OUTPUT = GetNodeOutputType<NODE>;
    type CONNECTIONS = GRAPH["connections"];

    const [
        nodes,
        setNodes,
    ] = useByKey(graph, setGraph, "nodes");

    const [
        connections,
        setConnections,
    ] = useByKey(graph, setGraph, "connections");

    const inputPortIdToPort = useMemo(() => new Map(
        nodes.map((node) =>
            node.inputs.map((port, index) => [port.id, {
                node,
                port,
                index,
            }] as const)).flat(),
    ), [nodes]);
    const outputPortIdToPort = useMemo(() => new Map(
        nodes.map((node) =>
            node.outputs.map((port, index) => [port.id, {
                node,
                port,
                index,
            }] as const)).flat(),
    ), [nodes]);

    useEffect(() => {
        // todo: get rid of reason
        setConnections((prev) => {
            const newConnections = prev.filter((c) =>
                outputPortIdToPort.get(c.portIdFrom) &&
                inputPortIdToPort.get(c.portIdTo),
            );
            if (newConnections.length !== prev.length) {
                console.warn("Removing orphaned connections");
                return newConnections;
            }
            return prev;
        });
    }, [connections, inputPortIdToPort, outputPortIdToPort, setConnections]);

    const cloneNode = (nodeId: string) => {
        setNodes((prev: NODE[]) => prev.map(
            (n: NODE) => n.id === nodeId
                ? [
                    n,
                    createClone(n, prev),
                ]
                : [n],
        ).flat());
    };

    const changeNodeDirection = (nodeId: string) => {
        setNodes((prev) => prev.map(
            (n) => n.id === nodeId
                ? {
                    ...n,
                    direction: n.direction === "ltr" ? "rtl" : "ltr",
                }
                : n,
        ));
    };

    const deleteNote = (nodeId: string) => {
        setGraph((prevgraph) => {
            const nodes: NODE[] = [];
            let connections = prevgraph.connections;
            for (const node of prevgraph.nodes) {
                if (node.id === nodeId) {
                    const prohibitedPortsIds = new Set([
                        node.inputs.map((i) => i.id),
                        node.outputs.map((o) => o.id),
                    ].flat());
                    connections = connections.filter((conn) =>
                        !prohibitedPortsIds.has(conn.portIdFrom) &&
                        !prohibitedPortsIds.has(conn.portIdTo),
                    );
                } else {
                    nodes.push(node);
                }
            }
            return {
                nodes,
                connections,
            } as GRAPH;
        });


    };

    const setNodeById = useSetById(setNodes);

    const [
        isConnectingNow,
        setIsConnectingNow,
    ] = useState<OUTPUT[]>([]);

    const onStartMoveWorld = useCallback(() => {
        setIsConnectingNow([]);
        setSelectedNodeId(undefined);
    }, [setSelectedNodeId]);

    const {
        offsetX,
        offsetY,
        zoomBase,
        zoom,
        isDraggingWorldNow,
        gridRef,
        resetPosition,
        zoomIn,
        zoomOut,
    } = useMoveableWorld(
        onStartMoveWorld,
    );

    const {
        draggedElementId,
        moveElement,
    } = useMoveableElements({
        zoom,
        elements: nodes,
        setElementById: setNodeById,
        setSelectedElementId: setSelectedNodeId,
    });

    const startConnection = (
        portId: string,
        ctrl: boolean,
        shift: boolean,
    ) => {
        const port = outputPortIdToPort.get(portId)?.port;
        if (!port) {
            return;
        }
        if (!ctrl && !shift) {
            setIsConnectingNow([port]);
            return;
        }
        if (ctrl && !shift) {
            setIsConnectingNow((prev) => {
                const copy = [...prev];
                const idx = copy.findIndex((p) => p.id === portId);
                if (idx < 0) {
                    copy.push(port);
                } else {
                    copy.splice(idx, 1);
                }
                return copy;
            });
            return;
        }
        if (!ctrl && shift) {
            const last = isConnectingNow.at(-1);
            if (!last) {
                return;
            }
            const lastNode = nodes.find((n) =>
                !!n.outputs.find((o) => o.id === last.id),
            );
            if (!lastNode) {
                return;
            }
            const currentNode = nodes.find((n) =>
                !!n.outputs.find((o) => o.id === portId),
            );
            if (!currentNode) {
                return;
            }
            if (lastNode.id !== currentNode.id) {
                return;
            }
            const lastIndex = lastNode.outputs.findIndex((o) => o.id === last.id);
            const currIndex = lastNode.outputs.findIndex((o) => o.id === portId);
            const min = Math.min(lastIndex, currIndex);
            const max = Math.max(lastIndex, currIndex);
            setIsConnectingNow((prev) => {
                const copy = [...prev];
                for (let i = min; i <= max; i++) {
                    if (i !== lastIndex) {
                        const port = lastNode.outputs[i]!;
                        const idx = copy.findIndex((p) => p.id === port.id);
                        if (idx < 0) {
                            copy.push(port);
                        } else {
                            copy.splice(idx, 1);
                        }
                    }
                }
                return copy;
            });
            return;
        }
    };

    const finishConnection = (
        portId: string[],
    ) => {
        setIsConnectingNow([]);
        if (isConnectingNow.length === 0) {
            return;
        }

        setConnections((prev: CONNECTIONS) => {
            const newConnections = [...prev];
            isConnectingNow.forEach((from, idx) => {
                const fromId = from.id;
                const toId = portId[idx]!;
                const index = newConnections.findIndex((c) =>
                    c.portIdFrom === fromId &&
                    c.portIdTo === toId,
                );
                if (index < 0) {
                    newConnections.push(createConnection(fromId, toId));
                } else {
                    newConnections.splice(index, 1);
                }
            });
            return newConnections;
        });
    };

    function leftPoint(node: NODE): number {
        return node.x;
    }

    function rightPoint(node: NODE): number {
        const dim = getNodeDim(node);
        return node.x +
            dim.contentWidth +
            dim.inputsWidth +
            dim.outputsWidth
        ;
    }

    const curves = connections.map((conn) => {

        const from = outputPortIdToPort.get(conn.portIdFrom);
        if (!from) {
            return;
        }
        const to = inputPortIdToPort.get(conn.portIdTo);
        if (!to) {
            return;
        }

        return [
            {
                x: from.node.direction === "ltr" ? rightPoint(from.node) : leftPoint(from.node),
                y: from.node.y + 2 + from.index,
                side: from.node.direction === "ltr" ? "right" : "left",
            },
            {
                x: to.node.direction === "ltr" ? leftPoint(to.node) : rightPoint(to.node),
                y: to.node.y + 2 + to.index,
                side: to.node.direction === "ltr" ? "left" : "right",
            },
        ] as [CurvePoint, CurvePoint];

    }).filter((a) => a) as [CurvePoint, CurvePoint][];


    return <div
        className={clsx(
            styles.wrapper,
            (draggedElementId !== undefined || isDraggingWorldNow) && styles.nodeEditorDragging,
        )}
    >
        <PositioningGridOverlay
            zoomIn={zoomIn}
            zoomOut={zoomOut}
            resetPosition={resetPosition}
        />
        <Grid
            offsetX={offsetX}
            offsetY={offsetY}
            zoomBase={zoomBase}
            gridRef={gridRef}
        >
            <Curves curves={curves}/>
            {nodes.map((node: NODE) => {
                return <Node
                    isSelectedNow={selectedNodeId === node.id}
                    key={node.id}
                    node={node}
                    title={getTitle(node)}
                    isNodeDeleteable={isNodeDeleteable(node)}
                    isNodeCopiable={isNodeCopiable(node)}
                    isNodeDraggedNow={draggedElementId === node.id}
                    getPortShape={getPortShape}
                    getPortTitle={getPortTitle}
                    getNodeDim={getNodeDim}
                    getContent={getContent}
                    isConnectingNow={isConnectingNow}
                    isPortLinkableTo={isPortLinkableTo}
                    startConnection={startConnection}
                    finishConnection={finishConnection}
                    moveNode={moveElement}
                    changeNodeDirection={changeNodeDirection}
                    cloneNode={cloneNode}
                    deleteNode={deleteNote}
                />;
            })}
        </Grid>
    </div>;
}


