
import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React from "react";

import { useDebug } from "../../../../../utils/useDebug";
import { Svg18ArrowLeft } from "../../../GeneratedIcons/18/Arrow/Left";
import { Svg18ArrowRight } from "../../../GeneratedIcons/18/Arrow/Right";
import { Svg18CommonCopy } from "../../../GeneratedIcons/18/Common/Copy";
import { Svg18CommonDelete } from "../../../GeneratedIcons/18/Common/Delete";
import { MoveableElement } from "../../MoveableElement/MoveableElement";
import {
    GetNodeInputOutputType, GetNodeInputType,
    GetNodeOutputType,
    NodeDim,
    NodeEditorNode,
    NodeEditorPort,
    PortShape,
} from "../NodeEditor";
import { Port } from "../Port/Port";
import styles from "./Node.module.css";



type NodeProps<
    NODE extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>
> = {
    isSelectedNow: boolean,
    title: string,
    node: NODE,
    isNodeDeleteable: boolean,
    isNodeCopiable: boolean,
    isNodeDraggedNow: boolean,
    getNodeDim: (t: NODE) => NodeDim,
    getContent: (node: NODE) => React.ReactNode,
    getPortTitle: (port: GetNodeInputOutputType<NODE>) => string,
    getPortShape: (port: GetNodeInputOutputType<NODE>) => PortShape,
    isConnectingNow: GetNodeOutputType<NODE>[],
    isPortLinkableTo: (
        from: GetNodeOutputType<NODE>[],
        to: GetNodeInputType<NODE>[]
    ) => string | undefined,
    startConnection: (nodeId: string, ctrl: boolean, shift: boolean) => void,
    finishConnection: (nodeId: string[]) => void,
    moveNode: (e: React.MouseEvent, nodeId: string) => void,
    changeNodeDirection: (nodeId: string) => void,
    cloneNode: (nodeId: string) => void,
    deleteNode: (nodeId: string) => void,
};

export function Node<
    const NODE extends NodeEditorNode<NodeEditorPort[], NodeEditorPort[]>,
>({
    isSelectedNow,
    title,
    node,
    isNodeDeleteable,
    isNodeCopiable,
    isNodeDraggedNow,
    getNodeDim,
    getContent,
    getPortShape,
    getPortTitle,
    isConnectingNow,
    isPortLinkableTo,
    startConnection,
    finishConnection,
    moveNode,
    changeNodeDirection,
    cloneNode,
    deleteNode,
}: NodeProps<NODE>) {
    const dim = getNodeDim(node);
    const height = Math.max(
        dim.contentHeight,
        node.inputs.length + 1,
        node.outputs.length + 1,
    );
    const [debug] = useDebug();
    return <MoveableElement
        isSelectedNow={isSelectedNow}
        x={node.x}
        y={node.y}
        title={debug === "on" ? `${title} / ${node.id}` : title}
        width={dim.contentWidth + dim.inputsWidth + dim.outputsWidth}
        height={height}
        isDraggedNow={isNodeDraggedNow}
        onMouseDown={(e) => moveNode(e, node.id)}
        buttons={[
            {
                onClick: () => changeNodeDirection(node.id),
                title: "Change direction",
                icon: node.direction === "ltr"
                    ? <Svg18ArrowRight/>
                    : <Svg18ArrowLeft/>,
            },
            isNodeCopiable && {
                onClick: () => cloneNode(node.id),
                title: "Clone node",
                icon: <Svg18CommonCopy/>,
            },
            isNodeDeleteable && {
                onClick: () => deleteNode(node.id),
                title: "Delete node",
                icon: <Svg18CommonDelete/>,
            },
        ]}
    >
        <div
            className={clsx(styles.container, node.direction === "rtl" && styles.rtl)}
            style={variables({
                "--inputsWidth": node.direction === "ltr" ? dim.inputsWidth : dim.outputsWidth,
                "--outputsWidth": node.direction === "ltr" ? dim.outputsWidth : dim.inputsWidth,
            })}
        >
            <div className={styles.inputContainer}>
                {node.inputs.map((input, idx) => {
                    const remaining = node.inputs.length - idx;
                    function targetNodes() {
                        return [...Array(isConnectingNow.length)]
                            .map((_, offset) => node.inputs[idx + offset]!);
                    }
                    const linkError =
                    isConnectingNow.length === 0
                        ? undefined
                        : remaining < isConnectingNow.length
                            ? "Not enough target nodes"
                            : isPortLinkableTo(
                                isConnectingNow,
                                targetNodes(),
                            );
                    return <Port
                        key={input.id}
                        port={input}
                        title={getPortTitle(input)}
                        shape={getPortShape(input)}
                        idx={idx}
                        side={node.direction === "ltr" ? "left" : "right"}
                        active={false}
                        disabled={!!linkError || !isConnectingNow}
                        tooltip={linkError}
                        onClickConnect={() => finishConnection(targetNodes().map((n) => n.id))}
                    />;
                })}
            </div>
            <div
                className={clsx(
                    styles.nodeContent,
                    node.inputs.length > 0 && styles.hasInputs,
                    node.outputs.length > 0 && styles.hasOutputs,
                )}
            >
                {getContent(node)}
            </div>
            <div className={styles.outputContainer}>
                {node.outputs.map((output, idx) => <Port
                    key={output.id}
                    port={output}
                    title={getPortTitle(output)}
                    shape={getPortShape(output)}
                    idx={idx}
                    side={node.direction === "ltr" ? "right" : "left"}
                    active={!!isConnectingNow.find((e) => e.id === output.id)}
                    disabled={false}
                    onClickConnect={(ctrl, shift) => startConnection(output.id, ctrl, shift)}
                />)}
            </div>

        </div>
    </MoveableElement>;
}
