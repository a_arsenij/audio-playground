import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React from "react";

import { NodeEditorPort, PortShape } from "../NodeEditor";
import styles from "./Port.module.css";

type PortProps<PORT extends NodeEditorPort> = {
    side: "left" | "right",
    idx: number,
    shape: PortShape,
    port: PORT,
    title: string,
    disabled: boolean,
    active: boolean,
    tooltip?: string | undefined,
    onClickConnect: (
        ctrlPressed: boolean,
        shiftPressed: boolean,
    ) => void,
}

export function Port<PORT_VALUE extends NodeEditorPort>({
    side,
    idx,
    shape,
    title,
    disabled,
    active,
    tooltip,
    onClickConnect,
}: PortProps<PORT_VALUE>) {
    return <button
        className={clsx(
            side === "left"
                ? styles.leftPort
                : styles.rightPort,
            shape === "circle" && styles.circle,
            shape === "square" && styles.square,
            active && styles.portActive,
            styles.port,
        )}
        disabled={disabled}
        title={tooltip ?? title}
        style={variables({ "--idx": idx })}
        onClick={(e) => onClickConnect(
            e.ctrlKey,
            e.shiftKey,
        )}
    >
        <div className={ styles.portShape}/>
        <div className={styles.portTitle}>
            {title}
        </div>
    </button>;
}
