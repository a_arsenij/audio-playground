import { generateId } from "@audio-playground/lib-common/std/Id";

import { NodeEditorConnection, NodeEditorNode, NodeEditorPort } from "./NodeEditor";


export function basicCloneNode<
    INPUT_PORTS extends NodeEditorPort[],
    OUTPUT_PORTS extends NodeEditorPort[],
    NODE extends NodeEditorNode<INPUT_PORTS, OUTPUT_PORTS>
>(
    n: NODE,
    nodes: NODE[],
): NODE {

    const prevX = n.x;
    const prevY = n.y;
    const dX = 0;
    const dY = 3 + Math.max(n.inputs.length, n.outputs.length);
    let nextX = prevX + dX;
    let nextY = prevY + dY;
    while (true) {
        const found = nodes.find(
            (n) => n.x === nextX && n.y === nextY,
        );
        if (!found) {
            break;
        }
        nextX += dX;
        nextY += dY;
    }

    return {
        ...n,
        id: generateId(),
        x: nextX,
        y: nextY,
        inputs: n.inputs.map((i) => ({
            ...i,
            id: generateId(),
        })),
        outputs: n.outputs.map((o) => ({
            ...o,
            id: generateId(),
        })),
    };
}


export function createConnection(
    from: string,
    to: string,
): NodeEditorConnection {
    return {
        id: generateId(),
        portIdFrom: from,
        portIdTo: to,
    };
}
