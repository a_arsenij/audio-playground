import { Point } from "@audio-playground/lib-common/math/Point";
import React from "react";

import styles from "./Curve.module.css";

export type CurvePoint = Point & {
    side: "left" | "right",
}

type CurveProps = {
    c: [CurvePoint, CurvePoint],
}

const NODE_EDITOR_CONNECTION_INERTIA = 5;

export function Curve({
    c,
}: CurveProps) {
    return <path
        className={styles.connection}
        d={`
                                M ${c[0].x} ${c[0].y} 
                                C ${c[0].x + (c[0].side === "right" ? 1 : -1) * NODE_EDITOR_CONNECTION_INERTIA} ${c[0].y},
                                  ${c[1].x + (c[1].side === "right" ? 1 : -1) * NODE_EDITOR_CONNECTION_INERTIA} ${c[1].y}, 
                                  ${c[1].x} ${c[1].y}
                          `}
        markerEnd="url(#arrowhead)"
    />;
}

type CurvesProps = {
    curves: [CurvePoint, CurvePoint][],
}

export function Curves({
    curves,
}: CurvesProps) {
    return <svg
        className={styles.svg}
        viewBox={"0 0 1000 1000"}
    >
        <defs>
            <marker
                id="arrowhead"
                markerWidth="10"
                markerHeight="7"
                refX="10"
                refY="3.5"
                orient="auto"
            >
                <polygon
                    className={styles.connectionCap}
                    points="0 0, 10 3.5, 0 7"
                />
            </marker>
        </defs>
        {
            curves.map((c) => {
                return <Curve
                    key={`${c[0].x}/${c[0].y}/${c[1].x}/${c[1].y}`}
                    c={c}
                />;
            })
        }
    </svg>;
}
