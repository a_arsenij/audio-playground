import React from "react";

import { Button } from "../../Button/Button";
import { Svg18ZoomMinus } from "../../GeneratedIcons/18/Zoom/Minus";
import { Svg18ZoomPlus } from "../../GeneratedIcons/18/Zoom/Plus";
import { Svg18ZoomReset } from "../../GeneratedIcons/18/Zoom/Reset";
import { GridOverlay } from "../GridOverlay/GridOverlay";

type PositioningGridOverlayProps = {
    zoomIn: () => void,
    zoomOut: () => void,
    resetPosition: () => void,
}

export function PositioningGridOverlay({
    zoomIn,
    zoomOut,
    resetPosition,
}: PositioningGridOverlayProps) {
    return <GridOverlay side={"left"}>
        <Button
            key="zoomIn"
            title={"Zoom in"}
            onClick={zoomIn}
            iconLeft={Svg18ZoomPlus}
        />
        <Button
            key="zoomOut"
            title={"Zoom out"}
            onClick={zoomOut}
            iconLeft={Svg18ZoomMinus}
        />
        <Button
            key="resetZoom"
            title={"Reset zoom"}
            onClick={resetPosition}
            iconLeft={Svg18ZoomReset}
        />
    </GridOverlay>;
}
