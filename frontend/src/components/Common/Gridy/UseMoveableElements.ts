import { LEFT_BUTTON } from "@audio-playground/lib-frontend/std/MouseButtons";
import { SetStateValue } from "@audio-playground/lib-frontend/std/SetState";
import React, { useCallback, useState } from "react";

import { GRID_CELL_PX } from "./Grid/Grid";


export type MoveableElements = {
    draggedElementId: string | undefined,
    moveElement: (e: React.MouseEvent, nodeId: string) => void,
}

export type MoveableElement = {
    x: number,
    y: number,
    id: string,
}

type Arg<T extends MoveableElement> = {
    zoom: number,
    elements: T[],
    setElementById: (
        id: string,
        set: SetStateValue<T>
    ) => void,
    setSelectedElementId?: (id: string | undefined) => void,
};

export function useMoveableElements<T extends MoveableElement>({
    zoom,
    elements,
    setElementById,
    setSelectedElementId,
}: Arg<T>): MoveableElements {

    const [draggedElementId, setDraggedElementId] = useState<string | undefined>(undefined);

    const moveElement = useCallback((e: React.MouseEvent, nodeId: string) => {
        if (e.button !== LEFT_BUTTON) {
            return;
        }
        e.stopPropagation();
        const selectedNode = elements.find((node) => node.id === nodeId);
        if (!selectedNode) {
            return;
        }
        setSelectedElementId?.(selectedNode.id);
        setDraggedElementId(selectedNode.id);
        const ox = selectedNode.x;
        const oy = selectedNode.y;
        const sx = e.clientX;
        const sy = e.clientY;
        let lsx = ox;
        let lsy = oy;

        function onMove(e: MouseEvent) {
            e.preventDefault();
            e.stopPropagation();
            const dx = Math.round((e.clientX - sx) / zoom / GRID_CELL_PX);
            const dy = Math.round((e.clientY - sy) / zoom / GRID_CELL_PX);
            const nx = ox + dx;
            const ny = oy + dy;
            if (lsx !== nx || lsy !== ny) {
                setElementById(nodeId, (n) => ({
                    ...n,
                    x: nx,
                    y: ny,
                }));
                lsx = nx;
                lsy = ny;
            }
        }

        function onUp() {
            setDraggedElementId(undefined);
            window.removeEventListener("mousemove", onMove);
            window.removeEventListener("mouseup", onUp);
        }

        window.addEventListener("mousemove", onMove);
        window.addEventListener("mouseup", onUp);
    }, [elements, setElementById, setSelectedElementId, zoom]);

    return {
        draggedElementId,
        moveElement,
    };

}
