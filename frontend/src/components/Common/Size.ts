

export type Size = "tiny" | "small" | "medium" | "big"
