import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React from "react";

import styles from "./Progress.module.css";

type ProgressProps = {
    value: number,
}

export function Progress(props: ProgressProps) {
    return <div className={styles.progress}>
        <div className={styles.valueTextOutside}>
            {Math.round(props.value * 100)}%
        </div>
        <div
            className={styles.valueFill}
            style={variables({
                "--progress-value": props.value,
            })}
        >
            <div className={styles.valueTextInside}>
                {Math.round(props.value * 100)}%
            </div>
        </div>
    </div>;
}
