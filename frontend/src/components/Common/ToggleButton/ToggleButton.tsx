import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React, { ReactNode, SVGProps } from "react";

import styles from "./ToggleButton.module.css";

export type ToggleButtonProps = {
    icon: (props: SVGProps<SVGSVGElement>) => ReactNode,
    title?: string,
    disabled?: boolean,
    active?: boolean,
    onClick?: (() => void) | undefined,
    onRightClick?: (() => void) | undefined,
}

export function ToggleButton(props: ToggleButtonProps) {
    const {
        title,
        disabled,
        active,
        onClick,
        onRightClick,
        icon: Icon,
    } = props;

    return <button
        title={title}
        onClick={onClick}
        onContextMenu={(e) => {
            if (onRightClick) {
                e.preventDefault();
                e.stopPropagation();
                onRightClick();
            }
        }}
        className={clsx(
            styles.toggleButton,
            active && styles.active,
        )}
        disabled={disabled}
    >
        <Icon />
    </button>;
}
