import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React from "react";

import { CheckboxBox } from "../CheckboxBox/CheckboxBox";
import styles from "./DropdownItem.module.css";

type DropdownItemProps = {
    title: string,
    description?: string,
    icon?: React.FC<{}>,
    onClick?: () => void,
    selected?: boolean,
}

export function DropdownItem({
    title,
    description,
    icon,
    onClick,
    selected,
}: DropdownItemProps) {
    return <button className={clsx(styles.dropdownItem, selected && styles.selected)} onClick={onClick}>
        {icon && <div className={styles.icon}>
            {icon({})}
        </div>}
        <div className={styles.title}>
            {title}
        </div>
        {description && <div className={styles.description}>
            {description}
        </div>}
    </button>;
}

type CheckboxDropdownItemProps = {
    title: string,
    description?: string,
    onClick?: () => void,
    checked: boolean,
}

export function CheckboxDropdownItem({
    title,
    description,
    onClick,
    checked,
}: CheckboxDropdownItemProps) {
    return <DropdownItem
        title={title}
        description={description}
        onClick={onClick}
        icon={
            () => <CheckboxBox checked={checked}/>
        }
    />;
}
