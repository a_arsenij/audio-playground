import { FILTER_TYPES, FilterType } from "@audio-playground/lib-sound/FilterType";
import React, { useRef } from "react";

import { ContextMenu } from "../ContextMenu/ContextMenu";
import { filterIcon } from "../FilterIcon/FilterIcon";
import { FilterSwitch } from "../FilterSwitch/FilterSwitch";
import styles from "./FilterButton.module.css";

type FilterButtonProps = {
    type: FilterType,
    setType: (type: FilterType) => void,
}

export function FilterButton(props: FilterButtonProps) {
    const ref = useRef<HTMLButtonElement>(null);
    const pickNextFilterType = () => {
        let idx = FILTER_TYPES.findIndex((t) => t === props.type);
        idx += 1;
        idx %= FILTER_TYPES.length;
        const newType = FILTER_TYPES[idx]!;
        props.setType(newType);
    };
    const Icon = filterIcon(props.type);
    return <>
        <ContextMenu
            rightMouseClickTargetRef={ref}
            ChildComponent={FilterSwitch}
            props={props}
        />
        <button
            className={styles.filterButton}
            ref={ref}
            onClick={pickNextFilterType}
        >
            <Icon/>
        </button>
    </>;
}
