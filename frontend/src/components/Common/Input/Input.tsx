import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React, { useCallback, useEffect, useRef, useState } from "react";

import { Size } from "../Size";
import styles from "./Input.module.css";

type InputProps = {
    inputRef?: React.RefObject<HTMLInputElement>,
    type: string,
    className?: string,
    value: string,
    onChange: (v: string) => void,
    onEnter?: () => void,
    valid?: boolean,
}

function classNames(
    valid: boolean,
    className: string | undefined,
): string {
    return clsx(
        styles.input,
        !valid && styles.invalid,
        className,
    );
}

export function Input({
    inputRef,
    type,
    value,
    onChange,
    valid = true,
    onEnter = undefined,
    className = "",
}: InputProps) {
    return <input
        ref={inputRef}
        type={type}
        className={classNames(valid, className)}
        value={value}
        onKeyDown={(e) => {
            if (e.code === "Enter") {
                onEnter && onEnter();
            }
        }}
        onChange={(e) => onChange(e.target.value)}
    />;
}

type TextInputProps = {
    className?: string,
    value: string,
    onChange: (v: string) => void,
    size?: Size,
}

export function TextInput({
    value,
    onChange,
    className = "",
}: TextInputProps) {
    return <Input
        className={className}
        type={"text"}
        value={value}
        onChange={onChange}
    />;
}

type NumberInputProps = {
    inputRef?: React.RefObject<HTMLInputElement>,
    className?: string,
    value: number,
    onChange: (v: number) => void,
    onEnter?: () => void,
    size?: Size,
}

export function NumberInput({
    inputRef,
    value,
    onChange,
    onEnter,
    className = "",
}: NumberInputProps) {

    const [
        valid, setValid,
    ] = useState(true);

    const [
        state, setState,
    ] = useState(
        value.toString(),
    );

    useEffect(() => {
        setState(value.toString());
    }, [value]);

    const handleChange = (v: string) => {
        const d = Number(v);
        setState(v);
        if (isNaN(d)) {
            setValid(false);
        } else {
            setValid(true);
            onChange(d);
        }
    };

    return <Input
        inputRef={inputRef}
        valid={valid}
        className={className}
        type={"text"}
        onEnter={onEnter}
        value={state}
        onChange={handleChange}
    />;

}

type HighlightColor =
    | "red"
    | "green"
    | "yellow"
    | "default"
;

export type HighlightedSpan = { text: string; color?: HighlightColor; bold?: boolean; underline?: boolean; italic?: boolean };

type HighlightedInputProps = {
    value: HighlightedSpan[],
    onChange: (v: string) => void,
    size?: Size,
}

export function HighlightedInput({
    value,
    onChange,
}: HighlightedInputProps) {
    const ref = useRef<HTMLDivElement>(null);

    const onInput = useCallback(() => {
        const el = ref.current!;
        const text = el.innerText;
        onChange(text);
    }, [onChange]);

    useEffect(() => {

        let range: Range | undefined;
        try {
            range = window.getSelection()?.getRangeAt(0);
        } catch (_) {

        }
        let num = 0;
        if (range) {
            const els = [...ref.current!.children].map((c) => [...c.childNodes]).flat();
            for (const e of els) {
                const sc = range.endContainer;
                if (e === sc) {
                    num += range.endOffset;
                    break;
                } else {
                    num += e.textContent!.length;
                }
            }
        }

        ref.current!.innerHTML = value.map((v) => `<span
        class="${clsx(
        v.color === "red" && styles.red,
        v.color === "green" && styles.green,
        v.color === "yellow" && styles.yellow,
        v.bold && styles.bold,
        v.italic && styles.italic,
        v.underline && styles.underline,
    )}"
    >${v.text}</span>`).join("");

        if (num === 0) {
            return;
        }

        let num2 = 0;
        if (range) {
            const els = [...ref.current!.children].map((c) => [...c.childNodes]).flat();
            for (const e of els) {
                const l = e.textContent!.length;
                if (num - num2 <= l) {
                    window.getSelection()?.setPosition(e, num - num2);
                    break;
                } else {
                    num2 += l;
                }
            }
        }

    }, [value]);

    return <div
        className={clsx(
            styles.input,
            styles.highlighted,
        )}
        contentEditable={"true"}
        suppressContentEditableWarning={true}
        onInput={onInput}
        ref={ref}
    />;
}
