import React from "react";

import styles from "./HoriLine.module.css";

type HoriLineProps = {
    children: React.ReactNode,
}

export function HoriLine(props: HoriLineProps) {
    return <div className={styles.horiLine}>
        {props.children}
    </div>;
}
