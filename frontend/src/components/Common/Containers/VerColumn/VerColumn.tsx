import React from "react";

import styles from "./VerColumn.module.css";

type VerColumnProps = {
    children: React.ReactNode,
}

export function VerColumn(props: VerColumnProps) {
    return <div className={styles.verColumn}>
        {props.children}
    </div>;
}
