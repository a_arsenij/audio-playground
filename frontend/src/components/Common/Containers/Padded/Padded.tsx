import React, { PropsWithChildren } from "react";

import styles from "./Padded.module.css";

type PaddedProps = PropsWithChildren;

export function Padded(props: PaddedProps) {
    return <div className={styles.padded}>
        {props.children}
    </div>;
}
