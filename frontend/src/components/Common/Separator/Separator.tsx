import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React from "react";

import styles from "./Separator.module.css";

type SeparatorProps = {
    type: "horizontal" | "vertical",
}

export function Separator({
    type,
}: SeparatorProps) {
    return <div className={clsx(
        styles.separator,
        type === "horizontal" && styles.horizontal,
        type === "vertical" && styles.vertical,
    )}>

    </div>;
}
