import React from "react";

import { CheckboxBox } from "../CheckboxBox/CheckboxBox";
import styles from "./Checkbox.module.css";

type KnobProps = {
    label?: string,
    value: boolean,
    setValue: (v: boolean) => void,
}

export function Checkbox({
    label,
    value,
    setValue,
}: KnobProps) {
    return <div
        className={styles.wrapper}
        onClick={() => setValue(!value)}
    >
        <CheckboxBox checked={value}/>
        {label && <div className={styles.title}>{label}</div> }
    </div>;
}
