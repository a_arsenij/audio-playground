
import React, { PropsWithChildren, useEffect, useRef } from "react";

import { Button } from "../Button/Button";
import { Svg18CommonCross } from "../GeneratedIcons/18/Common/Cross";
import styles from "./Dialog.module.css";

type DialogProps = PropsWithChildren & {
    open: boolean,
    title: string,
    onClose: () => void,
}



export function Dialog({
    title,
    onClose,
    open,
    children,
}: DialogProps) {
    const refToDialog = useRef<HTMLDialogElement>(null);
    useEffect(
        () => {
            const el = refToDialog.current!;
            if (open) {
                el.showModal();
            } else {
                el.close();
            }
        },
        [open],
    );
    useEffect(
        () => {
            const el = refToDialog.current!;
            el.addEventListener("close", onClose);
            return () => {
                el.removeEventListener("close", onClose);
            };
        },
        [onClose],
    );
    return <dialog ref={refToDialog} className={styles.dialog}>
        <div className={styles.titleLine}>
            <div className={styles.title}>
                {title}
            </div>
            <Button
                onClick={onClose}
                title={"Close dialog"}
                iconLeft={Svg18CommonCross}
                style={"ghost"}
            />
        </div>
        <div className={styles.content}>
            {children}
        </div>
    </dialog>;
}
