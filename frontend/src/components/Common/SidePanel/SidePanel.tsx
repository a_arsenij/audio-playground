import React from "react";

import styles from "./SidePanel.module.css";

type SidePanelProps = {
    mainContent: React.ReactNode,
    leftPanelContent?: React.ReactNode,
    rightPanelContent?: React.ReactNode,
}

export function SidePanel(props: SidePanelProps) {
    return <div className={styles.sidePanelContainer}>
        {props.leftPanelContent && <div className={styles.sidePanel}>
            {props.leftPanelContent}
        </div>}
        <div className={styles.main}>
            {props.mainContent}
        </div>
        {props.rightPanelContent && <div className={styles.sidePanel}>
            {props.rightPanelContent}
        </div>}
    </div>;
}
