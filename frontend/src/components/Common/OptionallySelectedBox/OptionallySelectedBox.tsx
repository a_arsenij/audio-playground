import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React, { PropsWithChildren } from "react";

import styles from "./OptionallySelectedBox.module.css";

type OptionallySelectedBoxProps =
    & PropsWithChildren
    & React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>
    & { isSelected?: boolean }

export function OptionallySelectedBox(props: OptionallySelectedBoxProps) {

    const { isSelected, children, ...restProps } = props;
    return <div
        className={clsx(styles.optionallySelectedBox, isSelected && styles.selected)}
        {...restProps}
    >
        {children}
    </div>;
}
