import { useSetByIndex } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import {
    FUNCTION_TYPES_MAP, PieceData, SvgFunctionTypeId,
} from "../../../../utils/SvgGraphFunctions";
import { Knob } from "../../Knob/Knob";
import { DeprecatedSwitch } from "../../Switch/Switch";
import styles from "./GraphTooltip.module.css";

type GraphTooltipProps = {
    pieceIndex: number,
    pieceData: PieceData[],
    setPieceData: React.Dispatch<React.SetStateAction<PieceData[]>>,

}

export function GraphTooltip(props: GraphTooltipProps) {
    const { pieceIndex, pieceData, setPieceData } = props;

    const setPieceByIndex = useSetByIndex(setPieceData);
    const currentPiece = pieceData[pieceIndex];

    if (!currentPiece) {
        return null;
    }

    function changeParam(val: number, i: number) {
        if (pieceIndex === null) {
            return;
        }
        setPieceByIndex(pieceIndex, (prev) => {
            const params = prev.params.slice();
            params[i] = val;
            return {
                ...prev,
                params,
            };
        });
    }

    function changeFunction(val: SvgFunctionTypeId) {
        if (pieceIndex === null || !currentPiece) {
            return;
        }

        const newParamsDefaults = FUNCTION_TYPES_MAP[val].paramsSettings.map((p) => p.defaultValue);

        setPieceByIndex(pieceIndex, {
            params: newParamsDefaults,
            fun: val,
        });

    }

    const knobs = currentPiece.params.map((p, i) => {

        const fun = FUNCTION_TYPES_MAP[currentPiece.fun];

        return <Knob
            func={"linear"}
            value={p}
            setValue={(val) => changeParam(val, i)}
            label={fun.paramsSettings[i]!.label}
        />;
    });

    const fTypes = Object.keys(FUNCTION_TYPES_MAP) as SvgFunctionTypeId[];

    return <div className={styles.graphTooltip}>
        {knobs}
        <DeprecatedSwitch
            options={fTypes}
            value={currentPiece.fun}
            setValue={changeFunction}

            renderOption={(option, isSelected) => {
                return <div > {option} selected:{isSelected}</div>;
            }}/>
    </div>;

}
