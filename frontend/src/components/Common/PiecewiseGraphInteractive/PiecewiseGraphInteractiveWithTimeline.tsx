import { useReffedState } from "@audio-playground/lib-frontend/hooks/useReffedState";
import React, { CSSProperties } from "react";

import { Timeline } from "../Timeline/Timeline";
import { PiecewiseGraphInteractive, PiecewiseGraphInteractiveProps } from "./PiecewiseGraphInteractive";
import styles from "./PiecewiseGraphInteractive.module.css";

export function PiecewiseGraphInteractiveWithTimeline(props: PiecewiseGraphInteractiveProps) {

    const [pxPerBar, , setPxPerBar] = useReffedState(() => 1024);

    const contentWidthBar = 1;
    const style = {
        "--width": `${contentWidthBar * pxPerBar}`,
    } as CSSProperties;
    return <Timeline
        rowsTitles={<div/>}
        rowsElems={<div className={styles.rows} style={style}>
            <PiecewiseGraphInteractive {...props} />
        </div>}
        contentWidthBars={1}
        rowTitlesWidth={0}
        rowHeight={1}
        pxPerBar={pxPerBar}
        setPxPerBar={setPxPerBar}
        isCssHeightParentDefined={true}
    />;
}
