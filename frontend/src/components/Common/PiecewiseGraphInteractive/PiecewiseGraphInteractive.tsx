import { clamp } from "@audio-playground/lib-common/math/Interval";
import { Point } from "@audio-playground/lib-common/math/Point";
import { RIGHT_BUTTON } from "@audio-playground/lib-frontend/std/MouseButtons";
import { SetState, useSetByIndex } from "@audio-playground/lib-frontend/std/SetState";
import React, { useEffect, useRef, useState } from "react";

import { getSvgCoordinates } from "../../../utils/svg";
import { PieceData, PiecewiseFunctionData } from "../../../utils/SvgGraphFunctions";
import { PiecePath } from "../PiecePath/PiecePath";
import { GraphTooltip } from "./GraphTooltip/GraphTooltip";
import styles from "./PiecewiseGraphInteractive.module.css";

export type PiecewiseGraphInteractiveProps = PiecewiseFunctionData & {
    setPieceData: SetState<PieceData[]>,
    setPoints: SetState<Point[]>,
}

const BBLeft = 0;
const BBRight = 1;
const BBBottom = 0;
const BBTop = 1;

export function PiecewiseGraphInteractive(props: PiecewiseGraphInteractiveProps) {
    const { points, pieceData, setPieceData, setPoints } = props;

    const setPointsByIndex = useSetByIndex(setPoints);
    const setPieceByIndex = useSetByIndex(setPieceData);

    const [draggedPoint, setDraggedPoint] = useState<number|null>(null);
    const [tooltipedPiece, setTooltipedPiece] = useState<number>(-1);
    const svg = useRef<SVGSVGElement>(null);

    const pieceElems = pieceData.map((f, i) => {
        return <PiecePath
            {...f}
            key={i}
            from={points[i]!}
            to={ points[i + 1]!}
            onClick={() => {
                setTooltipedPiece(i);
            }}
            onWheel={(diff) => {
                updateFirstParam(diff, i);
            }}
            hoverable={true}
        />;
    });

    const pieceDots = points.map((p, i) => {
        return <path
            key={i}
            className={styles.dot}
            vectorEffect="non-scaling-stroke"
            strokeLinecap="round"
            d={`M ${p.x} ${p.y} l 0.0001 0`}

            onMouseDown={(e) => {
                if (e.button === RIGHT_BUTTON) {
                    e.stopPropagation();
                    e.preventDefault();
                    onRemovePoint(i);
                } else {
                    setDraggedPoint(i);
                    if (i > 0) {
                        setTooltipedPiece(i - 1);
                    }
                }
            }}

            onContextMenu={(e) => {
                e.stopPropagation(); e.preventDefault();
            }
            }
        />;
    });

    const lastPointIdx = points.length - 1;
    const minX = draggedPoint !== null && draggedPoint !== 0 && points[draggedPoint - 1]!.x;
    const maxX = draggedPoint !== null &&
        draggedPoint !== lastPointIdx &&
        points[draggedPoint + 1]!.x;

    useEffect(() => {
        if (draggedPoint === null) {
            return;
        }
        const onDrag = (e: MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            if (draggedPoint === null) {
                console.error("draggedPoint is not defined");
                return;
            }
            const newP = getSvgCoordinates(e, svg.current);

            const clampedPoint = {
                x: clampX(newP, minX, maxX),
                y: clamp(newP.y, BBBottom, BBTop),
            };
            setPointsByIndex(draggedPoint, clampedPoint);
        };

        window.addEventListener("mousemove", onDrag);
        const stopDrag = () => {
            window.removeEventListener("mousemove", onDrag);
            setDraggedPoint(null);
        };
        window.addEventListener("mouseup", stopDrag, { once: true });

        return () => {
            window.removeEventListener("mousemove", onDrag);
            window.removeEventListener("mouseup", stopDrag);
        };
    }, [draggedPoint, lastPointIdx, maxX, minX, setPointsByIndex]);

    function onCreatePoint(e: React.MouseEvent) {
        e.preventDefault();
        e.stopPropagation();

        const svgPoint = getSvgCoordinates(e, svg.current);
        const newP = { x: svgPoint.x, y: svgPoint.y };
        const ind = points.findLastIndex((p) => newP.x >= p.x);

        const newPoints = points.slice();
        newPoints.splice(ind + 1, 0, newP);
        setPoints(newPoints);

        const newPieces = pieceData.slice();
        newPieces.splice(ind, 0, { fun: "linear", params: [] });
        setPieceData(newPieces);

        setDraggedPoint(ind + 1);
    }
    function onRemovePoint(i: number) {
        if (i === 0 || i === points.length - 1) {
            return;
        }

        const newPoints = points.slice();
        newPoints.splice(i, 1);
        setPoints(newPoints);

        const newPieces = pieceData.slice();
        newPieces.splice(i - 1, 1);
        setPieceData(newPieces);
    }
    function updateFirstParam(diff: number, i: number) {
        setPieceByIndex(i, (prev) => {
            const params = prev.params.slice();

            if (params.length > 0) {
                params[0] = clamp(params[0]! + diff, 0, 1);
            }

            return { ...prev, params };
        });
    }

    return <div
        className={styles.piecewiseGraphInteractive}
    >
        <svg
            xmlns={"http://www.w3.org/2000/svg"}
            className={styles.svg}
            viewBox={`${BBLeft} ${BBBottom} ${BBRight - BBLeft} ${BBTop - BBBottom}`}
            preserveAspectRatio={"none"}
            ref={svg}
            onMouseDown={(e) => {
                if (e.button === RIGHT_BUTTON) {
                    onCreatePoint(e);
                }
            }}
            onContextMenu={(e) => {
                e.stopPropagation(); e.preventDefault();
            }
            }
        >
            {pieceElems}
            {pieceDots}
        </svg>
        {tooltipedPiece > -1
            ? <GraphTooltip
                pieceIndex={tooltipedPiece}
                {...props}
            />
            : null}

    </div>;
}

function clampX(newP: Point, minX: number | false, maxX: number | false) {
    if (minX === false) {
        return BBLeft;
    }
    if (maxX === false) {
        return BBRight;
    }

    return clamp(newP.x, minX, maxX);
}
