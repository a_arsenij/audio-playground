import React, { ReactElement } from "react";

import { Button } from "../Button/Button";
import { Svg18ChevronDown } from "../GeneratedIcons/18/Chevron/Down";
import { Svg18ChevronUp } from "../GeneratedIcons/18/Chevron/Up";

type AnchorProps = {
    isOpen: boolean,
    onClick: () => void,
    title: string,
    icon?: (arg: object) => ReactElement,
    stretch?: boolean,
}

export function Anchor({
    isOpen,
    onClick,
    title,
    icon,
    stretch,
}: AnchorProps) {
    return <Button
        onClick={onClick}
        title={title}
        stretch={stretch}
        align={"between"}
        showBothIconAndTitle={true}
        iconLeft={icon}
        iconRight={isOpen ? Svg18ChevronUp : Svg18ChevronDown}
    />;
}
