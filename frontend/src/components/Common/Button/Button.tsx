import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { LEFT_BUTTON, RIGHT_BUTTON } from "@audio-playground/lib-frontend/std/MouseButtons";
import React from "react";

import styles from "./Button.module.css";

type ButtonProps = {
    style?: "primary" | "secondary" | "ghost",
    iconLeft?: React.FC<{}>,
    iconRight?: React.FC<{}>,
    title: string,
    showBothIconAndTitle?: boolean,
    disabled?: boolean,
    onClick: (() => void) | undefined,
    onRightClick?: (() => void) | undefined,
    stretch?: boolean,
    align?: "center" | "left" | "between",
}

export function Button({
    style = "secondary",
    disabled = false,
    title,
    iconLeft,
    iconRight,
    showBothIconAndTitle,
    onClick,
    onRightClick = undefined,
    stretch = false,
    align = "center",
}: ButtonProps) {
    const handle = (e: React.MouseEvent<HTMLButtonElement>) => {
        if (e.button === LEFT_BUTTON) {
            onClick && onClick();
        } else if (e.button === RIGHT_BUTTON) {
            if (onRightClick) {
                e.preventDefault();
                e.stopPropagation();
                onRightClick();
            }
        }
    };
    return <button
        title={showBothIconAndTitle ? undefined : title}
        onClick={handle}
        onContextMenu={handle}
        className={clsx(
            styles.button,
            style === "primary" && styles.primary,
            style === "secondary" && styles.secondary,
            style === "ghost" && styles.ghost,
            stretch && styles.fluid,
            align === "center" && styles.alignCenter,
            align === "left" && styles.alignLeft,
            align === "between" && styles.alignBetween,
        )}
        disabled={disabled}
    >
        <div className={styles.leftAndText}>
            { iconLeft && iconLeft({}) }
            {(showBothIconAndTitle || !(iconLeft || iconRight)) && title}
        </div>
        { iconRight && iconRight({}) }
    </button>;
}
