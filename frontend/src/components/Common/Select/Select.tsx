import React, { ReactElement } from "react";

import { Anchor } from "../Anchor/Anchor";
import { Dropdown } from "../Dropdown/Dropdown";
import { DropdownItem } from "../DropdownItem/DropdownItem";

type SelectProps<T> = {
    value: T,
    onChange: (value: T) => void,
    items: readonly T[],
    render: (item: T) => string | {
        title: string,
        description?: string,
        icon?: (arg: object) => ReactElement,
    },
    stretch?: boolean,
}

export function Select<T>({
    value,
    onChange,
    items,
    render,
    stretch = true,
}: SelectProps<T>) {
    const renderedTitle = render(value);
    const title = typeof renderedTitle === "string" ? renderedTitle : renderedTitle.title;
    const icon = typeof renderedTitle === "string" ? undefined : renderedTitle.icon;
    function wrappedRender(item: T): {
        title: string,
        description?: string,
        icon?: React.FC<{}>,
    } {
        const r = render(item);
        if (typeof r === "string") {
            return {
                title: r,
            };
        }
        return r;
    }
    return <Dropdown
        anchor={({ onClick, isOpen }) => <Anchor
            onClick={onClick}
            isOpen={isOpen}
            title={title}
            stretch={stretch}
            icon={icon}
        />}
        content={({ onClose }) => items.map((item) => <DropdownItem
            {...wrappedRender(item)}
            onClick={() => {
                onChange(item);
                onClose();
            }}
            selected={item === value}
        />)}
    />;
}
