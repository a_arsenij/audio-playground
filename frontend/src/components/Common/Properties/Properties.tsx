import { SetState } from "@audio-playground/lib-frontend/std/SetState";
import React from "react";

import { Button } from "../Button/Button";
import { Checkbox } from "../Checkbox/Checkbox";
import { HoriLine } from "../Containers/HoriLine/HoriLine";
import { Svg18CommonReset } from "../GeneratedIcons/18/Common/Reset";
import { NumberInput, TextInput } from "../Input/Input";
import { LabeledContainer } from "../LabeledContainer/LabeledContainer";
import { Select } from "../Select/Select";
import styles from "./Properties.module.css";

type PropertyRootType<VALUE> = {
    defaultValue: VALUE,
}

export type PropertyNumberType = PropertyRootType<number> & {
    type: "number",
}

export type PropertyEnumType = PropertyRootType<string> & {
    type: "enum",
    options: string[],
    getVisibleTitle: (v: string) => string,
}

export type PropertyStringType = PropertyRootType<string> & {
    type: "string",
}

export type PropertyBooleanType = PropertyRootType<boolean> & {
    type: "boolean",
}

export type PropertyType = (
    | PropertyNumberType
    | PropertyEnumType
    | PropertyStringType
    | PropertyBooleanType
);

export type Property = PropertyType & {
    name: string,
    get: (element: any) => any,
    set: (element: any, t: any) => any,
}


function gs<
    T,
    PROPS,
>(
    func: (
        get: (v: any) => T,
        set: (v: any, a: T) => void,
        props: PROPS
    ) => Property,
) {
    return (
        name: string,
        props: PROPS,
    ) => func(
        (v) => v[name],
        (v, a) => {
            const copy = { ...v };
            copy[name] = a;
            return copy;
        },
        props,
    );
}

type NumberProperty = {
    name: string,
    defaultValue?: number,
}

export function numberPropertyGS(
    get: (v: any) => number,
    set: (v: any, a: number) => any,
    arg: NumberProperty,
): Property {
    return {
        type: "number",
        defaultValue: arg.defaultValue ?? 0,
        name: arg.name,
        get,
        set,
    };
}

export const numberPropertyF = gs(numberPropertyGS);


type EnumProperty = {
    name: string,
    options: string[],
    defaultValue?: string,
    getVisibleTitle?: (v: string) => string,
}

export function enumPropertyGS(
    get: (v: any) => string,
    set: (v: any, a: string) => any,
    arg: EnumProperty,
): Property {
    return {
        type: "enum",
        defaultValue: arg.defaultValue ?? arg.options[0]!,
        options: arg.options,
        name: arg.name,
        get,
        set,
        getVisibleTitle: arg.getVisibleTitle ?? ((v: string) => v),
    };
}

export const enumPropertyF = gs(enumPropertyGS);

type StringProperty = {
    name: string,
    defaultValue?: string,
}

export function stringPropertyGS(
    get: (element: any) => string | undefined,
    set: (element: any, t: string) => any,
    arg: StringProperty,
): Property {
    return {
        type: "string",
        defaultValue: arg.defaultValue ?? "",
        name: arg.name,
        get,
        set,
    };
}

export const stringPropertyF = gs(stringPropertyGS);

type BooleanProperty = {
    name: string,
    defaultValue?: boolean,
}

export function booleanPropertyGS(
    get: (element: any) => boolean | undefined,
    set: (element: any, t: boolean) => any,
    arg: BooleanProperty,
): Property {
    return {
        type: "boolean",
        defaultValue: arg.defaultValue ?? false,
        name: arg.name,
        get,
        set,
    };
}

export const booleanPropertyF = gs(booleanPropertyGS);


type PropertyUIProps<PROPERTY extends Property> = {
    property: PROPERTY,
    value: PROPERTY["defaultValue"],
    onChange: (value: PROPERTY["defaultValue"]) => void,
}

function PropertyUI<PROPERTY extends Property>({
    property,
    value,
    onChange,
}: PropertyUIProps<PROPERTY>) {
    switch (property.type) {
        case "number":
            return <NumberInput
                value={value as number}
                onChange={onChange}
            />;
        case "enum":
            return <Select
                items={property.options}
                render={(v: string) => property.getVisibleTitle(v)}
                value={value as string}
                onChange={onChange}
            />;
        case "string":
            return <TextInput
                value={value as string}
                onChange={onChange}
            />;
        case "boolean":
            return <Checkbox
                value={value as boolean}
                setValue={onChange}
            />;
    }
}

type PropertyRowProp<PROPERTY extends Property> = {
    property: PROPERTY,
    value: PROPERTY["defaultValue"],
    onChange: (value: PROPERTY["defaultValue"]) => void,
}

function PropertyRow<PROPERTY extends Property>({
    property,
    value,
    onChange,
}: PropertyRowProp<PROPERTY>) {
    return <LabeledContainer label={property.name}>
        <HoriLine>
            <PropertyUI property={property} value={value} onChange={onChange}/>
            <Button
                onClick={() => onChange(property.defaultValue)}
                iconLeft={Svg18CommonReset}
                title={"Reset property value"}
            />
        </HoriLine>
    </LabeledContainer>;
}

type PropertiesProps<
    ELEMENT extends object,
> = {
    element: ELEMENT,
    setElement: SetState<ELEMENT>,
    properties: Property[],
}

export function Properties<ELEMENT extends object>({
    element,
    setElement,
    properties,
}: PropertiesProps<ELEMENT>) {
    return <div className={styles.properties}>
        {properties.map((property) =>
            <PropertyRow
                key={property.name}
                property={property}
                value={property.get(element) ?? property.defaultValue}
                onChange={(v) => setElement((prevEl) => property.set(prevEl, v))}
            />,
        )}
    </div>;
}
