import React from "react";

import { Anchor } from "../Anchor/Anchor";
import { Dropdown } from "../Dropdown/Dropdown";
import { DropdownItem } from "../DropdownItem/DropdownItem";

type MenuProps = {
    title: string,
    items: {
        icon?: React.FC<{}>,
        title: string,
        description?: string,
        onClick: () => void,
    }[],
    stretch?: boolean,
}

export function Menu({
    title,
    items,
    stretch = true,
}: MenuProps) {
    return <Dropdown
        anchor={({ onClick, isOpen }) => <Anchor
            onClick={onClick}
            isOpen={isOpen}
            title={title}
            stretch={stretch}
        />}
        content={({ onClose }) => items.map((item) => <DropdownItem
            {...item}
            onClick={() => {
                item.onClick();
                onClose();
            }}/>)}
    />;
}
