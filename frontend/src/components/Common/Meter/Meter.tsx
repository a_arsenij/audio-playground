import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React from "react";

import styles from "./Meter.module.css";

type MeterProps = {
    value: number,
    orientation?: "vertical" | "horizontal",
}

export function Meter({
    value,
    orientation = "horizontal",
}: MeterProps) {
    return <div
        className={ clsx(styles.meter, orientation === "vertical" && styles.meterVertical)}
    >
        <div
            style={variables({ "--level": value })}
            className={ clsx(styles.level, orientation === "vertical" && styles.levelVertical) }
        />
    </div>;
}

