import React from "react";

import { CheckboxDropdownItem } from "../DropdownItem/DropdownItem";

type AutomatedCheckboxProps = {
    automated: boolean,
    setAutomated: (newValue: boolean) => void,
}

export function AutomatedCheckbox(props: AutomatedCheckboxProps) {
    return <CheckboxDropdownItem
        checked={props.automated}
        onClick={() => props.setAutomated(!props.automated)}
        title={"Automated"}
    />;
}

type AutomatedKnobCheckboxProps = {
    isAutomated: (name: string) => boolean,
    setIsAutomated: (name: string, isAutomated: boolean) => void,
    knobName: string,
}

export function AutomatedKnobCheckbox(props: AutomatedKnobCheckboxProps) {
    return <AutomatedCheckbox
        automated={props.isAutomated(props.knobName)}
        setAutomated={(v) => props.setIsAutomated(props.knobName, v)}
    />;
}
