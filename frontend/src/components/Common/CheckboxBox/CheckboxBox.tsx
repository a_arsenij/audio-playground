import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import React from "react";

import { Svg18CommonCheck } from "../GeneratedIcons/18/Common/Check";
import styles from "./CheckboxBox.module.css";

type CheckboxBoxProps = {
    checked: boolean,
}

export function CheckboxBox({
    checked,
}: CheckboxBoxProps) {
    return <div className={clsx(styles.checkboxBox, checked && styles.checked)}>
        {checked && <Svg18CommonCheck/>}
    </div>;
}
