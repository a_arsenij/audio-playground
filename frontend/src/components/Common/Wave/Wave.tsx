import { ArrayBufferAllocator } from "@audio-playground/lib-common/std/ArrayBufferAllocator";
import { BlobRef } from "@audio-playground/lib-common/std/BlobRef";
import React, { memo, useMemo } from "react";

import styles from "./Wave.module.css";

type WaveProps = & {
    startSample: number,
    lengthSample: number,
    preview?: boolean,
}
& (
    | {
    allocator: ArrayBufferAllocator,
    blobRef: BlobRef,
}
    | {
    values: number[],
}
    )


function createPreview(
    numbers: number[],
): number[] {
    return [...Array(256)].map((_, idx) => {
        const from = numbers.length * idx / 256;
        const to = numbers.length * (idx + 1) / 256;
        return (idx & 2 ? Math.max : Math.min)(
            ...numbers.slice(from, to), 0,
        );
    });
}

function WaveImpl(props: WaveProps) {

    const {
        values,
        allocator,
        blobRef,
        preview,
        startSample,
        lengthSample,
    } = props as any;
    const wave: number[] = useMemo(() => {
        const tmp = (values ?? Array.from(allocator.readF64(blobRef))).slice(startSample, startSample + lengthSample);
        return preview ? createPreview(tmp) : tmp;
    }, [
        values,
        allocator,
        blobRef,
        preview,
        startSample,
        lengthSample,
    ]);

    return <div className={styles.wave}>
        <svg
            className={styles.svg}
            viewBox={`0 -1 ${wave.length} 2`}
            preserveAspectRatio={"none"}
        >
            <path
                className={styles.path}
                vectorEffect={"non-scaling-stroke"}
                d={`M 0 0 ${wave
                    .map((s, idx) => `L ${idx} ${s}`)
                    .join(" ")}`}
            />
        </svg>
    </div>;
}

export const Wave = memo(WaveImpl);
