import React from "react";

import { Separator } from "../Separator/Separator";
import styles from "./VerticalSplitter.module.css";

type VerticalSplitterProps = {
    top: React.ReactNode,
    bottom: React.ReactNode,
}

export function VerticalSplitter(props: VerticalSplitterProps) {
    if (!props.top) {
        return props.bottom;
    }
    if (!props.bottom) {
        return props.top;
    }
    return <div className={styles.verticalSplitter}>
        <div className={styles.top}>
            {props.top}
        </div>
        <Separator type={"horizontal"}/>
        <div className={styles.bottom}>
            {props.bottom}
        </div>
    </div>;
}
