import { Point } from "@audio-playground/lib-common/math/Point";
import React from "react";

import styles from "./PiecewiseLinearGraph.module.css";

type PiecewiseLinearGraphProps = {
    points: Point[],
    fromX?: number,
    fromY?: number,
    toX?: number,
    toY?: number,
}
export function PiecewiseLinearGraph(props: PiecewiseLinearGraphProps) {
    const { fromX = 0, fromY = 0, toX = 1, toY = 1, points } = props;
    const widthX = toX - fromX;
    const heightY = toY - fromY;
    const viewBox = `${fromX} ${fromY} ${widthX} ${heightY}`;
    const pointsStrings = points.map((p) => `${p.x},${p.y}`);
    const polylinePoints = pointsStrings.join(" ");

    return <svg
        xmlns={"http://www.w3.org/2000/svg"}
        className={styles.svg}
        viewBox={viewBox}
        preserveAspectRatio={"none"}
    >
        <polyline
            points={polylinePoints}
            className={styles.polyline}
            fill="none"
            vectorEffect="non-scaling-stroke"
        />
    </svg>;
}
