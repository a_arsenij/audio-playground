import React, { ReactNode, useCallback, useEffect, useRef, useState } from "react";

import styles from "./Dropdown.module.css";

export type DropdownProps = {
    anchor: (props: {isOpen: boolean; onClick: () => void}) => ReactNode,
    content: (props: {onClose: () => void}) => ReactNode,
}

export function Dropdown({
    anchor,
    content,
}: DropdownProps) {

    const [isOpen, setIsOpen] = useState(false);

    function onToggle() {
        setIsOpen((prev) => !prev);
    }
    const onClose = useCallback(() => {
        setIsOpen(false);
    }, []);
    const ref = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const handle = (e: MouseEvent) => {
            const dd = ref.current!;
            if (!dd.contains(e.target as Element)) {
                onClose();
            }
        };
        window.addEventListener("click", handle);
        return () => window.removeEventListener("click", handle);
    }, [onClose]);

    return <div className={styles.dropdown} ref={ref}>
        {anchor({ isOpen, onClick: onToggle })}
        {
            isOpen
                ? <div className={styles.list}>
                    {content({ onClose })}
                </div>
                : null
        }
    </div>;
}
