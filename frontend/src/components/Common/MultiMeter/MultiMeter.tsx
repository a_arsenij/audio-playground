import { clsx } from "@audio-playground/lib-frontend/std/Clsx";
import { variables } from "@audio-playground/lib-frontend/std/CssVariables";
import React from "react";

import { Meter } from "../Meter/Meter";
import styles from "./MultiMeter.module.css";

type StereoMeterProps = {
    value: number[],
    minDb?: number,
    maxDb?: number,
    marksIntervalDb?: number,
    orientation?: "horizontal" | "vertical",
    alignMarks?: "start" | "end",
}
function normilize(value: number, min: number, max: number) {
    return (value - min) / (max - min);
}
function getMarks(min: number, max: number, interval: number, align: "start" | "end" = "start") {
    const range = max - min;

    const items = [...Array(Math.round(range / interval))].map((_, i) => {
        switch (align) {
            case "start": return min + interval * i;
            case "end" : return min + interval * (i + 1);
        }

    });

    return items;

}
export function MultiMeter({
    value,
    minDb = -60,
    maxDb = 0,
    marksIntervalDb = 10,
    orientation = "horizontal",
    alignMarks = "start",
}: StereoMeterProps) {

    const range = maxDb - minDb;

    const isVertical = orientation === "vertical";

    const meters = value.map((v, i) => {
        const val = (v - minDb) / range;
        return <Meter key={i} value={val} orientation={orientation}/>;
    });
    return <div className={clsx(styles.multiMeter, isVertical && styles.multiMeterVertical)}>
        <div className={clsx(styles.meters, isVertical && styles.metersVertical)}>
            {meters}
        </div>
        <div
            className={clsx(styles.marks, isVertical && styles.marksVertical)}
            style={variables({
                "--meterRange": range,
            })}
        >
            {
                getMarks(minDb, maxDb, marksIntervalDb, alignMarks).map((db) => {
                    return <div
                        key={db}
                        className={clsx(styles.mark, isVertical && styles.markVertical, alignMarks === "start" ? styles.alignStart : styles.alignEnd)}
                        style={variables({
                            "--pos": normilize(db, minDb, maxDb),
                        })}
                    >
                        <div className={styles.markLabel}>
                            {db}
                        </div>
                    </div>
                    ;
                },
                )
            }
        </div>
    </div>;
}
