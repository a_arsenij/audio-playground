import {
    SetMasterBufferMessage,
    UiToDumbWorkletMessage,
} from "../types/Messages";
import { handle } from "../utils/OnPort";

export const AUDIO_WORKER_NAME = "Cirno is no baka!!";

export function initDumbWorklet() {

    class Renderer extends AudioWorkletProcessor {

        private i32: Int32Array | null = null;
        private f32: Float32Array | null = null;
        private prevBuffer: Float32Array | null = null;
        private bufferLength: number = 0;
        private sampleRate: number = 0;

        private handleSetMasterBuffer(msg: SetMasterBufferMessage) {
            this.i32 = new Int32Array(
                msg.masterBuffer,
                0,
            );
            this.f32 = new Float32Array(
                msg.masterBuffer,
                8,
            );
            this.prevBuffer = new Float32Array(
                msg.bufferLength * 2,
            );
            this.bufferLength = msg.bufferLength;
            this.sampleRate = msg.sampleRate;
        }

        constructor() {
            super();
            handle<UiToDumbWorkletMessage>(
                this.port,
                {
                    setMasterBuffer: (msg) => this.handleSetMasterBuffer(msg),
                },
            );
        }

        process(
            _: Float32Array[][],
            outputs: Float32Array[][],
        ) {

            const i32 = this.i32;
            const f32 = this.f32;
            const prev = this.prevBuffer;
            if (!i32 || !f32 || !prev) {
                return true;
            }
            const bufferLength = this.bufferLength;

            const output = outputs[0]!;

            const left = output[0]!;
            const right = output[1]!;

            for (let i = 0; i < bufferLength; i++) {
                left[i]! = prev[i * 2]!;
                right[i]! = prev[i * 2 + 1]!;
            }


            i32[0] = 1;
            Atomics.notify(i32, 0);
            const startedAt = Date.now();
            while (true) {
                const value = i32[1];
                if (value !== 0) {
                    break;
                }
                const now = Date.now();
                if (now - startedAt > 1000 * bufferLength / this.sampleRate) {
                    break;
                }
            }

            i32[1] = 0;
            for (let i = 0; i < f32.length; i++) {
                prev[i] = f32[i]!;
            }

            return true;
        }
    }

    registerProcessor(AUDIO_WORKER_NAME, Renderer);
}
