import { acquireArrayFromPool, PooledArray } from "@audio-playground/lib-common/std/ArraysPool";
import { createCompletablePromise } from "@audio-playground/lib-common/std/CompletablePromise";

import { createEngineState } from "../engine/EngineState";
import { halt } from "../engine/Halt";
import { renderMaster } from "../engine/render/RenderMaster";
import { MasterToUiMessage, UiToMasterMessage } from "../types/Messages";
import { handle } from "../utils/OnPort";


export function initMaster() {

    const me = self as DedicatedWorkerGlobalScope;

    const state = createEngineState();
    const BUFFER_LENGTH = state.project.get().bufferLength;

    let lastAction: "play" | "stop" = "stop";

    function play() {
        lastAction = "play";
        state.playingStartedAtSamples.set(
            state.timeSamples.get() + BUFFER_LENGTH,
        );
    }

    function stop() {
        state.playingStartedAtSamples.set(undefined);
        if (lastAction === "stop") {
            halt(state);
        }
        lastAction = "stop";
    }

    state.currentTimeBars.subscribe((v) => {
        if (v === undefined) {
            sendToUi({ type: "bufferRendered", timeBars: undefined, channelsLevels: [] });
        }
    });

    const buffer: [PooledArray, PooledArray] = [
        acquireArrayFromPool(BUFFER_LENGTH),
        acquireArrayFromPool(BUFFER_LENGTH),
    ];

    function sendToUi(event: MasterToUiMessage) {
        me.postMessage(event);
    }

    function onKeyPress(key: number) {
        /* state.pressedKeys.push({
            key,
            id: generateId(),
            startedAtSample: state.timeSamples.get() + BUFFER_LENGTH,
            endedAtSample: undefined,
        }); */
    }

    function onKeyRelease(key: number) {
        /* while (true) {
            const idx = state.pressedKeys
                .findIndex((k) => k.key === key && k.endedAtSample === undefined);
            if (idx === -1) {
                return;
            }
            state.pressedKeys.splice(idx, 1);
        } */
    }

    const p = createCompletablePromise<[Int32Array, Float32Array]>();

    handle<UiToMasterMessage>(
        me,
        {
            play,
            stop,
            setLoop: (msg) => state.loop.set(msg.loop),
            setSharedMemory: (msg) => state.sharedBuffer.set(msg.memory),
            setProject: (msg) => state.setProject(msg.project),
            pressKey: (msg) => onKeyPress(msg.key),
            releaseKey: (msg) => onKeyRelease(msg.key),
            setMasterBufferOnly: (msg) => {
                p.resolve([
                    new Int32Array(
                        msg.masterBuffer,
                        0,
                    ),
                    new Float32Array(
                        msg.masterBuffer,
                        8,
                    ),
                ]);
            },
        },
    );
    sendToUi({ type: "iAmReady" });
    p.then(async([i32, f32]) => {

        while (true) {
            await Atomics.waitAsync(i32, 0, 0).value;
            i32[0] = 0;

            const [everyLevelIsZero, channelsLevels] = renderMaster(
                state,
                state.sharedBuffer.get(),
                buffer,
            );
            sendToUi({
                type: "bufferRendered",
                timeBars: state.currentTimeBars.get(),
                channelsLevels: everyLevelIsZero
                    ? []
                    : channelsLevels,
            });
            for (let i = 0; i < BUFFER_LENGTH; i++) {
                f32[i * 2] = buffer[0][i]!;
                f32[i * 2 + 1] = buffer[1][i]!;
            }
            i32[1] = 1;
            Atomics.notify(i32, 1);
        }

    });

}
