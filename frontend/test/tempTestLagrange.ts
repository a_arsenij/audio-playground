import { describe, it } from "node:test";

import { getLagrangePolynomialCoeffs } from "../../tools/polynomizer/lagrange";
import {assertArraysAreClose} from "@audio-playground/lib-test-utils/assertArraysAreClose";


describe("Lagrange", () => {

    it("y = x", () => {
        const points: [number, number][] = [
            [0, 0],
            [1, 1],
        ];
        const expected = [0, 1];
        const poly = getLagrangePolynomialCoeffs(points).reverse();
        assertArraysAreClose(poly, expected);
    });
    it("y = 1 + 2x", () => {
        const points: [number, number][] = [
            [0, 1],
            [1, 3],
        ];
        const expected = [1, 2];
        const poly = getLagrangePolynomialCoeffs(points).reverse();
        assertArraysAreClose(poly, expected);
    });
    it("y = 2", () => {
        const points: [number, number][] = [
            [2, 2],
            [3, 2],
        ];
        const expected = [2, 0];
        const poly = getLagrangePolynomialCoeffs(points).reverse();
        assertArraysAreClose(poly, expected);
    });
    it("y = x^2 ", () => {
        const points: [number, number][] = [
            [0, 0],
            [1, 1],
            [2, 4],
        ];
        const expected = [0, 0, 1];
        const poly = getLagrangePolynomialCoeffs(points).reverse();
        assertArraysAreClose(poly, expected);
    });
    it("y = 1 + 2x + 3x^2 + 4x^3", () => {
        const points: [number, number][] = [
            [0, 1],
            [1, 10],
            [0o10, 0o4321],
            [10, 4321],
        ];
        const expected = [1, 2, 3, 4];
        const poly = getLagrangePolynomialCoeffs(points).reverse();
        assertArraysAreClose(poly, expected);
    });
    it("y = -13", () => {
        const points: [number, number][] = [
            [0, -13],
        ];
        const expected = [-13];
        const poly = getLagrangePolynomialCoeffs(points).reverse();
        assertArraysAreClose(poly, expected);
    });
    it("y = -13", () => {
        const points: [number, number][] = [
            [-1, -13],
        ];
        const expected = [-13];
        const poly = getLagrangePolynomialCoeffs(points).reverse();
        assertArraysAreClose(poly, expected);
    });
    it("y = -13 + x", () => {
        const points: [number, number][] = [
            [-1, -14],
            [1, -12],
        ];
        const expected = [-13, 1];
        const poly = getLagrangePolynomialCoeffs(points).reverse();
        assertArraysAreClose(poly, expected);
    });
    it("y = x^3", () => {
        const points: [number, number][] = [
            [-1, -1],
            [1, 1],
            [2, 8],
            [3, 27],
        ];
        const expected = [0, 0, 0, 1];
        const poly = getLagrangePolynomialCoeffs(points).reverse();
        assertArraysAreClose(poly, expected);
    });

    it("y = -13 + x - 2 * x^3", () => {
        const points: [number, number][] = [
            [-1, -12],
            [1, -14],
            [2, -27],
            [3, -64],
        ];
        const expected = [-13, 1, 0, -2];
        const poly = getLagrangePolynomialCoeffs(points).reverse();
        assertArraysAreClose(poly, expected);
    });
});

