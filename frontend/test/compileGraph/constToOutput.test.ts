import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import assert from "assert";

import { createConnection } from "../../src/components/Common/Gridy/NodeEditor/Utils";
import { createConstNode, WholeReverbGraph } from "../../src/plugins/list/Reverb/ReverbNodes";
import { compileToBoth, DEFAULT_CHECKBOXES, inputNode, outputNode } from "./_utils";

describe("Compile graph", () => {
    beforeEach(monkeyPatch);
    it("Const to output", async() => {

        await (async() => {
            const constNode = createConstNode(0);

            const graph: WholeReverbGraph = {
                main: {
                    nodes: [
                        inputNode,
                        constNode,
                        outputNode,
                    ],
                    connections: [
                        createConnection(
                            constNode.outputs[0].id,
                            outputNode.inputs[0]!.id,
                        ),
                        createConnection(
                            constNode.outputs[0].id,
                            outputNode.inputs[1]!.id,
                        ),
                    ],
                },
                macros: [],
            };
            const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);

            for (const r of ["js", "wasm"] as const) {
                assert.deepEqual(
                    compiled[r].run([], 0),
                    [0, 0],
                    r,
                );
            }

        })();

        await (async() => {
            const constNode = createConstNode(1);

            const graph: WholeReverbGraph = {
                main: {
                    nodes: [
                        inputNode,
                        constNode,
                        outputNode,
                    ],
                    connections: [
                        createConnection(
                            constNode.outputs[0].id,
                            outputNode.inputs[0]!.id,
                        ),
                        createConnection(
                            constNode.outputs[0].id,
                            outputNode.inputs[1]!.id,
                        ),
                    ],
                },
                macros: [],
            };
            const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);

            for (const r of ["js", "wasm"] as const) {
                assert.deepEqual(
                    compiled[r].run([], 0),
                    [1, 1],
                    `${r}: Const 1`,
                );
            }

        })();


        await (async() => {
            const constNodeL = createConstNode(0.2);
            const constNodeR = createConstNode(0.3);

            const graph: WholeReverbGraph = {
                main: {
                    nodes: [
                        inputNode,
                        constNodeL,
                        constNodeR,
                        outputNode,
                    ],
                    connections: [
                        createConnection(
                            constNodeL.outputs[0].id,
                            outputNode.inputs[0]!.id,
                        ),
                        createConnection(
                            constNodeR.outputs[0].id,
                            outputNode.inputs[1]!.id,
                        ),
                    ],
                },
                macros: [],
            };
            const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);


            for (const r of ["js", "wasm"] as const) {
                assert.deepEqual(
                    compiled[r].run([], 0),
                    [0.2, 0.3],
                    `${r}: Const 0.2 0.3`,
                );
            }
        })();

    });
});
