import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { createConnection } from "../../src/components/Common/Gridy/NodeEditor/Utils";
import { createMathOpNode, WholeReverbGraph } from "../../src/plugins/list/Reverb/ReverbNodes";
import { assertStereo, compileToBoth, DEFAULT_CHECKBOXES, inputNode, outputNode } from "./_utils";


describe("Compile graph", () => {
    beforeEach(monkeyPatch);
    it("Multiply LR", async() => {

        const multiplyNode = createMathOpNode("multiply");

        const graph: WholeReverbGraph = {
            main: {
                nodes: [
                    inputNode,
                    multiplyNode,
                    outputNode,
                ],
                connections: [
                    createConnection(
                        inputNode.outputs[0]!.id,
                        multiplyNode.inputs[0].id,
                    ),
                    createConnection(
                        inputNode.outputs[1]!.id,
                        multiplyNode.inputs[1].id,
                    ),
                    createConnection(
                        multiplyNode.outputs[0].id,
                        outputNode.inputs[0]!.id,
                    ),
                    createConnection(
                        multiplyNode.outputs[0].id,
                        outputNode.inputs[1]!.id,
                    ),
                ],
            },
            macros: [],
        };
        const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);


        for (const r of ["js", "wasm"] as const) {
            assertStereo(compiled[r].run([0, 0], 0), [0, 0], `${r}: Multiply LR 0 0`);
            assertStereo(compiled[r].run([1, 1], 0), [1, 1], `${r}: Multiply LR 1 1`);
            assertStereo(compiled[r].run([0.2, 0.2], 0), [0.04, 0.04], `${r}: Multiply LR 0.2 0.2`);
            assertStereo(compiled[r].run([0.5, 0.4], 0), [0.2, 0.2], `${r}: Multiply LR 0.5 0.4`);
            assertStereo(compiled[r].run([1, 0], 0), [0, 0], `${r}: Multiply LR 1 0`);
            assertStereo(compiled[r].run([0, 1], 0), [0, 0], `${r}: Multiply LR 0 1`);
        }
    });
});
