import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { createConnection } from "../../src/components/Common/Gridy/NodeEditor/Utils";
import { WholeReverbGraph } from "../../src/plugins/list/Reverb/ReverbNodes";
import { assertStereo, compileToBoth, DEFAULT_CHECKBOXES, inputNode, outputNode } from "./_utils";

describe("Compile graph", () => {
    beforeEach(monkeyPatch);
    it("Direct PassThrough", async() => {

        const graph: WholeReverbGraph = {
            main: {
                nodes: [
                    inputNode,
                    outputNode,
                ],
                connections: [
                    createConnection(
                        inputNode.outputs[0]!.id,
                        outputNode.inputs[0]!.id,
                    ),
                    createConnection(
                        inputNode.outputs[1]!.id,
                        outputNode.inputs[1]!.id,
                    ),
                ],
            },
            macros: [],
        };
        const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);
        const data = [
            [0, 0],
            [0.5, 0.5],
            [1, 1],
            [0, 1],
            [1, 0],

        ];

        for (const r of ["js", "wasm"] as const) {
            data.forEach((d) => {
                assertStereo(
                    compiled[r].run(d, 0),
                    d as [number, number],
                    `Passthrough ${d}`,
                );
            });
        }

    });
});
