import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import assert from "assert";

import { createConnection } from "../../src/components/Common/Gridy/NodeEditor/Utils";
import {
    createConstNode,
    createDelayNode, createInputNode, createReverbMainInputKnobNodePort,
    WholeReverbGraph,
} from "../../src/plugins/list/Reverb/ReverbNodes";
import { compileToBoth, DEFAULT_CHECKBOXES, outputNode } from "./_utils";
import {assertNumbersClose} from "@audio-playground/lib-test-utils/assertNumbersClose";

const inputNode = createInputNode(0, 0, { keyPort: false });

const data = [
    1, 1,
    0.2, 0.5,
    0, 0,
    0.1, 0.3,
];

describe("Compile graph", () => {
    beforeEach(monkeyPatch);
    it("Delay 1 sample", async() => {

        const constNode = createConstNode(1);
        const delayNode = createDelayNode();

        const graph: WholeReverbGraph = {
            main: {
                nodes: [
                    inputNode,
                    constNode,
                    delayNode,
                    outputNode,
                ],
                connections: [
                    createConnection(
                        inputNode.outputs[0]!.id,
                        delayNode.inputs[0].id,
                    ),
                    createConnection(
                        constNode.outputs[0].id,
                        delayNode.inputs[1].id,
                    ),
                    createConnection(
                        delayNode.outputs[0].id,
                        outputNode.inputs[0]!.id,
                    ),
                ],
            },
            macros: [],
        };
        const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 8);

        const input = data.flatMap((v) => [v, 0]);

        for (const r of ["js", "wasm"] as const) {

            const out = compiled[r].run(input, 0);


            const res = out.filter((_v, i) => i % 2 === 0);

            res.forEach((v, idx) => {
                if (idx < 1) {
                    return;
                }
                assertNumbersClose(
                    v,
                    data[idx - 1]!,
                    Number.EPSILON * 10,
                    `result: ${out} ; input: ${data}`,
                );
            });
        }

    });

    it("Delay 2 samples", async() => {

        const constNode = createConstNode(2);
        const delayNode = createDelayNode();

        const graph: WholeReverbGraph = {
            main: {
                nodes: [
                    inputNode,
                    constNode,
                    delayNode,
                    outputNode,
                ],
                connections: [
                    createConnection(
                        inputNode.outputs[0]!.id,
                        delayNode.inputs[0].id,
                    ),
                    createConnection(
                        constNode.outputs[0].id,
                        delayNode.inputs[1].id,
                    ),
                    createConnection(
                        delayNode.outputs[0].id,
                        outputNode.inputs[0]!.id,
                    ),
                ],
            },
            macros: [],
        };
        const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 8);
        const input = data.flatMap((v) => [v, 0]);

        for (const r of ["js", "wasm"] as const) {
            const out = compiled[r].run(input, 0);


            const res = out.filter((_v, i) => i % 2 === 0);

            res.forEach((v, idx) => {
                if (idx < 2) {
                    return;
                }
                assertNumbersClose(
                    v,
                    data[idx - 2]!,
                );
            });
        }

    });

    it("Delay 3 samples", async() => {

        const constNode = createConstNode(3);
        const delayNode = createDelayNode();

        const graph: WholeReverbGraph = {
            main: {
                nodes: [
                    inputNode,
                    constNode,
                    delayNode,
                    outputNode,
                ],
                connections: [
                    createConnection(
                        inputNode.outputs[0]!.id,
                        delayNode.inputs[0].id,
                    ),
                    createConnection(
                        constNode.outputs[0].id,
                        delayNode.inputs[1].id,
                    ),
                    createConnection(
                        delayNode.outputs[0].id,
                        outputNode.inputs[0]!.id,
                    ),
                ],
            },
            macros: [],
        };
        const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 8);

        const input = data.flatMap((v) => [v, 0]);

        for (const r of ["js", "wasm"] as const) {
            const out = compiled[r].run(input, 0);


            const res = out.filter((_v, i) => i % 2 === 0);

            res.forEach((v, idx) => {
                if (idx < 3) {
                    return;
                }
                assertNumbersClose(
                    v,
                    data[idx - 3]!,
                );
            });
        }

    });

    it("Knob -> Delay", async() => {

        const timePort = createReverbMainInputKnobNodePort("time", 1, 10);
        const delayNode = createDelayNode();


        const inputNode = createInputNode(0, 0, { keyPort: false });
        inputNode.outputs.push(timePort);

        const graph: WholeReverbGraph = {
            main: {
                nodes: [
                    inputNode,
                    delayNode,
                    outputNode,
                ],
                connections: [
                    createConnection(
                        inputNode.outputs[0]!.id,
                        delayNode.inputs[0].id,
                    ),
                    createConnection(
                        timePort.id,
                        delayNode.inputs[1].id,
                    ),
                    createConnection(
                        delayNode.outputs[0].id,
                        outputNode.inputs[0]!.id,
                    ),
                ],
            },
            macros: [],
        };
        const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 8);

        for (const r of ["js", "wasm"] as const) {
            assert.deepEqual(
                compiled[r].run([
                    0, 0, 1,
                    1, 0, 1,
                    2, 0, 1,
                    3, 0, 1,
                    4, 0, 1,
                    5, 0, 1,
                    6, 0, 1,
                    7, 0, 1,
                ], 0),
                [
                    0, 0,
                    0, 0,
                    1, 0,
                    2, 0,
                    3, 0,
                    4, 0,
                    5, 0,
                    6, 0,
                ],
                `compiled To ${r}, delay 1`,
            );

            assert.deepEqual(
                compiled[r].run([
                    8, 0, 2,
                    9, 0, 2,
                    10, 0, 2,
                    11, 0, 2,
                    12, 0, 2,
                    13, 0, 2,
                    14, 0, 2,
                    15, 0, 2,
                ], 8),
                [
                    6, 0,
                    7, 0,
                    8, 0,
                    9, 0,
                    10, 0,
                    11, 0,
                    12, 0,
                    13, 0,
                ],
                `compiled To ${r}, delay 2`,
            );

            assert.deepEqual(
                compiled[r].run([
                    16, 0, 3,
                    17, 0, 3,
                    18, 0, 3,
                    19, 0, 3,
                    20, 0, 3,
                    21, 0, 3,
                    22, 0, 3,
                    23, 0, 3,
                ], 16),
                [
                    13, 0,
                    14, 0,
                    15, 0,
                    16, 0,
                    17, 0,
                    18, 0,
                    19, 0,
                    20, 0,
                ],
                `compiled To ${r}, delay 3`,
            );


            {
                const actual = compiled[r].run([
                    24, 0, 1,
                    25, 0, 1,
                    26, 0, 1,
                    27, 0, 1,
                    28, 0, 1,
                    29, 0, 1,
                    30, 0, 1,
                    31, 0, 1,
                ], 24);
                const expected = [
                    23, 0,
                    24, 0,
                    25, 0,
                    26, 0,
                    27, 0,
                    28, 0,
                    29, 0,
                    30, 0,
                ];
                assert.deepEqual(
                    actual,
                    expected,
                    `compiled To ${r}, delay back to 1; expected: ${expected} ; actual: :${actual}`,
                );
            }
        }
    });

});
