import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { createConnection } from "../../src/components/Common/Gridy/NodeEditor/Utils";
import {
    createConstNode,
    createExprNode, createInputNode, createReverbMainInputKnobNodePort,
    numberPort,
    WholeReverbGraph,
} from "../../src/plugins/list/Reverb/ReverbNodes";
import { assertStereo, compileToBoth, outputNode } from "./_utils";


const inputNode = createInputNode(0, 0, { keyPort: false });
const b = createReverbMainInputKnobNodePort("b", -1000, 1000);
inputNode.outputs.push(b);

describe("Compile graph", () => {
    beforeEach(monkeyPatch);
    it("Expr", async() => {

        const a = createConstNode(2);

        const expr = createExprNode();
        expr.expr = "(_0 + _1) * (_2 / _3)";
        expr.inputs = [...Array(4)].map((_, idx) => numberPort("expr.variable", { variable: `_${idx}` }));

        const graph: WholeReverbGraph = {
            main: {
                nodes: [
                    inputNode,
                    outputNode,
                    a,
                    expr,
                ],
                connections: [
                    createConnection(inputNode.outputs[0]!.id, expr.inputs[0]!.id),
                    createConnection(inputNode.outputs[1]!.id, expr.inputs[1]!.id),
                    createConnection(a.outputs[0].id, expr.inputs[2]!.id),
                    createConnection(b.id, expr.inputs[3]!.id),
                    createConnection(expr.outputs[0].id, outputNode.inputs[0]!.id),
                ],
            },
            macros: [],
        };


        const compiled = await compileToBoth(
            graph,
            {},
            44100,
            1,
        );


        for (const r of ["js", "wasm"] as const) {
            assertStereo(
                compiled[r].run([0, 1, 3], 0),
                [(0 + 1) * (2 / 3), 0],
                "(0 + 1) * (2 / 3) = 0.(6)",
            );

            assertStereo(
                compiled[r].run([4, 5, 7], 0),
                [(4 + 5) * (2 / 7), 0],
                "(4 + 5) * (2 / 7) = 2.57142857142857142857",
            );
        }




    });
});
