import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { BiquadFilter, butterworthHighPass, butterworthLowPass } from "@audio-playground/lib-sound/IIR";

import { createConnection } from "../../../src/components/Common/Gridy/NodeEditor/Utils";
import { DEFAULT_SAMPLE_RATE } from "../../../src/engine/DefaultProject";
import { MACROS_STD } from "../../../src/plugins/list/Reverb/macroStd/macroStd";
import {
    createConstNode,
    createMacroUsageNode, getMacro,
    ReverbMacro,
    updateMacroUsagePortsAccordingToMacro, WholeReverbGraph,
} from "../../../src/plugins/list/Reverb/ReverbNodes";
import { assertStereo, compileToBoth, DEFAULT_CHECKBOXES, inputNode, outputNode } from "../_utils";

const SAMPLE_RATE = DEFAULT_SAMPLE_RATE;

describe("Compile graph", () => {
    beforeEach(monkeyPatch);

    const epsi = 1e-14;
    const freqs = [
        100,
        500,
        1000,
        2500,
        5000,
        10000,
    ];

    function testFilters(
        name: string,
        coeffs: (freq: number) => BiquadFilter,
        filterId: string,
    ) {
        for (const freq of freqs) {
            it(`${name}@${freq}hz`, async() => {
                const c = coeffs(freq);

                const constNode = createConstNode(freq);
                const filterNode = createMacroUsageNode(filterId);
                const macro = getMacro(filterId, new Map<string, ReverbMacro>())!;
                updateMacroUsagePortsAccordingToMacro(filterNode, macro);

                await (async() => {
                    // a1, a2
                    const graph: WholeReverbGraph = {
                        main: {
                            nodes: [
                                inputNode,
                                constNode,
                                filterNode,
                                outputNode,
                            ],
                            connections: [
                                createConnection(
                                    constNode.outputs[0].id,
                                    filterNode.inputs[0]!.id,
                                ),
                                createConnection(
                                    filterNode.outputs[0]!.id,
                                    outputNode.inputs[0]!.id,
                                ),
                                createConnection(
                                    filterNode.outputs[1]!.id,
                                    outputNode.inputs[1]!.id,
                                ),
                            ],
                        },
                        macros: MACROS_STD.values().toArray(),
                    };
                    const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);

                    for (const r of ["js", "wasm"] as const) {
                        assertStereo(
                            compiled[r].run([0, 0], 0),
                            [c.a1, c.a2],
                            `${r}: a1, a2 of ${name}@${freq}hz`,
                            epsi,
                        );
                    }
                })();

                await (async() => {
                    // b0, b1
                    const graph: WholeReverbGraph = {
                        main: {
                            nodes: [
                                inputNode,
                                constNode,
                                filterNode,
                                outputNode,
                            ],
                            connections: [
                                createConnection(
                                    constNode.outputs[0].id,
                                    filterNode.inputs[0]!.id,
                                ),
                                createConnection(
                                    filterNode.outputs[2]!.id,
                                    outputNode.inputs[0]!.id,
                                ),
                                createConnection(
                                    filterNode.outputs[3]!.id,
                                    outputNode.inputs[1]!.id,
                                ),
                            ],
                        },
                        macros: MACROS_STD.values().toArray(),
                    };
                    const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);

                    for (const r of ["js", "wasm"] as const) {
                        assertStereo(
                            compiled[r].run([0, 0], 0),
                            [c.b0, c.b1],
                            `${r}: b0, b1 of ${name}@${freq}hz`,
                            epsi,
                        );
                    }
                })();

                await (async() => {
                    // b2
                    const graph: WholeReverbGraph = {
                        main: {
                            nodes: [
                                inputNode,
                                constNode,
                                filterNode,
                                outputNode,
                            ],
                            connections: [
                                createConnection(
                                    constNode.outputs[0].id,
                                    filterNode.inputs[0]!.id,
                                ),
                                createConnection(
                                    filterNode.outputs[4]!.id,
                                    outputNode.inputs[0]!.id,
                                ),
                            ],
                        },
                        macros: MACROS_STD.values().toArray(),
                    };
                    const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);

                    for (const r of ["js", "wasm"] as const) {
                        assertStereo(
                            compiled[r].run([0, 0], 0),
                            [c.b2, 0],
                            `${r}: b2 of ${name}@${freq}hz`,
                            epsi,
                        );
                    }
                })();

            });
        }
    }

    testFilters(
        "Butterworth Low Pass",
        (freq) => butterworthLowPass(freq, SAMPLE_RATE),
        "std::musical::ButterworthLowPassCoefficients",
    );
    testFilters(
        "Butterworth High Pass",
        (freq) => butterworthHighPass(freq, SAMPLE_RATE),
        "std::musical::ButterworthHighPassCoefficients",
    );


});
