
import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { createConnection } from "../../../src/components/Common/Gridy/NodeEditor/Utils";
import { MACROS_STD } from "../../../src/plugins/list/Reverb/macroStd/macroStd";
import {
    createInputNode,
    createMacroUsageNode,
    getMacro,
    ReverbMacro,
    updateMacroUsagePortsAccordingToMacro,
    WholeReverbGraph,
} from "../../../src/plugins/list/Reverb/ReverbNodes";
import { compileToBoth, DEFAULT_CHECKBOXES, outputNode } from "../_utils";
import {assertArraysAreClose} from "@audio-playground/lib-test-utils/assertArraysAreClose";

const inputNode = createInputNode(0, 0, { keyPort: false });

describe("Compile graph", () => {
    beforeEach(monkeyPatch);

    it("Trigger", async() => {

        const macroId = "std::conditions::trigger";

        const interNode = createMacroUsageNode(macroId);
        const macro = getMacro(macroId, new Map<string, ReverbMacro>())!;
        updateMacroUsagePortsAccordingToMacro(interNode, macro);

        const graph: WholeReverbGraph = {
            main: {
                nodes: [
                    inputNode,
                    interNode,
                    outputNode,
                ],
                connections: [
                    createConnection(
                        inputNode.outputs[0]!.id,
                        interNode.inputs[0]!.id,
                    ),
                    createConnection(
                        inputNode.outputs[1]!.id,
                        interNode.inputs[1]!.id,
                    ),
                    createConnection(
                            interNode.outputs[0]!.id,
                            outputNode.inputs[0]!.id,
                    ),
                ],
            },
            macros: MACROS_STD.values().toArray(),
        };
        const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 8);

        for (const r of ["js", "wasm"] as const) {
            assertArraysAreClose(
                compiled[r].run(
                    [
                        0, 0,
                        1, 0,
                        2, 1,
                        3, 0,
                        4, 0,
                        5, 1,
                        6, 0,
                        7, 0,
                    ],
                    0,
                ),
                [
                    0, 0,
                    0, 0,
                    2, 0,
                    2, 0,
                    2, 0,
                    5, 0,
                    5, 0,
                    5, 0,
                ],
            );
        }

    });


});
