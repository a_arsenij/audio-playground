
import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { createConnection } from "../../../src/components/Common/Gridy/NodeEditor/Utils";
import { MACROS_STD } from "../../../src/plugins/list/Reverb/macroStd/macroStd";
import {
    createInputNode,
    createMacroUsageNode,
    createReverbMainInputKnobNodePort,
    getMacro,
    ReverbMacro,
    updateMacroUsagePortsAccordingToMacro,
    WholeReverbGraph,
} from "../../../src/plugins/list/Reverb/ReverbNodes";
import { compileToBoth, DEFAULT_CHECKBOXES, outputNode } from "../_utils";
import {assertNumbersClose} from "@audio-playground/lib-test-utils/assertNumbersClose";

export const inputNode = createInputNode(0, 0);
const knobPort = createReverbMainInputKnobNodePort(
    "knob",
    0, 1,
);
inputNode.outputs.push(knobPort);

describe("Compile graph", () => {
    beforeEach(monkeyPatch);

    it("Linear interpolate", async() => {

        const macroId = "std::conditions::linearInterpolate";

        const interNode = createMacroUsageNode(macroId);
        const macro = getMacro(macroId, new Map<string, ReverbMacro>())!;
        updateMacroUsagePortsAccordingToMacro(interNode, macro);

        const graph: WholeReverbGraph = {
            main: {
                nodes: [
                    inputNode,
                    interNode,
                    outputNode,
                ],
                connections: [
                    createConnection(
                        inputNode.outputs[2]!.id,
                        interNode.inputs[0]!.id,
                    ),
                    createConnection(
                        inputNode.outputs[0]!.id,
                        interNode.inputs[1]!.id,
                    ),
                    createConnection(
                        inputNode.outputs[1]!.id,
                        interNode.inputs[2]!.id,
                    ),
                    createConnection(
                            interNode.outputs[0]!.id,
                            outputNode.inputs[0]!.id,
                    ),
                ],
            },
            macros: MACROS_STD.values().toArray(),
        };
        const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);

        for (const r of ["js", "wasm"] as const) {
            assertNumbersClose(
                compiled[r].run([10, 20, 0], 0)[0]!,
                10,
            );

            assertNumbersClose(
                compiled[r].run([10, 20, 0.25], 0)[0]!,
                12.5,
            );

            assertNumbersClose(
                compiled[r].run([10, 20, 0.5], 0)[0]!,
                15,
            );

            assertNumbersClose(
                compiled[r].run([10, 20, 0.75], 0)[0]!,
                17.5,
            );

            assertNumbersClose(
                compiled[r].run([10, 20, 1], 0)[0]!,
                20,
            );
        }

    });


});
