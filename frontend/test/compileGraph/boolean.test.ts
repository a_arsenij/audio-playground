import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { createConnection } from "../../src/components/Common/Gridy/NodeEditor/Utils";
import {
    createGateOpNode, createInputNode,
    createMathOpNode, createReverbMainInputCheckboxNodePort,
    WholeReverbGraph,
} from "../../src/plugins/list/Reverb/ReverbNodes";
import { assertStereo, compileToBoth, outputNode } from "./_utils";

describe("Compile graph", () => {
    beforeEach(monkeyPatch);
    it("Boolean", async() => {

        /*
         *
         *  i.left                      o.left
         *
         *  i.right ────── [i] PLUS ─── o.right
         *               ┌ [d]
         *  a ─┐         │
         *     AND ──┐   │
         *  b ─┘     OR ─┘
         *           │
         *  c ─ NOT ─┘
         *
         *
         */

        for (const aa of [true, false]) {
            for (const bb of [true, false]) {
                for (const cc of [true, false]) {


                    const a = createReverbMainInputCheckboxNodePort("A");
                    const b = createReverbMainInputCheckboxNodePort("B");
                    const c = createReverbMainInputCheckboxNodePort("C");

                    const and = createGateOpNode("and");
                    const not = createGateOpNode("not");
                    const or = createGateOpNode("or");

                    const plus = createMathOpNode("plus");

                    const inputNode = createInputNode(0, 0);
                    inputNode.outputs.push(a, b, c);

                    const graph: WholeReverbGraph = {
                        main: {
                            nodes: [
                                inputNode,
                                outputNode,
                                and, or, not,
                                plus,
                            ],
                            connections: [
                                createConnection(inputNode.outputs[1]!.id, plus.inputs[0].id),
                                createConnection(a.id, and.inputs[0].id),
                                createConnection(b.id, and.inputs[1].id),
                                createConnection(and.outputs[0].id, or.inputs[0].id),
                                createConnection(c.id, not.inputs[0].id),
                                createConnection(not.outputs[0].id, or.inputs[1].id),
                                createConnection(or.outputs[0].id, plus.inputs[2].id),
                                createConnection(plus.outputs[0].id, outputNode.inputs[1]!.id),
                            ],
                        },
                        macros: [],
                    };

                    const compiled = await compileToBoth(
                        graph,
                        { [a.id]: aa, [b.id]: bb, [c.id]: cc },
                        44100,
                        1,
                    );
                    const expected = !(aa && bb || !cc) ? 1 : 0;

                    for (const r of ["js", "wasm"] as const) {
                        assertStereo(
                            compiled[r].run([0, 1], 0),
                            [0, expected],
                            `${r}: a=${aa}, b=${bb}, c=${cc}, (a && b || !c)=${!expected}`,
                        );
                    }


                }
            }
        }




    });
});
