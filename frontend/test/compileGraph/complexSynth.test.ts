import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { MACROS_STD } from "../../src/plugins/list/Reverb/macroStd/macroStd";
import { WholeReverbGraph } from "../../src/plugins/list/Reverb/ReverbNodes";
import { compileToBoth, DEFAULT_CHECKBOXES } from "./_utils";

const graph: WholeReverbGraph = {
    main: {
        nodes: [
            {
                id: "_mainInput_73dff43514baac8a2511eff900d59b1c",
                direction: "ltr",
                x: -11,
                y: 0,
                inputs: [],
                outputs: [
                    {
                        type: "mainInput.signal",
                        valueType: "number",
                        id: "0252f45276a27ba7789f39d14cd9e74c",
                        title: "Left",
                    },
                    {
                        type: "mainInput.signal",
                        valueType: "number",
                        id: "a80965d0af93d4fae7ef531eec93d880",
                        title: "Right",
                    },
                    {
                        type: "mainInput.key",
                        valueType: "number",
                        id: "e5b6ecc0f93bf15a6ec816ec24749b1b",
                        title: "Key1",
                    },
                    {
                        type: "mainInput.key",
                        valueType: "number",
                        id: "6c2edeb9627c85b438d268c781e432fe",
                        title: "Key2",
                    },
                    {
                        type: "mainInput.key",
                        valueType: "number",
                        id: "cb380d556c1b389b2b54ba79838cdf4f",
                        title: "Key3",
                    },
                    {
                        type: "mainInput.key",
                        valueType: "number",
                        id: "7cc84d4b612b14b944813741f46e1732",
                        title: "Key4",
                    },
                    {
                        type: "mainInput.checkbox",
                        valueType: "boolean",
                        id: "e3d3a613285d98144372da26e19ded6b",
                        title: "Is_sin",
                    },
                    {
                        type: "mainInput.checkbox",
                        valueType: "boolean",
                        id: "b523a8b7e18a94ebd8e7b459e5f8918c",
                        title: "Is_saw",
                    },
                    {
                        type: "mainInput.checkbox",
                        valueType: "boolean",
                        id: "71dac675fee9e3c72e420dd4129aceeb",
                        title: "Is_square",
                    },
                ],
                type: "mainInput",
            },
            {
                id: "_mainOutput_9dd3edf5cb5f59c01ffe997922d89055",
                direction: "ltr",
                x: 47,
                y: 0,
                inputs: [
                    {
                        type: "mainOutput.signal",
                        valueType: "number",
                        id: "f5571ccc39ee891c9aa6a25d8d57a603",
                        title: "Left",
                    },
                    {
                        type: "mainOutput.signal",
                        valueType: "number",
                        id: "4b0c59ff6e169d24121e1ac2637786f8",
                        title: "Right",
                    },
                ],
                outputs: [],
                type: "mainOutput",
            },
            {
                id: "_macroUsage_72fa79b92df7ad9dd0c3a3a76f20c435",
                direction: "ltr",
                x: 8,
                y: 10,
                inputs: [
                    {
                        id: "012261a2fd380df663b36a648c0d4172",
                        type: "macroUsage.input",
                        valueType: "number",
                        macroPortId: "33508a1239f9cb408d63c5a6ada237b2",
                    },
                    {
                        id: "46daee411e6a7b3c6159aa2dfe4b28c0",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "3e80da5c7c10225d4302597e1542abe9",
                    },
                    {
                        id: "c9b1d4bbdc488121af0c9f882d434918",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "7ef05eb00a20a507bde6375bc25f2524",
                    },
                    {
                        id: "e2a3be1da61762c28b35d73a424557cc",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "f02e895b2f983030df0db86264152414",
                    },
                ],
                outputs: [
                    {
                        id: "1bb1f79521c1cc6ba9391315f7ae5e81",
                        type: "macroUsage.output",
                        valueType: "number",
                        macroPortId: "0b6e96f42612be860186b8c8f01f9ac0",
                    },
                ],
                type: "macroUsage",
                macroId: "fdbdd40e3b20d336ce5aa73d408e8b88",
            },
            {
                id: "45dc5fb4e88c7abd29c267c457a023e7",
                direction: "ltr",
                x: 8,
                y: 17,
                inputs: [
                    {
                        id: "b58421a9bf0898306506561cc35da088",
                        type: "macroUsage.input",
                        valueType: "number",
                        macroPortId: "33508a1239f9cb408d63c5a6ada237b2",
                    },
                    {
                        id: "ed98a467899eaf6046ae3486344afb16",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "3e80da5c7c10225d4302597e1542abe9",
                    },
                    {
                        id: "12e403b3b8ba4e2d053e71adb8108b3a",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "7ef05eb00a20a507bde6375bc25f2524",
                    },
                    {
                        id: "5a2fd967b913d2951898142c92cf0658",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "f02e895b2f983030df0db86264152414",
                    },
                ],
                outputs: [
                    {
                        id: "6fe05c4fd51ba0617fdac83036385321",
                        type: "macroUsage.output",
                        valueType: "number",
                        macroPortId: "0b6e96f42612be860186b8c8f01f9ac0",
                    },
                ],
                type: "macroUsage",
                macroId: "fdbdd40e3b20d336ce5aa73d408e8b88",
            },
            {
                id: "a8a2ab9260f7be1c78cb7f1924655b55",
                direction: "ltr",
                x: 8,
                y: 24,
                inputs: [
                    {
                        id: "440747b0d07151698fd1a7c215eeb9f0",
                        type: "macroUsage.input",
                        valueType: "number",
                        macroPortId: "33508a1239f9cb408d63c5a6ada237b2",
                    },
                    {
                        id: "7d8cbeaa15ccf6acca7a467872f595bd",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "3e80da5c7c10225d4302597e1542abe9",
                    },
                    {
                        id: "fe88607e1f3588e70d8f443b7c89a488",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "7ef05eb00a20a507bde6375bc25f2524",
                    },
                    {
                        id: "dfde09330bc7d5326e67275987d1540b",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "f02e895b2f983030df0db86264152414",
                    },
                ],
                outputs: [
                    {
                        id: "39315506c42f7efa51faf8c32655091c",
                        type: "macroUsage.output",
                        valueType: "number",
                        macroPortId: "0b6e96f42612be860186b8c8f01f9ac0",
                    },
                ],
                type: "macroUsage",
                macroId: "fdbdd40e3b20d336ce5aa73d408e8b88",
            },
            {
                id: "34d8eadb627da5181d57a7999c0e986f",
                direction: "ltr",
                x: 8,
                y: 31,
                inputs: [
                    {
                        id: "56593e1988cf0e01eecd69d8b3c44a5a",
                        type: "macroUsage.input",
                        valueType: "number",
                        macroPortId: "33508a1239f9cb408d63c5a6ada237b2",
                    },
                    {
                        id: "e66ad22b9c7697cb53f3e51ab1a48e15",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "3e80da5c7c10225d4302597e1542abe9",
                    },
                    {
                        id: "3a19fc189b486f0ef0d4c91310af768f",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "7ef05eb00a20a507bde6375bc25f2524",
                    },
                    {
                        id: "f6ebe0a9e013f4fcda46f812a0fc8b56",
                        type: "macroUsage.input",
                        valueType: "boolean",
                        macroPortId: "f02e895b2f983030df0db86264152414",
                    },
                ],
                outputs: [
                    {
                        id: "6b705c1f3d287c71835e1f31280b9c93",
                        type: "macroUsage.output",
                        valueType: "number",
                        macroPortId: "0b6e96f42612be860186b8c8f01f9ac0",
                    },
                ],
                type: "macroUsage",
                macroId: "fdbdd40e3b20d336ce5aa73d408e8b88",
            },
            {
                id: "_math_065806791ed5515196a144e79bd7089c",
                direction: "ltr",
                x: 32,
                y: 10,
                inputs: [
                    {
                        id: "_math_input_09fcde85870d9de95fe1c0f36e72b70a",
                        type: "math.input",
                        valueType: "number",
                        inputIndex: 0,
                    },
                    {
                        id: "_math_input_994da926575774768ebb69cfa800b5ef",
                        type: "math.input",
                        valueType: "number",
                        inputIndex: 1,
                    },
                    {
                        id: "_math_disable_6236e92b845718b5a7c2b31bc8b5af84",
                        type: "math.disable",
                        valueType: "boolean",
                    },
                ],
                outputs: [
                    {
                        id: "_math_output_41a83854fe3ffcf14693e670e4eb6b40",
                        type: "math.output",
                        valueType: "number",
                    },
                ],
                type: "math",
                op: "plus",
            },
        ],
        connections: [
            {
                id: "ab25160e410eec0e58c317ae70c380ff",
                portIdFrom: "e5b6ecc0f93bf15a6ec816ec24749b1b",
                portIdTo: "012261a2fd380df663b36a648c0d4172",
            },
            {
                id: "38233aa8a309d6c72696076c048ca79f",
                portIdFrom: "1bb1f79521c1cc6ba9391315f7ae5e81",
                portIdTo: "_math_input_09fcde85870d9de95fe1c0f36e72b70a",
            },
            {
                id: "4867e016784e66c9b67638d5ffcca3b8",
                portIdFrom: "6fe05c4fd51ba0617fdac83036385321",
                portIdTo: "_math_input_09fcde85870d9de95fe1c0f36e72b70a",
            },
            {
                id: "af81d0b97ad3fd332c2320400b3f478e",
                portIdFrom: "39315506c42f7efa51faf8c32655091c",
                portIdTo: "_math_input_09fcde85870d9de95fe1c0f36e72b70a",
            },
            {
                id: "07432e0dcd85b3b021f3a06511fe9960",
                portIdFrom: "6b705c1f3d287c71835e1f31280b9c93",
                portIdTo: "_math_input_09fcde85870d9de95fe1c0f36e72b70a",
            },
            {
                id: "e56b25fe285c7745ded0c159a540d536",
                portIdFrom: "_math_output_41a83854fe3ffcf14693e670e4eb6b40",
                portIdTo: "f5571ccc39ee891c9aa6a25d8d57a603",
            },
            {
                id: "b58b4416fd188e9912bed58efc8ba470",
                portIdFrom: "_math_output_41a83854fe3ffcf14693e670e4eb6b40",
                portIdTo: "4b0c59ff6e169d24121e1ac2637786f8",
            },
            {
                id: "12ee8782fbfd5c3ff5d386a1501a0e4a",
                portIdFrom: "6c2edeb9627c85b438d268c781e432fe",
                portIdTo: "b58421a9bf0898306506561cc35da088",
            },
            {
                id: "e93628124c497bf351f262c43c174609",
                portIdFrom: "cb380d556c1b389b2b54ba79838cdf4f",
                portIdTo: "440747b0d07151698fd1a7c215eeb9f0",
            },
            {
                id: "bd6626340ab49768eda37af278ecd685",
                portIdFrom: "7cc84d4b612b14b944813741f46e1732",
                portIdTo: "56593e1988cf0e01eecd69d8b3c44a5a",
            },
            {
                id: "8bfa45aaf5b2e9dd4c81a0f7506251d2",
                portIdFrom: "e3d3a613285d98144372da26e19ded6b",
                portIdTo: "46daee411e6a7b3c6159aa2dfe4b28c0",
            },
            {
                id: "f5c0e75b4475428c4f875561c4310544",
                portIdFrom: "e3d3a613285d98144372da26e19ded6b",
                portIdTo: "ed98a467899eaf6046ae3486344afb16",
            },
            {
                id: "c51e44f610664aadb56f0d5a8c306516",
                portIdFrom: "e3d3a613285d98144372da26e19ded6b",
                portIdTo: "7d8cbeaa15ccf6acca7a467872f595bd",
            },
            {
                id: "5d2719bb9c58defa9e1860137284300c",
                portIdFrom: "e3d3a613285d98144372da26e19ded6b",
                portIdTo: "e66ad22b9c7697cb53f3e51ab1a48e15",
            },
            {
                id: "c3fd8904b638cbd7e2aa9db4fd526aca",
                portIdFrom: "b523a8b7e18a94ebd8e7b459e5f8918c",
                portIdTo: "c9b1d4bbdc488121af0c9f882d434918",
            },
            {
                id: "51928f4680e90673028d6f3daa3be065",
                portIdFrom: "b523a8b7e18a94ebd8e7b459e5f8918c",
                portIdTo: "12e403b3b8ba4e2d053e71adb8108b3a",
            },
            {
                id: "0743d9dc80e692d1e1c641cd7f4c7d0c",
                portIdFrom: "b523a8b7e18a94ebd8e7b459e5f8918c",
                portIdTo: "fe88607e1f3588e70d8f443b7c89a488",
            },
            {
                id: "2d3a2add9915b08d88b416ccdceca7fc",
                portIdFrom: "b523a8b7e18a94ebd8e7b459e5f8918c",
                portIdTo: "3a19fc189b486f0ef0d4c91310af768f",
            },
            {
                id: "d96af17274b96144d52467ef24215ca5",
                portIdFrom: "71dac675fee9e3c72e420dd4129aceeb",
                portIdTo: "e2a3be1da61762c28b35d73a424557cc",
            },
            {
                id: "b1134ff1e8c131ede728088e0297a46f",
                portIdFrom: "71dac675fee9e3c72e420dd4129aceeb",
                portIdTo: "5a2fd967b913d2951898142c92cf0658",
            },
            {
                id: "5e5b32f885422513e2dfd1d4db62cf49",
                portIdFrom: "71dac675fee9e3c72e420dd4129aceeb",
                portIdTo: "dfde09330bc7d5326e67275987d1540b",
            },
            {
                id: "e61b59a71eea9bc34509c8d1629cb970",
                portIdFrom: "71dac675fee9e3c72e420dd4129aceeb",
                portIdTo: "f6ebe0a9e013f4fcda46f812a0fc8b56",
            },
            {
                id: "14218b8b8e9e3652d0f4957ed944552a",
                portIdFrom: "0252f45276a27ba7789f39d14cd9e74c",
                portIdTo: "f5571ccc39ee891c9aa6a25d8d57a603",
            },
            {
                id: "3cb30ea8f7476befa91c864eed18d8e5",
                portIdFrom: "a80965d0af93d4fae7ef531eec93d880",
                portIdTo: "4b0c59ff6e169d24121e1ac2637786f8",
            },
        ],
    },
    macros: [
        {
            title: "MonoSynth",
            id: "fdbdd40e3b20d336ce5aa73d408e8b88",
            graph: {
                nodes: [
                    {
                        id: "_macroDefinitionInput_a54d43a5c0f53f5c54316c82ecf237f9",
                        direction: "ltr",
                        x: -12,
                        y: 10,
                        inputs: [],
                        outputs: [
                            {
                                type: "macroDefinitionInput.output",
                                valueType: "number",
                                id: "33508a1239f9cb408d63c5a6ada237b2",
                                title: "Key",
                            },
                            {
                                type: "macroDefinitionInput.output",
                                valueType: "boolean",
                                id: "3e80da5c7c10225d4302597e1542abe9",
                                title: "Is_sin",
                            },
                            {
                                type: "macroDefinitionInput.output",
                                valueType: "boolean",
                                id: "7ef05eb00a20a507bde6375bc25f2524",
                                title: "Is_saw",
                            },
                            {
                                type: "macroDefinitionInput.output",
                                valueType: "boolean",
                                id: "f02e895b2f983030df0db86264152414",
                                title: "Is_square",
                            },
                        ],
                        type: "macroDefinitionInput",
                    },
                    {
                        id: "_macroDefinitionOutput_c771491e66965051fa6648efb141279f",
                        direction: "ltr",
                        x: 55,
                        y: 10,
                        inputs: [
                            {
                                type: "macroDefinitionOutput.input",
                                valueType: "number",
                                id: "0b6e96f42612be860186b8c8f01f9ac0",
                                title: "New port",
                            },
                        ],
                        outputs: [],
                        type: "macroDefinitionOutput",
                    },
                    {
                        id: "_macroUsage_f5b2dfd2b6d97bda3b9e54f5123d4fcf",
                        direction: "ltr",
                        x: 0,
                        y: 10,
                        inputs: [
                            {
                                id: "f6cc7f3a868e70d8e03679ca925ff29b",
                                type: "macroUsage.input",
                                valueType: "number",
                                macroPortId: "9019e21099fac92b97042dd683c3e50e",
                            },
                        ],
                        outputs: [
                            {
                                id: "c04040fab12b8acef99de8cff0a81839",
                                type: "macroUsage.output",
                                valueType: "number",
                                macroPortId: "c54333a8f0053ad8ca14664fdb19e74b",
                            },
                            {
                                id: "409a3ec976c95e7cce6b2a139304ddf3",
                                type: "macroUsage.output",
                                valueType: "number",
                                macroPortId: "91725d81297115601a4fadcae02b923a",
                            },
                        ],
                        type: "macroUsage",
                        macroId: "std::musical::Oscillator",
                    },
                    {
                        id: "_gate_d265acba6836b37efe25a13d8c10cbb2",
                        direction: "ltr",
                        x: 0,
                        y: 16,
                        inputs: [
                            {
                                id: "_gate_input_f1a410c4823fa40ddde7c54bc08cef75",
                                type: "gate.input",
                                valueType: "boolean",
                                inputIndex: 0,
                            },
                        ],
                        outputs: [
                            {
                                id: "_gate_output_0ae39d3703540d3a7e087114866acdf6",
                                type: "gate.output",
                                valueType: "boolean",
                            },
                        ],
                        type: "gate",
                        op: "not",
                    },
                    {
                        id: "28f793dad627ace0f548e31476947ecb",
                        direction: "ltr",
                        x: 0,
                        y: 20,
                        inputs: [
                            {
                                id: "4f850061482751f8c8986dee9613e594",
                                type: "gate.input",
                                valueType: "boolean",
                                inputIndex: 0,
                            },
                        ],
                        outputs: [
                            {
                                id: "e50b13c09ee2b40fdc73ed1608f93b14",
                                type: "gate.output",
                                valueType: "boolean",
                            },
                        ],
                        type: "gate",
                        op: "not",
                    },
                    {
                        id: "cfb853e60e6f96f05bb4b99b0cb274cc",
                        direction: "ltr",
                        x: 0,
                        y: 24,
                        inputs: [
                            {
                                id: "953901e34ad50fef8a37c22aad7702bc",
                                type: "gate.input",
                                valueType: "boolean",
                                inputIndex: 0,
                            },
                        ],
                        outputs: [
                            {
                                id: "bc9a00f7a7e92234f2406a2ee4922175",
                                type: "gate.output",
                                valueType: "boolean",
                            },
                        ],
                        type: "gate",
                        op: "not",
                    },
                    {
                        id: "_expr_4d2818443a0e563da94994408b017e2c",
                        direction: "ltr",
                        x: 24,
                        y: 10,
                        inputs: [
                            {
                                id: "72807b604a2221fd14ec3e7e343e66ee",
                                variable: "t",
                                type: "expr.variable",
                                valueType: "number",
                            },
                        ],
                        outputs: [
                            {
                                id: "_expr_result_ea5b1b9df9b8a7a796d567db50ffe88e",
                                type: "expr.result",
                                valueType: "number",
                            },
                        ],
                        type: "expr",
                        expr: "sin(t * 3.1416 * 2)",
                    },
                    {
                        id: "8c358555eff8df7e696b431f7f93d7ba",
                        direction: "ltr",
                        x: 24,
                        y: 16,
                        inputs: [
                            {
                                id: "6cf3379c5909c09bd96c55d3dcc5eb23",
                                variable: "t",
                                type: "expr.variable",
                                valueType: "number",
                            },
                        ],
                        outputs: [
                            {
                                id: "834cf8562ff2c51380651495f1142107",
                                type: "expr.result",
                                valueType: "number",
                            },
                        ],
                        type: "expr",
                        expr: "((t - 0.5) % 1 - 0.5) * 2",
                    },
                    {
                        id: "b57d56b629b8c7626308edf19529b15a",
                        direction: "ltr",
                        x: 24,
                        y: 22,
                        inputs: [
                            {
                                id: "a85c18217d7cccc78f5daf299c3b047d",
                                variable: "t",
                                type: "expr.variable",
                                valueType: "number",
                            },
                        ],
                        outputs: [
                            {
                                id: "21aaf33e49083eba2edadf17516cd732",
                                type: "expr.result",
                                valueType: "number",
                            },
                        ],
                        type: "expr",
                        expr: "heaviside(t % 1 - 0.5) * 2 - 1",
                    },
                    {
                        id: "_math_698b2910f01e831edb037028b63a52dc",
                        direction: "ltr",
                        x: 41,
                        y: 10,
                        inputs: [
                            {
                                id: "_math_input_01eb53417514db93826ecc1f720962a8",
                                type: "math.input",
                                valueType: "number",
                                inputIndex: 0,
                            },
                            {
                                id: "_math_input_2fbfb19cfcba944fce1debd27ef8a6e4",
                                type: "math.input",
                                valueType: "number",
                                inputIndex: 1,
                            },
                            {
                                id: "_math_disable_936fec08a6c9c436841334fc8271ec30",
                                type: "math.disable",
                                valueType: "boolean",
                            },
                        ],
                        outputs: [
                            {
                                id: "_math_output_37f3b88d8a10770a2d3ddc9efc4c26f2",
                                type: "math.output",
                                valueType: "number",
                            },
                        ],
                        type: "math",
                        op: "plus",
                    },
                    {
                        id: "c31397a8cfa9bb73d6e907c80ef16412",
                        direction: "ltr",
                        x: 41,
                        y: 16,
                        inputs: [
                            {
                                id: "ee47400b18c3df7d19df78c2997f7fb9",
                                type: "math.input",
                                valueType: "number",
                                inputIndex: 0,
                            },
                            {
                                id: "ff863e5e966b3379c84a0c493ab6bd17",
                                type: "math.input",
                                valueType: "number",
                                inputIndex: 1,
                            },
                            {
                                id: "5373fcc1cf223e4d0b76b18ce1e2b95e",
                                type: "math.disable",
                                valueType: "boolean",
                            },
                        ],
                        outputs: [
                            {
                                id: "778d1a75d98c4c4769f379b911c8aff2",
                                type: "math.output",
                                valueType: "number",
                            },
                        ],
                        type: "math",
                        op: "plus",
                    },
                    {
                        id: "1cc9f002184812f5cb45529361cda026",
                        direction: "ltr",
                        x: 41,
                        y: 22,
                        inputs: [
                            {
                                id: "364d40a51625435515d50f57b49ca077",
                                type: "math.input",
                                valueType: "number",
                                inputIndex: 0,
                            },
                            {
                                id: "ce49ab73d5e8955d33880273479c3963",
                                type: "math.input",
                                valueType: "number",
                                inputIndex: 1,
                            },
                            {
                                id: "29efa623aef57916cf6df1e76be1e86d",
                                type: "math.disable",
                                valueType: "boolean",
                            },
                        ],
                        outputs: [
                            {
                                id: "f4ce1673db90f2c1968d0d60b0367375",
                                type: "math.output",
                                valueType: "number",
                            },
                        ],
                        type: "math",
                        op: "plus",
                    },
                ],
                connections: [
                    {
                        id: "b748574c4f08c1f0b377e2dd53318fb1",
                        portIdFrom: "33508a1239f9cb408d63c5a6ada237b2",
                        portIdTo: "f6cc7f3a868e70d8e03679ca925ff29b",
                    },
                    {
                        id: "c2e4dd3c0f4e1c3fab5fa5c94255a432",
                        portIdFrom: "3e80da5c7c10225d4302597e1542abe9",
                        portIdTo: "_gate_input_f1a410c4823fa40ddde7c54bc08cef75",
                    },
                    {
                        id: "d4a76a80db75e49456ff86393bc2b84d",
                        portIdFrom: "7ef05eb00a20a507bde6375bc25f2524",
                        portIdTo: "4f850061482751f8c8986dee9613e594",
                    },
                    {
                        id: "684a1aaaa47ad823dc56c95ede27d1ba",
                        portIdFrom: "f02e895b2f983030df0db86264152414",
                        portIdTo: "953901e34ad50fef8a37c22aad7702bc",
                    },
                    {
                        id: "5c93ee269d2a3a9a6f270ea408ee3023",
                        portIdFrom: "c04040fab12b8acef99de8cff0a81839",
                        portIdTo: "72807b604a2221fd14ec3e7e343e66ee",
                    },
                    {
                        id: "9b2dd343dcd649da43cd67c12816dca1",
                        portIdFrom: "c04040fab12b8acef99de8cff0a81839",
                        portIdTo: "6cf3379c5909c09bd96c55d3dcc5eb23",
                    },
                    {
                        id: "e418babfd24913f04240070c57652082",
                        portIdFrom: "c04040fab12b8acef99de8cff0a81839",
                        portIdTo: "a85c18217d7cccc78f5daf299c3b047d",
                    },
                    {
                        id: "f226c85170c89f176173d0c68215f972",
                        portIdFrom: "_expr_result_ea5b1b9df9b8a7a796d567db50ffe88e",
                        portIdTo: "_math_input_01eb53417514db93826ecc1f720962a8",
                    },
                    {
                        id: "c77bb84d06a54cf1b042b1abf8e33a59",
                        portIdFrom: "834cf8562ff2c51380651495f1142107",
                        portIdTo: "ee47400b18c3df7d19df78c2997f7fb9",
                    },
                    {
                        id: "51a5f5efece4b8aa71ffddbf639ac51f",
                        portIdFrom: "21aaf33e49083eba2edadf17516cd732",
                        portIdTo: "364d40a51625435515d50f57b49ca077",
                    },
                    {
                        id: "3b4dda75e88e09bab4249dd667f979ce",
                        portIdFrom: "_gate_output_0ae39d3703540d3a7e087114866acdf6",
                        portIdTo: "_math_disable_936fec08a6c9c436841334fc8271ec30",
                    },
                    {
                        id: "f05f0ff053450f7b96d79170c2725ad2",
                        portIdFrom: "e50b13c09ee2b40fdc73ed1608f93b14",
                        portIdTo: "5373fcc1cf223e4d0b76b18ce1e2b95e",
                    },
                    {
                        id: "29afae642842feac54d2b790d200c43c",
                        portIdFrom: "bc9a00f7a7e92234f2406a2ee4922175",
                        portIdTo: "29efa623aef57916cf6df1e76be1e86d",
                    },
                    {
                        id: "471eef2f093a0283b93199ffbb59b597",
                        portIdFrom: "f4ce1673db90f2c1968d0d60b0367375",
                        portIdTo: "0b6e96f42612be860186b8c8f01f9ac0",
                    },
                    {
                        id: "943c1189b3eb98a5486069a4bc185c88",
                        portIdFrom: "778d1a75d98c4c4769f379b911c8aff2",
                        portIdTo: "0b6e96f42612be860186b8c8f01f9ac0",
                    },
                    {
                        id: "31f02ed9404cc773d59e2a74520b76ad",
                        portIdFrom: "_math_output_37f3b88d8a10770a2d3ddc9efc4c26f2",
                        portIdTo: "0b6e96f42612be860186b8c8f01f9ac0",
                    },
                ],
            },
        },
    ],
};

describe("Compile graph", () => {
    beforeEach(monkeyPatch);
    it("Complex graph", async() => {

        await (async() => {
            graph.macros = [
                ...graph.macros,
                ...MACROS_STD.values().toArray(),
            ];
            await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);
        })();

    });
});
