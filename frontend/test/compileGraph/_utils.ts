import { compileReverbGraphToJs, compileReverbGraphToWASM } from "../../src/plugins/list/Reverb/compile/ReverbCompile";
import { createInputNode, createOutputNode, WholeReverbGraph } from "../../src/plugins/list/Reverb/ReverbNodes";
import { ReverbCompilationResult } from "../../src/plugins/list/Reverb/ReverbState";
import {assertNumbersClose} from "@audio-playground/lib-test-utils/assertNumbersClose";

export function assertStereo(
    actual: number[],
    expected: [number, number],
    message?: string,
    epsilon = Number.EPSILON,
) {
    assertNumbersClose(actual[0]!, expected[0], epsilon, `${message}: Left  expected to be ${expected[0]}, but actual ${actual[0]}`);
    assertNumbersClose(actual[1]!, expected[1], epsilon, `${message}: Right expected to be ${expected[1]}, but actual ${actual[1]}`);
}
export const inputNode = createInputNode(0, 0);
export const outputNode = createOutputNode(0, 0);

export const DEFAULT_CHECKBOXES = {};
type UniResult = {
    run: (interlacedValues: number[], timeSamples: number) => number[],
}
type Both = {
    wasm: UniResult,
    js: UniResult,
}

function wrapWasmResult(
    wasmResult: ReverbCompilationResult,
    bufferLength: number,
): UniResult {

    return {
        run: (data: number[], timeSamples: number) => {

            const { inputsCount, outputsCount, run, memory } = wasmResult;
            const view = new Float64Array(memory);

            data.forEach((v, i) => {
                view[i] = v;
            });

            run(timeSamples);

            const OUTPUT_OFFSET = bufferLength * inputsCount;

            const res = [
                ...Array(bufferLength * outputsCount),
            ].map((_, idx) => view[idx + OUTPUT_OFFSET]!);

            return res;
        },
    };
}
function wrapJsResult(
    wasmResult: ReverbCompilationResult,
    bufferLength: number,
): UniResult {

    return {
        run: (data: number[], timeSamples: number) => {

            const { inputsCount, outputsCount, run, memory } = wasmResult;
            const view = new Float64Array(memory);

            data.forEach((v, i) => {
                view[i] = v;
            });

            run(timeSamples);

            const OUTPUT_OFFSET = bufferLength * inputsCount;

            const res = [
                ...Array(bufferLength * outputsCount),
            ].map((_, idx) => view[idx + OUTPUT_OFFSET]!);

            return res;
        },
    };
}
export async function compileToBoth(
    graph: WholeReverbGraph,
    checkboxesValues: Record<string, boolean>,
    sampleRate: number,
    bufferLength: number,
): Promise<Both> {
    const js = compileReverbGraphToJs(graph, checkboxesValues, sampleRate, bufferLength);
    const wasm = await compileReverbGraphToWASM(graph, checkboxesValues, sampleRate, bufferLength);
    return {
        js,
        wasm: wrapWasmResult(wasm, bufferLength),
    };
}
