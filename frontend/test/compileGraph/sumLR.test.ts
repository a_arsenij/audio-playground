import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { createConnection } from "../../src/components/Common/Gridy/NodeEditor/Utils";
import { createMathOpNode, WholeReverbGraph } from "../../src/plugins/list/Reverb/ReverbNodes";
import { assertStereo, compileToBoth, DEFAULT_CHECKBOXES, inputNode, outputNode } from "./_utils";


describe("Compile graph", () => {
    beforeEach(monkeyPatch);
    it("Sum LR", async() => {

        const sumNode = createMathOpNode("plus");

        const graph: WholeReverbGraph = {
            main: {
                nodes: [
                    inputNode,
                    sumNode,
                    outputNode,
                ],
                connections: [
                    createConnection(
                        inputNode.outputs[0]!.id,
                        sumNode.inputs[0].id,
                    ),
                    createConnection(
                        inputNode.outputs[1]!.id,
                        sumNode.inputs[0].id,
                    ),
                    createConnection(
                        sumNode.outputs[0].id,
                        outputNode.inputs[0]!.id,
                    ),
                    createConnection(
                        sumNode.outputs[0].id,
                        outputNode.inputs[1]!.id,
                    ),
                ],
            },
            macros: [],
        };
        const compiled = await compileToBoth(graph, DEFAULT_CHECKBOXES, 44100, 1);

        for (const r of ["js", "wasm"] as const) {
            assertStereo(compiled[r].run([0, 0], 0), [0, 0], `${r}: Sum LR 0 0`);
            assertStereo(compiled[r].run([0.5, 0.5], 0), [1, 1], `${r}: Sum LR 0.5 0.5`);
            assertStereo(compiled[r].run([0.2, 0.2], 0), [0.4, 0.4], `${r}: Sum LR 0.2 0.2`);
            assertStereo(compiled[r].run([1, 0], 0), [1, 1], `${r}: Sum LR 1 0`);
            assertStereo(compiled[r].run([0, 1], 0), [1, 1], `${r}: Sum LR 0 1`);
        }

    });
});
