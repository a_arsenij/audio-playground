import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { createConnection } from "../../src/components/Common/Gridy/NodeEditor/Utils";
import {
    createInputNode,
    createMathOpNode,
    createReverbMainInputCheckboxNodePort,
    WholeReverbGraph,
} from "../../src/plugins/list/Reverb/ReverbNodes";
import { assertStereo, compileToBoth, outputNode } from "./_utils";

describe("Compile graph", () => {
    beforeEach(monkeyPatch);
    it("Boolean", async() => {

        /*
         *
         *  i.left                      o.left
         *
         *  i.right ────── [i] PLUS ─── o.right
         *               ┌ [d]
         *  a ───────────┘
         *
         */

        for (const aa of [true, false]) {


            const a = createReverbMainInputCheckboxNodePort("Checkbox");
            const plus = createMathOpNode("plus");

            const inputNode = createInputNode(0, 0);
            inputNode.outputs.push(a);

            const graph: WholeReverbGraph = {
                main: {
                    nodes: [
                        inputNode,
                        outputNode,
                        plus,
                    ],
                    connections: [
                        createConnection(inputNode.outputs[1]!.id, plus.inputs[0].id),
                        createConnection(a.id, plus.inputs[2].id),
                        createConnection(plus.outputs[0].id, outputNode.inputs[1]!.id),
                    ],
                },
                macros: [],
            };

            const compiled = await compileToBoth(
                graph,
                { [a.id]: aa },
                44100,
                1,
            );
            const expected = !aa ? 1 : 0;

            for (const r of ["js", "wasm"] as const) {
                assertStereo(
                    compiled[r].run([0, 1], 0),
                    [0, expected],
                    `${r}: a=${aa}`,
                );
            }


        }
    });
});
