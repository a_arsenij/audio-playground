import { beforeEach, describe, it } from "node:test";

import { generateId } from "@audio-playground/lib-common/std/Id";
import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";

import { createConnection } from "../../src/components/Common/Gridy/NodeEditor/Utils";
import {
    createConstNode, createGateOpNode,
    createInputNode,
    createMacroDefinitionInputNode,
    createMacroDefinitionOutputNode,
    createMacroUsageNode,
    createMathOpNode, createReverbMainInputCheckboxNodePort, createReverbMainInputKnobNodePort,
    MacrosDefinitionInputPort,
    reverbCreateMacroUsageInputs,
    reverbCreateMacroUsageOutputs,
    ReverbGraph,
    ReverbMacrosDefinitionInputNode,
    ReverbMacrosDefinitionOutputNode,
    WholeReverbGraph,
} from "../../src/plugins/list/Reverb/ReverbNodes";
import { assertStereo, compileToBoth, DEFAULT_CHECKBOXES, outputNode } from "./_utils";
import macroDelayTest from "./macroDelayTest.json";
import {assertNumbersClose} from "@audio-playground/lib-test-utils/assertNumbersClose";

describe("Compile graph", () => {
    beforeEach(monkeyPatch);

    it("Macro - just connection", async() => {

        /*
         * MACROS:
         *
         * in ─── out
         *
         * MAIN:
         *
         * l ─ MACROS ─ l
         *
         * r            r
         *
         */

        const mIn = createMacroDefinitionInputNode();
        mIn.outputs.push({
            type: "macroDefinitionInput.output",
            valueType: "number",
            id: generateId(),
            title: "in",
        });
        const mOut = createMacroDefinitionOutputNode();
        mOut.inputs.push({
            type: "macroDefinitionOutput.input",
            valueType: "number",
            id: generateId(),
            title: "out",
        });
        const mUse = createMacroUsageNode("MACRO");
        mUse.inputs = reverbCreateMacroUsageInputs(mIn);
        mUse.outputs = reverbCreateMacroUsageOutputs(mOut);

        const inputNode = createInputNode(0, 0);
        const graph: WholeReverbGraph = {
            main: {
                nodes: [inputNode, outputNode, mUse],
                connections: [
                    // @ts-ignore
                    createConnection(inputNode.outputs[0].id, mUse.inputs[0].id),
                    // @ts-ignore
                    createConnection(mUse.outputs[0].id, outputNode.inputs[0].id),
                ],
            },
            macros: [{
                id: "MACRO",
                title: "Macro",
                graph: {
                    nodes: [mIn, mOut],
                    connections: [
                        // @ts-ignore
                        createConnection(mIn.outputs[0].id, mOut.inputs[0].id),
                    ],
                },
            }],
        };

        const compiled = await compileToBoth(
            graph,
            DEFAULT_CHECKBOXES,
            44100,
            1,
        );
        for (const r of ["js", "wasm"] as const) {
            assertStereo(
                compiled[r].run([0, 0], 0),
                [0, 0],
                "Zero = zero",
            );
            assertStereo(
                compiled[r].run([1, 1], 0),
                [1, 0],
                "Left passed, right ignored",
            );
        }


    });

    it("Macro - ^2^2=^4", async() => {

        const graph: WholeReverbGraph = {
            main: {
                nodes: [{
                    id: "dd07845957f1e7d913e5012e3210f845",
                    direction: "rtl",
                    x: 0,
                    y: 2,
                    inputs: [],
                    outputs: [{
                        id: "f9800d8eaa752d6d9c9835fd538694c4",
                        type: "mainInput.signal",
                        valueType: "number",
                        title: "left",
                    }, {
                        id: "f4fc6d5b0c31d73c820e9ce17973813d",
                        type: "mainInput.signal",
                        valueType: "number",
                        title: "right",
                    }],
                    type: "mainInput",
                }, {
                    id: "7023ef9a6d8415aa58b959ca0909f8dc",
                    direction: "ltr",
                    x: 0,
                    y: 17,
                    inputs: [{
                        id: "35acd5e947702cb3b66a140f38b259f9",
                        type: "mainOutput.signal",
                        valueType: "number",
                        title: "left",
                    }, {
                        id: "27d8c1c0108af3aa67cfeeed43789f26",
                        type: "mainOutput.signal",
                        valueType: "number",
                        title: "right",
                    }],
                    outputs: [],
                    type: "mainOutput",
                }, {
                    id: "e000a65c78e18320f5fbbacd93e60861",
                    direction: "ltr",
                    x: 0,
                    y: 7,
                    inputs: [{
                        id: "ab06fd5a918347ad9fb5e13e6061f162",
                        type: "macroUsage.input",
                        valueType: "number",
                        macroPortId: "de2daa3c9e8ce3a57fc74b687a4d21e7",
                    }],
                    outputs: [{
                        id: "9ccd9fa8fd1279e7ee7a2ed7c6031a33",
                        type: "macroUsage.output",
                        valueType: "number",
                        macroPortId: "c8b8119ad2f0f17d1d5f44040317673e",
                    }],
                    type: "macroUsage",
                    macroId: "dd3212b52dc85d971cab4c2385255dfc",
                }, {
                    id: "ed1dcc8a4dce076ed29864dee9bc03b2",
                    direction: "rtl",
                    x: 0,
                    y: 12,
                    inputs: [{
                        id: "9f96d14cf97038f63a99a1e354ba8e83",
                        type: "macroUsage.input",
                        valueType: "number",
                        macroPortId: "de2daa3c9e8ce3a57fc74b687a4d21e7",
                    }],
                    outputs: [{
                        id: "3ecac2f3c9e254220a00b7ab26a4bbaf",
                        type: "macroUsage.output",
                        valueType: "number",
                        macroPortId: "c8b8119ad2f0f17d1d5f44040317673e",
                    }],
                    type: "macroUsage",
                    macroId: "dd3212b52dc85d971cab4c2385255dfc",
                }],
                connections: [{
                    id: "09af28c1518abd2f2848a12ee4907685",
                    portIdFrom: "f9800d8eaa752d6d9c9835fd538694c4",
                    portIdTo: "ab06fd5a918347ad9fb5e13e6061f162",
                }, {
                    id: "8e4fcf6d380ea27baf726a4fd32e3127",
                    portIdFrom: "9ccd9fa8fd1279e7ee7a2ed7c6031a33",
                    portIdTo: "9f96d14cf97038f63a99a1e354ba8e83",
                }, {
                    id: "3f84d869ab6a5a8e8b49b8ec0b74997c",
                    portIdFrom: "3ecac2f3c9e254220a00b7ab26a4bbaf",
                    portIdTo: "35acd5e947702cb3b66a140f38b259f9",
                }],
            },
            macros: [{
                id: "dd3212b52dc85d971cab4c2385255dfc",
                title: "Macro",
                graph: {
                    nodes: [{
                        id: "fabdb6ccaf53c4b792920afbc57076f6",
                        direction: "ltr",
                        x: -4,
                        y: 0,
                        inputs: [],
                        outputs: [{
                            type: "macroDefinitionInput.output",
                            valueType: "number",
                            id: "de2daa3c9e8ce3a57fc74b687a4d21e7",
                            title: "New port",
                        }],
                        type: "macroDefinitionInput",
                    }, {
                        id: "4e7099bb34562da43e5155d80925764f",
                        direction: "ltr",
                        x: 10,
                        y: 0,
                        inputs: [{
                            type: "macroDefinitionOutput.input",
                            valueType: "number",
                            id: "c8b8119ad2f0f17d1d5f44040317673e",
                            title: "New port",
                        }],
                        outputs: [],
                        type: "macroDefinitionOutput",
                    }, {
                        id: "711b2ea3da7d76c4beb6f1acc5acd50c",
                        direction: "ltr",
                        x: 3,
                        y: 8,
                        inputs: [
                            {
                                id: "942c5197a6790b78d1921f6cd2a5a8ba",
                                type: "math.input",
                                valueType: "number",
                                inputIndex: 0,
                            }, {
                                id: "d41d01665d7b2ec84520763601ad2fd9",
                                type: "math.input",
                                valueType: "number",
                                inputIndex: 1,
                            },
                            {
                                id: "e480cf890af9a7ad585d24cb47b23bf8",
                                type: "math.disable",
                                valueType: "boolean",
                            },
                        ],
                        outputs: [
                            {
                                id: "ad188952796bdb7853379c7affc9bf68",
                                type: "math.output",
                                valueType: "number",
                            },
                        ],
                        type: "math",
                        op: "multiply",
                    }],
                    connections: [{
                        id: "05541420473c362ceeca7e9964243e5c",
                        portIdFrom: "de2daa3c9e8ce3a57fc74b687a4d21e7",
                        portIdTo: "942c5197a6790b78d1921f6cd2a5a8ba",
                    }, {
                        id: "4d0012931d9ab2c4dc5ece4b50dee471",
                        portIdFrom: "de2daa3c9e8ce3a57fc74b687a4d21e7",
                        portIdTo: "d41d01665d7b2ec84520763601ad2fd9",
                    }, {
                        id: "5053d27e24aaa54f0349597f73357c61",
                        portIdFrom: "ad188952796bdb7853379c7affc9bf68",
                        portIdTo: "c8b8119ad2f0f17d1d5f44040317673e",
                    }],
                },
            }],
        };

        const compiled = await compileToBoth(
            graph,
            DEFAULT_CHECKBOXES,
            44100,
            1,
        );

        for (const r of ["js", "wasm"] as const) {
            assertStereo(
                compiled[r].run([0, 0], 0),
                [0, 0],
                "0^4=0",
            );
            assertStereo(
                compiled[r].run([0.3, 0], 0),
                [0.0081, 0],
                "0.3^4=0.0081",
            );
            assertStereo(
                compiled[r].run([0.5, 0], 0),
                [0.0625, 0],
                "0.5^4=0.0625",
            );
            assertStereo(
                compiled[r].run([0.8, 0], 0),
                [0.4096, 0],
                "0.8^4=0.4096",
            );
            assertStereo(
                compiled[r].run([1, 0], 0),
                [1, 0],
                "1^4=1",
            );
        }


    });

    it("Macro - delay 1 delay 1=delay 2", async() => {

        const graph = macroDelayTest as WholeReverbGraph;

        const compiled = await compileToBoth(
            graph,
            DEFAULT_CHECKBOXES,
            44100,
            16,
        );


        const arr = [
            0, 1, 2, 5, 2, 87, 2, 4, 2, 7, 1, 2, 1, 2, 3, 4,
        ];

        const input = arr.flatMap((v) => [v, 0]);
        for (const r of ["js", "wasm"] as const) {
            const out = compiled[r].run(input, 0);


            const res = out.filter((_v, i) => i % 2 === 0);

            res.forEach((v, idx) => {
                if (idx < 2) {
                    return;
                }
                assertNumbersClose(
                    v,
                    arr[idx - 2]!,
                );
            });
        }


    });

    it("Macro - cubic polynomial", async() => {

        /*
         * MACRO:
         *
         * A ─────────────┐
         *                *5 ──┐
         * B ─────────┐   │   +6 ──┐
         *            *4 ──────┘   │
         * C ─────┐   │   │       +8 ─── RES
         *        *3 ──────────┐   │
         *        │   │   │   +7 ──┘
         * D ──────────────────┘
         *        │   │   │
         *    ┌───┤   │   │
         *    │   *1 ─┤   │
         *    ├───┘   *2 ─┘
         * X ─┴───────┘
         */

        function createMacro(): ReverbGraph {
            function i(name: string): MacrosDefinitionInputPort {
                return {
                    type: "macroDefinitionInput.output",
                    valueType: "number",
                    id: generateId(),
                    title: name,
                };
            }

            const input = createMacroDefinitionInputNode();
            input.outputs.push(...["A", "B", "C", "D", "X"].map((c) => i(c)));
            const output = createMacroDefinitionOutputNode();
            output.inputs.push({
                type: "macroDefinitionOutput.input",
                valueType: "number",
                id: generateId(),
                title: "RES",
            });
            const _1 = createMathOpNode("multiply");
            const _2 = createMathOpNode("multiply");
            const _3 = createMathOpNode("multiply");
            const _4 = createMathOpNode("multiply");
            const _5 = createMathOpNode("multiply");
            const _6 = createMathOpNode("plus");
            const _7 = createMathOpNode("plus");
            const _8 = createMathOpNode("plus");
            return {
                nodes: [input, output, _1, _2, _3, _4, _5, _6, _7, _8],
                connections: [

                    // @ts-ignore
                    createConnection(input.outputs[4].id, _1.inputs[0].id),
                    // @ts-ignore
                    createConnection(input.outputs[4].id, _1.inputs[1].id),

                    // @ts-ignore
                    createConnection(_1.outputs[0].id, _2.inputs[0].id),
                    // @ts-ignore
                    createConnection(input.outputs[4].id, _2.inputs[1].id),

                    // @ts-ignore
                    createConnection(input.outputs[2].id, _3.inputs[0].id),
                    // @ts-ignore
                    createConnection(input.outputs[4].id, _3.inputs[1].id),

                    // @ts-ignore
                    createConnection(input.outputs[1].id, _4.inputs[0].id),
                    // @ts-ignore
                    createConnection(_1.outputs[0].id, _4.inputs[1].id),

                    // @ts-ignore
                    createConnection(input.outputs[0].id, _5.inputs[0].id),
                    // @ts-ignore
                    createConnection(_2.outputs[0].id, _5.inputs[1].id),

                    // @ts-ignore
                    createConnection(_5.outputs[0].id, _6.inputs[0].id),
                    // @ts-ignore
                    createConnection(_4.outputs[0].id, _6.inputs[1].id),

                    // @ts-ignore
                    createConnection(_3.outputs[0].id, _7.inputs[0].id),
                    // @ts-ignore
                    createConnection(input.outputs[3].id, _7.inputs[1].id),

                    // @ts-ignore
                    createConnection(_6.outputs[0].id, _8.inputs[0].id),
                    // @ts-ignore
                    createConnection(_7.outputs[0].id, _8.inputs[1].id),

                    // @ts-ignore
                    createConnection(_8.outputs[0].id, output.inputs[0].id),
                ],
            };
        }

        const macro = createMacro();

        /*
         * MAIN:
         *
         * knob.a1 ───── [a] MACRO ─── o.left
         * knob.b1 ───── [b]
         * knob.c1 ───── [c]
         *            ┌─ [d]
         * i.left  ───── [x]
         *            │
         * knob.a2 ───── [a] MACRO ─── o.right
         * knob.b2 ───── [b]
         * knob.c2 ───── [c]
         *            ├─ [d]
         * i.right  ──── [x]
         *            │
         * knob.d ────┘
         */

        function createMain(): ReverbGraph {


            const inputNode = createInputNode(0, 0, { keyPort: false });
            const a1 = createReverbMainInputKnobNodePort("a1", -10, 10);
            const b1 = createReverbMainInputKnobNodePort("b1", -10, 10);
            const c1 = createReverbMainInputKnobNodePort("c", -10, 10);
            const a2 = createReverbMainInputKnobNodePort("a2", -10, 10);
            const b2 = createReverbMainInputKnobNodePort("b2", -10, 10);
            const c2 = createReverbMainInputKnobNodePort("c", -10, 10);
            const d = createReverbMainInputKnobNodePort("d", -10, 10);
            inputNode.outputs.push(a1, b1, c1, a2, b2, c2, d);

            const m1 = createMacroUsageNode("MACRO");
            m1.inputs = reverbCreateMacroUsageInputs(
                macro.nodes[0] as ReverbMacrosDefinitionInputNode,
            );
            m1.outputs = reverbCreateMacroUsageOutputs(
                macro.nodes[1] as ReverbMacrosDefinitionOutputNode,
            );

            const m2 = createMacroUsageNode("MACRO");
            m2.inputs = reverbCreateMacroUsageInputs(
                macro.nodes[0] as ReverbMacrosDefinitionInputNode,
            );
            m2.outputs = reverbCreateMacroUsageOutputs(
                macro.nodes[1] as ReverbMacrosDefinitionOutputNode,
            );

            return {
                nodes: [
                    m1, m2,
                    inputNode, outputNode,
                ],
                connections: [
                    // @ts-ignore
                    createConnection(a1.id, m1.inputs[0].id),
                    // @ts-ignore
                    createConnection(b1.id, m1.inputs[1].id),
                    // @ts-ignore
                    createConnection(c1.id, m1.inputs[2].id),
                    // @ts-ignore
                    createConnection(d.id, m1.inputs[3].id),
                    // @ts-ignore
                    createConnection(inputNode.outputs[0].id, m1.inputs[4].id),

                    // @ts-ignore
                    createConnection(m1.outputs[0].id, outputNode.inputs[0].id),

                    // @ts-ignore
                    createConnection(a2.id, m2.inputs[0].id),
                    // @ts-ignore
                    createConnection(b2.id, m2.inputs[1].id),
                    // @ts-ignore
                    createConnection(c2.id, m2.inputs[2].id),
                    // @ts-ignore
                    createConnection(d.id, m2.inputs[3].id),
                    // @ts-ignore
                    createConnection(inputNode.outputs[1].id, m2.inputs[4].id),

                    // @ts-ignore
                    createConnection(m2.outputs[0].id, outputNode.inputs[1].id),
                ],
            };
        }

        const graph: WholeReverbGraph = {
            main: createMain(),
            macros: [{
                id: "MACRO",
                title: "Macro",
                graph: macro,
            }],
        };

        const compiled = await compileToBoth(
            graph,
            DEFAULT_CHECKBOXES,
            44100,
            1,
        );
        for (const r of ["js", "wasm"] as const) {
            assertStereo(
                compiled[r].run([7, 8, 1, 2, 5, 3, 4, 5, 6], 0),
                [
                    482 /* 1 * 7 ** 3 + 2 * 7 ** 2 + 5 * 7 + 6 */,
                    1838 /* 3 * 8 ** 3 + 4 * 8 ** 2 + 5 * 8 + 6 */,
                ],
                "Cubic polynoms should match",
            );
        }


    });

    it("Macro - boolean", async() => {

        /*
         * MACROS:
         *
         * in ─ d const 5 ── out
         *
         * MAIN:
         *
         * l  MACROS ─ l
         *    |
         * r  |         r
         * d -|
         */

        const mIn = createMacroDefinitionInputNode();
        mIn.outputs.push({
            type: "macroDefinitionInput.output",
            valueType: "boolean",
            id: generateId(),
            title: "in",
        });
        const mOut = createMacroDefinitionOutputNode();
        mOut.inputs.push({
            type: "macroDefinitionOutput.input",
            valueType: "number",
            id: generateId(),
            title: "out",
        });
        const mConst = createConstNode(5);
        const mUse = createMacroUsageNode("MACRO");
        mUse.inputs = reverbCreateMacroUsageInputs(mIn);
        mUse.outputs = reverbCreateMacroUsageOutputs(mOut);

        const inputNode = createInputNode(0, 0);
        const boolport = createReverbMainInputCheckboxNodePort("aaaa");
        inputNode.outputs.push(boolport);
        const graph: WholeReverbGraph = {
            main: {
                nodes: [inputNode, outputNode, mUse],
                connections: [
                    // @ts-ignore
                    createConnection(inputNode.outputs[3].id, mUse.inputs[0].id),
                    // @ts-ignore
                    createConnection(mUse.outputs[0].id, outputNode.inputs[0].id),
                ],
            },
            macros: [{
                id: "MACRO",
                title: "Macro",
                graph: {
                    nodes: [mIn, mConst, mOut],
                    connections: [
                        // @ts-ignore
                        createConnection(mIn.outputs[0].id, mConst.inputs[0].id),
                        // @ts-ignore
                        createConnection(mConst.outputs[0].id, mOut.inputs[0].id),
                    ],
                },
            }],
        };

        const compiled = await compileToBoth(
            graph,
            { [boolport.id]: true },
            44100,
            1,
        );
        for (const r of ["js", "wasm"] as const) {
            const res = compiled[r].run([0, 0], 0);
            assertStereo(
                res,
                [0, 0],
                `${r}, ${res}`,
            );
        }

        const compiled2 = await compileToBoth(
            graph,
            { [boolport.id]: false },
            44100,
            1,
        );
        for (const r of ["js", "wasm"] as const) {
            const res = compiled2[r].run([0, 0], 0);
            assertStereo(
                res,
                [5, 0],
                `${r}, ${res}`,
            );
        }
    });

    it("Macro - boolean gate", async() => {

        /*
         * MACROS:
         *
         * in ─ not - d const 5 ── out
         *
         * MAIN:
         *
         * l  MACROS ─ l
         *    |
         * r  |         r
         * d -|
         */

        const mIn = createMacroDefinitionInputNode();
        mIn.outputs.push({
            type: "macroDefinitionInput.output",
            valueType: "boolean",
            id: generateId(),
            title: "in",
        });
        const mOut = createMacroDefinitionOutputNode();
        mOut.inputs.push({
            type: "macroDefinitionOutput.input",
            valueType: "number",
            id: generateId(),
            title: "out",
        });
        const mConst = createConstNode(5);

        const mNotGate = createGateOpNode("not");
        const mUse = createMacroUsageNode("MACRO");

        mUse.inputs = reverbCreateMacroUsageInputs(mIn);
        mUse.outputs = reverbCreateMacroUsageOutputs(mOut);

        const inputNode = createInputNode(0, 0);
        const boolport = createReverbMainInputCheckboxNodePort("aaaa");
        inputNode.outputs.push(boolport);
        const graph: WholeReverbGraph = {
            main: {
                nodes: [inputNode, outputNode, mUse],
                connections: [
                    // @ts-ignore
                    createConnection(inputNode.outputs[3].id, mUse.inputs[0].id),
                    // @ts-ignore
                    createConnection(mUse.outputs[0].id, outputNode.inputs[0].id),
                ],
            },
            macros: [{
                id: "MACRO",
                title: "Macro",
                graph: {
                    nodes: [mIn, mConst, mNotGate, mOut],
                    connections: [
                        // @ts-ignore
                        createConnection(mNotGate.outputs[0].id, mConst.inputs[0].id),
                        // @ts-ignore
                        createConnection(mIn.outputs[0].id, mNotGate.inputs[0].id),
                        // @ts-ignore
                        createConnection(mConst.outputs[0].id, mOut.inputs[0].id),
                    ],
                },
            }],
        };

        const compiled = await compileToBoth(
            graph,
            { [boolport.id]: false },
            44100,
            1,
        );
        for (const r of ["js", "wasm"] as const) {
            const res = compiled[r].run([0, 0], 0);
            assertStereo(
                res,
                [0, 0],
                `${r}, ${res}`,
            );
        }

        const compiled2 = await compileToBoth(
            graph,
            { [boolport.id]: true },
            44100,
            1,
        );
        for (const r of ["js", "wasm"] as const) {
            const res = compiled2[r].run([0, 0], 0);
            assertStereo(
                res,
                [5, 0],
                `${r}, ${res}`,
            );
        }
    });
});
