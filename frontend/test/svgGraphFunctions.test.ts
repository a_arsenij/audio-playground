import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { assertArraysAreClose } from "@audio-playground/lib-test-utils/assertArraysAreClose";

import { createPiecewiseFunction, PiecewiseFunctionData } from "../src/utils/SvgGraphFunctions";

describe("Svg Graph Functions", () => {
    beforeEach(monkeyPatch);

    describe("createPiecewiseFunction", () => {
        it("linear", () => {
            const data1: PiecewiseFunctionData = {
                points: [
                    { x: 0, y: 0 },
                    { x: 1, y: 1 },
                ],
                pieceData: [
                    {
                        fun: "linear",
                        params: [],
                    },
                ],
            };

            const fun1 = createPiecewiseFunction(data1);

            const X = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
            const EXPECTED = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];

            const Y = X.map((x) => {
                return fun1(x);
            });

            assertArraysAreClose(Y, EXPECTED);

        });
        it("power", () => {
            const data1: PiecewiseFunctionData = {
                points: [
                    { x: 0, y: 0 },
                    { x: 1, y: 1 },
                ],
                pieceData: [
                    {
                        fun: "power",
                        params: [0.75],
                    },
                ],
            };

            const fun1 = createPiecewiseFunction(data1);

            const X = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
            const EXPECTED = [0, 0.001, 0.008, 0.027, 0.064, 0.125, 0.216, 0.343, 0.512, 0.729, 1];

            const Y = X.map((x) => {
                return fun1(x);
            });

            assertArraysAreClose(Y, EXPECTED);

        });
        it("3 linear pieces", () => {
            const data1: PiecewiseFunctionData = {
                points: [
                    { x: 0, y: 0 },
                    { x: 0.1, y: 0.5 },
                    { x: 0.8, y: 0.5 },
                    { x: 1, y: 0.3 },
                ],
                pieceData: [
                    {
                        fun: "linear",
                        params: [],
                    }, {
                        fun: "linear",
                        params: [],
                    }, {
                        fun: "linear",
                        params: [],
                    },
                ],
            };

            const fun1 = createPiecewiseFunction(data1);

            const X = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
            const EXPECTED = [0, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.4, 0.3];

            const Y = X.map((x) => {
                return fun1(x);
            });

            assertArraysAreClose(Y, EXPECTED);
        });
        it("invalid data fallback", () => {
            const data1: PiecewiseFunctionData = {
                points: [
                    { x: 0, y: 0 },
                    { x: 1, y: 1 },
                ],
                pieceData: [],
            };
            const data2: PiecewiseFunctionData = {
                points: [
                    { x: 0.5, y: 0 },
                    { x: 1, y: 1 },
                ],
                pieceData: [
                    {
                        fun: "linear",
                        params: [],
                    },
                ],
            };
            const data3: PiecewiseFunctionData = {
                points: [
                    { x: 0, y: 0 },
                    { x: 0.6, y: 1 },
                ],
                pieceData: [
                    {
                        fun: "linear",
                        params: [],
                    },
                ],
            };

            const fun1 = createPiecewiseFunction(data1);
            const fun2 = createPiecewiseFunction(data2);
            const fun3 = createPiecewiseFunction(data3);

            const X = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
            const EXPECTED = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

            const Y1 = X.map((x) => fun1(x));
            const Y2 = X.map((x) => fun2(x));
            const Y3 = X.map((x) => fun3(x));

            assertArraysAreClose(Y1, EXPECTED);
            assertArraysAreClose(Y2, EXPECTED);
            assertArraysAreClose(Y3, EXPECTED);
        });

    });
});
