import { beforeEach, describe, it } from "node:test";

import { acquireArrayFromPool, PooledArray } from "@audio-playground/lib-common/std/ArraysPool";
import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { deepCloneJson } from "@audio-playground/lib-common/std/Object";

import { EMPTY_PROJECT } from "../../src/engine/DefaultProject";
import { createEngineState } from "../../src/engine/EngineState";
import { renderMaster } from "../../src/engine/render/RenderMaster";
import { ReverbRenderer } from "../../src/plugins/list/Reverb/ReverbRenderer";
import { ReverbState } from "../../src/plugins/list/Reverb/ReverbState";
import { PluginCommonSettings } from "../../src/types/Plugin";
import { PluginRenderer } from "../../src/types/PluginRenderer";
import { NotesClip } from "../../src/types/project/Clip";
import { ClipUsage } from "../../src/types/project/ClipUsage";
import { addNthPlugin } from "../../src/utils/projectChanges/addPlugin";
import { reroutePluginOnChangeSettings } from "../../src/utils/projectChanges/reroutePluginOnChangeSettings";
import { createState } from "../state";


describe("No extra keys", () => {
    beforeEach(monkeyPatch);

    it("Reverb", () => {

        const [getProject, setProject] = createState(deepCloneJson(EMPTY_PROJECT));

        const instanceId = addNthPlugin(
            setProject,
            "master",
            ReverbRenderer as unknown as PluginRenderer<PluginCommonSettings, object>,
            0,
        );
        reroutePluginOnChangeSettings(
            setProject,
            "master",
            instanceId,
            { knobsValues: {}, checkboxesValues: {}, graph: { main: { nodes: [{ id: "_mainInput_c892f502e03b006e430e2d0afd8a791b", direction: "ltr", x: 0, y: 0, inputs: [], outputs: [{ type: "mainInput.signal", valueType: "number", id: "bd603689082d42dc19cf9f2adb685326", title: "Left" }, { type: "mainInput.signal", valueType: "number", id: "6ad5a22d6176e0fa330d25edf435bcac", title: "Right" }, { type: "mainInput.key", valueType: "number", id: "d6e90877868cca36fcd4ae7d78ecc354", title: "Key" }, { type: "mainInput.key", valueType: "number", id: "04b2776b186e3ff28b0cbd7912965812", title: "Key" }, { type: "mainInput.key", valueType: "number", id: "23fd4fd9692f1eeffd17c6ee30a51ab1", title: "Key" }, { type: "mainInput.key", valueType: "number", id: "8dec87dc9cb22d59e32d4058760e315c", title: "Key" }], type: "mainInput" }, { id: "_mainOutput_25a544ae44eb88bf6f1ad414fb31444d", direction: "ltr", x: 8, y: 0, inputs: [{ type: "mainOutput.signal", valueType: "number", id: "a9875f07a79ba32ffbb1269df96556c3", title: "Left" }, { type: "mainOutput.signal", valueType: "number", id: "305ed9a6b0489cc0500c02118e9a564b", title: "Right" }], outputs: [], type: "mainOutput" }], connections: [{ id: "7189fb8765ee3cde260bdc3647cc204b", portIdFrom: "d6e90877868cca36fcd4ae7d78ecc354", portIdTo: "a9875f07a79ba32ffbb1269df96556c3" }, { id: "16b441136222d47a2190dd695a16b428", portIdFrom: "04b2776b186e3ff28b0cbd7912965812", portIdTo: "a9875f07a79ba32ffbb1269df96556c3" }, { id: "80b2674d6079a5f977d516c2c3b1c4cf", portIdFrom: "23fd4fd9692f1eeffd17c6ee30a51ab1", portIdTo: "305ed9a6b0489cc0500c02118e9a564b" }, { id: "f5969b9c17e2bee762a0fd1a879c415c", portIdFrom: "8dec87dc9cb22d59e32d4058760e315c", portIdTo: "305ed9a6b0489cc0500c02118e9a564b" }] }, macros: [] }, elements: [], inputMidiChannels: [{ title: "Key", id: "15fd79f1681fcf240606f1b0a85b24ce:key" }], outputMidiChannels: [], inputSignalChannels: [{ title: "Left", id: "bd603689082d42dc19cf9f2adb685326" }, { title: "Right", id: "6ad5a22d6176e0fa330d25edf435bcac" }], outputSignalChannels: [{ title: "Left", id: "a9875f07a79ba32ffbb1269df96556c3" }, { title: "Right", id: "305ed9a6b0489cc0500c02118e9a564b" }] } as any,
        );

        const midiTrack: NotesClip = {
            id: "miditrack",
            title: "Midi track",
            type: "notes",
            notes: [
                {
                    id: "b997aee23643a92bc5d7c5d1ed9671fb",
                    timelineTimeStartBars: 0,
                    timelineTimeLengthBars: 1,
                    rowId: 91,
                },
                {
                    id: "933ad223addb7f8e39558c767e59b642",
                    timelineTimeStartBars: 1,
                    timelineTimeLengthBars: 1,
                    rowId: 90,
                },
                {
                    id: "c76802fb9fd477c49637c7778edb3cbd",
                    timelineTimeStartBars: 2,
                    timelineTimeLengthBars: 1,
                    rowId: 88,
                },
                {
                    id: "a6844655676d0296d7417e55bf2a1b17",
                    timelineTimeStartBars: 3,
                    timelineTimeLengthBars: 1,
                    rowId: 86,
                },
            ],
        };
        const usage: ClipUsage = {
            id: "fde7fa2bd539d110066da36950e0f823",
            rowId: "master",
            timelineTimeStartBars: 0,
            timelineTimeLengthBars: 4,
            sourceTimeStart: 0,
            sourceTimeLength: 4,
            clipId: "miditrack",
        };
        setProject((p) => ({
            ...p,
            clips: [midiTrack],
            clipsUsages: [usage],
        }));

        const p = getProject();
        const engineState = createEngineState();
        const BUFFER_LENGTH = engineState.project.get().bufferLength;
        engineState.setProject(p);
        engineState.loop.set({
            type: "playlist",
            borders: {
                timeStartBar: 0,
                timeEndBar: 4,
            },
        });
        engineState.playingStartedAtSamples.set(
            engineState.timeSamples.get() + BUFFER_LENGTH,
        );

        const buffer: [PooledArray, PooledArray] = [
            acquireArrayFromPool(BUFFER_LENGTH),
            acquireArrayFromPool(BUFFER_LENGTH),
        ];
        const SAMPLE_RATE = engineState.project.get().sampleRate;

        let i = 0;
        let timeSamples = 0;
        const OVERPOLYPHONY_THRESHOLD = 2;
        const state = engineState.getStateForPlugin(
            instanceId,
            ReverbRenderer.name,
        ) as ReverbState;
        while (true) {
            const fromTimeSecAbs = timeSamples / SAMPLE_RATE;
            let msg: string;
            {
                const toTimeSecAbs = (timeSamples + BUFFER_LENGTH - 1) / SAMPLE_RATE;
                const fromTimeSecSong = engineState.timeBarsToTimeSamples(engineState.getCurrentTimeBars(0)) / SAMPLE_RATE;
                const toTimeSecSong = engineState.timeBarsToTimeSamples(engineState.getCurrentTimeBars(BUFFER_LENGTH - 1)) / SAMPLE_RATE;
                msg = `Rendered ${fromTimeSecAbs.toFixed(2)}..${toTimeSecAbs.toFixed(2)} abs sec (${fromTimeSecSong.toFixed(2)}..${toTimeSecSong.toFixed(2)} song sec) / iter ${i}...`;
            }
            const [everyLevelIsZero, channelsLevels] = renderMaster(
                engineState,
                engineState.sharedBuffer.get(),
                buffer,
            );
            {
                timeSamples += BUFFER_LENGTH;
                i++;
                const states = state.cache.portsStates.filter((s) => s.key !== undefined);
                if (states.length >= OVERPOLYPHONY_THRESHOLD) {
                    console.log(msg);
                    throw new Error(`Overpolyphony x${OVERPOLYPHONY_THRESHOLD} found!`);
                }
                if (fromTimeSecAbs > 60) {
                    break;
                }
            }
        }
        console.log("No overpolyphony found");


    });

});
