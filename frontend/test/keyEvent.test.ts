import { describe, it } from "node:test";

import assert from "assert";

import {
    createNoteOffEvent,
    createNoteOnEvent,
    getKeyEventKey,
    getKeyEventTimeSample,
    getKeyEventType, getKeyEventVelocity,
} from "../src/utils/KeyEvent";


describe("Key event", () => {

    const MAX_KEY = 127;
    const MAX_VELOCITY = 127;
    const TIMES = [
        0,
        1,
        3,
        13,
        255,
        256,
        1027,
        60 * 48000,
        60 * 60 * 48000,
        24 * 60 * 60 * 48000,
    ];

    it("Off", () => {
        for (const t of TIMES) {
            for (let v = 0; v <= MAX_VELOCITY; v++) {
                for (let k = 0; k <= MAX_KEY; k++) {
                    const enc = createNoteOffEvent(k, v, t);
                    const decType = getKeyEventType(enc);
                    const decKey = getKeyEventKey(enc);
                    const decVelocity = getKeyEventVelocity(enc);
                    const decTime = getKeyEventTimeSample(enc);
                    assert(decType === "noteOff");
                    assert(decKey === k);
                    assert(decVelocity === v);
                    assert(decTime === t);
                }
            }
        }
    });

    it("On", () => {
        for (const t of TIMES) {
            for (let v = 0; v <= MAX_VELOCITY; v++) {
                for (let k = 0; k <= MAX_KEY; k++) {
                    const enc = createNoteOnEvent(k, v, t);
                    const decType = getKeyEventType(enc);
                    const decKey = getKeyEventKey(enc);
                    const decVelocity = getKeyEventVelocity(enc);
                    const decTime = getKeyEventTimeSample(enc);
                    assert(decType === "noteOn");
                    assert(decKey === k);
                    assert(decVelocity === v);
                    assert(decTime === t);
                }
            }
        }
    });

});

