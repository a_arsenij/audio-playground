import { NotAFunction } from "@audio-playground/lib-common/types/UtilityTypes";
import { applySetState } from "@audio-playground/lib-frontend/std/ApplySetState";
import { SetState, SetStateValue } from "@audio-playground/lib-frontend/std/SetState";


export function createState<T extends NotAFunction>(
    initial: T,
): [() => T, SetState<T>] {
    let value = initial;
    return [
        () => value,
        (v: SetStateValue<T>) => {
            value = applySetState(v, value);
        },
    ];
}
