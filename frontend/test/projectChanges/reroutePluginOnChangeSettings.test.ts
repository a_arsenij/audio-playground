import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { deepCloneJson } from "@audio-playground/lib-common/std/Object";
import { areArraysUnorderlyEqualByJsonValues } from "@audio-playground/lib-common/std/Set";
import assert from "assert";

import { EMPTY_PROJECT } from "../../src/engine/DefaultProject";
import { PluginCommonSettings } from "../../src/types/Plugin";
import { createAndAddNewChannel, createNewChannelPort } from "../../src/utils/projectChanges/addChannel";
import { addNthPlugin } from "../../src/utils/projectChanges/addPlugin";
import { checkNoOrphanRoutes } from "../../src/utils/projectChanges/checkNoOrphanRoutes";
import { reroutePluginOnChangeSettings } from "../../src/utils/projectChanges/reroutePluginOnChangeSettings";
import { createState } from "../state";
import { createDummyPlugin } from "./utils";

export const dummySynthPlugin1 = createDummyPlugin({
    inputMidiChannels: [
        createNewChannelPort("Notes"),
    ],
    outputMidiChannels: [],
    inputSignalChannels: [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ],
    outputSignalChannels: [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ],
});
export const dummySynthPlugin2 = createDummyPlugin({
    inputMidiChannels: [
        createNewChannelPort("Notes"),
    ],
    outputMidiChannels: [],
    inputSignalChannels: [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ],
    outputSignalChannels: [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ],
});

export const dummyComboPlugin1 = createDummyPlugin({
    inputMidiChannels: [
        createNewChannelPort("Notes"),
    ],
    outputMidiChannels: [
        createNewChannelPort("Notes"),
    ],
    inputSignalChannels: [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ],
    outputSignalChannels: [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ],
});
const dummyComboPlugin1Preset: PluginCommonSettings = {
    inputSignalChannels: [
        {
            id: "dummySynthPlugin1Preset.signalIn.left",
            title: "Left",
        },
        {
            id: "dummySynthPlugin1Preset.signalIn.right",
            title: "Right",
        },
    ],
    outputSignalChannels: [
        {
            id: "dummySynthPlugin1Preset.signalOut.left",
            title: "Left",
        },
        {
            id: "dummySynthPlugin1Preset.signalOut.right",
            title: "Right",
        },
    ],
    inputMidiChannels: [
        {
            id: "dummySynthPlugin1Preset.midiIn.key",
            title: "Notes",
        },
    ],
    outputMidiChannels: [
        {
            id: "dummySynthPlugin1Preset.midiOut.key",
            title: "Notes",
        },
    ],
};

describe("Replace plugin settings", () => {
    beforeEach(monkeyPatch);

    it("Replace synth with preset", () => {

        const [getProject, setProject] = createState(deepCloneJson(EMPTY_PROJECT));
        createAndAddNewChannel(setProject);
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummySynthPlugin1,
            0,
        );

        function checkExpectedConnection() {
            const project = getProject();
            const channel = project.channels[1]!;
            const plugin = channel.plugins[0]!;

            assert(
                areArraysUnorderlyEqualByJsonValues(
                    channel.signalPluginsRouting,
                    [
                        {
                            from: channel.inputSignalPorts[0]?.id,
                            to: plugin.settings.inputSignalChannels[0]!.id,
                        },
                        {
                            from: channel.inputSignalPorts[1]?.id,
                            to: plugin.settings.inputSignalChannels[1]!.id,
                        },
                        {
                            from: plugin.settings.outputSignalChannels[0]!.id,
                            to: channel.outputSignalPorts[0]?.id,
                        },
                        {
                            from: plugin.settings.outputSignalChannels[1]!.id,
                            to: channel.outputSignalPorts[1]?.id,
                        },
                    ],
                ),
                "Routes are from channel inputs, through plugin, to channel outputs",
            );

            assert(
                areArraysUnorderlyEqualByJsonValues(
                    channel.midiPluginsRouting,
                    [
                        {
                            from: channel.inputMidiPorts[0]?.id,
                            to: plugin.settings.inputMidiChannels[0]!.id,
                        },
                    ],
                ),
                "Midi channel input should be routed to plugin",
            );
        }


        checkExpectedConnection();

        reroutePluginOnChangeSettings(
            setProject,
            getProject().channels[1]!.id,
            getProject().channels[1]!.plugins[0]!.id,
            dummySynthPlugin2.createSettings({ pluginInstanceId: getProject().channels[1]!.plugins[0]!.id }),
        );

        checkExpectedConnection();
        checkNoOrphanRoutes(getProject());
        checkExpectedConnection();

    });

    it("Ports ids are not changed on minor change", () => {

        const [getProject, setProject] = createState(deepCloneJson(EMPTY_PROJECT));
        createAndAddNewChannel(setProject);
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyComboPlugin1,
            0,
        );
        let proj = getProject();
        const inSignalIdsBefore = proj.channels[1]!.plugins[0]!.settings.inputSignalChannels.map((c) => c.id);
        const outSignalIdsBefore = proj.channels[1]!.plugins[0]!.settings.outputSignalChannels.map((c) => c.id);
        const inMidiIdsBefore = proj.channels[1]!.plugins[0]!.settings.inputMidiChannels.map((c) => c.id);
        const outMidiIdsBefore = proj.channels[1]!.plugins[0]!.settings.outputMidiChannels.map((c) => c.id);
        reroutePluginOnChangeSettings(
            setProject,
            getProject().channels[1]!.id,
            getProject().channels[1]!.plugins[0]!.id,
            { ...getProject().channels[1]!.plugins[0]!.settings },
        );
        proj = getProject();
        const inSignalIdsAfter = proj.channels[1]!.plugins[0]!.settings.inputSignalChannels.map((c) => c.id);
        const outSignalIdsAfter = proj.channels[1]!.plugins[0]!.settings.outputSignalChannels.map((c) => c.id);
        const inMidiIdsAfter = proj.channels[1]!.plugins[0]!.settings.inputMidiChannels.map((c) => c.id);
        const outMidiIdsAfter = proj.channels[1]!.plugins[0]!.settings.outputMidiChannels.map((c) => c.id);
        assert(
            areArraysUnorderlyEqualByJsonValues(
                inSignalIdsBefore,
                inSignalIdsAfter,
            ),
            "Ids should be same",
        );
        assert(
            areArraysUnorderlyEqualByJsonValues(
                outSignalIdsBefore,
                outSignalIdsAfter,
            ),
            "Ids should be same",
        );
        assert(
            areArraysUnorderlyEqualByJsonValues(
                inMidiIdsBefore,
                inMidiIdsAfter,
            ),
            "Ids should be same",
        );
        assert(
            areArraysUnorderlyEqualByJsonValues(
                outMidiIdsBefore,
                outMidiIdsAfter,
            ),
            "Ids should be same",
        );

    });

    it("Ports ids are changed on loading preset", () => {

        const [getProject, setProject] = createState(deepCloneJson(EMPTY_PROJECT));
        createAndAddNewChannel(setProject);
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyComboPlugin1,
            0,
        );
        reroutePluginOnChangeSettings(
            setProject,
            getProject().channels[1]!.id,
            getProject().channels[1]!.plugins[0]!.id,
            dummyComboPlugin1Preset,
        );
        const proj = getProject();
        const inSignalIds = proj.channels[1]!.plugins[0]!.settings.inputSignalChannels.map((c) => c.id);
        const outSignalIds = proj.channels[1]!.plugins[0]!.settings.outputSignalChannels.map((c) => c.id);
        const inMidiIds = proj.channels[1]!.plugins[0]!.settings.inputMidiChannels.map((c) => c.id);
        const outMidiIds = proj.channels[1]!.plugins[0]!.settings.outputMidiChannels.map((c) => c.id);
        assert(
            !areArraysUnorderlyEqualByJsonValues(
                inSignalIds,
                dummyComboPlugin1Preset.inputSignalChannels.map((c) => c.id),
            ),
            "Ids should be different",
        );
        assert(
            !areArraysUnorderlyEqualByJsonValues(
                outSignalIds,
                dummyComboPlugin1Preset.outputSignalChannels.map((c) => c.id),
            ),
            "Ids should be different",
        );
        assert(
            !areArraysUnorderlyEqualByJsonValues(
                inMidiIds,
                dummyComboPlugin1Preset.inputMidiChannels.map((c) => c.id),
            ),
            "Ids should be different",
        );
        assert(
            !areArraysUnorderlyEqualByJsonValues(
                outMidiIds,
                dummyComboPlugin1Preset.outputMidiChannels.map((c) => c.id),
            ),
            "Ids should be different",
        );

    });
});

