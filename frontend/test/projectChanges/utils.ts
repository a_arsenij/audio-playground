import { PluginCommonSettings } from "../../src/types/Plugin";
import { PluginRenderer } from "../../src/types/PluginRenderer";
import { createNewChannelPort } from "../../src/utils/projectChanges/addChannel";


export function createDummyPlugin(ports: PluginCommonSettings): PluginRenderer<PluginCommonSettings, object> {
    return {
        name: "Dummy plugin",
        createSettings: () => ports,
        createState: () => ({}),
        destroy: () => {
        },
        renderBuffer: () => {
        },
    };
}

export const dummyEffectPlugin = createDummyPlugin({
    inputMidiChannels: [],
    outputMidiChannels: [],
    inputSignalChannels: [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ],
    outputSignalChannels: [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ],
});

export const dummySynthPlugin = createDummyPlugin({
    inputMidiChannels: [
        createNewChannelPort("Notes"),
    ],
    outputMidiChannels: [],
    inputSignalChannels: [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ],
    outputSignalChannels: [
        createNewChannelPort("Left"),
        createNewChannelPort("Right"),
    ],
});

export const dummyMidiPlugin = createDummyPlugin({
    inputMidiChannels: [
        createNewChannelPort("Notes"),
    ],
    outputMidiChannels: [
        createNewChannelPort("Notes"),
    ],
    inputSignalChannels: [],
    outputSignalChannels: [],
});
