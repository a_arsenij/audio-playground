import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { deepCloneJson } from "@audio-playground/lib-common/std/Object";
import { areArraysUnorderlyEqualByJsonValues } from "@audio-playground/lib-common/std/Set";
import assert from "assert";

import { EMPTY_PROJECT } from "../../src/engine/DefaultProject";
import { createAndAddNewChannel } from "../../src/utils/projectChanges/addChannel";
import { addNthPlugin } from "../../src/utils/projectChanges/addPlugin";
import { createState } from "../state";
import { dummyEffectPlugin, dummyMidiPlugin, dummySynthPlugin } from "./utils";



describe("Add plugin", () => {
    beforeEach(monkeyPatch);

    it("Add first effect plugin", () => {
        const [getProject, setProject] = createState(deepCloneJson(EMPTY_PROJECT));
        createAndAddNewChannel(setProject);

        const oldChannelsConnections = getProject().channelsRouting;
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyEffectPlugin,
            0,
        );

        const project = getProject();

        assert(
            JSON.stringify(project.channelsRouting) === JSON.stringify(oldChannelsConnections),
            "Adding plugins should not modify channels connections",
        );

        const channel = project.channels[1]!;
        assert(
            channel.plugins.length === 1,
            "There should be 1 plugin after adding plugin",
        );
        const plugin = channel.plugins[0]!;

        assert(
            areArraysUnorderlyEqualByJsonValues(
                channel.signalPluginsRouting,
                [
                    {
                        from: channel.inputSignalPorts[0]?.id,
                        to: plugin.settings.inputSignalChannels[0]!.id,
                    },
                    {
                        from: channel.inputSignalPorts[1]?.id,
                        to: plugin.settings.inputSignalChannels[1]!.id,
                    },
                    {
                        from: plugin.settings.outputSignalChannels[0]!.id,
                        to: channel.outputSignalPorts[0]?.id,
                    },
                    {
                        from: plugin.settings.outputSignalChannels[1]!.id,
                        to: channel.outputSignalPorts[1]?.id,
                    },
                ],
            ),
            "Routes are from channel inputs, through plugin, to channel outputs",
        );

        assert(
            channel.midiPluginsRouting.length === 0,
            "There should be no midi routes",
        );
    });

    it("Add first synth plugin", () => {
        const [getProject, setProject] = createState(deepCloneJson(EMPTY_PROJECT));
        createAndAddNewChannel(setProject);

        const oldChannelsConnections = getProject().channelsRouting;
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummySynthPlugin,
            0,
        );

        const project = getProject();

        assert(
            JSON.stringify(project.channelsRouting) === JSON.stringify(oldChannelsConnections),
            "Adding plugins should not modify channels connections",
        );

        const channel = project.channels[1]!;
        assert(
            channel.plugins.length === 1,
            "There should be 1 plugin after adding plugin",
        );
        const plugin = channel.plugins[0]!;

        assert(
            areArraysUnorderlyEqualByJsonValues(
                channel.signalPluginsRouting,
                [
                    {
                        from: channel.inputSignalPorts[0]?.id,
                        to: plugin.settings.inputSignalChannels[0]!.id,
                    },
                    {
                        from: channel.inputSignalPorts[1]?.id,
                        to: plugin.settings.inputSignalChannels[1]!.id,
                    },
                    {
                        from: plugin.settings.outputSignalChannels[0]!.id,
                        to: channel.outputSignalPorts[0]?.id,
                    },
                    {
                        from: plugin.settings.outputSignalChannels[1]!.id,
                        to: channel.outputSignalPorts[1]?.id,
                    },
                ],
            ),
            "Routes are from channel inputs, through plugin, to channel outputs",
        );

        assert(
            areArraysUnorderlyEqualByJsonValues(
                channel.midiPluginsRouting,
                [
                    {
                        from: channel.inputMidiPorts[0]?.id,
                        to: plugin.settings.inputMidiChannels[0]!.id,
                    },
                ],
            ),
            "Midi channel input should be routed to plugin",
        );
    });

    it("Add sound effect plugin in the middle", () => {
        const [getProject, setProject] = createState(deepCloneJson(EMPTY_PROJECT));
        createAndAddNewChannel(setProject);

        /*
         * Add first
         */
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyEffectPlugin,
            0,
        );
        /*
         * Add last
         */
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyEffectPlugin,
            1,
        );
        /*
         * Add between
         */
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyEffectPlugin,
            1,
        );

        const project = getProject();

        const channel = project.channels[1]!;
        assert(
            channel.plugins.length === 3,
            "There should be 3 plugins after adding 3 plugins",
        );
        const p1 = channel.plugins[0]!;
        const p2 = channel.plugins[1]!;
        const p3 = channel.plugins[2]!;

        assert(
            areArraysUnorderlyEqualByJsonValues(
                channel.signalPluginsRouting,
                [
                    {
                        from: channel.inputSignalPorts[0]!.id,
                        to: p3.settings.inputSignalChannels[0]!.id,
                    },
                    {
                        from: channel.inputSignalPorts[1]!.id,
                        to: p3.settings.inputSignalChannels[1]!.id,
                    },

                    {
                        from: p3.settings.outputSignalChannels[0]!.id,
                        to: p2.settings.inputSignalChannels[0]!.id,
                    },
                    {
                        from: p3.settings.outputSignalChannels[1]!.id,
                        to: p2.settings.inputSignalChannels[1]!.id,
                    },

                    {
                        from: p2.settings.outputSignalChannels[0]!.id,
                        to: p1.settings.inputSignalChannels[0]!.id,
                    },
                    {
                        from: p2.settings.outputSignalChannels[1]!.id,
                        to: p1.settings.inputSignalChannels[1]!.id,
                    },

                    {
                        from: p1.settings.outputSignalChannels[0]!.id,
                        to: channel.outputSignalPorts[0]!.id,
                    },
                    {
                        from: p1.settings.outputSignalChannels[1]!.id,
                        to: channel.outputSignalPorts[1]!.id,
                    },
                ],
            ),
            "Routes are from channel inputs, through plugin, to channel outputs",
        );

        assert(
            channel.midiPluginsRouting.length === 0,
            "There should be no midi routes",
        );
    });

    it("Add midi transform plugin in the middle", () => {
        const [getProject, setProject] = createState(deepCloneJson(EMPTY_PROJECT));
        createAndAddNewChannel(setProject);

        /*
         * Add first
         */
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyMidiPlugin,
            0,
        );
        /*
         * Add last
         */
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyMidiPlugin,
            1,
        );
        /*
         * Add between
         */
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyMidiPlugin,
            1,
        );

        const project = getProject();

        const channel = project.channels[1]!;
        assert(
            channel.plugins.length === 3,
            "There should be 3 plugins after adding 3 plugins",
        );
        const p1 = channel.plugins[0]!;
        const p2 = channel.plugins[1]!;
        const p3 = channel.plugins[2]!;

        assert(
            areArraysUnorderlyEqualByJsonValues(
                channel.midiPluginsRouting,
                [
                    {
                        from: channel.inputMidiPorts[0]!.id,
                        to: p3.settings.inputMidiChannels[0]!.id,
                    },

                    {
                        from: p3.settings.outputMidiChannels[0]!.id,
                        to: p2.settings.inputMidiChannels[0]!.id,
                    },

                    {
                        from: p2.settings.outputMidiChannels[0]!.id,
                        to: p1.settings.inputMidiChannels[0]!.id,
                    },
                ],
            ),
            "Routes are from channel inputs, through plugin, to channel outputs",
        );

        assert(
            channel.signalPluginsRouting.length === 0,
            "There should be no signal routes",
        );
    });

    it("Realistic chain", () => {
        const [getProject, setProject] = createState(deepCloneJson(EMPTY_PROJECT));
        createAndAddNewChannel(setProject);

        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyMidiPlugin,
            0,
        );
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummySynthPlugin,
            0,
        );
        addNthPlugin(
            setProject,
            getProject().channels[1]?.id!,
            dummyEffectPlugin,
            0,
        );

        const project = getProject();

        const channel = project.channels[1]!;
        assert(
            channel.plugins.length === 3,
            "There should be 3 plugins after adding 3 plugins",
        );
        const p1 = channel.plugins[0]!;
        const p2 = channel.plugins[1]!;
        const p3 = channel.plugins[2]!;

        assert(
            areArraysUnorderlyEqualByJsonValues(
                channel.signalPluginsRouting,
                [
                    {
                        from: p2.settings.outputSignalChannels[0]!.id,
                        to: p1.settings.inputSignalChannels[0]!.id,
                    },
                    {
                        from: p2.settings.outputSignalChannels[1]!.id,
                        to: p1.settings.inputSignalChannels[1]!.id,
                    },

                    {
                        from: p1.settings.outputSignalChannels[0]!.id,
                        to: channel.outputSignalPorts[0]!.id,
                    },
                    {
                        from: p1.settings.outputSignalChannels[1]!.id,
                        to: channel.outputSignalPorts[1]!.id,
                    },
                ],
            ),
            "Routes are from channel inputs, through plugin, to channel outputs",
        );

        assert(
            areArraysUnorderlyEqualByJsonValues(
                channel.midiPluginsRouting,
                [
                    {
                        from: channel.inputMidiPorts[0]!.id,
                        to: p3.settings.inputMidiChannels[0]!.id,
                    },

                    {
                        from: p3.settings.outputMidiChannels[0]!.id,
                        to: p2.settings.inputMidiChannels[0]!.id,
                    },
                ],
            ),
            "Routes are from channel inputs, through plugin, to channel outputs",
        );

    });

});
