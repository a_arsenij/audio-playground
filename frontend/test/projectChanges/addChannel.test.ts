import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/std/MonkeyPatching";
import { deepCloneJson } from "@audio-playground/lib-common/std/Object";
import { areArraysUnorderlyEqualByJsonValues } from "@audio-playground/lib-common/std/Set";
import assert from "assert";

import { EMPTY_PROJECT } from "../../src/engine/DefaultProject";
import { createAndAddNewChannel } from "../../src/utils/projectChanges/addChannel";
import { createState } from "../state";

describe("Add channel", () => {
    beforeEach(monkeyPatch);

    it("Add first non-master channel", async() => {

        const [getProject, setProject] = createState(deepCloneJson(EMPTY_PROJECT));

        assert(
            getProject().channels.length === 1,
            "There should be only one channel in new project",
        );

        createAndAddNewChannel(setProject);

        const project = getProject();

        assert(
            project.channels.length === 2,
            "There should be 2 channels",
        );

        const c1 = project.channels[0]!;
        const c2 = project.channels[1]!;

        assert(
            c2.outputSignalPorts.length === 2,
            "There should be 2 signal output ports in new channel",
        );
        assert(
            c2.inputSignalPorts.length === 2,
            "There should be 2 signal input ports in new channel",
        );
        assert(
            c2.inputMidiPorts.length === 1,
            "There should be 1 midi input ports in new channel",
        );
        assert(
            c2.outputMidiPorts.length === 0,
            "There should be no midi output ports in new channel",
        );

        assert(
            areArraysUnorderlyEqualByJsonValues(
                project.channelsRouting,
                [
                    {
                        from: c2.outputSignalPorts[0]!.id,
                        to: c1.inputSignalPorts[0]!.id,
                    },
                    {
                        from: c2.outputSignalPorts[1]!.id,
                        to: c1.inputSignalPorts[1]!.id,
                    },
                ],
            ),
            "Routes are from channel inputs to channel outputs",
        );

        assert(
            c2.plugins.length === 0,
            "There should be no plugins in new channel",
        );

        assert(
            areArraysUnorderlyEqualByJsonValues(
                c2.signalPluginsRouting,
                [
                    {
                        from: c2.inputSignalPorts[0]!.id,
                        to: c2.outputSignalPorts[0]!.id,
                    },
                    {
                        from: c2.inputSignalPorts[1]!.id,
                        to: c2.outputSignalPorts[1]!.id,
                    },
                ],
            ),
            "Routes are from channel inputs to channel outputs",
        );
    });

});
