#!/usr/bin/env zx

await $`

npx ts-node \
    --require module-alias/register tools/generate-samples-library-metainfo/index.ts \
    --workspace=@audio-playground/generate-samples-library-metainfo -- \
    --wavsFolder=frontend/assets/samples-library \
    --metaFolder=samples-library-metainfo
cp -r samples-library-metainfo public/samples-library-metainfo

npm run build --workspace=@audio-playground/frontend
cp -r frontend/build/* public
cp -r frontend/assets/samples-library public/samples-library
`;
