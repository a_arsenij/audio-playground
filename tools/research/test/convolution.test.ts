import { describe, it } from "node:test";

import { furieConvolution, hartleyConvolution } from "../utils/Convolution";
import {generateRandomArray} from "@audio-playground/lib-test-utils/generateRandomArray";
import {assertArraysAreClose} from "@audio-playground/lib-test-utils/assertArraysAreClose";

describe("Convolution", () => {
    it("FFT Convolution should be equal to FHT Convolution", () => {
        const filter = generateRandomArray(100);
        const signal = generateRandomArray(100 + 10);
        const timeSample = 0;

        const out1 = new Float64Array(10);
        const out2 = new Float64Array(10);

        hartleyConvolution(filter, signal, timeSample, out2);

        furieConvolution(filter, signal, timeSample, out1);

        assertArraysAreClose(out1, out2);

    });
});
