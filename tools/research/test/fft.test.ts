import { describe, it } from "node:test";

import assert from "assert";

import { fft } from "../utils/FFT";
import {areArraysClose} from "@audio-playground/lib-test-utils/areArraysClose";

describe("FFt", () => {
    it("FFT - book case", () => {
        const realInput = new Float64Array([1, 0, 0, 1]);

        const resReal = new Float64Array(4);
        const resImag = new Float64Array(4);

        fft(resReal, resImag, realInput, true);

        const EXPECTED_REAL = new Float64Array([2, 1, 0, 1]);
        const EXPECTED_IMAG = new Float64Array([0, 1, 0, -1]);

        assert(areArraysClose(resReal, EXPECTED_REAL));
        assert(areArraysClose(resImag, EXPECTED_IMAG));

    });

    it("FFT - online FFT calculator case", () => {
        const realInput = new Float64Array([1, 2, 3, 2, 4, 3, 2, 3]);
        const imgInput = new Float64Array([0, 0, 0, 0, 1, 1, 1, 1]);

        const resReal = new Float64Array(8);
        const resImag = new Float64Array(8);

        fft(resReal, resImag, realInput, true, imgInput);

        const EXPECTED_REAL = new Float64Array([
            20.000000,
            -5.414214,
            0.000000,
            -3.414214,
            0.000000,
            -2.585786,
            0.000000,
            -0.585786,
        ]);

        assert(areArraysClose(resReal, EXPECTED_REAL));
    });

});
