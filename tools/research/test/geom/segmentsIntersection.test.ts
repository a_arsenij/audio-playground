import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/src/std/MonkeyPatching";
import assert from "assert";

import { getSegmentsIntersection } from "../../utils/Geom";
import {assertNumbersClose} from "@audio-playground/lib-test-utils/assertNumbersClose";

describe("Segments intersect", () => {
    beforeEach(monkeyPatch);

    it("Trivial cross ┼", () => {

        const a = getSegmentsIntersection(
            [{ x: 0, y: -1 }, { x: 0, y: 1 }],
            [{ x: -1, y: 0 }, { x: 1, y: 0 }],
        );

        assert(a !== undefined, "There should be intersection");
        assertNumbersClose(
            a!.x,
            0,
            0.00001,
            "Intersection should be at [0, 0]",
        );
        assertNumbersClose(
            a!.y,
            0,
            0.00001,
            "Intersection should be at [0, 0]",
        );

    });

    it("Trivial cross ╳", () => {

        const a = getSegmentsIntersection(
            [{ x: -1, y: -1 }, { x: 1, y: 1 }],
            [{ x: -1, y: 1 }, { x: 1, y: -1 }],
        );

        assert(a !== undefined, "There should be intersection");
        assertNumbersClose(
            a!.x,
            0,
            0.00001,
            "Intersection should be at [0, 0]",
        );
        assertNumbersClose(
            a!.y,
            0,
            0.00001,
            "Intersection should be at [0, 0]",
        );

    });

    it("Intersection lays on one of segments ┴", () => {

        const a = getSegmentsIntersection(
            [{ x: 0, y: -1 }, { x: 0, y: 1 }],
            [{ x: -1, y: 0 }, { x: 0, y: 0 }],
        );

        assert(a !== undefined, "There should be intersection");
        assertNumbersClose(
            a!.x,
            0,
            0.00001,
            "Intersection should be at [0, 0]",
        );
        assertNumbersClose(
            a!.y,
            0,
            0.00001,
            "Intersection should be at [0, 0]",
        );

    });

    it("Intersection lays out of segments", () => {

        const a = getSegmentsIntersection(
            [{ x: 0, y: -1 }, { x: 0, y: 1 }],
            [{ x: -1, y: 0 }, { x: -0.1, y: 0 }],
        );

        assert(a === undefined, "There should be no intersection");

    });

});
