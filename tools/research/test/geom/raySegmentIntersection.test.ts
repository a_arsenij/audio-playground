import { beforeEach, describe, it } from "node:test";

import { monkeyPatch } from "@audio-playground/lib-common/src/std/MonkeyPatching";
import assert from "assert";

import { getSegmentRayIntersection } from "../../utils/Geom";
import {assertNumbersClose} from "@audio-playground/lib-test-utils/assertNumbersClose";

describe("Segment and ray intersect", () => {
    beforeEach(monkeyPatch);

    it("Trivial cross ●┼", () => {

        const a = getSegmentRayIntersection(
            [{ x: 0, y: -1 }, { x: 0, y: 1 }],
            { x: -1, y: 0, angle: 0 },
        );

        assert(a !== undefined, "There should be intersection");
        assertNumbersClose(
            a!.x,
            0,
            0.00001,
            "Intersection should be at [0, 0]",
        );
        assertNumbersClose(
            a!.y,
            0,
            0.00001,
            "Intersection should be at [0, 0]",
        );

    });

    it("Trivial no cross ─● │", () => {

        const a = getSegmentRayIntersection(
            [{ x: 0, y: -1 }, { x: 0, y: 1 }],
            { x: -1, y: 0, angle: Math.PI },
        );

        assert(a === undefined, "There should be no intersection");

    });

    it("Trivial cross ┼●", () => {

        const a = getSegmentRayIntersection(
            [{ x: 0, y: -1 }, { x: 0, y: 1 }],
            { x: 1, y: 0, angle: Math.PI },
        );

        assert(a !== undefined, "There should be intersection");
        assertNumbersClose(
            a!.x,
            0,
            0.00001,
            "Intersection should be at [0, 0]",
        );
        assertNumbersClose(
            a!.y,
            0,
            0.00001,
            "Intersection should be at [0, 0]",
        );

    });

    it("Trivial no cross │ ●─", () => {

        const a = getSegmentRayIntersection(
            [{ x: 0, y: -1 }, { x: 0, y: 1 }],
            { x: 1, y: 0, angle: 0 },
        );

        assert(a === undefined, "There should be no intersection");

    });

    it("Cross .╳", () => {

        const a = getSegmentRayIntersection(
            [{ x: 1, y: 0 }, { x: 0, y: 1 }],
            { x: 0, y: 0, angle: Math.PI / 4 },
        );

        assert(a !== undefined, "There should be intersection");
        assertNumbersClose(
            a!.x,
            0.5,
            0.00001,
            "Intersection should be at [0.5, 0.5]",
        );
        assertNumbersClose(
            a!.y,
            0.5,
            0.00001,
            "Intersection should be at [0.5, 0.5]",
        );

    });

    it("Cross '╳", () => {

        const a = getSegmentRayIntersection(
            [{ x: 0, y: 0 }, { x: 1, y: 1 }],
            { x: 0, y: 1, angle: -Math.PI / 4 },
        );

        assert(a !== undefined, "There should be intersection");
        assertNumbersClose(
            a!.x,
            0.5,
            0.00001,
            "Intersection should be at [0.5, 0.5]",
        );
        assertNumbersClose(
            a!.y,
            0.5,
            0.00001,
            "Intersection should be at [0.5, 0.5]",
        );

    });

    it("Cross ╳.", () => {

        const a = getSegmentRayIntersection(
            [{ x: 0, y: 0 }, { x: 1, y: 1 }],
            { x: 1, y: 0, angle: Math.PI * 3 / 4 },
        );

        assert(a !== undefined, "There should be intersection");
        assertNumbersClose(
            a!.x,
            0.5,
            0.00001,
            "Intersection should be at [0.5, 0.5]",
        );
        assertNumbersClose(
            a!.y,
            0.5,
            0.00001,
            "Intersection should be at [0.5, 0.5]",
        );

    });

    it("Cross ╳'", () => {

        const a = getSegmentRayIntersection(
            [{ x: 0, y: 1 }, { x: 1, y: 0 }],
            { x: 1, y: 1, angle: Math.PI * 5 / 4 },
        );

        assert(a !== undefined, "There should be intersection");
        assertNumbersClose(
            a!.x,
            0.5,
            0.00001,
            "Intersection should be at [0.5, 0.5]",
        );
        assertNumbersClose(
            a!.y,
            0.5,
            0.00001,
            "Intersection should be at [0.5, 0.5]",
        );

    });

});
