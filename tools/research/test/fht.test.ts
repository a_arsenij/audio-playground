import { describe, it } from "node:test";

import assert from "assert";

import { fht } from "../utils/Hartley";
import {areArraysClose} from "@audio-playground/lib-test-utils/areArraysClose";

describe("FHT", () => {
    it("should work", () => {
        const inputs = [
            new Float64Array([0, 1, 0, 0]),
            new Float64Array([5, 7]),
        ];
        const EXPECTED = [
            new Float64Array([0.5, 0.5, -0.5, -0.5]),
            new Float64Array([6 * Math.sqrt(2), -1 * Math.sqrt(2)]),
        ];

        for (let i = 0; i < inputs.length; i++) {
            const output = fht(inputs[i]!);

            assert(areArraysClose(output, EXPECTED[i]!));
        }
    });

    it("FHT( FHT( A ) ) = A", () => {
        const input = new Float64Array([1, 3, 5, 7, -5, 24, 0, 0]);

        const res = fht(fht(input));
        assert(areArraysClose(input, res));
    });

});
