import { acquireArrayFromPool } from "@audio-playground/lib-common/src/std/ArraysPool";
import { FilterType } from "@audio-playground/lib-sound/src/FilterType";
import { frequencyToNote, NOTES_COUNT, noteToFrequency } from "@audio-playground/lib-sound/src/NoteFrequency";

import { SOUND_FFT_LENGTH } from "./FFT";

export function generateLowPassNotesSpectre(
    cutNote: number,
    target: Float64Array = acquireArrayFromPool(NOTES_COUNT),
): Float64Array {
    target.forEach((_, idx) => {
        target[idx] = idx > cutNote ? 0 : 1;
    });
    return target;
}

function mirrorSecondHalfOfArray(target: Float64Array) {
    const L_2 = target.length / 2;
    for (let idx = L_2 + 1; idx < target.length; idx++) {
        target[idx] = target[2 * L_2 - idx]!;
    }
}

export function generateFilterSpectre(
    type: FilterType,
    cutNote: number,
    resonance: number,
    target: Float64Array = acquireArrayFromPool(SOUND_FFT_LENGTH),
) {
    target.fill(1, 0, target.length);
    switch (type) {
        case "lowpass":
            generateLowPassFrequencySpectre(cutNote, target);
            break;
        case "highpass":
            generateHighPassFrequencySpectre(cutNote, target);
            break;
        case "bandpass": {
            generateLowPassFrequencySpectre(cutNote, target);
            generateHighPassFrequencySpectre(cutNote, target);
            break;
        }
        default:
            break;
    }
    generateResonancePeak(cutNote, resonance, target);
    mirrorSecondHalfOfArray(target);
    return target;
}

const RESONANCE_EPSILON = 0.0001;
const RESONANCE_WIDTH_NOTES = 6;
const RESONANCE_AMP = 10;

function generateResonancePeak(
    cutNote: number,
    resonance: number,
    target: Float64Array = acquireArrayFromPool(SOUND_FFT_LENGTH),
) {
    if (resonance < RESONANCE_EPSILON) {
        return;
    }
    const L_2 = target.length / 2;
    for (let idx = 0; idx <= L_2; idx++) {
        const note = frequencyToNote(idx);
        const diff = Math.abs(note - cutNote);
        if (diff <= RESONANCE_WIDTH_NOTES) {
            target[idx] += resonance *
                RESONANCE_AMP *
                (RESONANCE_WIDTH_NOTES - diff) /
                RESONANCE_WIDTH_NOTES;
        }
    }
}

function generateLowPassFrequencySpectre(
    cutNote: number,
    target: Float64Array = acquireArrayFromPool(SOUND_FFT_LENGTH),
) {
    const L_2 = target.length / 2;
    for (let idx = 0; idx <= L_2; idx++) {
        const note = frequencyToNote(idx);
        if (note < cutNote) {
            // target[idx] = 1;
        } else {
            target[idx] *= Math.max(0, 1 - (note - cutNote) / 6);
        }
    }
}

function generateHighPassFrequencySpectre(
    cutNote: number,
    target: Float64Array = acquireArrayFromPool(SOUND_FFT_LENGTH),
) {
    const L_2 = target.length / 2;
    for (let idx = 0; idx <= L_2; idx++) {
        const note = frequencyToNote(idx);
        if (note > cutNote) {
            // target[idx] = 1;
        } else {
            target[idx] *= Math.max(0, 1 - (cutNote - note) / 6);
        }
    }
}

export function notesSpectreToFrequencySpectre(
    notes: number[],
    target: Float64Array = acquireArrayFromPool(SOUND_FFT_LENGTH),
): Float64Array {
    const L_2 = target.length / 2;
    for (let idx = 1; idx <= L_2; idx++) {
        const startNote = frequencyToNote(idx);
        const endNote = frequencyToNote(idx + 1);

        const from = Math.floor(startNote);
        const to = Math.ceil(endNote);

        let value = 0;
        let divideBy = 0;

        if (startNote > from && endNote < to) {
            value += notes[from] || 0;
            divideBy += (endNote - from);
        } else if (startNote > from) {
            value += notes[from] || 0;
            divideBy += (startNote - from);
        } else if (endNote < to) {
            value += notes[to] || 0;
            divideBy += (to - endNote);
        }

        for (let note = from + 1; note < to; note++) {
            value += notes[note] || 0;
            divideBy += 1;
        }
        target[idx] = value / (divideBy || 1);
    }
    mirrorSecondHalfOfArray(target);

    return target;
}

export function frequencySpectreToNotesSpectre(
    frequencies: number[],
    target: Float64Array = acquireArrayFromPool(NOTES_COUNT),
): Float64Array {
    for (let idx = 0; idx < target.length; idx++) {
        const startFrequency = noteToFrequency(idx);
        const endFrequency = noteToFrequency(idx + 1);

        const from = Math.floor(startFrequency);
        const to = Math.ceil(endFrequency);

        let value = 0;
        let divideBy = 0;

        if (startFrequency > from && endFrequency < to) {
            value += frequencies[from] || 0;
            divideBy += (endFrequency - from);
        } else if (startFrequency > from) {
            value += frequencies[from] || 0;
            divideBy += (startFrequency - from);
        } else if (endFrequency < to) {
            value += frequencies[to] || 0;
            divideBy += (to - endFrequency);
        }

        for (let note = from + 1; note < to; note++) {
            value += frequencies[note] || 0;
            divideBy += 1;
        }
        target[idx] = value / (divideBy || 1);
    }
    return target;
}
