import { Complex, createComplex } from "@audio-playground/lib-common/src/math/Complex";
import { f64ArrayCopyTo } from "@audio-playground/lib-common/src/std/Array";
import { acquireArrayFromPool } from "@audio-playground/lib-common/src/std/ArraysPool";

function W(
    kn: number,
    N: number,
): Complex {
    const pow = kn * 2 * Math.PI / N;

    return [Math.cos(pow), -1 * Math.sin(pow)];
}

const W0_2 = W(0, 1);
const W1_2 = W(1, 2);
const W_MINUS_1_2 = W(-1, 2);

function reorderIndex(
    n: number,
    bits: number,
) {
    let nn = n;
    const rems: number[] = [];
    while (rems.length < bits) {
        rems.push(nn % 2);
        nn = Math.floor(nn / 2);
    }
    nn = 0;
    for (const d of rems) {
        nn *= 2;
        nn += d;
    }
    return nn;
}

function generateReordering(
    N: number,
): number[] {
    const bits = Math.log2(N);
    return [...Array(N)].map((_, idx) => reorderIndex(idx, bits));
}

const reorderingsCache: number[][] = [];

function getReordering(
    N: number,
): number[] {
    let old = reorderingsCache[N];
    if (!old) {
        old = generateReordering(N);
        reorderingsCache[N] = old;
    }
    return old;
}

export function fft(
    outputReal: Float64Array,
    outputImaginary: Float64Array,
    real: Float64Array,
    isDirect: boolean = true,
    imaginary: Float64Array | undefined = undefined,
) {
    const N = real.length;
    const reordering = getReordering(N);
    const DEEPNESS = Math.log2(N);

    const directionSignum = isDirect ? 1 : -1;
    const W1_2_ = isDirect ? W1_2 : W_MINUS_1_2;

    let currLayerReal = acquireArrayFromPool(N);
    let currLayerImag = acquireArrayFromPool(N);
    let nextLayerReal = acquireArrayFromPool(N);
    let nextLayerImag = acquireArrayFromPool(N);

    // const www = [...Array(N)].map((_, idx) => W(directionSignum * idx, N));

    const tmp1 = createComplex();
    const tmp2 = createComplex();
    const tmp3 = createComplex();
    const tmp4 = createComplex();

    for (let i = 0; i < N; i += 2) {
        const r1 = reordering[i]!;
        tmp1[0] = real[r1]!;
        tmp1[1] = imaginary ? imaginary[r1]! : 0;

        const r2 = reordering[i + 1]!;
        tmp2[0] = real[r2]!;
        tmp2[1] = imaginary ? imaginary[r2]! : 0;

        tmp3[0] = (tmp1[0] * W0_2[0] - tmp1[1] * W0_2[1]);
        tmp3[1] = (tmp1[0] * W0_2[1] + tmp1[1] * W0_2[0]);

        tmp4[0] = (tmp2[0] * W0_2[0] - tmp2[1] * W0_2[1]);
        tmp4[1] = (tmp2[0] * W0_2[1] + tmp2[1] * W0_2[0]);

        currLayerReal[i] = tmp3[0] + tmp4[0];
        currLayerImag[i] = tmp3[1] + tmp4[1];

        tmp4[0] = (tmp2[0] * W1_2_[0] - tmp2[1] * W1_2_[1]);
        tmp4[1] = (tmp2[0] * W1_2_[1] + tmp2[1] * W1_2_[0]);

        currLayerReal[i + 1] = tmp3[0] + tmp4[0];
        currLayerImag[i + 1] = tmp3[1] + tmp4[1];
    }

    for (let S = 2; S <= DEEPNESS; S++) {
        const BUTTERFLY_POINTS = Math.pow(2, S);
        const BUTTERFLIES_NUMBER = N / BUTTERFLY_POINTS;
        const BUTTERFLY_CROSSES = BUTTERFLY_POINTS / 2;

        const LOWER_INDEX = Math.pow(2, S);

        for (let batterflyIndex = 0; batterflyIndex < BUTTERFLIES_NUMBER; batterflyIndex++) {
            for (let crossIndex = 0; crossIndex < BUTTERFLY_CROSSES; crossIndex++) {

                const LOW_COEF = W(directionSignum * crossIndex, LOWER_INDEX);

                const i1idx = batterflyIndex * BUTTERFLY_POINTS + crossIndex;
                const i2idx = i1idx + BUTTERFLY_CROSSES;

                const OLD_TOP_R = currLayerReal[i1idx]!;
                const OLD_TOP_I = currLayerImag[i1idx]!;

                const OLD_BOTTOM_R = currLayerReal[i2idx]!;
                const OLD_BOTTOM_I = currLayerImag[i2idx]!;

                tmp1[0] = (LOW_COEF[0] * OLD_BOTTOM_R - LOW_COEF[1] * OLD_BOTTOM_I);
                tmp1[1] = (LOW_COEF[0] * OLD_BOTTOM_I + LOW_COEF[1] * OLD_BOTTOM_R);

                nextLayerReal[i1idx] = OLD_TOP_R + tmp1[0];
                nextLayerImag[i1idx] = OLD_TOP_I + tmp1[1];

                tmp2[0] = (LOW_COEF[0] * OLD_BOTTOM_R - LOW_COEF[1] * OLD_BOTTOM_I);
                tmp2[1] = (LOW_COEF[0] * OLD_BOTTOM_I + LOW_COEF[1] * OLD_BOTTOM_R);

                nextLayerReal[i2idx] = OLD_TOP_R - tmp2[0];
                nextLayerImag[i2idx] = OLD_TOP_I - tmp2[1];
            }
        }

        const tmpR = currLayerReal;
        currLayerReal = nextLayerReal;
        nextLayerReal = tmpR;

        const tmpI = currLayerImag;
        currLayerImag = nextLayerImag;
        nextLayerImag = tmpI;
    }
    if (!isDirect) {
        for (let i = 0; i < N; i++) {
            currLayerReal[i] /= N;
            currLayerImag[i] /= N;
        }
    }

    f64ArrayCopyTo(currLayerReal, outputReal);
    f64ArrayCopyTo(currLayerImag, outputImaginary);

    currLayerReal.iDontNeedItAnymore();
    currLayerImag.iDontNeedItAnymore();
    nextLayerReal.iDontNeedItAnymore();
    nextLayerImag.iDontNeedItAnymore();
}

export const SOUND_FFT_LENGTH = 32768;
