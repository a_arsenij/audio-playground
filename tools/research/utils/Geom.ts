import { Point } from "@audio-playground/lib-common/src/math/Point";

export type Ray = Point & {
    angle: number,
}

export type Segment = [
    Point,
    Point,
]

export function getLinesIntersection(
    a: Segment,
    b: Segment,
): Point | undefined {

    const xa0 = a[0].x;
    const xa1 = a[1].x;
    const ya0 = a[0].y;
    const ya1 = a[1].y;
    const xb0 = b[0].x;
    const xb1 = b[1].x;
    const yb0 = b[0].y;
    const yb1 = b[1].y;

    const denominator = (xa0 - xa1) * (yb0 - yb1) -
        (xb0 - xb1) * (ya0 - ya1);

    if (denominator === 0) {
        return undefined;
    }

    const numerator =
        -xa1 * (ya0 - ya1) * (yb0 - yb1) +
        ya1 * (xa0 - xa1) * (yb0 - yb1) +
        xb1 * (yb0 - yb1) * (ya0 - ya1) -
        yb1 * (xb0 - xb1) * (ya0 - ya1);

    const y = numerator / denominator;

    const x =
        ya0 === ya1
            ? xb1 + (xb0 - xb1) * (y - yb1) / (yb0 - yb1)
            : xa1 + (xa0 - xa1) * (y - ya1) / (ya0 - ya1);

    return { x, y };

}

const EPSILON = 1e-10;

export function isPointInSegmentAABB(
    segment: Segment,
    point: Point,
): boolean {
    const xMin = Math.min(
        segment[0].x,
        segment[1].x,
    ) - EPSILON;
    const xMax = Math.max(
        segment[0].x,
        segment[1].x,
    ) + EPSILON;
    const yMin = Math.min(
        segment[0].y,
        segment[1].y,
    ) - EPSILON;
    const yMax = Math.max(
        segment[0].y,
        segment[1].y,
    ) + EPSILON;
    const { x, y } = point;
    return (xMin <= x && x <= xMax && yMin <= y && y <= yMax);
}

export function getSegmentsIntersection(
    a: Segment,
    b: Segment,
): Point | undefined {
    const p = getLinesIntersection(a, b);
    if (p === undefined) {
        return undefined;
    }
    return (isPointInSegmentAABB(a, p) && isPointInSegmentAABB(b, p))
        ? p
        : undefined;
}

export function getSegmentRayIntersection(
    a: Segment,
    b: Ray,
): Point | undefined {

    const dx1 = Math.cos(b.angle);
    const dy1 = Math.sin(b.angle);
    const x1 = b.x + dx1;
    const y1 = b.y + dy1;
    const pseudoSegment: Segment = [
        { x: b.x, y: b.y },
        { x: x1, y: y1 },
    ];
    const p = getLinesIntersection(a, pseudoSegment);
    if (p === undefined) {
        return undefined;
    }

    if (!isPointInSegmentAABB(a, p)) {
        return undefined;
    }

    const dx2 = p.x - b.x;
    const dy2 = p.y - b.y;

    return Math.sign(dx1) === Math.sign(dx2) &&
        Math.sign(dy1) === Math.sign(dy2)
        ? p
        : undefined;
}
