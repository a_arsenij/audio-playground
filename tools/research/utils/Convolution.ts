import { acquireArrayFromPool } from "@audio-playground/lib-common/src/std/ArraysPool";

import { fft } from "./FFT";
import { fastHartleyTransformNotNormalized, getSequenceInverseIndex } from "./Hartley";


const furieFilterCache: Map<
    number,
    WeakMap<Float64Array, [Float64Array, Float64Array]>
> = new Map();
export const furieConvolution = (
    filter: Float64Array,
    cyclicArraySignal: Float64Array,
    index: number,
    target: Float64Array,
) => {
    const signalLength = target.length + filter.length - 1;
    const signalTrueIndex = index + target.length;

    const maxLength = Math.max(signalLength, filter.length);
    const n = 2 ** Math.ceil(Math.log2(maxLength));

    let weakMapN = furieFilterCache.get(n);
    if (!weakMapN) {
        weakMapN = new WeakMap<Float64Array, [Float64Array, Float64Array]>();
        furieFilterCache.set(n, weakMapN);
    }

    const fa = weakMapN.get(filter);
    const faReal = fa ? fa[0] : new Float64Array(n);
    const faImag = fa ? fa[1] : new Float64Array(n);
    if (!fa) {
        const aReal = acquireArrayFromPool(n);
        filter.forEach((item, i) => {
            aReal[i] = item;
        });

        fft(faReal, faImag, aReal, true);
        aReal.iDontNeedItAnymore();

        weakMapN.set(filter, [faReal, faImag]);
    }

    const bReal = acquireArrayFromPool(n);
    fillFromCyclic(bReal, cyclicArraySignal, signalLength, signalTrueIndex);

    const fbReal = acquireArrayFromPool(n);
    const fbImag = acquireArrayFromPool(n);
    fft(fbReal, fbImag, bReal, true);
    bReal.iDontNeedItAnymore();

    const fcreal = acquireArrayFromPool(n);
    const fcimag = acquireArrayFromPool(n);

    for (let k = 0; k < n; k++) {
        const ar = faReal[k]!;
        const ai = faImag[k]!;
        const br = fbReal[k]!;
        const bi = fbImag[k]!;

        fcreal[k] = ar * br - ai * bi;
        fcimag[k] = ar * bi + ai * br;
    }

    fbReal.iDontNeedItAnymore();
    fbImag.iDontNeedItAnymore();

    const cImag = acquireArrayFromPool(n);
    const cReal = acquireArrayFromPool(n);

    fft(cReal, cImag, fcreal, false, fcimag);

    fcreal.iDontNeedItAnymore();
    fcimag.iDontNeedItAnymore();

    cImag.iDontNeedItAnymore();

    for (let i = 1; i < target.length; i++) {
        target[target.length - i] = cReal[cReal.length - i]!;
    }

    cReal.iDontNeedItAnymore();

};

const hartleyFilterCache: Map<number, WeakMap<Float64Array, Float64Array>> = new Map();
export const hartleyConvolution = (
    filter: Float64Array,
    cyclicArraySignal: Float64Array,
    index: number,
    target: Float64Array,
) => {

    const signalLength = target.length + filter.length - 1;
    const signalTrueIndex = index + target.length;

    const maxLength = Math.max(signalLength, filter.length);
    const n = 2 ** Math.ceil(Math.log2(maxLength));

    let weakMapN = hartleyFilterCache.get(n);
    if (!weakMapN) {
        weakMapN = new WeakMap<Float64Array, Float64Array>();
        hartleyFilterCache.set(n, weakMapN);
    }

    let C = weakMapN.get(filter);
    if (!C) {
        const a = acquireArrayFromPool(n);
        filter.forEach((item, i) => {
            a[i] = item;
        });

        C = new Float64Array(n);
        fastHartleyTransformNotNormalized(C, a);
        a.iDontNeedItAnymore();

        weakMapN.set(filter, C);
    }

    const b = acquireArrayFromPool(n);
    fillFromCyclic(b, cyclicArraySignal, signalLength, signalTrueIndex);
    const D = acquireArrayFromPool(n);
    fastHartleyTransformNotNormalized(D, b);
    b.iDontNeedItAnymore();

    const H = acquireArrayFromPool(n);
    for (let k = 0; k < n; k++) {
        const mK = getSequenceInverseIndex(k, n);
        // @ts-ignore
        H[k] = (C[k] * D[k] + C[k] * D[mK] + C[mK] * D[k] - C[mK] * D[mK]) / 2 / n;
    }

    D.iDontNeedItAnymore();

    const res: Float64Array = new Float64Array(n);
    fastHartleyTransformNotNormalized(res, H);

    H.iDontNeedItAnymore();

    for (let i = 1; i < target.length; i++) {
        target[target.length - i] = res[res.length - i]!;
    }
};

function fillFromCyclic(
    target: Float64Array,
    cyclic: Float64Array,
    length: number,
    trueIndex: number,
) {
    for (let i = 0; i < length; i++) {
        const cycleIndex = (((trueIndex - i) % cyclic.length) + cyclic.length) % cyclic.length;

        // @ts-ignore
        target[target.length - 1 - i] = cyclic[cycleIndex];
    }
}
