import { acquireArrayFromPool } from "@audio-playground/lib-common/src/std/ArraysPool";

export function fht(seq: Float64Array) {

    const res = new Float64Array(seq.length);
    fastHartleyTransformNotNormalized(res, seq);

    const sqrt = Math.sqrt(seq.length);

    for (let i = 0; i < res.length; i++) {
        res[i] /= sqrt;
    }

    return res;
}

export function fastHartleyTransformNotNormalized(result: Float64Array, seq: Float64Array) {
    const length = seq.length;

    if (length === 2) {
        result[0] = seq[0]! + seq[1]!;
        result[1] = seq[0]! - seq[1]!;
        return;
    }

    const halfLength = length >> 1;

    const even = acquireArrayFromPool(halfLength);
    const odd = acquireArrayFromPool(halfLength);

    for (let i = 0; i < halfLength; i++) {
        even[i] = seq[i * 2]!;
        odd[i] = seq[i * 2 + 1]!;
    }

    const hEven = acquireArrayFromPool(halfLength);
    fastHartleyTransformNotNormalized(hEven, even);
    even.iDontNeedItAnymore();

    const hOdd = acquireArrayFromPool(halfLength);
    fastHartleyTransformNotNormalized(hOdd, odd);
    odd.iDontNeedItAnymore();

    const XHOdd = acquireArrayFromPool(halfLength);
    XOneHalf(XHOdd, hOdd);
    hOdd.iDontNeedItAnymore();

    for (let i = 0; i < halfLength; i++) {
        // @ts-ignore
        result[i] = hEven[i] + XHOdd[i];
    }
    for (let i = 0; i < halfLength; i++) {
        // @ts-ignore
        result[halfLength + i] = hEven[i] - XHOdd[i];
    }
    XHOdd.iDontNeedItAnymore();
    hEven.iDontNeedItAnymore();
}

export function getSequenceInverseIndex(k: number, n: number) {
    return k === 0 ? 0 : n - k;
}

function XOneHalf(result: Float64Array, a: Float64Array) {
    // X(k) = A(k) * cos(pi * k / n) + A(~k) * sin(pi * k / n),
    // where ~k = k == 0? 0 : n - k
    // where n = A.length

    const n = a.length;
    const piN = Math.PI / n;

    // @ts-ignore
    result[0] = a[0];
    // @ts-ignore
    result[n / 2] = a[n / 2];
    for (let k = 1; k < n / 2; k++) {
        const minusK = getSequenceInverseIndex(k, n);
        const piKN = piN * k;
        const cos = Math.cos(piKN);
        const sin = Math.sin(piKN);

        // @ts-ignore
        result[k] = a[k] * cos + a[minusK] * sin;
        // @ts-ignore
        result[minusK] = a[k] * sin - a[minusK] * cos;
    }
}
