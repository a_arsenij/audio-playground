import * as fs from "fs";

const DATA = [
    [
        "unreachable",
        "0x00",
        "[t1∗]→[t2∗]",
    ],
    [
        "nop",
        "0x01",
        "[]→[]",
    ],
    [
        "block bt",
        "0x02",
        "[t1∗]→[t2∗]",
    ],
    [
        "loop bt",
        "0x03",
        "[t1∗]→[t2∗]",
    ],
    [
        "if bt",
        "0x04",
        "[t1∗ i32]→[t2∗]",
    ],
    [
        "else",
        "0x05",
        "",
    ],
    [
        "end",
        "0x0B",
        "",
    ],
    [
        "br l",
        "0x0C",
        "[t1∗ t∗]→[t2∗]",
    ],
    [
        "br_if l",
        "0x0D",
        "[t∗ i32]→[t∗]",
    ],
    [
        "br_table l∗ l",
        "0x0E",
        "[t1∗ t∗ i32]→[t2∗]",
    ],
    [
        "return",
        "0x0F",
        "[t1∗ t∗]→[t2∗]",
    ],
    [
        "call x",
        "0x10",
        "[t1∗]→[t2∗]",
    ],
    [
        "call_indirect x y",
        "0x11",
        "[t1∗ i32]→[t2∗]",
    ],
    [
        "drop",
        "0x1A",
        "[t]→[]",
    ],
    [
        "select",
        "0x1B",
        "[t t i32]→[t]",
    ],
    [
        "select t",
        "0x1C",
        "[t t i32]→[t]",
    ],
    [
        "local.get x",
        "0x20",
        "[]→[t]",
    ],
    [
        "local.set x",
        "0x21",
        "[t]→[]",
    ],
    [
        "local.tee x",
        "0x22",
        "[t]→[t]",
    ],
    [
        "global.get x",
        "0x23",
        "[]→[t]",
    ],
    [
        "global.set x",
        "0x24",
        "[t]→[]",
    ],
    [
        "table.get x",
        "0x25",
        "[i32]→[t]",
    ],
    [
        "table.set x",
        "0x26",
        "[i32 t]→[]",
    ],
    [
        "i32.load memarg",
        "0x28",
        "[i32]→[i32]",
    ],
    [
        "i64.load memarg",
        "0x29",
        "[i32]→[i64]",
    ],
    [
        "f32.load memarg",
        "0x2A",
        "[i32]→[f32]",
    ],
    [
        "f64.load memarg",
        "0x2B",
        "[i32]→[f64]",
    ],
    [
        "i32.load8_s memarg",
        "0x2C",
        "[i32]→[i32]",
    ],
    [
        "i32.load8_u memarg",
        "0x2D",
        "[i32]→[i32]",
    ],
    [
        "i32.load16_s memarg",
        "0x2E",
        "[i32]→[i32]",
    ],
    [
        "i32.load16_u memarg",
        "0x2F",
        "[i32]→[i32]",
    ],
    [
        "i64.load8_s memarg",
        "0x30",
        "[i32]→[i64]",
    ],
    [
        "i64.load8_u memarg",
        "0x31",
        "[i32]→[i64]",
    ],
    [
        "i64.load16_s memarg",
        "0x32",
        "[i32]→[i64]",
    ],
    [
        "i64.load16_u memarg",
        "0x33",
        "[i32]→[i64]",
    ],
    [
        "i64.load32_s memarg",
        "0x34",
        "[i32]→[i64]",
    ],
    [
        "i64.load32_u memarg",
        "0x35",
        "[i32]→[i64]",
    ],
    [
        "i32.store memarg",
        "0x36",
        "[i32 i32]→[]",
    ],
    [
        "i64.store memarg",
        "0x37",
        "[i32 i64]→[]",
    ],
    [
        "f32.store memarg",
        "0x38",
        "[i32 f32]→[]",
    ],
    [
        "f64.store memarg",
        "0x39",
        "[i32 f64]→[]",
    ],
    [
        "i32.store8 memarg",
        "0x3A",
        "[i32 i32]→[]",
    ],
    [
        "i32.store16 memarg",
        "0x3B",
        "[i32 i32]→[]",
    ],
    [
        "i64.store8 memarg",
        "0x3C",
        "[i32 i64]→[]",
    ],
    [
        "i64.store16 memarg",
        "0x3D",
        "[i32 i64]→[]",
    ],
    [
        "i64.store32 memarg",
        "0x3E",
        "[i32 i64]→[]",
    ],
    [
        "memory.size",
        "0x3F",
        "[]→[i32]",
    ],
    [
        "memory.grow",
        "0x40",
        "[i32]→[i32]",
    ],
    [
        "i32.const i32",
        "0x41",
        "[]→[i32]",
    ],
    [
        "i64.const i64",
        "0x42",
        "[]→[i64]",
    ],
    [
        "f32.const f32",
        "0x43",
        "[]→[f32]",
    ],
    [
        "f64.const f64",
        "0x44",
        "[]→[f64]",
    ],
    [
        "i32.eqz",
        "0x45",
        "[i32]→[i32]",
    ],
    [
        "i32.eq",
        "0x46",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.ne",
        "0x47",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.lt_s",
        "0x48",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.lt_u",
        "0x49",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.gt_s",
        "0x4A",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.gt_u",
        "0x4B",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.le_s",
        "0x4C",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.le_u",
        "0x4D",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.ge_s",
        "0x4E",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.ge_u",
        "0x4F",
        "[i32 i32]→[i32]",
    ],
    [
        "i64.eqz",
        "0x50",
        "[i64]→[i32]",
    ],
    [
        "i64.eq",
        "0x51",
        "[i64 i64]→[i32]",
    ],
    [
        "i64.ne",
        "0x52",
        "[i64 i64]→[i32]",
    ],
    [
        "i64.lt_s",
        "0x53",
        "[i64 i64]→[i32]",
    ],
    [
        "i64.lt_u",
        "0x54",
        "[i64 i64]→[i32]",
    ],
    [
        "i64.gt_s",
        "0x55",
        "[i64 i64]→[i32]",
    ],
    [
        "i64.gt_u",
        "0x56",
        "[i64 i64]→[i32]",
    ],
    [
        "i64.le_s",
        "0x57",
        "[i64 i64]→[i32]",
    ],
    [
        "i64.le_u",
        "0x58",
        "[i64 i64]→[i32]",
    ],
    [
        "i64.ge_s",
        "0x59",
        "[i64 i64]→[i32]",
    ],
    [
        "i64.ge_u",
        "0x5A",
        "[i64 i64]→[i32]",
    ],
    [
        "f32.eq",
        "0x5B",
        "[f32 f32]→[i32]",
    ],
    [
        "f32.ne",
        "0x5C",
        "[f32 f32]→[i32]",
    ],
    [
        "f32.lt",
        "0x5D",
        "[f32 f32]→[i32]",
    ],
    [
        "f32.gt",
        "0x5E",
        "[f32 f32]→[i32]",
    ],
    [
        "f32.le",
        "0x5F",
        "[f32 f32]→[i32]",
    ],
    [
        "f32.ge",
        "0x60",
        "[f32 f32]→[i32]",
    ],
    [
        "f64.eq",
        "0x61",
        "[f64 f64]→[i32]",
    ],
    [
        "f64.ne",
        "0x62",
        "[f64 f64]→[i32]",
    ],
    [
        "f64.lt",
        "0x63",
        "[f64 f64]→[i32]",
    ],
    [
        "f64.gt",
        "0x64",
        "[f64 f64]→[i32]",
    ],
    [
        "f64.le",
        "0x65",
        "[f64 f64]→[i32]",
    ],
    [
        "f64.ge",
        "0x66",
        "[f64 f64]→[i32]",
    ],
    [
        "i32.clz",
        "0x67",
        "[i32]→[i32]",
    ],
    [
        "i32.ctz",
        "0x68",
        "[i32]→[i32]",
    ],
    [
        "i32.popcnt",
        "0x69",
        "[i32]→[i32]",
    ],
    [
        "i32.add",
        "0x6A",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.sub",
        "0x6B",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.mul",
        "0x6C",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.div_s",
        "0x6D",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.div_u",
        "0x6E",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.rem_s",
        "0x6F",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.rem_u",
        "0x70",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.and",
        "0x71",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.or",
        "0x72",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.xor",
        "0x73",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.shl",
        "0x74",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.shr_s",
        "0x75",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.shr_u",
        "0x76",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.rotl",
        "0x77",
        "[i32 i32]→[i32]",
    ],
    [
        "i32.rotr",
        "0x78",
        "[i32 i32]→[i32]",
    ],
    [
        "i64.clz",
        "0x79",
        "[i64]→[i64]",
    ],
    [
        "i64.ctz",
        "0x7A",
        "[i64]→[i64]",
    ],
    [
        "i64.popcnt",
        "0x7B",
        "[i64]→[i64]",
    ],
    [
        "i64.add",
        "0x7C",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.sub",
        "0x7D",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.mul",
        "0x7E",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.div_s",
        "0x7F",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.div_u",
        "0x80",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.rem_s",
        "0x81",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.rem_u",
        "0x82",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.and",
        "0x83",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.or",
        "0x84",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.xor",
        "0x85",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.shl",
        "0x86",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.shr_s",
        "0x87",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.shr_u",
        "0x88",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.rotl",
        "0x89",
        "[i64 i64]→[i64]",
    ],
    [
        "i64.rotr",
        "0x8A",
        "[i64 i64]→[i64]",
    ],
    [
        "f32.abs",
        "0x8B",
        "[f32]→[f32]",
    ],
    [
        "f32.neg",
        "0x8C",
        "[f32]→[f32]",
    ],
    [
        "f32.ceil",
        "0x8D",
        "[f32]→[f32]",
    ],
    [
        "f32.floor",
        "0x8E",
        "[f32]→[f32]",
    ],
    [
        "f32.trunc",
        "0x8F",
        "[f32]→[f32]",
    ],
    [
        "f32.nearest",
        "0x90",
        "[f32]→[f32]",
    ],
    [
        "f32.sqrt",
        "0x91",
        "[f32]→[f32]",
    ],
    [
        "f32.add",
        "0x92",
        "[f32 f32]→[f32]",
    ],
    [
        "f32.sub",
        "0x93",
        "[f32 f32]→[f32]",
    ],
    [
        "f32.mul",
        "0x94",
        "[f32 f32]→[f32]",
    ],
    [
        "f32.div",
        "0x95",
        "[f32 f32]→[f32]",
    ],
    [
        "f32.min",
        "0x96",
        "[f32 f32]→[f32]",
    ],
    [
        "f32.max",
        "0x97",
        "[f32 f32]→[f32]",
    ],
    [
        "f32.copysign",
        "0x98",
        "[f32 f32]→[f32]",
    ],
    [
        "f64.abs",
        "0x99",
        "[f64]→[f64]",
    ],
    [
        "f64.neg",
        "0x9A",
        "[f64]→[f64]",
    ],
    [
        "f64.ceil",
        "0x9B",
        "[f64]→[f64]",
    ],
    [
        "f64.floor",
        "0x9C",
        "[f64]→[f64]",
    ],
    [
        "f64.trunc",
        "0x9D",
        "[f64]→[f64]",
    ],
    [
        "f64.nearest",
        "0x9E",
        "[f64]→[f64]",
    ],
    [
        "f64.sqrt",
        "0x9F",
        "[f64]→[f64]",
    ],
    [
        "f64.add",
        "0xA0",
        "[f64 f64]→[f64]",
    ],
    [
        "f64.sub",
        "0xA1",
        "[f64 f64]→[f64]",
    ],
    [
        "f64.mul",
        "0xA2",
        "[f64 f64]→[f64]",
    ],
    [
        "f64.div",
        "0xA3",
        "[f64 f64]→[f64]",
    ],
    [
        "f64.min",
        "0xA4",
        "[f64 f64]→[f64]",
    ],
    [
        "f64.max",
        "0xA5",
        "[f64 f64]→[f64]",
    ],
    [
        "f64.copysign",
        "0xA6",
        "[f64 f64]→[f64]",
    ],
    [
        "i32.wrap_i64",
        "0xA7",
        "[i64]→[i32]",
    ],
    [
        "i32.trunc_f32_s",
        "0xA8",
        "[f32]→[i32]",
    ],
    [
        "i32.trunc_f32_u",
        "0xA9",
        "[f32]→[i32]",
    ],
    [
        "i32.trunc_f64_s",
        "0xAA",
        "[f64]→[i32]",
    ],
    [
        "i32.trunc_f64_u",
        "0xAB",
        "[f64]→[i32]",
    ],
    [
        "i64.extend_i32_s",
        "0xAC",
        "[i32]→[i64]",
    ],
    [
        "i64.extend_i32_u",
        "0xAD",
        "[i32]→[i64]",
    ],
    [
        "i64.trunc_f32_s",
        "0xAE",
        "[f32]→[i64]",
    ],
    [
        "i64.trunc_f32_u",
        "0xAF",
        "[f32]→[i64]",
    ],
    [
        "i64.trunc_f64_s",
        "0xB0",
        "[f64]→[i64]",
    ],
    [
        "i64.trunc_f64_u",
        "0xB1",
        "[f64]→[i64]",
    ],
    [
        "f32.convert_i32_s",
        "0xB2",
        "[i32]→[f32]",
    ],
    [
        "f32.convert_i32_u",
        "0xB3",
        "[i32]→[f32]",
    ],
    [
        "f32.convert_i64_s",
        "0xB4",
        "[i64]→[f32]",
    ],
    [
        "f32.convert_i64_u",
        "0xB5",
        "[i64]→[f32]",
    ],
    [
        "f32.demote_f64",
        "0xB6",
        "[f64]→[f32]",
    ],
    [
        "f64.convert_i32_s",
        "0xB7",
        "[i32]→[f64]",
    ],
    [
        "f64.convert_i32_u",
        "0xB8",
        "[i32]→[f64]",
    ],
    [
        "f64.convert_i64_s",
        "0xB9",
        "[i64]→[f64]",
    ],
    [
        "f64.convert_i64_u",
        "0xBA",
        "[i64]→[f64]",
    ],
    [
        "f64.promote_f32",
        "0xBB",
        "[f32]→[f64]",
    ],
    [
        "i32.reinterpret_f32",
        "0xBC",
        "[f32]→[i32]",
    ],
    [
        "i64.reinterpret_f64",
        "0xBD",
        "[f64]→[i64]",
    ],
    [
        "f32.reinterpret_i32",
        "0xBE",
        "[i32]→[f32]",
    ],
    [
        "f64.reinterpret_i64",
        "0xBF",
        "[i64]→[f64]",
    ],
    [
        "i32.extend8_s",
        "0xC0",
        "[i32]→[i32]",
    ],
    [
        "i32.extend16_s",
        "0xC1",
        "[i32]→[i32]",
    ],
    [
        "i64.extend8_s",
        "0xC2",
        "[i64]→[i64]",
    ],
    [
        "i64.extend16_s",
        "0xC3",
        "[i64]→[i64]",
    ],
    [
        "i64.extend32_s",
        "0xC4",
        "[i64]→[i64]",
    ],
    [
        "ref.null t",
        "0xD0",
        "[]→[t]",
    ],
    [
        "ref.is_null",
        "0xD1",
        "[t]→[i32]",
    ],
    [
        "ref.func x",
        "0xD2",
        "[]→[funcref]",
    ],
    [
        "i32.trunc_sat_f32_s",
        "0xFC 0x00",
        "[f32]→[i32]",
    ],
    [
        "i32.trunc_sat_f32_u",
        "0xFC 0x01",
        "[f32]→[i32]",
    ],
    [
        "i32.trunc_sat_f64_s",
        "0xFC 0x02",
        "[f64]→[i32]",
    ],
    [
        "i32.trunc_sat_f64_u",
        "0xFC 0x03",
        "[f64]→[i32]",
    ],
    [
        "i64.trunc_sat_f32_s",
        "0xFC 0x04",
        "[f32]→[i64]",
    ],
    [
        "i64.trunc_sat_f32_u",
        "0xFC 0x05",
        "[f32]→[i64]",
    ],
    [
        "i64.trunc_sat_f64_s",
        "0xFC 0x06",
        "[f64]→[i64]",
    ],
    [
        "i64.trunc_sat_f64_u",
        "0xFC 0x07",
        "[f64]→[i64]",
    ],
    [
        "memory.init x",
        "0xFC 0x08",
        "[i32 i32 i32]→[]",
    ],
    [
        "data.drop x",
        "0xFC 0x09",
        "[]→[]",
    ],
    [
        "memory.copy",
        "0xFC 0x0A",
        "[i32 i32 i32]→[]",
    ],
    [
        "memory.fill",
        "0xFC 0x0B",
        "[i32 i32 i32]→[]",
    ],
    [
        "table.init x y",
        "0xFC 0x0C",
        "[i32 i32 i32]→[]",
    ],
    [
        "elem.drop x",
        "0xFC 0x0D",
        "[]→[]",
    ],
    [
        "table.copy x y",
        "0xFC 0x0E",
        "[i32 i32 i32]→[]",
    ],
    [
        "table.grow x",
        "0xFC 0x0F",
        "[t i32]→[i32]",
    ],
    [
        "table.size x",
        "0xFC 0x10",
        "[]→[i32]",
    ],
    [
        "table.fill x",
        "0xFC 0x11",
        "[i32 t i32]→[]",
    ],
    [
        "v128.load memarg",
        "0xFD  0x00",
        "[i32]→[v128]",
    ],
    [
        "v128.load8x8_s memarg",
        "0xFD  0x01",
        "[i32]→[v128]",
    ],
    [
        "v128.load8x8_u memarg",
        "0xFD  0x02",
        "[i32]→[v128]",
    ],
    [
        "v128.load16x4_s memarg",
        "0xFD  0x03",
        "[i32]→[v128]",
    ],
    [
        "v128.load16x4_u memarg",
        "0xFD  0x04",
        "[i32]→[v128]",
    ],
    [
        "v128.load32x2_s memarg",
        "0xFD  0x05",
        "[i32]→[v128]",
    ],
    [
        "v128.load32x2_u memarg",
        "0xFD  0x06",
        "[i32]→[v128]",
    ],
    [
        "v128.load8_splat memarg",
        "0xFD  0x07",
        "[i32]→[v128]",
    ],
    [
        "v128.load16_splat memarg",
        "0xFD  0x08",
        "[i32]→[v128]",
    ],
    [
        "v128.load32_splat memarg",
        "0xFD  0x09",
        "[i32]→[v128]",
    ],
    [
        "v128.load64_splat memarg",
        "0xFD  0x0A",
        "[i32]→[v128]",
    ],
    [
        "v128.store memarg",
        "0xFD  0x0B",
        "[i32 v128]→[]",
    ],
    [
        "v128.const i128",
        "0xFD  0x0C",
        "[]→[v128]",
    ],
    [
        "i8x16.shuffle laneidx16",
        "0xFD  0x0D",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.swizzle",
        "0xFD  0x0E",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.splat",
        "0xFD  0x0F",
        "[i32]→[v128]",
    ],
    [
        "i16x8.splat",
        "0xFD  0x10",
        "[i32]→[v128]",
    ],
    [
        "i32x4.splat",
        "0xFD  0x11",
        "[i32]→[v128]",
    ],
    [
        "i64x2.splat",
        "0xFD  0x12",
        "[i64]→[v128]",
    ],
    [
        "f32x4.splat",
        "0xFD  0x13",
        "[f32]→[v128]",
    ],
    [
        "f64x2.splat",
        "0xFD  0x14",
        "[f64]→[v128]",
    ],
    [
        "i8x16.extract_lane_s laneidx",
        "0xFD  0x15",
        "[v128]→[i32]",
    ],
    [
        "i8x16.extract_lane_u laneidx",
        "0xFD  0x16",
        "[v128]→[i32]",
    ],
    [
        "i8x16.replace_lane laneidx",
        "0xFD  0x17",
        "[v128 i32]→[v128]",
    ],
    [
        "i16x8.extract_lane_s laneidx",
        "0xFD  0x18",
        "[v128]→[i32]",
    ],
    [
        "i16x8.extract_lane_u laneidx",
        "0xFD  0x19",
        "[v128]→[i32]",
    ],
    [
        "i16x8.replace_lane laneidx",
        "0xFD  0x1A",
        "[v128 i32]→[v128]",
    ],
    [
        "i32x4.extract_lane laneidx",
        "0xFD  0x1B",
        "[v128]→[i32]",
    ],
    [
        "i32x4.replace_lane laneidx",
        "0xFD  0x1C",
        "[v128 i32]→[v128]",
    ],
    [
        "i64x2.extract_lane laneidx",
        "0xFD  0x1D",
        "[v128]→[i64]",
    ],
    [
        "i64x2.replace_lane laneidx",
        "0xFD  0x1E",
        "[v128 i64]→[v128]",
    ],
    [
        "f32x4.extract_lane laneidx",
        "0xFD  0x1F",
        "[v128]→[f32]",
    ],
    [
        "f32x4.replace_lane laneidx",
        "0xFD  0x20",
        "[v128 f32]→[v128]",
    ],
    [
        "f64x2.extract_lane laneidx",
        "0xFD  0x21",
        "[v128]→[f64]",
    ],
    [
        "f64x2.replace_lane laneidx",
        "0xFD  0x22",
        "[v128 f64]→[v128]",
    ],
    [
        "i8x16.eq",
        "0xFD  0x23",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.ne",
        "0xFD  0x24",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.lt_s",
        "0xFD  0x25",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.lt_u",
        "0xFD  0x26",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.gt_s",
        "0xFD  0x27",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.gt_u",
        "0xFD  0x28",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.le_s",
        "0xFD  0x29",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.le_u",
        "0xFD  0x2A",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.ge_s",
        "0xFD  0x2B",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.ge_u",
        "0xFD  0x2C",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.eq",
        "0xFD  0x2D",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.ne",
        "0xFD  0x2E",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.lt_s",
        "0xFD  0x2F",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.lt_u",
        "0xFD  0x30",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.gt_s",
        "0xFD  0x31",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.gt_u",
        "0xFD  0x32",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.le_s",
        "0xFD  0x33",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.le_u",
        "0xFD  0x34",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.ge_s",
        "0xFD  0x35",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.ge_u",
        "0xFD  0x36",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.eq",
        "0xFD  0x37",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.ne",
        "0xFD  0x38",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.lt_s",
        "0xFD  0x39",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.lt_u",
        "0xFD  0x3A",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.gt_s",
        "0xFD  0x3B",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.gt_u",
        "0xFD  0x3C",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.le_s",
        "0xFD  0x3D",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.le_u",
        "0xFD  0x3E",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.ge_s",
        "0xFD  0x3F",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.ge_u",
        "0xFD  0x40",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.eq",
        "0xFD  0x41",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.ne",
        "0xFD  0x42",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.lt",
        "0xFD  0x43",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.gt",
        "0xFD  0x44",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.le",
        "0xFD  0x45",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.ge",
        "0xFD  0x46",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.eq",
        "0xFD  0x47",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.ne",
        "0xFD  0x48",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.lt",
        "0xFD  0x49",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.gt",
        "0xFD  0x4A",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.le",
        "0xFD  0x4B",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.ge",
        "0xFD  0x4C",
        "[v128 v128]→[v128]",
    ],
    [
        "v128.not",
        "0xFD  0x4D",
        "[v128]→[v128]",
    ],
    [
        "v128.and",
        "0xFD  0x4E",
        "[v128 v128]→[v128]",
    ],
    [
        "v128.andnot",
        "0xFD  0x4F",
        "[v128 v128]→[v128]",
    ],
    [
        "v128.or",
        "0xFD  0x50",
        "[v128 v128]→[v128]",
    ],
    [
        "v128.xor",
        "0xFD  0x51",
        "[v128 v128]→[v128]",
    ],
    [
        "v128.bitselect",
        "0xFD  0x52",
        "[v128 v128 v128]→[v128]",
    ],
    [
        "v128.any_true",
        "0xFD  0x53",
        "[v128]→[i32]",
    ],
    [
        "v128.load8_lane memarg laneidx",
        "0xFD  0x54",
        "[i32 v128]→[v128]",
    ],
    [
        "v128.load16_lane memarg laneidx",
        "0xFD  0x55",
        "[i32 v128]→[v128]",
    ],
    [
        "v128.load32_lane memarg laneidx",
        "0xFD  0x56",
        "[i32 v128]→[v128]",
    ],
    [
        "v128.load64_lane memarg laneidx",
        "0xFD  0x57",
        "[i32 v128]→[v128]",
    ],
    [
        "v128.store8_lane memarg laneidx",
        "0xFD  0x58",
        "[i32 v128]→[]",
    ],
    [
        "v128.store16_lane memarg laneidx",
        "0xFD  0x59",
        "[i32 v128]→[]",
    ],
    [
        "v128.store32_lane memarg laneidx",
        "0xFD  0x5A",
        "[i32 v128]→[]",
    ],
    [
        "v128.store64_lane memarg laneidx",
        "0xFD  0x5B",
        "[i32 v128]→[]",
    ],
    [
        "v128.load32_zero memarg",
        "0xFD  0x5C",
        "[i32]→[v128]",
    ],
    [
        "v128.load64_zero memarg",
        "0xFD  0x5D",
        "[i32]→[v128]",
    ],
    [
        "f32x4.demote_f64x2_zero",
        "0xFD  0x5E",
        "[v128]→[v128]",
    ],
    [
        "f64x2.promote_low_f32x4",
        "0xFD  0x5F",
        "[v128]→[v128]",
    ],
    [
        "i8x16.abs",
        "0xFD  0x60",
        "[v128]→[v128]",
    ],
    [
        "i8x16.neg",
        "0xFD  0x61",
        "[v128]→[v128]",
    ],
    [
        "i8x16.popcnt",
        "0xFD  0x62",
        "[v128]→[v128]",
    ],
    [
        "i8x16.all_true",
        "0xFD  0x63",
        "[v128]→[i32]",
    ],
    [
        "i8x16.bitmask",
        "0xFD  0x64",
        "[v128]→[i32]",
    ],
    [
        "i8x16.narrow_i16x8_s",
        "0xFD  0x65",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.narrow_i16x8_u",
        "0xFD  0x66",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.ceil",
        "0xFD  0x67",
        "[v128]→[v128]",
    ],
    [
        "f32x4.floor",
        "0xFD  0x68",
        "[v128]→[v128]",
    ],
    [
        "f32x4.trunc",
        "0xFD  0x69",
        "[v128]→[v128]",
    ],
    [
        "f32x4.nearest",
        "0xFD  0x6A",
        "[v128]→[v128]",
    ],
    [
        "i8x16.shl",
        "0xFD  0x6B",
        "[v128 i32]→[v128]",
    ],
    [
        "i8x16.shr_s",
        "0xFD  0x6C",
        "[v128 i32]→[v128]",
    ],
    [
        "i8x16.shr_u",
        "0xFD  0x6D",
        "[v128 i32]→[v128]",
    ],
    [
        "i8x16.add",
        "0xFD  0x6E",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.add_sat_s",
        "0xFD  0x6F",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.add_sat_u",
        "0xFD  0x70",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.sub",
        "0xFD  0x71",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.sub_sat_s",
        "0xFD  0x72",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.sub_sat_u",
        "0xFD  0x73",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.ceil",
        "0xFD  0x74",
        "[v128]→[v128]",
    ],
    [
        "f64x2.floor",
        "0xFD  0x75",
        "[v128]→[v128]",
    ],
    [
        "i8x16.min_s",
        "0xFD  0x76",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.min_u",
        "0xFD  0x77",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.max_s",
        "0xFD  0x78",
        "[v128 v128]→[v128]",
    ],
    [
        "i8x16.max_u",
        "0xFD  0x79",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.trunc",
        "0xFD  0x7A",
        "[v128]→[v128]",
    ],
    [
        "i8x16.avgr_u",
        "0xFD  0x7B",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.extadd_pairwise_i8x16_s",
        "0xFD  0x7C",
        "[v128]→[v128]",
    ],
    [
        "i16x8.extadd_pairwise_i8x16_u",
        "0xFD  0x7D",
        "[v128]→[v128]",
    ],
    [
        "i32x4.extadd_pairwise_i16x8_s",
        "0xFD  0x7E",
        "[v128]→[v128]",
    ],
    [
        "i32x4.extadd_pairwise_i16x8_u",
        "0xFD  0x7F",
        "[v128]→[v128]",
    ],
    [
        "i16x8.abs",
        "0xFD  0x80  0x01",
        "[v128]→[v128]",
    ],
    [
        "i16x8.neg",
        "0xFD  0x81  0x01",
        "[v128]→[v128]",
    ],
    [
        "i16x8.q15mulr_sat_s",
        "0xFD  0x82  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.all_true",
        "0xFD  0x83  0x01",
        "[v128]→[i32]",
    ],
    [
        "i16x8.bitmask",
        "0xFD  0x84  0x01",
        "[v128]→[i32]",
    ],
    [
        "i16x8.narrow_i32x4_s",
        "0xFD  0x85  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.narrow_i32x4_u",
        "0xFD  0x86  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.extend_low_i8x16_s",
        "0xFD  0x87  0x01",
        "[v128]→[v128]",
    ],
    [
        "i16x8.extend_high_i8x16_s",
        "0xFD  0x88  0x01",
        "[v128]→[v128]",
    ],
    [
        "i16x8.extend_low_i8x16_u",
        "0xFD  0x89  0x01",
        "[v128]→[v128]",
    ],
    [
        "i16x8.extend_high_i8x16_u",
        "0xFD  0x8A  0x01",
        "[v128]→[v128]",
    ],
    [
        "i16x8.shl",
        "0xFD  0x8B  0x01",
        "[v128 i32]→[v128]",
    ],
    [
        "i16x8.shr_s",
        "0xFD  0x8C  0x01",
        "[v128 i32]→[v128]",
    ],
    [
        "i16x8.shr_u",
        "0xFD  0x8D  0x01",
        "[v128 i32]→[v128]",
    ],
    [
        "i16x8.add",
        "0xFD  0x8E  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.add_sat_s",
        "0xFD  0x8F  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.add_sat_u",
        "0xFD  0x90  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.sub",
        "0xFD  0x91  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.sub_sat_s",
        "0xFD  0x92  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.sub_sat_u",
        "0xFD  0x93  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.nearest",
        "0xFD  0x94  0x01",
        "[v128]→[v128]",
    ],
    [
        "i16x8.mul",
        "0xFD  0x95  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.min_s",
        "0xFD  0x96  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.min_u",
        "0xFD  0x97  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.max_s",
        "0xFD  0x98  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.max_u",
        "0xFD  0x99  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.avgr_u",
        "0xFD  0x9B  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.extmul_low_i8x16_s",
        "0xFD  0x9C  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.extmul_high_i8x16_s",
        "0xFD  0x9D  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.extmul_low_i8x16_u",
        "0xFD  0x9E  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i16x8.extmul_high_i8x16_u",
        "0xFD  0x9F  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.abs",
        "0xFD  0xA0  0x01",
        "[v128]→[v128]",
    ],
    [
        "i32x4.neg",
        "0xFD  0xA1  0x01",
        "[v128]→[v128]",
    ],
    [
        "i32x4.all_true",
        "0xFD  0xA3  0x01",
        "[v128]→[i32]",
    ],
    [
        "i32x4.bitmask",
        "0xFD  0xA4  0x01",
        "[v128]→[i32]",
    ],
    [
        "i32x4.extend_low_i16x8_s",
        "0xFD  0xA7  0x01",
        "[v128]→[v128]",
    ],
    [
        "i32x4.extend_high_i16x8_s",
        "0xFD  0xA8  0x01",
        "[v128]→[v128]",
    ],
    [
        "i32x4.extend_low_i16x8_u",
        "0xFD  0xA9  0x01",
        "[v128]→[v128]",
    ],
    [
        "i32x4.extend_high_i16x8_u",
        "0xFD  0xAA  0x01",
        "[v128]→[v128]",
    ],
    [
        "i32x4.shl",
        "0xFD  0xAB  0x01",
        "[v128 i32]→[v128]",
    ],
    [
        "i32x4.shr_s",
        "0xFD  0xAC  0x01",
        "[v128 i32]→[v128]",
    ],
    [
        "i32x4.shr_u",
        "0xFD  0xAD  0x01",
        "[v128 i32]→[v128]",
    ],
    [
        "i32x4.add",
        "0xFD  0xAE  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.sub",
        "0xFD  0xB1  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.mul",
        "0xFD  0xB5  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.min_s",
        "0xFD  0xB6  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.min_u",
        "0xFD  0xB7  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.max_s",
        "0xFD  0xB8  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.max_u",
        "0xFD  0xB9  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.dot_i16x8_s",
        "0xFD  0xBA  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.extmul_low_i16x8_s",
        "0xFD  0xBC  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.extmul_high_i16x8_s",
        "0xFD  0xBD  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.extmul_low_i16x8_u",
        "0xFD  0xBE  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.extmul_high_i16x8_u",
        "0xFD  0xBF  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.abs",
        "0xFD  0xC0  0x01",
        "[v128]→[v128]",
    ],
    [
        "i64x2.neg",
        "0xFD  0xC1  0x01",
        "[v128]→[v128]",
    ],
    [
        "i64x2.all_true",
        "0xFD  0xC3  0x01",
        "[v128]→[i32]",
    ],
    [
        "i64x2.bitmask",
        "0xFD  0xC4  0x01",
        "[v128]→[i32]",
    ],
    [
        "i64x2.extend_low_i32x4_s",
        "0xFD  0xC7  0x01",
        "[v128]→[v128]",
    ],
    [
        "i64x2.extend_high_i32x4_s",
        "0xFD  0xC8  0x01",
        "[v128]→[v128]",
    ],
    [
        "i64x2.extend_low_i32x4_u",
        "0xFD  0xC9  0x01",
        "[v128]→[v128]",
    ],
    [
        "i64x2.extend_high_i32x4_u",
        "0xFD  0xCA  0x01",
        "[v128]→[v128]",
    ],
    [
        "i64x2.shl",
        "0xFD  0xCB  0x01",
        "[v128 i32]→[v128]",
    ],
    [
        "i64x2.shr_s",
        "0xFD  0xCC  0x01",
        "[v128 i32]→[v128]",
    ],
    [
        "i64x2.shr_u",
        "0xFD  0xCD  0x01",
        "[v128 i32]→[v128]",
    ],
    [
        "i64x2.add",
        "0xFD  0xCE  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.sub",
        "0xFD  0xD1  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.mul",
        "0xFD  0xD5  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.eq",
        "0xFD  0xD6  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.ne",
        "0xFD  0xD7  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.lt_s",
        "0xFD  0xD8  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.gt_s",
        "0xFD  0xD9  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.le_s",
        "0xFD  0xDA  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.ge_s",
        "0xFD  0xDB  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.extmul_low_i32x4_s",
        "0xFD  0xDC  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.extmul_high_i32x4_s",
        "0xFD  0xDD  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.extmul_low_i32x4_u",
        "0xFD  0xDE  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i64x2.extmul_high_i32x4_u",
        "0xFD  0xDF  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.abs",
        "0xFD  0xE0  0x01",
        "[v128]→[v128]",
    ],
    [
        "f32x4.neg",
        "0xFD  0xE1  0x01",
        "[v128]→[v128]",
    ],
    [
        "f32x4.sqrt",
        "0xFD  0xE3  0x01",
        "[v128]→[v128]",
    ],
    [
        "f32x4.add",
        "0xFD  0xE4  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.sub",
        "0xFD  0xE5  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.mul",
        "0xFD  0xE6  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.div",
        "0xFD  0xE7  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.min",
        "0xFD  0xE8  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.max",
        "0xFD  0xE9  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.pmin",
        "0xFD  0xEA  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f32x4.pmax",
        "0xFD  0xEB  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.abs",
        "0xFD  0xEC  0x01",
        "[v128]→[v128]",
    ],
    [
        "f64x2.neg",
        "0xFD  0xED  0x01",
        "[v128]→[v128]",
    ],
    [
        "f64x2.sqrt",
        "0xFD  0xEF  0x01",
        "[v128]→[v128]",
    ],
    [
        "f64x2.add",
        "0xFD  0xF0  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.sub",
        "0xFD  0xF1  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.mul",
        "0xFD  0xF2  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.div",
        "0xFD  0xF3  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.min",
        "0xFD  0xF4  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.max",
        "0xFD  0xF5  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.pmin",
        "0xFD  0xF6  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "f64x2.pmax",
        "0xFD  0xF7  0x01",
        "[v128 v128]→[v128]",
    ],
    [
        "i32x4.trunc_sat_f32x4_s",
        "0xFD  0xF8  0x01",
        "[v128]→[v128]",
    ],
    [
        "i32x4.trunc_sat_f32x4_u",
        "0xFD  0xF9  0x01",
        "[v128]→[v128]",
    ],
    [
        "f32x4.convert_i32x4_s",
        "0xFD  0xFA  0x01",
        "[v128]→[v128]",
    ],
    [
        "f32x4.convert_i32x4_u",
        "0xFD  0xFB  0x01",
        "[v128]→[v128]",
    ],
    [
        "i32x4.trunc_sat_f64x2_s_zero",
        "0xFD  0xFC  0x01",
        "[v128]→[v128]",
    ],
    [
        "i32x4.trunc_sat_f64x2_u_zero",
        "0xFD  0xFD  0x01",
        "[v128]→[v128]",
    ],
    [
        "f64x2.convert_low_i32x4_s",
        "0xFD  0xFE  0x01",
        "[v128]→[v128]",
    ],
    [
        "f64x2.convert_low_i32x4_u",
        "0xFD  0xFF  0x01",
        "[v128]→[v128]",
    ],
];

function toSingleOpcode(
    code: string,
): string {
    return `0x${
        code.split(" ")
            .filter((v) => v)
            .map((v) => v.substring(2)).join("_")
    }`;
}

function prefix(
    type: string,
    name: string,
) {
    return `    ${name.substring(type.length + 1)}: `;
}

function formatOp(
    type: string,
    name: string,
    opcode: string,
): string {
    if (name.includes("shuffle")) {
        const split = name.split("shuffle");
        const order = parseInt(split[1]!.substring(8));
        name = `${split[0]}shuffle`;
        return `${prefix(type, name)}(laneidx: [${[...Array(order)].map(() => "number").join(", ")}]) => createSetArgs({
        title: "${name}",
        opcode: ${toSingleOpcode(opcode)},
        laneidx,
    })`;
    }
    if (
        (name.includes("load") || name.includes("store")) &&
        name.includes("lane")
    ) {
        if (name.endsWith(" laneidx")) {
            name = name.substring(0, name.length - " laneidx".length);
        }
        if (name.endsWith(" memarg")) {
            name = name.substring(0, name.length - " memarg".length);
        }
        return `${prefix(type, name)}(laneidx: number, offset: number, align?: number) => createSetArgs({
        title: "${name}",
        opcode: ${toSingleOpcode(opcode)},
        offset,
        align,
        laneidx,
    })`;
    }
    if (name.includes("load") || name.includes("store")) {
        if (name.endsWith(" memarg")) {
            name = name.substring(0, name.length - " memarg".length);
        }
        return `${prefix(type, name)}(offset: number, align?: number) => createSetArgs({
        title: "${name}",
        opcode: ${toSingleOpcode(opcode)},
        offset,
        align,
    })`;
    }
    if (name.includes("lane")) {
        if (name.endsWith(" laneidx")) {
            name = name.substring(0, name.length - " laneidx".length);
        }
        return `${prefix(type, name)}(laneidx: number) => createSetArgs({
        title: "${name}",
        opcode: ${toSingleOpcode(opcode)},
        laneidx,
    })`;
    }
    if (name.includes("const")) {
        name = `${name.split("const")[0]}const`;
        return `${prefix(type, name)}(v: number | bigint) => ({
        title: "${name}",
        opcode: ${toSingleOpcode(opcode)},
        value: v,
        type: ${type}Type,
        args: [],
    })`;
    }
    return `${prefix(type, name)}createSetArgs({
        title: "${name}",
        opcode: ${toSingleOpcode(opcode)},
    })`;
}

[
    "i32",
    "i64",
    "f32",
    "f64",
    "i8x16",
    "i16x8",
    "i32x4",
    "i64x2",
    "f32x4",
    "f64x2",
    "v128",
].forEach((t) => {

    fs.writeFileSync(
        `/home/nameless/IdeaProjects/audio-playground/frontend/src/utils/common/wasm/ops/opsList/${t.toUpperCase()}Ops.ts`,
        `
${!t.includes("x") ? `import { ${t}Type } from "../../Types";` : ""}
import { createSetArgs } from "../CreateSetArgs";

export const ${t}BasicOps = {
${DATA
        .map((v) => v as [string, string, string])
        .filter(([name]) => name.startsWith(`${t}.`))
        .map(([name, opcode]) => `${formatOp(t, name, opcode)},`)
        .join("\n")
}
};
`,
    );

});

