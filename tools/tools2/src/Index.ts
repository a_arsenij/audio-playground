import { byteToString } from "@audio-playground/lib-common/src/std/byteToString";
import { padStart } from "@audio-playground/lib-common/src/std/String";
import { compileModuleToWASM } from "@audio-playground/lib-wasm/src/CompileWASM";
import { compileModuleToWAT } from "@audio-playground/lib-wasm/src/CompileWAT";
import { createModule, func } from "@audio-playground/lib-wasm/src/Op";
import { f32x4, i32, v128 } from "@audio-playground/lib-wasm/src/ops";
import * as fs from "fs";
import { writeFileSync } from "fs";


const myModule = createModule()
    .memory(["js", "mem"], 1)
    .func(() => func()
        .exported(true)
        .name("f2call")
        .body((locals) => [
            i32.const(0),
            i32.const(0),
            v128.load(0),
            i32.const(0),
            v128.load(16),
            f32x4.add,
            v128.store(32),
        ]),

    )
    .build();

const wat = compileModuleToWAT(myModule);
fs.writeFileSync("Index.wat", wat);

const wasm = compileModuleToWASM(myModule);
const wasmBytes = new Uint8Array(wasm.bytecode);
fs.writeFileSync("Index.wasm", wasmBytes);

writeFileSync(
    "Index.wasm.hex",
    [...wasm.bytecode].map((byte, idx) => `${padStart((idx).toString(16), 7, "0")} : ${byteToString(byte)} ${wasm.comments[idx] ? `;; ${wasm.comments[idx]}` : ""}`).join("\n"),
);
