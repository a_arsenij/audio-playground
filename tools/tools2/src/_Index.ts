import * as fs from "fs";

type Chunk = {
    type: string,
    size: number,
    data: ArrayBuffer,
}

type ListChunk = Chunk & {
    contentType: string,
    content: Chunk[],
}

type StringChunk = Chunk & {
    content: string,
}

function readAsciiString(
    view: DataView,
    offset: number,
    length: number,
): string {
    const res: string[] = [];
    for (let i = 0; i < length; i++) {
        const b = view.getInt8(offset + i);
        if (b === 0) {
            break;
        }
        res.push(
            String.fromCharCode(b),
        );
    }
    return res.join("");
}

function readType(
    view: DataView,
    offset: number,
): string {
    return readAsciiString(view, offset, 4);
}

function parseChunks(
    data: ArrayBuffer,
): Chunk[] {
    const view = new DataView(data);
    const chunks: Chunk[] = [];
    let offset = 0;
    while (true) {
        const type = readType(view, offset);
        const size = view.getUint32(offset + 4, true);

        if (
            type === "RIFF" ||
            type === "LIST" ||
            type === "INFO"
        ) {
            const data = view.buffer.slice(
                offset + 12,
                offset + 12 + size - 4,
            );
            const chunk: ListChunk = {
                type,
                size,
                data,
                contentType: readType(view, offset + 8),
                content: parseChunks(data),
            };
            chunks.push(chunk);
        } else {
            const data = view.buffer.slice(
                offset + 8,
                offset + 8 + size,
            );
            chunks.push({
                type, size, data,
            });
        }
        offset += 8 + size;
        if (offset === view.byteLength) {
            break;
        } else if (offset > view.byteLength) {
            throw new Error(`Offset=${offset} is out of bounds=${view.byteLength} now`);
        }
    }
    return chunks;
}

const file: ArrayBuffer = fs.readFileSync(
    "/home/nameless/Samples/soundfonts/pianos/25 Piano Soundfonts/Electric Grand HQ.sf2",
).buffer;

const chunks = parseChunks(
    file,
);

type PHDR = {
    presetName: string,
    preset: number,
    bank: number,
    presetBadNdx: number,
    library: number,
    genre: number,
    morphology: number,
}[];

function readPHDR(
    chunk: Chunk,
): PHDR {
    if (chunk.type !== "phdr") {
        throw new Error("Expected phdr to be phdr");
    }
    const data = new DataView(chunk.data);
    const res: PHDR = [];
    let idx = 0;
    while (true) {
        res.push({
            presetName: readAsciiString(data, idx, 20),
            preset: data.getUint16(idx + 20),
            bank: data.getUint16(idx + 22),
            presetBadNdx: data.getUint16(idx + 24),
            library: data.getUint32(idx + 26),
            genre: data.getUint32(idx + 30),
            morphology: data.getUint32(idx + 34),
        });
        idx += 38;
        if (idx === data.byteLength) {
            break;
        } else if (idx > data.byteLength) {
            throw new Error("Out of bounds");
        }
    }
    return res;
}

type PBAG = {
    genNdx: number,
    modNdx: number,
}[];

function readPBAG(
    chunk: Chunk,
): PBAG {
    if (chunk.type !== "pbag") {
        throw new Error("Expected pbag to be pbag");
    }
    const data = new DataView(chunk.data);
    const res: PBAG = [];
    let idx = 0;
    while (true) {
        res.push({
            genNdx: data.getUint16(idx + 0),
            modNdx: data.getUint16(idx + 2),
        });
        idx += 4;
        if (idx === data.byteLength) {
            break;
        } else if (idx > data.byteLength) {
            throw new Error("Out of bounds");
        }
    }
    return res;
}

type INST = {
    instName: string,
    instBagNdx: number,
}[];

function readINST(
    chunk: Chunk,
): INST {
    if (chunk.type !== "inst") {
        throw new Error("Expected inst to be inst");
    }
    const data = new DataView(chunk.data);
    const res: INST = [];
    let idx = 0;
    while (true) {
        res.push({
            instName: readAsciiString(data, idx, 20),
            instBagNdx: data.getUint16(idx + 2),
        });
        idx += 22;
        if (idx === data.byteLength) {
            break;
        } else if (idx > data.byteLength) {
            throw new Error("Out of bounds");
        }
    }
    return res;
}

type SHDR = {
    sampleName: string,
    start: number,
    end: number,
    startLoop: number,
    endLoop: number,
    sampleRate: number,
    originalPitch: number,
    pitchCorrection: number,
    sampleLink: number,
    sampleType: number,
}[];

function readSHDR(
    chunk: Chunk,
): SHDR {
    if (chunk.type !== "shdr") {
        throw new Error("Expected shdr to be shdr");
    }
    const data = new DataView(chunk.data);
    const res: SHDR = [];
    let idx = 0;
    while (true) {
        res.push({
            sampleName: readAsciiString(data, idx, 20),
            start: data.getUint32(idx + 20),
            end: data.getUint32(idx + 24),
            startLoop: data.getUint32(idx + 28),
            endLoop: data.getUint32(idx + 32),
            sampleRate: data.getUint32(idx + 36),
            originalPitch: data.getInt8(idx + 40),
            pitchCorrection: data.getUint8(idx + 41),
            sampleLink: data.getUint16(idx + 42),
            sampleType: data.getUint16(idx + 44),
        });
        idx += 46;
        if (idx === data.byteLength) {
            break;
        } else if (idx > data.byteLength) {
            throw new Error("Out of bounds");
        }
    }
    return res;
}

type IGEN = {
    genOper: number,
    genAmount: number,
}[];

function readIGEN(
    chunk: Chunk,
): IGEN {
    if (chunk.type !== "igen") {
        throw new Error("Expected igen to be shdr");
    }
    const data = new DataView(chunk.data);
    const res: IGEN = [];
    let idx = 0;
    while (true) {
        res.push({
            genOper: data.getUint16(idx),
            genAmount: data.getUint16(idx + 2),
        });
        idx += 4;
        if (idx === data.byteLength) {
            break;
        } else if (idx > data.byteLength) {
            throw new Error("Out of bounds");
        }
    }
    return res;
}

function parseSoundFont(
    chunks: Chunk[],
) {
    const rootChunk = chunks[0];
    if (!rootChunk || chunks.length !== 1) {
        throw new Error("Expected only 1 root chunk");
    }
    if (!("content" in rootChunk)) {
        throw new Error("Expected root chunk to be RIFF list");
    }
    const listRaw = rootChunk.content;
    if (!Array.isArray(listRaw)) {
        throw new Error("Root->List chunk expected to contain list of chunks");
    }
    const list = listRaw as ListChunk[];
    if (list.length !== 3) {
        throw new Error("Expected list chunk to have exactly 3 elements");
    }
    const info = list[0]!;
    if (info.type !== "LIST" && info.contentType === "INFO") {
        throw new Error("Expected info to be info");
    }

    const sdta = list[1]!;
    if (sdta.type !== "LIST" && info.contentType === "sdta") {
        throw new Error("Expected sdta to be sdta");
    }
    const smpl = sdta.content[0];
    if (!smpl) {
        throw new Error("Expected smpl chunk to exist");
    }
    if (smpl.type !== "smpl") {
        throw new Error("Expected smpl to be smpl");
    }

    const pdta = list[2]!;
    if (pdta.type !== "LIST" && info.contentType === "sdta") {
        throw new Error("Expected pdta to be pdta");
    }
    if (pdta.content.length !== 9) {
        throw new Error("Expected pdta to contain exactly 9 chunks");
    }

    const phdr = readPHDR(
        pdta.content[0]!,
    );

    const pbag = readPBAG(
        pdta.content[1]!,
    );

    const pmod = pdta.content[2]!;
    const pgen = pdta.content[3]!;
    const inst = readINST(pdta.content[4]!);
    const ibag = pdta.content[5]!;
    const imod = pdta.content[6]!;
    const igen = readIGEN(pdta.content[7]!);
    const shdr = readSHDR(pdta.content[8]!);

    console.log(shdr);

}

parseSoundFont(chunks);
