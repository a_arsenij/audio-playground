const { readFileSync, writeFileSync } = require("fs");


const data = readFileSync(
    "Output.wasm",
);

const u8 = new Uint8Array(data);

function byteToString(b) {
    const res = b.toString(16);
    if (res.length < 2) {
        return `0${res}`;
    }
    return res;
}

const res = [...u8].map(byteToString).join("\n");

writeFileSync(
    "Output.wasm.hex",
    res,
);
