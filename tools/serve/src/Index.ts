import { serveFolder, setDefaultHeader, startHttpServer } from "./HttpServer";

setDefaultHeader("Cross-Origin-Opener-Policy", "same-origin");
setDefaultHeader("Cross-Origin-Embedder-Policy", "require-corp");
[
    "lib-common",
    "lib-frontend",
    "lib-sound",
    "lib-wasm",
].forEach((s) => {
    serveFolder(`/${s}/src`, `${s}/src`);
});
serveFolder("/src", "frontend/src");
serveFolder("/", "public");
startHttpServer(8080);
