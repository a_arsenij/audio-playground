import fs from "fs";
import http, { IncomingMessage, ServerResponse } from "http";
import { promisify } from "util";

const defaultHeaders = new Map<string, string>();
export function setDefaultHeader(k: string, v: string) {
    defaultHeaders.set(k, v);
}

function setDefaultHeaders(r: ServerResponse) {
    for (const [k, v] of defaultHeaders) {
        r.setHeader(k, v);
    }
}

export type MyServerResponse = ServerResponse & {
    setContentType(type: ContentType): MyServerResponse,
    setStatus(status: Status): MyServerResponse,
}

function myify(r: ServerResponse): MyServerResponse {
    const rr = r as any;
    rr.setContentType = (type: ContentType) => {
        rr.setHeader("Content-Type", type);
        return rr;
    };
    rr.setStatus = (status: Status) => {
        rr.writeHead(status);
        return rr;
    };
    return rr;
}

type MyRequestListener = (req: IncomingMessage, res: MyServerResponse) => void;

export type GetController = (
    queryParams: Map<string, string>,
    res: MyServerResponse
) => void

export type PostController = (
    body: any,
    res: MyServerResponse
) => void

export let http404Handler: MyRequestListener = (req, res) => {
    res
        .setStatus(Status.NOT_FOUND)
        .end("Route not found");
};
const routes: {
    matches: (req: IncomingMessage) => boolean,
    handler: MyRequestListener,
}[] = [];
const simpleMatch = (
    route: string,
    method: string,
) => (req: IncomingMessage) => method === req.method && route === req.url!.split("?")[0];
export const addRoute = (
    matches: (req: IncomingMessage) => boolean,
    handler: MyRequestListener,
) => {
    routes.push({
        matches,
        handler,
    });
};
export const get = (route: string, handler: GetController) => {
    routes.push({
        matches: simpleMatch(route, "GET"),
        handler: (req, res) => {
            const u = new URL(req.url!, "https://localhost");
            const params = new Map<string, string>();
            for (const [k, v] of u.searchParams) {
                params.set(k, v);
            }
            handler(params, res);
        },
    });
};
export const post = (route: string, handler: PostController) => {
    routes.push({
        matches: simpleMatch(route, "POST"),
        handler: (req, res) => {
            let body = "";
            req.addListener("data", (buffer) => {
                body += buffer;
            });
            req.addListener("end", () => {
                let j;
                try {
                    j = JSON.parse(body);
                } catch (e) {
                    return res
                        .setStatus(Status.BAD_REQUEST)
                        .end("Body is not a valid json");
                }
                handler(j, res);
            });
        },
    });
};
export const handle404 = (
    handler: MyRequestListener,
) => {
    http404Handler = handler;
};
export const respondRedirect = (
    res: ServerResponse,
    to: string,
) => {
    res
        .setHeader("Location", to)
        .writeHead(302)
        .end("Redirected");
};
export const redirectFromTo = (
    from: string,
    to: string,
) => {
    addRoute(
        (req) => req.url?.split("?")?.at(0) === from,
        (req, res) => respondRedirect(res, to),
    );
};

export const respondFile = async(
    req: IncomingMessage,
    res: MyServerResponse,
    file: string,
) => {

    // eslint-disable-next-line n/no-deprecated-api
    if (!await promisify(fs.exists)(file)) {
        return http404Handler(req, res);
    }

    const type: ContentType | undefined = (() => {
        const f = file.toLowerCase();
        if (f.endsWith(".svg")) {
            return ContentType.Image.SVG;
        }
        if (f.endsWith(".png")) {
            return ContentType.Image.PNG;
        }
        if (f.endsWith(".mp4")) {
            return ContentType.Video.MP4;
        }
        if (f.endsWith(".jpg") || f.endsWith(".jpeg")) {
            return ContentType.Image.JPEG;
        }
        if (f.endsWith(".js")) {
            return ContentType.Text.JAVASCRIPT;
        }
        if (f.endsWith(".css")) {
            return ContentType.Text.CSS;
        }
        if (f.endsWith(".html")) {
            return ContentType.Text.HTML;
        }
    })();

    if (type) {
        res.setContentType(type);
    }
    setDefaultHeaders(res);

    res
        .setStatus(Status.OK)
        .end(await promisify(fs.readFile)(file));

};
export const serveFile = (
    route: string,
    file: string,
) => {
    addRoute(
        (req) => {
            return req.url!!.split("?")[0] === route;
        },
        async(req, res) => {
            await respondFile(req, res, file);
        },
    );
};

function ensureEndsWith(
    s: string | unknown,
    c: string,
) {
    const ss = (`${s}`);
    if (ss.endsWith(c)) {
        return ss;
    } else {
        return ss + c;
    }
}

export const serveFolder = (
    route: string,
    folder: string,
) => {
    const f = ensureEndsWith(folder, "/");
    addRoute(
        (req) => {
            return req.url?.startsWith(route) || false;
        },
        async(req, res) => {
            let path = req.url!.split("?")[0]!;
            if (path === "/") {
                const pathToIndex = `${f}index.html`;
                return await respondFile(req, res, pathToIndex);
            }
            path = path.substring(route.length);
            path = decodeURIComponent(path);
            if (path.includes("..")) {
                return res
                    .setStatus(Status.BAD_REQUEST)
                    .end("Invalid path");
            }
            const filename = f + path;
            await respondFile(req, res, filename);
        },
    );
};
const requestListener = (req: IncomingMessage, res: ServerResponse) => {
    const myres = myify(res);
    for (const {
        matches,
        handler,
    } of routes) {
        if (matches(req)) {
            try {
                handler(req, myres);
            } catch (e) {
                console.error(e);
                res.writeHead(Status.INTERNAL_SERVER_ERROR);
                res.end("Internal error");
            }
            return;
        }
    }
    http404Handler(req, myres);
};
export const startHttpServer = (port: number | string) => {
    const server = http.createServer(requestListener);
    server.listen(parseInt(port.toString()));
    console.log(`\n\n        Started http server at\n        http://localhost:${port}\n\n`);
};

type LeafTypes<T> =
    T extends object
        ? LeafTypes<T[keyof T]>
        : T extends string
            ? T
            : T extends number
                ? T
                : never;

export const ContentType = {
    Application: {
        JSON: "application/json",
    },
    Text: {
        HTML: "text/html",
        CSS: "text/css",
        JAVASCRIPT: "text/javascript",
    },
    Image: {
        JPEG: "image/jpeg",
        SVG: "image/svg+xml",
        PNG: "image/png",
    },
    Video: {
        MP4: "video/mp4",
    },
} as const;

export type ContentType = LeafTypes<typeof ContentType>;

export const Status = {
    OK: 200,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    INTERNAL_SERVER_ERROR: 500,
} as const;

export type Status = LeafTypes<typeof Status>;
