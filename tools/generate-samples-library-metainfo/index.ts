import { error } from "@audio-playground/lib-common/std/Error";
import fs from "fs";
import * as process from "process";
import { WaveFile } from "wavefile";

function getArg(name: string): string {
    const splitter = `--${name}=`;
    return (process.argv
        .find((s) => s.startsWith(splitter)) ??
        error(`Please provide ${splitter} argument`)
    ).split(splitter)[1]!;
}

const wavsFolder = getArg("wavsFolder");
const metaFolder = getArg("metaFolder");

if (fs.existsSync(metaFolder)) {
    fs.rmSync(metaFolder, { recursive: true, force: true });
}

type Meta = {
    name: string,
    preview: string,
    sampleRate: number,
    channels: number,
    length: number,
}

const PREVIEW_LENGTH = 256;

function wavToMeta(
    prefix: string,
    wav: string,
): Meta {

    const wavFile = new WaveFile(fs.readFileSync(`${prefix}/${wav}`));

    // @ts-ignore
    const sampleRate: number = wavFile.fmt.sampleRate;
    // @ts-ignore
    const channels: number = wavFile.fmt.numChannels;
    // @ts-ignore
    let samples: Float64Array | (Float64Array[]) = wavFile.getSamples();
    if (Array.isArray(samples)) {
        const ss: Float64Array[] = samples;
        samples = ss[0]!.map((_: any, idx: number) =>
            ss
                .map((s) => s[idx]!)
                .reduce((a, b) => a + b) / ss.length,
        );
    }

    const downsampled: number[] = [];
    if (samples.length < PREVIEW_LENGTH) {
        downsampled.push(...Array.from(samples));
    } else {
        for (let i = 0; i < PREVIEW_LENGTH; i++) {
            const from = Math.floor(i * samples.length / PREVIEW_LENGTH);
            const to = Math.ceil((i + 1) * samples.length / PREVIEW_LENGTH - 1);
            let acc = 0;
            const func = i % 2 === 0 ? Math.min : Math.max;
            for (let j = from; j <= to; j++) {
                acc = func(acc, samples[j]!);
            }
            downsampled.push(acc);
        }
    }
    const max = downsampled
        .map(Math.abs)
        .reduce((a, b) => Math.max(a, b));
    const bytes = downsampled.map((v) => Math.round(v / max * 127 + 128));
    const preview = Buffer.from(
        new Uint8Array(bytes),
    ).toString("base64");
    return {
        name: wav,
        preview,
        sampleRate,
        channels,
        length: samples.length,
    };

}

function goDeep(
    suffix: string,
) {
    console.log("suffix", suffix);
    fs.mkdirSync(metaFolder + suffix);
    const sub = fs.readdirSync(wavsFolder + suffix);
    const wavs: string[] = [];
    const dirs: string[] = [];
    for (const f of sub) {
        if (fs.statSync(`${wavsFolder + suffix}/${f}`).isDirectory()) {
            goDeep(`${suffix}/${f}`);
            dirs.push(f);
        } else if (f.endsWith(".wav")) {
            wavs.push(f);
        }
    }
    const metas = wavs.map((w) => wavToMeta(
        `${wavsFolder + suffix}`,
        w,
    ));
    fs.writeFileSync(
        `${metaFolder + suffix}/index.json`,
        JSON.stringify({
            files: metas,
            dirs,
        }),
    );
}
goDeep("");
