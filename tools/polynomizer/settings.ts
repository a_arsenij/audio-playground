

export const FUNC = Math.log2;
export const FROM = 1;
export const TO = 2;
export const DEGREE = 8;
export const THREADS = 28;
