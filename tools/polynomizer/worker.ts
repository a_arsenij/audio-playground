import { parentPort } from "node:worker_threads";

import { findCoeffsGradient } from "./gradient";

parentPort!.on("message", (msg) => {
    const arr: number[] = msg.arr;
    const descendStep: number = msg.descendStep;
    const res = findCoeffsGradient(
        arr,
        descendStep,
    );
    parentPort!.postMessage({
        coeffs: res[0],
        diff: res[1],
    });
});
