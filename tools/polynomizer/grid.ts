import { Worker } from "node:worker_threads";

import { createSemaphore } from "@audio-playground/lib-common/src/std/Sync";

import { DEGREE, FROM, THREADS, TO } from "./settings";

const OVERAMP = 4;
const POOL_SIZE = OVERAMP * THREADS;
const semaphore = createSemaphore(POOL_SIZE);


function getOptionsNumber(
    wordLength: number,
    maxDigit: number,
) {

    const map = new Map<string, number>();

    function get(
        prefixLength: number,
        nextNumber: number,
    ): number {
        if (prefixLength === 0) {
            return 1;
        }
        const key = `${prefixLength}-${nextNumber}`;
        if (map.has(key)) {
            return map.get(key)!;
        }
        let res = 0;
        for (let i = nextNumber + 1; i < maxDigit; i++) {
            res += get(prefixLength - 1, i);
        }
        map.set(key, res);
        return res;
    }
    return get(wordLength, -1);

}

const BAR_WIDTH = 40;
function printProgress(
    done: number,
    total: number,
    bestError: number,
    betterThanPrev: boolean,
) {
    let doneString = `${done}`;
    const totalString = `${total}`;
    while (doneString.length < totalString.length) {
        doneString = ` ${doneString}`;
    }
    let percentString = `(${((100 * done) / total).toFixed(1)}%)`;
    while (percentString.length < 8) {
        percentString = ` ${percentString}`;
    }
    console.log(`Done: ${doneString}/${totalString} ${percentString} | [${
        [...Array(BAR_WIDTH)].map((_, idx) =>
            idx * total / BAR_WIDTH <= done ? "#" : " ",
        ).join("")
    }] | Best error: ${bestError.toExponential(2)}${betterThanPrev ? " (better than previous!)" : ""}`);
}

export async function findCoeffsGradientFromGrid(): Promise<[number[], number]> {

    const gridElements = Math.floor(DEGREE * 1.5);
    const variables = DEGREE - 2;
    let bestDiff = Number.POSITIVE_INFINITY;
    let bestCoeffs: number[] = [];

    const totalJobs = getOptionsNumber(
        DEGREE - 2,
        gridElements,
    );
    console.log(`Starting search, jobs to do: ${totalJobs}...\n`);
    let doneJobs = 0;

    const freeWorkers: Worker[] = [];
    [...Array(THREADS)].forEach(() => {
        const worker = new Worker("./worker", {
            execArgv: ["-r", "ts-node/register/transpile-only"],
        });
        worker.on("message", (msg: any) => {
            doneJobs++;
            const coeffs: number[] = msg.coeffs;
            const diff: number = msg.diff;
            const betterThanPrev = diff < bestDiff;
            if (betterThanPrev) {
                bestDiff = diff;
                bestCoeffs = coeffs;
            }
            printProgress(doneJobs, totalJobs, bestDiff, betterThanPrev);
            freeWorkers.push(worker);
            semaphore.unlock();
        });
        worker.on("exit", (code) => {
            if (code !== 0) {
                console.error(`Worker stopped with exit code ${code}`);
            }
        });
        for (let i = 0; i < OVERAMP; i++) {
            freeWorkers.push(worker);
        }
    });

    const diffs = [...Array(variables)].map((_, idx) =>
        Math.max(0, variables - idx - 1),
    );
    const gridStep = (TO - FROM) / (gridElements + 1);
    while (true) {

        await semaphore.lock();
        (async() => {
            const arr = diffs.map((a) => FROM + (a + 1) * gridStep);
            const worker = freeWorkers.pop();
            worker!.postMessage({
                arr,
                descendStep: gridStep / 4,
            });
        })();

        let overflow = true;
        let exit = false;
        while (true) {
            for (let i = 0; i < variables; i++) {
                if (overflow) {
                    overflow = false;
                    diffs[i]++;
                    if (diffs[i]! >= gridElements) {
                        if (i === variables - 1) {
                            exit = true;
                            break;
                        }
                        diffs[i]! = 0;
                        overflow = true;
                    }
                } else {
                    break;
                }
            }
            if (exit) {
                break;
            }
            let skip = false;
            for (let i = 1; i < diffs.length; i++) {
                const curr = diffs[i]!;
                const prev = diffs[i - 1]!;
                if (curr >= prev) {
                    skip = true;
                    break;
                }
            }
            if (!skip) {
                break;
            } else {
                overflow = true;
            }
        }
        if (exit) {
            break;
        }
        if (overflow) {
            break;
        }
    }
    for (let i = 0; i < POOL_SIZE; i++) {
        await semaphore.lock();
    }
    for (let i = 0; i < POOL_SIZE; i++) {
        semaphore.unlock();
    }
    return [bestCoeffs, bestDiff];
}
