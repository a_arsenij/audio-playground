const OP_SYMBOLS = {
    plus: "+",
    multiply: "*",
    divide: "/",
    power: "^",
};
function exprToString(e) {
    switch (e.type) {
        case "const":
            return `${e.value}`;
        case "variable":
            return e.variable;
        case "op2":
            return `(${exprToString(e.left)}${OP_SYMBOLS[e.op]}${exprToString(e.right)})`;
        case "opn":
            return `(${e.args.map(exprToString).join(OP_SYMBOLS[e.op])})`;
    }
}
function isConstExpr(v) {
    return v.type === "const";
}
function isVariableExp(v) {
    return v.type === "variable";
}
function isPlusOp(v) {
    return v.type === "opn" && v.op === "plus";
}
function isMultiplyOp(v) {
    return v.type === "opn" && v.op === "multiply";
}
function isDivideOp(v) {
    return v.type === "op2" && v.op === "divide";
}
function isPowerOp(v) {
    return v.type === "op2" && v.op === "power";
}
function isMultiplyDivideOp(v) {
    return isMultiplyOp(v) || isDivideOp(v);
}
function variable(v) {
    return {
        type: "variable",
        variable: v,
    };
}
function value(v) {
    return {
        type: "const",
        value: v,
    };
}
function toExpr(a) {
    if (typeof a === "number") {
        return value(a);
    }
    return a;
}
function plus(...other) {
    if (other.length === 0) {
        throw Error("???");
    }
    if (other.length === 1) {
        return toExpr(other[0]);
    }
    return {
        type: "opn",
        op: "plus",
        args: other.map(toExpr),
    };
}
function multiply(...other) {
    if (other.length === 0) {
        throw Error("???");
    }
    if (other.length === 1) {
        return toExpr(other[0]);
    }
    return {
        type: "opn",
        op: "multiply",
        args: other.map(toExpr),
    };
}
const createNExpr = {
    plus,
    multiply,
};
function divide(a, b) {
    return {
        type: "op2",
        op: "divide",
        left: toExpr(a),
        right: toExpr(b),
    };
}
function power(a, b) {
    return {
        type: "op2",
        op: "power",
        left: toExpr(a),
        right: toExpr(b),
    };
}
function expandTwoPolynomialsMultiplication(left, right) {
    return plus(...left.args.map((l) => right.args.map((r) => multiply(l, r))).flat());
}
function expandPolynomialsMultiplication(...members) {
    const copy = [...members];
    let res = expandTwoPolynomialsMultiplication(copy.pop(), copy.pop());
    while (copy.length > 0) {
        res = expandTwoPolynomialsMultiplication(res, copy.pop());
    }
    return res;
}
function expandSingleMultiplication(op) {
    if (isMultiplyOp(op)) {
        const plusOps = [];
        const otherOps = [];
        op.args.forEach((a) => {
            if (isPlusOp(a)) {
                plusOps.push(a);
            }
            else {
                otherOps.push(a);
            }
        });
        if (plusOps.length >= 1) {
            const exped = plusOps.length === 1 ? plusOps[0] : expandPolynomialsMultiplication(...plusOps);
            if (otherOps.length === 0) {
                return exped;
            }
            const otherop = multiply(...otherOps);
            return plus(...exped.args.map((a) => multiply(a, otherop)).flat());
        }
    }
    return op;
}
function expandMultiplications(op) {
    const un = applyToMembers(op, expandMultiplications);
    const res = expandSingleMultiplication(un);
    return res;
}
function applyToMembers(expr, a) {
    if (expr.type === "op2") {
        const left = a(expr.left);
        const right = a(expr.right);
        if (left !== expr.left || right !== expr.right) {
            return {
                ...expr,
                left,
                right,
            };
        }
        else {
            return expr;
        }
    }
    if (expr.type === "opn") {
        const changed = [];
        let c = false;
        for (const e of expr.args) {
            const n = a(e);
            if (n !== e) {
                c = true;
            }
            changed.push(n);
        }
        if (c) {
            return {
                ...expr,
                args: changed,
            };
        }
        else {
            return expr;
        }
    }
    return expr;
}
function glueNOps(expr) {
    const membersApplied = applyToMembers(expr, glueNOps);
    if (membersApplied.type === "op2") {
        return membersApplied;
    }
    if (isConstExpr(membersApplied) ||
        isVariableExp(membersApplied)) {
        return membersApplied;
    }
    const args = [];
    for (const a of membersApplied.args) {
        if (isConstExpr(a) ||
            isVariableExp(a) ||
            a.type === "op2" ||
            a.op !== membersApplied.op) {
            args.push(a);
        }
        else {
            args.push(...a.args);
        }
    }
    ;
    if (args.length !== membersApplied.args.length) {
        return {
            ...membersApplied,
            args,
        };
    }
    return membersApplied;
}
function wrapConstOps(a) {
    const expr = applyToMembers(a, wrapConstOps);
    if (isConstExpr(expr) ||
        isVariableExp(expr)) {
        return expr;
    }
    if (expr.type === "op2") {
        if (isConstExpr(expr.left) && isConstExpr(expr.right)) {
            switch (expr.op) {
                case "divide":
                    return value(expr.left.value / expr.right.value);
                case "power":
                    return value(expr.left.value ** expr.right.value);
            }
        }
        return expr;
    }
    const constOps = [];
    const otherOps = [];
    for (const a of expr.args) {
        if (isConstExpr(a)) {
            constOps.push(a.value);
        }
        else {
            otherOps.push(a);
        }
    }
    if (constOps.length >= 2) {
        let collectedValue = constOps[0];
        if (expr.op === "multiply") {
            for (let i = 1; i < constOps.length; i++) {
                collectedValue *= constOps[i];
            }
        }
        else {
            for (let i = 1; i < constOps.length; i++) {
                collectedValue += constOps[i];
            }
        }
        const v = value(collectedValue);
        if (otherOps.length === 0) {
            return v;
        }
        return {
            ...expr,
            args: [
                v,
                ...otherOps,
            ],
        };
    }
    return expr;
}
function extractVariablePowsForSpecificVariable(expr, vari) {
    if (!isMultiplyOp(expr)) {
        return expr;
    }
    const pows = [];
    const otherOps = [];
    expr.args.forEach((a) => {
        if (a.type === "variable" && a.variable === vari) {
            pows.push(power(a, 1));
            return;
        }
        if (isPowerOp(a) &&
            a.left.type === "variable" &&
            a.left.variable === vari) {
            pows.push(a);
            return;
        }
        otherOps.push(a);
    });
    if (pows.length >= 2) {
        const powed = power(variable(vari), plus(...pows.map((p) => p.right)));
        if (otherOps.length === 0) {
            return powed;
        }
        return multiply(powed, ...otherOps);
    }
    return expr;
}
function extractVariablePows(expr) {
    if (isMultiplyOp(expr)) {
        const variables = [...new Set(expr.args.filter(isVariableExp).map((v) => v.variable))];
        let res = expr;
        for (const v of variables) {
            res = extractVariablePowsForSpecificVariable(res, v);
        }
        return res;
    }
    return applyToMembers(expr, extractVariablePows);
}
function divideToConstToMultiplyToConst(expr) {
    if (isDivideOp(expr) && isConstExpr(expr.right)) {
        return multiply(expr.left, 1 / expr.right.value);
    }
    return applyToMembers(expr, divideToConstToMultiplyToConst);
}
function singleMultiplicationOfDivisionsToDivisionsOfMultiplications(expr) {
    if (expr.type === "opn" && expr.op === "multiply") {
        const divisions = [];
        const other = [];
        expr.args.forEach((a) => {
            if (isDivideOp(a)) {
                divisions.push(a);
            }
            else {
                other.push(a);
            }
        });
        return divide(multiply(...divisions.map((d) => d.left), ...other), multiply(...divisions.map((d) => d.right)));
    }
    return expr;
}
function getPolyMember(e, vari) {
    const expr = prettify(e);
    if (isVariableExp(expr) && expr.variable === vari) {
        return { power: 1, multiplier: value(1) };
    }
    if (isPowerOp(expr) && isVariableExp(expr.left) && expr.left.variable === vari && isConstExpr(expr.right)) {
        return { power: expr.right.value, multiplier: value(1) };
    }
    if (isMultiplyOp(expr)) {
        const mul = expr.args.find((f) => isVariableExp(f) && f.variable === vari);
        if (mul) {
            const args = expr.args.filter((f) => f !== mul);
            return { power: 1, multiplier: args.length === 1 ? args[0] : multiply(...args) };
        }
        const pow = expr.args.find((f) => isPowerOp(f) &&
            isVariableExp(f.left) &&
            f.left.variable === vari &&
            isConstExpr(f.right));
        if (pow) {
            return { power: pow.right.value, multiplier: multiply(...expr.args.filter((f) => f !== pow)) };
        }
    }
    return { power: 0, multiplier: e };
}
function getPolynomialCoeffs(e, vari) {
    const expr = prettify(e);
    if (!isPlusOp(expr)) {
        const mem = getPolyMember(e, "x");
        const res = [mem.multiplier];
        while (res.length <= mem.power) {
            res.push(value(0));
        }
        return res;
    }
    const pows = new Map();
    function pushPow(pow, e) {
        const old = pows.get(pow) ?? [];
        old.push(e);
        pows.set(pow, old);
    }
    const t = expr.args
        .map((a) => getPolyMember(a, vari));
    t.forEach((m) => pushPow(m.power, m.multiplier));
    const max = Math.max(...pows.keys());
    return [...Array(max + 1)]
        .map((_, idx) => max - idx)
        .map((idx) => pows.get(idx) ?? [value(0)])
        .map((a) => prettify(plus(...a)));
}
function groupAdditionsOfSameVariablePows(e, vari) {
    const coeffs = getPolynomialCoeffs(e, vari);
    if (!coeffs) {
        return applyToMembers(e, (e) => groupAdditionsOfSameVariablePows(e, vari));
    }
    return plus(...coeffs.map((v, idx) => multiply(v, power(variable(vari), coeffs.length - idx - 1))));
}
function removeNeutralElementForOp(expr, neutral) {
    const filtered = [];
    for (const v of expr.args) {
        if (v.type !== "const" || v.value !== neutral) {
            filtered.push(v);
        }
    }
    if (filtered.length !== expr.args.length) {
        if (filtered.length === 0) {
            return value(neutral);
        }
        if (filtered.length === 1) {
            return filtered[0];
        }
        return createNExpr[expr.op](...filtered);
    }
    return expr;
}
function removeNeutralElements(expr) {
    const sub = applyToMembers(expr, removeNeutralElements);
    if (isPlusOp(sub)) {
        return removeNeutralElementForOp(sub, 0);
    }
    if (isMultiplyOp(sub)) {
        return removeNeutralElementForOp(sub, 1);
    }
    return sub;
}
function prettify(expr) {
    let res = expr;
    while (true) {
        let c = res;
        c = glueNOps(c);
        c = wrapConstOps(c);
        c = extractVariablePows(c);
        c = divideToConstToMultiplyToConst(c);
        c = removeNeutralElements(c);
        if (c === res) {
            return c;
        }
        else {
            res = c;
        }
    }
}
export function getLagrangePolynomialCoeffs(rawPoints) {
    const map = new Map();
    rawPoints.forEach((p) => map.set(p[0], p[1]));
    const points = [...map.entries()];
    const basisPolynomials = [];
    for (let i = 0; i < points.length; i++) {
        const [xi, yi] = points[i];
        const basisLMultipliers = points.map(([xj, yj], j) => j !== i
            ? divide(plus(variable("x"), multiply(-1, xj)), plus(xi, multiply(-1, xj)))
            : value(1));
        const oneOf = multiply(yi, ...basisLMultipliers);
        basisPolynomials.push(oneOf);
    }
    const combination = prettify(plus(...basisPolynomials));
    const b = expandMultiplications(combination);
    const c = prettify(b);
    const d = groupAdditionsOfSameVariablePows(c, "x");
    const e = prettify(d);
    const coeffs = getPolynomialCoeffs(e, "x").map((v) => {
        if (v.type !== "const") {
            throw new Error("?");
        }
        return v.value;
    });
    return coeffs;
}
/*
const a = prettify(groupAdditionsOfSameVariablePows(prettify(expandMultiplications(multiply(
    plus(
        multiply(variable("a1"), power(variable("x"), 2)),
        multiply(variable("b1"), power(variable("x"), 1)),
        multiply(variable("c1"), power(variable("x"), 0)),
    ),
    plus(
        multiply(variable("a2"), power(variable("x"), 2)),
        multiply(variable("b2"), power(variable("x"), 1)),
        multiply(variable("c2"), power(variable("x"), 0)),
    ),
))), "x"));
console.log(
    exprToString(a),
);
*/
//# sourceMappingURL=lagrange.js.map