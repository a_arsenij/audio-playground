import { findCoeffsGradientFromGrid } from "./grid";

(async() => {
    const result = await findCoeffsGradientFromGrid();
    console.log(`\nDone!\nPolynomial coeffs: \n[\n    /* Err: ${result[1].toExponential(4)} */\n${result[0].map((v) => `    ${v},\n`).join("")}]`);
    process.exit(0);
})();
