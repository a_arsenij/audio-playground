

import { getLagrangePolynomialCoeffs } from "./lagrange";
import { FROM, FUNC, TO } from "./settings";

function getCoefs(
    points: [number, number][],
    func: (x: number) => number,
    xFrom: number,
    xTo: number,
): number[] {
    const rawPoints: [number, number][] = [
        [xFrom, func(xFrom)],
        ...points,
        [xTo, func(xTo)],
    ];
    return getLagrangePolynomialCoeffs(rawPoints);
}


function getPolyValue(
    x: number,
    coefs: number[],
) {
    let sum = 0;
    let xPow = 1;
    for (let i = coefs.length - 1; i >= 0; i--) {
        sum += coefs[i]! * xPow;
        xPow *= x;
    }
    return sum;
}

const CHECK_STEP = 0.001;
function getWorstDiff(
    xes: number[],
): number {
    const coefs = getCoefs(
        xes.map((x) => [x, FUNC(x)]),
        FUNC,
        FROM,
        TO,
    );
    let max = 0;
    for (let x = FROM; x <= TO; x += CHECK_STEP) {
        max = Math.max(
            max,
            Math.abs(FUNC(x) - getPolyValue(x, coefs)),
        );
    }
    return max;
}

export function findCoeffsGradient(
    initial: number[],
    initialDescendStep: number,
): [number[], number] {


    let descendStep = initialDescendStep;
    const descends = [-1, 1];
    let bestDiff = getWorstDiff(initial);
    const bestXes = [...initial];

    while (true) {
        let bestFound: [number, number, number] | undefined;
        for (let i = 0; i < bestXes.length; i++) {
            const original = bestXes[i]!;
            for (const d of descends) {
                const dd = d * descendStep;
                const val = original + dd;
                if (val <= FROM || val >= TO) {
                    continue;
                }
                bestXes[i] = val;
                const err = getWorstDiff(bestXes);
                if (err < bestDiff) {
                    bestDiff = err;
                    bestFound = [err, i, val];
                }
            }
            bestXes[i] = original;
        }
        if (bestFound === undefined) {
            descendStep /= 2;
            if (descendStep < 1e-10) {
                break;
            }
        } else {
            bestDiff = bestFound[0];
            bestXes[bestFound[1]] = bestFound[2];
        }
    }
    const res = getCoefs(
        bestXes.map((x) => [x, FUNC(x)]),
        FUNC,
        FROM,
        TO,
    );
    return [res, bestDiff];
}
