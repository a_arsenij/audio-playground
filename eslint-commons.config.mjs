import globals from "globals";

import tsParser from "@typescript-eslint/parser";
import stylisticTs from "@stylistic/eslint-plugin-ts";
import typescriptEslint from "@typescript-eslint/eslint-plugin";
import simpleImportSort from "eslint-plugin-simple-import-sort";

const badEndiannessTypeMessage = "This type uses platform-dependent endianness. Please, use DataView instead.";
const badEndiannessSetCallMessage = "This method requires third argument (littleEndian) to be set.";
const badEndiannessGetCallMessage = "This method requires second argument (littleEndian) to be set.";
const badEndiannessMethods = [
    "Int16",
    "Uint16",

    "Int32",
    "Uint32",

    "BigInt64",
    "BigUint64",

    "Float32",
    "Float64",
];
const badEndiannessTypes = [
    "Int8",
    "Uint8",
    ...badEndiannessMethods,
];

export const commonNoRestrictedSyntax = badEndiannessMethods.map((t) => [
    {
        message: badEndiannessSetCallMessage,
        selector: `CallExpression[arguments.length!=3] > MemberExpression[property.name='set${t}']`,
    },
    {
        message: badEndiannessGetCallMessage,
        selector: `CallExpression[arguments.length!=2] > MemberExpression[property.name='get${t}']`,
    },
]).flat();

export const codeSpecificNoRestrictedSyntax = [{
    message: "Do not use /src/ in imports",
    selector: "ImportDeclaration[source.value=/\\u002Fsrc\\u002F/]",
}];

export const testSpecificNoRestrictedSyntax = [{
    message: "Do not use @.../src/ in imports",
    selector: "ImportDeclaration[source.value=/@.*\\u002Fsrc\\u002F/]",
}];

export const languageOptions = {
    globals: {
        ...globals.browser,
    },

    parser: tsParser,
    ecmaVersion: "latest",
    sourceType: "module",

    parserOptions: {
        project: "./tsconfig.json",
    },
};

export const plugins = {
    "@typescript-eslint": typescriptEslint,
    "simple-import-sort": simpleImportSort,
    "@stylistic/ts": stylisticTs,
};

export const commonRules = {
    "no-mixed-operators": "off",
    quotes: ["warn", "double"],

    "max-len": ["off", {
        code: 100,
        ignoreTemplateLiterals: true,
        ignorePattern: "^import .* from \".*\";$",
    }],

    "no-tabs": ["warn", {
        allowIndentationTabs: false,
    }],

    indent: ["warn", 4, {
        ignoreComments: false,
        SwitchCase: 1,
        MemberExpression: 1,
    }],

    semi: ["warn", "always"],

    "comma-dangle": ["warn", {
        arrays: "always-multiline",
        objects: "always-multiline",
        imports: "always-multiline",
        exports: "always-multiline",
        functions: "always-multiline",
    }],

    "prefer-template": ["warn"],
    "simple-import-sort/imports": "warn",
    "object-curly-spacing": ["warn", "always"],
    curly: ["warn", "all"],

    "keyword-spacing": ["warn", {
        before: true,
        after: true,
    }],

    "arrow-parens": ["warn", "always"],

    "arrow-spacing": ["warn", {
        before: true,
        after: true,
    }],

    "brace-style": ["warn", "1tbs"],
    "no-useless-constructor": "off",
    "@typescript-eslint/switch-exhaustiveness-check": "error",

    "@stylistic/ts/member-delimiter-style": ["error", {
        multiline: {
            delimiter: "comma",
            requireLast: true,
        },
    }],
    "@stylistic/ts/type-annotation-spacing": ["warn", {
        "before": true,
        "after": true,
        "overrides": {
            "colon": {
                "before": false,
                "after": true,
            },
            "arrow": {
                "before": true,
            },
        },
    }],
    "@stylistic/ts/func-call-spacing": ["warn", "never"],

    "func-call-spacing": "off",
    "no-use-before-define": "off",
    "space-before-function-paren": ["warn", "never"],
    "array-callback-return": "off",

    "@typescript-eslint/no-unused-vars": "off",
    "no-unused-vars": "off",

    "padded-blocks": "off",

    "no-trailing-spaces": ["warn", {
        ignoreComments: true,
    }],

    "no-multiple-empty-lines": "off",
    "no-useless-return": "off",
    "no-new-func": "off",
    "no-redeclare": "off",
    "@typescript-eslint/no-redeclare": "error",
    "no-extend-native": "off",
};

export const initEslint = {
    files: ["**/*.ts", "**/*.tsx"],
    plugins,
    languageOptions,
}

export const codeConfigMatch = {
    files: ["**/*.ts", "**/*.tsx"],
    ignores: ["**/test/**/*.ts"],
}

export const codeConfig = {
    ...codeConfigMatch,
    rules: {
        ...commonRules,
        "no-restricted-syntax": [
            "error",
            ...codeSpecificNoRestrictedSyntax,
            ...commonNoRestrictedSyntax,
        ],
    },
};

export const testConfigMatch = {
    files: ["**/test/**/*.ts"],
}

export const testConfig = {
    ...testConfigMatch,
    rules: {
        ...commonRules,
        "no-restricted-syntax": [
            "error",
            ...testSpecificNoRestrictedSyntax,
            ...commonNoRestrictedSyntax
        ],
    },
};
